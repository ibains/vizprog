package Dse

import Metadata.U
import org.apache.tinkerpop.gremlin.structure.Vertex

//object U {
//  def Q(s: String) : String = "\"" + s + "\""
//  def Q(a: Any) : Any = a match {
//    case n: Number => a
//    case s: String => Q(a.toString)
//    case c: Char   => Q(a.toString)
//    case _         => ""
//  }
//  def ?(s: Option[String]) : String = if (s.isDefined) Q(s.get) else ""
//}

object QueryBuilder {
  // Vertex, Edge
  sealed abstract class GraphMutationQuery(label: String, properties: List[(String, Any)]) {
    def getQueryPrefix: String

    override def toString: String = {
      val query = new StringBuilder
      val prefix = getQueryPrefix
      query ++= prefix
      properties.foreach{case (id, value) => query ++= s", ${U.Q(id)}, ${U.Q(value)}"}
      query ++= ")"
      query.toString
    }
  }

  case class addVQ(label: String, properties: List[(String, Any)]) extends GraphMutationQuery(label, properties) {
    override def getQueryPrefix = s"graph.addVertex(label, ${U.Q(label)}"
  }

  val emptyQ: String = ""

  sealed abstract class GraphAccessQuery(base: String) {
    var query = new StringBuilder(base)
    override def toString: String = query.toString

    def values(): String = { query ++= ".values()" ; query.toString }
    def valueByProperty(property: String): String = { query ++= s".values(${U.Q(property)}"; query.toString }
    def count(): String = { query ++= ".count()" ; query.toString }
  }

  case class VQBuilder(base: String) extends GraphAccessQuery(base: String) {
    if (base.isEmpty) query ++= "g.V()"

    def findVByLabelPropQ(label: String, property: String, value: Any): VQBuilder = {
      val p = if (property == "id") "member_id" else property
      query ++= s".has(${U.Q(label)}, ${U.Q(p)}, ${U.Q(value)})"
      this
    }

    def findVByLabelQ(label: String) : VQBuilder = {
      query ++= s".hasLabel(${U.Q(label)})"
      this
    }

    def findVByEQ(edge: (String, Option[String]) ) : VQBuilder = {
      query ++= s".${edge._1}(${U.?(edge._2)})"
      this
    }

    def outE(label: Option[String]): VQBuilder = {
      if (label.isDefined) query ++= s".outE(${U.Q(label)})" else query ++= ".outE()"
      this
    }

    def inE(label: Option[String]): VQBuilder = {
      if (label.isDefined) query ++= s".inE(${U.Q(label)})" else query ++= ".inE()"
      this
    }

    def outV(): VQBuilder = {
      query ++= ".outV()"
      this
    }

    def inV(): VQBuilder = {
      query ++= ".inV()"
      this
    }
  }

  case class EQBuilder(base: String) extends GraphAccessQuery(base: String) {
    if (base.isEmpty) query ++= "g.E()"

    def findEByLabelQ(label: String): EQBuilder = {
      query ++= s".hasLabel(${U.Q(label)})"
      this
    }

    def outV(): EQBuilder = {
      query ++= ".outV()"
      this
    }

    def inV(): EQBuilder = {
      query ++= ".inV()"
      this
    }
  }

  /*case class findVQ(label: String, property: Option[(String, String)])
    extends BaseVQ {
    if (property.isDefined) {
      val pTuple = property.get
      query ++= s".has(${U.Q(label)}, ${U.Q(pTuple._1)}, ${U.Q(pTuple._2)})"
    } else {
      query ++= s".hasLabel($label)"
    }
  }

  case class findVByLabelQ(queryPattern: GraphAccessQuery, label: String) extends BaseVQ {
    query ++= s".hasLabel($label)"
  }

  case class findVByLabelPropQ(queryPattern: GraphAccessQuery, label: String, property: Option[(String, String)]) extends BaseVQ {
    query.clear()
    query ++= queryPattern.toString
    findVQ(label, property)
  }

  case class findVByEQ(queryPattern: GraphAccessQuery, edges: List[(String, Option[String])], property: Option[String]) extends BaseVQ {
    query ++= queryPattern.query
    edges.foreach{ case (direction, label) => query ++= s".$direction(${U.?(label)})" }
//    query.toString
  }

  case class findEByLabelQ(label: String) extends BaseEQ {
    query ++= s".hasLabel(${U.Q(label)})"
  }

  case class outE(queryPattern: GraphAccessQuery, label: Option[String]) extends GraphAccessQuery {
    if (label.isDefined) query ++= s".outE(${U.Q(label)})" else query ++= ".outE()"
  }

  case class inE(queryPattern: GraphAccessQuery, label: Option[String]) extends GraphAccessQuery {
    if (label.isDefined) query ++= s".inE(${U.Q(label)})" else query ++= ".inE()"
  }

  case class outV(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".inV()" }
  case class inV(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".outV()" }
  case class values(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".values()" }
  case class valueByProperty(queryPattern: GraphAccessQuery, property: String) extends GraphAccessQuery { query ++= s".values(${U.Q(property)}" }
  case class count(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".count()" }
  */
}
