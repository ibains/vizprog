package Dse

import java.util

import Dse.QueryBuilder.{GraphAccessQuery, GraphMutationQuery}
import Metadata._
import com.datastax.driver.dse.graph._
import com.datastax.driver.dse.{DseCluster, DseSession}
import com.google.common.reflect.TypeToken
import org.apache.tinkerpop.gremlin.structure.{Edge, Vertex}
import com.datastax.driver.dse.graph.SimpleGraphStatement

import scala.collection.JavaConversions._

//class DSEDriver extends AutoCloseable {
//  import DSEDriver._
//
//  val (cluster, session) = connect()
//  initialize()
//
//  def getSession : DseSession = session
//
//  override def close(): Unit = disconnect(session, cluster)
//}

object K {

  val devIp = "54.152.95.174"
}

object DSEDriver {

  // TODO: Hacky constructor use to avoid session param everywhere
  val (cluster, session) = connect()
//  initialize()
  sys.addShutdownHook(disconnect(session, cluster))

  def printSchema() = {
    val propertyKeySchema = dseKey.schemaString
    val vertexSchema      = dseVertex.schemaString
    val edgeSchema        = dseEdge.schemaString
    val indexSchema       = dseIndex.schemaString

    println(propertyKeySchema)
    println()
    println(vertexSchema)
    println()
    println(edgeSchema)
    println()
    println(indexSchema)
  }

  def connect() = {

    var cluster: DseCluster = null
    var session: DseSession = null

    cluster = DseCluster.builder()
      .addContactPoint("dev.vizprog.com")
      .withGraphOptions(new GraphOptions().setGraphName("dev"))
      .withPort(9042)
      .build()

    session = cluster.connect()

    (cluster, session)
  }

  def disconnect(session: DseSession, cluster: DseCluster): Unit = {
    if (!session.isClosed) session.close()
    if (!cluster.isClosed) cluster.close()
  }

  // NOTE: Setting graph name after creating cluster doesn't work, so need to
  // create schema, close session and create new session with graph name set
  // for queries of the form "g.V()" to work !!

  def initialize(): Unit = {
    val (cluster, session) = connect()

    session.executeGraph("system.graph(\"dev\").option(\"graph.allow_scan\")" +
      ".set(\"true\").option(\"graph.schema_mode\").set(\"Development\")" +
      ".ifNotExists().create()")

    session.executeGraph("schema.clear()")

    dseKey.keys.foreach(e => session.executeGraph(e._2.toString))
    dseVertex.vertices.foreach(e => session.executeGraph(e._2.toString))
    dseEdge.edges.foreach(e => session.executeGraph(e._2.toString))
    dseIndex.indexes.foreach(e => session.executeGraph(e._2.toString))
    disconnect(session, cluster)
  }

  def runTests(): Unit = {
    println("Vertices:" + session.executeGraph("g.V().count()").one())
    val scenarios = new MetadataScenarios()
    scenarios.NewCompanyCreation()
    scenarios.AddUsersToCompany()
    scenarios.AddGroupAndProjects()
    scenarios.AddWorkflowsAndDatasets()
    println("Vertices:" + session.executeGraph("g.V().count()").one())
  }

  def main(args: Array[String]): Unit = {
    //DSEDriver.printSchema()
    val (cluster, session) = connect()

    try {
//      initialize()
      // Drop all data without dropping a graph and schema, drop all vertices.
      session.executeGraph("g.V().drop().iterate()")
      runTests()
      session.executeGraph("g.V().count()")
    } finally {
      disconnect(session, cluster)
    }
    sys.exit()
  }

  // Coerce dse to tinkerpop data structures
  // Mutations
  def addVertex(query: GraphMutationQuery) : Vertex = {
    println(s"Execute :: ${query.toString}")
    session.executeGraph(query.toString).one().as(new TypeToken[Vertex]() {})
  }

  def addEdge(v1: Vertex, label: String, v2: Vertex) : Edge = {
    println("Execute :: addEdge: " + v1.id() + " -- " + label + " --> " + v2.id())
    val s = new SimpleGraphStatement(
      "def v1 = g.V(id1).next()\n" +
        "def v2 = g.V(id2).next()\n" +
          s"v1.addEdge(${U.Q(label)}, v2)")
      .set("id1", v1.id())
      .set("id2", v2.id())
    session.executeGraph(s).one().as(new TypeToken[Edge]() {})
    //    session.executeGraph(query.toString).one().as(new TypeToken[Edge]() {})
  }

  // Queries
  def getVertex(query: GraphAccessQuery): Vertex = {
    println(s"Execute :: ${query.toString}")
    session.executeGraph(query.toString).one().as(new TypeToken[Vertex]() {})
  }


  def getVertices(query: GraphAccessQuery): List[Vertex] = {
    println(s"Execute :: ${query.toString}")
    val nodes: util.List[GraphNode] = session.executeGraph(query.toString).all()
    nodes.map(e => e.as(new TypeToken[Vertex]() {})).toList
  }

  def getEdge(query: GraphAccessQuery): Edge = {
    println(s"Execute :: ${query.toString}")
    session.executeGraph(query.toString + ".next()").one().as(new TypeToken[Edge]() {})
  }

  def getEdges(query: GraphAccessQuery): List[Edge] = {
    println(s"Execute :: ${query.toString}")
    val nodes: util.List[GraphNode] = session.executeGraph(query.toString + ".next()").all()
    nodes.map(e => e.as(new TypeToken[Edge]() {})).toList
  }
}

/* 1. CREATE PROPERTIES
Property key definition
 - unique key
 - data type
 - cardinality
 - meta-properties

  schema.propertyKey("year").Int().single().create();
  schema.propertyKey("source").Text().create();
  schema.propertyKey("date").Timestamp().create()
  // Multi-property key with two meta-properties
  schema.propertyKey("budget").Text().multiple().properties("source","date").create();
*/

/* 2. CREATE VERTEX
// Vertex label with six associated property keys
  schema.vertexLabel("movie").
       properties("movieId","title","year", "duration","country","production").
       create();
 */


