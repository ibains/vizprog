package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Workflow {

  // ADDERS

  def + (project: Project, name: String, contents: String) : Workflow = {
    val w = new Workflow(
      addVertex(addVQ(DB_VERTEX.workflow, List(
        (DB_PROPERTY.name, name), (DB_PROPERTY.contents, contents), (DB_PROPERTY.created, Util.tt)
      )))
    )
//    val w = new Workflow(Graph.get + DB_Workflow(None, name, contents, Util.tt))
    project --> w
    w
  }

  // GETTERS - ALL

  def getAll : List[Workflow] = {
    getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.workflow)
    ).map(x => new Workflow(x))
//    Graph.get.V.hasLabel(DB_LABELS.workflow).toList.map(x => new Workflow(x))
  }

  // GETTERS - FILTERED

  def get(name: String) : Workflow = {
    new Workflow(getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.workflow, DB_PROPERTY.name, name)
    ))
//    new Workflow(Graph.get.V.hasLabel(DB_LABELS.workflow).has(DB_KEY.name, name).head())
  }
}

class Workflow(v: Vertex) extends SimpleEntity(v) {
  val D: DB_Workflow = Model.coerce(v, this).asInstanceOf[DB_Workflow]
  val id       = D.id
  val name     = D.name
  val contents = D.contents
  val created  = D.created
}