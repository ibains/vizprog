package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Credentials {

  // ADDERS

  def + (credentialType: String, key1: String, key2: String) : Credentials = {
    new Credentials(addVertex(addVQ(DB_VERTEX.credentials, List(
      (DB_PROPERTY.credentialType, credentialType), (DB_PROPERTY.key1, key1),
      (DB_PROPERTY.key2, key2), (DB_PROPERTY.created, Util.tt)
    ))))
//    new Credentials(Graph.get + DB_Credentials(None, credentialType, key1, key2, Util.tt))
  }

}

class Credentials(v: Vertex) extends SimpleEntity(v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_Credentials]
}