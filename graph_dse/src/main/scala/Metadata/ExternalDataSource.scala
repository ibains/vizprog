package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object addExternalDataSource {

  // ADDERS

  def + (location: String, retrievalJsonNode: String) : ExternalDataSource = {
    new ExternalDataSource(addVertex(addVQ(DB_VERTEX.externaldatasource, List(
      (DB_PROPERTY.location, location), (DB_PROPERTY.retrievalJsonNode, retrievalJsonNode),
      (DB_PROPERTY.created, Util.tt)
    ))))
//    new ExternalDataSource(Graph.get + DB_ExternalDataSource(None, location, retrievalJsonNode, Util.tt))
  }

}

class ExternalDataSource(v: Vertex) extends SimpleEntity(v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_ExternalDataSource]
}

