package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Person {

  // ADDERS

  def + (company: Company, name: String, email: String) : Person = {
    val v = addVertex(addVQ(
      DB_VERTEX.person, List(
        (DB_PROPERTY.name, name), (DB_PROPERTY.email, email), (DB_PROPERTY.created, Util.tt)
      )
    ))
    val p = new Person(v)
    // NOTE: java.lang.IllegalStateException: Edge additions not supported
    //    company.V.addEdge(DB_EDGE.Member, v) ==> won't work !!

//    val p = new Person(Graph.get + DB_Person(None, name, email, Util.tt))
    company --> p
    p
  }

  // GETTERS - ALL

  def getAll : List[Person] = {
    getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.person)
    ).map(x => new Person(x))
//    Graph.get.V.hasLabel[DB_Person].toList.map(x => new Person(x))
  }

  // GETTERS - FILTERED

  def get(companyName: String, name: String) : Person = {
    val company = Company.get(companyName)
    // TODO change to lookup by id
    val persons = getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.name, company.D.name) //company
        .outE(Some(DB_EDGE.Member)).inV()
          .findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.name, name)
    )
//    val persons = company.V.out(DB_EDGE.Member).
//                            hasLabel(DB_LABELS.person).
//                            has(DB_KEY.name, name).toList()
    if (persons.length < 1)
      return null

    new Person(persons.head)
  }
}

class Person(v: Vertex) extends SimpleEntity(v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_Person]

  // member accessors
  def id      : Long   = v.property(DB_PROPERTY.id).value()
  def name    : String = v.property(DB_PROPERTY.name).value()
  def email   : String = v.property(DB_PROPERTY.email).value()
  def created : Long   = v.property(DB_PROPERTY.created).value()

  def getWorkflows : List[(Int, String, SimpleEntity)] = {

    val indirectRoleNodes = getVertices(
      VQBuilder(emptyQ).findVByEQ((inE.name, Some(DB_EDGE.Member))).outV()
        .findVByLabelQ(DB_VERTEX.group)
          .outE(Some(DB_EDGE.Permission)).inV()
              .findVByLabelQ(DB_VERTEX.role)
    )

//    val indirectRoleNodes = V.
//                              in(DB_EDGE.Member).
//                              hasLabel(DB_LABELS.group).
//                              out(DB_EDGE.Permission).
//                              hasLabel(DB_LABELS.role).toList()


    val directRoleNodes = getVertices(
      VQBuilder(emptyQ).findVByEQ((outE.name, Some(DB_EDGE.Permission))).inV()
          .findVByLabelQ(DB_VERTEX.role)
    )

//    val directRoleNodes   = V.
//                              out(DB_EDGE.Permission).
//                              hasLabel(DB_LABELS.role).toList()

    val allRoleNodes =
      (directRoleNodes ::: indirectRoleNodes).
        map( x => SimpleEntity.get(x).asRole )

    allRoleNodes.flatMap(
      role => role.getItems(
        DB_VERTEX.workflow::Nil))
  }
}