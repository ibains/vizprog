package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Dataset {

  // ADDERS

  def + (name: String, schemaJson: String, statsJson: String, diskSizeGb: Long) : Dataset = {
    new Dataset(addVertex(addVQ(DB_VERTEX.dataset, List(
      (DB_PROPERTY.name, name), (DB_PROPERTY.schemaJson, schemaJson),
      (DB_PROPERTY.statsJson, statsJson), (DB_PROPERTY.diskSizeGb, diskSizeGb),
      (DB_PROPERTY.created, Util.tt)
    ))))
//    new Dataset(Graph.get + DB_Dataset(None, name, schemaJson, statsJson, diskSizeGb, Util.tt))
  }

}

class Dataset(v: Vertex) extends SimpleEntity(v) {
  val D: DB_Dataset = Model.coerce(v, this).asInstanceOf[DB_Dataset]
}
