package Metadata

import java.text.ParsePosition
import java.util

import Metadata.DB_PROPERTY._
import com.datastax.shaded.jackson.databind.util.ISO8601Utils
import org.apache.tinkerpop.gremlin.structure.Vertex

import scala.collection.JavaConversions._
import scala.collection.immutable.HashSet

sealed abstract class Model

case class DB_Company(id: Option[Long],
                      name: String,
                      created: Long) extends Model

case class DB_Group(id: Option[Long],
                    name: String,
                    description: String,
                    created: Long) extends Model

case class DB_Project(id: Option[Long],
                      name: String,
                      description: String,
                      created: Long) extends Model

case class DB_Person(id: Option[Long],
                     name: String,
                     email: String,
                     created: Long) extends Model

case class DB_Role(id: Option[Long],
//                   name: String,
                   permission: Int,
                   created: Long) extends Model

case class DB_Workflow(id: Option[Long],
                       name: String,
                       contents: String,
                       created: Long) extends Model

case class DB_Dataset(id: Option[Long],
                      name: String,
                      schemaJson: String,
                      statsJson: String,
                      diskSizeGb: Long,
                      created: Long) extends Model

case class DB_Execution(id: Option[Long],
                        status: String,
                        submitted: Long,
                        completed: Long) extends Model

case class DB_ExternalDataSource(id: Option[Long],
                                 location: String,
                                 retrievalJsonNode: String,
                                 created: Long) extends Model

case class DB_Credentials(id: Option[Long],
                          credentialType: String,
                          key1: String,
                          key2: String,
                          created: Long) extends Model

// TODO: Use reflection to simplify
object Model {
  def coerce(v: Vertex, simpleEntity: AnyRef): Model = {
    def V(pId: String): String = {
      // TODO: default to member id with 1 node cassandra this is consistent
      if (pId == "id") {
        v.id().asInstanceOf[util.LinkedHashMap[String, Object]].get("member_id").toString
      } else if (pId == "created") {
        ISO8601Utils.parse(v.property("created").value.toString, new ParsePosition(0)).getTime.toString
      } else {
        v.property(pId).value().toString
      }
    }

    simpleEntity match {
      case _: Person => DB_Person(Some(V(id).toLong), V(name), V(email), V(created).toLong)
      case _: Company => DB_Company(Some(V(id).toLong), V(name), V(created).toLong)
      case _: Workflow => DB_Workflow(Some(V(id).toLong), V(name), V(contents), V(created).toLong)
      case _: Group => DB_Group(Some(V(id).toLong), V(name), V(description), V(created).toLong)
      case _: Dataset => DB_Dataset(Some(V(id).toLong), V(name), V(schemaJson), V(statsJson), V(diskSizeGb).toLong, V(created).toLong)
      case _: ExternalDataSource => DB_ExternalDataSource(Some(V(id).toLong), V(name), V(email), V(created).toLong)
      case _: Role => DB_Role(Some(V(id).toLong), V(permission).toInt, V(created).toLong)
      case _: Project => DB_Project(Some(V(id).toLong), V(name), V(description), V(created).toLong)
      case _: Execution => DB_Execution(Some(V(id).toLong), V(status), V(submitted).toLong, V(completed).toLong)
      case _ => throw new RuntimeException("Cannot coerce to any known type")
    }
  }
}

//object U { def Q(s: String) : String = "\"" + s + "\"" }
object U {
  def Q(s: String) : String = "\"" + s + "\""
  def Q(a: Any) : Any = a match {
    case n: Number => a
    case s: String => Q(a.toString)
    case c: Char   => Q(a.toString)
    case _         => ""
  }
  def ?(s: Option[String]) : String = if (s.isDefined) Q(s.get) else ""
}


class dseKey(name : String, dataType : String, multiCardinality : Boolean, unique : Boolean, metaProperties : List[dseKey]) {
  override def toString: String = {
    val multi = if (multiCardinality) ".multiple()" else ".single()"
    val meta  = if (metaProperties == null)  { "" }
                else {
                  val props = metaProperties.map( x => U.Q(x.getName) ).mkString(",")
                  s".properties($props)"
                }
    s"schema.propertyKey(${U.Q(name)}).$dataType()$multi$meta.create()"
  }
  def getName : String = name
}

class dseVertex(name: String, properties: Map[String, String]) {
  override def toString: String = {
    val props = properties.keys.map( x => U.Q(x) ).mkString(",")
    s"schema.vertexLabel(${U.Q(name)}).properties($props).create()"
  }
}

class dseEdge(name: String, multiCardinality: Boolean, properties: Map[String, String], connections: Map[String, Set[String]]) {
  override def toString: String = {
    val multi = if (multiCardinality) ".multiple()" else ".single()"
    val props = if (properties == null) { "" }
                else {
                  val props = properties.keys.map( x => U.Q(x) ).mkString(",")
                  s".properties($props)"
                }
    val cons  = connections.keys.map(x =>
                                     connections(x).map(y =>
                                       s"connection(${U.Q(x)}, ${U.Q(y)})").mkString(".")).mkString(".")
    s"schema.edgeLabel(${U.Q(name)})$multi$props.$cons.create()"
  }
}

// Vertex Indexes
// Secondary
//schema.vertexLabel('author').index('byName').secondary().by('name').add()
// Materialized
//schema.vertexLabel('recipe').index('byRecipe').materialized().by('name').add()
// Search
// schema.vertexLabel('recipe').index('search').search().by('instructions').asText().add()
// schema.vertexLabel('recipe').index('search').search().by('instructions').asString().add()
// If more than one property key is search indexed
// schema.vertexLabel('recipe').index('search').search().by('instructions').asText().by('category').asString().add()

// Property index using meta-property 'livedIn':
//schema.vertexLabel('author').index('byLocation').property('country').by('livedIn').add()

// Edge Index
//schema.vertexLabel('reviewer').index('ratedByStars').outE('rated').by('stars').add()

sealed abstract class index
case class secondaryIndex(indexBy: String) extends index
case class materializedIndex(indexBy: String) extends index
case class searchIndex(indexBy: List[(String, String)]) extends index
case class metaPropertyIndex(property: String, metaProperty: String) extends index
case class edgeIndex(func: String, edgeLabel: String, property: String) extends index

sealed trait edgeFunc { def name: String }
case object outE extends edgeFunc { val name = "outE" }
case object inE extends edgeFunc { val name = "inE" }

sealed trait searchOpt { def name: String }
case object asText { val name = "asText" }
case object asString { val name = "asString" }

class dseIndex(name: String, vertexLabel: String, indexDef : index) {
  override def toString: String = {
    var query = new StringBuilder
    query ++= s"schema.vertexLabel(${U.Q(vertexLabel)}).index(${U.Q(name)})"
    indexDef match {
      case secondaryIndex(indexBy) => query ++= s".secondary().by(${U.Q(indexBy)}).add()"
      case materializedIndex(indexBy) => query ++= s".materialized().by(${U.Q(indexBy)}).add()"
      case searchIndex(indexBy) => query ++= ".search()" ; indexBy.foreach{ case (p, f) => query ++= s".by(${U.Q(p)}).$f().add()" }
      case metaPropertyIndex(prop, metaProp) => query ++= s".property(${U.Q(prop)}).by(${U.Q(metaProp)}).add()"
      case edgeIndex(func, label, prop) => query ++= s".$func(${U.Q(label)}).by(${U.Q(prop)}).add()"
      case _ => throw new RuntimeException("Unsupported index type")
    }

    query.toString
  }
}

object DB_Permissions {
  val admin = 1
  val read  = 2
  val write = 4
  def s(a: Int) : String =
    a match {
      case `admin` => "admin"
      case `read`  => "read"
      case `write` => "write"
      case _       => "unknown"
    }
}

object DB_EDGE {
  val Permission         = "permission"
  val Member             = "member"
  val User               = "user"
}

object DB_VERTEX {
  val company            = "company"
  val group              = "group"
  val project            = "project"
  val person             = "person"
  val role               = "role"
  val workflow           = "workflow"
  val dataset            = "dataset"
  val execution          = "execution"
  val externaldatasource = "externaldatasource"
  val credentials        = "credentials"
}

object DB_PROPERTY {
  val created            = "created"
  val completed          = "completed"
  val contents           = "contents"
  val credentialType     = "credentialType"
  val description        = "description"
  val diskSizeGb         = "diskSizeGb"
  val email              = "email"
  val id                 = "id"
  val key1               = "key1"
  val key2               = "key2"
  val location           = "location"
  val name               = "name"
  val permission         = "permission"
  val retrievalJsonNode  = "retrievalJsonNode"
  val schemaJson         = "schemaJson"
  val statsJson          = "statsJson"
  val status             = "status"
  val submitted          = "submitted"
}

object dseDataTypes {
  val Smallint           = "Smallint"
  val Int                = "Int"
  val Bigint             = "Bigint"
  val Varint             = "Varint"
  val Float              = "Float"
  val Double             = "Double"
  val Decimal            = "Decimal"
  val Boolean            = "Boolean"
  val Text               = "Text"
  val Uuid               = "Uuid"
  val Timestamp          = "Timestamp"
  val Duration           = "Duration"
  val Point              = "Point"
  val Linestring         = "Linestring"
  val Polygon            = "Polygon"
  val Inet               = "Inet"
  val Blob               = "Blob"
}

object DB_INDEXES {
  val personId           = "personId" // m
  val projectName        = "projectName" // secondary
  val personEmail        = "personEmail" // secondary
  val datasetSchema      = "datasetSchemaJson" // search
}

object dseKey {
  var keys = Map(
    created           -> new dseKey(created          , dseDataTypes.Timestamp, multiCardinality = false, unique = false, null),
    completed         -> new dseKey(completed        , dseDataTypes.Timestamp, multiCardinality = false, unique = false, null),
    contents          -> new dseKey(contents         , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    credentialType    -> new dseKey(credentialType   , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    description       -> new dseKey(description      , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    diskSizeGb        -> new dseKey(diskSizeGb       , dseDataTypes.Bigint   , multiCardinality = false, unique = false, null),
    email             -> new dseKey(email            , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    id                -> new dseKey(id               , dseDataTypes.Bigint   , multiCardinality = false, unique = false, null),
    key1              -> new dseKey(key1             , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    key2              -> new dseKey(key2             , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    location          -> new dseKey(location         , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    name              -> new dseKey(name             , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    permission        -> new dseKey(permission       , dseDataTypes.Int      , multiCardinality = false, unique = false, null),
    retrievalJsonNode -> new dseKey(retrievalJsonNode, dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    schemaJson        -> new dseKey(schemaJson       , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    statsJson         -> new dseKey(statsJson        , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    status            -> new dseKey(status           , dseDataTypes.Text     , multiCardinality = false, unique = false, null),
    submitted         -> new dseKey(submitted        , dseDataTypes.Timestamp, multiCardinality = false, unique = false, null)
  )
  def schemaString : String = keys.values.mkString("\n")
}

object dseVertex {

  var vertices = Map(

  DB_VERTEX.company                 -> new dseVertex(DB_VERTEX.company,
    Map(id              -> id,
      name              -> name,
      created           -> created
    )
  ),

  DB_VERTEX.group                   -> new dseVertex(DB_VERTEX.group,
    Map(id              -> id,
      name              -> name,
      description       -> description,
      created           -> created
    )
  ),

  DB_VERTEX.project                 -> new dseVertex(DB_VERTEX.project,
    Map(id              -> id,
      name              -> name,
      description       -> description,
      created           -> created
    )
  ),
  DB_VERTEX.person                  -> new dseVertex(DB_VERTEX.person,
    Map(id              -> id,
      name              -> name,
      email             -> email,
      created           -> created
    )
  ),

  DB_VERTEX.role                    -> new dseVertex(DB_VERTEX.role,
    Map(id              -> id,
      permission        -> permission,
      created           -> created
    )
  ),

  DB_VERTEX.workflow                -> new dseVertex(DB_VERTEX.workflow,
    Map(id              -> id,
      name              -> name,
      contents          -> contents,
      created           -> created
    )
  ),

  DB_VERTEX.dataset                 -> new dseVertex(DB_VERTEX.dataset,
    Map(id              -> id,
      name              -> name,
      schemaJson        -> schemaJson,
      statsJson         -> statsJson,
      diskSizeGb        -> diskSizeGb,
      created           -> created
    )
  ),

  DB_VERTEX.execution               -> new dseVertex(DB_VERTEX.execution,
    Map(id              -> id,
      status            -> status,
      submitted         -> submitted,
      completed         -> completed
    )
  ),

  DB_VERTEX.externaldatasource      -> new dseVertex(DB_VERTEX.externaldatasource,
    Map(id              -> id,
      location          -> location,
      retrievalJsonNode -> retrievalJsonNode,
      created           -> created
    )
  ),

  DB_VERTEX.credentials             -> new dseVertex(DB_VERTEX.credentials,
    Map(id              -> id,
      credentialType    -> credentialType,
      key1              -> key1,
      key2              -> key2,
      created           -> created
    )
  )

  ) // map

  def schemaString : String = vertices.values.mkString("\n")
}

object dseEdge {

  var edges = Map (

    DB_EDGE.Member     -> new dseEdge(DB_EDGE.Member, false, properties = null,
                          Map(
                            DB_VERTEX.company -> HashSet(DB_VERTEX.group, DB_VERTEX.project, DB_VERTEX.person),
                            DB_VERTEX.group   -> HashSet(DB_VERTEX.person),
                            DB_VERTEX.project -> HashSet(DB_VERTEX.workflow, DB_VERTEX.dataset,
                                                         DB_VERTEX.externaldatasource, DB_VERTEX.execution)
                          )),

    DB_EDGE.Permission -> new dseEdge(DB_EDGE.Permission, false, properties = null,
                          Map(
                            DB_VERTEX.person -> HashSet(DB_VERTEX.role),
                            DB_VERTEX.group  -> HashSet(DB_VERTEX.role),
                            DB_VERTEX.role   -> HashSet(DB_VERTEX.project, DB_VERTEX.workflow,
                                                        DB_VERTEX.dataset,DB_VERTEX.externaldatasource,
                                                        DB_VERTEX.execution)
                          ))
  )

  def schemaString : String = edges.values.mkString("\n")

}

// TODO: Search index creation throws error : Cannot create search index with workload: Cassandra
object dseIndex {
  val indexes = Map(
    DB_INDEXES.projectName          -> new dseIndex(DB_INDEXES.projectName, DB_VERTEX.person, secondaryIndex("name")),
    DB_INDEXES.personId             -> new dseIndex(DB_INDEXES.personId, DB_VERTEX.person, materializedIndex("id")),
    DB_INDEXES.personEmail          -> new dseIndex(DB_INDEXES.personEmail, DB_VERTEX.person, secondaryIndex("email"))
//    DB_INDEXES.datasetSchema        -> new dseIndex("search", DB_VERTEX.dataset, searchIndex(List(("schemaJson", asText.name))))
  )

  def schemaString : String = indexes.values.mkString("\n")
}