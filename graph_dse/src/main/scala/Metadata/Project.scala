package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Project {

  // ADDERS

  def + (name: String, description: String) : Project = {
    new Project(
      addVertex(addVQ(DB_VERTEX.project, List(
        (DB_PROPERTY.name, name), (DB_PROPERTY.description, description), (DB_PROPERTY.created, Util.tt)
      )))
    )
//    new Project(Graph.get + DB_Project(None, name, description, Util.tt))
  }

  def get(companyName: String, name: String) : Project = {
    val company = Company.get(companyName)

    new Project(
      getVertex(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.id, company.D.id.get) //company
          .findVByEQ((outE.name, Some(DB_EDGE.Member))).inV()
            .findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.name, name)
      )
    )

//    val v = company.V.
//                      out(DB_EDGE.Member).
//                      hasLabel(DB_LABELS.project).
//                      has(DB_KEY.name, name).toList()
//    if (v == null || v.length < 1)
//      return null
//    new Project(v.head)
  }
}


class Project(v: Vertex) extends GroupEntity(v)  {
  val D: DB_Project = Model.coerce(v, this).asInstanceOf[DB_Project]

  def --> (workflow: Workflow)             : Unit = { addMember(V, workflow.V   ) }
  def --> (dataset: Dataset)               : Unit = { addMember(V, dataset.V    ) }
  def --> (datasource: ExternalDataSource) : Unit = { addMember(V, datasource.V ) }

}

