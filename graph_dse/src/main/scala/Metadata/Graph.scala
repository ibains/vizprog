package Metadata

/*
 * This file provides basic CRUD operations on the graph
 *
 * There is no business logic in this class
 */

import gremlin.scala._
import gremlin.scala.ScalaGraph


object  Graph {
  var gg: ScalaGraph = null
  def set(g: ScalaGraph): Unit = gg = g
  def get = {
    if (gg== null)
      throw new Exception("Dude: the graph database wasn't set, stop asking for it")
    gg
  }
}

