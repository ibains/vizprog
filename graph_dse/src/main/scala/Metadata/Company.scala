package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Company {

  // ADDERS

  /*
   * Invariants:
   * 1. No user can exist without a company
   * 2. No company can exist without an administrator
   *
   * Therefore the company and it's admin shall be created together
   */

  def + (companyName: String, adminName: String, adminEmail: String) : Company = {
    val company = new Company(addVertex(addVQ(DB_VERTEX.company, List(
      (DB_PROPERTY.name, companyName), (DB_PROPERTY.created, Util.tt)))))
//    val company = new Company(Graph.get + DB_Company(None, companyName, Util.tt))

    val adminUser = Person + (company, adminName, adminEmail)

    company <== (adminUser, Permissions.admin)
    company
  }

  // GETTERS - ALL

  def getAll : List[Company] =
    getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.company)
    ).map(x => new Company(x))
//    Graph.get.V.hasLabel[DB_Company].toList.map(x => new Company(x))

  // GETTERS - FILTERED

  def get(name: String) : Company =
    new Company(getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.name, name)
    ))
//    new Company(Graph.get.V.hasLabel(DB_LABELS.company).has(DB_KEY.name, name).head())
}

class Company(v: Vertex) extends GroupEntity(v) {
  val D: DB_Company = Model.coerce(v, this).asInstanceOf[DB_Company]

  def --> (project: Project): Unit = { addMember(V, project.V) }
  def --> (person: Person)  : Unit = { addMember(V, person.V ) }
  def --> (group: Group)    : Unit = { addMember(V, group.V  ) }

  def users : List[Person] =
    getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.id, D.id.get) //company
        .outE(Some(DB_EDGE.Member)).inV().findVByLabelQ(DB_VERTEX.person) // person
    ).map(x => new Person(x))
//    v.out(DB_EDGE.Member).hasLabel(DB_LABELS.person).toList().map( x => new Person(x))

  def projects : List[Project] =
    getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.id, D.id.get) //company
        .outE(Some(DB_EDGE.Member)).inV().findVByLabelQ(DB_VERTEX.project) // project
    ).map(x => new Project(x))
//    v.out(DB_EDGE.Member).hasLabel(DB_LABELS.project).toList().map( x => new Project(x))

  def groups : List[Group] =
    getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.id, D.id.get) //company
        .outE(Some(DB_EDGE.Member)).inV().findVByLabelQ(DB_VERTEX.group) // group
    ).map(x => new Group(x))
//    v.out(DB_EDGE.Member).hasLabel(DB_LABELS.group).toList().map( x => new Group(x))

}
