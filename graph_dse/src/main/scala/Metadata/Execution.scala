package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Execution {

  // ADDERS

  def addExecution(status: String) : Execution = {
    new Execution(addVertex(addVQ(DB_VERTEX.execution, List(
      (DB_PROPERTY.status, status)
    ))))
//    new Execution(Graph.get + DB_Execution(None, status, Util.tt, 0L))
  }
}


class Execution(v: Vertex) extends SimpleEntity(v)  {
  val D = Model.coerce(v, this).asInstanceOf[DB_Execution]

}