package Metadata

import org.apache.tinkerpop.gremlin.structure.{Direction, Vertex}
import Dse.QueryBuilder._
import Dse.DSEDriver._
import scala.collection.JavaConverters._

object Role {
  // ADDERS

  def + (permission: Int) : Role = {
    new Role(addVertex(addVQ(DB_VERTEX.role, List(
      (DB_PROPERTY.permission, permission), (DB_PROPERTY.created, Util.tt)
    ))))
//    new Role(Graph.get + DB_Role(None, permission, Util.tt))
  }

  def + (entity: Vertex, user: Vertex, permission: Int): Unit = {
    var entityPermissionVertex: Role = null

    val permissionVertices = entity.vertices(Direction.IN, DB_EDGE.Permission).asScala
    for (v <- permissionVertices)
      if (v.property(DB_PROPERTY.permission).value() == permission)
        entityPermissionVertex = new Role(v)

    // if it doesn't exist, create such a node
    if (entityPermissionVertex == null) {
      entityPermissionVertex = Role + permission

      // role has permissions over this entity
//      entityPermissionVertex.V --- DB_EDGE.Permission --> entity
      addEdge(entityPermissionVertex.V, DB_EDGE.Permission, entity)
    } else {
      // check the user is already there in the permission vertex

      val users = entityPermissionVertex.V.vertices(Direction.IN, DB_EDGE.User).asScala
      for (u <- users)
        if (u.property(DB_PROPERTY.name) == user.property(DB_PROPERTY.name))
          return
    }

    // user has permission to this role
    addEdge(user, DB_EDGE.Permission, entityPermissionVertex.V)
//    user --- DB_EDGE.Permission --> entityPermissionVertex.V
  }

}

class Role(v: Vertex) extends SimpleEntity(v) {
  val D: DB_Role = Model.coerce(v, this).asInstanceOf[DB_Role]
  val permission = D.permission

  def getItems : List[(Int, String, SimpleEntity)] = {

    var ret : List[(Int, String, SimpleEntity)] = Nil

    val vertices = getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.role, DB_PROPERTY.id, D.id.get)
          .findVByEQ((outE.name, Some(DB_EDGE.Permission))).inV()
    )

    for (p <- vertices.map( x => SimpleEntity.get(x) )) {
      if (p.isGroupEntity) {
        ret = {
          for {
            (label, simpleEntity) <- p.asInstanceOf[GroupEntity].members()
          } yield (permission, label, simpleEntity) } ::: ret
      } else {
        ret = (permission, p.vLabel, p) :: ret
      }
    }
    ret
  }

  def getItems(labels: List[String]) : List[(Int, String, SimpleEntity)] = {
    for {
      (permission, label, simpleEntity) <- this.getItems
      if labels.contains(label)
    } yield (permission, label, simpleEntity)
  }
}
