package Metadata

import org.apache.tinkerpop.gremlin.structure.Vertex
import Dse.QueryBuilder._
import Dse.DSEDriver._

object Group {

  // ADDERS

  def + (name: String, description: String) : Group = {
    new Group(addVertex(addVQ(DB_VERTEX.group, List(
      (DB_PROPERTY.name, name), (DB_PROPERTY.description, description),
      (DB_PROPERTY.created, Util.tt)
    ))))
//    new Group(Graph.get + DB_Group(None, name, description, Util.tt))
  }

  // GETTERS - ALL

  def getAll : List[Group] = {
    getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.group)).map(x => new Group(x))
//    Graph.get.V.hasLabel(DB_LABELS.group).toList.map(x => new Group(x))
  }

  // GETTERS - FILTERED

  def get(name: String) : Group = {
    new Group(getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.group, DB_PROPERTY.name, name)))
//    new Group(Graph.get.V.hasLabel(DB_LABELS.group).has(DB_KEY.name, name).head())
  }
}

class Group(v: Vertex) extends GroupEntity(v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_Group]

  def --> (person: Person)  : Unit = { addMember(V, person.V ) }
}