name := "metadata"
version := "1.0.0-SNAPSHOT"
scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.apache.tinkerpop" % "neo4j-gremlin" % "3.2.1" exclude("com.github.jeremyh", "jBCrypt"), // travis can't find jBCrypt...
  "org.neo4j" % "neo4j-tinkerpop-api-impl" % "0.4-3.0.3",
  "org.slf4j" % "slf4j-simple" % "1.7.12", // "org.neo4j.driver" % "neo4j-java-driver" % "1.0.4"
  "org.scalatest" % "scalatest" % "2.2.5" % "test",
  "org.apache.tinkerpop" % "gremlin-driver" % "3.2.0-incubating",
  "com.datastax.cassandra" % "dse-driver" % "1.1.0",
  "joda-time" % "joda-time" % "2.9.4"
)
resolvers += Resolver.mavenLocal
