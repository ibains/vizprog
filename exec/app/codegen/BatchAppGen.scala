package codegen

import conf.AppConfiguration
import model.AppStructure

class BatchAppGen {

  // TODO: Spark Job Server API verison is used as 0.6.1 as of now, update it to 0.6.2
  private val SPARK_VERSION      = "1.6.0"
  private val SCALA_VERSION      = "2.10.4"
  private val SCALA_JAR_VERSION  = "2.10"
  private val SPARK_JOB_SERVER_VERSION = "0.6.1"

  protected def generateSbtBuildFile(id: String): String = {
    s"""name := "$id"

version := "1.0"

scalaVersion := "$SCALA_VERSION"

resolvers += "Job Server Bintray" at "https://dl.bintray.com/spark-jobserver/maven"

libraryDependencies += "org.apache.spark" %% "spark-core" % "$SPARK_VERSION"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "$SPARK_VERSION"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "$SPARK_VERSION"
libraryDependencies += "com.amazonaws" % "aws-java-sdk" % "1.11.5"
libraryDependencies += "commons-lang" % "commons-lang" % "2.6"
      """
  }

  def generateApp(tmpRootDir: String, sparkCode: String, workflowId: String): AppStructure = {

    val rootDir    = s"$tmpRootDir/$workflowId"
    val interimDir = s"$tmpRootDir/tmp$workflowId"

    val app = new AppStructure

    app.root    = rootDir
    app.interim = interimDir
    app.id      = workflowId

    app.appendDirectory(rootDir                         )
    app.appendDirectory(rootDir + "/" + "src"           )
    app.appendDirectory(rootDir + "/" + "src/main"      )
    app.appendDirectory(rootDir + "/" + "src/main/scala")
    app.appendDirectory(interimDir                      )

    val buildFile  = rootDir + "/build.sbt"
    val codeFile   = rootDir + s"/src/main/scala/${workflowId}App.scala"
    val target     = rootDir + s"/target/scala-$SCALA_JAR_VERSION/${workflowId.toLowerCase}_$SCALA_JAR_VERSION-1.0.jar"

    app.target = target
    app.addFile(buildFile, generateSbtBuildFile(workflowId))
    app.addFile(codeFile, sparkCode)

    app.compileCmd = Seq(AppConfiguration.activatorLocation, "compile", "package")
    app.batchCmd   = Seq(AppConfiguration.sparkSubmitLocation,
      "--class", app.id,
      "--master", "local[8]",
      app.target
    )
    app
  }
}
