package engine

import java.io.{ByteArrayOutputStream, FileInputStream}
import java.net.URLDecoder

import akka.actor.{Actor, ActorLogging, Props}
import conf.AppConfiguration._
import engine.CompilationEngine._
import model._
import model.Exceptions._
import org.apache.commons.lang.{StringEscapeUtils, StringUtils}

import scala.io.Source
import scala.util.{Failure, Success, Try}

object CompilationEngine {
  def props = Props[CompilationEngine]

  def main(args: Array[String]) {
    val file = new FileInputStream("test\\resources\\log.txt")
    val str = Source.fromInputStream(file).getLines().mkString("\n")
    println(stripANSIColorCodes(str))
  }

  private def stripANSIColorCodes(text: String) = {
    text.replaceAll("\u001B\\[[;\\d]*[ -/]*[@-~]", "")
  }

  case class CompileJob(sessionId: String, jobId: String, sparkCode: String)

  case class CompilationSucceeded(sessionId: String, jobId: String, jarLocation: String)

  case class CompilationFailed(jobId: String, error: Throwable)
}

class CompilationEngine extends Actor with ActorLogging {

  import context.dispatcher

  override def receive: Receive = {
    case CompileJob(sessionId, jobId, sparkCode) =>
      val master = sender
      JobModel.updateJobStateWithResult(jobId, JobState.Compiling, None).map { res =>
        log.info(s"Initiating the compile process for job $jobId")
        compileJob(sessionId, jobId, sparkCode) match {
          case Success(jarLocation) =>
            log.info(s"Successfully compiled the job $jobId")
            master ! CompilationSucceeded(
              sessionId = sessionId,
              jobId = jobId,
              jarLocation = jarLocation
            )
          case Failure(ex) =>
            master ! CompilationFailed(jobId = jobId, error = ex)
        }
      }
  }

  /**
    * Compiles the spark code with the job server profile and dependency API.
    *
    * @param jobId
    * @param sparkCode
    */
  private def compileJob(sessionId: String, jobId: String, sparkCode: String): Try[String] = {
    val fs = new LocalFileSystem
    val sparkCodeDecoded = URLDecoder.decode(sparkCode, "UTF-8")
    val app = generateApp(fs.tempDir, jobId, sessionId, sparkCodeDecoded)
    Try {
      writeAndCompileApp(app)
      app.target
    }
  }

  private def writeAndCompileApp(app: AppStructure) {
    val fs = new LocalFileSystem
    /*Creating required directories*/
    app.dirs.map {
      fs.createDirectory(_)
    }
    /*Creating required files*/
    app.files.map { f =>
      fs.writeFile(f._1, f._2)
    }

    /*Removing targets if available*/
    if (fs.hasFile(app.target))
      fs.deleteFile(app.target)

    /*Compiling the source*/
    val compileResult = fs.execute(app.root, app.compileCmd)
    if (compileResult._1 > 0 || !fs.hasFile(app.target)) {
      throw CompilationError("Failed to compile the application.",
        s"STDOUT: ${stripANSIColorCodes(compileResult._2)}\n\n. STDERROR: ${stripANSIColorCodes(compileResult._3)}"
      )
    }
  }

  private def stripANSIColorCodes(text: String) = {
    text.replaceAll("\u001B\\[[;\\d]*[ -/]*[@-~]", "").replaceAll("\\n", "\n")
  }

  private def generateApp(tmpDir: String, jobId: String, sessionId: String, sparkCode: String): AppStructure = {

    val rootDir = s"$tempDirectory/$sessionId/$jobId"
    val interimDir = s"$tempDirectory/$sessionId/$jobId"

    val app = new AppStructure

    app.root = rootDir
    app.interim = interimDir
    app.id = jobId

    app.appendDirectory(rootDir)
    app.appendDirectory(rootDir + "/" + "src")
    app.appendDirectory(rootDir + "/" + "src/main")
    app.appendDirectory(rootDir + "/" + "src/main/scala")
    app.appendDirectory(rootDir + "/" + "project")
    app.appendDirectory(interimDir)

    val buildFile = rootDir + "/build.sbt"
    val pluginsFile = rootDir + "/project/plugins.sbt"
    val codeFile = rootDir + s"/src/main/scala/App.scala"
    val target = rootDir + s"/target/scala-$versionScalaJar/${jobId.toLowerCase}_$versionScalaJar-1.0.jar"

    app.target = target
    app.addFile(buildFile, generateSbtBuild(jobId))
    app.addFile(pluginsFile, generatePluginsTemplate)
    app.addFile(codeFile, sparkCode)

    app.compileCmd = Seq(activatorLocation, "clean", "compile", "package")
    app
  }


  private def generatePluginsTemplate: String = {
    s"""addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "$versionAssemblyPlugin")
    """
  }

  private def generateSbtBuild(id: String): String = {
    s"""name := "$id"
        version := "1.0"
        scalaVersion := "$versionScala"
        assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false, includeDependency = true)
        resolvers += "Job Server Bintray" at "https://dl.bintray.com/spark-jobserver/maven"
        libraryDependencies += "org.apache.spark" %% "spark-core" % "$versionSpark" % "provided"
        libraryDependencies += "org.apache.spark" %% "spark-sql" % "$versionSpark" % "provided"
        libraryDependencies += "org.apache.spark" %% "spark-mllib" % "$versionSpark" % "provided"
        libraryDependencies += "spark.jobserver" %% "job-server-api" % "$versionSparkJobServer" % "provided"
        libraryDependencies += "spark.jobserver" %% "job-server-extras" % "$versionSparkJobServer" % "provided"
        libraryDependencies += "com.databricks" %% "spark-csv" % "$versionCsvReader" % "provided"
        libraryDependencies += "com.amazonaws" % "aws-java-sdk-s3" % "$versionAwsSdk" % "provided"
    """
  }
}