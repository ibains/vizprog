package engine

import java.io.{BufferedInputStream, FileInputStream}
import javax.security.auth.login.AppConfigurationEntry

import akka.actor.{Actor, ActorLogging, Props}
import conf.AppConfiguration._
import engine.ExecutionEngine.Events
import engine.JobServerBridge._
import model.Exceptions.JobServerError
import model.RequestBeans.SessionReq
import model.{JobModel, JobState, ModelUtils}
import play.api.Logger
import play.api.libs.json._
import spray.client.pipelining
import spray.client.pipelining._
import spray.http._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import conf.AppConfiguration._

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object JobServerBridge {
  implicit val intpResultReads: Reads[InterpreterResult] = (
    (JsPath \ "application/json").readNullable[String] and
      (JsPath \ "application/vnd.table.v1+json").readNullable[String] and
      (JsPath \ "image/png").readNullable[String] and
      (JsPath \ "text/plain").readNullable[String]
    ) (InterpreterResult.apply _)

  def props = Props[JobServerBridge]

  case class UploadJar(sessionId: String, jobId: String, jarLocation: String)

  case class JarUploadFailed(jobId: String, error: Throwable)

  case class JarUploadCompleted(sessionId: String, jobId: String)

  case class TriggerJob(sessionId: String, jobId: String)

  case class JobTriggerFailed(jobId: String, error: Throwable)

  case class JobTriggered(jobId: String, internalJobId: String)

  case class TrackJobProgress(jobId: String, internalJobId: String)

  case class JobStatus(jobId: String,
                       internalJobId: String,
                       status: Option[String],
                       duration: Option[String],
                       result: Option[String])

  case class ExecuteInteractive(sessionId: String, sparkCode: String, events: Events)

  case class ExecuteInteractiveResult(mime: String, contents: String)

  case class ExecuteInteractiveFailed(msg: String, ex: Throwable)

  case class InterpreterResult(`application/json`: Option[String],
                               `application/vnd.table.v1+json`: Option[String],
                               `image/png`: Option[String],
                               `text/plain`: Option[String]) {
    def getDefined = {
      if (`application/json`.isDefined)
        ("application/json", `application/json`.get)
      else if (`application/vnd.table.v1+json`.isDefined)
        ("application/vnd.table.v1+json", `application/vnd.table.v1+json`.get)
      else if (`image/png`.isDefined)
        ("image/png", `image/png`.get)
      else
        ("text/plain", `text/plain`.get)
    }
  }
}

class JobServerBridge extends Actor with ActorLogging {

  /*Execution Context for Spray Client Futures*/

  import context.dispatcher

  override def receive: Receive = {
    case UploadJar(sessionId: String, jobId: String, jarLocation: String) =>
      val master = sender()
      uploadJob(sessionId, jobId, jarLocation).map { _ =>
        log.info(s"Successfully uploaded the job $jobId to the job server.")
        master ! JarUploadCompleted(sessionId = sessionId, jobId = jobId)
      }.recover {
        case ex =>
          master ! JarUploadFailed(jobId, ex)
      }
    case TriggerJob(sessionId: String, jobId: String) =>
      val master = sender()
      executeJob(sessionId, jobId).map { result =>
        result._2 match {
          case "STARTED" =>
            JobModel.updateJobState(jobId, result._1, JobState.Triggered)
            log.info(s"Successfully triggered the job $jobId.")
            master ! JobTriggered(jobId, result._1)
          case _ =>
            throw JobServerError(s"Invalid status returned ${result._2}, Reason: ${result._1}")
        }
      }.recover {
        case ex =>
          master ! JobTriggerFailed(jobId, ex)
      }
    case TrackJobProgress(jobId: String, internalJobId: String) =>
      val master = sender()
      trackJobProgress(internalJobId).map { result =>
        log.info("Successfully retrieved the job status information.")
        master ! JobStatus(jobId = jobId,
          internalJobId = internalJobId,
          status = Some(result._1),
          duration = Some(result._2),
          result = Some(result._3))
      }.recover {
        case ex =>
          log.error("Failed to retrieve the job status information, Exception {}", ex)
          val error = Json.stringify(ModelUtils.toJson(ex))
          master ! JobStatus(jobId = jobId, internalJobId = internalJobId, None, None, Some(error))
      }
    case ExecuteInteractive(sessionId: String, sparkCode: String, events: Events) =>
      log.info("Executing the spark code to the interactive shell.")
      val master = sender()
      executeInteractive(sessionId = sessionId, sparkCode = sparkCode, events = events).map {
        result =>
          log.info("Successfully executed the code using an interactive shell.")
          val json = Json.parse(result)
          (json \ "status").get.asInstanceOf[JsString].value match {
            case "OK" =>
              val mime = (json \ "type").get.asInstanceOf[JsString].value
              val contents = (json \ "result").get.asInstanceOf[JsString].value
              master ! ExecuteInteractiveResult(mime, contents)
            case _ =>
              master ! ExecuteInteractiveResult("Error", "Invalid response found.")
          }
      }.recover {
        case ex =>
          log.error("Failed to execute code using an interactive shell, Exception {}", ex)
          master ! ExecuteInteractiveFailed(ex.getMessage, ex)
      }
    case msg =>
      log.error(s"Unknown message received, $msg")
  }

  private def uploadJob(sessionId: String, jobId: String, jarLocation: String) = {

    log.info(s"Uploading the jar $jarLocation of job $jobId to the job server")
    val url = s"http://$sparkJobServerHost/jars/$jobId"
    val response = Try {
      JobModel.updateJobStateWithResult(jobId, JobState.Submitting, None)
      val bis = new BufferedInputStream(new FileInputStream(jarLocation))
      val bArray =
        Stream.continually(bis.read).takeWhile(-1 !=).map(_.toByte).toArray
      val pipeline = sendReceive
      pipeline(pipelining.Post(
        url, HttpEntity(MediaTypes.`application/java-archive`, bArray)))
    }

    val result = response match {
      case Success(fs) =>
        fs.map { res =>
          res.status match {
            case StatusCodes.OK =>
              Future.successful(jobId)
            case _ =>
              Future.failed(JobServerError(
                s"Failed to send the jar to the spark job server, msg: ${res.entity.asString}"))
          }
        }
      case Failure(ex) =>
        Future.failed(JobServerError(
          s"Failed to send the jar to the spark job server.", ex))
    }

    result.flatMap(identity)
  }

  private def executeJob(sessionId: String, jobId: String) = {

    log.debug(s"Executing the job $jobId to the job server")
    val url =
      s"http://$sparkJobServerHost/jobs?appName=$jobId&classPath=sparkwf.job.App&context=$sessionId&sync=false"
    val pipeline = sendReceive
    val result = pipeline(pipelining.Post(url, HttpEntity("")))

    result.map { response =>
      val json = Json.parse(response.entity.asString)
      val status = (json \ "status").as[String]
      if(status == "STARTED") {
        val internalJobId = (json \ "result" \ "jobId").as[String]
        (internalJobId, status)
      } else {
        val result = (json \ "result").as[String]
        (result, status)
      }
    }
  }

  def executeInteractive(
                          sessionId: String, sparkCode: String, events: Events): Future[String] = {

    import scala.concurrent.duration._

    /** Interactive job execution is handled in the sync mode only. Async mode would allow getting an interim job id and
      * and tracking its progress.
      */
    Logger.debug(
      s"Executing the code to the job server in the interactive mode.")
    // TODO: Provide repl timeout from a configuration lookup or a user defined one.
    val url = s"http://$sparkJobServerHost/repl?context=${sessionId}&timeout=300"
    implicit val sendReceiveTimeout = akka.util.Timeout(300 seconds)
    val pipeline = sendReceive
    val future = pipeline(pipelining.Post(url, HttpEntity(sparkCode)))
    val result = future.map { res =>
      res.status match {
        case StatusCodes.OK =>
          Future.successful(res.entity.asString)
        case _ =>
          val json = Json.parse(res.entity.asString)
          val status = (json \ "status").get.asInstanceOf[JsString].value
          val eName = (json \ "ErrorName").get.asInstanceOf[JsString].value
          val eValue = (json \ "ErrorValue").get.asInstanceOf[JsString].value
          Future.failed(JobServerError(s"Status: $status, ErrorName:$eName, ErrorValue: $eValue"))
      }
    }

    result.flatMap(identity)
  }

  private def trackJobProgress(jobId: String) = {

    log.debug(s"Tracking the progress for job $jobId")
    val url = s"http://$sparkJobServerHost/jobs/$jobId"
    val pipeline = sendReceive
    val result: Future[HttpResponse] = pipeline(pipelining.Get(url))

    result.map { response =>
      val json = Json.parse(response.entity.asString)
      val status = (json \ "status").as[String]
      val duration = (json \ "duration").as[String]
      val result = Json.stringify((json \ "result").getOrElse(JsString("")))
      (status, duration, result)
    }
  }
}
