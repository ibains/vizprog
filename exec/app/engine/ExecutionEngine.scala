package engine

import akka.actor._
import akka.routing.{ActorRefRoutee, RoundRobinPool, RoundRobinRoutingLogic, Router}
import com.google.inject.name.Named
import com.google.inject.{AbstractModule, Inject}
import conf.AppConfiguration._
import engine.CompilationEngine.{CompilationFailed, CompilationSucceeded, CompileJob}
import engine.ExecutionEngine.{Events, SubmitBatchJob, SubmitInteractiveJob}
import engine.JobServerBridge._
import model.Exceptions.JobServerError
import model._
import play.api.Logger
import play.api.libs.concurrent.{AkkaGuiceSupport, InjectedActorSupport}
import play.api.libs.json.Json
import spray.client.pipelining
import spray.client.pipelining._
import spray.http.{HttpEntity, StatusCode}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object ExecutionEngine {

  trait Events

  case object Sync extends Events

  case object Async extends Events

  def props = Props[ExecutionEngine]

  case class SubmitBatchJob(sessionId: String, jobId: String, sparkCode: String)

  case class SubmitInteractiveJob(sessionId: String, sparkCode: String, events: Events = Sync)

}

class ExecutionEngine @Inject()(@Named("JobServerBridgeActor") jobServerBridge: ActorRef,
                                @Named("CompilationEngineActor") compilationEngine: ActorRef) extends Actor with ActorLogging {

  //  Actor System for the Scheduler
  val schedulerSystem = ActorSystem("SchedulerActorSystem")

  // Master Actor Reference.
  var master: ActorRef = _

  import schedulerSystem.dispatcher

  override def receive: Receive = {

    case SubmitBatchJob(sessionId, jobId, sparkCode) =>
      Logger.info(s"Initiating the workflow for the new job $jobId")
      compilationEngine ! CompileJob(sessionId, jobId, sparkCode)
    case SubmitInteractiveJob(sessionId, sparkCode, events) =>
      log.info("initiating repl for interactive execution.")
      master = sender()
      jobServerBridge ! ExecuteInteractive(sessionId, sparkCode, events)
    case executeInteractiveResult: ExecuteInteractiveResult =>
      master ! executeInteractiveResult
    case executeInteractiveFailed: ExecuteInteractiveFailed =>
      master ! executeInteractiveFailed
    case CompilationSucceeded(sessionId, jobId, jarLocation) =>
      Logger.info(s"Initiating the job $jobId to be uploaded.")
      jobServerBridge ! UploadJar(sessionId, jobId, jarLocation)
    case CompilationFailed(jobId, ex) =>
      Logger.error(s"Failed to compile the application with the job id ${jobId}", ex)
      handleFailures(jobId, ex)
    case JarUploadCompleted(sessionId, jobId) =>
      log.info(s"Initiating job execution for the new job $jobId")
      jobServerBridge ! TriggerJob(sessionId, jobId)
    case JarUploadFailed(jobId, ex) =>
      Logger.error(s"Failed to upload the jar for the application with the job id ${jobId}", ex)
      handleFailures(jobId, ex)
    case JobTriggerFailed(jobId, ex) =>
      Logger.error(s"Failed to trigger the application with the job id ${jobId}", ex)
      handleFailures(jobId, ex)
    case JobTriggered(jobId, internalJobId) =>
      log.info(s"Initiating tracking job progress for job $jobId")
      schedulerSystem.scheduler.scheduleOnce(jobStatusPollInterval seconds, jobServerBridge, TrackJobProgress(jobId, internalJobId))
    case JobStatus(jobId, internalJobId, status, duration, result) =>
      handleJobStateChange(jobId, internalJobId, status, duration, result)
    case msg =>
      log.info(s"Received an unknown message, $msg")
  }

  private def handleFailures(jobId: String, ex: Throwable): Unit = {
    //Do Nothing else as of now.
    JobModel.updateJobStateWithException(jobId, JobState.Failed, Some(ex))
  }

  private def handleJobStateChange(jobId: String, internalJobId: String, status: Option[String],
                                   duration: Option[String], result: Option[String]): Unit = {
    status.fold[Unit] {
      log.error(s"Failed to retrieve the status for the job $jobId.")
      schedulerSystem.scheduler.scheduleOnce(jobStatusPollInterval second, jobServerBridge, TrackJobProgress(jobId, internalJobId))
    } { status: String =>
      log.info(s"Handling the new job state $status found, Result: ${result}")
      status match {
        case SparkJobServerStatus.FINISHED.name =>
          JobModel.updateJobStateWithResult(jobId, model.JobState.Completed, result)
        case SparkJobServerStatus.ERROR.name =>
          JobModel.updateJobStateWithResult(jobId, model.JobState.Failed, result)
        case SparkJobServerStatus.RUNNING.name | SparkJobServerStatus.STARTED.name =>
          //TODO: Update duration here.
          JobModel.updateJobStateWithResult(jobId, model.JobState.Running, result).map { _ =>
            schedulerSystem.scheduler.scheduleOnce(jobStatusPollInterval second, jobServerBridge, TrackJobProgress(jobId, internalJobId))
          }
        case _ =>
          Future.failed(JobServerError(s"Unknown job state found: ${status}"))
          schedulerSystem.scheduler.scheduleOnce(jobStatusPollInterval second, jobServerBridge, TrackJobProgress(jobId, internalJobId))
      }
    }
  }
}

object SparkJobServerStatus {

  sealed trait Status {
    def name: String
  }

  case object FINISHED extends Status {
    val name = "FINISHED"
  }

  case object RUNNING extends Status {
    val name = "RUNNING"
  }

  case object STARTED extends Status {
    val name = "STARTED"
  }

  case object ERROR extends Status {
    val name = "ERROR"
  }

}

class ExecutionEngineActorModule extends AbstractModule with AkkaGuiceSupport {
  def configure = {
    bindActor[ExecutionEngine]("ExecutionEngineActor")
    bindActor[JobServerBridge]("JobServerBridgeActor", RoundRobinPool(numWorkersJobServerBridge).props)
    bindActor[CompilationEngine]("CompilationEngineActor", RoundRobinPool(numWorkersCompilerWorkers).props)
  }
}