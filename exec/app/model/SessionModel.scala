package model

import model.Exceptions.DBIOError
import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import model.ClusterModel.Cluster

object SessionModel extends {
  val profile = slick.driver.MySQLDriver
} with SessionMetadata {

  import profile.api._
  import scala.concurrent.ExecutionContext.Implicits.global

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  /**
    * Adds a new session to the cluster.
    *
    * @param sessionRow
    * @return
    */
  def addSession(sessionRow: SessionRow): Future[String] = {
    dbConfig.db.run(Session += sessionRow).transform({ _ =>
      sessionRow.sessionId
    }, {
      DBIOError("Failed to create a new session.", _)
    })
  }

  /**
    * Adds a new session to the cluster.
    *
    * @param sessionId
    * @return
    */
  def deleteSession(sessionId: String): Future[String] = {
    val q = for {s <- Session if s.sessionId === sessionId} yield (s.status)
    val action = q.update(SessionStatus.Unavailable.name)
    dbConfig.db.run(action).transform({ _ =>
      sessionId
    }, { ex =>
      DBIOError(s"Failed to delete the session ${sessionId}", ex)
    })
  }

  /**
    * Lists down the available/unavailable sessions.
    *
    * @param status
    * @return
    */
  def listSessions(status: Option[SessionStatus.Status]) = {
    val query = if (status.isDefined) {
      Session.filter(_.status === status.toString).result
    } else {
      Session.result
    }
    dbConfig.db.run(query)
  }
}

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait SessionMetadata {
  val profile: slick.driver.JdbcProfile

  import profile.api._
  import slick.model.ForeignKeyAction

  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** Entity class storing rows of table Session
    *
    * @param clusterId Database column CLUSTER_ID SqlType(VARCHAR), Length(255,true)
    * @param userId    Database column USER_ID SqlType(VARCHAR), Length(255,true)
    * @param projectId Database column PROJECT_ID SqlType(VARCHAR), Length(255,true)
    * @param sessionId Database column SESSION_ID SqlType(VARCHAR), PrimaryKey, Length(255,true)
    * @param status    Database column STATUS SqlType(VARCHAR), Length(255,true) */
  case class SessionRow(clusterId: String, userId: String, projectId: String, sessionId: String, status: String)

  /** GetResult implicit for fetching SessionRow objects using plain SQL queries */
  implicit def GetResultSessionRow(implicit e0: GR[String]): GR[SessionRow] = GR {
    prs => import prs._
      SessionRow.tupled((<<[String], <<[String], <<[String], <<[String], <<[String]))
  }

  /** Table description of table SESSION. Objects of this class serve as prototypes for rows in queries. */
  class Session(_tableTag: Tag) extends Table[SessionRow](_tableTag, "SESSION") {
    def * = (clusterId, userId, projectId, sessionId, status) <>(SessionRow.tupled, SessionRow.unapply)

    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(clusterId), Rep.Some(userId), Rep.Some(projectId), Rep.Some(sessionId), Rep.Some(status)).shaped.<>({ r => import r._; _1.map(_ => SessionRow.tupled((_1.get, _2.get, _3.get, _4.get, _5.get))) }, (_: Any) => throw new Exception("Inserting into ? projection not supported."))

    /** Database column CLUSTER_ID SqlType(VARCHAR), Length(255,true) */
    val clusterId: Rep[String] = column[String]("CLUSTER_ID", O.Length(255, varying = true))
    /** Database column USER_ID SqlType(VARCHAR), Length(255,true) */
    val userId: Rep[String] = column[String]("USER_ID", O.Length(255, varying = true))
    /** Database column PROJECT_ID SqlType(VARCHAR), Length(255,true) */
    val projectId: Rep[String] = column[String]("PROJECT_ID", O.Length(255, varying = true))
    /** Database column SESSION_ID SqlType(VARCHAR), PrimaryKey, Length(255,true) */
    val sessionId: Rep[String] = column[String]("SESSION_ID", O.PrimaryKey, O.Length(255, varying = true))
    /** Database column STATUS SqlType(VARCHAR), Length(255,true) */
    val status: Rep[String] = column[String]("STATUS", O.Length(255, varying = true))

    /** Foreign key referencing Cluster (database name SESSION_FK1) */
    lazy val clusterFk = foreignKey("SESSION_FK1", (clusterId, userId), Cluster)(r => (r.clusterId, r.userId), onUpdate = ForeignKeyAction.NoAction, onDelete = ForeignKeyAction.NoAction)
  }

  /** Collection-like TableQuery object for table Session */
  lazy val Session = new TableQuery(tag => new Session(tag))
}



