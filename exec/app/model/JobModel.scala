package model

import model.Exceptions.DBIOError
import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}
import model.SessionModel._
import play.api.libs.json.Json

object JobModel extends {
  val profile = slick.driver.MySQLDriver
} with JobMetadata {

  import profile.api._
  import scala.concurrent.ExecutionContext.Implicits.global

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  /**
    * Adds a new job to the metadata.
    *
    * @param jobRow
    * @return
    */
  def addJob(jobRow: JobRow) = {
    dbConfig.db.run(Job += jobRow).transform({ _ =>
      jobRow.jobId
    }, { ex =>
      DBIOError("Failed to add a new job to the metadata.", ex)
    })
  }

  /**
    * Returns the job with the job id.
    *
    * @param jobId
    * @return
    */
  def getJob(jobId: String) = {
    val query = Job.filter(_.jobId === jobId).result
    dbConfig.db.run(query)
  }

  /**
    * Updates a job state.
    *
    * @param jobId
    * @param state
    * @param ex
    * @return
    */
  def updateJobStateWithException(jobId: String, state: JobState.State, ex: Option[Throwable]) = {
    val error = ex.map(ModelUtils.toJson(_)).map(Json.stringify(_))
    updateJobStateWithResult(jobId, state, error)
  }


  /**
    * Updates a job state.
    *
    * @param jobId
    * @param state
    * @param result
    * @return
    */
  def updateJobStateWithResult(jobId: String, state: JobState.State, result: Option[String]) = {
    val q = for {j <- Job if j.jobId === jobId} yield (j.state, j.result)
    val action = q.update((state.name, result))
    dbConfig.db.run(action).transform({ _ =>
      state
    }, { ex =>
      DBIOError("Failed to update the job state.", ex)
    })
  }

  /**
    * Updates a job state.
    *
    * @param jobId
    * @param state
    * @return
    */
  def updateJobState(jobId: String, internalJobId: String, state: JobState.State) = {
    val q = for {j <- Job if j.jobId === jobId} yield (j.state, j.intJobId)
    val action = q.update((state.name, Some(internalJobId)))
    dbConfig.db.run(action).transform({ _ =>
      state
    }, { ex =>
      DBIOError("Failed to update the job state.", ex)
    })
  }

  /**
    * List down jobs with the given state.
    *
    * @param state
    * @return
    */
  def listJobs(state: Option[JobState.State] = None) = {
    val query = if (state.isDefined) {
      Job.filter(_.state === state.toString).result
    } else {
      Job.result
    }
    dbConfig.db.run(query)
  }
}

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait JobMetadata {
  val profile: slick.driver.JdbcProfile

  import profile.api._
  import slick.model.ForeignKeyAction

  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** Entity class storing rows of table Job
    *  @param jobId Database column JOB_ID SqlType(VARCHAR), PrimaryKey, Length(255,true)
    *  @param workflowId Database column WORKFLOW_ID SqlType(VARCHAR), Length(255,true)
    *  @param state Database column STATE SqlType(VARCHAR), Length(255,true)
    *  @param startTime Database column START_TIME SqlType(TIMESTAMP)
    *  @param endTime Database column END_TIME SqlType(TIMESTAMP), Default(None)
    *  @param sessionId Database column SESSION_ID SqlType(VARCHAR), Length(255,true)
    *  @param intJobId Database column INT_JOB_ID SqlType(VARCHAR), Length(255,true), Default(None)
    *  @param result Database column RESULT SqlType(TEXT), Default(None) */
  case class JobRow(jobId: String, workflowId: String, state: String, startTime: java.sql.Timestamp, endTime: Option[java.sql.Timestamp] = None, sessionId: String, intJobId: Option[String] = None, result: Option[String] = None)
  /** GetResult implicit for fetching JobRow objects using plain SQL queries */
  implicit def GetResultJobRow(implicit e0: GR[String], e1: GR[java.sql.Timestamp], e2: GR[Option[java.sql.Timestamp]], e3: GR[Option[String]]): GR[JobRow] = GR{
    prs => import prs._
      JobRow.tupled((<<[String], <<[String], <<[String], <<[java.sql.Timestamp], <<?[java.sql.Timestamp], <<[String], <<?[String], <<?[String]))
  }
  /** Table description of table JOB. Objects of this class serve as prototypes for rows in queries. */
  class Job(_tableTag: Tag) extends Table[JobRow](_tableTag, "JOB") {
    def * = (jobId, workflowId, state, startTime, endTime, sessionId, intJobId, result) <> (JobRow.tupled, JobRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(jobId), Rep.Some(workflowId), Rep.Some(state), Rep.Some(startTime), endTime, Rep.Some(sessionId), intJobId, result).shaped.<>({r=>import r._; _1.map(_=> JobRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6.get, _7, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column JOB_ID SqlType(VARCHAR), PrimaryKey, Length(255,true) */
    val jobId: Rep[String] = column[String]("JOB_ID", O.PrimaryKey, O.Length(255,varying=true))
    /** Database column WORKFLOW_ID SqlType(VARCHAR), Length(255,true) */
    val workflowId: Rep[String] = column[String]("WORKFLOW_ID", O.Length(255,varying=true))
    /** Database column STATE SqlType(VARCHAR), Length(255,true) */
    val state: Rep[String] = column[String]("STATE", O.Length(255,varying=true))
    /** Database column START_TIME SqlType(TIMESTAMP) */
    val startTime: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("START_TIME")
    /** Database column END_TIME SqlType(TIMESTAMP), Default(None) */
    val endTime: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("END_TIME", O.Default(None))
    /** Database column SESSION_ID SqlType(VARCHAR), Length(255,true) */
    val sessionId: Rep[String] = column[String]("SESSION_ID", O.Length(255,varying=true))
    /** Database column INT_JOB_ID SqlType(VARCHAR), Length(255,true), Default(None) */
    val intJobId: Rep[Option[String]] = column[Option[String]]("INT_JOB_ID", O.Length(255,varying=true), O.Default(None))
    /** Database column RESULT SqlType(TEXT), Default(None) */
    val result: Rep[Option[String]] = column[Option[String]]("RESULT", O.Default(None))

    /** Foreign key referencing Session (database name JOB_FK1) */
    lazy val sessionFk = foreignKey("JOB_FK1", sessionId, Session)(r => r.sessionId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  }
  /** Collection-like TableQuery object for table Job */
  lazy val Job = new TableQuery(tag => new Job(tag))
}

