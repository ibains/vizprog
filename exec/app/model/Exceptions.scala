package model

import java.io.IOError

object Exceptions {
  case class DBIOError(msg: String, ex: Throwable) extends RuntimeException(msg, ex)
  case class AWSError(msg:String) extends RuntimeException(msg)
  case class JobServerError(msg: String, ex: Throwable = null) extends RuntimeException(msg, ex)
  case class CompilationError(msg: String, backtrace: String) extends RuntimeException(s"${msg}, Reason:\n${backtrace}")
}
