package model

import model.RequestBeans.ClusterReq
import org.joda.time.DateTime
import slick.lifted.Tag
import slick.model.{Column, Table}
import slick.driver.JdbcProfile
import slick.driver.MySQLDriver.api._

object Clusters {
  def get = Db.clusters

  def save(cc: ClusterReq) = Db.clusters = Db.clusters ::: List(cc)

  def get(id: String) = Db.execClusters.get(id)

/*  def add(id: String, exec: ExecCluster): Boolean = {
    Db.execClusters.put(id, exec)
    true
  }*/
}

object Db {
  protected var maxPort: Long = 12000
  var clusters = List[ClusterReq]()
  val execClusters = new ConcurMap[ClusterReq]()
  val jobMap = new ConcurMap[Job]()
  def newPort : Long = { maxPort = maxPort + 1; maxPort }   // this will get into database
}