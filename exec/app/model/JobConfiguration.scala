package model

object ClusterMode {

  sealed trait Mode {
    def name: String
  }

  case object LOCAL extends Mode {
    val name = "LOCAL"
  }

  case object REMOTE extends Mode {
    val name = "REMOTE"
  }

  case class ANY_CLUSTER_MODE(name: String) extends Mode

}

object ExecutionMode {

  sealed trait Mode {
    def name: String
  }

  case object Interactive extends Mode {
    val name = "INTERACTIVE"
  }

  case object Batch extends Mode {
    val name = "BATCH"
  }

  case class ANY_EXECUTION_MODE(name: String) extends Mode

}