package model

import java.util.UUID

import model.ClusterMode.ANY_CLUSTER_MODE
import model.ExecutionMode.ANY_EXECUTION_MODE
import org.joda.time.DateTime

import scala.collection.mutable.Map
import java.util.concurrent.locks.ReentrantLock

import play.api.libs.json.{JsNumber, _}

class ConcurMap[T] {
  protected val l = new ReentrantLock()
  protected val m = Map[String, T]()
  var i = 0

  def has(k: String): Boolean = {
    lk();
    val r = m.contains(k);
    uk();
    r
  }

  def put(k: String, v: T): Unit = {
    lk();
    m(k) = v;
    uk()
  }

  def get(k: String): T = {
    lk();
    val r = m(k);
    m.remove(k);
    uk();
    r
  }

  def getId: String = {
    lk();
    i = i + 1;
    val r = "id" + i;
    uk();
    r
  }

  def lk(): Unit = l.lock()

  def uk(): Unit = l.unlock()
}

object RequestBeans {

  case class ClusterReq(memory: Int, processor: Int, userId: String)

  case class SessionReq(clusterId: String, userId: String, projectId: String, interactive: Option[Boolean])

  case class SubmitJobReq(sessionId: String, workflowId: String, sparkCode: String)

  case class ExecuteInteractiveReq(sessionId: String, sparkCode: String)

  case class CompileRequest(workflowId: String, clusterId: String, script: String)

  case class ExecutionRequest(jobId: String, clusterId: String)

  case class InterimNode(id: String, port: String)

  case class InterimNodes(id: String, tmpFolder: String, nodes: Seq[InterimNode])

  case class BackTrace(declaringClass: String, methodName: String, fileName: String, lineNumber: Int)

  case class ServerException(message: String, cause: ServerException, stackTrace: Seq[BackTrace])

}

object JobState {

  sealed trait State {
    def name: String
  }

  case object New extends State {
    override def name: String = "New"
  }

  case object Compiling extends State {
    override def name: String = "Compiling"
  }

  case object Submitting extends State {
    override def name: String = "Submitting"
  }

  case object Triggered extends State {
    override def name: String = "Triggered"
  }

  case object Running extends State {
    override def name: String = "Running"
  }

  case object Failed extends State {
    override def name: String = "Failed"
  }

  case object Completed extends State {
    override def name: String = "Completed"
  }

}

object ModelUtils {

  def toJson(th: Throwable): JsValue = Json.obj(
    "errorStackTrace" -> Json.obj(
      "errorMessage" -> JsString(th.getMessage),
      "stackTrace" -> JsArray(
        th.getStackTrace.toList.map { st =>
          Json.obj(
            "file" -> JsString(st.getFileName),
            "class" -> JsString(st.getClassName),
            "method" -> JsString(st.getMethodName),
            "line" -> JsNumber(st.getLineNumber)
          )
        })
    )
  )
}

object ClusterStatus {

  sealed trait Status {
    def name: String
  }

  case object Available extends Status {
    override def name: String = "Available"
  }

  case object Unavailable extends Status {
    override def name: String = "Unavailable"
  }

}

object SessionStatus {

  sealed trait Status {
    def name: String
  }

  case object Available extends Status {
    override def name: String = "Available"
  }

  case object Unavailable extends Status {
    override def name: String = "Unavailable"
  }

}

// Lives in interim to encapsulate all data required for generating an app
class AppStructure {
  val files = Map[String, String]()
  var root = ""
  var id = ""
  var interim = ""
  var target = ""
  var dirs = List[String]()
  var compileCmd: Seq[String] = null
  var batchCmd: Seq[String] = null

  def appendDirectory(dir: String) = {
    dirs = dirs :+ dir
  }

  def addFile(loc: String, content: String) = {
    files(loc) = content
  }
}

// Lives for the duration of
class Job {
  var id: String = ""
  var wid: String = ""
  var script: String = ""
  var root: String = ""
  var interimRoot: String = ""
  var jar: String = ""
  var batchCmd: Seq[String] = null
  var clusterMode: ClusterMode.Mode = ANY_CLUSTER_MODE("NA")
  var executionMode: ExecutionMode.Mode = ANY_EXECUTION_MODE("NA")
  var compileResult: String = ""
  var executionResult: String = ""
}
