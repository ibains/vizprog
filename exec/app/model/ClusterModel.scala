package model

import model.Exceptions.DBIOError
import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile

import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

object ClusterModel extends {
  val profile = slick.driver.MySQLDriver
} with ClusterMetadata {

  import profile.api._
  import scala.concurrent.ExecutionContext.Implicits.global

  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

  /**
    * Adds a new cluster to the metadata.
    *
    * @param clusterRow
    * @return
    */
  def addCluster(clusterRow: ClusterRow): Future[Try[String]] = {
    dbConfig.db.run(Cluster += clusterRow).map(res => Success(clusterRow.clusterId)).recover {
      case ex: Throwable => Failure(DBIOError("Failed to add a new Cluster", ex))
    }
  }

  /**
    * Lists down the available/unavailable clusters.
    *
    * @param status
    * @return
    */
  def listClusters(status: Option[ClusterStatus.Status]): Future[Seq[ClusterRow]] = {
    val query = if(status.isDefined) {
      Cluster.filter(_.status === status.get.name).result
    } else {
      Cluster.result
    }
    dbConfig.db.run(query)
  }
}

/** Slick data model trait for extension, choice of backend or usage in the cake pattern. (Make sure to initialize this late.) */
trait ClusterMetadata {
  val profile: slick.driver.JdbcProfile
  import profile.api._
  // NOTE: GetResult mappers for plain SQL are only generated for tables where Slick knows how to map the types of all columns.
  import slick.jdbc.{GetResult => GR}

  /** Entity class storing rows of table Cluster
    *  @param clusterId Database column CLUSTER_ID SqlType(VARCHAR), Length(255,true)
    *  @param userId Database column USER_ID SqlType(VARCHAR), Length(255,true)
    *  @param status Database column STATUS SqlType(VARCHAR), Length(255,true)
    *  @param startTime Database column START_TIME SqlType(TIMESTAMP)
    *  @param endTime Database column END_TIME SqlType(TIMESTAMP), Default(None)
    *  @param memory Database column MEMORY SqlType(INT)
    *  @param cpu Database column CPU SqlType(INT)
    *  @param ip Database column IP SqlType(VARCHAR), Length(255,true), Default(None) */
  case class ClusterRow(clusterId: String, userId: String, status: String, startTime: java.sql.Timestamp, endTime: Option[java.sql.Timestamp] = None, memory: Int, cpu: Int, ip: Option[String] = None)
  /** GetResult implicit for fetching ClusterRow objects using plain SQL queries */
  implicit def GetResultClusterRow(implicit e0: GR[String], e1: GR[java.sql.Timestamp], e2: GR[Option[java.sql.Timestamp]], e3: GR[Int], e4: GR[Option[String]]): GR[ClusterRow] = GR{
    prs => import prs._
      ClusterRow.tupled((<<[String], <<[String], <<[String], <<[java.sql.Timestamp], <<?[java.sql.Timestamp], <<[Int], <<[Int], <<?[String]))
  }
  /** Table description of table CLUSTER. Objects of this class serve as prototypes for rows in queries. */
  class Cluster(_tableTag: Tag) extends Table[ClusterRow](_tableTag, "CLUSTER") {
    def * = (clusterId, userId, status, startTime, endTime, memory, cpu, ip) <> (ClusterRow.tupled, ClusterRow.unapply)
    /** Maps whole row to an option. Useful for outer joins. */
    def ? = (Rep.Some(clusterId), Rep.Some(userId), Rep.Some(status), Rep.Some(startTime), endTime, Rep.Some(memory), Rep.Some(cpu), ip).shaped.<>({r=>import r._; _1.map(_=> ClusterRow.tupled((_1.get, _2.get, _3.get, _4.get, _5, _6.get, _7.get, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

    /** Database column CLUSTER_ID SqlType(VARCHAR), Length(255,true) */
    val clusterId: Rep[String] = column[String]("CLUSTER_ID", O.Length(255,varying=true))
    /** Database column USER_ID SqlType(VARCHAR), Length(255,true) */
    val userId: Rep[String] = column[String]("USER_ID", O.Length(255,varying=true))
    /** Database column STATUS SqlType(VARCHAR), Length(255,true) */
    val status: Rep[String] = column[String]("STATUS", O.Length(255,varying=true))
    /** Database column START_TIME SqlType(TIMESTAMP) */
    val startTime: Rep[java.sql.Timestamp] = column[java.sql.Timestamp]("START_TIME")
    /** Database column END_TIME SqlType(TIMESTAMP), Default(None) */
    val endTime: Rep[Option[java.sql.Timestamp]] = column[Option[java.sql.Timestamp]]("END_TIME", O.Default(None))
    /** Database column MEMORY SqlType(INT) */
    val memory: Rep[Int] = column[Int]("MEMORY")
    /** Database column CPU SqlType(INT) */
    val cpu: Rep[Int] = column[Int]("CPU")
    /** Database column IP SqlType(VARCHAR), Length(255,true), Default(None) */
    val ip: Rep[Option[String]] = column[Option[String]]("IP", O.Length(255,varying=true), O.Default(None))

    /** Primary key of Cluster (database name CLUSTER_PK) */
    val pk = primaryKey("CLUSTER_PK", (clusterId, userId))
  }
  /** Collection-like TableQuery object for table Cluster */
  lazy val Cluster = new TableQuery(tag => new Cluster(tag))
}



