package model

import org.joda.time.DateTime
import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Writes}

/**
  * Data Transfer Objects for Entity Exchanges.
  */
object DataTransferObjects {

  case class Cluster(clusterId: String, userId: String, status: String, startTime: DateTime,
                     endTime: Option[DateTime], memory: Int, cpu: Int, ip: String)

  case class Session(clusterId: String, userId: String, projectId: String, sessionId: String, status: String)

  case class Job(jobId: String, workflowId: String, state: String, startTime: DateTime, endTime: Option[DateTime],
                 sessionId: String, internalJobId: String, result: String)

}

object DTOImplicits {

  import DataTransferObjects._

  implicit val clusterWrites: Writes[Cluster] = (
    (JsPath \ "clusterId").write[String] and
      (JsPath \ "userId").write[String] and
      (JsPath \ "status").write[String] and
      (JsPath \ "startTime").write[DateTime] and
      (JsPath \ "endTime").writeNullable[DateTime] and
      (JsPath \ "memory").write[Int] and
      (JsPath \ "cpu").write[Int] and
      (JsPath \ "ip").write[String]
    ) (unlift(Cluster.unapply))

  implicit val sessionWrites: Writes[Session] = (
    (JsPath \ "clusterId").write[String] and
      (JsPath \ "userId").write[String] and
      (JsPath \ "projectId").write[String] and
      (JsPath \ "sessionId").write[String] and
      (JsPath \ "status").write[String]
    ) (unlift(Session.unapply))


  implicit val jobWrites: Writes[DataTransferObjects.Job] = (
    (JsPath \ "jobId").write[String] and
      (JsPath \ "workflowId").write[String] and
      (JsPath \ "state").write[String] and
      (JsPath \ "startTime").write[DateTime] and
      (JsPath \ "endTime").writeNullable[DateTime] and
      (JsPath \ "sessionId").write[String] and
      (JsPath \ "internalJobId").write[String] and
      (JsPath \ "result").write[String]
    ) (unlift(Job.unapply))
}
