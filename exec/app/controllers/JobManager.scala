package controllers

import java.net.URLDecoder
import java.sql.Timestamp
import java.util.UUID

import akka.actor.{ActorRef, ActorSystem}
import com.google.inject.name.Named
import com.google.inject.{Inject, Singleton}
import engine.ExecutionEngine.{SubmitBatchJob, SubmitInteractiveJob}
import model.Exceptions.JobServerError
import model.JobModel._
import model._
import org.joda.time.DateTime
import play.api.Logger
import play.api.libs.json.Json
import spray.client.pipelining
import spray.client.pipelining._
import spray.http.{HttpEntity, StatusCodes}

import scala.concurrent.{Await, Future}
import conf.AppConfiguration._

import scala.util.{Failure, Success}

@Singleton
class JobManager @Inject()(@Named("ExecutionEngineActor") executionEngine: ActorRef) {


  implicit val system = ActorSystem("JobManager")

  import akka.pattern.ask
  import akka.util.Timeout
  import scala.concurrent.duration._

  implicit val ec = system.dispatcher


  //  Execution Engine Master Actor.
  //  val executionEngine = system.actorOf(ExecutionEngine.props, "ExecutionEngine")

  def submitJob(submitJobReq: RequestBeans.SubmitJobReq): Future[String] = {

    //    Creating a job id
    // TODO update to use DB generated UUID if required.
    val jobId = s"$uuidPrefix-${UUID.randomUUID().toString}"

    //    Current time.
    val now = new Timestamp(new DateTime().getMillis)

    //    New Job Metadata entry.
    val job = JobRow(
      jobId = jobId,
      workflowId = submitJobReq.workflowId,
      state = JobState.New.name,
      startTime = now,
      endTime = None,
      sessionId = submitJobReq.sessionId,
      intJobId = None
    )

    //    Not saving spark code into the database for resiliance, update if required.
    JobModel.addJob(job).map { jobId =>
      executionEngine ! SubmitBatchJob(submitJobReq.sessionId, jobId, submitJobReq.sparkCode)
      jobId
    }
  }

  def executeInteractive(executeReq: RequestBeans.ExecuteInteractiveReq): Future[Any] = {

    implicit val timeout = Timeout(300 seconds)
    val sparkCodeDecoded = URLDecoder.decode(executeReq.sparkCode, "UTF-8")
    executionEngine ? SubmitInteractiveJob(executeReq.sessionId, sparkCodeDecoded)
  }

  def listJob(jobId: String) = {
    JobModel.getJob(jobId).map {
      _.map {
        jr =>
          DataTransferObjects.Job(
            jobId = jr.jobId,
            workflowId = jr.workflowId,
            state = jr.state,
            startTime = new DateTime(jr.startTime.getTime),
            endTime = jr.endTime.map(new DateTime(_)),
            sessionId = jr.sessionId,
            internalJobId = jr.intJobId.getOrElse(""),
            result = jr.result.getOrElse("")
          )
      }
    }
  }

  def listAllJobs: Future[Seq[DataTransferObjects.Job]] = {
    //TODO Use Cache if required.
    JobModel.listJobs().map {
      _.map {
        jr =>
          DataTransferObjects.Job(
            jobId = jr.jobId,
            workflowId = jr.workflowId,
            state = jr.state,
            startTime = new DateTime(jr.startTime.getTime),
            endTime = jr.endTime.map(new DateTime(_)),
            sessionId = jr.sessionId,
            internalJobId = jr.intJobId.getOrElse(""),
            result = jr.result.getOrElse("")
          )
      }
    }
  }

}
