package controllers

import java.io.File

import com.google.inject.Singleton
import play.api.Logger
import scala.concurrent.{Promise, Future}
import scala.io.Source._
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class LogManager {
  // File functions
  protected def hasFileLog      (path: String)  : Boolean          = new File(path) exists
  protected def canReadFileLog  (path: String)  : Boolean          = new File(path) canRead
  protected def readFileLog     (path: String)  : String           = fromFile(path).getLines mkString "\n"

  def readLogFile(fileName: String): Future[String] = {
    val p = Promise[String]()
    Future {
      if (fileName == null) {
        p.failure(new Exception("Unknown Log requested"))
      } else {
        Logger.info("Running this log: " + fileName)

        if (!hasFileLog(fileName)) {
          Logger.error("Missing log: " + fileName)
          p.failure(new Exception("Log file is missing: " + fileName))
        } else {
          if (!canReadFileLog(fileName)) {
            Logger.error("No permission to read log: " + fileName)
            p.failure(new Exception("Log exists, but unable to read: " + fileName))
          } else {
            p.success(readFileLog(fileName))
          }
        }
      }
    }
    p.future
  }
}
