package controllers

import java.sql.Timestamp
import java.util.UUID

import com.google.inject.Singleton
import model.ClusterModel._
import model.ClusterStatus.Available
import model.{ClusterModel, ClusterStatus}
import model.RequestBeans
import org.joda.time.{JodaTimePermission, DateTime}
import sun.awt.datatransfer.DataTransferer

import scala.concurrent.Future
import scala.util.Try

import model._
import conf.AppConfiguration._

@Singleton
class ClusterManager {

  import scala.concurrent.ExecutionContext.Implicits.global

  def listAvailableClusters: Future[Seq[DataTransferObjects.Cluster]] = {
    //TODO Use Cache if required.
    ClusterModel.listClusters(Some(ClusterStatus.Available)).map {
      _.map { cr =>
        DataTransferObjects.Cluster(
          clusterId = cr.clusterId,
          userId = cr.userId,
          status = cr.status,
          startTime = new DateTime(cr.startTime.getTime),
          endTime = cr.endTime.map(new DateTime(_)),
          memory = cr.memory,
          cpu = cr.cpu,
          ip = cr.ip.getOrElse("")
        )
      }
    }
  }

  protected var counter = 9

  protected def getIp: String = {
    counter += 1
    "192.168.0." + counter
  }

  protected def clusterAllocate(cluster: RequestBeans.ClusterReq) = {
    //TODO Stand a new Spark Cluster and return the IP

    // Generate a new cluster id. Could be an AWS handler as well.
    val clusterId = s"$uuidPrefix-${UUID.randomUUID().toString}"
    Try(getIp, clusterId)
  }

  protected def clusterDestroy(): Boolean = {
    // Destroy the cluster and confirm inaccessible
    true
  }

  // PUBLIC INTERFACE
  def allocateCluster(cluster: RequestBeans.ClusterReq): Try[Future[Try[String]]] = {
    clusterAllocate(cluster).map { ec =>
      val now = new Timestamp(new DateTime().getMillis)
      val clusterRow = ClusterRow.apply(
        ip = Some(ec._1),
        clusterId = ec._2,
        userId = cluster.userId,
        status = ClusterStatus.Available.name,
        startTime = now,
        endTime = None,
        memory = cluster.memory,
        cpu = cluster.processor
      )
      ClusterModel.addCluster(clusterRow)
    }
  }

  /*def destroyCluster(id: String): Boolean = {
    val ec = Clusters.get(id)
    if (ec != null && clusterDestroy(ec))
      return true
    false
  }*/
}
