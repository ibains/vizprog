package controllers

import com.google.inject.Inject
import model._
import play.api.Logger
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.util.{Failure, Success}
import RequestBeans._
import engine.JobServerBridge.{ExecuteInteractiveFailed, ExecuteInteractiveResult}
import model.RequestBeans.ServerException.{unapply => _, _}

import scala.io.Source._
import scala.util.{Failure, Success}

class Application @Inject()(cm: ClusterManager,
                            jm: JobManager,
                            sm: SessionManager,
                            lm: LogManager)
  extends Controller {

  import DTOImplicits._
  import Implicits._

  // ACTIONS
  def index = Action {
    Ok("Monkeys!!!")
  } // dummy

  // CLUSTER MANAGEMENT

  /**
    * Handles the list cluster REST request.
    *
    * @return
    */
  def listClusters = Action.async {
    cm.listAvailableClusters.map { result =>
      val json = Json.toJson(result)
      Ok(json)
    }
  }

  /**
    * Handles the list session REST request.
    *
    * @return
    */
  def listSessions = Action.async {
    sm.listAvailableSessions.map { result =>
      Ok(Json.toJson(result))
    }
  }

  /**
    * Handles the list jobs REST request.
    *
    * @return
    */
  def listJobs = Action.async {
    jm.listAllJobs.map { result =>
      Ok(Json.toJson(result))
    }
  }

  /**
    * Handles the list job REST request.
    *
    * @return
    */
  def listJob(jobId: String) = Action.async {
    jm.listJob(jobId).map { result =>
      Ok(Json.toJson(result.head))
    }
  }

  /**
    * Handles the add cluster REST request.
    *
    * @return
    */
  def addCluster = Action.async { implicit request =>
    request.body.asJson.map { json =>
      json
        .validate[ClusterReq]
        .map {
          case cluster => {
            val result = cm.allocateCluster(cluster)
            result.map {
              _.map {
                case Success(id) => {
                  Logger.info(
                    s"Successfully create the cluster with id: $id")
                  Ok(Json.obj("status" -> "OK", "clusterId" -> id))
                }
                case Failure(ex) => {
                  Logger.error(s"Failed to create a new cluster", ex)
                  InternalServerError(
                    Json.obj("status" -> "KO",
                      "message" -> s"Failed to create Cluster, Cause: ${ex.getMessage}",
                      "traceback" -> ModelUtils.toJson(ex)
                    ))
                }
              }
            } match {
              case Success(id) => id
              case Failure(ex) => {
                Logger.error(s"Failed to provision the new cluster.", ex)
                Future.successful[Result](
                  BadRequest(
                    Json.obj(
                      "status" -> "KO",
                      "message" -> s"Failed to create the cluster, Cause: ${ex.getMessage}",
                      "traceback" -> ModelUtils.toJson(ex))
                  ))
              }
            }
          }
        }
        .recoverTotal { errors =>
          Future.successful[Result](BadRequest(Json.obj(
            "status" -> "KO", "message" -> JsError.toJson(errors))))
        }
    }.getOrElse(Future.successful[Result](BadRequest))
  }

  /**
    * Handles the create session REST request.
    *
    * @return
    */
  def addSession = Action.async { implicit request =>
    request.body.asJson.map { json =>
      json
        .validate[RequestBeans.SessionReq]
        .map {
          case session => {
            val result = sm.allocateSession(session)
            result.map { id =>
              Logger.info(s"Successfully created the session with id: $id")
              Ok(Json.obj("status" -> "OK", "sessionId" -> id))
            }.recover {
              case ex =>
                Logger.error(s"Failed to create a new session", ex)
                InternalServerError(
                  Json.obj("status" -> "KO",
                    "message" -> s"Failed to create session, Cause: ${ex.getMessage}",
                    "traceback" -> ModelUtils.toJson(ex)
                  ))
            }
          }
        }
        .recoverTotal { errors =>
          Future.successful[Result](BadRequest(Json.obj(
            "status" -> "KO", "message" -> JsError.toJson(errors))))
        }
    }.getOrElse(Future.successful[Result](BadRequest))
  }

  /**
    * Handles the destroy session REST request.
    *
    * @return
    */
  def destroySession(sessionId: String) = Action.async {
    sm.destroySession(sessionId).map { id =>
      Logger.info(s"Successfully destroyed the session with id: $id")
      Ok(Json.obj("status" -> "OK"))
    }.recover {
      case ex =>
        Logger.error(s"Failed to destroy the session", ex)
        InternalServerError(
          Json.obj("status" -> "KO",
            "message" -> s"Failed to destroy the session, Cause: ${ex.getMessage}",
            "traceback" -> ModelUtils.toJson(ex)
          ))
    }
  }

  /**
    * Handles the submit job REST request.
    *
    * @return
    */
  def executeBatch = Action.async { implicit request =>
    request.body.asJson.map { json =>
      json
        .validate[RequestBeans.SubmitJobReq]
        .map {
          case submitJobRequest => {
            val result = jm.submitJob(submitJobRequest)
            result.map { id =>
              Logger.info(s"Successfully triggered a new job with id: $id")
              Ok(Json.obj("status" -> "OK", "jobId" -> id))
            }.recover {
              case ex =>
                Logger.error(s"Failed to create a new job.", ex)
                InternalServerError(
                  Json.obj("status" -> "KO",
                    "message" -> s"Failed to create a new job, Cause: ${ex.getMessage}",
                    "traceback" -> ModelUtils.toJson(ex)
                  ))
            }
          }
        }
        .recoverTotal { errors =>
          Future.successful[Result](BadRequest(Json.obj(
            "status" -> "KO", "message" -> JsError.toJson(errors))))
        }
    }.getOrElse(Future.successful[Result](BadRequest))
  }

  //
  def compileScriptLocalDeploy = Action(BodyParsers.parse.json) { request =>
    val ee = request.body.validate[CompileRequest]
    val execution = new local.Execution
    ee.fold(
      errors => {
        BadRequest(
          Json.obj("status" -> "KO", "message" -> JsError.toJson(errors)))
      },
      compileRequest => {
        val (success, result) =
          execution.compileDeployExecution(compileRequest.workflowId,
                                           compileRequest.clusterId,
                                           compileRequest.script)
        if (success) Ok(result) else InternalServerError(result)
      }
    )
  }

  def executeScriptLocalDeploy = Action(BodyParsers.parse.json) { request =>
    val ee = request.body.validate[ExecutionRequest]
    val execution = new local.Execution
    ee.fold(
      errors => {
        BadRequest(
          Json.obj("status" -> "KO", "message" -> JsError.toJson(errors)))
      },
      executionRequest => {
        val (success, result) =
          execution.deployModel(executionRequest.jobId,
                                executionRequest.clusterId)
        if (success) Ok(result) else InternalServerError(result)
      }
    )
  }

  // LOCAL EXECUTION
  def compileScriptLocalBatch = Action(BodyParsers.parse.json) { request =>
    val ee = request.body.validate[CompileRequest]
    val execution = new local.Execution
    ee.fold(
      errors => {
        BadRequest(
          Json.obj("status" -> "KO", "message" -> JsError.toJson(errors)))
      },
      compileRequest => {
        val (success, result) =
          execution.compileBatchExecution(compileRequest.workflowId,
            compileRequest.clusterId,
            compileRequest.script)
        if (success) Ok(result) else InternalServerError(result)
      }
    )
  }

  def executeScriptLocalBatch = Action(BodyParsers.parse.json) { request =>
    val ee = request.body.validate[ExecutionRequest]
    val execution = new local.Execution
    ee.fold(
      errors => {
        BadRequest(
          Json.obj("status" -> "KO", "message" -> JsError.toJson(errors)))
      },
      executionRequest => {
        val (success, result) =
          execution.submitBatchExecution(executionRequest.jobId,
                                         executionRequest.clusterId)
        if (success) Ok(result) else InternalServerError(result)
      }
    )
  }

  def executeScriptLocalInteractive = Action(BodyParsers.parse.json) {
    request =>
      val ee = request.body.validate[ExecutionRequest]
      val execution = new local.Execution
      ee.fold(
        errors => {
          BadRequest(Json.obj("status" -> "KO",
            "message" -> JsError.toJson(errors)))
        },
        executionRequest => {
          Ok(Json.obj("message" -> execution.submitInteractiveExecution(
            executionRequest.jobId, executionRequest.clusterId)))
        }
      )
  }

  def executeCodeInInteractiveSynch = Action(BodyParsers.parse.json) {
    request =>
      val ee = request.body.validate[RequestBeans.SubmitJobReq]
      ee.fold(
        errors => {
          BadRequest(Json.obj("status" -> "KO",
            "message" -> JsError.toJson(errors)))
        },
        submitJobrequest => {
          val result = jm.submitJob(submitJobrequest)
          Ok(Json.obj("status" -> "OK", "result" -> result.toString))
        }
      )
  }

  /**
    * Handles the execute interactive request.
    *
    * @return
    */
  def executeInteractive = Action.async { implicit request =>
    request.body.asJson.map { json =>
      json
        .validate[RequestBeans.ExecuteInteractiveReq]
        .map {
          case executeInteractiveReq => {
            val result = jm.executeInteractive(executeInteractiveReq)
            result.map {
              case ExecuteInteractiveResult(mime: String, contents: String) =>
                Logger.info(s"Successfully executed the code in the interactive shell.")
                Ok(Json.obj("status" -> "OK", "mime" -> mime, "contents" -> contents))
              case ExecuteInteractiveFailed(message: String, ex: Throwable) =>
                Logger.error(s"Failed to execute the code in the interactive shell.")
                InternalServerError(Json.obj(
                  "status" -> "Failed",
                  "error" -> s"${message}, Cause: ${ex.getMessage}",
                  "traceback" -> ModelUtils.toJson(ex)
                ))
            }.recover {
              case ex =>
                Logger.error(s"Failed to execute code in the interactive shell.", ex)
                InternalServerError(
                  Json.obj(
                    "status" -> "KO",
                    "error" -> ex.getMessage,
                    "traceback" -> ModelUtils.toJson(ex)
                  ))
            }
          }
        }
        .recoverTotal { errors =>
          Future.successful[Result](BadRequest(
            Json.obj(
              "status" -> "KO",
              "message" -> JsError.toJson(errors))
          ))
        }
    }.getOrElse(Future.successful[Result](BadRequest))
  }

  def getInterimLocal = Action(BodyParsers.parse.json) { request =>
    val ee = request.body.validate[InterimNodes]
    val execution = new local.Execution
    ee.fold(
      errors => {
        BadRequest(
          Json.obj("status" -> "KO", "message" -> JsError.toJson(errors)))
      },
      nodes => {
        Ok(execution.retrieveInterimFiles(nodes))
      }
    )
  }

  def getLogs(logName: String) = Action.async {
    val fileName: String = logName match {
      case "exec" => conf.AppConfiguration.execLogLocation
      case _ => null
    }
    lm.readLogFile(fileName)
      .map(res => Ok(res))
      .recover { case e: Exception => InternalServerError(e.getMessage) }
  }
}

object Implicits {

  implicit val clusterReads: Reads[ClusterReq] = (
    (JsPath \ "memory").read[Int] and
      (JsPath \ "processor").read[Int] and
      (JsPath \ "userId").read[String]
    ) (ClusterReq.apply _)

  implicit val sessionReads: Reads[SessionReq] = (
    (JsPath \ "clusterId").read[String] and
      (JsPath \ "userId").read[String] and
      (JsPath \ "projectId").read[String] and
      (JsPath \ "interactive").readNullable[Boolean]
    ) (SessionReq.apply _)

  implicit val submitJobReads: Reads[SubmitJobReq] = (
    (JsPath \ "sessionId").read[String] and
      (JsPath \ "workflowId").read[String] and
      (JsPath \ "sparkCode").read[String]
    ) (SubmitJobReq.apply _)

  implicit val executeInterpreterReads: Reads[ExecuteInteractiveReq] = (
    (JsPath \ "sessionId").read[String] and
      (JsPath \ "sparkCode").read[String]
    ) (ExecuteInteractiveReq.apply _)

  implicit val compileRequestWrites: Writes[CompileRequest] = (
    (JsPath \ "workflowId").write[String] and
      (JsPath \ "clusterId").write[String] and
      (JsPath \ "script").write[String]
    ) (unlift(CompileRequest.unapply))

  implicit val compileRequestReads: Reads[CompileRequest] = (
    (JsPath \ "workflowId").read[String] and
      (JsPath \ "clusterId").read[String] and
      (JsPath \ "script").read[String]
    ) (CompileRequest.apply _)

  implicit val executionRequestWrites: Writes[ExecutionRequest] = (
    (JsPath \ "jobId").write[String] and
      (JsPath \ "clusterId").write[String]
    ) (unlift(ExecutionRequest.unapply))

  implicit val executionRequestReads: Reads[ExecutionRequest] = (
    (JsPath \ "jobId").read[String] and
      (JsPath \ "clusterId").read[String]
    ) (ExecutionRequest.apply _)

  implicit val gnodeWrites: Writes[InterimNode] = (
    (JsPath \ "id").write[String] and
      (JsPath \ "port").write[String]
    ) (unlift(InterimNode.unapply _))

  implicit val nodeReads: Reads[InterimNode] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "port").read[String]
    ) (InterimNode.apply _)

  implicit val gnodesWrites: Writes[InterimNodes] = (
    (JsPath \ "id").write[String] and
      (JsPath \ "tmpFolder").write[String] and
      (JsPath \ "nodes").write[Seq[InterimNode]]
    ) (unlift(InterimNodes.unapply _))

  implicit val gnodesReads: Reads[InterimNodes] = (
    (JsPath \ "id").read[String] and
      (JsPath \ "tmpFolder").read[String] and
      (JsPath \ "nodes").read[Seq[InterimNode]]
    ) (InterimNodes.apply _)

  implicit object JsErrorJsonWriter extends Writes[JsError] {
    def writes(o: JsError): JsValue = Json.obj(
      "errors" -> JsArray(
        o.errors.map {
          case (path, validationErrors) =>
            Json.obj(
              "path" -> Json.toJson(path.toString()),
              "validationErrors" -> JsArray(
                validationErrors.map(validationError =>
                  Json.obj(
                    "message" -> JsString(
                      validationError.message),
                    "args" -> JsArray(
                      validationError.args.map(_ match {
                        case x: Int => JsNumber(x)
                        case x => JsString(x.toString)
                      }))
                  )))
            )
        }
      )
    )
  }

}