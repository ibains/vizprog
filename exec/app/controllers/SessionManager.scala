package controllers

import java.util.UUID

import akka.actor.ActorSystem
import com.google.inject.Singleton
import conf.AppConfiguration._
import model.Exceptions.JobServerError
import model.SessionModel._
import model._
import play.api.Logger
import spray.client.pipelining
import spray.client.pipelining._
import spray.http.{HttpEntity, StatusCodes}

import scala.concurrent.Future
import scala.util.Failure

/**
  * Session Management API's.
  */
@Singleton
class SessionManager {

  implicit val system = ActorSystem("HttpClient")

  import system.dispatcher

  def listAvailableSessions: Future[Seq[DataTransferObjects.Session]] = {
    //TODO Use Cache if required.
    SessionModel.listSessions(Some(SessionStatus.Available)).map {
      _.map { sr =>
        DataTransferObjects.Session(
          clusterId = sr.clusterId,
          userId = sr.userId,
          projectId = sr.projectId,
          sessionId = sr.sessionId,
          status = sr.status
        )
      }
    }
  }

  private def sessionDestroy(sessionId: String): Future[String] = {

    //Communication with job server to create a context via spawning a JVM.
    val url = s"http://$sparkJobServerHost/contexts/${sessionId}"
    Logger.info(s"Communication with job server located at $url")
    val pipeline = sendReceive
    val result = pipeline(pipelining.Delete(url))

    result.map { response =>
      response.status match {
        case StatusCodes.OK =>
          util.Success(sessionId)
        case _ =>
          Failure(JobServerError(s"Failed to destroy the session, msg: ${response.message.toString}"))
      }
    }.flatMap {
      case util.Success(s) => Future.successful(s)
      case util.Failure(f) => Future.failed(f)
    }
  }


  private def sessionAllocate(session: RequestBeans.SessionReq): Future[String] = {

    // TODO - FIGURE OUT HOW TO SEND SPARK OPTIONS
    // This will take execution time down to a few seconds
    // "?spark.cores.max=2"

    // Generate a new session id. Could be an AWS handler as well.
    val sessionId = s"$uuidPrefix-${UUID.randomUUID().toString}"

    //Communication with job server to create a context via spawning a JVM.
    val url = s"http://$sparkJobServerHost/contexts/$sessionId?developer-mode="+session.interactive.getOrElse(false)
    Logger.info(s"Communication with job server located at $url")
    val pipeline = sendReceive
    val result = pipeline(pipelining.Post(url, HttpEntity("")))

    result.map { response =>
      response.status match {
        case StatusCodes.OK =>
          util.Success(sessionId)
        case _ =>
          Failure(JobServerError(s"Failed to create a new session, msg: ${response.message.toString}"))
      }
    }.flatMap {
      case util.Success(s) => Future.successful(s)
      case util.Failure(f) => Future.failed(f)
    }
  }

  def destroySession(sessionId: String): Future[String] = {
    sessionDestroy(sessionId).flatMap { sessionId =>
      SessionModel.deleteSession(sessionId)
    }
  }

  // PUBLIC INTERFACE
  def allocateSession(session: RequestBeans.SessionReq): Future[String] = {
    sessionAllocate(session).flatMap { sessionId =>
      val sessionRow = SessionRow(
        clusterId = session.clusterId,
        userId = session.userId,
        projectId = session.projectId,
        sessionId = sessionId,
        status = SessionStatus.Available.name
      )
      SessionModel.addSession(sessionRow)
    }
  }

  /*def destroyCluster(id: String): Boolean = {
    val ec = Clusters.get(id)
    if (ec != null && clusterDestroy(ec))
      return true
    false
  }*/
}
