package local

import model._
import conf._
import codegen.BatchAppGen
import java.net._
import java.util.concurrent.TimeUnit

import play.api.Logger
import play.api.libs.json._
import RequestBeans._

class Execution {

  // BATCH
  protected def writeAndCompileApp(app: AppStructure): (Int, String) = {
    val fs = new LocalFileSystem

    for (f <- app.dirs) {
      fs.createDirectory(f)
    }

    for ((k,v) <- app.files) {
      fs.writeFile(k, v)
    }

    if (fs.hasFile(app.target))
      fs.deleteFile(app.target)

    Logger.debug("Compiling " + app.root)
    val compileResult = fs.execute(app.root, app.compileCmd)
    Logger.debug("Compiled")

    (compileResult._1, s"STDOUT: ${compileResult._2}\n\n, STDERROR: ${compileResult._3}")
  }

  def getDeployRoot(workflowId: String) = AppConfiguration.deployLocalRoot + "/" + workflowId

  def deployModel(jobId: String, clusterId: String): (Boolean, JsObject) = {
    val localFS             = new LocalFileSystem
    val job                 = Db.jobMap.get(jobId)

    Logger.info(s"DeployMode: job is: $job")

    if (! localFS.hasDirectory (job.root) ||
        ! localFS.hasFile      (job.jar ))
      return (false, Json.obj("status" -> "KO",
                              "id"     -> s"${job.wid}",
                              "status" -> "The deploy package unavailable to execute"))

    Logger.debug("Daemonizing the deployment..")
    val (execStatus, execStdout, execStderr) = localFS.execute(job.root, job.batchCmd, true)

    if (execStatus != 0)
      return (false, Json.obj("id" -> s"${job.wid}",
                        "command" -> s"${job.batchCmd}",
                        "stdout"  -> execStdout,
                        "stderr"  -> execStderr))

    // daemon program returns immediately, so wait until we connect to the port of timeout expires
    val (running, resp) = checkDeploymentStatus(job)
    if (!running)
      return (false, resp)

    val response = Json.obj("status" -> "OK", "id" -> s"${job.id}", "url" -> s"${job.interimRoot}")
    Logger.debug(s"Returning deploy model response: $response")
    (true, response)
  }

  def checkDeploymentStatus(job: Job): (Boolean, JsObject) = {
    val host = job.interimRoot.split(":")(0)
    val port = job.interimRoot.split(":")(1).toInt
    val timeout = TimeUnit.SECONDS.toMillis(AppConfiguration.deployCheckIntervalSeconds).toInt
    var retry = true
    var retryAttempts = AppConfiguration.deployCheckAttempts
    while(retry && retryAttempts >= 0) {
      try {
        Logger.info(s"Waiting for deployment to come up on ${job.interimRoot} within $timeout ms..")
        val socket = new Socket()
        socket.connect(new InetSocketAddress(host, port), timeout)
        Logger.info("Connected to deployment successfully!")
        // TODO: we can the pid of the deployment here by running "fuser $port/tcp" command and parse stdout if required
        retry = false
      } catch {
        case e: Exception => {
          Logger.warn(s"Unable to connect to ${job.interimRoot} in ${timeout}ms.. Sleeping for 10 seconds." +
            s" Attempts remaining: $retryAttempts")
          retryAttempts -= 1
          try {
            Thread.sleep(timeout)
          } catch {
            case ie: InterruptedException => Logger.warn("Interrupted while sleeping..")
          }
          if (retryAttempts < 0) {
            return (false, Json.obj("id" -> s"${job.wid}",
              "command" -> "Server socket connect",
              "stderr" -> s"Unable to connect to deployed host:port - ${job.interimRoot}"))
          }
        }
      }
    }

    (true, Json.obj("id" -> s"${job.wid}", "status" -> "OK"))
  }

  def compileDeployExecution(workflowId: String, clusterId: String, script: String): (Boolean, JsObject) = {
    val localFS             = new LocalFileSystem
    val awsS3FS             = new S3FileSystem
    val packageZipFile      = AppConfiguration.deployS3Package
    val newControllerScript = URLDecoder.decode(script, "UTF-8")

    val deployRoot          = getDeployRoot(workflowId)
    val deployZip           = deployRoot  +  "/deploy.zip"
    val unzipFolder         = deployRoot  +  "/deploy"
    val overwriteController = deployRoot  +  "/deploy/app/controllers/ServeController.scala"
    val cleanCmd            = "rm"        :: "-rf"        :: deployRoot  :: Nil
    val unzipCmd            = "unzip"     :: "deploy.zip" :: Nil
    val compileCmd          = "activator" :: "stage"      :: Nil
    val deployServer        = "master.vizprog.com"
    val port                = Db.newPort // unique

    val job         = new Job
    job.id          = Db.jobMap.getId
    job.wid         = workflowId
    job.script      = newControllerScript
    job.jar         = s"$unzipFolder/target/universal/stage/bin/deploy-model"
    job.root        = unzipFolder
    job.interimRoot = s"$deployServer:$port"   // TODO - Bad use of field, remove overloaded use for deploy
    job.batchCmd    = s"${job.jar}" :: s"-Dhttp.port=$port" :: Nil

    Logger.info("CompileDeployExecution: Deploy root is: " + deployRoot + " workflow: " + workflowId)

    assert(awsS3FS.hasFile(packageZipFile) && awsS3FS.canReadFile(packageZipFile),
      "deploy package not readable from S3")

    var compileOut = ""

    // Get Package from S3 and Unzip it
    try {

      if (localFS.hasDirectory(deployRoot)) {
        Logger.debug(s"$deployRoot already exists. Cleaning up..")
        localFS.execute(deployRoot, cleanCmd)
      }

      if (!localFS.hasDirectory(deployRoot)) {
        Logger.debug(s"Creating $deployRoot..")
        localFS.createDirectory(deployRoot)
      }

      Logger.info(s"Downloading file from AWS path: $packageZipFile to deployment path: $deployZip")
      awsS3FS.moveFile(packageZipFile, deployZip)

      Logger.debug("Extracting deployment..")
      val (unzipStatus, unzipStdout, unzipStderr) = localFS.execute(deployRoot, unzipCmd)

      if (unzipStatus == 1)
        return (false, Json.obj("id"     -> s"$workflowId",
                                "stage"  -> "Unzip",
                                "stdout" -> unzipStdout,
                                "stderr" -> unzipStderr))

    } catch {

      case e: Exception => return (false, Json.obj("id"    -> s"$workflowId",
                                            "stage" -> s"Download and Unzip Package",
                                            "error" -> e.getMessage))
    }

    // Overwrite the controller with code sent and compile

    try {

      Logger.debug("Replacing the service controller..")
      localFS.writeFile(overwriteController, newControllerScript)
      Logger.debug("Staging the deployment for running..")
      val (compStatus, compStdout, compStderr) = localFS.execute(unzipFolder, compileCmd)
      compileOut = compStdout
      if (compStatus == 1)
        return (false, Json.obj("id"     -> s"$workflowId",
                                "stage"  -> "Compile",
                                "stdout" -> compStdout,
                                "stderr" -> compStderr))
    } catch {
      case e: Exception => return (false, Json.obj("id"    -> s"$workflowId",
                                            "stage" -> s"Overwrite and Compile",
                                            "error" -> e.getMessage))
    }

    // Double check that we produced the right artifact
    if (! localFS.hasFile(job.jar))
      return (false, Json.obj("id"    -> s"$workflowId",
                              "stage" -> s"Check deploy jar",
                              "error" -> s"Not found: ${job.jar}"))

    Db.jobMap.put(job.id, job)

    (true, Json.obj("id"     -> s"${job.id}",
                    "status" -> s"$compileOut"))
  }

  def compileBatchExecution(workflowId: String, clusterId: String, script: String): (Boolean, JsObject) = {
    val fs     = new LocalFileSystem
    val job    = new Job
    val appGen = new BatchAppGen
    job.id     = Db.jobMap.getId
    job.wid    = workflowId
    job.script = URLDecoder.decode(script, "UTF-8")

    Logger.debug(s"Generating app: ${job.id} ...")
    val app = appGen.generateApp(fs.tempDir, job.script, workflowId)
    Logger.debug("Generated")

    job.root        = app.root
    job.interimRoot = app.interim
    job.batchCmd    = app.batchCmd
    job.jar         = app.target

    Logger.debug(s"Compiling app: ${job.id} ...")
    val (compileSuccess, compileOutput) = writeAndCompileApp(app)
    Logger.debug("Compiled")

    job.compileResult = compileOutput

    val success = (compileSuccess == 0) && fs.hasFile(job.jar)
    if (!success)
      Logger.error("Compile Failed: MISSING TARGET: " + app.target + " \nREASON: " + job.compileResult)

    val encodedCompileResult = URLEncoder.encode(compileOutput, "UTF-8")

    val result = Json.obj(
      "id"     -> s"${job.id}",
      "status" -> s"$encodedCompileResult")

    Db.jobMap.put(job.id, job)

    (success, result)
  }

  def submitBatchExecution(jobId: String, clusterId: String): (Boolean, JsObject) = {
    val fs = new LocalFileSystem
    val job = Db.jobMap.get(jobId)
    Logger.debug(s"Submitting to cluster:$clusterId workflow:${job.wid} app: $jobId ...")
    val (executeResult, executeOutput, executeError) = fs.execute(job.root, job.batchCmd)
    Logger.debug("Submitted")

    job.executionResult = s"STDOUT: ${executeOutput}\n\n, STDERROR: ${executeError}"

    val success = executeResult == 0
    if (!success)
      Logger.error(s"Execution Command:${job.batchCmd}\nResult: ${job.executionResult}")
    
    val encodedExecuteResult = URLEncoder.encode(executeOutput, "UTF-8")

    val result = Json.obj(
      "id"     -> s"${job.id}",
      "status" -> s"$encodedExecuteResult")

    (success, result)
  }

  def submitInteractiveExecution(id: String, script: String) : String = {
    Logger.debug(s"1. Got Interactive Request: $id")
    "Interactive execution not implemented"
  }

  def retrieveInterimFiles(nodes: InterimNodes): String = {
    val fs = new LocalFileSystem
    Logger.debug("Reading interim files for:" + nodes.id + " from: " + nodes.tmpFolder)

    val tmpPath = nodes.tmpFolder + "/"

    var wholeString = "{ \"interimData\": {"
    var i = 0

    for (node <- nodes.nodes) {
      val port = if (node.port == "out") "" else "_" + node.port
      var schema = "\"Schema\":\"NA\""
      var data   = "\"Data\" : \"NA\""
      var stats  = "\"Stats\" : \"NA\""
      var hist   = "\"Histogram\" : \"NA\""
      var cleanse= "\"Cleanse\" : \"NA\""
      var topn   = "\"topN\" : \"NA\""
      val prefix = tmpPath + node.id + port
      val schemaFile = prefix + ".json"
      val dataFile   = prefix + ".csv"
      val statsFile  = prefix + "_stats.json"
      val histFile   = prefix + "_histogram.json"
      val cleanseFile= prefix + "_cleanse.json"
      val topnFile   = prefix + "_topn.json"

      if (fs.canReadFile(schemaFile))
        schema = fs.readFile(schemaFile)
      if (fs.canReadFile(dataFile)) {
        val lines = fs.readFileLines(dataFile)
        var i = 0
        val s = new StringBuilder()
        s ++= "\n\"Data\": {\n"

        for (line <- lines) {
          if (i > 0) s ++= ",\n"
          i += 1

          s ++= "\"Row" + i + "\": ["
          s ++= line
          s ++= "]"
        }
        s ++= "\n}"
        data = s.toString
      }
      if (fs.canReadFile(statsFile)) {
        stats = "\"Stats\" : " + fs.readFile(statsFile)
      }
      if (fs.canReadFile(histFile)) {
        hist = "\"Histogram\" : " + fs.readFile(histFile)
      }
      if (fs.canReadFile(cleanseFile)) {
        cleanse = "\"Cleanse\" : " + fs.readFile(cleanseFile)
      }
      if (fs.canReadFile(topnFile)) {
        topn = fs.readFile(topnFile)
      }
      val jsonString = (if (i > 0) ",\n\"" else "\"") +
        node.id + "_" + node.port + "\": { " +
        schema  + ", " +
        data    + ", " +
        stats   + ", " +
        topn    + ", " +
        hist    +
        cleanse + " }\n"

      wholeString = wholeString + jsonString
      i = i + 1
    }
    wholeString + "}\n}\n"
  }
}
