package conf

import com.typesafe.config.ConfigFactory

// FIXME: Use the new dependency injection for configuration for play 2.5. We should also have default values for all
// these arguments. Something like getInt("config.prop").getOrElse()..
object AppConfiguration {

  lazy val config = ConfigFactory.load()

  lazy val activatorLocation = config.getString("execsrv.env.activator")
  lazy val sparkSubmitLocation = config.getString("execsrv.env.spark-submit")

  lazy val execLogLocation = config.getString("app.log.exec.location")

  /* Spark Job Server Host*/
  lazy val sparkJobServerHost = config.getString("execsrv.jobserver.hosts")

  /* Job Server Communication threshold */
  lazy val sparkJobServerCommunicationTimeout = config.getInt("execsrv.jobserver.communication.timeout")

  /* Compile Engine Configurations */
  lazy val deployS3Package = config.getString("execsrv.compile-engine.deploy.s3location")
  lazy val deployLocalRoot = config.getString("app.deploy.root")
  lazy val deployCheckIntervalSeconds = config.getInt("execsrv.compile-engine.deploy.check.interval.seconds")
  lazy val deployCheckAttempts = config.getInt("execsrv.compile-engine.deploy.check.attempts")

  /* Execution Engine Configurations */
  lazy val numWorkersCompilerWorkers = config.getInt("execsrv.execution-engine.workers.compilers.count")
  lazy val numWorkersJobServerBridge = config.getInt("execsrv.execution-engine.workers.job-server-bridge.count")
  lazy val jobStatusPollInterval = config.getInt("execsrv.execution-engine.status-poll-interval.sec")
  lazy val uuidPrefix = config.getString("execsrv.execution-engine.uuid-prefix")

  /*Execution Engine Compiler Configurations*/
  lazy val versionSpark = config.getString("execsrv.execution-engine.dependencies.version.spark")
  lazy val versionScala = config.getString("execsrv.execution-engine.dependencies.version.scala")
  lazy val versionScalaJar = config.getString("execsrv.execution-engine.dependencies.version.scala-jar")
  lazy val versionSparkJobServer = config.getString("execsrv.execution-engine.dependencies.version.spark-job-server")
  lazy val versionCsvReader = config.getString("execsrv.execution-engine.dependencies.version.csv-reader")
  lazy val versionAwsSdk = config.getString("execsrv.execution-engine.dependencies.version.aws-sdk")
  lazy val versionAssemblyPlugin = config.getString("execsrv.execution-engine.dependencies.version.assembly-plugin")
  lazy val tempDirectory = config.getString("execsrv.execution-engine.temp-directory")
  lazy val replJarLocation = config.getString("execsrv.execution-engine.repl-jar-location")
  lazy val replJobId = config.getString("execsrv.execution-engine.repl-job-id")
}