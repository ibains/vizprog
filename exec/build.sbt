name := """execsrv"""

version := "1.0"

lazy val execsrv = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"
libraryDependencies ++= Seq(
  cache,
  specs2 % Test,
  "com.typesafe.akka" %% "akka-actor" % "2.4.3",
  "com.typesafe.akka" %% "akka-stream" % "2.4.3",
  "mysql" % "mysql-connector-java" % "5.1.34",
  "com.typesafe.play" %% "play-slick" % "2.0.0",
  "com.typesafe.play" %% "play-slick-evolutions" % "2.0.0",
  "io.spray" %% "spray-client" % "1.3.3",
  "org.json4s" %% "json4s-native" % "3.3.0",
  "commons-lang" % "commons-lang" % "2.6",
  "com.amazonaws" % "aws-java-sdk" % "1.11.5"
)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator


fork in run := false
