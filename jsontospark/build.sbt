name := "JsonToSpark"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-native" % "3.3.0",
  "org.scalaj" %% "scalaj-http" % "2.2.1",
  "com.amazonaws" % "aws-java-sdk" % "1.11.5",
  "org.slf4j" % "slf4j-api" % "1.7.12",
  "commons-lang" % "commons-lang" % "2.6"
  )
