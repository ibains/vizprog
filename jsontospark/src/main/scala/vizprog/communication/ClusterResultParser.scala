package vizprog.communication

import java.net.URLDecoder

import org.apache.commons.lang.StringEscapeUtils
import org.joda.time.DateTime
import org.json4s._
import org.json4s.native.JsonMethods._
import vizprog.common._

import scala.collection.mutable.{Map, ListBuffer}
import scala.util.Try
import scalaj.http.HttpResponse

class ClusterResultParser {

  def parseBatchResult(json: String): (String, String) = {
    if (json == null)
      return null
    val parsed = parse(json)
    val JString(id) = parsed \ "id"
    val JString(status) = parsed \ "status"
    val decodedStatus = URLDecoder.decode(status, "UTF-8")
    (id, decodedStatus)
  }

  def parseInterim(workflowId: String, source: String): InterimWorkflow = {
    val interim = parse(source) \ "interimData"
    val interimNodes = Map[String, InterimNode]()

    for (JObject(nodeList) <- interim) {
      for ((nodeId, v) <- nodeList) {
        val interimNode = parseInterimNode(nodeId, v)
        interimNodes(nodeId) = interimNode
      }
      return new InterimWorkflow(workflowId, interimNodes)
    }
    null
  }

  protected def convertJStringArrayToList(a: JValue) : List[String] = {
    val JArray(b) = a
    val lb = ListBuffer[String]()
    for (e <- b) {
      val JString(c) = e
      lb.append(c)
    }
    lb.toList
  }

  def parseInterimNode(nodeId: String, nodeValue: JValue): InterimNode = {
    val schemaVal = nodeValue \ "schema"
    val dataVal = nodeValue \ "Data"
    val statsVal = nodeValue \ "Stats"
    val histVal = nodeValue \ "Histogram"
    val cleanseVal = nodeValue \ "Cleanse"
    val topVal     = nodeValue \ "topN"

    val schema: List[(String, String)] = schemaVal match {
      case JString(unavailable) => Nil
      case JObject(schemaList) =>
        var seq: List[(String, String)] = Nil
        for ((column, JString(dataType)) <- schemaList) {
          seq = (column, dataType) :: seq
        }
        seq.reverse
      case _ => Nil
    }

    val data: List[(String, List[String])] = dataVal match {
      case JString(unavailable) => Nil
      case JObject(dataList) =>
        var seq: List[(String, List[String])] = Nil
        for ((rowId, rowArray) <- dataList) {
          val rowValue = convertJStringArrayToList(rowArray)
          seq = (rowId, rowValue) :: seq
        }

        seq.reverse
      case _ => Nil
    }
    val stats: List[(String, JValue)] = statsVal match {
      case JString(unavailable) => Nil
      case JArray(statList) =>
        var seq: List[(String, JValue)] = Nil
        for (JObject(propList) <- statList) {
          for ((k, v) <- propList) {
            if (k == "summary") {
              val JString(key) = v
              seq = (key, JObject(propList)) :: seq
            }
          }
        }
        seq.reverse
      case _ => Nil
    }
    val hist: List[(String, JValue)] = histVal match {
      case JString(unavailable) => Nil
      case JObject(histList) =>
        var seq: List[(String, JValue)] = Nil
        for ((k, v) <- histList) {
          seq = (k, v) :: seq
        }
        seq.reverse
      case _ => Nil
    }
    val cleanse: List[(String, JValue)] = cleanseVal match {
      case JString(unavailable) => Nil
      case JObject(cleanseList) =>
        var seq: List[(String, JValue)] = Nil
        for ((k, v) <- cleanseList) {
          seq = (k, v) :: seq
        }
        seq.reverse
      case _ => Nil
    }
    val topN: List[(String, JValue)] = topVal match {
      case JString(unavailable) => Nil
      case JObject(topList) =>
        var seq: List[(String, JValue)] = Nil
        for ((value, count) <- topList) {
          seq = (value, count) :: seq
        }
        seq.reverse
      case _ => Nil
    }
    new InterimNode(nodeId, schema, data, stats, hist, cleanse, topN)
  }

  def parseClusterId(json: String): String = {
    if (json == null)
      return null
    val parsed = parse(json)
    val JString(clusterId) = parsed \ "clusterId"
    clusterId
  }

  def parseSessionId(json: String): String = {
    if (json == null)
      return null
    val parsed = parse(json)
    val JString(sessionId) = parsed \ "sessionId"
    sessionId
  }

  def parseJobId(json: String): String = {
    if (json == null)
      return null
    val parsed = parse(json)
    val JString(jobId) = parsed \ "jobId"
    jobId
  }

  def parseJobStatus(json: String): (String, String) = {
    if (json == null)
      return null
    val parsed = parse(json)
    val JString(state) = parsed \ "state"
    val JString(result) = parsed \ "result"
    if (result == "" || result == """""""" || result == """"()""""") {
      (state, "")
    }
    else {
      Try {
        val jsonResult = pretty(render(parse(result)))
        (state, StringEscapeUtils.unescapeJava(jsonResult))
      }.getOrElse((state, result))
    }
  }


  def parseBodyResponse(httpResponse: HttpResponse[String]): String = {
    /*if (httpResponse.code != 200) {
      print(httpResponse.body)
      return null
    }*/
    val json = httpResponse.body
    json
  }
}