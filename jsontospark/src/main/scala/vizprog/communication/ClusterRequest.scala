package vizprog.communication

import java.net.URLEncoder

import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import org.slf4j.LoggerFactory
import scala.util.{Failure, Success, Try}
import scalaj.http._

class ClusterRequest(clusterIp: String) {

  val clusterManagerIp     = clusterIp
  val addClusterURI        = clusterManagerIp + "cluster"
  val listClusterURI       = clusterManagerIp + "clusters"
  val generateLocalAppURI  = clusterManagerIp + "execute/local/batchcompile"
  val generateDeployAppURI = clusterManagerIp + "execute/local/deployCompile"
  val executeDeployAppURI  = clusterManagerIp + "execute/local/deployExecute"
  val submitLocalScriptURI = clusterManagerIp + "execute/local/batchrun"
  val getInterimResultURI  = clusterManagerIp + "interim/local"
  val submitInterJobURI       = clusterManagerIp + "execute/interactive"
  val addSessionURI        = clusterManagerIp + "session"
  val submitBatchJobURI    = clusterManagerIp + "execute/job"
  val jobStatusURI         = clusterManagerIp + "job"
  val smallTimeout         = 100000
  val largeTimeout         = 1500000

  val log = LoggerFactory.getLogger(getClass)

  def setupCluster(memory: BigInt, processor: BigInt, userId: String): HttpResponse[String] = {

    val req = (
      ("memory"    -> memory   ) ~
      ("processor" -> processor) ~
      ("userId"    -> userId   ))

    println(s"Posting create cluster req to $addClusterURI")

    val result = Http(addClusterURI).postData(compact(render(req)))
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(smallTimeout)).asString

    println("SETUP CLUSTER RESULT CODE: " + result.code)
    result
  }

  def listAllClusters() : String = {
    val a = Http(listClusterURI).asString
    a.body.toString
  }
  def destroyCluster(name: String) : String = { "" }

  def submitCompileBatchScriptToCluster(clusterId: String, workFlowId: String,
                                        code: String, callback: String => Unit) : Boolean = {
    val theCode = URLEncoder.encode(code, "UTF-8")
    val script =
        s"""{ "workflowId":"$workFlowId", "clusterId":"$clusterId", "script":"$theCode" }"""

    val result = Http(generateLocalAppURI).postData(script)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString

    callback(result.body)

    println("SUBMIT SCRIPT RESULT CODE: " + result.code)
    result.code == 200
  }

  def submitCompileDeployScriptToCluster(clusterId: String, workflowId: String,
                                         code: String, callback: String => Unit) : Boolean = {
    val theCode = URLEncoder.encode(code, "UTF-8")
    val script =
      s"""{ "workflowId":"$workflowId", "clusterId":"$clusterId", "script":"$theCode" }"""

    val result = Http(generateDeployAppURI).postData(script)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString

    val success = result.code == 200

    if (result != null && result.body != null)
      log.info("Compile Deploy result" + result.body)

    if (success)
      callback(result.body)
    else
      log.error("Compile Deploy error: " + result.body)

    success
  }

  def submitExecuteDeployScriptToCluster(jobId: String, clusterId: String,
                                         callback: String => Unit) : Boolean = {
    val script =
      s"""{ "jobId":"$jobId", "clusterId":"$clusterId" }"""

    val result = Http(executeDeployAppURI).postData(script)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString

    val success = result.code == 200

    log.info("Execute Deploy result" + result.body)

    if (success)
      callback(result.body)
    else
      log.error("Execute Deploy error: " + result.body)

    success
  }

  def submitExecuteBatchScriptToCluster(jobId: String, clusterId:String,
                                        callback: String => Unit) : Boolean = {
    val script =
      s"""{ "jobId":"$jobId", "clusterId":"$clusterId" }"""

    val result = Http(submitLocalScriptURI).postData(script)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString

    callback(result.body)

    println("SUBMIT SCRIPT RESULT CODE: " + result.code)
    result.code == 200
  }

  def fetchInterimFiles(name: String, interimRequest:String, callback: String => Unit) : Boolean = {

    val result = Http(getInterimResultURI).postData(interimRequest)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString

    println("FETCH INTERIM RESULT CODE: " + result.code)

    if (result.code != 200)
      return false

    callback(result.body)

    true
  }

  def addSession(clusterId: String, userId: String, projectId: String, developerMode: Boolean): HttpResponse[String] = {

    val req = (
      ("clusterId"      -> clusterId) ~
      ("userId"         -> userId) ~
      ("projectId"      -> projectId) ~
      ("interactive"    -> developerMode))


    println(s"Posting create session to $addSessionURI")

    val result = Http(addSessionURI).postData(compact(render(req)))
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(100000)).asString

    println("SETUP  SESSION CODE: " + result.code)
    result
  }

  def submitBatchJob(sessionId: String, workflowId: String, sparkCode: String): HttpResponse[String] = {

    val theCode = URLEncoder.encode(sparkCode, "UTF-8")
    val req = (
      ("sessionId"  -> sessionId ) ~
      ("workflowId" -> workflowId) ~
      ("execType"   -> "BATCH"  ) ~
      ("sparkCode"  -> theCode   ))

    print(s"Posting submit job to $submitBatchJobURI");
    val result = Http(submitBatchJobURI).postData(compact(render(req)))
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString
    println("SUBMIT JOb RESULT CODE: " + result.code)
    result
  }

  def submitInteractiveJob(sessionId: String, workflowId: String, sparkCode: String) = {

    val theCode = URLEncoder.encode(sparkCode, "UTF-8")
    val req = (
      ("sessionId"  -> sessionId ) ~
        ("sparkCode"  -> theCode   ))

    log.info(s"Posting submit job to $submitInterJobURI");
    Http(submitInterJobURI).postData(compact(render(req)))
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString
  }

  def getJobStatus(jobId: String) = {
    val url = s"$jobStatusURI/$jobId"
    //println(s"Posting track job request to $url");
    Http(url)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(largeTimeout)).asString
  }

  def destroySession(sessionId: String) = {
    val url = s"$addSessionURI/$sessionId"
    //println(s"Posting track job request to $url");
    Http(url)
      .header("Content-Type", "application/json")
      .header("Charset", "UTF-8")
      .method("DELETE")
      .option(HttpOptions.readTimeout(largeTimeout)).asString
  }
}