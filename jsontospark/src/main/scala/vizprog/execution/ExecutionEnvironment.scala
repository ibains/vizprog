package vizprog.execution

import org.json4s.native.JsonMethods._
import vizprog.codegen.NodeParser
import vizprog.common.{S3FileSystem, LocalFileSystem, FileSystem}

import scala.collection.mutable.Map


abstract class ExecutionEnvironment{
  var intermediateRoot    : String = null
  var executionServiceURL : String = null
  var exampleDataUrl      : String = null
  var exampleWorkflowUrl  : String = null
  var exampleDatasetUrl   : String = null
  var exampleDatasourceURL: String = null
  var userDatasetUrl      : String = null
  var bucketName          : String = null

  // Initialization functions
  def getWorkflows()         : Map[String, String]
  def getDataSets()          : Map[String, String]
  def getDataSources()       : Map[String, String]
  def getDataSourceSchemas() : Map[String, String]
  def readOneWorkflow(path: String) : String

  // Workflow functions
  def setupFileStructure(workflowId: String) : Unit

  //
  def saveOneFile(path: String, content: String) : String
}

class VagrantExecutionEnvironment extends ExecutionEnvironment {
  var fs : FileSystem = new LocalFileSystem
  intermediateRoot = fs.tempDir

  override def getWorkflows()         : Map[String, String] = {null}
  override def getDataSets()          : Map[String, String] = {null}
  override def getDataSources()       : Map[String, String] = {null}
  override def getDataSourceSchemas() : Map[String, String] = {null}
  override def readOneWorkflow(path: String) : String = {null}
  override def saveOneFile(path: String, content: String) : String = { fs.writeFile(path, content) }

  // Create directory structure for local files

  override def setupFileStructure(workflowId: String) : Unit = {

    if (!fs.hasDirectory(fs.tempDir)) {
      val b = fs.createDirectory(fs.tempDir)
      assert(b, "Could not create temporary directory: " + intermediateRoot)
    }
    val workflowTemp = fs.tempDir + "/" + workflowId
    if (!fs.hasDirectory(workflowTemp)) {
      val b = fs.createDirectory(workflowTemp)
      assert(b, "Could not create workflow directory: " + workflowId)
    }
  }
}

class AWSExecutionEnvironment extends ExecutionEnvironment {
  var fs : FileSystem = new S3FileSystem
  intermediateRoot = fs.tempDir

  def setBucketName(bucket: String): Unit = {
    bucketName = bucket
    fs.bucketName = bucketName
  }

  override def readOneWorkflow(path: String) : String = {
    val fileContent = fs.readFile(path)
    fileContent
  }

  override def getWorkflows() : Map[String, String] = {
    val workflowMap = Map[String, String]()
    val items = fs.listDirectoryFiles(exampleWorkflowUrl)
    for (item <- items) {
      val oneWorkflow = readOneWorkflow(item)
      workflowMap(item) = oneWorkflow
    }
    workflowMap
  }

  protected def readOneDatasource(nodeParser: NodeParser, path: String) : (String, String) = {
    val fileContent = fs.readFile(path)
    val name = nodeParser.getNameFromNode(fileContent)
    (name, fileContent)
  }

  override def getDataSources(): Map[String, String] = {
    val datasourceMap = Map[String, String]()
    val nodeParser = new NodeParser
    val items = fs.listDirectoryFiles(exampleDatasourceURL)
    for (item <- items) {
      val (name, content) = readOneDatasource(nodeParser, item)
      datasourceMap(name) = content
    }
    datasourceMap
  }

  protected def readOneDataset(nodeParser: NodeParser, path: String) : (String, String) = {
    val fileContent = fs.readFile(path)
    val name = nodeParser.getNameFromNode(fileContent)
    (name, fileContent)
  }

  override def getDataSets()          : Map[String, String] = {null}

  override def getDataSourceSchemas() : Map[String, String] = {null}
  override def setupFileStructure(workflowId: String) : Unit = {}
  override def saveOneFile(path: String, content: String) : String = { fs.writeFile(path, content) }
}
