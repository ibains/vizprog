package vizprog.codegen

import org.json4s._
import scala.collection.mutable.Map
import vizprog.common._


class NodeSpark(wf: Workflow, outFolder  : String)
{
  val graphId                         = wf.workFlowId
  val order       : List[GNode]       = wf.nodeOrder
  val nodeDetails : Map[String, Node] = wf.allNodes
  val edgeDetails : Map[String, Edge] = wf.allEdges
  val pipelines                       = wf.mlPipeline
  val transformFunctions              = wf.transformFns
  val tmpFolder   : String            = outFolder
  var cg          : CGBuilder         = null
  var pendingCode                     = Map[(String, String), (String) => String]()
  var variableReplaceMap              = Map[String, String]()

  def setCodeGeneration(theCg: CGBuilder) = { cg = theCg }

  // Helpers
  protected def getOrderNode(id: String) : GNode = {
    for (n <- order) if (n.id == id) return n
    null
  }

  protected def addJobServerImports(file: String) : Unit = {
    cg.addImport(file, "com.typesafe.config.Config")
    cg.addImport(file, "spark.jobserver.SparkJobValid")
    cg.addImport(file, "spark.jobserver.SparkJobValidation")
    cg.addImport(file, "spark.jobserver.SparkJob")
  }

  protected def addAWSImports(file: String) : Unit = {
    cg.addImport(file, "com.amazonaws.AmazonServiceException")
    cg.addImport(file, "com.amazonaws.auth.BasicAWSCredentials")
    cg.addImport(file, "com.amazonaws.services.s3._")
    cg.addImport(file, "com.amazonaws.services.s3.model._")
    cg.addImport(file, "com.amazonaws.auth.profile.ProfileCredentialsProvider")
  }

  protected def addPlayDeployImports(file: String) : Unit = {
    cg.addImport(file, "javax.inject._")
    cg.addImport(file, "play.api.libs.json._")
    cg.addImport(file, "play.api.libs.functional.syntax._")
    cg.addImport(file, "play.api.mvc._")
    cg.addImport(file, "play.api.Logger")
    cg.addImport(file, "scala.concurrent.Future")
    cg.addImport(file, "scala.io.Source._")
    cg.addImport(file, "scala.util.parsing.json._")
    cg.addImport(file, "scala.concurrent.ExecutionContext.Implicits.global")
  }

  def addStandardImports(file: String) : Unit = {
    cg.addImport(file, "org.apache.spark.SparkConf")
    cg.addImport(file, "org.apache.spark.SparkContext")
    cg.addImport(file, "org.apache.spark.SparkContext._")
    cg.addImport(file, "org.apache.spark.sql")
    cg.addImport(file, "org.apache.spark.sql.functions._")
    cg.addImport(file, "org.apache.spark.sql.DataFrame")
    cg.addImport(file, "org.apache.spark.sql.Row")
    cg.addImport(file, "org.apache.spark.sql.SQLContext")
    cg.addImport(file, "org.apache.spark.ml")
    cg.addImport(file, "org.apache.spark.sql.types._")
    cg.addImport(file, "org.apache.spark.ml._")
    cg.addImport(file, "org.apache.spark.ml.feature._")
    cg.addImport(file, "org.apache.spark.ml.classification._")
    cg.addImport(file, "org.apache.spark.ml.clustering._")
    cg.addImport(file, "org.apache.spark.ml.evaluation._")
    cg.addImport(file, "org.apache.spark.ml.tuning._")
    cg.addImport(file, "org.apache.spark.sql.functions._")
    cg.addImport(file, "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
    cg.addImport(file, "scala.util.parsing.json._")
    cg.addImport(file, "scala.collection.JavaConversions._")
    cg.addImport(file, "org.apache.commons.lang.StringEscapeUtils.escapeJava")
    cg.target match {
      case CGJobServerBatch => addJobServerImports(file)
      case CGDeploy         => addPlayDeployImports(file)
      case _                => // do nothing
    }
    addAWSImports(file)
  }

  def generateScala(): Unit = {
    for (n <- order) {
      val nodeType = nodeDetails(n.id).component
      nodeType match {
        case "AddColumn"          => getAddColumn(n)
        case "AFTSurvivalRegression"          => getAFTSurvivalRegression(n)
        case "Aggregate"          => getAggregate(n)
        case "ApplyFunctions"     => getApplyFunctions(n)
        case "ApplyStoredModel"   => getApplyStoredModel(n)
        case "Binarizer"          => getBinarizer(n)
        case "BinaryClassificationEvaluator"  => getBinaryClassificationEvaluator(n)
        case "Bucketizer"         => getBucketizer(n)
        case "ChiSqSelector"      => getChiSqSelector(n)
        case "Cube"               => getCube(n)
        case "CrossValidator"     => getCrossValidator(n)
        case "DecisionTreeClassifier"         => getDecisionTreeClassifier(n)
        case "DCT"                => getDCT(n)
        case "Distinct"           => getDistinct(n)
        case "DropMissing"        => getDropMissing(n)
        case "Except"             => getExcept(n)
        case "FileInput"          => getFileInput(n)
        case "FileOutput"         => getFileOutput(n)
        case "Filter"             => getFilter(n)
        case "HashingTF"          => getHashingTF(n)
        case "IndexToString"      => getIndexToString(n)
        case "Intersect"          => getIntersect(n)
        case "Join"               => getJoin(n)
        case "Limit"              => getLimit(n)
        case "LogisticRegression" => getLogisticRegression(n)
        case "MLPipeline"         => getMLPipeline(n)
        case "MLCrossValidator"   => ; // pass
        case "ModelApplier"       => getModelApplier(n)
        case "ModelSaver"         => getModelSaver(n)
        case "MultiFileInput"     => getMultiFileInput(n)
        case "OrderBy"            => getOrderBy(n)
        case "ParamGridBuilder"   => getParamGridBuilder(n)
        case "RESTInput"          => getRestInput(n)
        case "RESTOutput"         => getRestOutput(n)
        case "Sample"             => getSample(n)
        case "Script"             => getScript(n)
        case "Select"             => getSelect(n)
        case "Split"              => getSplit(n)
        case "StringIndexer"      => getStringIndexer(n)
        case "Tokenizer"          => getTokenizer(n)
        case "UnionAll"           => getUnionAll(n)
        case "VectorIndexer"      => getVectorIndexer(n)
        case "WindowFunction"     => getWindowFunction(n)
        case "WriteDataset"       => getWriteDataset(n)
        case "PieChart"           => ; //pass
        case "BarChart"           => ; //pass
        case "LineChart"          => ; //pass
        case "ScatterPlot"        => ; //pass
        case "BoxPlot"            => ; //pass
        case "Visualize"          => ; //pass
        case whatever             => assert(false, "vizprog.codegen.Node type missing: " + nodeType)
      }
    }
  }

  protected def isDependencyEdge(edge: Edge) : Boolean = {
    if (edge.endPort == "_dep_" && edge.startPort == "_dep_")
      return true
    false
  }
  protected def hasPendingCode(nodeId: String, port: String) : Boolean = {
    pendingCode.contains((nodeId, port))
  }
  protected def getPendingCode(nodeId: String, port: String) : (String) => String = {
    pendingCode((nodeId, port))
  }
  protected def registerPendingCode(nodeId: String, port: String, foo: (String) => String): Unit = {
    pendingCode((nodeId, port)) = foo
  }
  protected def removeRegisteredPendingFunction(nodeId: String, port: String) : Unit = {
    pendingCode.remove((nodeId, port))
  }

  def registerReplace(older: String, newer: String) = {
    variableReplaceMap(older) = newer
  }
  def replaced(in: String) : String = {
    if (!variableReplaceMap.contains(in))
      return in
    variableReplaceMap(in)
  }
  protected def getVariableName(node: Node, port: String, bypassCheck: Boolean = false): String = {
    val actualOutputName = s"${port}_${node.id}"

    if (! hasPendingCode(node.id, port) || bypassCheck)  // normal case
      return replaced(actualOutputName)

    // now, we're not generating code for 'node', some later node is
    // generating code and has requested the name for an input read from 'node'
    // however, 'node' has some pending code that should be generated.

    // so we'll read the output of 'node' as we would have. But now we'll generate
    // the pending code using the provided function into a new variable
    // we'll return this variable so that the current node for which code is being
    // generated can use this name as it's input, thereby getting the right input

    val generatingFunction = getPendingCode(node.id, port)
    val newMagicOutput = s"magic_$actualOutputName"
    val generatedPendingCode = generatingFunction(newMagicOutput)
    cg.addCode("main", generatedPendingCode)
    removeRegisteredPendingFunction(node.id, port)
    registerReplace(actualOutputName, newMagicOutput)
    newMagicOutput
  }

  protected def getFirstOutputVar(n: Node, bypassCheck: Boolean = false) : String =
    getVariableName(n, Util.getFirstOutPort(n), bypassCheck)

  protected def getSingleEdgeInput(n: GNode) : String = {
    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge))
        return getVariableName(nodeDetails(edge.startNode), edge.startPort)
    }
    null
  }

  protected def addInterimInfo(s: StringBuilder, out_val: String, node_id: String): Unit = {
    if (!cg.generateIntermediate)
      return
    s++= s"""  writeInterimInfo($out_val, "$tmpFolder/$node_id")\n"""
  }
  protected def cc: String = if (cg.generateIntermediate) ".cache()" else ""

  protected def getSample(n: GNode): Unit = {
    val node = nodeDetails(n.id)
    val s = new StringBuilder()
    val out_val = getFirstOutputVar(node)

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        s++= s"val $out_val = $input.sample("
        s++= Util.getSP(node, "withReplacement")._2; s++= ", "
        s++= Util.getDP(node, "fraction")._2       ; s++= ", "
        s++= Util.getIP(node, "seed")._2           ; s++= ")\n"
      }
    }
    addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getAddColumn(n: GNode): Unit = {
    val node = nodeDetails(n.id)
    val s = new StringBuilder()
    val out_val = getFirstOutputVar(node)
    val interim = Util.getBP(node, "interim")._2

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        s++= s"""val $out_val = ${input}.withColumn("""
        s++= Util.getSP(node, "columnName")._2; s++= "\", \""
        s++= Util.getDP(node, "expression")._2; s++= "\")\n"
      }
    }
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getSplit(n: GNode): Unit = {
    val node        = nodeDetails(n.id)
    val s           = new StringBuilder()
    val out_val     = getVariableName(node, "out")
    val out_val_one = getVariableName(node, "one")
    val out_val_two = getVariableName(node, "two")
    val interim     = Util.getBP(node, "interim")._2

    val JDouble(ratio1) = node.properties("ratio")
    val ratio2          = 1.0 - ratio1
    val seed            = Util.getIP(node, "seed")._2

    assert(ratio1 >= 0.0 && ratio1 <= 1.0)

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        cg.addCode("main", s"val $out_val = $input.randomSplit(Array($ratio1, $ratio2), $seed)\n")
        cg.addCode("main", s"val $out_val_one = $out_val(0)$cc\n")
        cg.addCode("main", s"val $out_val_two = $out_val(1)$cc\n")
      }
    }
    if (interim) {
      addInterimInfo(s, out_val_one, n.id + "_one")
      addInterimInfo(s, out_val_two, n.id + "_two")
    }
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  // one.op(columns)
  protected def getSimpleOne(n: GNode, op : String): String = {
    val node    = nodeDetails(n.id)
    val s       = new StringBuilder()
    val out_val = getFirstOutputVar(node)
    val interim = Util.getBP(node, "interim")._2

    for (edgeId <- n.inEdges) {
      val edge  = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        val cols  = Util.getArrayListSP(node, "columns")._2
        s++= s"val $out_val = $input.$op($cols)$cc\n"
      }
    }
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    s.toString()
  }
  protected def getOrderBy(n: GNode) : Unit = cg.addCode("main", getSimpleOne(n, "orderBy"))
  protected def getDistinct(n: GNode): Unit = cg.addCode("main", getSimpleOne(n, "distinct"))

  // TODO - Aggregate, Cube and Window Functions

  protected def getAggregate(n: GNode): Unit = {
    /*
  def buildAggregate: vizprog.codegen.Node = {
    val props        = getProperties
    val aggFn        = JString(hh.getValuesString("ChooseOne" :: "Sum" :: "Avg" :: "Min" :: "Max" :: "Count" :: Nil))
    props("comment") = JString("Aggregate by GroupBy columns/expressions and compute any aggregate functions")
    props("GroupBy") = getExprList
    props("Columns") = JObject(List((data.getString, aggFn), (data.getString, aggFn)))
    vizprog.codegen.Node(getId, "Aggregate", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }
  */
  }

  protected def getCube(n: GNode): Unit = {
    /*
    def buildCube: vizprog.codegen.Node = {
    val props         = getProperties
    props("comment")  = JString("Computes a cube to which faster aggregation can be applied")
    props("Columns")  = getStringList
    vizprog.codegen.Node(getId, "Cube", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }
   */
  }

  protected def getWindowFunction(n: GNode): Unit = {
    /*
      def buildWindowFunction: vizprog.codegen.Node = {
      val props        = getProperties
      val fns          = JString(hh.getValuesString("ChooseOne" :: "Sum" :: "Avg" :: "Min" :: "Max" :: Nil))
      props("comment") = JString("Select using window functions")
      props("Columns") = getStringList
      props("Over1")   = JObject(List(
        ("column"                , JString(data.getString)),
        ("function"              , fns),
        ("orderByExpressions"    , JString(expr.Expr + "*")),
        ("orderByColumns"        , JString(data.getString + "*")),
        ("partitionByExpressions", JString(expr.Expr + "*")),
        ("partitionByColumns"    , JString(data.getString + "*")),
        ("rangeBetween"          , JObject(List(("start", JString(data.getLong)), ("end", JString(data.getLong))))),
        ("rowsBetween"           , JObject(List(("from", JString(data.getLong)), ("to", JString(data.getLong)))))
      ))
      props("Over2") = JObject(List())
      vizprog.codegen.Node(getId, "WindowFunction", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
    } */
  }

  protected def getDropMissing(n: GNode): Unit = {
    val node    = nodeDetails(n.id)
    val s       = new StringBuilder()
    val out_val = getFirstOutputVar(node)
    val interim = Util.getBP(node, "interim")._2

    for (edgeId <- n.inEdges) {
      val edge  = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        s++= s"val $out_val = $input.df.na.drop()\n"
      }
    }
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getFileInput(n: GNode): Unit = {
    val node     = nodeDetails(n.id)
    val s        = new StringBuilder()
    val fileType = Util.getSP(node, "fileType")._2
    val out_val  = getFirstOutputVar(node)
    val interim  = Util.getBP(node, "interim")._2
    val fileSystem  = Util.getSP(node, "fileSystem")
    val credentials = Util.getSP(node, "credentials")

    if (fileSystem._1 && credentials._1)  {
      // we have an external file system and credentials for it
      assert(fileSystem._2 == "s3" && credentials._2 == "system")
      s++= s"""hadoopConf.set("fs.s3.awsAccessKeyId", "${aws.K}")"""
      s++= "\n  "
      s++= s"""hadoopConf.set("fs.s3.awsSecretAccessKey" ,"${aws.S}")"""
      s++= "\n\n"
    }

    fileType match {
      case "csv" =>
        s++= s"""val $out_val = sqlContext.read.format("com.databricks.spark.csv")"""
        s++= ".option(\"header\", \""     ; s++= Util.getSP(node, "header")._2     ; s++= "\")"
        s++= ".option(\"inferSchema\", \""; s++= Util.getSP(node, "inferSchema")._2; s++= "\")"
        s++= ".load(\""                   ; s++= Util.getSP(node, "fileName")._2   ;
        s++= s"""\")$cc\n"""
      case x =>
        assert(false, "Unsupported file type: " + x)
    }
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getMultiFileInput(n: GNode): Unit = {
    val node     = nodeDetails(n.id)
    val s        = new StringBuilder()
    val fileType = Util.getSP(node, "fileType")._2
    val out_val  = getFirstOutputVar(node)
    val interim  = Util.getBP(node, "interim")._2

    fileType match {
      case "text" =>
        cg.addDefinition("main", "case class WholeFiles(file: String, text: String)")
        val tmp_val = out_val + "_RDD_1"
        val (found, pathName) = Util.getSP(node, "pathName")
        assert(found, "Not found property pathName")

        val fileSystem  = Util.getSP(node, "fileSystem")
        val credentials = Util.getSP(node, "credentials")

        if (fileSystem._1 && credentials._1)  {
          // we have an external file system and credentials for it
          assert(fileSystem._2 == "s3" && credentials._2 == "system")
          s++= s"""hadoopConf.set("fs.s3.awsAccessKeyId", "${aws.K}")"""
          s++= "\n  "
          s++= s"""hadoopConf.set("fs.s3.awsSecretAccessKey" ,"${aws.S}")"""
          s++= "\n\n"
        }

        s++= "import sqlContext.implicits._\n"
        s++= s"""  val $tmp_val = sc.wholeTextFiles("$pathName")\n"""
        s++= s"""  val $out_val = $tmp_val.map(x => {val (file,text) = x; WholeFiles(file,text)} ).toDF()$cc\n"""
      case x =>
        assert(false, "Unsupported file type: " + x)
    }
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def gCols(df: String, aa: Array[String]) : String = {
    val s = new StringBuilder()
    var begin = true
    if (aa != null)
      for (a <- aa) {
        if (!begin) s++= ", "
        s++= s"""$df("$a")"""
        begin = false
      }
    s.toString()
  }

  protected def gArgs(fn : String, aa: Array[JValue]) : String = {
    val s = new StringBuilder()
    fn match {
      case "upper"         => ;
      case "lower"         => ;
      case "initcap"       => ;
      case "ltrim"         => ;
      case "rtrim"         => ;
      case "trim"          => ;
      case "length"        => ;
      case "reverse"       => ;
      case "select"        => ;

      case "encode"        =>
        val JString(encoding) = aa(0)
        s++= s""", "$encoding""""

      case "decode"        =>
        val JString(encoding) = aa(0)
        s++= s""", "$encoding""""

      case "format_number" =>
        val JInt(decimals) = aa(0)
        s++= s""", $decimals"""

      case "split"         =>
        val JString(pattern) = aa(0)
        s++= s""", "$pattern""""

      case "take_last_nth" =>
        val JInt(loc) = aa(0)
        s++= s""", lit($loc)"""

      case "column_expression" =>
        val JString(expr) = aa(0)
        s++= s""", $expr"""
    }
    s.toString()
  }

  protected def getApplyFunctions(n: GNode): Unit = {
    val functions = transformFunctions(n.id)
    val node      = nodeDetails(n.id)
    val out_val   = getFirstOutputVar(node)
    val s         = new StringBuilder()
    val interim   = Util.getBP(node, "interim")._2

    var input = ""
    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge))
        input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
    }

    var i = 0
    var out = ""
    for (f <- functions.functions) {
      out = s"out_${n.id}_$i"
      val inCols = gCols(input, f.inputColumns)
      val args   = gArgs(f.functionName, f.inputLiterals)
      f.functionName match {
        case "select" =>
          s++= s"""  val $out = $input.select($inCols)\n"""
        case "column_expression" =>
          s++= s"""  val $out = $input.withColumn("${f.outColumn}"$args)\n"""
        case _        =>
          s++= s"""  val $out = $input.withColumn("${f.outColumn}", ${f.functionName}($inCols$args))\n"""
      }
      i = i + 1
      input = out
    }
    s++= s"  val $out_val = $input$cc\n"
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getFilter(n: GNode): Unit = {
    val node    = nodeDetails(n.id)
    val s       = new StringBuilder()
    val out_val = getFirstOutputVar(node)
    val interim = Util.getBP(node, "interim")._2

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        val condition = Util.getSP(node, "condition")._2
        s++= s"""val $out_val = $input.filter("$condition")$cc\n"""
      }
    }
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getJoin(n: GNode): Unit = {
    val node = nodeDetails(n.id)
    val s = new StringBuilder()
    var leftInput  = ""
    var rightInput = ""
    var leftNode   = ""
    var rightNode  = ""
    val out_val = getFirstOutputVar(node)
    val interim = Util.getBP(node, "interim")._2

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {

        if (edge.endPort == "left") {
          leftNode = nodeDetails(edge.startNode).id
          leftInput = getVariableName(nodeDetails(edge.startNode), edge.startPort)

        } else {          // "right"
          rightNode = nodeDetails(edge.startNode).id
          rightInput = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        }
      }
    }
    val condition = Util.getSP(node, "joinCondition")._2
      .replaceAll(leftNode,  leftInput)
      .replaceAll(rightNode, rightInput)

    val joinType = Util.getSP(node, "joinType")._2

    s++= s"""val $out_val = $leftInput.join($rightInput, $condition, "$joinType")$cc\n"""

    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getLimit(n: GNode): Unit = {
    val node = nodeDetails(n.id)
    val s = new StringBuilder()
    val out_val = getFirstOutputVar(node)
    val interim = Util.getBP(node, "interim")._2

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        val limit = Util.getIP(node, "limit")._2
        s++= s"val $out_val = $input.limit($limit)\n"
      }
    }
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  // one.op(two)
  protected def getSimpleTwo(n: GNode, op : String): String = {
    val node     = nodeDetails(n.id)
    val s        = new StringBuilder()
    val out_val  = getFirstOutputVar(node)
    val interim  = Util.getBP(node, "interim")._2
    var oneInput = ""
    var twoInput = ""

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        if (edge.endPort == "one")
          oneInput = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        else             // "two"
          twoInput = getVariableName(nodeDetails(edge.startNode), edge.startPort)
      }
    }
    s++= s"val $out_val = $oneInput.$op($twoInput)$cc\n"
    if (interim)
      addInterimInfo(s, out_val, n.id)
    s++= "\n"
    s.toString()
  }
  protected def getExcept    (n: GNode) : Unit = cg.addCode("main", getSimpleTwo(n, "except"))
  protected def getIntersect (n: GNode) : Unit = cg.addCode("main", getSimpleTwo(n, "intersect"))
  protected def getUnionAll  (n: GNode) : Unit = cg.addCode("main", getSimpleTwo(n, "unionAll"))

  protected def getFileOutput(n: GNode): Unit = {
    val node        = nodeDetails(n.id)
    val s           = new StringBuilder()
    val fileType    = Util.getSP(node, "fileType")._2
    val fileSystem  = Util.getSP(node, "fileSystem")
    val credentials = Util.getSP(node, "credentials")
    val fileName    = Util.getSP(node, "fileName")._2
    val IsAwsS3     = fileSystem._1 && credentials._1
    if (IsAwsS3) {
      // we have an external file system and credentials for it
      assert(fileSystem._2 == "s3" && credentials._2 == "system")
      val sfs = new S3FileSystem()
      s++= s"""hadoopConf.set("fs.s3.awsAccessKeyId", "${aws.K}")"""
      s++= "\n  "
      s++= s"""hadoopConf.set("fs.s3.awsSecretAccessKey" ,"${aws.S}")"""
      s++= "\n  "
      s++= s"""AwsCommunication.deleteS3Object("${sfs.bucketName}", "${fileName.stripPrefix("s3://" + sfs.bucketName + "/")}")""".stripMargin
      s++= "\n\n"
    }

    fileType match {
      case "csv" =>
        for (edgeId <- n.inEdges) {
          val edge = edgeDetails(edgeId)
          if (!isDependencyEdge(edge)) {
            val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
            s++= s"""  $input.save("$fileName", "com.databricks.spark.csv")\n"""
          }
        }
      case "txt" =>
        assert(IsAwsS3)
        for (edgeId <- n.inEdges) {
          val edge = edgeDetails(edgeId)
          if (!isDependencyEdge(edge)) {
            val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
            s++= s"""  AwsCommunication.writeS3File("vizprog", "$fileName", $input.toString())\n"""
          }
        }
      case _ => assert(false, s"Don't know how to handle file type: $fileType")

    }
    cg.addCode("main", s.toString())
  }

  case class PipProperty(name: String,
                         text: String,
                         dataType: String)

  protected def getPipNode(n: GNode, op: String, props: List[PipProperty]): String = {
    val node      = nodeDetails(n.id)
    val out_val   = getFirstOutputVar(node)
    val s         = new StringBuilder()

    val commonProps  =
      PipProperty("inputColumn"   , "setInputCol"  , "String") ::
      PipProperty("outputColumn"  , "setOutputCol" , "String") ::
      PipProperty("labelColumn"   , "setLabelCol"  , "String") ::
      PipProperty("featuresColumn", "seFeaturesCol", "String") ::
      Nil
    val allProps = commonProps:::props
    for (prop <- allProps) {
      val nm = prop.name; val txt = prop.text; val dt = prop.dataType
      val propValue = dt match {
        case "String"      => val s = Util.getSP(node, nm)          ; if (!s._1) "" else s""".$txt("${s._2}")"""
        case "Integer"     => val s = Util.getIP(node, nm)          ; if (!s._1) "" else s""".$txt( ${s._2} )"""
        case "Double"      => val s = Util.getDP(node, nm)          ; if (!s._1) "" else s""".$txt( ${s._2} )"""
        case "StringList"  => val s = Util.getArrayListSP(node, nm) ; if (!s._1) "" else s""".$txt( ${s._2} )"""
        case "DoubleArray" => val s = Util.getArrayArrayDP(node, nm); if (!s._1) "" else s""".$txt( ${s._2} )"""
        case "Boolean"     => val s = Util.getBP(node, nm)          ; if (!s._1) "" else s""".$txt( ${s._2} )"""
        case _             => assert(false, "property datatype missing: " + dt); ""
      }
      s++= propValue
    }
    val ss = s"val $out_val = new $op()${s.toString()}\n"
    ss
  }
  protected def edgeToProperty(n: GNode, inPort: String, prop: String): Unit = {

  }

  protected def getTokenizer(n: GNode): Unit = {
    val s = getPipNode(n, "Tokenizer", Nil)
    cg.addCode("main", s)
  }
  protected def getAFTSurvivalRegression(n: GNode): Unit = {
    val properties =
      PipProperty("quantilesCol"         , "setQuantilesCol"         , "String"     ) ::
      PipProperty("quantileProbabilities", "setQuantileProbabilities", "DoubleArray") ::
      Nil
    val s = getPipNode(n, "AFTSurvivalRegression", properties)
    cg.addCode("main", s)
    cg.addImport("main", "org.apache.spark.ml.regression.AFTSurvivalRegression")
  }
  protected def getBinarizer(n: GNode): Unit = {
    val properties =
      PipProperty("threshold"   , "setThreshold", "Double") ::
      Nil
    val s = getPipNode(n, "Binarizer", properties)
    cg.addCode("main", s)
    cg.addImport("main", "org.apache.spark.ml.feature.Binarizer")
  }
  protected def getDCT(n: GNode): Unit = {
    val properties =
      PipProperty("inverse", "setInverse", "Boolean") ::
      Nil
    val s = getPipNode(n, "DCT", properties)
    cg.addCode("main", s)
    cg.addImport("main", "org.apache.spark.ml.feature.DCT")
  }
  protected def getBucketizer(n: GNode): Unit = {
    val properties =
      PipProperty("splits"      , "setSplits"   , "DoubleArray" ) ::
      Nil
    val s = getPipNode(n, "Bucketizer", properties)
    cg.addCode("main", s)
    cg.addImport("main", "org.apache.spark.ml.feature.Bucketizer")
  }
  protected def getChiSqSelector(n: GNode): Unit = {
    val properties =
      PipProperty("numTopFeatures", "setNumTopFeatures" , "Integer" ) ::
      Nil
    val s = getPipNode(n, "ChiSqSelector", properties)
    cg.addCode("main", s)
    cg.addImport("main", "org.apache.spark.ml.feature.ChiSqSelector")
  }
  protected def getHashingTF(n: GNode): Unit = {
    val properties =
      PipProperty("numFeatures" , "setNumFeatures", "Integer") ::
      Nil
    val s = getPipNode(n, "HashingTF", properties)
    cg.addCode("main", s)
  }
  protected def getIndexToString(n: GNode): Unit = {
    // convert label port to property
    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        if (edge.endPort == "label") {
          val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
          val node  = nodeDetails(n.id)
          node.properties("labels") = JString(input)
        }
      }
    }
    val properties =
      PipProperty("labels" , "setLabels", "String") ::
        Nil
    val s = getPipNode(n, "IndexToString", properties)
    cg.addCode("main", s)
  }
  protected def getStringIndexer(n: GNode): Unit = {
    // convert label port to property
    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        if (edge.endPort == "label") {
          val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
          val node  = nodeDetails(n.id)
          node.properties("labels") = JString(input)
        }
      }
    }
    val properties =
      PipProperty("numFeatures" , "setNumFeatures", "Integer") ::
        Nil
    val s = getPipNode(n, "HashingTF", properties)
    cg.addCode("main", s)
  }
  protected def getVectorIndexer(n: GNode): Unit = {
    val properties =
      PipProperty("numFeatures" , "setNumFeatures", "Integer") ::
        Nil
    val s = getPipNode(n, "HashingTF", properties)
    cg.addCode("main", s)
  }
  protected def getDecisionTreeClassifier(n: GNode): Unit = {
    val properties =
      PipProperty("numFeatures" , "setNumFeatures", "Integer") ::
        Nil
    val s = getPipNode(n, "HashingTF", properties)
    cg.addCode("main", s)
  }

  protected def getLogisticRegression(n: GNode): Unit = {
    val properties =
      PipProperty("maxIter"      , "setMaxIter"    , "Integer") ::
      PipProperty("regParam"     , "setRegParam"   , "Double" ) ::
      PipProperty("featureColumn", "setFeaturesCol", "String" ) ::
      Nil
    val s = getPipNode(n, "LogisticRegression", properties)
    cg.addCode("main", s)
  }

  protected def getParamGridBuilder(n: GNode): Unit = {
    val node = nodeDetails(n.id)
    val s    = new StringBuilder()

    val out_val      = getFirstOutputVar(node)
    val JArray(pm)   = node.properties("parameterMap")
    s++= s"val $out_val = new ParamGridBuilder()"
    for (p <- pm) {
      val JString(pNodeId) = p \ "node"
      val JObject(pProps)  = p \ "props"
      val node_val = getFirstOutputVar(nodeDetails(pNodeId), bypassCheck = true)
      val mProps = Util.jlistToMap(pProps)
      for ((k, v) <- mProps) {
        val JArray(vv) = v
        val values = s"Array(${Util.getListString(vv)})"
        s++= s".addGrid($node_val.$k, $values)"
      }
    }
    s++= s".build()\n"

    cg.addCode("main", s.toString())
  }

  protected def getScript(n: GNode): Unit = {
    val node      = nodeDetails(n.id)
    val language  = Util.getSP(node, "language")._2

    if (language == "scala") {
      val ms      = new StringBuilder()
      val pkg     = Util.getSP(node, "package")._2
      val interim = Util.getBP(node, "interim")._2
      val fname   = Util.getSP(node, "name")._2

      // Don't regenerate bodies of functions called multiple times
      val uniqueName = pkg + "." + fname
      if (! cg.generatedFunction.contains(uniqueName)) {
        cg.generatedFunction(uniqueName) = true

        var ps      = new StringBuilder()
        val body    = Util.getSP(node, "body")._2

        // Imports
        val JArray(imports) = node.properties("imports")
        for (imp <- imports) {
          val JString(importPackage) = imp
          cg.addImport(pkg, importPackage)
        }

        // Definitions used by node function (such as case class)
        val (haveDefs, theDefs) = Util.getSP(node, "definitions")
        if (haveDefs)
          cg.addDefinition(pkg, theDefs)

        // Function definition
        ps ++= s"\ndef $fname(sc: SparkContext"
        val JArray(inPorts) = node.ports("inputs")
        for (port <- inPorts) {
          val JString(arg) = port
          ps ++= s", $arg : DataFrame"
        }
        ps++= ") : DataFrame = {\n"

        ps++= "  val sqlContext = SQLContext.getOrCreate(sc)\n"
        ps++= "  import sqlContext.implicits._\n\n"

        // Function body
        val codeBody = StringContext.treatEscapes(body)
        ps ++= codeBody

        ps ++= "\n}\n"

        // Add Function to pkg
        cg.addCode(pkg, ps.toString())
      }

      // Call from main function
      val out_val = getFirstOutputVar(node)
      val objectName = cg.getObjectName(pkg)
      ms++= s"val $out_val = $objectName.$fname(sc"
      for (edgeId <- n.inEdges) {
        val edge = edgeDetails(edgeId)
        if (!isDependencyEdge(edge)) {
          val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
          ms++= s", $input"
        }
      }
      ms++=s""")$cc\n"""

      if (interim)
        addInterimInfo(ms, out_val, n.id)
      cg.addCode("main", ms.toString())
    }
  }

  protected def getDirectParameter(n: GNode): Unit = {
    // ignore
  }

  protected def getMLFeature(n: GNode): Unit = {
    // ignore
  }
  protected def getMLTune(n: GNode): Unit = {
    // ignore
  }
  protected def getMLAlgorithm(n: GNode): Unit = {
    // ignore
  }

  protected def getMLPipelineStatement(classificationPipeline: MLClassificationPipeline,
                                       mid_val: String) : String = {
    var i = 0
    val s = new StringBuilder()

    // STATEMENT 1:

    val features = classificationPipeline.featureNodes

    s++= s"""val $mid_val = new Pipeline().setStages(Array("""

    if (features != null) {
      for (stage <- features) {
        if (i > 0) s ++= ", "
        val stageNode = nodeDetails(stage)
        val stagePort = Util.getFirstOutPort(stageNode)
        val stageName = getVariableName(stageNode, stagePort)
        s++= stageName
        i = i + 1
      }

      if (classificationPipeline.algorithmNodeId != null) {
        if (i > 0) s ++= ", "
        val stageNode = nodeDetails(classificationPipeline.algorithmNodeId)
        val stagePort = Util.getFirstOutPort(stageNode)
        val stageName = getVariableName(stageNode, stagePort)
        s++= stageName
        i = i + 1
      }
    }
    s++= "))\n"

    s.toString()
  }

  protected def getBinaryClassificationEvaluator(n: GNode) : Unit = {
    val node     = nodeDetails(n.id)
    val out_val  = getFirstOutputVar(node)
    //val interim  = Util.getBP(node, "interim")._2
    val s        = new StringBuilder()

    val (found, metric) = Util.getSP(node, "metricName")

    if (! found) {
      cg.addCode("main", s"val $out_val = new BinaryClassificationEvaluator\n")
      return
    }

    val tmp_val = out_val + "_tmp_1"
    s++= s"""val $tmp_val = new BinaryClassificationEvaluator().setMetricName("$metric")\n"""
    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val eval_input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        s++= s"""val $out_val = $tmp_val.evaluate($eval_input)\n"""
      }
    }
    s++= "\n"
    cg.addCode("main", s.toString())
  }

  protected def getCrossValidator(n: GNode) : String = {
    val node    = nodeDetails(n.id)
    val out_val = getFirstOutputVar(node)

    var evaluatorInput : String = null
    var paramInput     : String = null
    var pipelineInput  : String = null
    var data_input_val : String = null

    val (hasFolds, pFolds) = Util.getIP(node, "folds")
    val numFolds = if (hasFolds) pFolds else 1

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        edge.endPort match {
          case "pipeline"  =>
            val pipelineLastNodeId   = edge.startNode
            val pipelineLastNodePort = edge.startPort

            removeRegisteredPendingFunction(pipelineLastNodeId, pipelineLastNodePort)

            val pip          = MLPipelines.getEnclosingMLPipelineNode(pipelineLastNodeId, wf).asInstanceOf[MLClassificationPipeline]
            val mlPipelineId = pip.rootNodeId
            val firstId      = pip.firstPipelineNodeId
            data_input_val   = getSingleEdgeInput(getOrderNode(firstId))
            pipelineInput    = getVariableName(nodeDetails(mlPipelineId), "out")

          case "evaluator" =>
            evaluatorInput   = getVariableName(nodeDetails(edge.startNode), edge.startPort)

          case "params"    =>
            paramInput       = getVariableName(nodeDetails(edge.startNode), edge.startPort)

          case _           => assert(false, s"Unknown port ${edge.endPort}")
        }
      }
    }
    assert(evaluatorInput != null && paramInput != null && pipelineInput != null && data_input_val != null,
      s"evaluatorInput = $evaluatorInput && paramInput = $paramInput && pipelineInput = $pipelineInput && data_input_val = $data_input_val")

    cg.addCode("main",
      s"""val $out_val = new CrossValidator().
                  setEstimator($pipelineInput).
                  setEvaluator($evaluatorInput).
                  setEstimatorParamMaps($paramInput).
                  setNumFolds($numFolds)
""")

    val pendingCode : (String) => String = new_output => s"""val $new_output = $out_val.fit($data_input_val)\n"""
    registerPendingCode(n.id, "out", pendingCode)

    out_val
  }


  protected def getMLPipeline(n: GNode): Unit = {
    val node         = nodeDetails(n.id)
    val pipeline     = pipelines(n.id)
    val out_val      = getVariableName(node, "out")
    val pip          = pipeline.asInstanceOf[MLClassificationPipeline]
    val statementOne = getMLPipelineStatement(pip, out_val)              // this is the pipeline array

    cg.addCode("main", statementOne)

    // STATEMENT 2:

    val firstId = pip.firstPipelineNodeId
    val lastId  = pip.lastPipelineNodeId

    val lastPipelineNode       = nodeDetails(lastId)
    val lastNodeOutPort        = Util.getFirstOutPort(lastPipelineNode)

    val firstPipelineGraphNode = getOrderNode(firstId)
    val data_input_val         = getSingleEdgeInput(firstPipelineGraphNode)

    val pendingCode : (String) => String = new_output => s"""val $new_output = $out_val.fit($data_input_val)\n"""
    registerPendingCode(lastId, lastNodeOutPort, pendingCode)
  }

  protected def getModelApplier(n: GNode): Unit = {
    val node       = nodeDetails(n.id)
    val interim    = Util.getBP(node, "interim")._2
    val out_val    = getFirstOutputVar(node)
    var modelInput : String = null
    var dataInput  : String = null

    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        if (edge.endPort == "model")
          modelInput = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        else             // "train_data"
          dataInput = getVariableName(nodeDetails(edge.startNode), edge.startPort)
      }
    }
    assert(modelInput != null && dataInput != null, "model applier doesn't have correct ")

    val s = s"""val $out_val = $modelInput.transform($dataInput)$cc\n"""
    cg.addCode("main", s)
    if (interim) {
      val ms = new StringBuilder()
      addInterimInfo(ms, out_val, n.id)
      cg.addCode("main", ms.toString())
    }
  }

  protected def getModelSaver(n: GNode): Unit = {
    val node      = nodeDetails(n.id)
    val (found, modelFile) = Util.getSP(node, "fileName")

    assert(found, "You need to provide storedModel property")

    for (edgeId  <- n.inEdges) {
      val edge    = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val s     = new StringBuilder()
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        s++= s"""$input.write.overwrite().save(""""
        s++= modelFile
        s++= "\")\n"
        cg.addCode("main", s.toString())
      }
    }
  }

  protected def getApplyStoredModel(n: GNode): Unit = {
    val node         = nodeDetails(n.id)
    val s            = new StringBuilder()
    val out_val      = getFirstOutputVar(node)
    val (is1, modelType)    = Util.getSP(node, "modelType")
    val (is2, fileName)     = Util.getSP(node, "fileName")
    val (is3, fileSystem)   = Util.getSP(node, "fileSystem")
    val (is4, credentials)  = Util.getSP(node, "credentials")

    assert(is1 && is2 && is3 && is4, "Apply Stored Model missing properties")
    assert(fileSystem == "s3" && credentials == "system", "Only system s3 credentials allowed")

    s++= "\n"
    s++= s"""object TrainedModel {"""
    s++= "\n  "
    s++= s"""var model:  $modelType = null"""
    s++= "\n  "
    s++= s"""def loadModel(sc : SparkContext): Unit = {"""
    s++= "\n    "
    s++= s"""val hadoopConf = sc.hadoopConfiguration"""
    s++= "\n    "
    s++= s"""hadoopConf.set("fs.s3.awsAccessKeyId", "${aws.K}")"""
    s++= "\n    "
    s++= s"""hadoopConf.set("fs.s3.awsSecretAccessKey" ,"${aws.S}")"""
    s++= "\n\n    "
    s++= s"""model = PipelineModel.load("$fileName")  """
    s++= "\n  }\n}"

    cg.addDefinition("main", s.toString())

    var input : String = null
    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge))
        input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
    }
    assert(input != null, "Cannot find incoming edge")

    cg.addCode("main", s"    val $out_val = TrainedModel.model.transform($input)\n")
  }

  protected def openRestIOFile() : Unit = {
    if (!cg.hasFile("restio")) cg.addCode("restio", "\nobject IO {\n")
  }
  protected def closeRestIOFile() : Unit = {
    assert(cg.hasFile("restio"))
    cg.addCode("restio", "\n}\n")
  }
  protected def getRestInput(n: GNode): Unit = {
    val node         = nodeDetails(n.id)
    val sCaseClass   = new StringBuilder()
    val sInputWrites = new StringBuilder()
    val sInputReads  = new StringBuilder()
    val out_val      = getFirstOutputVar(node)

    // From schema create the case classes and the implicit json converters
    val JArray(columnArray) =  node.properties("schema")

    var i = 0
    sCaseClass   ++= "case class Input ("
    sInputWrites ++= "implicit val InputWrites: Writes[Input] = ("
    sInputReads  ++= "implicit val InputReads: Reads[Input] = ("

    for (JArray(oneColumn) <- columnArray) {

      assert(oneColumn.length == 2, "Expecting column name and datatype per column")

      if (i > 0) {
        sCaseClass   ++= ","
        sInputWrites ++= " and "
        sInputReads  ++= " and "
      }

      val JString(columnName) = oneColumn.head
      val JString(columnType) = oneColumn.tail.head
      val scalaColumnType     = cg.sparkTypeToScalaType(columnType)
      sCaseClass   ++= "\n"
      sInputWrites ++= "\n"
      sInputReads  ++= "\n"
      sCaseClass   ++= s"""        $columnName: $scalaColumnType"""
      sInputWrites ++= s"""        (JsPath \\ "$columnName"  ).write[$scalaColumnType] """
      sInputReads  ++= s"""        (JsPath \\ "$columnName"  ).read[$scalaColumnType] """
      i = i + 1
    }
    sCaseClass   ++= "\n   )\n"
    sInputWrites ++= "\n   ) (unlift(Input.unapply))\n"
    sInputReads  ++= "\n   ) (Input.apply _)\n"

    // Write this to restio file, they need to be in scope for implicit conversions
    // done by the controller, for now it'll end up in the same file
    openRestIOFile()
    cg.addCode("restio", sCaseClass.toString())
    cg.addCode("restio", sInputWrites.toString())
    cg.addCode("restio", sInputReads.toString())


    cg.addCode("main","  def runRequest(theInput: IO.Input) : Future[IO.Output] = { Future {\n")
    cg.addCode("main","    val sqlContext = SQLContext.getOrCreate(sc)\n")
    cg.addCode("main","    import sqlContext.implicits._\n")
    cg.addCode("main",s"    val $out_val = Seq(theInput).toDF()\n")
  }

  protected def getRestOutput(n: GNode): Unit = {
    val node         = nodeDetails(n.id)
    val sCaseClass   = new StringBuilder()
    val sInputWrites = new StringBuilder()
    val sInputReads  = new StringBuilder()
    val sSelect      = new StringBuilder()
    val tSelect      = new StringBuilder()
    val uSelect      = new StringBuilder()

    // From schema create the case classes and the implicit json converters
    val JArray(columnArray) =  node.properties("schema")

    var i = 0
    sCaseClass   ++= "case class Output ("
    sInputWrites ++= "implicit val OutputWrites: Writes[Output] = ("
    sInputReads  ++= "implicit val OutputReads: Reads[Output] = ("

    for (JArray(oneColumn) <- columnArray) {

      assert(oneColumn.length == 2, "Expecting column name and datatype per column")

      if (i > 0) {
        uSelect      ++= ","
        tSelect      ++= ","
        sSelect      ++= ","
        sCaseClass   ++= ","
        sInputWrites ++= " and "
        sInputReads  ++= " and "
      }

      val JString(columnName) = oneColumn.head
      val JString(columnType) = oneColumn.tail.head
      val scalaColumnType     = cg.sparkTypeToScalaType(columnType)

      uSelect      ++= s""" $columnName """
      sSelect      ++= s""" "$columnName" """
      tSelect      ++= s""" $columnName: $scalaColumnType """
      sCaseClass   ++= "\n"
      sInputWrites ++= "\n"
      sInputReads  ++= "\n"
      sCaseClass   ++= s"""        $columnName: $scalaColumnType"""
      sInputWrites ++= s"""        (JsPath \\ "$columnName"  ).write[$scalaColumnType] """
      sInputReads  ++= s"""        (JsPath \\ "$columnName"  ).read[$scalaColumnType] """
      i = i + 1
    }
    sCaseClass   ++= "\n   )\n"
    sInputWrites ++= "\n   ) (unlift(Output.unapply))\n"
    sInputReads  ++= "\n   ) (Output.apply _)\n"

    // Write this to restio file, they need to be in scope for implicit conversions
    // done by the controller, for now it'll end up in the same file
    cg.addCode("restio", sCaseClass.toString())
    cg.addCode("restio", sInputWrites.toString())
    cg.addCode("restio", sInputReads.toString())
    closeRestIOFile()

    var input : String = null
    for (edgeId <- n.inEdges) {
      val edge = edgeDetails(edgeId)
      if (!isDependencyEdge(edge))
        input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
    }
    assert(input != null, "Cannot find incoming edge")

    val columnList         = sSelect.toString()
    val typedColumnList    = tSelect.toString()
    val unquotedColumnList = uSelect.toString()

    cg.addCode("main",s"    val selectedDF = $input.select($columnList)\n")
    cg.addCode("main",s"    val dataframeRow = selectedDF.collect()(0)\n")
    cg.addCode("main",s"    val Row($typedColumnList) = dataframeRow\n")
    cg.addCode("main",s"    IO.Output($unquotedColumnList) }}\n")
  }

  protected def getSelect(n: GNode): Unit = {
    val node     = nodeDetails(n.id)
    val interim  = Util.getBP(node, "interim")._2
    val out_val  = getFirstOutputVar(node)
    for (edgeId <- n.inEdges) {
      val edge   = edgeDetails(edgeId)
      if (!isDependencyEdge(edge)) {
        val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
        val JArray(columns) = node.properties("columns")
        val s = new StringBuilder()
        s++= s"""val $out_val = $input.select("""
        var i = 0
        for (col <- columns) {
          val JString(c) = col
          if (i > 0)
            s++= ","
          s++= s""" "$c" """
          i = i + 1
        }
        s++= ")\n"
        if (interim)
          addInterimInfo(s, out_val, n.id)
        cg.addCode("main", s.toString())
      }
    }
  }

  protected def getDatasetS3URL(bucket: String, user: String, project: String, dataset: String): (String, String) = {
    (s"s3://$bucket/users/$user/$project/datasets/$dataset.parquet",
      s"users/$user/$project/datasets/$dataset.json")
  }

  protected def getWriteDataset(n: GNode) : Unit = {
    val node        = nodeDetails(n.id)
    val s           = new StringBuilder()
    val fileType    = Util.getSP(node, "fileType"    )
    val project     = Util.getSP(node, "project"     )
    val user        = Util.getSP(node, "user"        )
    val dataset     = Util.getSP(node, "dataset"     )
    val fileSystem  = Util.getSP(node, "fileSystem"  )
    val credentials = Util.getSP(node, "credentials" )

    if (fileSystem._1 && fileSystem._2 == "s3") {

      assert(credentials._1 && project._1 && user._1 && dataset._1) // don't handle other cases

      val sfs = new S3FileSystem()

      if (credentials._1 && credentials._2 == "system") {
        s++= s"""hadoopConf.set("fs.s3.awsAccessKeyId", "${aws.K}")"""
        s++= "\n  "
        s++= s"""hadoopConf.set("fs.s3.awsSecretAccessKey" ,"${aws.S}")"""
        s++= "\n\n"
      }

      for (edgeId <- n.inEdges) {
        val edge  = edgeDetails(edgeId)
        if (!isDependencyEdge(edge)) {
          val input = getVariableName(nodeDetails(edge.startNode), edge.startPort)
          val (s3File, s3SchemaFile) = getDatasetS3URL(sfs.bucketName, user._2, project._2, dataset._2)
          s++= "\n"
          s++= s"""  AwsCommunication.deleteS3Object("${sfs.bucketName}", "${s3File.stripPrefix("s3://" + sfs.bucketName + "/")}")""".stripMargin

          s++= "\n\n"
          s++= s"""  $input.write.format("${fileType._2}").save("$s3File")"""
          s++= "\n\n"
          // we also need to write interim data to S3
          s++=
            s"""  AwsCommunication.deleteS3Object("${sfs.bucketName}", "${s3SchemaFile.stripPrefix("s3://" + sfs.bucketName + "/")}")""".stripMargin
          s++= "\n\n"
          val is = "interimString"
          s++= s"""  val $is = getInterimString($input)"""
          s++= "\n"
          s++= s"""  AwsCommunication.writeS3File("${sfs.bucketName}", "$s3SchemaFile", $is)"""
          s++= "\n"
        }
      }
    }
    cg.addCode("main", s.toString())
  }
}

