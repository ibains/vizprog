package vizprog.codegen

import scala.collection.mutable.Map
import vizprog.common._

class WorkflowSpecification {

  def generateSpecification() : String = {

    val exprTypes = new ExpressionTypes()
    val nodeSpec = new NodeSpecification()
    val wp = new WorkflowPrinter()

    val nns = Map[String, Node]()
    val ees = Map[String, Edge]()
    nodeSpec.buildNodes(nns)

    val exprs = exprTypes.printExpression
    val dummyWf = Workflow("","",null,nns,ees, null, null, null)
    val nodes = wp.printWorkflow(dummyWf)
    exprs + nodes
  }
}
