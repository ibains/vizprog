package vizprog.codegen

class SparkHelpers {

  def getSchemaToJson: String =
    """
  def schemaToJson(cols: Array[(String, String)]): String = {
    var i = 0
    val sb = new StringBuffer("\"schema\": {\n ")
    for ((colName, colType) <- cols) {
      if (i > 0)
        sb.append(",")
      sb.append("\"")
      sb.append(colName)
      sb.append("\" : \"")
      sb.append(colType)
      sb.append("\"")
      i = i + 1
    }
    sb.append("\n}\n")
    sb.toString
  }
"""

  def getSampleToCsv :String =
    """
  def columnToString(item: Any) : String = {
      val str = item match {
          case a: Seq[String] => a.mkString("[", ",", "]")
          case x => x.toString()
      }
      "\"" + escapeJava(str) + "\""
  }
  def sampleToCsv(rows: Array[org.apache.spark.sql.Row]) = {
    val sb = new StringBuffer()
    for (row <- rows) {
      val itemSeq = row.toSeq
      0 to (row.size-1) foreach { x => if (x > 0) sb.append(",") ; sb.append(columnToString(itemSeq(x)))}
      sb.append("\n")
    }
    sb.toString
  }
"""

  def getTakeLastNth :String =
    """
  val take_last_nth = udf( (a: Seq[String], b: Int) => { a.takeRight(b).head } )
  val take_nth      = udf( (a: Seq[String], b: Int) => { a.apply(b) } )
"""

  def getStringToFile :String =
    """
  def stringToFile(value: String, file: String) : Unit = {
    import java.io._
    val pw = new PrintWriter(new File(file))
    pw.write(value)
    pw.close
  }
"""

  def getDataFrameStats :String =
    """
  import java.io._

  def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
    val p = new java.io.PrintWriter(f)
    try { op(p) } finally { p.close() }
  }

  def isNumericType(s: String) : Boolean = {
    val isNumeric = s match {
      case "DecimalType" => true
      case "DoubleType"  => true
      case "FloatType"   => true
      case "IntegerType" => true
      case "LongType"    => true
      case _             => false
    }
    isNumeric
  }

  def getNumericSubset(df: DataFrame) : DataFrame = {
    var aa : List[org.apache.spark.sql.Column] = Nil
    for ((kCol, kType) <- df.dtypes) {
      if (isNumericType(kType))
        aa = df(kCol)::aa
    }
    val cols = aa.reverse
    val numericCols = df.select(cols:_*)
    numericCols
  }

  def getRange(min: Double, max: Double) : Array[Double] = {
    val r = new Array[Double](11)
    val increment = (max - min) / 10
    0 to 10 foreach { i => r(i) = min + i * increment }
    r
  }

  def getNumNull(df: DataFrame) : String = {
    var aa : List[org.apache.spark.sql.Column] = Nil
    for ((kCol, kType) <- df.dtypes) {
        val expr = kType match {
            case "StringType" => 
                    sum(when(df(kCol).isNull, 1).
                        when(df(kCol).equalTo(""), 1).
                        otherwise(0)) as kCol
            case _ =>
                    sum(when(df(kCol).isNull, 1).
                        otherwise(0)) as kCol
        }
        aa = (expr) :: aa
    }
    val cols = aa.reverse
    val nullCounts = df.select(cols:_*)
    nullCounts.toJSON.collect()(0)
  }

  def getColumnBucketCounts(df: DataFrame, col: String, tenBuckets: Array[Double]) : String = {
    val bucketIds = df.select(
      when(df(col) < tenBuckets(1), 1).
      when(df(col) < tenBuckets(2), 2).
      when(df(col) < tenBuckets(3), 3).
      when(df(col) < tenBuckets(4), 4).
      when(df(col) < tenBuckets(5), 5).
      when(df(col) < tenBuckets(6), 6).
      when(df(col) < tenBuckets(7), 7).
      when(df(col) < tenBuckets(8), 8).
      when(df(col) < tenBuckets(9), 9).
        otherwise(10) as "bucket")
    val lcounts = bucketIds.groupBy("bucket").count()
    val counts = lcounts.collect()
    val sb = new StringBuilder()
    sb ++= "  \"" + col + "\" : ["
    0 to 9 foreach { i =>
      if (i > 0) sb ++= ", "
      var buckCount : Long = 0
      for (a <- counts) if (a.getInt(0) == i + 1) buckCount = a.getLong(1)
      sb ++= "\n  { \"min\" : " + tenBuckets(i) + ", \"max\" : " + tenBuckets(i+1) + ", \"count\" : " + buckCount + " }"
    }
    sb ++= "\n  ]\n"
    sb.toString()
  }

  def getAllColumnBucketCounts(df: DataFrame) : String = {
    val cols = df.columns
    val desc = df.describe(cols: _*).collect()
    val minRow = desc(3)
    val maxRow = desc(4)
    val sb = new StringBuilder()
    sb ++= "{\n"
    1 to minRow.size-1 foreach { i =>
      if (i > 1) sb ++= ", "
      sb ++= getColumnBucketCounts(df, cols(i-1), getRange(minRow.getString(i).toDouble, maxRow.getString(i).toDouble))
    }
    sb ++= "\n}\n"
    sb.toString()
  }

  def getTopN(df: DataFrame): String = {
    val cols = df.columns
    val sb = new StringBuffer()
    sb.append("\"topN\": {")
    var j = 0
    for (col <- cols) {
      j = j + 1
      if (j > 1)
        sb.append(",\n")
      val c = df.groupBy(df(col)).count().toDF(col, "num_values")
      val sorted = c.orderBy(c("num_values").desc)
      val rows = sorted.take(20)
      var i = 0
      sb.append("\"" + col + "\": [ \n")
      for (row <- rows) {
        i = i + 1
        if (i > 1)
          sb.append(",\n")
        val items = row.toSeq
        sb.append(" { \"value\":")
        sb.append(columnToString(items(0)))
        sb.append(", \"count\":")
        sb.append(items(1))
        sb.append(" }")
      }
      sb.append("\n]")
    }
    sb.append("}")
    sb.toString()
  }

  def writeDataFrameStats(inDf: DataFrame,
                          histogramFile : String,
                          statsFile     : String,
                          cleanseFile   : String,
                          topnFile      : String) : Unit =
  {
    val cleanseJson = getNumNull(inDf)
    val cleanseF = new File(cleanseFile)
    printToFile(cleanseF) { p => p.println(cleanseJson) }

    val topNJson = getTopN(inDf)
    val topnF = new File(topnFile)
    printToFile(topnF) { p => p.println(topNJson) }

    val df = getNumericSubset(inDf)

    val histogramJson = getAllColumnBucketCounts(df)
    val histFile = new File(histogramFile)
    printToFile(histFile) { p => p.println(histogramJson) }

    val stringColumns = df.columns
    val decStatsJson = df.describe(stringColumns: _*).toJSON.collect()
    val statFile = new File(statsFile)
    printToFile(statFile) { p =>
      p.print("[\n")
      var i = 0
      for (t <- decStatsJson) {
        if (i > 0) p.print(",\n  ") else p.print("  ")
        p.print(t)
        i = i + 1
      }
      p.print("\n]\n")
    }
  }

  def writeInterimInfo(df: DataFrame, filePrepend: String) : Unit = {
    val df_schema = schemaToJson(df.dtypes)
    val df_sample = sampleToCsv(df.take(20))
    stringToFile(df_schema, filePrepend + ".json")
    stringToFile(df_sample, filePrepend + ".csv")
    writeDataFrameStats(df,
      filePrepend + "_histogram.json",
      filePrepend + "_stats.json",
      filePrepend + "_cleanse.json",
      filePrepend + "_topn.json")
  }

"""

  def getDirectInterimFunctions =
    """
  def sampleToJson(rows: Array[org.apache.spark.sql.Row]) : String = {
    val sb = new StringBuffer()
    var i = 0
    sb.append("\"data\": { \n")
    for (row <- rows) {
      i = i + 1
      if (i > 1)
        sb.append(",\n")
      sb.append("  \"Row" + i + "\":[")
      val itemSeq = row.toSeq
      0 to (row.size-1) foreach { x => if (x > 0) sb.append(",") ; sb.append("\""+ escapeJava(itemSeq(x).toString()) + "\"")}
      sb.append("]")
    }
    sb.append("\n}")
    sb.toString
  }

  def getDescribeStats(df: DataFrame) : String = {
    val stringColumns = df.columns
    val decStatsJson = df.describe(stringColumns: _*).toJSON.collect()
    val sb = new StringBuilder()
    sb++= "\"stats\":{"
    var i = 0
    for (t <- decStatsJson) {
      val start = if (i > 0) ",\n" else "\n"
      val oneStatForColumns = JSON.parseFull(t)
      val oneStatMap = oneStatForColumns.get.asInstanceOf[Map[String, Any]]
      val oneStatName = oneStatMap("summary")
      sb++= start + "\"" + oneStatName + "\":" + t
      i = i + 1
    }
    sb++= "},\n"
    sb.toString()
  }

  def getNumericStats(inDf: DataFrame) : String = {
    val cleanseExpr = getNumNull(inDf)
    val cleanseJson = "\"nulls\":" + cleanseExpr + ",\n"

    val df = getNumericSubset(inDf)
    val histogramExpr = getAllColumnBucketCounts(df)
    val histogramJson = "\"histogram\":" + histogramExpr + ","

    val descJson = getDescribeStats(inDf)
    descJson + histogramJson + cleanseJson
  }

  def getInterimString(df: DataFrame) : String = {
    val sb = new StringBuffer()
    sb.append("{\n")
    sb.append(schemaToJson(df.dtypes))
    sb.append(",\n")
    sb.append(getNumericStats(df))
    sb.append(sampleToJson(df.take(40)))
    sb.append("\n}\n")
    sb.toString()
  }
"""
  def getAWSWritingFunctions =
  """
object AwsCommunication {
  import java.io.{File, PrintWriter}
  val awsCredentials = new BasicAWSCredentials ("AWS_KEY",
                                                "AWS_SECRET")
  val awsClient = new AmazonS3Client (awsCredentials)
  def writeLocalFile(path: String, fc: String): Unit = new PrintWriter(path) { write(fc); close }
  def deleteFile(path: String) : Boolean = new File(path) delete
  def createDirectory(path: String) : Boolean = new File(path) mkdirs
  def hasDirectory(path: String) : Boolean = new File(path) exists

  protected def deleteS3Directory(bucketName: String, path: String) : Boolean = {
    awsClient.listObjects(bucketName, path).getObjectSummaries.map { file =>
      awsClient.deleteObject(bucketName, file.getKey)
    }
    true
  }

  def deleteS3Object(bucketName: String, path: String) : Boolean = { deleteS3Directory(bucketName, path) }
  def writeS3File(bucket: String, path: String, fc: String) : Unit = {

    val tmpPlace = "/tmp/vizprog/"
    if (!hasDirectory(tmpPlace))
      createDirectory(tmpPlace)
    val localFileName = tmpPlace+ "tmp.txt"
    writeLocalFile(localFileName, fc)
    awsClient.putObject(bucket, path, new File(localFileName))
    val acl = awsClient.getObjectAcl (bucket, path)
    acl.grantPermission (GroupGrantee.AllUsers, Permission.Read)
    awsClient.setObjectAcl(bucket, path, acl)
    deleteFile(localFileName)
  }
}
"""

  def getPlayDeployController =
    """
  @Singleton
  class ServeController @Inject() extends Controller {

    var model: SparkModel = null

    def init() = {
      model = new SparkModel
      model.connectAndCreateSession()

      Logger.info("\n\n    init: connectAndCreateSession done")

      model.loadModel()

      Logger.info("\n\n    init: loadModel done. Initialization complete")
    }

    init()

    def index = Action {
      Ok("Your service is up, hit the /model endpoint")
    }

    def runModel = Action.async { implicit request =>

      request.body.asJson.map { json =>

        Logger.info("Got request: " + json)

        json.validate[IO.Input].map {

          case theInput => {

            Logger.info("Validated input: " + theInput)

            model.runRequest(theInput).map {

              case theOutput =>

                val result = Json.toJson(theOutput)
                Logger.info("Scored output: " + theOutput)
                Ok(result)

            }.recover {

              case ex: Exception  =>
                Logger.error(s"Failed to run model", ex)
                InternalServerError(
                  Json.obj(
                    "status"    -> "KO",
                    "message"   -> s"Failed to run model, cause: ${ex.getMessage}",
                    "traceback" -> ModelUtils.toJson(ex)
                  )
                )
            } // recover
          } // case input
        } // map
          .recoverTotal { errors =>
          Future.successful[Result](
            BadRequest(
              Json.obj(
                "status" -> "KO",
                "message" -> JsError.toJson(errors)
              )
            )
          )
        }
      }.getOrElse(Future.successful[Result](BadRequest))
    }

  }

  object Implicits {
    implicit object JsErrorJsonWriter extends Writes[JsError] {
      def writes(o: JsError): JsValue = Json.obj(
        "errors" -> JsArray(
          o.errors.map {
            case (path, validationErrors) =>
              Json.obj(
                "path" -> Json.toJson(path.toString()),
                "validationErrors" -> JsArray(
                  validationErrors.map(validationError =>
                    Json.obj(
                      "message" -> JsString(validationError.message),
                      "args" -> JsArray(validationError.args.map(_ match {
                        case x: Int => JsNumber(x)
                        case x => JsString(x.toString)
                      }))
                    )
                  )
                )
              )
          }
        )
      )
    }
  }

  object ModelUtils {
    def toJson(th: Throwable): JsValue = Json.obj(
      "errorStackTrace" -> Json.obj(
        "errorMessage" -> JsString(th.getMessage),
        "stackTrace" -> JsArray(
          th.getStackTrace.toList.map { st =>
            Json.obj(
              "file" -> JsString(st.getFileName),
              "class" -> JsString(st.getClassName),
              "method" -> JsString(st.getMethodName),
              "line" -> JsNumber(st.getLineNumber)
            )
          })
      )
    )
  }
  """
}
