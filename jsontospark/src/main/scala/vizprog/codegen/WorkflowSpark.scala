package vizprog.codegen

import vizprog.common._

// STATEFUL, CREATE NEW PER COMPILATION

// ALL TARGET DEPEND CHANGES SHOULD ONLY BE IN THIS FILE

class WorkflowSpark {
  var nodeSpark : NodeSpark = null
  var cg        : CGBuilder = null


  protected def addInteractiveCall(graphId: String) : Unit = {
    // The call is outside the object, so it needs to use the object name
    cg.addCode("main", s"""\n\n${graphId}.${graphId}Graph()\n""")
  }


  protected def addMainCall(graphId: String) : Unit = {
    cg.addCode("main",
      s"""\n
def main(args: Array[String]): Unit = {
  ${graphId}Graph()
}
""")
  }


  protected def addSparkJobCall(graphId: String) : Unit = {
      cg.addCode("main", s"""

  override def runJob(sc: SparkContext, jobConfig: Config): Any =
  {
    ${graphId}Graph(sc,jobConfig)
  }

  override def validate(sc: SparkContext, config: Config): SparkJobValidation =
  {
    SparkJobValid
  }
""")
  }

  def generateScalaCode(wf: Workflow, tmpRoot: String) = {
    val graphId = wf.workFlowId
    val helpers = new SparkHelpers()

    cg.target match {
      case CGJobServerBatch => cg.addPackage("main", "package sparkwf.job\n")
      case CGDeploy => cg.addPackage("main", "package controllers\n")
      case _ => // nothing
    }

    nodeSpark.addStandardImports("main") // IDEMPOTENT

    cg.target match {
      case CGJobServerBatch => cg.addCode("main", s"\nobject App extends SparkJob {\n")
      case CGDeploy => // do nothing
      case _ => cg.addCode("main", s"\nobject $graphId {\n")
    }

    cg.target match {
      case CGDeploy =>
        cg.addDefinition("main", helpers.getPlayDeployController) // FUNCTION - PRIMARY
        cg.addDefinition("main", helpers.getAWSWritingFunctions) // FUNCTION - HELPER
      case _ =>
        cg.addCode("main", helpers.getSchemaToJson) // FUNCTION - HELPER
        cg.addCode("main", helpers.getSampleToCsv) // FUNCTION - HELPER
        cg.addCode("main", helpers.getStringToFile) // FUNCTION - HELPER
        cg.addCode("main", helpers.getDataFrameStats) // FUNCTION - HELPER
        cg.addCode("main", helpers.getTakeLastNth) // FUNCTION - HELPER
        cg.addCode("main", helpers.getDirectInterimFunctions) // FUNCTION - HELPER
        cg.addDefinition("main", helpers.getAWSWritingFunctions) // FUNCTION - HELPER
    }

    cg.target match {
      case CGJobServerBatch => cg.addCode("main", s"\ndef ${graphId}Graph(sc: SparkContext, jobConfig: Config): Unit = {\n")
      case CGDeploy => cg.addCode("main",
        """
class SparkModel {
  var sc : SparkContext = null

def connectAndCreateSession(): Unit = { """)
      case _                => cg.addCode("main", s"\ndef ${graphId}Graph(): Unit = {\n\n")
    }

    cg.target match {
      case CGDeploy =>
        cg.addCode("main", s"""
  val conf = new SparkConf().setAppName("$graphId")
                            .set("spark.sql.shuffle.partitions","4")
                            .set("spark.default.parallelism","4")
                            .set("spark.ui.port","40440")
                            .setMaster("local[4]")
  sc   = new SparkContext(conf)
""")
      case CGSparkSubmit =>
        cg.addCode("main", s"""val conf = new SparkConf().setAppName("$graphId")""")
        cg.addCode("main", s""".set("spark.sql.shuffle.partitions","4")""")
        cg.addCode("main", s""".set("spark.default.parallelism","4")\n""")
        cg.addCode("main", s"""val sc         = new SparkContext(conf)\n""")
      case _ =>
        // spark context is already available on Zeppelin and SparkJob, do nothing
    }

    cg.addCode("main", "val sqlContext = new org.apache.spark.sql.SQLContext(sc)\n\n")
    cg.addCode("main", "val hadoopConf = sc.hadoopConfiguration\n")
    cg.addCode("main", """hadoopConf.set("fs.s3.impl", "org.apache.hadoop.fs.s3native.NativeS3FileSystem")""")
    cg.addCode("main", "\n\n")
    cg.addCode("main", s"""if (!AwsCommunication.hasDirectory("${tmpRoot}")) AwsCommunication.createDirectory("${tmpRoot}")\n""")

    if (cg.target == CGDeploy)
      cg.addCode("main","""
} // close connectAndCreateSession

    def loadModel(): Unit = TrainedModel.loadModel(sc)

""")

    // ADD PER NODE CODE TO CG
    nodeSpark.generateScala()

    if (cg.target != CGDeploy)
    cg.addCode("main", "\n}")                                                // FUNCTION - GRAPH - END

    cg.target match {
      case CGDeploy =>
        cg.addCode("main", "\n}")                                            // OBJECT - END

      case CGSparkSubmit =>
        addMainCall(graphId)
        cg.addCode("main", "\n}")                                            // OBJECT - END, after call

      case CGInteractiveFunction =>
        cg.addCode("main", "\n}")                                            // OBJECT - END, before call
        addInteractiveCall(graphId)

      case CGJobServerBatch =>
        addSparkJobCall(graphId)
        cg.addCode("main", "\n}")                                            // OBJECT - END, after call

      case _ =>
        assert(false, "Bad target: " + cg.target)
        // should never reach here :)
    }
  }

  def generateSparkForWorkflow(mode: Int, wf: Workflow, tmpRoot: String) : String = {
    nodeSpark = new NodeSpark(wf, tmpRoot)

    // Function Pointer - bad here. keep for now
    cg  = new CGBuilder(nodeSpark.addStandardImports)
    cg.setTarget(mode)
    cg.setIntermediateResults(true)  // depends on individual node properties, but we can switch off universally

    nodeSpark.setCodeGeneration(cg)

    // ADD CODE TO CG
    generateScalaCode(wf, tmpRoot)

    // RETURN THE GENERATED CODE
    cg.getCode
  }

  def getInterimRequest(wf: Workflow, tmpRoot: String) : String = {
    val nodePrinter = new NodePrinter
    val nodeRequest = nodePrinter.generateNodeRequest(wf.workFlowId, wf.allNodes, tmpRoot)
    nodeRequest
  }
}
