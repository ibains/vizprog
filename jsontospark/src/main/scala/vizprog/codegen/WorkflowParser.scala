package vizprog.codegen

import org.json4s._
import org.json4s.native.JsonMethods._
import vizprog.common._
import scala.collection.mutable.Map

class WorkflowParser {

  protected def getMetaInfo(gg: JValue)   : (String, BigInt, BigInt, String, String) = {
    val metaInfo          = gg       \ "metainfo"
    val JString(id)       = metaInfo \ "id"
    val JInt(memory)      = metaInfo \ "memory"
    val JInt(processors)  = metaInfo \ "processors"
    val JString(cluster)  = metaInfo \ "cluster"
    val JString(mode)     = metaInfo \ "mode"
    (id, memory, processors, cluster, mode)
  }

  def getWorkflowId(sourceString: String) : String = {
    val gg = parse(sourceString) \ "graph"
    val metaInfo    = getMetaInfo(gg)
    metaInfo._1
  }
  def getWorkflowMode(sourceString: String) : String = {
    val gg = parse(sourceString) \ "graph"
    val metaInfo    = getMetaInfo(gg)
    metaInfo._5
  }

  def parseWorkflow(sourceString: String) : Workflow = {
    val nodeParser = new NodeParser
    nodeParser.parseAndBuildGraph(sourceString)
    val gg          = nodeParser.getGraphFromString(sourceString)
    val metaInfo    = getMetaInfo(gg)
    val exec        = new WorkflowExecutionCommon
    exec.workflowId = metaInfo._1
    exec.memory     = metaInfo._2
    exec.processors = metaInfo._3
    exec.cluster    = metaInfo._4
    exec.mode       = metaInfo._5

    val workflow = new Workflow(sourceString,
                                exec.workflowId,
                                nodeParser.sortedList,
                                nodeParser.nodes,
                                nodeParser.edges,
                                Map[String, MLConstruct](),
                                Map[String, FunctionList](),
                                exec)

    for ((nodeId, node) <- workflow.allNodes) {

      if (MLPipelines.isPipelineNode(nodeId, workflow))
        workflow.mlPipeline(nodeId) = MLPipelines.getPipeline(nodeId, workflow)

      if (TransformFunctions.isTransformFunction(nodeId, workflow))
        workflow.transformFns(nodeId) = TransformFunctions.getTransformFunction(nodeId, workflow)
    }

    workflow
  }
}


