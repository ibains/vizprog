package vizprog.codegen


import org.json4s._
import org.json4s.native.JsonMethods._
import scala.collection.mutable.{Map, ListBuffer}
import vizprog.common._

class WorkflowPrinter {

  val nodePrinter = new NodePrinter()

  def printWorkflow(wf: Workflow): String = {
    val processes   = pretty(render(nodePrinter.allNodesToJson(wf.allNodes)))
    val connections = pretty(render(nodePrinter.allEdgesToJson(wf.allEdges)))
    val order = nodePrinter.OrderToJson(wf.nodeOrder)
    s""" { "processes": $processes, "connections": $connections, "nodeOrder": $order } """
  }

  def printInterimNode(node: InterimNode): String =
    pretty(render(JObject(nodePrinter.printInterimNode(node))))

  def printInterimWorkflow(wf: InterimWorkflow) : String = {
    val s = new StringBuilder
    s++= "{\"" + wf.id + "\":\n"
    val sb = ListBuffer[(String, JObject)]()

    for ((nodeId, node) <- wf.workflow)
      sb.append(nodePrinter.printInterimNode(node))

    s++= pretty(render(JObject(sb.toList)))
    s++= "\n}"
    s.toString()
  }
}
