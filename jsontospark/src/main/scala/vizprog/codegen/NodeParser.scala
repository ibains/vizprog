package vizprog.codegen

import org.json4s._
import org.json4s.native.JsonMethods._
import scala.collection.mutable.{Map, HashSet}
import vizprog.common._

// PARSES DAG FROM JSON STRING

class NodeParser {

  val nodes = Map[String, Node]()
  val edges = Map[String, Edge]()

  var sortedList: List[GNode] = null
  var nodeGraph : NodeGraph   = null
  var graphId   : String      = null
  var graph     : JValue      = null

  protected def getAllNodes(ats: Map[String, Node], graph: JValue) = {
    implicit val formats  = DefaultFormats

    val JObject(nodeList) = graph \ "processes"
    for ((id: String, xx: JObject ) <- nodeList)
    {
      val JString(component)  = xx \ "component"
      val JObject(metadata)   = xx \ "metadata"
      val JObject(properties) = xx \ "properties"
      val JObject(ports)      = xx \ "ports"

      ats(id) = Node(id, component,
        Util.jlistToMap(metadata),
        Util.jlistToMap(ports),
        Util.jlistToMap(properties),
        List[String](),
        HashSet[String](), false)
    }
    val JArray(groupList) = graph \ "groups"
    for ((xx: JValue ) <- groupList)
    {
      val JString(id)         = xx \ "name"
      val JArray(nodes)       = xx \ "nodes"
      val JString(component)  = xx \ "component"
      val JObject(metadata)   = xx \ "metadata"
      val maybeProps          = xx \ "properties"

      var pp : Map[String, JValue] = null
      if (maybeProps != JNothing) {
        val JObject(properties) = maybeProps
        pp = Util.jlistToMap(properties)
      } else {
        pp = Map[String, JValue]()
      }

      ats(id) = Node(id, component,
        Util.jlistToMap(metadata),
        Util.jlistToMap(null),
        pp,
        Util.jlistToStringList(nodes),
        HashSet[String](), true)
    }
  }

  protected def getValue(pp: List[(String, JValue)], key: String): String = {
    for ((a: String, b: JValue) <- pp)
      if (a == key) { val JString(ret) = b; return ret }
    assert(false, "Key Missing in JSON: " + key)
    ""
  }

  protected def getAllEdges(ats: Map[String, Edge], nds: Map[String, Node], gg: JValue) = {
    implicit val formats    = DefaultFormats
    val JArray(connections) = gg \ "connections"
    for (xx <- connections) {
        for (JObject(src)      <- xx \ "src";
             JObject(tgt)      <- xx \ "tgt";
             JObject(metadata) <- xx \ "metadata") {
          val key = getValue(metadata, "route")
          assert(!ats.contains(key), "Edge already exists, possible overwrite")
          ats(key) = Edge(
            getValue(metadata, "route"),
            getValue(src, "process"),
            getValue(src, "port"),
            getValue(tgt, "process"),
            getValue(tgt, "port"))
        }
    }

    // Say nodes depend on their children
    var n = 0
    for ((nid, node) <- nds) {
      for (c <- node.children) {
        n = n + 1
        val id = "fe" + n
        ats(id) = Edge(id, c, "_dep_", nid, "_dep_")
      }
    }
  }

  protected def hasEdge(eds : Map[String, Edge], node: Node,
                        checkId: String, pred: Boolean) : Boolean = {
    val nid = node.id
    if (pred) {
      for (edgeId <- nodeGraph.graph(nid).inEdges)
        if (eds(edgeId).startNode == checkId)
          return true
    } else {
      for (edgeId <- nodeGraph.graph(nid).outEdges)
        if (eds(edgeId).endNode == checkId)
          return true
    }
    false
  }

  protected def buildTransitiveChildren(nodeGraph : NodeGraph) : List[GNode] = {
    // we need topological order based on existing dependencies
    // the dependencies of direct children are already there
    // so one pass builds transitive children correctly
    val order = nodeGraph.topoSort()
    for (gnode <- order) {
      for (child <- nodeGraph.nodeDetails(gnode.id).children) {
        nodeGraph.nodeDetails(gnode.id).allChildren.add(child)
        for (childChild <- nodeGraph.nodeDetails(child).children)
          nodeGraph.nodeDetails(gnode.id).allChildren.add(childChild)
      }
    }
    order
  }

  protected def addChildDependences(child: String, nid: String,
                                    nodeGraph : NodeGraph) : Unit =
  {
    val node = nodeGraph.nodeDetails(nid)
    val childNode = nodeGraph.graph(child)

    for (edgeId <- childNode.inEdges) {
      // CHECK INPUT EDGES
      val edge = nodeGraph.edgeDetails(edgeId)
      val predNodeId = edge.startNode
      if (nid != predNodeId && !node.allChildren.contains(predNodeId)) // NOT SELF/CHILD-NODE
        if (!hasEdge(nodeGraph.edgeDetails, node, predNodeId, true)) // EDGE NOT ALREADY THERE
          nodeGraph.addDependence(predNodeId, nid)
    }
    for (edgeId <- childNode.outEdges) {
      // CHECK OUTPUT EDGES
      val edge = nodeGraph.edgeDetails(edgeId)
      val succNodeId = edge.endNode
      if (nid != succNodeId && !node.allChildren.contains(succNodeId)) // NOT SELF/CHILD-NODE
        if (!hasEdge(nodeGraph.edgeDetails, node, succNodeId, false)) // EDGE NOT ALREADY THERE
          nodeGraph.addDependence(nid, succNodeId)
    }
  }

  // A post processing pass right after parsing nodes and edges
  protected def postProcess(nodeGraph : NodeGraph) : Unit =
  {
    // the nodeOder we are computing is not the final one and is incorrect
    // since the transitive dependencies based on children's children are
    // not yet captured in the edges, so we'll need to compute order again
    val nodeOrder = buildTransitiveChildren(nodeGraph)

    // Add transitive dependencies based on children
    var i = 0
    for (gnode <- nodeOrder) {
      val nid  = gnode.id
      val node = nodeGraph.nodeDetails(nid)

      // The predecessors and successors of the children of a node are
      // predecessors and successors of this node as well, make it transitive
      for (child <- node.children) {
        addChildDependences(child, nid, nodeGraph)
      }

      // Handle a special case for ML
      if (node.component == "ParamGridBuilder") {
        val predNodes = node.properties("parameterMap") \\ "node"
        val JObject(pp) = predNodes
        for ((n, child) <- pp) {
          val JString(cc) = child
          nodeGraph.addDependence(cc, nid)
        }
      }
    }
  }

  // PUBLIC NODE JSON PARSING

  def getGraphFromString(b: String): JValue = {
    graph = parse(b) \ "graph"
    graph
  }

  def getNameFromNode(b: String) : String = {
    val fileJson = parse(b)
    val JObject(allNodes) = fileJson
    for ((name, value) <- allNodes)
      return name
    null
  }

  def parseAndBuildGraph(source: String): Unit = {

    val gg = getGraphFromString(source)

    getAllNodes(nodes, gg)
    getAllEdges(edges, nodes, gg)


    // BUILD GRAPH WITH NODES AND EDGE PUT TOGETHER

    nodeGraph = new NodeGraph(nodes, edges)
    nodeGraph.buildGraph()

    // ADD ADDITIONAL SEMANTICS
    postProcess(nodeGraph)

    // DO TOPOLOGICAL SORT TO GET VALID PROGRAM ORDER

    sortedList = nodeGraph.topoSort()

  }
}
