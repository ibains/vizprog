package vizprog.codegen

import vizprog.common._
import scala.collection.mutable.{HashSet, Map}

class CGBuilder(filePreamble : (String) => Unit)
{
  val allFiles                        = AllFiles(Map[String, OneFile]())
  var target : CodeGenerationTarget   = CGNone
  val preamble                        = filePreamble
  val generatedFunction               = Map[String, Boolean]()
  var generateIntermediate            = true

  def setTarget(mode: Int) = {
    target = mode match {
      case 1 => CGSparkSubmit
      case 2 => CGJobServerBatch
      case 3 => CGInteractiveFunction
      case 4 => CGDeploy
      case _ => CGNone
    }
    assert(target != CGNone, "Specify a valid target (1) SparkSubmit (2) JobServerBatch (3) Interactive (4) Deploy")
  }

  def sparkTypeToScalaType(columnType: String) = {
    columnType match {
      case "IntegerType" => "Integer"
      case "LongType"    => "Long"
      case "FloatType"   => "Float"
      case "DoubleType"  => "Double"
      case "StringType"  => "String"
      case "BooleanType" => "Boolean"

      case "NullType" => assert(false, "Are you sure you want to use Null type?")

      case "ByteType" | "ShortType" | "DecimalType" | "BinaryType" | "TimestampType" |
           "DateType" | "CalendarIntervalType" =>
        assert(false, "Basic Type not handled yet, haven't gotten to it")

      case "ArrayType" | "MapType" |"ColumnVarargType" | "StringVarargType" | "StructType" |
        "UserDefinedType" | "VectorUDT" =>
        assert(false, "Complex Types not handled yet, haven't gotten to it")
    }
  }

  def setIntermediateResults(intermediate: Boolean) = { generateIntermediate = intermediate }

  def hasFile(file: String) : Boolean = allFiles.fs.contains(file)

  def getObjectName(file: String) : String = file + "App"
  protected def getObjectDefinition(file: String) : String = {
    if (target == CGJobServerBatch)
      return s"\nobject ${getObjectName(file)} {\n"
    s"\nobject ${getObjectName(file)} {\n"
  }

  protected def addFileCode(sb: StringBuffer, oneFile: OneFile,
                            fileName: String, addObject: Boolean): Unit =
  {
    for (imp <- oneFile.imports)
      sb.append(s"import $imp\n")
    sb.append("\n\n")
    for (dfn <- oneFile.definitions)
      sb.append(dfn + "\n\n")
    if (addObject)
      sb.append(getObjectDefinition(fileName))
    sb.append(oneFile.code + "\n\n")
    if (addObject)
      sb.append("}\n")
  }

  def getCode: String = {
    val sb = new StringBuffer()

    for ((fileName, oneFile) <- allFiles.fs)
      if (fileName == "main")
        if (oneFile.thePackage.size > 0)
          for (pak <- oneFile.thePackage)
            sb.append(s"$pak\n")
    for ((fileName, oneFile) <- allFiles.fs)
      if (fileName != "main")  {
        val addObject = fileName != "restio" // add object except for Rest IO
        addFileCode(sb, oneFile, fileName, addObject)
      }
    for ((fileName, oneFile) <- allFiles.fs)
      if (fileName == "main")
        addFileCode(sb, oneFile, null, false)
    sb.toString
  }

  protected def ensureFile(file: String): Unit = {
    if (allFiles.fs.contains(file))
      return
    val oneFile = OneFile(HashSet[String](), HashSet[String](), HashSet[String](), new StringBuffer())
    allFiles.fs(file) = oneFile
    preamble(file)
  }

  def addImport(file: String, pkg: String) : Unit = {
    ensureFile(file)
    allFiles.fs(file).imports.add(pkg)
  }

  def addCode(file: String, s: String): Unit = {
    ensureFile(file)
    allFiles.fs(file).code.append("  ")
    allFiles.fs(file).code.append(s)
  }

  def addDefinition(file: String, s: String): Unit = {
    ensureFile(file)
    allFiles.fs(file).definitions.add(s)
  }

  def addPackage(file: String, s: String) : Unit = {
    ensureFile(file)
    allFiles.fs(file).thePackage.add(s)
  }

}