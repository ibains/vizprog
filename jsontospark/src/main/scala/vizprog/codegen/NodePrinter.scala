package vizprog.codegen

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import scala.collection.mutable.{Map, ListBuffer}
import vizprog.common._

// PRINTS A DAG AS JSON

class NodePrinter {

  protected def MapToJson(mm: Map[String, JValue]): JObject = {
    val lb = ListBuffer[(String, JValue)]()
    if (mm != null)
      for ((k, v) <- mm)
        lb.append((k, v))
    JObject(lb.toList)
  }

  protected def nodeToJson(n: Node): (String, JObject) = {
    val json =
      ("process" ->
        ("id" -> n.id) ~
          ("component" -> n.component) ~
          ("metadata" -> MapToJson(n.metadata)) ~
          ("properties" -> MapToJson(n.properties)) ~
          ("ports" -> MapToJson(n.ports)) ~
          ("nodes" -> n.children))
    json
  }

  // PRINT EDGES

  protected def edgeToJson(e: Edge): (String, JsonAST.JObject) = {
    val json =
      ("connection" ->
        ("src" ->
          ("process" -> e.startNode) ~
            ("port" -> e.startPort)) ~
          ("tgt" ->
            ("process" -> e.endNode) ~
              ("port" -> e.endPort)) ~
          ("metadata" ->
            ("route" -> e.id))
        )
    json
  }

  protected def allNodeRequestJson(nodes:Map[String, Node]) : String = {
    val lb = ListBuffer[JsonAST.JObject]()
    for ((k, n) <- nodes) {
      if (n.ports.contains("outputs")) {
        val JArray(ports) = n.ports("outputs")
        for (p <- ports) {
          val json =
            ("id" -> n.id) ~
              ("port" -> p)
          lb.append(json)
        }
      }
    }
    pretty(render(lb.toList))
  }

  //
  // PUBLIC INTERFACE
  //

  def generateNodeRequest(id: String, nodes: Map[String, Node], interimRoot: String) : String = {
    """{
      "id": """"        + id + """",
      "tmpFolder": """" + interimRoot + """",
      "nodes": """      + allNodeRequestJson(nodes) + "}".stripMargin
  }

  def allNodesToJson(ats: Map[String, Node]): JObject = {
    val lb = ListBuffer[(String, JsonAST.JObject)]()
    for ((k, v) <- ats)
      lb.append(nodeToJson(v))
    JObject(lb.toList)
  }

  def allEdgesToJson(ats: Map[String, Edge]): JObject = {
    val lb = ListBuffer[(String, JsonAST.JObject)]()
    for ((k, v) <- ats)
      lb.append(edgeToJson(v))
    JObject(lb.toList)
  }

  def OrderToJson(nodeOrder: List[GNode]) : String = {
    var sb = new StringBuilder()
    var i = false
    sb ++= "{"
    for (gnode <- nodeOrder) { if (i) sb ++= ", "; sb ++= gnode.id; i = true }
    sb ++= "}"
    sb.toString()
  }

  def printInterimNode(node: InterimNode) : (String, JObject) = {
    val sb = ListBuffer[(String, JString)]()
    for ((k,t) <- node.schema)
      sb.append((k, JString(t)))

    val db = ListBuffer[(String, JArray)]()
    for ((k,d) <- node.data) {
      val rb = ListBuffer[(JString)]()
      for (s <- d)
        rb.append(JString(s))
      db.append((k, JArray(rb.toList)))
    }

    val json =
      node.nodeId ->
        ("schema" -> sb.toList) ~
          ("stats" -> node.stats) ~
          ("topn" -> node.topN) ~
          ("histogram" -> node.hist) ~
          ("nulls" -> node.cleanse) ~
          ("data", db.toList)
    json
  }

  def printJob(we : WorkflowExecution) : (String, JObject) = {
    val json =
      we.executionId ->
        ("executionId" -> we.executionId) ~
          ("workflowId" -> we.common.workflowId) ~
          ("userId" -> we.userId) ~
          ("project" -> we.project) ~
          ("beginTime" -> we.beginTime) ~
          ("endTime" -> we.endTime)
    json
  }
  def printAllJobs(n : Int, jobHistory: ConcurArray[WorkflowExecution]) : String = {
    val numJobs = math.min(n, jobHistory.len)
    val lb = ListBuffer[(String, JsonAST.JObject)]()
    0 to numJobs - 1 foreach { i =>
      val job = jobHistory.get(i)
      lb.append(printJob(job))
    }
    pretty(render(JObject(lb.toList)))
  }
}
