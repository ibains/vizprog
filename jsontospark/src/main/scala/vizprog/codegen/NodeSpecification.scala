package vizprog.codegen

import org.json4s.JsonAST._
import scala.collection.mutable.Map
import vizprog.common._

class Helper {
  def getValuesString(in: List[String]): String = {
    val sb = new StringBuilder()
    var ii = 0
    for (s <- in) {
      if (ii > 0)
        sb.append(", ")
      sb.append(s)
      ii = ii + 1
    }
    sb.toString
  }

  def toJString(in: List[String]): List[JString] = {
    var ll = List[JString]()
    for (ss <- in)
      ll = JString(ss) :: ll
    ll
  }
}

class DataTypes {
  def getByte      : String = "ByteType"      // Represents 1-byte signed integer numbers. The range of numbers is from -128 to 127.
  def getShort     : String = "ShortType"     // Represents 2-byte signed integer numbers. The range of numbers is from -32768 to 32767.
  def getInt       : String = "IntegerType"   // Represents 4-byte signed integer numbers. The range of numbers is from -2147483648 to 2147483647.
  def getLong      : String = "LongType"      // Represents 8-byte signed integer numbers. The range of numbers is from -9223372036854775808 to 9223372036854775807.
  def getFloat     : String = "FloatType"     // Represents 4-byte single-precision floating point numbers.
  def getDouble    : String = "DoubleType"    // Represents 8-byte double-precision floating point numbers.
  def getDecimal   : String = "DecimalType"   // Represents arbitrary-precision signed decimal numbers. Backed internally by java.math.BigDecimal. A BigDecimal consists of an arbitrary precision integer unscaled value and a 32-bit integer scale.
  def getString    : String = "StringType"    // Represents character string values.
  def getBinary    : String = "BinaryType"    // Represents byte sequence values.
  def getBoolean   : String = "BooleanType"   // Represents boolean values.
  def getTimestamp : String = "TimestampType" // Represents values comprising values of fields year, month, day, hour, minute, and second.
  def getDate      : String = "DateType"      // Represents values comprising values of fields year, month, day.
  def getAllTypes  : List[String] = getByte      :: getShort  :: getInt     :: getLong ::
                                    getFloat     :: getDouble :: getDecimal ::
                                    getString    :: getBinary :: getBoolean ::
                                    getTimestamp :: getDate   :: Nil

  /* ADD COMPLEX TYPES LATER
  def getArray(elementType: String, containsNull: String)              : String = "ArrayType(" + elementType + ", " + containsNull + ")"
  def getMap(keyType: String, valueType: String, containsNull: String) : String = "MapType(" + keyType + ", " + valueType + ", " + containsNull + ")"
  def getStructField(name:String, dataType: String, nullable: String)  : String = "StructField(" + name + ", " + dataType + ", " + nullable + ")"
  def getStruct(fields: List[String])                                  : String = "StructType(" + getValuesString(fields) + ")"
  */
}

class ExpressionTypes {
  val tt   = new DataTypes()
  val hh   = new Helper()
  val str  = tt.getString
  val int  = tt.getInt
  val expr =
    "(Column)"                                ::
      "(Expr cast Type)"                      ::
      "(Expr substr " + int + " " + int + ")" ::
      "(Expr substr Expr Expr)"               ::
      "(Expr bitwiseAND Any)"                 ::
      "(Expr bitwiseOR  Any)"                 ::
      "(Expr bitwiseXOR Any)"                 ::
      "(Expr + Any)"                          ::
      "(Expr - Any)"                          ::
      "(Expr / Any)"                          ::
      "(- Expr)"                              ::
      "(BoolExpr)"                            ::
      "(WhenExpr)"                            :: Nil

  val binaryBoolOperators  = "!==" :: "%" :: "&&" :: "||" :: "<" :: "<=" :: "<=>" :: "===" :: ">" :: ">=" :: Nil
  val binaryBoolFns        = "like" :: "rlike" :: "startsWith" :: "endsWith" :: Nil
  val binaryBoolExpr       = "(Expr BinaryBoolSymbol Any)" :: "(Expr BinaryBoolFunction " + str + ")" :: Nil
  val unaryBoolExpr        = "(Expr isNaN)" :: "(Expr isNotNull)" :: "(Expr isNull)" :: "(! BoolExpr)" :: Nil

  val col                  = "Column             = Dataframe(ColName:" + tt.getString + ")"
  val tty                  = "Type               = " + hh.getValuesString(tt.getAllTypes)
  val any                  = "Any                = Column | Type"
  val valueExpr            = "Expr               = " + hh.getValuesString(expr)
  val binaryBoolSym        = "BinaryBoolSymbol   = " + hh.getValuesString(binaryBoolOperators)
  val binaryBoolFunctions  = "BinaryBoolFunction = " + hh.getValuesString(binaryBoolFns)
  val binaryBoolExpression = "BinaryBoolExpr     = " + hh.getValuesString(binaryBoolExpr)
  val unaryBoolExpression  = "UnaryBoolExpr      = " + hh.getValuesString(unaryBoolExpr)
  val boolExpr             = "BoolExpr           = BinaryBoolExpr | UnaryBoolExpr"
  val whenExpr             = "WhenExpr           = " + hh.getValuesString("When(BoolExpr,Any)+" :: "Otherwise(Any)" :: Nil)

  def BoolExpr : String = "BoolExpr"
  def Expr     : String = "Expr"

  def printExpression: String = {
    val s = new StringBuilder("\n\nGRAMMAR FOR EXPRESSIONS:\n\n")
    s++= tty                  ; s++= "\n"
    s++= col                  ; s++= "\n"
    s++= any                  ; s++= "\n"
    s++= valueExpr            ; s++= "\n"
    s++= binaryBoolSym        ; s++= "\n"
    s++= binaryBoolFunctions  ; s++= "\n"
    s++= binaryBoolExpression ; s++= "\n"
    s++= unaryBoolExpression  ; s++= "\n"
    s++= boolExpr             ; s++= "\n"
    s.toString()
  }
}

class NodeSpecification {

  val data        = new DataTypes
  val expr        = new ExpressionTypes
  val hh          = new Helper

  def getMetadata: Map[String, JValue] = {
    val mm        = Map[String, JValue]()
    mm("x")       = JString(data.getInt)
    mm("y")       = JString(data.getInt)
    mm("label")   = JString(data.getString)
    mm
  }
  def getPorts(inPorts: List[String], outPorts: List[String]): Map[String, JValue] = {
    val mm        = Map[String, JValue]()
    mm("inputs")  = JArray(hh.toJString(inPorts))
    mm("outputs") = JArray(hh.toJString(outPorts))
    mm
  }

  def getId         = data.getString
  def getProperties = Map[String, JValue]()
  def getStringList = JArray(List(JString(data.getString), JString("*")))
  def getExprList   = JArray(List(JString(expr.Expr)     , JString("*")))
  def getDoubleList = JArray(List(JString(data.getDouble), JString("*")))

  def node(id         : String,
           component  : String,
           metadata   : Map[String, JValue],
           ports      : Map[String, JValue],
           properties : Map[String, JValue]) : Node =
  {
    Node(id, component, metadata, ports, properties, null, null, false)
  }


  /* SELECT / AS / ALIAS

  1) select(col: String, cols: String*): DataFrame
      Selects a set of columns. This is a variant of select that can only select existing columns
      using column names (i.e. cannot construct expressions).
      // The following two are equivalent:
      df.select("colA", "colB")
      df.select($"colA", $"colB")
  2) select(cols: Column*): DataFrame
      Selects a set of column based expressions.
      df.select($"colA", $"colB" + 1)
  3) selectExpr(exprs: String*): DataFrame
      Selects a set of SQL expressions. This is a variant of select that accepts SQL expressions.
      df.selectExpr("colA", "colB as newName", "abs(colC)")
  4) as(alias: String, metadata: Metadata): Column
      Gives the column an alias with metadata.
      val metadata: Metadata = ...
      df.select($"colA".as("colB", metadata))
  5) as(alias: Symbol): Column
      Gives the column an alias.
      // Renames colA to colB in select output.
      df.select($"colA".as('colB))
      If the current column has metadata associated with it, this metadata will be propagated to
      the new column. If this not desired, use as with explicitly empty metadata.
  6) as(aliases: Array[String]): Column
      Assigns the given aliases to the results of a table generating function.
      // Renames colA to colB in select output.
      df.select(explode($"myMap").as("key" :: "value" :: Nil))
  7) as(aliases: Seq[String]): Column
      Assigns the given aliases to the results of a table generating function.
      // Renames colA to colB in select output.
      df.select(explode($"myMap").as("key" :: "value" :: Nil))
  8) as(alias: String): Column
      Gives the column an alias.
      // Renames colA to colB in select output.
      df.select($"colA".as("colB"))
      If the current column has metadata associated with it, this metadata will be propagated to
      the new column. If this not desired, use as with explicitly empty metadata.
  9) alias(alias: String): Column
      Gives the column an alias. Same as as.
      // Renames colA to colB in select output.
      df.select($"colA".alias("colB"))
  */

  def buildSelect: Node = {
    val props        = getProperties
    props("comment") = JString("Select columns (with optional renames)")
    props("columns") = JObject(List(
      (data.getString, JString(data.getString)),
      (data.getString, JString(data.getString))))
    node(getId, "Select", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* FILTER - IMPLEMENT VERSION 1 ONLY

  1) filter(conditionExpr: String): DataFrame
      Filters rows using the given SQL expression.
      peopleDf.filter("age > 15")
  2) filter(condition: Column): DataFrame
      Filters rows using the given condition.
      // The following are equivalent:
      peopleDf.filter($"age" > 15)
      peopleDf.where($"age" > 15)
  */

  def buildFilter: Node = {
    val props          = getProperties
    props("comment")   = JString("Filter using boolean expression on one or more columns")
    props("condition") = JString(expr.BoolExpr)
    node(getId, "Filter", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* JOIN

  1) join(right: DataFrame, joinExprs: Column, joinType: String): DataFrame
      Join with another DataFrame, using the given join expression. The following performs a full
      outer join between df1 and df2.
      // Scala:
      import org.apache.spark.sql.functions._
      df1.join(df2, $"df1Key" === $"df2Key", "outer")
      joinType: One of: inner, outer, left_outer, right_outer, leftsemi.
  2) join(right: DataFrame, joinExprs: Column): DataFrame
      Inner join with another DataFrame, using the given join expression.
      // The following two are equivalent:
      df1.join(df2, $"df1Key" === $"df2Key")
      df1.join(df2).where($"df1Key" === $"df2Key")
  3) join(right: DataFrame, usingColumns: Seq[String]): DataFrame
      Inner equi-join with another DataFrame using the given columns.
      Different from other join functions, the join columns will only appear once in the output,
      i.e. similar to SQL's JOIN USING syntax.
      // Joining df1 and df2 using the columns "user_id" and "user_name"
      df1.join(df2, Seq("user_id", "user_name"))
      Note that if you perform a self-join using this function without aliasing the input
      DataFrames, you will NOT be able to reference any columns after the join, since there is no
      way to disambiguate which side of the join you would like to reference.
      usingColumns: Names of the columns to join on. This columns must exist on both sides.
  4) join(right: DataFrame, usingColumn: String): DataFrame
      Same as above, one colun instead of multiple
  5) join(right: DataFrame): DataFrame
      Cartesian join with another DataFrame.
      Note that cartesian joins are very expensive extra filter that can be pushed down.
  */

  def buildJoin: Node = {
    val props              = getProperties
    props("comment")       = JString("Join left and right inputs using the given boolean expression and the join type")
    props("joinCondition") = JString(expr.BoolExpr)
    props("joinType")      = JString(hh.getValuesString(
      "ChooseOne" :: "inner" :: "outer" :: "left_outer" :: "right_outer" :: "leftsemi" :: Nil))
    node(getId, "Join", getMetadata, getPorts("left" :: "right" :: Nil, "out" :: Nil), props)
  }

  /* UNION ALL

  1) unionAll(other: DataFrame): DataFrame
      Returns a new DataFrame containing union of rows in this frame and another frame.
      This is equivalent to UNION ALL in SQL.
 */
  def buildUnionAll: Node = {
    val props        = getProperties
    props("comment") = JString("Combine both inputs, duplicates are kept")
    node(getId, "UnionAll", getMetadata, getPorts("one" :: "two" :: Nil, "out" :: Nil), props)
  }

  /* AGGREGATE

  1) agg(expr: Column, exprs: Column*): DataFrame
      Aggregates on the entire DataFrame without groups.
      // df.agg(...) is a shorthand for df.groupBy().agg(...)
      df.agg(max($"age"), avg($"salary"))
      df.groupBy().agg(max($"age"), avg($"salary"))
  2) agg(exprs: Map[String, String]): DataFrame
      (Scala-specific) Aggregates on the entire DataFrame without groups.
      // df.agg(...) is a shorthand for df.groupBy().agg(...)
      df.agg(Map("age" -> "max", "salary" -> "avg"))
      df.groupBy().agg(Map("age" -> "max", "salary" -> "avg"))
  3) agg(aggExpr: (String, String), aggExprs: (String, String)*): DataFrame
      (Scala-specific) Aggregates on the entire DataFrame without groups.
      // df.agg(...) is a shorthand for df.groupBy().agg(...)
      df.agg("age" -> "max", "salary" -> "avg")
      df.groupBy().agg("age" -> "max", "salary" -> "avg")
  4) groupBy(col1: String, cols: String*): GroupedData
      Groups the DataFrame using the specified columns, so we can run aggregation on them.
      See GroupedData for all the available aggregate functions.
      This is a variant of groupBy that can only group by existing columns using column names
      (i.e. cannot construct expressions).
      // Compute the average for all numeric columns grouped by department.
      df.groupBy("department").avg()
      // Compute the max age and average salary, grouped by department and gender.
      df.groupBy($"department", $"gender").agg(Map(
        "salary" -> "avg",
        "age" -> "max"
      ))
  5) groupBy(cols: Column*): GroupedData
      Groups the DataFrame using the specified columns, so we can run aggregation on them.
      See GroupedData for all the available aggregate functions.
      // Compute the average for all numeric columns grouped by department.
      df.groupBy($"department").avg()
      // Compute the max age and average salary, grouped by department and gender.
      df.groupBy($"department", $"gender").agg(Map(
        "salary" -> "avg",
        "age" -> "max"
      ))
  */

  def buildAggregate: Node = {
    val props        = getProperties
    val aggFn        = JString(hh.getValuesString("ChooseOne" :: "Sum" :: "Avg" :: "Min" :: "Max" :: "Count" :: Nil))
    props("comment") = JString("Aggregate by GroupBy columns/expressions and compute any aggregate functions")
    props("groupBy") = getExprList
    props("columns") = JObject(List((data.getString, aggFn), (data.getString, aggFn)))
    node(getId, "Aggregate", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* ROLLUP / CUBE

  1) rollup(col1: String, cols: String*): GroupedData
      Create a multi-dimensional rollup for the current DataFrame using the specified columns, so we can run aggregation
      on them. See GroupedData for all the available aggregate functions.
      This is a variant of rollup that can only group by existing columns using column names (i.e. cannot construct
      expressions).
      // Compute the average for all numeric columns rolluped by department and group.
        df.rollup("department", "group").avg()
  2) rollup(cols: Column*): GroupedData
      Create a multi-dimensional rollup for the current DataFrame using the specified columns, so we can run aggregation
      on them. See GroupedData for all the available aggregate functions.
      // Compute the average for all numeric columns rolluped by department and group.
        df.rollup($"department", $"group").avg()
      // Compute the max age and average salary, rolluped by department and gender.
        df.rollup($"department", $"gender").agg(Map(
          "salary" -> "avg",
          "age" -> "max"))
  3) cube(col1: String, cols: String*): GroupedData
      Create a multi-dimensional cube for the current DataFrame using the specified columns, so we can run aggregation
      on them. See GroupedData for all the available aggregate functions.
      This is a variant of cube that can only group by existing columns using column names (i.e. cannot construct
      expressions).
      // Compute the average for all numeric columns cubed by department and group.
        df.cube("department", "group").avg()
      // Compute the max age and average salary, cubed by department and gender.
        df.cube($"department", $"gender").agg(Map(
          "salary" -> "avg",
          "age" -> "max"))
  4) cube(cols: Column*): GroupedData
      Create a multi-dimensional cube for the current DataFrame using the specified columns, so we can run aggregation
      on them. See GroupedData for all the available aggregate functions.
      // Compute the average for all numeric columns cubed by department and group.
        df.cube($"department", $"group").avg()
      // Compute the max age and average salary, cubed by department and gender.
        df.cube($"department", $"gender").agg(Map(
          "salary" -> "avg",
          "age" -> "max"))
  */
  def buildCube: Node = {
    val props         = getProperties
    props("comment")  = JString("Computes a cube to which faster aggregation can be applied")
    props("columns")  = getStringList
    node(getId, "Cube", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* INTERSECT

  1) intersect(other: DataFrame): DataFrame
      Returns a new DataFrame containing rows only in both this frame and another frame.
      This is equivalent to INTERSECT in SQL.
  */
  def buildIntersect: Node = {
    val props        = getProperties
    props("comment") = JString("Rows present in both one and two")
    node(getId, "Intersect", getMetadata, getPorts("one" :: "two" :: Nil, "out" :: Nil), props)
  }

  /* EXCEPT

  1) except(other: DataFrame): DataFrame
      Returns a new DataFrame containing rows in this frame but not in another frame.
      This is equivalent to EXCEPT in SQL.
  */
  def buildExcept: Node = {
    val props        = getProperties
    props("comment") = JString("Rows in one that are not present in two")
    node(getId, "Except", getMetadata, getPorts("one" :: "two" :: Nil, "out" :: Nil), props)
  }

  /* DISTINCT

  1) dropDuplicates(colNames: Array[String]): DataFrame
      Returns a new DataFrame with duplicate rows removed, considering only the subset of columns.
  2) dropDuplicates(colNames: Seq[String]): DataFrame
      (Scala-specific) Returns a new DataFrame with duplicate rows removed, considering only the subset of columns.
  3) dropDuplicates(): DataFrame
      Returns a new DataFrame that contains only the unique rows from this DataFrame. This is an alias for distinct.
  4) distinct(): DataFrame
      Returns a new DataFrame that contains only the unique rows from this DataFrame. This is an alias for
      dropDuplicates.
  */
  def buildDistinct: Node = {
    val props        = getProperties
    props("comment") = JString("Remove rows with duplicates, compare all column or only specified columns")
    props("columns") = getStringList
    node(getId, "Distinct", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* DROP_MISSING

  1) na: DataFrameNaFunctions
  Returns a DataFrameNaFunctions for working with missing data.
    // Dropping rows containing any null values.
      df.na.drop()
  */
  def buildDropMissing: Node = {
    val props        = getProperties
    props("comment") = JString("Remove rows with missing or null values")
    node(getId, "DropMissing", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }


  /* LIMIT

  1) limit(n: Int): DataFrame
      Returns a new DataFrame by taking the first n rows. The difference between this function and head is that head
      returns an array while limit returns a new DataFrame.
  */
  def buildLimit: Node = {
    val props        = getProperties
    props("comment") = JString("Only keep Limit number of rows")
    props("limit")   = JString(data.getInt)
    node(getId, "Limit", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* ORDER_BY

  1) orderBy(sortExprs: Column*): DataFrame
      Returns a new DataFrame sorted by the given expressions. This is an alias of the sort function.
  2) orderBy(sortCol: String, sortCols: String*): DataFrame
      Returns a new DataFrame sorted by the given expressions. This is an alias of the sort function.
  */
  def buildOrderBy: Node = {
    val props        = getProperties
    props("comment") = JString("Sort rows by given columns/expressions")
    props("orderBy") = getExprList
    node(getId, "OrderBy", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* RANDOM_SPLIT

  1) randomSplit(weights: Array[Double]): Array[DataFrame]
      Randomly splits this DataFrame with the provided weights.
      weights - weights for splits, will be normalized if they don't sum to 1.
  2) randomSplit(weights: Array[Double], seed: Long): Array[DataFrame]
      Randomly splits this DataFrame with the provided weights.
      weights - weights for splits, will be normalized if they don't sum to 1.
      seed - Seed for sampling.
  */
  def buildSplit: Node = {
    val props        = getProperties
    props("comment") = JString("Splits into two, one gets ratio (0 < ratio < 1) of rows, two gets the rest")
    props("ratio")   = JString(data.getDouble)
    props("seed")    = JString(data.getLong)
    node(getId, "Split", getMetadata, getPorts("in" :: Nil, "one" :: "two" :: Nil), props)
  }

  /* SAMPLE

  1) sample(withReplacement: Boolean, fraction: Double): DataFrame
      Returns a new DataFrame by sampling a fraction of rows, using a random seed.
      withReplacement - Sample with replacement or not.
      fraction - Fraction of rows to generate.
  2) sample(withReplacement: Boolean, fraction: Double, seed: Long): DataFrame
      Returns a new DataFrame by sampling a fraction of rows.
      withReplacement - Sample with replacement or not.
      fraction - Fraction of rows to generate.
      seed - Seed for sampling.
  */
  def buildSample: Node = {
    val props                = getProperties
    props("comment")         = JString("Get a sample (fraction) of the data")
    props("fraction")        = JString(data.getDouble)
    props("seed")            = JString(data.getLong)
    props("withReplacement") = JString(data.getBoolean)
    node(getId, "Sample", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* ADD_COLUMN

  1) withColumn(colName: String, col: Column): DataFrame
      Returns a new DataFrame by adding a column or replacing the existing column that has the same name.
  */
  def buildAddColumn: Node = {
    val props           = getProperties
    props("comment")    = JString("Add Column generated using the given expression")
    props("columnName") = JString(data.getString)
    props("expression") = JString(expr.Expr)
    node(getId, "AddColumn", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }

  /* WINDOW_FUNCTION
  1) WindowSpec
    1.1) orderBy(cols: Column*): WindowSpec
          Defines the ordering columns in a WindowSpec.
    1.2) orderBy(colName: String, colNames: String*): WindowSpec
          Defines the ordering columns in a WindowSpec.
    1.3) partitionBy(cols: Column*): WindowSpec
          Defines the partitioning columns in a WindowSpec.
    1.4) partitionBy(colName: String, colNames: String*): WindowSpec
          Defines the partitioning columns in a WindowSpec.
    1.5) rangeBetween(start: Long, end: Long): WindowSpec
          Defines the frame boundaries, from start (inclusive) to end (inclusive).
    1.6) rowsBetween(start: Long, end: Long): WindowSpec
          Defines the frame boundaries, from start (inclusive) to end (inclusive).
  2) over(window: WindowSpec): Column
      Define a windowing column.
      val w = Window.partitionBy("name").orderBy("id")
      df.select(
        sum("price").over(w.rangeBetween(Long.MinValue, 2)),
        avg("price").over(w.rowsBetween(0, 4))
      )
  */
  def buildWindowFunction: Node = {
    val props        = getProperties
    val fns          = JString(hh.getValuesString("ChooseOne" :: "Sum" :: "Avg" :: "Min" :: "Max" :: Nil))
    props("comment") = JString("Select using window functions")
    props("Columns") = getStringList
    props("Over1")   = JObject(List(
      ("column"                , JString(data.getString)),
      ("function"              , fns),
      ("orderByExpressions"    , JString(expr.Expr + "*")),
      ("orderByColumns"        , JString(data.getString + "*")),
      ("partitionByExpressions", JString(expr.Expr + "*")),
      ("partitionByColumns"    , JString(data.getString + "*")),
      ("rangeBetween"          , JObject(List(("start", JString(data.getLong)), ("end", JString(data.getLong))))),
      ("rowsBetween"           , JObject(List(("from", JString(data.getLong)), ("to", JString(data.getLong)))))
    ))
    props("Over2") = JObject(List())
    node(getId, "WindowFunction", getMetadata, getPorts("in" :: Nil, "out" :: Nil), props)
  }


  /*
  stat - todo as part of statistical info
  */

  def buildNodes(nodes: Map[String, Node]): Unit = {
    nodes("Select")         = buildSelect
    nodes("Filter")         = buildFilter
    nodes("Join")           = buildJoin
    nodes("UnionAll")       = buildUnionAll
    nodes("Aggregate")      = buildAggregate
    nodes("Cube")           = buildCube
    nodes("Intersect")      = buildIntersect
    nodes("Except")         = buildExcept
    nodes("Distinct")       = buildDistinct
    nodes("DropMissing")    = buildDropMissing
    nodes("OrderBy")        = buildOrderBy
    nodes("Split")          = buildSplit
    nodes("Sample")         = buildSample
    nodes("AddColumn")      = buildAddColumn
    nodes("Windowfunction") = buildWindowFunction
  }
}