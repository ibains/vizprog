package vizprog.common
import scala.collection.mutable.Map

class WorkflowExecutionCommon {
  var memory: BigInt = 0
  var processors: BigInt = 0
  var cluster: String = ""
  var workflowId: String = ""
  var mode: String = ""
}

class WorkflowExecution {
  var common: WorkflowExecutionCommon = null
  var resultCode: BigInt = 0
  var resultString: String = ""
  var executionId: String = ""
  var userId: String = ""
  var project: String = ""
  var beginTime: String = ""
  var endTime: String = ""
}

case class Workflow(workflowJson : String,
                    workFlowId   : String,
                    nodeOrder    : List[GNode],
                    allNodes     : Map[String, Node],
                    allEdges     : Map[String, Edge],
                    mlPipeline   : Map[String, MLConstruct],
                    transformFns : Map[String, FunctionList],
                    execution    : WorkflowExecutionCommon)

case class InterimWorkflow(id       : String,
                           workflow : Map[String, InterimNode])
