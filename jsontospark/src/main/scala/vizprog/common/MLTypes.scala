package vizprog.common

object MLTypes {
  sealed trait MLType
  case object MLFeature        extends MLType
  case object MLAlgorithm      extends MLType
  case object MLEvaluator      extends MLType
  case object MLCrossValidator extends MLType
  case object MLParamBuilder   extends MLType
  case object MLOther          extends MLType

  def getString(t: MLType) : String = {
    t match {
      case MLFeature        => "MLFeature"
      case MLAlgorithm      => "MLAlgorithm"
      case MLEvaluator      => "MLEvaluator"
      case MLCrossValidator => "MLCrossValidator"
      case MLParamBuilder   => "MLParamBuilder"
      case MLOther          => "MLOther"
      case _                => "MLType"
    }
  }
  val allTypes = Seq(MLFeature, MLAlgorithm, MLEvaluator, MLCrossValidator, MLParamBuilder)

  def getMLNodeType(nodeId: String, wf: Workflow) : MLType = {
    val operator = wf.allNodes(nodeId).component
    operator match {
      case "Binarizer"           |
           "Bucketizer"          |
           "ChiSqSelector"       |
           "CountVectorizer"     |
           "DCT"                 |
           "ElementwiseProduct"  |
           "HashingTF"           |
           "IDF"                 |
           "IndexToString"       |
           "MinMaxScaler"        |
           "NGram"               |
           "Normalizer"          |
           "OneHotEncoder"       |
           "PCA"                 |
           "PolynomialExpansion" |
           "QuantileDiscretizer" |
           "RegexTokenizer"      |
           "Rformlua"            |
           "SQLTransformer"      |
           "StandardScaler"      |
           "StopWordsReomver"    |
           "StringIndexer"       |
           "Tokenizer"           |
           "VectorAssembler"     |
           "VectorIndexer"       |
           "VectorSlicer"        |
           "Word2Vec"            => MLFeature

      case "AFTSurvivalRegression" |
           "LogisticRegression"    |
           "DecisionTrees"         |
           "GBT"                   |
           "NaiveBayes"            |
           "MultilayerPerceptron"  |
           "Probablistic"          |
           "RandomForests"         |
           "ALS"                   |
           "LDA"                   |
           "KMeans"                |
           "IsotonicRegression"    |
           "LinearRegression"      => MLAlgorithm

      case "CrossValidator"        => MLCrossValidator

      case "RegressionEvaluator"                |
           "BinaryClassificationEvaluator"      |
           "MulticlassClassificationEvaluator"  => MLEvaluator

      case "DirectParameter" | "ModelApplier" =>  MLOther

      case _ =>
        assert(false, "Uknown ML Component : " + operator)
        MLOther
    }
  }
}
