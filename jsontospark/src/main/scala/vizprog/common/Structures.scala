package vizprog.common

import java.util.concurrent.locks.ReentrantLock
import scala.collection.mutable.{ArrayBuffer, Map}

class ConcurMap[T] {
  protected var i = 0
  protected val l = new ReentrantLock()
  val m = Map[String, T]()
  protected def lk() : Unit = l.lock()
  protected def uk() : Unit = l.unlock()
  def has(k: String)            : Boolean = { lk(); val r = m.contains(k);       uk(); r }
  def put(k: String, v: T)      : Unit    = { lk(); m(k) = v;                    uk()    }
  def get(k: String)            : T       = { lk(); val r = m(k);                uk(); r }
  def remove(k: String)         : T       = { lk(); val r = m(k); m.remove(k);   uk(); r }
  def getId                     : String  = { lk(); i = i + 1; val r = "id" + i; uk(); r }
}

class ConcurArray[T] {
  protected var i = 0
  val m = ArrayBuffer[T]()
  protected val l = new ReentrantLock()
  protected def lk() : Unit  = l.lock()
  protected def uk() : Unit  = l.unlock()
  def len          : Int     = m.length
  def get(i : Int) : T       = m(i)
  def append(t: T) : Unit    = { lk(); m += t; uk() }
  def getId        : String  = { lk(); i = i + 1; val r = "id" + i; uk(); r }
}
