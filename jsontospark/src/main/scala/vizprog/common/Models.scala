package vizprog.common

import org.json4s.JValue
import org.json4s.JsonAST._
import org.json4s.native.JsonMethods._
import scala.collection.mutable.{HashSet, Map}

object aws {
  val K = "AWS_KEY"
  val S = "AWS_SECRET"
}

case class Node(id         : String,
                component  : String,
                metadata   : Map[String, JValue],
                ports      : Map[String, JValue],
                properties : Map[String, JValue],
                children   : List[String],
                allChildren: HashSet[String],
                isMetaNode : Boolean)

case class GNode(id        : String,
                 inEdges   : HashSet[String],
                 outEdges  : HashSet[String])

case class Edge(id         : String,
                startNode  : String,
                startPort  : String,
                endNode    : String,
                endPort    : String)

case class InterimNode(nodeId  : String,
                       schema  : List[(String, String)],
                       data    : List[(String, List[String])],
                       stats   : List[(String, JValue)],
                       hist    : List[(String, JValue)],
                       cleanse : List[(String, JValue)],
                       topN    : List[(String, JValue)])

case class OneFile(thePackage  : HashSet[String],
                   imports     : HashSet[String],
                   definitions : HashSet[String],
                   code        : StringBuffer)

case class AllFiles(fs         : Map[String, OneFile])

sealed trait CodeGenerationTarget { def value: Int }
case object CGNone                extends CodeGenerationTarget { val value = 0 }
case object CGSparkSubmit         extends CodeGenerationTarget { val value = 1 }
case object CGJobServerBatch      extends CodeGenerationTarget { val value = 2 }
case object CGInteractiveFunction extends CodeGenerationTarget { val value = 3 }
case object CGDeploy              extends CodeGenerationTarget { val value = 4 }

case class CGDataset(name: String, location: String, generatingWorkflowName: String)
case class CGDatasource(name: String, location: String, content: String)
case class CGWorkflow(name: String, location: String, content: String)

object Util {

  def jlistToStringList(l: List[JValue]): List[String] = {
    var h = List[String]()
    h = Nil
    for (ll <- l.reverse) {
      val JString(s) = ll
      h = s :: h
    }
    h
  }

  def jlistToMap(pp: List[(String, JValue)]): Map[String, JValue] = {
    val mm = Map[String, JValue]()
    if (pp != null)
      for ((a: String, b: JValue) <- pp)
        mm(a) = b
    mm
  }

  def jlistToHashSet(l: List[JValue]) : HashSet[String] = {
    val h = HashSet[String]()
    for (ll <- l) {
      val JString(s) = ll
      h.add(s)
    }
    h
  }

  // Accessors for properties
  def getP(node: Node, property: String): (Boolean, JValue) = {
    if (!node.properties.contains(property)) return (false, null)
    (true, node.properties(property))
  }
  def getSP(node: Node, property: String): (Boolean, String) = {
    if (!node.properties.contains(property)) return (false, null)
    val JString(theString) = node.properties(property)
    (true, theString)
  }
  def getIP(node: Node, property: String): (Boolean, String) = {
    if (!node.properties.contains(property)) return (false, null)
    val JInt(theInt) = node.properties(property)
    (true, theInt.toString)
  }
  def getDP(node: Node, property: String): (Boolean, String) = {
    if (!node.properties.contains(property)) return (false, null)
    val JDouble(theDouble) = node.properties(property)
    (true, theDouble.toString)
  }
  def getBP(node: Node, property: String): (Boolean, Boolean) = {
    if (!node.properties.contains(property)) return (false, false)
    val JBool(theBoolean) = node.properties(property)
    (true, theBoolean)
  }
  def getArrayListSP(node: Node, property: String): (Boolean, String) = {
    if (!node.properties.contains(property)) return (false, null)
    var i = 0
    var s = ""
    val JArray(columns) =  node.properties(property)
    for (column <- columns) {
      val JString(c) = column
      if (i > 0)
        s = s + ", "
      s += "\"" + c + "\""
      i += 1
    }
    (true, s)
  }
  def getArrayArrayDP(node: Node, property: String): (Boolean, String) = {
    if (!node.properties.contains(property)) return (false, null)
    var i = 0
    var s = "Array("
    val JArray(columns) =  node.properties(property)
    for (column <- columns) {
      if (i > 0)
        s+= ", "
      column match {
        case d : JDouble => val JDouble(dd) = column; s+= dd
        case a : JString => val JString(aa) = column; s+= aa
        case x           => assert(false, "Unknown datatype in double array: " + x)
      }
      i += 1
    }
    s += ")"
    (true, s)
  }
  /*
  def getArrayFirstSP(node: Node, property: String): (Boolean, String) = {
    if (!node.properties.contains(property)) return (false, null)
    val JArray(props)  = node.properties(property)
    val JString(first) = props.head
    (true, first)
  }
  */
  def addReplaceProperty(node: Node, property: String, value: JValue) = {
    node.properties(property) = value
  }
  def addReplacePort(node: Node, property: String, value: JValue) = {
    node.ports(property) = value
  }
  def getFirstOutPort(node: Node) : String = {
    val JArray(outs) = node.ports("outputs")
    val JString(first) = outs.head
    first
  }
  def getListString(list: JValue) : String = {
    pretty(render(list))
  }
  def getListString(list: List[JValue]) : String = {
    val s     = new StringBuilder()
    var first = true
    for (ll <- list) {
      if (!first)
        s++= ", "
      s++= pretty(render(ll))
      first = false
    }
    s.toString()
  }
  def printListString(list: List[String]) : String = {
    val s     = new StringBuilder()
    var first = true
    for (ll <- list) {
      if (!first)
        s++= ", "
      s++= ll
      first = false
    }
    s.toString()
  }
}

