package vizprog.common

import java.io.{File, PrintWriter}

import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3._
import com.amazonaws.services.s3.model._

import scala.collection.JavaConversions._
import scala.io.Source._
import scala.sys.process._

abstract class FileSystem {
  var tempDir: String = null
  var bucketName: String = null

  def createDirectory  (path: String)             : Boolean
  def hasDirectory     (path: String)             : Boolean
  def listDirectoryFiles(path: String)            : List[String]

  def hasFile          (path: String)             : Boolean
  def canReadFile      (path: String)             : Boolean
  def readFile         (path: String)             : String
  def readFileLines    (path: String)             : Iterator[String]
  def writeFile        (path: String, fc: String) : String
  def deleteFile       (path: String)             : Boolean

  def execute(path: String, cmd: Seq[String])     : (Int, String)
}

class LocalFileSystem extends FileSystem {
  tempDir = "/vagrant/tmp"
  bucketName = "" // unused

  override def createDirectory    (path: String)  : Boolean          = new File(path) mkdirs
  override def hasDirectory       (path: String)  : Boolean          = new File(path) exists
  override def listDirectoryFiles (path: String)  : List[String] = null

  override def hasFile            (path: String)  : Boolean          = hasDirectory(path)
  override def canReadFile        (path: String)  : Boolean          = new File(path) canRead
  override def readFile           (path: String)  : String           = fromFile(path).getLines mkString
  override def readFileLines      (path: String)  : Iterator[String] = fromFile(path).getLines
  override def writeFile(path: String, fc: String): String           = {new PrintWriter(path) { write(fc); close};path}
  override def deleteFile         (path: String)  : Boolean          = new File(path) delete

  override def execute(path: String, cmd: Seq[String]): (Int, String) = {
    val stdout = new StringBuilder
    val stderr = new StringBuilder
    val file = new java.io.File(path)
    var status = 1
    try {
      status = Process(cmd, file).!(
        ProcessLogger(
          stdout append _ + "\n",
          stderr append _ + "\n"))
      (status, s"STDOUT:$stdout \nSTDERR:$stderr")
    } catch {
      case e: java.lang.RuntimeException => {
        e.printStackTrace
        (1, s"EXCEPTION:${e.getStackTrace.toString}")
      }
    }
  }

  if (!hasDirectory(tempDir))
    createDirectory(tempDir)
}

class S3FileSystem extends FileSystem {
  val lfs = new LocalFileSystem
  lfs.tempDir = "/tmp"
  tempDir = "/tmp"
  bucketName = "vizprog"

  val credentials = new BasicAWSCredentials (aws.K, aws.S)
  val client      = new AmazonS3Client (credentials)

  override def createDirectory(path: String) : Boolean = { true }
  override def hasDirectory(path: String) : Boolean = { true }
  override def listDirectoryFiles(path: String) : List[String] = {
    var files = List[String]()
    val listing = client.listObjects(
      new ListObjectsRequest().withBucketName(bucketName).withPrefix(path))
    val items = listing.getObjectSummaries
    for (item <- items) {
      val size = item.getSize
      val key = item.getKey
      if (size > 0)
        files = key :: files
    }
    files
  }

  protected def deleteS3Directory(path: String) : Boolean = {
    val listing = client.listObjects(
      new ListObjectsRequest().withBucketName(bucketName).withPrefix(path))
    if (listing != null) {
      val items = listing.getObjectSummaries
      for (item <- items) {
        val size = item.getSize
        if (size == 0)
          deleteS3Directory(path + "/" + item.getKey)
        else
          client.deleteObject(bucketName, item.getKey)
      }
    }
      client.deleteObject(bucketName, path)
    true
  }
  override def deleteFile(path: String) : Boolean = { deleteS3Directory(path) }

  override def hasFile(path: String) : Boolean = { canReadFile(path) }

  override def canReadFile(path: String): Boolean = {
    try { client.getObjectMetadata (bucketName, path) ; true
    } catch {
      case e: AmazonServiceException if e.getStatusCode == 404 => false
      // case x => printf("Uknown Exception in S3 canReadFile" + x); false
    }
  }

  protected def downloadFile(path: String, dpath: String): (Boolean, String) = {
    if (! canReadFile(path))
      throw new Exception(s"File $path is not present or readable")

    client.getObject(new GetObjectRequest(bucketName, path), new File(dpath))
    (true, "Success")
  }

  protected def ensureFileHasDirectory(filePath : String): Unit = {
    lfs.createDirectory(new File(filePath).getParent())
  }

  override def readFile(path: String) : String = {
    val tmpPath = lfs.tempDir + "/dwn/vizprog/" + path
    ensureFileHasDirectory(tmpPath)
    downloadFile(path, tmpPath)
    val content = lfs.readFile(tmpPath)
    lfs.deleteFile(tmpPath)
    content
  }

  override def readFileLines(path: String) : Iterator[String]  = {
    val tmpPath = lfs.tempDir + "/dwn/vizprog/" + path
    downloadFile(path, tmpPath)
    val content = lfs.readFileLines(tmpPath)
    lfs.deleteFile(tmpPath)
    content
  }

  override def writeFile(path: String, fc: String) : String = {
    val localFileName = lfs.tempDir + "/tmp/vizprog/" + path
    lfs.writeFile(localFileName, fc)
    client.putObject(bucketName, path, new File(localFileName))

    val acl = client.getObjectAcl (bucketName, path)
    acl.grantPermission (GroupGrantee.AllUsers, Permission.Read)
    client.setObjectAcl (bucketName, path, acl)
    lfs.deleteFile(localFileName)
    "s3://" + bucketName + "/" + path
  }

  override def execute(path: String, cmd: Seq[String])     : (Int, String) = { (1, "Unimplemented") }
}

