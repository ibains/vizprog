package vizprog.common

import org.json4s.JsonAST.{JObject, JString, JArray}
import org.json4s._

class MLConstruct {
  var allOrderedNodes : List[String] = Nil
  def build(nodeId: String, wf: Workflow) : Unit = {}
}

/**
  *  The Cross Validator has a strict definition where it requires
  *  exactly one of each of the subsumed nodes and requires a handle
  *  to the incoming pipeline that is being cross validation with this
  *  validator
 **/

class MLCrossValidation extends MLConstruct {

  var wf                   : Workflow    = null
  var rootNodeId           : String      = null
  var crossValidatorNodeId : String      = null
  var evaluatorNodeId      : String      = null
  var paramBuilderNodeId   : String      = null
  var incomingPipeline     : MLConstruct = null

  override def build(nodeId: String, wf: Workflow) : Unit = {

  }
}

/**
  *  The ML Classification Pipeline has a strict definition. It has
  *  1. N Features
  *     A feature is an entity from the package org.apache.spark.ml.feature
  *     It adds feature columns to incoming DataFrame
  *     It is a PipelineStage of the kind Transformer
  **/

class MLClassificationPipeline extends MLConstruct {
  var wf                   : Workflow     = null
  var rootNodeId           : String       = null
  var algorithmNodeId      : String       = null
  var inputDataNodeId      : String       = null
  var featureNodes         : List[String] = Nil

  def firstPipelineNodeId = allOrderedNodes.head
  def lastPipelineNodeId  = allOrderedNodes.last

  protected def findFirstNode(nodeId: String) : String = {
    val node =  wf.allNodes(nodeId)
    if (node.children.isEmpty)
      return node.id
    findFirstNode(node.children.head)
  }

  protected def findLastNode(nodeId: String) : String = {
    val node =  wf.allNodes(nodeId)
    if (node.children.isEmpty)
      return node.id
    findLastNode(node.children.last)
  }

  override def build(pipelineNodeId: String, workflow: Workflow) : Unit = {
    wf                   = workflow
    rootNodeId           = pipelineNodeId
    val rootPipelineNode = wf.allNodes(rootNodeId)
    val rootChildren     = rootPipelineNode.allChildren

    for (gnode <- wf.nodeOrder.reverse)
      if (rootChildren.contains(gnode.id))
        allOrderedNodes = gnode.id::allOrderedNodes

    for (oneNode <- allOrderedNodes) {
      val nodeType = MLTypes.getMLNodeType(oneNode, wf)

      nodeType match {

        case MLTypes.MLAlgorithm =>
          assert (algorithmNodeId == null)
          algorithmNodeId = oneNode

        case MLTypes.MLFeature =>
          if (!featureNodes.contains(oneNode))
            featureNodes = (oneNode :: featureNodes.reverse).reverse

        case _     => ; // skip
      }
    }
  }
  def printPipeline() : Unit = {
    val s = new StringBuilder()
    s++= s"Root Node      : $rootNodeId\n"
    s++= s"AllOrderedNodes: ${Util.printListString(allOrderedNodes)}\n"
    s++= s"FeatureNodes   : ${Util.printListString(featureNodes)}\n"
    s++= s"Algorithm Node : $algorithmNodeId\n"
    println(s.toString())
  }
}

object MLPipelines {

  def getEnclosingMLPipelineNode(nodeId: String, wf: Workflow) : MLConstruct = {
    for ((node, pipeline) <- wf.mlPipeline) {
      if (pipeline.allOrderedNodes.contains(nodeId))
        return pipeline
    }
    null
  }

  def isPipelineNode(nodeId: String, wf: Workflow) : Boolean = {
    wf.allNodes(nodeId).component == "MLPipeline"
  }

  def getPipeline(nodeId: String, wf: Workflow) : MLConstruct = {
    val p = new MLClassificationPipeline()
    p.build(nodeId, wf)
    //p.printPipeline()
    p
  }
}

