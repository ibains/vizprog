package vizprog.common

import org.json4s.JsonAST.{JArray, JObject}
import org.json4s._

case class OneFunction(outColumn     : String,
                       functionName  : String,
                       inputColumns  : Array[String],
                       inputLiterals : Array[JValue])

class FunctionList {
  var wf : Workflow = null
  var functions : List[OneFunction] = Nil

  def generate(nodeId: String, inWf: Workflow) : Unit = {
    wf = inWf
    val node = wf.allNodes(nodeId)
    val JArray(scriptNodes) = node.properties("functions")

    for (scriptNode <- scriptNodes) {
      val JString(outColumn)    = scriptNode \ "addReplaceColumn"
      val JString(functionName) = scriptNode \ "functionName"
      val JArray(iColumns)      = scriptNode \ "inputColumns"
      val JArray(iLiterals)     = scriptNode \ "inputLiterals"

      var colNameList : List[String] = Nil
      for (colName <- iColumns) {
        val JString(col) = colName
        colNameList = col :: colNameList
      }
      val inputColumns = colNameList.reverse.toArray[String]

      var literalList : List[JValue] = Nil
      for (literal <- iLiterals) {
        literalList = literal :: literalList
      }
      val inputLiterals = literalList.reverse.toArray[JValue]
      val oneFunction = new OneFunction(outColumn, functionName, inputColumns, inputLiterals)
      functions = oneFunction :: functions
    }
    functions = functions.reverse
  }
}

object TransformFunctions {

  def isTransformFunction(nodeId: String, wf: Workflow) : Boolean = {
    wf.allNodes(nodeId).component == "ApplyFunctions"
  }

  def getTransformFunction(nodeId: String, wf: Workflow) : FunctionList = {
    val fl = new FunctionList()
    fl.generate(nodeId, wf)
    fl
  }
}
