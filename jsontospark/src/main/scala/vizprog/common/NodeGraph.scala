package vizprog.common

// BUILDS A GRAPH TO DETERMINE A VALID ORDERING OF NODES

import scala.collection.mutable.{HashSet, Map}

class NodeGraph(allNodes: Map[String, Node], allEdges: Map[String, Edge]) {
  val nodeDetails = allNodes
  val edgeDetails = allEdges
  val graph       = Map[String, GNode]()
  val mark        = Map[String, Int]()
  var i           = 0

  def getDependenceEdgeId : String = { i = i + 1; "dep" + i }

  def addDependence(predId: String, succId: String) : Unit = {
    val id = getDependenceEdgeId
    edgeDetails(id) = Edge(id, predId, "_dep_", succId, "_dep_")
    graph(predId).outEdges.add(id)
    graph(succId).inEdges.add(id)
    //println(s"\nDEP_EDGE : $predId -> $succId")
  }

  def buildGraph() = {
    for ((k, e) <- nodeDetails)
      graph(k) = GNode(k, HashSet[String](), HashSet[String]())

    for ((k, e) <- edgeDetails) {
      graph(e.startNode).outEdges.add(e.id)
      graph(e.endNode).inEdges.add(e.id)
    }
  }

  def topoSort(): List[GNode] = {
    // Use Tarjan's algorithm
    // mark: unvisited = 0, temp = 1, visited = 2

    def getUnmarkedNode(): GNode = {
      for ((k, e) <- graph)
        if (mark(k) != 2)
          return e
      null
    }

    def visit(n: GNode, l: List[GNode]): List[GNode] = {
      assert(mark(n.id) != 1)                       // cycle, abort
      if (mark(n.id) == 2)                          // visited, skip
        return l
      mark(n.id) = 1                                // mark temporarily for cycle discovery

      var ll = l
      for (edge <- n.outEdges) {
        val suc = edgeDetails(edge).endNode         // visit successors
        ll = visit(graph(suc), ll)
      }

      mark(n.id) = 2                                // mark visited
      return n :: ll                                // add to list, all successors are visited/added
    }

    for ((k, e) <- graph)                           // Initialize Marks as 0
      mark(k) = 0
    var ll = List[GNode]()

    var a = getUnmarkedNode()                       // Next Unmarked vizprog.codegen.Node, works with graph forests
    while (a != null) {
      ll = visit(a, ll)
      a = getUnmarkedNode()
    }
    ll
  }

  def printGraph() = {
    println("\nSummary Graph {")
    for ((k, e) <- graph)
      println(e)
    println("}")
  }
}