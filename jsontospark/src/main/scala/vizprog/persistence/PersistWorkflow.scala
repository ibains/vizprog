package vizprog.persistence

import java.util.concurrent.ConcurrentHashMap

import scala.collection._
import scala.collection.convert.decorateAsScala._
import vizprog.common._

import scala.concurrent.Future

object Persist {
  // Dummy in-mem for single thread
/*
  val interim       = new ConcurMap[InterimWorkflow]()
  val executionIds  = new ConcurMap[String]()
  val executionRes  = new ConcurMap[String]()
  val jobInterimReq = new ConcurMap[String]()
*/

  val interim = new ConcurrentHashMap[String, InterimWorkflow]().asScala
  val executionIds  = new ConcurrentHashMap[String, String]().asScala
  val executionRes = new ConcurrentHashMap[String, (String, String)]().asScala
  val jobInterimReq = new ConcurrentHashMap[String, (String, Boolean)]().asScala
  val interactiveReqs = new ConcurrentHashMap[String, Future[(String, String)]]().asScala

  def addInterimData(wf: InterimWorkflow) : String = { interim.put(wf.id, wf); wf.id }
  def freeInterimData(wfId: String) { interim.remove(wfId); }

  def getInterimData(workflowId: String) : InterimWorkflow = interim.getOrElse(workflowId, null)

  def addExecutionState(workflowId: String, executionId: String, result: String, mimeType: String = "text/plain") : Unit = {
    executionIds.put(workflowId, executionId)
    executionRes.put(executionId, (mimeType, result))
  }
  def getExecutionState(jobId: String) = executionRes.getOrElse(jobId, null)
  def addInterimRequest(jobId: String, request: String, ephemeral: Boolean = false) : Unit = jobInterimReq.put(jobId, (request, ephemeral))
  def getInterimRequest(jobId: String) : (String, Boolean) = jobInterimReq.getOrElse(jobId, (null, false))

  def addInteractiveReq(jobId: String, future: Future[(String, String)]) = interactiveReqs.put(jobId, future)
  def getInteractiveReq(jobId: String) = interactiveReqs.getOrElse(jobId, Future.successful("", ""))
  def removeInteractiveReq(jobId: String) = interactiveReqs.remove(jobId)
}
