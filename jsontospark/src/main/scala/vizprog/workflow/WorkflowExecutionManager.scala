package vizprog.workflow

import java.util.UUID
import java.util.concurrent.ConcurrentHashMap
import scala.collection.convert.decorateAsScala._
import org.json4s.JsonAST.JString
import org.json4s.native.JsonMethods._
import org.slf4j.LoggerFactory
import org.json4s.JsonDSL._
import vizprog.codegen._
import vizprog.common._
import vizprog.communication._
import vizprog.execution._
import vizprog.persistence._
import vizprog.specification.PrintSpecs

import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.{Failure, Success, Try}

/**
  * This class is the primary interface to jsontospark module
  *
  * This class handles code generation and communication with execution service
  *
  * TODO - This code has gotten way too convoluted for the tiny amount it does
  *        this needs to get simplified into 1-2 pages of code
  */

class WorkflowExecutionManager() {

  val workflowParser = new WorkflowParser
  val printer = new WorkflowPrinter
  val resultParser = new ClusterResultParser
  var exec: ExecutionEnvironment = null
  var codegen: WorkflowSpark = null

  // hackhack ---
  val enableHackedCache = true
  val workflowHashes  = new ConcurrentHashMap[String, String]().asScala
  protected def addWorkflowCache(workflowId: String, workflowCode: String): Unit = {
    if (enableHackedCache)
      workflowHashes(workflowId) = workflowCode
  }
  protected def inWorkflowCache(workflowId: String, workflowCode: String): Boolean = {
    enableHackedCache &&
      workflowHashes.contains(workflowId) &&
      workflowHashes(workflowId) == workflowCode
  }
  def clearCache(): String = { workflowHashes.clear(); "Cache Cleared" }
  // ----

  val log = LoggerFactory.getLogger(getClass)

  /**
    * This allows looking at generated Spark code easily without running it
    *
    */

  def getGeneratedCode(workflowId: String,
                       workflowJson: String,
                       mode: Int): (Boolean, String) = {
    refresh()
    val wf = workflowParser.parseWorkflow(workflowJson)
    if (wf == null)
      return (false, "Unable to parse workflow")

    // Generate code for this workflow

    val tmpDir = exec.intermediateRoot
    val interimDir = tmpDir + "tmp" + workflowId

    val sparkCode = codegen.generateSparkForWorkflow(mode, wf, interimDir)
    (true, sparkCode)
  }

  def getId(workflowJson: String): String = {
    workflowParser.getWorkflowId(workflowJson)
  }
  def getMode(workflowJson: String): String = {
    workflowParser.getWorkflowMode(workflowJson)
  }

  /**
    * This is the only interface to get jobs executed
    *
    * For spark-submit, it blocks till the execution is completed (this is debug mode)
    *
    * For job-server, batch and interactive modes,
    * tt submits a job to execution service and returns
    *
    * on success the jobId is returned that is used to get the status and results of execution
    */

  def execute(projectId: String,
              userId: String,
              workflowId: String,
              workflowJson: String,
              mode: Int): Try[(String, Option[String])] = {

    refresh()

    // TODO: Refactor, added to fix the bug (Multiple executions of different requests skips the track progress.)
    if (!enableHackedCache)
      resetSchemaAndSampleInterimData(workflowId)

    implicit val cluster = new ClusterRequest(exec.executionServiceURL)
    log.info("json_to_spark: request to execute workflow : " + workflowId)

    val result = parseWorkflow(workflowJson).flatMap { wf =>
      genCode(wf, mode, workflowId).flatMap { genCodeRes =>
        createCluster(wf, userId).flatMap { clusterId =>
          codegen.cg.target match {
            case CGSparkSubmit =>
              // hackhack: Add a hack to not re-run workflows for faster UI development
              val (generatedCode, interimRequest) = genCodeRes
              if (inWorkflowCache(workflowId, generatedCode)) {
                log.info(s"Found in cache - same workflow, same code: $workflowId, look at results...")
                val (succ, whatever) = getSchemaAndSample(workflowId)
                if (succ) {
                  log.info(s"Found schema & sample for $workflowId, skip execution")
                  return Success(("dummyId", None))
                }
              }
              val res = executeSparkSubmit(workflowId, wf,
                                           clusterId, cluster,
                                           generatedCode, interimRequest).map {
                                             jobId => (jobId, None)
                                           }
              // hackhack - add to cache only when actually executing
              addWorkflowCache(workflowId, generatedCode)
              log.info(s"Add to cache - the code for this workflow: $workflowId")
              return res
            case CGJobServerBatch =>
              val res = createSession(clusterId, userId, projectId, false).map { sessionId =>
                submitBatchJob(sessionId, workflowId, genCodeRes._1).map { jobId =>
                  Persist.addInterimRequest(jobId, genCodeRes._2)
                  (jobId, Some(sessionId))
                }
              }.flatMap(identity)
              res
            case CGInteractiveFunction =>
              val res = createSession(clusterId, userId, projectId, true).map { sessionId =>
                val jobStatus = submitInteractiveJob(sessionId, workflowId, genCodeRes._1, genCodeRes._2)
                // Marking the job id as ephemeral job id for futures.
                Persist.addInterimRequest(jobStatus._1, genCodeRes._2, true)
                Persist.addInteractiveReq(jobStatus._1, jobStatus._2)
                (jobStatus._1, Some(sessionId))
              }.map(identity)
              res
            case CGDeploy =>
              val (generatedCode, interimRequest) = genCodeRes
              val (res, json) = executeDeploySubmit(clusterId, workflowId, cluster, generatedCode)
              log.info(s"Execute deployment response: $res json: $json")
              res match {
                case true => return Success(("OK", Some(json)))
                case false => return Failure(new RuntimeException("Failed Deploy Request"))
              }
            case _ =>
              Failure(new RuntimeException("Unknown execution mode."))
          }
        }
      }
    }
    result
  }

  /**
    * Refresh needs to be called every time new code is generated
    *
    */

  protected def refresh(): Unit = {
    codegen = new WorkflowSpark
  } // CODEGEN KEEPS STATE

  def printResult(input: String) : Unit = {
    log.info(input)
  }

  protected def executeDeploySubmit(clusterId: String,
                                    workflowId: String,
                                    cluster: ClusterRequest,
                                    sparkCode: String) : (Boolean, String) = {

    val addResultFunction = addDeployResult(workflowId) _

    log.info("json_to_spark: deploy compile submit : " + workflowId)


    val compileResult =
      cluster.submitCompileDeployScriptToCluster(clusterId,
                                                 workflowId,
                                                 sparkCode,
                                                 addResultFunction)

    if (!compileResult)
      return (false, compact(render(("error" -> "Deployment compilation failure"))))

    log.info("json_to_spark: deploy compile success: " + workflowId)

    val jobId = Persist.executionIds.getOrElse(workflowId, null)

    log.info(s"json_to_spark: deploy: for workflow $workflowId found job $jobId")

    val execResult =
      cluster.submitExecuteDeployScriptToCluster(jobId,
                                                  clusterId,
                                                  addResultFunction)

    if (!execResult)
      return (false, compact(render(("error" -> "Deployment execution failure"))))

    cluster.destroyCluster(workflowId)

    val json = Persist.executionRes.getOrElse(jobId, null)._2

    (true, json)
  }

  /**
    * This is synchronous and runs spark-submit script
    * This is the base implementation kept for debugging purposes
    *
    */

  protected def executeSparkSubmit(workflowId: String,
                                   wf: Workflow,
                                   clusterId: String,
                                   cluster: ClusterRequest,
                                   sparkCode: String,
                                   interimRequest: String): Try[String] = {

    val addResultFunction = addExecResult(workflowId) _
    log.info("json_to_spark: compile submit : " + workflowId)
    val compileResult =
      cluster.submitCompileBatchScriptToCluster(clusterId,
                                                workflowId,
                                                sparkCode,
                                                addResultFunction)

    if (!compileResult)
      Failure(new RuntimeException("Compile Failure"))

    log.info("json_to_spark: compile success: " + workflowId)

    val jobId = Persist.executionIds.getOrElse(workflowId, null)
    Persist.addInteractiveReq(jobId, Future.successful(("", "")))

    log.info("json_to_spark: execution submit: " + workflowId + ", jobId : " + jobId)

    val execResult =
      cluster.submitExecuteBatchScriptToCluster(jobId,
                                                clusterId,
                                                addResultFunction)

    if (!execResult)
      Failure(new RuntimeException("Execution Failure"))

    log.info("json_to_spark: execution success: " + workflowId + ", jobId : " + jobId)

    // If we're here, the job has succeeded, we can get interim files now...
    val interimFileFunction = addInterimFiles(workflowId) _

    log.info("json_to_spark: interim request: " + workflowId)

    val interim =
      cluster.fetchInterimFiles(workflowId,
                                interimRequest,
                                interimFileFunction)

    if (!interim)
      Failure(new RuntimeException("Unable to fetch interim data"))

    log.info("json_to_spark: interim success: " + workflowId)

    cluster.destroyCluster(workflowId)

    Success(jobId)
  }

  protected def addExecResult(workflowId: String)(source: String): Unit = {
    val (jobId, status) = resultParser.parseBatchResult(source)
    Persist.addExecutionState(workflowId, jobId, status)
  }

  protected def addDeployResult(workflowId: String)(source: String): Unit = {
    val (jobId, _) = resultParser.parseBatchResult(source)
    Persist.addExecutionState(workflowId, jobId, source, "application/json")
  }

  /**
    * These functions are passed as arguments to functions that talk to execution service
    *
    * They are helpers to save state
    *
    */

  protected def addInterimFiles(workflowId: String)(source: String): Unit = {
    val interimWorkflow = resultParser.parseInterim(workflowId, source)
    Persist.addInterimData(interimWorkflow)
  }

  private def submitInteractiveJob(sessionId: String,
                                   workflowId: String,
                                   sparkCode: String,
                                   interimRequest: String)
                                  (implicit clusterReq: ClusterRequest): (String, Future[(String, String)]) = {

    // Creating an ephemeral job id for references.
    val jobId = "EPHEMERAL-JOB-" + UUID.randomUUID().toString
    val future = Future[(String, String)] {

      log.info("Submitting the interactive job for execution...")
      val submitResult =
        clusterReq.submitInteractiveJob(sessionId,
                                        workflowId,
                                        sparkCode)

      submitResult.code match {
        case 200 =>
          val resJson = parse(submitResult.body)
          val JString(mimeType) = (resJson \ "mime")
          val JString(contents) = (resJson \ "contents")
          log.info(s"Successfully executed the interactive code with result mime type ${mimeType}")
          (mimeType, contents)
        case _ =>
          val resJson = parse(submitResult.body)
          val JString(status) = (resJson \ "status")
          val JString(error)  = (resJson \ "error")
          val traceback       = (resJson \ "traceback")
          val tracebackPretty = pretty(render(traceback))
          throw new RuntimeException(s"Failed to execute the interactive code, Reason: ${error}, Traceback: ${tracebackPretty}")
      }
    }
    (jobId, future)
  }

  private def submitBatchJob(sessionId: String, workflowId: String, sparkCode: String)(implicit clusterReq: ClusterRequest): Try[String] = {

    log.info("Submitting the batch job for execution...")
    val submitResult =
      clusterReq.submitBatchJob(sessionId,
        workflowId,
        sparkCode)

    val jobId =
      resultParser.parseJobId(
        resultParser.parseBodyResponse(
          submitResult
        ))

    log.info(s"Job Submitted with the job id $jobId")

    if (jobId == null)
      Failure(new RuntimeException("Did not get a valid jobId"))
    else
      Success(jobId)
  }

  private def parseWorkflow(workflowJson: String) = {
    val wf = workflowParser.parseWorkflow(workflowJson)
    if (wf == null)
      Failure(new RuntimeException("Unable to parse workflow"))
    else
      Success(wf)

  }

  private def genCode(wf: Workflow, mode: Int, workflowId: String): Try[(String, String)] = {

    Try {
      val interimDir = s"""${exec.intermediateRoot}tmp$workflowId"""
      val sparkCode =
        codegen.generateSparkForWorkflow(mode,
          wf,
          interimDir)

      val interimRequest =
        codegen.getInterimRequest(wf,
          interimDir)

      (sparkCode, interimRequest)
    }
  }

  private def createSession(clusterId: String, userId: String, projectId: String, developerMode: Boolean)(implicit clusterReq: ClusterRequest): Try[String] = {
    log.info("Creating a new session...")
    val sessionResult =
      clusterReq.addSession(clusterId,
        userId,
        projectId,
        developerMode)

    val sessionId =
      resultParser.parseSessionId(
        resultParser.parseBodyResponse(
          sessionResult
        ))

    log.info(s"Session created with the session id $sessionId")
    if (sessionId == null)
      Failure(new RuntimeException("Unable to create session on jobserver"))
    else
      Success(sessionId)
  }

  private def createCluster(wf: Workflow, userId: String)(implicit clusterReq: ClusterRequest): Try[String] = {
    log.info("Creating a new cluster...")
    val clusterResult =
      clusterReq.setupCluster(wf.execution.memory,
        wf.execution.processors,
        userId)

    val clusterId =
      resultParser.parseClusterId(
        resultParser.parseBodyResponse(
          clusterResult
        ))

    log.info(s"Cluster created with cluster id $clusterId")

    if (clusterId == null)
      Failure(new RuntimeException("Enable to create cluster"))
    else
      Success(clusterId)
  }

  /**
    * This asynchronously polls the execution service till the job completes or fails
    *
    * For completed jobs, interim data is returned
    *
    * It updates the current job status on every poll, and that can be fetched through
    * getStatus function
    *
    */

  def getInterimResults(jobId: String, sessionId: Option[String], workflowId: String): Future[String] = {

    import scala.concurrent.duration._
    val cluster = new ClusterRequest(exec.executionServiceURL)
    val p = Promise[String]()

    Future {
      // We might already have the results for spark-submit
      val (succ, res) = getSchemaAndSample(workflowId)
      if (succ) {
        log.warn("Found already saved interim results.")
        p.success(res)
      } else {

        //
        // Poll the execution service till the job fails or succeeds, updating status
        //

        log.info(s"Tracking the progress of the job $jobId")
        var iterate = true
        var success = false
        var result = ""
        var error = ""
        val ephemeral: Boolean = Persist.getInterimRequest(jobId)._2

        while (iterate) {
          log.info("Fetching result.")
          if (!ephemeral) {
            val jobStatus = cluster.getJobStatus(jobId)
            log.info(s"Parsing result ${jobStatus.code}, ${jobStatus.body}")
            val jobResult =
              resultParser.parseJobStatus(
                resultParser.parseBodyResponse(
                  jobStatus
                ))

            log.info(s"Got the status of the job ${jobId}, Results: ${jobResult}")
            Persist.addExecutionState(workflowId,
              jobId,
              jobResult._1)
            log.info("Processing the results.")
            jobResult._1 match {
              case "Completed" => success = true; iterate = false; result = jobResult._2
              case "Failed" => success = false; iterate = false; error = jobResult._2
              case "Compiling" =>
              case "Triggered" =>
              case "Running" =>
              case "Submitting" =>
              case x => success = false; iterate = true; print("\nUnknown State: " + x)
            }
          } else {
            val jobStatus = Persist.getInteractiveReq(jobId)
            log.info(s"Got the status of the job ${jobId}, Completed Status: ${jobStatus.isCompleted}")
            if(jobStatus.isCompleted) {
              val interResult = jobStatus.value.get
              interResult match {
                case Success(futureRes) =>
                  success = true
                  iterate = false
                  result = futureRes._2
                case Failure(ex) =>
                  success = false
                  iterate = false
                  error = ex.getMessage
              }
            }
          }
          Thread.sleep(10000)
        }

        //
        // Polling is done, we have a status
        //

        if (!success) {
          p.failure(new Exception(s"Job did not execute successfully, Reason: ${error}"))
        } else {

          //
          // We ran successfully, now fetch interim files
          //
          log.info(s"Job Completed Successfully with result : ${result}\nTracking interim data now.")
          val interimFileFunction = addInterimFiles(workflowId) _
          val interimRequest = Persist.getInterimRequest(jobId)

          val interim =
            cluster.fetchInterimFiles(workflowId,
              interimRequest._1,
              interimFileFunction)

          if (!interim) {
            p.failure(new Exception("Unable to fetch interim data"))
          } else {

            //
            // Interim files fetched, return schema and sample
            //
            val (succ, res) = getSchemaAndSample(workflowId)
            if (!succ) p.failure(new Exception("Unable to get schema and sample due to local error"))
            else p.success(res)
          }
        }
      }

      // Always destroy the cluster
      if(sessionId.isDefined) {
        log.info(s"Destroying the session ${sessionId}")
        cluster.destroySession(sessionId.get)
      }
      cluster.destroyCluster(workflowId)
    }

    p.future
  }

  /**
    * This returns the interim data for a workflow that has executed
    *
    */

  def getSchemaAndSample(workflowId: String): (Boolean, String) = {
    val interimWorkflow = Persist.getInterimData(workflowId)
    if (interimWorkflow == null)
      return (false, "Could not find interim data")
    (true, printer.printInterimWorkflow(interimWorkflow))
  }

  /**
    * This returns the interim data for a workflow that has executed
    *
    */

  def resetSchemaAndSampleInterimData(workflowId: String) {
    // This is added to get rid of an existing bug. Multiple executions of different run types stops to pull the results.
    Persist.freeInterimData(workflowId)
  }



  /**
    * This return the status of an executing job
    *
    */

  def getStatus(workflowId: String, jobId: String): String = Persist.getExecutionState(jobId)._2


  /**
    * This returns the spec used to construct cleansing functions
    *
    */

  def getFunctionSpecification: String = PrintSpecs.getSpecFunctionJson()

  /**
    * This returns the spec used to construct nodes
    *
    */

  def getNodeSpecification: String = PrintSpecs.getSpecNodeJson()

}


