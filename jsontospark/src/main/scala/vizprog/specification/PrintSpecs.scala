package vizprog.specification

import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.native.JsonMethods._
import vizprog.common.MLTypes
import scala.collection.mutable.{Map, ListBuffer}

object PrintSpecs {

  var sDateTime        : SpecDateTime        = null
  var sDataType        : SpecDataType        = null
  var sMathFunctions   : SpecMathFunctions   = null
  var sStringFunctions : SpecStringFunctions = null
  var sAggFunctions    : SpecAggFunctions    = null
  var sMiscFunctions   : SpecMiscFunctions   = null
  var sNodesSQL        : SpecNodeSQL         = null
  var sNodesML         : SpecNodeML          = null

  protected def initialize = {
    if (sDateTime        == null) sDateTime        = new SpecDateTime
    if (sDataType        == null) sDataType        = new SpecDataType
    if (sMathFunctions   == null) sMathFunctions   = new SpecMathFunctions
    if (sStringFunctions == null) sStringFunctions = new SpecStringFunctions
    if (sAggFunctions    == null) sAggFunctions    = new SpecAggFunctions
    if (sMiscFunctions   == null) sMiscFunctions   = new SpecMiscFunctions
    if (sNodesSQL        == null) sNodesSQL        = new SpecNodeSQL
    if (sNodesML         == null) sNodesML         = new SpecNodeML
  }

  protected def oneNameValueJson(a: NameValue) : JObject = {
    val json =
      ("name" -> a.name) ~
        ("datatype" -> a.dataType)
    json
  }
  protected def listNameValueJson(ats: Array[NameValue]) : JArray = {
    val lb = ListBuffer[JsonAST.JObject]()
    if (ats != null)
      for (k <- ats)
        lb.append(oneNameValueJson(k))
    JArray(lb.toList)
  }
  protected def listPortsJson(ats: scala.collection.immutable.Map[String, String]) : JObject = {
    val inputs  = ListBuffer[JsonAST.JString]()
    val outputs = ListBuffer[JsonAST.JString]()

    for ((k, v) <- ats) {
      if (v == "input")
        inputs.append(JString(k))
      else
        outputs.append(JString(k))
    }
    val inputArray  = JArray(inputs.toList)
    val outputArray = JArray(outputs.toList)
    val json =
      ("inputs" -> inputArray) ~
        ("outputs" -> outputArray)
    json
  }
  protected def onePropertyJson(p: (NameValue, String)) : (String, JObject) = {
    val (nv, description) = p
    val json =
      (nv.name ->
        ("name" -> nv.name) ~
        ("datatype" -> nv.dataType) ~
        ("description" -> description))
    json
  }
  protected def listNodeProperties(ats: scala.collection.immutable.Map[String, (NameValue, String)]) : JObject = {
    val lb = ListBuffer[(String, JObject)]()
    for ((k, v) <- ats)
      lb.append(onePropertyJson(v))
    JObject(lb.toList)
  }
  protected def oneFunctionJson(name: String, f: OneFunction): (String, JObject) = {
    val nameValues = listNameValueJson(f.inputTypes)
    val json =
      (name ->
        ("name" -> f.name) ~
          ("outputType" -> f.outputType) ~
          ("numInputs" -> f.numInputs) ~
          ("inputTypes" -> nameValues) ~
          ("description" -> f.description))
    json
  }
  protected def oneDataTypeJson(name: String, f: OneDataType): (String, JObject) = {
    val comps = listNameValueJson(f.components)
    val json =
      (name ->
        ("name" -> f.name) ~
          ("IsNumeric" -> f.isNumeric) ~
          ("IsDatetime" -> f.isDatetime) ~
          ("IsAggregate" -> f.isAggregate) ~
          ("inputTypes" -> comps) ~
          ("description" -> f.description))
    json
  }

  protected def oneNodeToJson(name: String, n: OneNode) : (String, JObject) = {
    val json =
      (name ->
        ("name" -> n.component) ~
          ("description" -> n.description) ~
          ("nodeType" -> MLTypes.getString(n.nodeType)) ~
          ("ports" -> listPortsJson(n.ports)) ~
          ("properties" -> listNodeProperties(n.properties))
        )
    json
  }
  protected def functionMapToJson(ats: Map[String, OneFunction]) : JObject = {
    val lb = ListBuffer[(String, JObject)]()
    for ((k, v) <- ats)
      lb.append(oneFunctionJson(k, v))
    JObject(lb.toList)
  }
  protected def dataMapToJson(ats: Map[String, OneDataType]) : JObject = {
    val lb = ListBuffer[(String, JObject)]()
    for ((k, v) <- ats)
      lb.append(oneDataTypeJson(k, v))
    JObject(lb.toList)
  }
  protected def nodeMapToJson(ats: Map[String, OneNode]) : JObject = {
    val lb = ListBuffer[(String, JObject)]()
    for ((k, v) <- ats)
      lb.append(oneNodeToJson(k, v))
    JObject(lb.toList)
  }

  def getSpecFunctionJson(): String = {

    initialize

    val json =
      ("Specification" ->
        ("DataType" -> dataMapToJson(sDataType.dataTypes)) ~
          ("StringFunctions" -> functionMapToJson(sStringFunctions.stringFunctions)) ~
          ("DateTimeFunctions" -> functionMapToJson(sDateTime.dateTimeFunctions)) ~
          ("AggregateFunctions" -> functionMapToJson(sAggFunctions.aggFunctions))~
          ("MiscFunctions" -> functionMapToJson(sMiscFunctions.miscFunctions))~
          ("MathFunctions" -> functionMapToJson(sMathFunctions.mathFunctions))
        )

    pretty(render(json))
  }

  def getSpecNodeJson(): String = {

    initialize

    val json =
      ("Specification" ->
        ("API_VERSION" -> sNodesML.API_VERSION) ~
          ("MLNodes" -> nodeMapToJson(sNodesML.allMLNodes))
        )

    pretty(render(json))
  }
}
