package vizprog.specification

import scala.collection.mutable.Map


class SpecMathFunctions {

  val API_VERSION = "1.6.1"

  val mathFunctions = Map[String, OneFunction]()

  mathFunctions("acos") = OneFunction("acos", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the cosine inverse of the given column; the returned angle is in the range 0.0 through pi.")

  mathFunctions("acos_c") = OneFunction("acos", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the cosine inverse of the given value; the returned angle is in the range 0.0 through pi.")

  mathFunctions("asin") = OneFunction("asin", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the sine inverse of the given column; the returned angle is in the range -pi/2 through pi/2.")

  mathFunctions("asin_c") = OneFunction("asin", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the sine inverse of the given value; the returned angle is in the range -pi/2 through pi/2.")

  mathFunctions("atan") = OneFunction("atan", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the tangent inverse of the given column.")

  mathFunctions("atan_c") = OneFunction("atan", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the tangent inverse of the given value.")

  mathFunctions("atan2_n_n") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("rightName", "StringType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("atan2_l_n") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("l", "DoubleType"), NameValue("rightName", "StringType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("atan2_l_c") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("l", "DoubleType"), NameValue("r", "ColumnType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("atan2_n_r") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("r", "DoubleType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("atan2_c_r") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("r", "DoubleType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("atan2_n_c") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("r", "ColumnType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("atan2_c_n") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("rightName", "StringType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("atan2_c_c") = OneFunction("atan2", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("r", "ColumnType")),
    "Returns the angle theta from the conversion of rectangular coordinates (x, y) to polar coordinates (r, theta).")

  mathFunctions("bin") = OneFunction("bin", "StringType", 1,
    Array(NameValue("columnName", "StringType")),
    "An expression that returns the string representation of the binary value of the given long column.")

  mathFunctions("bin_c") = OneFunction("bin", "StringType", 1,
    Array(NameValue("expression", "ColumnType")),
    "An expression that returns the string representation of the binary value of the given long value.")

  mathFunctions("cbrt") = OneFunction("cbrt", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the cube-root of the given column.")

  mathFunctions("cbrt_c") = OneFunction("cbrt", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the cube-root of the given value.")

  mathFunctions("ceil") = OneFunction("cbrt", "LongType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the ceiling of the given column.")

  mathFunctions("ceil_c") = OneFunction("cbrt", "LongType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the ceiling of the given value.")

  mathFunctions("conv") = OneFunction("conv", "LongType", 3,
    Array(NameValue("num", "ColumnType"), NameValue("fromBase", "IntType"), NameValue("toBase", "IntType")),
    "Convert a number in a string column from one base to another.")

  mathFunctions("cos") = OneFunction("cos", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the cosine of the given column.")

  mathFunctions("cos_c") = OneFunction("cos", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the cosine of the given value.")

  mathFunctions("cosh") = OneFunction("cosh", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the cosine of the given column.")

  mathFunctions("cosh_c") = OneFunction("cosh", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the cosine of the given value.")

  mathFunctions("exp") = OneFunction("exp", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the exponential of the given column.")

  mathFunctions("exp_c") = OneFunction("exp", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the exponential of the given value.")

  mathFunctions("expm1") = OneFunction("expm1", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the exponential of the given column minus one.")

  mathFunctions("expm1_c") = OneFunction("expm1", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the exponential of the given value minus one.")

  mathFunctions("factorial_c") = OneFunction("expm1", "LongType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the factorial of the given value.")

  mathFunctions("floor") = OneFunction("floor", "LongType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the floor of the given column.")

  mathFunctions("floor_c") = OneFunction("floor", "LongType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the floor of the given value.")

  mathFunctions("hex_c") = OneFunction("hex", "StringType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes hex value of the given column.")

  mathFunctions("hypot_n_n") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("rightName", "StringType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("hypot_l_n") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("l", "DoubleType"), NameValue("rightName", "StringType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("hypot_l_c") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("l", "DoubleType"), NameValue("r", "ColumnType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("hypot_n_r") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("r", "DoubleType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("hypot_c_r") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("r", "DoubleType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("hypot_n_c") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("r", "ColumnType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("hypot_c_n") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("rightName", "StringType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("hypot_c_c") = OneFunction("hypot", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("r", "ColumnType")),
    "Computes sqrt(a2 + b2) without intermediate overflow or underflow.")

  mathFunctions("pow_n_n") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("rightName", "StringType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("pow_l_n") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("l", "DoubleType"), NameValue("rightName", "StringType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("pow_l_c") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("l", "DoubleType"), NameValue("r", "ColumnType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("pow_n_r") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("r", "DoubleType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("pow_c_r") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("r", "DoubleType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("pow_n_c") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("leftName", "StringType"), NameValue("r", "ColumnType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("pow_c_n") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("rightName", "StringType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("pow_c_c") = OneFunction("pow", "DoubleType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("r", "ColumnType")),
    "Returns the value of the first argument raised to the power of the second argument.")

  mathFunctions("log") = OneFunction("log", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the natural logarithm of the given column.")

  mathFunctions("log_c") = OneFunction("log", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the natural logarithm of the given value.")

  mathFunctions("log_b") = OneFunction("log", "DoubleType", 2,
    Array(NameValue("base", "DoubleType"), NameValue("columnName", "StringType")),
    "Returns the first argument-base logarithm of the second argument.")

  mathFunctions("log_b_c") = OneFunction("log", "DoubleType", 2,
    Array(NameValue("base", "DoubleType"), NameValue("expression", "ColumnType")),
    "Returns the first argument-base logarithm of the second argument.")

  mathFunctions("log10") = OneFunction("log10", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the logarithm of the given column in base 10.")

  mathFunctions("log10_c") = OneFunction("log10", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the logarithm of the given value in base 10.")

  mathFunctions("log1p") = OneFunction("log1p", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the natural logarithm of the given column plus one.")

  mathFunctions("log1p_c") = OneFunction("log1p", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the natural logarithm of the given value plus one.")

  mathFunctions("log2") = OneFunction("log2", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the logarithm of the given column in base 2.")

  mathFunctions("log2_c") = OneFunction("log2", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the logarithm of the given value in base 2.")

  mathFunctions("pmod") = OneFunction("pmod", "DoubleType", 2,
    Array(NameValue("dividend", "ColumnType"), NameValue("divisor", "ColumnType")),
    "Returns the positive value of dividend mod divisor.")

  mathFunctions("rint") = OneFunction("rint", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Returns the double value that is closest in value to the argument and is equal to a mathematical integer.")

  mathFunctions("rint_c") = OneFunction("rint", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Returns the double value that is closest in value to the argument and is equal to a mathematical integer.")

  mathFunctions("round") = OneFunction("round", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Returns the value of the column e rounded to 0 decimal places.")

  mathFunctions("round_s") = OneFunction("round", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType"), NameValue("scale", "IntType")),
    "Round the value of e to scale decimal places if scale >= 0 or at integral part when scale < 0.")

  mathFunctions("shiftLeft") = OneFunction("shiftLeft", "IntType", 1,
    Array(NameValue("expression", "ColumnType"), NameValue("numBits", "IntType")),
    "Shift the the given value numBits left.")

  mathFunctions("shiftRight") = OneFunction("shiftRight", "IntType", 1,
    Array(NameValue("expression", "ColumnType"), NameValue("numBits", "IntType")),
    "Shift the the given value numBits right.")

  mathFunctions("shiftRightUnsigned") = OneFunction("shiftRightUnsigned", "IntType", 1,
    Array(NameValue("expression", "ColumnType"), NameValue("numBits", "IntType")),
    "Unsigned shift the the given value numBits right.")

  mathFunctions("signum") = OneFunction("signum", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the signum of the given column.")

  mathFunctions("signum_c") = OneFunction("signum", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the signum of the given value.")

  mathFunctions("sin") = OneFunction("sin", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the sine of the given column.")

  mathFunctions("sin_c") = OneFunction("sin", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the sine of the given value.")

  mathFunctions("sinh") = OneFunction("sinh", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the hyperbolic sine of the given column.")

  mathFunctions("sinh_c") = OneFunction("sinh", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the hyperbolic sine of the given value.")

  mathFunctions("sqrt") = OneFunction("sqrt", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the square root of the specified float column.")

  mathFunctions("sqrt_c") = OneFunction("sqrt", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the square root of the specified float value.")

  mathFunctions("tan") = OneFunction("tan", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the tangent of the given column.")

  mathFunctions("tan_c") = OneFunction("tan", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the tangent of the given value.")

  mathFunctions("tanh") = OneFunction("tanh", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Computes the hyperbolic tangent of the given column.")

  mathFunctions("tanh_c") = OneFunction("tanh", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Computes the hyperbolic tangent of the given value.")

  mathFunctions("toDegrees") = OneFunction("toDegrees", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Converts an angle measured in radians to an approximately equivalent angle measured in degrees.")

  mathFunctions("toDegrees_c") = OneFunction("toDegrees", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Converts an angle measured in radians to an approximately equivalent angle measured in degrees.")

  mathFunctions("toRadians") = OneFunction("toRadians", "DoubleType", 1,
    Array(NameValue("columnName", "StringType")),
    "Converts an angle measured in degrees to an approximately equivalent angle measured in radians.")

  mathFunctions("toRadians_c") = OneFunction("toRadians", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Converts an angle measured in degrees to an approximately equivalent angle measured in radians.")

  mathFunctions("unhex") = OneFunction("unhex", "DoubleType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Inverse of hex.")

}
