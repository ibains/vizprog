package vizprog.specification

//import scala.collection.mutable.Map

import vizprog.common.MLTypes

case class NameValue(  name        : String,
                       dataType    : String)

case class OneDataType(name        : String,
                       isNumeric   : Boolean,
                       isDatetime  : Boolean,
                       isAggregate : Boolean,
                       components  : Array[NameValue],
                       description : String)

case class OneFunction(name        : String,
                       outputType  : String,
                       numInputs   : Int,
                       inputTypes  : Array[NameValue],
                       description : String)

case class OneNode(component  : String,
                   description: String,
                   ports      : Map[String, String],
                   properties : Map[String, (NameValue, String)],
                   nodeType   : MLTypes.MLType)

