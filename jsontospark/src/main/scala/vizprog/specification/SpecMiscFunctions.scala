package vizprog.specification

import scala.collection.mutable.Map

class SpecMiscFunctions {


  val API_VERSION = "1.6.1"

  val miscFunctions = Map[String, OneFunction]()

  miscFunctions("column_expression") = OneFunction("column_expression", "Any", 1,
    Array(NameValue("expression", "StringType")),
    "Computes a Spark column expression.")

  miscFunctions("array") = OneFunction("array", "ArrayType", 1,
    Array(NameValue("columnNames", "ColumnVarargType")),
    "Creates a new array column. The input columns must all have the same data type.")

  miscFunctions("abs") = OneFunction("abs", "Any", 1,
    Array(NameValue("e", "ColumnType")),
    "Computes the absolute value.")

  miscFunctions("negate") = OneFunction("negate", "Any", 1,
    Array(NameValue("columnName", "ColumnType")),
    "Unary minus, i.e. e the expression.")

  miscFunctions("not") = OneFunction("not", "BooleanType", 1,
    Array(NameValue("e", "ColumnType")),
    "Computes the absolute value.")

  miscFunctions("isnan") = OneFunction("isnan", "BooleanType", 1,
    Array(NameValue("e", "ColumnType")),
    "Return true iff the column is NaN.")

  miscFunctions("isnull") = OneFunction("isnull", "BooleanType", 1,
    Array(NameValue("e", "ColumnType")),
    "Return true iff the column is null.")

  miscFunctions("bitwiseNOT") = OneFunction("bitwiseNOT", "Any", 1,
    Array(NameValue("e", "ColumnType")),
    "Computes bitwise NOT.")

  miscFunctions("sha2") = OneFunction("sha2", "StringType", 2,
    Array(NameValue("e", "ColumnType"), NameValue("numBits", "IntType")),
    "Calculates the SHA-2 family of hash functions of a binary column and returns the value as a hex string." +
      "e: column to compute SHA-2 on. numBits: one of 224, 256, 384, or 512.")

  miscFunctions("sha1") = OneFunction("sha1", "StringType", 1,
    Array(NameValue("e", "ColumnType")),
    "Calculates the SHA-1 digest of a binary column and returns the value as a 40 character hex string")

  miscFunctions("md5") = OneFunction("md5", "StringType", 1,
    Array(NameValue("e", "ColumnType")),
    "Calculates the MD5 digest of a binary column and returns the value as a 32 character hex string")

  miscFunctions("crc32") = OneFunction("crc32", "LongType", 1,
    Array(NameValue("e", "ColumnType")),
    "Calculates the cyclic redundancy check value (CRC32) of a binary column and returns the value as a bigint.")

  miscFunctions("rand") = OneFunction("rand", "DoubleType", 0,
    null,
    "Generate a random column with i.i.d. samples from U[0.0, 1.0].")

  miscFunctions("rand_s") = OneFunction("rand", "DoubleType", 1,
    Array(NameValue("seed", "LongType")),
    "Generate a random column with i.i.d. samples from U[0.0, 1.0].")

  miscFunctions("randn") = OneFunction("randn", "DoubleType", 0,
    null,
    "Generate a column with i.i.d. samples from the standard normal distribution.")

  miscFunctions("randn_s") = OneFunction("randn", "DoubleType", 1,
    Array(NameValue("seed", "LongType")),
    "Generate a column with i.i.d. samples from the standard normal distribution.")

  miscFunctions("asc") = OneFunction("asc", "Any", 1,
    Array(NameValue("ColumnName", "StringType")),
    "Returns a sort expression based on ascending order of the column.")

  miscFunctions("desc") = OneFunction("desc", "Any", 1,
    Array(NameValue("ColumnName", "StringType")),
    "Returns a sort expression based on the descending order of the column.")

  miscFunctions("coalesce") = OneFunction("coalesce", "Any", 1,
    Array(NameValue("e", "ColumnVarargType")),
    "Returns the first column that is not null, or null if all inputs are null. For example, " +
      "coalesce(a, b, c) will return a if a is not null, or b if a is null and b is not null, or c " +
      "if both a and b are null but c is not null.")

  miscFunctions("greatest") = OneFunction("greatest", "Any", 2,
    Array(NameValue("ColumnName", "StringType"), NameValue("ColumnNames", "StringVarargType")),
    "Returns the greatest value of the list of column names, skipping null values. This function takes at " +
      "least 2 parameters. It will return null iff all parameters are null.")

  miscFunctions("greatest_c") = OneFunction("greatest", "Any", 2,
    Array(NameValue("exprs", "ColumnVarargType")),
    "Returns the greatest value of the list of values, skipping null values. This function takes at least 2 " +
      "parameters. It will return null iff all parameters are null.")

  miscFunctions("least") = OneFunction("least", "Any", 2,
    Array(NameValue("ColumnName", "StringType"), NameValue("ColumnNames", "StringVarargType")),
    "Returns the least value of the list of column names, skipping null values. This function takes at least " +
      "2 parameters. It will return null iff all parameters are null.")

  miscFunctions("lit") = OneFunction("lit", "Any", 1,
    Array(NameValue("literal", "Any")),
    "The passed in object is returned directly if it is already a Column. If the object is a Scala Symbol, " +
      "it is converted into a Column also. Otherwise, a new Column is created to represent the literal value.")

  miscFunctions("monotonically_increasing_id") = OneFunction("monotonically_increasing_id", "LongType", 0,
    null,
    "A column expression that generates monotonically increasing 64-bit integers. The generated ID is guaranteed " +
      "to be monotonically increasing and unique, but not consecutive. The current implementation puts the partition " +
      "ID in the upper 31 bits, and the record number within each partition in the lower 33 bits. The assumption " +
      "is that the data frame has less than 1 billion partitions, and each partition has less than 8 billion records." +
      "As an example, consider a DataFrame with two partitions, each with 3 records. This expression would return " +
      "the following IDs: 0, 1, 2, 8589934592 (1L << 33), 8589934593, 8589934594.")

  miscFunctions("nanvl") = OneFunction("nanvl", "DoubleType", 1,
    Array(NameValue("col1", "ColumnType"), NameValue("col2", "ColumnType")),
    "Returns col1 if it is not NaN, or col2 if col1 is NaN. Both inputs should be floating point " +
      "columns (DoubleType or FloatType).")
}
