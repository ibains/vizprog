package vizprog.specification

import scala.collection.mutable.Map

class SpecDateTime {

  val API_VERSION = "1.6.1"

  val dateTimeFunctions = Map[String, OneFunction]()

  dateTimeFunctions("add_months") = OneFunction("add_months", "DateType", 2,
    Array(NameValue("startDate", "ColumnType"), NameValue("numMonths", "IntType")),
    "Returns the date that is numMonths after startDate.")

  dateTimeFunctions("current_date") = OneFunction("current_date", "DateType", 0,
    null,
    "Returns the current date as a date column.")

  dateTimeFunctions("current_timestamp") = OneFunction("current_timestamp", "DateType", 0,
    null,
    "Returns the current timestamp as a timestamp column.")

  dateTimeFunctions("date_add") = OneFunction("date_add", "DateType", 2,
    Array(NameValue("startDate", "ColumnType"), NameValue("days", "IntType")),
    "Returns the date that is days days after start.")

  dateTimeFunctions("date_format") = OneFunction("date_format", "StringType", 2,
    Array(NameValue("dateExpr", "ColumnType"), NameValue("format", "StringType")),
    "Converts a date/timestamp/string to a value of string in the format specified by the date format given by the " +
      "second argument.")

  dateTimeFunctions("date_sub") = OneFunction("date_sub", "DateType", 2,
    Array(NameValue("startDate", "ColumnType"), NameValue("days", "IntType")),
    "Returns the date that is days days before start")

  dateTimeFunctions("datediff") = OneFunction("datediff", "DateType", 2,
    Array(NameValue("end", "ColumnType"), NameValue("start", "ColumnType")),
    "Returns the number of days from start to end.")

  dateTimeFunctions("dayofmonth") = OneFunction("dayofmonth", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the day of the month as an integer from a given date/timestamp/string.")

  dateTimeFunctions("dayofyear") = OneFunction("dayofyear", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the day of the year as an integer from a given date/timestamp/string.")

  dateTimeFunctions("from_unixtime_f") = OneFunction("from_unixtime", "StringType", 2,
    Array(NameValue("ut", "ColumnType"), NameValue("format", "StringType")),
    "Converts the number of seconds from unix epoch (1970-01-01 00:00:00 UTC) to a string representing the timestamp " +
      "of that moment in the current system time zone in the given format.")

  dateTimeFunctions("from_unixtime") = OneFunction("from_unixtime", "StringType", 1,
    Array(NameValue("ut", "ColumnType")),
    "Converts the number of seconds from unix epoch (1970-01-01 00:00:00 UTC) to a string representing the timestamp " +
      "of that moment in the current system time zone in the given format.")

  dateTimeFunctions("from_utc_timestamp") = OneFunction("from_utc_timestamp", "TimestampType", 2,
    Array(NameValue("ut", "ColumnType"), NameValue("timeZone", "StringType")),
    "Assumes given timestamp is UTC and converts to given timezone.")

  dateTimeFunctions("hour") = OneFunction("hour", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the hours as an integer from a given date/timestamp/string.")

  dateTimeFunctions("last_day") = OneFunction("last_day", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Given a date column, returns the last day of the month which the given date belongs to.")

  dateTimeFunctions("minute") = OneFunction("minute", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the minutes as an integer from a given date/timestamp/string.")

  dateTimeFunctions("month") = OneFunction("month", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the month as an integer from a given date/timestamp/string.")

  dateTimeFunctions("next_day") = OneFunction("next_day", "DateType", 2,
    Array(NameValue("date", "ColumnType"), NameValue("dayOfWeek", "StringType")),
    "Given a date column, returns the first date which is later than the value of the date column that is on the " +
      "specified day of the week.")

  dateTimeFunctions("quarter") = OneFunction("quarter", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the quarter as an integer from a given date/timestamp/string.")

  dateTimeFunctions("second") = OneFunction("second", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the seconds as an integer from a given date/timestamp/string.")

  dateTimeFunctions("to_date") = OneFunction("to_date", "DateType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Converts the column into DateType.")

  dateTimeFunctions("to_utc_timestamp") = OneFunction("to_utc_timestamp", "TimestampType", 2,
    Array(NameValue("timestamp", "ColumnType"), NameValue("timeZone", "StringType")),
    "Converts the column into DateType.")

  dateTimeFunctions("trunc") = OneFunction("trunc", "DateType", 2,
    Array(NameValue("date", "ColumnType"), NameValue("format", "StringType")),
    "Converts the column into DateType.")

  dateTimeFunctions("unix_timestamp_c_f") = OneFunction("unix_timestamp", "LongType", 2,
    Array(NameValue("s", "ColumnType"), NameValue("p", "StringType")),
    " Convert time string with given pattern " +
      "(see [http://docs.oracle.com/javase/tutorial/i18n/format/simpleDateFormat.html]) " +
      "to Unix time stamp (in seconds), return null if fail.")

  dateTimeFunctions("unix_timestamp_c") = OneFunction("unix_timestamp", "LongType", 1,
    Array(NameValue("s", "ColumnType")),
    "Converts time string in format yyyy-MM-dd HH:mm:ss to Unix timestamp (in seconds), using the default timezone " +
      "and the default locale, return null if fail.")

  dateTimeFunctions("unix_timestamp") = OneFunction("unix_timestamp", "LongType", 0,
    null,
    "Gets current Unix timestamp in seconds.")

  dateTimeFunctions("weekofyear") = OneFunction("weekofyear", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the week number as an integer from a given date/timestamp/string.")

  dateTimeFunctions("year") = OneFunction("year", "IntType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Extracts the year as an integer from a given date/timestamp/string.")
}
