package vizprog.specification

import scala.collection.mutable.Map


class SpecNodeSQL {

  val API_VERSION = "1.6.1"

  val allSQLNodes = Map[String, OneNode]()

  /* SELECT / AS / ALIAS

    1) select(col: String, cols: String*): DataFrame
        Selects a set of columns. This is a variant of select that can only select existing columns
        using column names (i.e. cannot construct expressions).
        // The following two are equivalent:
        df.select("colA", "colB")
        df.select($"colA", $"colB")
    2) select(cols: Column*): DataFrame
        Selects a set of column based expressions.
        df.select($"colA", $"colB" + 1)
    3) selectExpr(exprs: String*): DataFrame
        Selects a set of SQL expressions. This is a variant of select that accepts SQL expressions.
        df.selectExpr("colA", "colB as newName", "abs(colC)")
    4) as(alias: String, metadata: Metadata): Column
        Gives the column an alias with metadata.
        val metadata: Metadata = ...
        df.select($"colA".as("colB", metadata))
    5) as(alias: Symbol): Column
        Gives the column an alias.
        // Renames colA to colB in select output.
        df.select($"colA".as('colB))
        If the current column has metadata associated with it, this metadata will be propagated to
        the new column. If this not desired, use as with explicitly empty metadata.
    6) as(aliases: Array[String]): Column
        Assigns the given aliases to the results of a table generating function.
        // Renames colA to colB in select output.
        df.select(explode($"myMap").as("key" :: "value" :: Nil))
    7) as(aliases: Seq[String]): Column
        Assigns the given aliases to the results of a table generating function.
        // Renames colA to colB in select output.
        df.select(explode($"myMap").as("key" :: "value" :: Nil))
    8) as(alias: String): Column
        Gives the column an alias.
        // Renames colA to colB in select output.
        df.select($"colA".as("colB"))
        If the current column has metadata associated with it, this metadata will be propagated to
        the new column. If this not desired, use as with explicitly empty metadata.
    9) alias(alias: String): Column
        Gives the column an alias. Same as as.
        // Renames colA to colB in select output.
        df.select($"colA".alias("colB"))
  */
/*
  allSQLNodes("select") = OneNode(
    "select",
    Map("column names" -> "StringVarargType"),
    Map("in" -> "input", "out" -> "output"),
    false)
*/

}
