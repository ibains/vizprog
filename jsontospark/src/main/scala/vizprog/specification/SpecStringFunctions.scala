package vizprog.specification

import scala.collection.mutable.Map

class SpecStringFunctions {

  val API_VERSION = "1.6.1"

  val stringFunctions = Map[String, OneFunction]()

  stringFunctions("ascii") = OneFunction("ascii", "IntType", 1,
    Array(NameValue("expression", "StringType")),
    "Computes the numeric value of the first character of the string column, and returns the result as a int column.")

  stringFunctions("base64") = OneFunction("base64", "StringType", 1,
    Array(NameValue("expression", "StringType")),
    "Computes the BASE64 encoding of a binary column and returns it as a string column.")

  stringFunctions("concat") = OneFunction("concat", "StringType", 1,
    Array(NameValue("expression", "StringVarargType")),
    "Concatenates multiple input string columns together into a single string column.")

  stringFunctions("concat_ws") = OneFunction("concat_ws", "StringType", 2,
    Array(NameValue("sep", "StringType"), NameValue("expression", "StringVarargType")),
    "Concatenates multiple input string columns together into a single string column, using the given separator.")

  stringFunctions("decode") = OneFunction("decode", "StringType", 2,
    Array(NameValue("value", "ColumnType"), NameValue("charset", "StringType")),
    "Computes the first argument into a string from a binary using the provided character set " +
      "(one of 'US-ASCII', 'ISO-8859-1', 'UTF-8', 'UTF-16BE', 'UTF-16LE', 'UTF-16').")

  stringFunctions("encode") = OneFunction("encode", "StringType", 2,
    Array(NameValue("value", "ColumnType"), NameValue("charset", "StringType")),
    "Computes the first argument into a binary from a string using the provided character set " +
      "(one of 'US-ASCII', 'ISO-8859-1', 'UTF-8', 'UTF-16BE', 'UTF-16LE', 'UTF-16').")

  stringFunctions("format_number") = OneFunction("format_number", "StringType", 2,
    Array(NameValue("x", "ColumnType"), NameValue("d", "IntType")),
    "Formats numeric column x to a format like '#,###,###.")

  stringFunctions("format_string") = OneFunction("format_string", "StringType", 2,
    Array(NameValue("format", "StringType"), NameValue("arguments", "ColumnVarargType")),
    "Formats the arguments in printf-style and returns the result as a string column.")

  stringFunctions("initcap") = OneFunction("initcap", "StringType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Returns a new string column by converting the first letter of each word to uppercase.")

  stringFunctions("instr") = OneFunction("instr", "StringType", 2,
    Array(NameValue("str", "StringType"), NameValue("substring", "StringType")),
    "Locate the position of the first occurrence of substr column in the given string.")

  stringFunctions("length") = OneFunction("length", "IntType", 1,
    Array(NameValue("expression", "StringType")),
    "Computes the length of a given string or binary column.")

  stringFunctions("levenshtein") = OneFunction("levenshtein", "IntType", 2,
    Array(NameValue("l", "ColumnType"), NameValue("r", "ColumnType")),
    "Computes the Levenshtein distance of the two given string columns.")

  stringFunctions("locate_pos") = OneFunction("locate", "IntType", 3,
    Array(NameValue("substr", "StringType"), NameValue("str", "ColumnType"), NameValue("pos", "IntType")),
    "Locate the position of the first occurrence of substr in a string column, after position pos.")

  stringFunctions("locate") = OneFunction("locate", "IntType", 2,
    Array(NameValue("substr", "StringType"), NameValue("str", "ColumnType")),
    "Locate the position of the first occurrence of substr.")

  stringFunctions("lower") = OneFunction("lower", "StringType", 1,
    Array(NameValue("expression", "StringType")),
    "Converts a string column to lower case.")

  stringFunctions("lpad") = OneFunction("lpad", "StringType", 3,
    Array(NameValue("str", "ColumnType"), NameValue("len", "IntType"), NameValue("pad", "StringType")),
    "Left-padded with pad to a length of len.")

  stringFunctions("ltrim") = OneFunction("ltrim", "StringType", 1,
    Array(NameValue("expression", "StringType")),
    "Trim the spaces from left end for the specified string value.")

  stringFunctions("regexp_extract") = OneFunction("regexp_extract", "StringType", 3,
    Array(NameValue("e", "ColumnType"), NameValue("expr", "StringType"), NameValue("groupIdx", "IntType")),
    "Extract a specific(idx) group identified by a java regex, from the specified string column.")

  stringFunctions("regexp_replace") = OneFunction("regexp_replace", "StringType", 3,
    Array(NameValue("e", "ColumnType"), NameValue("pattern", "StringType"), NameValue("replacement", "StringType")),
    "Replace all substrings of the specified string value that match regexp with rep.")

  stringFunctions("repeat") = OneFunction("repeat", "StringType", 2,
    Array(NameValue("str", "ColumnType"), NameValue("n", "InyType")),
    "Repeats a string column n times, and returns it as a new string column.")

  stringFunctions("reverse") = OneFunction("reverse", "StringType", 1,
    Array(NameValue("str", "ColumnType")),
    "Reverses the string column and returns it as a new string column.")

  stringFunctions("rpad") = OneFunction("rpad", "StringType", 3,
    Array(NameValue("str", "ColumnType"), NameValue("len", "IntType"), NameValue("pad", "StringType")),
    "Right-padded with pad to a length of len.")

  stringFunctions("rtrim") = OneFunction("rtrim", "StringType", 1,
    Array(NameValue("expression", "StringType")),
    "Trim the spaces from right end for the specified string value.")

  stringFunctions("soundex") = OneFunction("soundex", "StringType", 1,
    Array(NameValue("expression", "StringType")),
    "Return the soundex code for the specified expression.")

  stringFunctions("split") = OneFunction("split", "ArrayType", 2,
    Array(NameValue("str", "ColumnType"), NameValue("pattern", "StringType")),
    "Splits str around pattern (pattern is a regular expression).")

  stringFunctions("substring") = OneFunction("substring", "StringType", 3,
    Array(NameValue("str", "ColumnType"), NameValue("pos", "IntType"), NameValue("len", "IntType")),
    "Substring starts at pos and is of length len when str is String type or returns the slice of byte array " +
      "that starts at pos in byte and is of length len when str is Binary type")

  stringFunctions("substring_index") = OneFunction("substring_index", "StringType", 3,
    Array(NameValue("str", "ColumnType"), NameValue("delim", "StringType"), NameValue("count", "IntType")),
    "Returns the substring from string str before count occurrences of the delimiter delim.")

  stringFunctions("translate") = OneFunction("translate", "StringType", 3,
    Array(NameValue("src", "ColumnType"), NameValue("matchingString", "StringType"), NameValue("replaceString", "StringType")),
    "Translate any character in the src by a character in replaceString.")

  stringFunctions("trim") = OneFunction("trim", "StringType", 0,
    null,
    "Trim the spaces from both ends for the specified string column.")

  stringFunctions("unbase64") = OneFunction("unbase64", "BinaryType", 1,
    Array(NameValue("expression", "ColumnType")),
    "Decodes a BASE64 encoded string column and returns it as a binary column.")

  stringFunctions("upper") = OneFunction("upper", "StringType", 1,
    Array(NameValue("expression", "StringType")),
    "Converts a string column to upper case.")
}
