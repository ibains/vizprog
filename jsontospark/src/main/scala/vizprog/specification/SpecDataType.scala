package vizprog.specification

import scala.collection.mutable.Map

class SpecDataType {

  val API_VERSION = "1.6.1"

  val dataTypes = Map[String, OneDataType]()

  dataTypes("ByteType") =
    OneDataType("ByteType", true, false, false, null,
      "Represents 1-byte signed integer numbers. The range of numbers is from -128 to 127.")

  dataTypes("ShortType") =
    OneDataType("ShortType", true, false, false, null,
      "Represents 2-byte signed integer numbers. The range of numbers is from -32768 to 32767.")

  dataTypes("IntegerType") =
    OneDataType("IntegerType", true, false, false, null,
      "Represents 4-byte signed integer numbers. The range of numbers is from -2147483648 to 2147483647.")

  dataTypes("LongType") =
    OneDataType("LongType", true, false, false, null,
      "Represents 8-byte signed integer numbers. The range of numbers is from -9223372036854775808 to " +
        "9223372036854775807.")

  dataTypes("FloatType") =
    OneDataType("FloatType", true, false, false, null,
      "Represents 4-byte single-precision floating point numbers.")

  dataTypes("DoubleType") =
    OneDataType("DoubleType", true, false, false, null,
      "Represents 8-byte double-precision floating point numbers.")

  dataTypes("DecimalType") =
    OneDataType("DecimalType", true, false, false, null,
      "Represents arbitrary-precision signed decimal numbers. Backed internally by java.math.BigDecimal. " +
        "A BigDecimal consists of an arbitrary precision integer unscaled value and a 32-bit integer scale.")

  dataTypes("StringType") =
    OneDataType("StringType", false, false, false, null,
      "Represents character string values.")

  dataTypes("BinaryType") =
    OneDataType("BinaryType", false, false, false, null,
      "Represents byte sequence values.")

  dataTypes("BooleanType") =
    OneDataType("BooleanType", false, false, false, null,
      "Represents boolean values.")

  dataTypes("TimestampType") =
    OneDataType("TimestampType", false, true, false, null,
      "Represents values comprising values of fields year, month, day, hour, minute, and second.")

  dataTypes("DateType") =
    OneDataType("DateType", false, true, false, null,
      "Represents values comprising values of fields year, month, day.")

  dataTypes("CalendarIntervalType") =
    OneDataType("CalendarIntervalType", false, true, false, null,
      "The data type representing calendar time intervals. The calendar time interval is stored internally in two " +
        "components: number of months the number of microseconds.")

  dataTypes("NullType") =
    OneDataType("NullType", false, false, false, null,
      "The data type representing NULL values.")

  // IGNORE AtomicType: An internal type used to represent everything that is not null, UDTs, arrays, structs, and maps.


  dataTypes("ArrayType") =
    OneDataType("ArrayType", false, false, true,
      Array(NameValue("elementType", "DataType"), NameValue("containsNull", "BooleanType")),
      "The data type for collections of multiple values. An ArrayType object comprises two fields, " +
        "elementType: DataType and containsNull: Boolean. The field of elementType is used to specify the type of " +
        "array elements. The field of containsNull is used to specify if the array has null values.")

  dataTypes("MapType") =
    OneDataType("MapType", false, false, true,
      Array(NameValue("keyType", "DataType"), NameValue("valueType", "DataType"), NameValue("valueContainsNull", "BooleanType")),
      "The data type for Maps. Keys in a map are not allowed to have null values.")

  dataTypes("ColumnVarargType") =
    OneDataType("ColumnVarargType", false, false, true,
      Array(NameValue("oneValue", "ColumnType")),
      "Represents a comma separated list of columns to be passed as vararg")

  dataTypes("StringVarargType") =
    OneDataType("StringVarargType", false, false, true,
      Array(NameValue("oneValue", "StringType")),
      "Represents a comma separated list of strings to be passed as vararg")

  /*
  TODO - ADD REMAINING COMPLEX TYPES
     StructType,
     UserDefinedType,
     VectorUDT
  */
}

