package vizprog.specification

import vizprog.common.MLTypes

class SpecNodeML {

  val API_VERSION = "1.6.1"

  val allMLNodes = scala.collection.mutable.Map[String, OneNode]()

  allMLNodes("LogisticRegression") = OneNode("LogisticRegression",
    "Logistic regression. Currently, this class only supports binary classification. It will support multiclass in the future.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "elasticNetParam"  -> (NameValue("elasticNetParam", "DoubleType"),
                                 "the ElasticNet mixing parameter, in range [0, 1]. For alpha = 0, the penalty is an L2 penalty. For alpha = 1, it is an L1 penalty (default: 0.0)"),
          "featuresCol"      -> (NameValue("featuresCol", "StringType"),
                                 "features column name (default: features, current: myFeatures)"),
          "fitIntercept"     -> (NameValue("fitIntercept", "Booleantype"),
                                 "whether to fit an intercept term (default: true)"),
          "labelCol"         -> (NameValue("labelCol", "StringType"),
                                 "label column name (default: label)"),
          "maxIter"          -> (NameValue("maxIter", "IntType"),
                                 "maximum number of iterations (>= 0) (default: 100, current: 10)"),
          "predictionCol"    -> (NameValue("predictionCol", "StringType"),
                                 "prediction column name (default: prediction)"),
          "probabilityCol"   -> (NameValue("probabilityCol", "StringType"),
                                 "column name for predicted class conditional probabilities. Note: Not all models output well-calibrated probability estimates! These probabilities should be treated as confidences, not precise probabilities (default: probability)"),
          "rawPredictionCol" -> (NameValue("rawPredictionCol", "StringType"),
                                 "raw prediction (a.k.a. confidence) column name (default: rawPrediction)"),
          "regParam"         -> (NameValue("regParam", "DoubleType"),
                                 "regularization parameter (>= 0) (default: 0.0, current: 0.001)"),
          "standardization"  -> (NameValue("standardization", "BooleanType"),
                                 "whether to standardize the training features before fitting the model (default: true)"),
          "threshold"        -> (NameValue("threshold", "DoubleType"),
                                 "threshold in binary classification prediction, in range [0, 1] (default: 0.5)"),
          "tol"              -> (NameValue("tol", "DoubleType"),
                                 "the convergence tolerance for iterative algorithms (default: 1.0E-6)"),
          "weightCol"        -> (NameValue("weightCol", "StringType"),
                                 "weight column name. If this is not set or empty, we treat all instance weights as 1.0. (default: )"))
    , MLTypes.MLAlgorithm)

  allMLNodes("Tokenizer") = OneNode("Tokenizer",
    "A tokenizer that converts the input string to lowercase and then splits it by white spaces.",
    Map ("in" -> "input", "out" -> "output"),
    Map ( "inputCol"         -> (NameValue("inputCol", "StringType" ), "input column name "),
          "outputCol"        -> (NameValue("outputCol", "StringType"), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("HashingTF") = OneNode("HashingTF",
    "Maps a sequence of terms to their term frequencies using the hashing trick. Currently we use Austin Appleby's MurmurHash 3 algorithm (MurmurHash3_x86_32) to calculate the hash code value for the term object. Since a simple modulo is used to transform the hash function to a column index, it is advisable to use a power of two as the numFeatures parameter; otherwise the features will not be mapped evenly to the columns.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"         -> (NameValue("inputCol",    "StringType" ), "input column name "),
          "numFeatures"      -> (NameValue("numFeatures", "IntType"    ), "number of features (> 0) (default: 262144)"),
          "outputCol"        -> (NameValue("outputCol",   "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("DecisionTreeClassifier") = OneNode("DecisionTreeClassifier",
    "Decision tree learning algorithm (http://en.wikipedia.org/wiki/Decision_tree_learning) for classification. It supports both binary and multiclass labels, as well as both continuous and categorical features.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "cacheNodeIds"         -> (NameValue("cacheNodeIds"        , "BooleanType"), "If false, the algorithm will pass trees to executors to match instances with nodes. If true, the algorithm will cache node IDs for each instance. Caching can speed up training of deeper trees. (default: false)"),
          "checkpointInterval"   -> (NameValue("checkpointInterval"  , "IntType"    ), "set checkpoint interval (>= 1) or disable checkpoint (-1). E.g. 10 means that the cache will get checkpointed every 10 iterations (default: 10)"),
          "featuresCol"          -> (NameValue("featuresCol"         , "StringType" ), "features column name (default: features)"),
          "impurity"             -> (NameValue("impurity"            , "StringType" ), "Criterion used for information gain calculation (case-insensitive). Supported options: entropy, gini (default: gini)"),
          "labelCol"             -> (NameValue("featuresCol"         , "StringType" ), "label column name (default: label)"),
          "maxBins"              -> (NameValue("maxBins"             , "IntType"    ), "Max number of bins for discretizing continuous features.  Must be >=2 and >= number of categories for any categorical feature. (default: 32)"),
          "maxDepth"             -> (NameValue("maxDepth"            , "IntType"    ), "Maximum depth of the tree. (>= 0) E.g., depth 0 means 1 leaf node; depth 1 means 1 internal node + 2 leaf nodes. (default: 5)"),
          "maxMemoryInMB"        -> (NameValue("maxMemoryInMB"       , "IntType"    ), "Maximum memory in MB allocated to histogram aggregation. (default: 256)"),
          "minInfoGain"          -> (NameValue("minInfoGain"         , "DoubleType" ), "Minimum information gain for a split to be considered at a tree node. (default: 0.0)"),
          "minInstancesPerNode"  -> (NameValue("minInstancesPerNode" , "IntType"    ), "Minimum number of instances each child must have after split.  If a split causes the left or right child to have fewer than minInstancesPerNode, the split will be discarded as invalid. Should be >= 1. (default: 1)"),
          "predictionCol"        -> (NameValue("predictionCol"       , "StringType" ), "prediction column name (default: prediction)"),
          "probabilityCol"       -> (NameValue("probabilityCol"      , "StringType" ), "Column name for predicted class conditional probabilities. Note: Not all models output well-calibrated probability estimates! These probabilities should be treated as confidences, not precise probabilities (default: probability)"),
          "rawPredictionCol"     -> (NameValue("rawPredictionCol"    , "StringType" ), "raw prediction (a.k.a. confidence) column name (default: rawPrediction)"),
          "seed"                 -> (NameValue("seed"                , "LongType"   ), "random seed (default: 159147643)"),
          "thresholds"           -> (NameValue("thresholds"          , "ArrayType(DoubleType)"), "Thresholds in multi-class classification to adjust the probability of predicting each class. Array must have length equal to the number of classes, with values >= 0. The class with largest value p/t is predicted, where p is the original probability of that class and t is the class' threshold. (undefined)"))
    , MLTypes.MLAlgorithm)


  allMLNodes("CrossValidator") = OneNode("CrossValidator",
    "K-fold cross validation.",
    Map ( "estimator" -> "input", "evaluator" -> "input", "params"->"input", "out" -> "output"),
    Map ( "numFolds"  -> (NameValue("numFolds", "IntType"), "numFolds: number of folds for cross validation (>= 2) (default: 3)"))
    , MLTypes.MLCrossValidator)


  allMLNodes("Binarizer") = OneNode("Binarizer",
    "Binarize a column of continuous features given a threshold.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"  ->  (NameValue("inputCol"  , "StringType"), "input column name (undefined)"),
          "outputCol" ->  (NameValue("outputCol" , "StringType"), "output column name"),
          "threshold" ->  (NameValue("threshold" , "DoubleType"), "threshold used to binarize continuous features (default: 0.0)"))
    , MLTypes.MLFeature)


  allMLNodes("Bucketizer") = OneNode("Bucketizer",
    "Bucketizer maps a column of continuous features to a column of feature buckets.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"   -> (NameValue("inputCol"  , "StringType"), "input column name (undefined)"),
          "outputCol"  -> (NameValue("outputCol" , "StringType"), "output column name"),
          "splits"     -> (NameValue("splits"    , "IntType"   ), "Split points for mapping continuous features into buckets. With n+1 splits, there are n buckets. A bucket defined by splits x,y holds values in the range [x,y) except the last bucket, which also includes y. The splits should be strictly increasing. Values at -inf, inf must be explicitly provided to cover all Double values; otherwise, values outside the splits specified will be treated as errors. (undefined)"))
    , MLTypes.MLFeature)


  allMLNodes("ChiSqSelector") = OneNode("ChiSqSelector",
    "Chi-Squared feature selection, which selects categorical features to use for predicting a categorical label.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "featuresCol"    -> (NameValue("featuresCol"    , "StringType"), "features column name (default: features)"),
          "labelCol"       -> (NameValue("labelCol"       , "StringType"), "label column name (default: label)"),
          "numTopFeatures" -> (NameValue("numTopFeatures" , "IntType"   ), "Number of features that selector will select, ordered by statistics value descending. If the number of features is < numTopFeatures, then this will select all features. (default: 50)"),
          "outputCol"      -> (NameValue("outputCol"      , "StringType"), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("CountVectorizer") = OneNode("CountVectorizer",
    "Extracts a vocabulary from document collections and generates a CountVectorizerModel.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol"  , "StringType"), "inputCol: input column name (undefined)"),
          "minDF"       -> (NameValue("minDF"     , "DoubleType"), "Specifies the minimum number of different documents a term must appear in to be included in the vocabulary. If this is an integer >= 1, this specifies the number of documents the term must appear in; if this is a double in [0,1), then this specifies the fraction of documents. (default: 1.0)"),
          "minTF"       -> (NameValue("minTF"     , "DoubleType"), "Filter to ignore rare words in a document. For each document, terms with frequency/count less than the given threshold are ignored. If this is an integer >= 1, then this specifies a count (of times the term must appear in the document); if this is a double in [0,1), then this specifies a fraction (out of the document's token count). Note that the parameter is only used in transform of CountVectorizerModel and does not affect fitting. (default: 1.0)"),
          "outputCol"   -> (NameValue("outputCol" , "StringType"), "output column name"),
          "vocabSize"   -> (NameValue("vocabSize" , "LongType"  ), "max size of the vocabulary (default: 262144)"))
    , MLTypes.MLFeature)


  allMLNodes("DCT") = OneNode("DCT",
    "A feature transformer that takes the 1D discrete cosine transform of a real vector. No zero padding is performed on the input vector. It returns a real vector of the same length representing the DCT. The return vector is scaled such that the transform matrix is unitary (aka scaled DCT-II).",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol"  , "StringType"), "inputCol: input column name"),
          "inverse"     -> (NameValue("inverse"  , "BooleanType"), "Set transformer to perform inverse DCT (default: false)"),
          "outputCol"   -> (NameValue("outputCol" , "StringType"), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("ElementwiseProduct") = OneNode("ElementwiseProduct",
    "Outputs the Hadamard product (i.e., the element-wise product) of each input vector with a provided \"weight\" vector. In other words, it scales each column of the dataset by a scalar multiplier.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol"  , "StringType"), "inputCol: input column name (undefined)"),
          "scalingVec"  -> (NameValue("inverse"  , "VectorType(DoubleType)"), "vector for hadamard product (undefined)"),
          "outputCol"   -> (NameValue("outputCol" , "StringType"), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("IDF") = OneNode("IDF",
    "Compute the Inverse Document Frequency (IDF) given a collection of documents.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol"  , "StringType"), "inputCol: input column name (undefined)"),
          "minDocFreq"  -> (NameValue("inverse"   , "IntType"   ), "minimum of documents in which a term should appear for filtering (default: 0)"),
          "outputCol"   -> (NameValue("outputCol" , "StringType"), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("IndexToString") = OneNode("IndexToString",
    "A Transformer that maps a column of indices back to a new column of corresponding string values. The index-string mapping is either from the ML attributes of the input column, or from user-supplied labels (which take precedence over ML attributes).",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol"  , "StringType"           ), "inputCol: input column name (undefined)"),
          "labels"      -> (NameValue("inverse"   , "ArrayType[StringType]"), "Optional array of labels specifying index-string mapping. If not provided or if empty, then metadata from inputCol is used instead."),
          "outputCol"   -> (NameValue("outputCol" , "StringType"           ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("Interaction") = OneNode("Interaction",
    "Implements the feature interaction transform. This transformer takes in Double and Vector type columns and outputs a flattened vector of their feature interactions. To handle interaction, we first one-hot encode any nominal features. Then, a vector of the feature cross-products is produced.For example, given the input feature values Double(2) and Vector(3, 4), the output would be Vector(6, 8) if all input features were numeric. If the first feature was instead nominal with four categories, the output would then be Vector(0, 0, 0, 0, 3, 4, 0, 0).",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCols"    -> (NameValue("inputCols" , "ArrayType(StringType)" ), "inputCol: input column name"),
          "outputCol"   -> (NameValue("outputCol" , "StringType"             ), "output column name"))
    , MLTypes.MLFeature)

  /* 2.0
  MaxAbsScaler
  Rescale each feature individually to range [-1, 1] by dividing through the largest maximum absolute value in each feature. It does not shift/center the data, and thus does not destroy any sparsity.
  */


  allMLNodes("MinMaxScaler") = OneNode("MinMaxScaler",
    "Rescale each feature individually to a common range [min, max] linearly using column summary statistics, which is also known as min-max normalization or Rescaling. The rescaled value for feature E is calculated as,\n\nRescaled(e_i) = \\frac{e_i - E_{min}}{E_{max} - E_{min}} * (max - min) + min\n\nFor the case E_{max} == E_{min}, Rescaled(e_i) = 0.5 * (max + min). Note that since zero values will probably be transformed to non-zero values, output of the transformer will be DenseVector even for sparse input.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol" , "StringType" ), "inputCol: input column name"),
          "max"          -> (NameValue("max"     , "DoubleType" ), "upper bound of the output feature range (default: 1.0)"),
          "min"          -> (NameValue("min"     , "DoubleType" ), "lower bound of the output feature range (default: 0.0)"),
          "outputCol"   -> (NameValue("outputCol", "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("NGram") = OneNode("NGram",
    "A feature transformer that converts the input array of strings into an array of n-grams. Null values in the input array are ignored. It returns an array of n-grams where each n-gram is represented by a space-separated string of words.\n\nWhen the input is empty, an empty array is returned. When the input array length is less than n (number of elements per n-gram), no n-grams are returned.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol" , "StringType" ), "inputCol: input column name"),
          "n"           -> (NameValue("n"        , "IntType"    ), "number elements per n-gram (>=1) (default: 2)"),
          "outputCol"   -> (NameValue("outputCol", "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("Normalizer") = OneNode("Normalizer",
    "Normalize a vector to have unit norm using the given p-norm.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol" , "StringType" ), "inputCol: input column name"),
          "p"           -> (NameValue("p"        , "DoubleType" ), "the p norm value (default: 2.0)"),
          "outputCol"   -> (NameValue("outputCol", "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("OneHotEncoder") = OneNode("OneHotEncoder",
    "A one-hot encoder that maps a column of category indices to a column of binary vectors, with at most a single one-value per row that indicates the input category index. For example with 5 categories, an input value of 2.0 would map to an output vector of [0.0, 0.0, 1.0, 0.0]. The last category is not included by default (configurable via OneHotEncoder!.dropLast because it makes the vector entries sum up to one, and hence linearly dependent. So an input value of 4.0 maps to [0.0, 0.0, 0.0, 0.0]. Note that this is different from scikit-learn's OneHotEncoder, which keeps all categories. The output vectors are sparse.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol" , "StringType" ), "inputCol: input column name"),
          "dropLast"    -> (NameValue("dropLast" , "DoubleType" ), "whether to drop the last category (default: true)"),
          "outputCol"   -> (NameValue("outputCol", "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("PCA") = OneNode("PCA",
    "PCA trains a model to project vectors to a lower dimensional space of the top PCA!.k principal components.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol" , "StringType" ), "inputCol: input column name"),
          "k"           -> (NameValue("k"        , "IntType"    ), "the number of principal components"),
          "outputCol"   -> (NameValue("outputCol", "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("PolynomialExpansion") = OneNode("PolynomialExpansion",
    "Perform feature expansion in a polynomial space. As said in wikipedia of Polynomial Expansion, which is available at http://en.wikipedia.org/wiki/Polynomial_expansion, \"In mathematics, an expansion of a product of sums expresses it as a sum of products by using the fact that multiplication distributes over addition\". Take a 2-variable feature vector as an example: (x, y), if we want to expand it with degree 2, then we get (x, x * x, y, x * y, y * y).",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol" , "StringType" ), "inputCol: input column name"),
          "degree"      -> (NameValue("degree"   , "IntType"    ), "the polynomial degree to expand (>= 1) (default: 2)"),
          "outputCol"   -> (NameValue("outputCol", "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("QuantileDiscretizer") = OneNode("QuantileDiscretizer",
    "QuantileDiscretizer takes a column with continuous features and outputs a column with binned categorical features. The number of bins can be set using the numBuckets parameter. The bin ranges are chosen using an approximate algorithm (see the documentation for approxQuantile for a detailed description). The precision of the approximation can be controlled with the relativeError parameter. The lower and upper bin bounds will be -Infinity and +Infinity, covering all real values.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"    -> (NameValue("inputCol"   , "StringType" ), "inputCol: input column name"),
          "numBuckets"  -> (NameValue("numBuckets" , "IntType"    ), "Maximum number of buckets (quantiles, or categories) into which data points are grouped. Must be >= 2. (default: 2)"),
          "outputCol"   -> (NameValue("outputCol"  , "StringType" ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("RegexTokenizer") = OneNode("RegexTokenizer",
    "A regex based tokenizer that extracts tokens either by using the provided regex pattern to split the text (default) or repeatedly matching the regex (if gaps is false). Optional parameters also allow filtering tokens using a minimal length. It returns an array of strings that can be empty.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"       -> (NameValue("inputCol"       , "StringType"  ), "inputCol: input column name"),
          "gaps"           -> (NameValue("gaps"           , "BooleanType" ), "Set regex to match gaps or tokens (default: true)"),
          "minTokenLength" -> (NameValue("minTokenLength" , "IntType"     ), "minimum token length (>= 0) (default: 1)"),
          "pattern"        -> (NameValue("pattern"        , "StringType"  ), "regex pattern used for tokenizing (default: \\s+)"),
          "toLowercase"    -> (NameValue("toLowercase"    , "BooleanType" ), "whether to convert all characters to lowercase before tokenizing. (default: true)"),
          "outputCol"      -> (NameValue("outputCol"      , "StringType"  ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("RFormula") = OneNode("RFormula",
    "Implements the transforms required for fitting a dataset against an R model formula. Currently we support a limited subset of the R operators, including '~', '.', ':', '+', and '-'. Also see the R formula docs here: http://stat.ethz.ch/R-manual/R-patched/library/stats/html/formula.html\n\nThe basic operators are:\n\n~ separate target and terms\n+ concat terms, \"+ 0\" means removing intercept\n- remove a term, \"- 1\" means removing intercept\n: interaction (multiplication for numeric values, or binarized categorical values)\n. all columns except target\nSuppose a and b are double columns, we use the following simple examples to illustrate the effect of RFormula:\n\ny ~ a + b means model y ~ w0 + w1 * a + w2 * b where w0 is the intercept and w1, w2 are coefficients.\ny ~ a + b + a:b - 1 means model y ~ w1 * a + w2 * b + w3 * a * b where w1, w2, w3 are coefficients.\nRFormula produces a vector column of features and a double or string column of label. Like when formulas are used in R for linear regression, string input columns will be one-hot encoded, and numeric columns will be cast to doubles. If the label column is of type string, it will be first transformed to double with StringIndexer. If the label column does not exist in the DataFrame, the output label column will be created from the specified response variable in the formula.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "featuresCol" -> (NameValue("featuresCol" , "StringType" ), "features column name"),
          "formula"     -> (NameValue("formula"     , "StringType" ), "R model formula"),
          "labelCol"    -> (NameValue("labelCol"    , "StringType" ), "label column name"))
    , MLTypes.MLFeature)


  allMLNodes("SQLTransformer") = OneNode("SQLTransformer",
    "Implements the transformations which are defined by SQL statement. Currently we only support SQL syntax like 'SELECT ... FROM THIS ...' where 'THIS' represents the underlying table of the input dataset. The select clause specifies the fields, constants, and expressions to display in the output, it can be any select clause that Spark SQL supports. Users can also use Spark SQL built-in function and UDFs to operate on these selected columns. For example, SQLTransformer supports statements like:\n\nSELECT a, a + b AS a_b FROM THIS\nSELECT a, SQRT(b) AS b_sqrt FROM THIS where a > 5\nSELECT a, b, SUM(c) AS c_sum FROM THIS GROUP BY a, b",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "statement" -> (NameValue("statement" , "StringType" ), "features column name"))
    , MLTypes.MLFeature)


  allMLNodes("StandardScaler") = OneNode("StandardScaler",
    "Standardizes features by removing the mean and scaling to unit variance using column summary statistics on the samples in the training set.\n\nThe \"unit std\" is computed using the corrected sample standard deviation, which is computed as the square root of the unbiased sample variance.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"  -> (NameValue("inputCol"  , "StringType"  ), "input column name "),
          "outputCol" -> (NameValue("outputCol" , "StringType"  ), "output column name "),
          "withMean"  -> (NameValue("withMean"  , "BooleanType" ), "Whether to center data with mean (default: false)"),
          "withStd"   -> (NameValue("withStd"   , "BooleanType" ), "Whether to scale the data to unit standard deviation (default: true)"))
    , MLTypes.MLFeature)


  allMLNodes("StopWordsRemover") = OneNode("StopWordsRemover",
    "A feature transformer that filters out stop words from input. Note: null values from input array are preserved unless adding null to stopWords explicitly.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"       -> (NameValue("inputCol"  , "StringType"        ), "input column name "),
          "outputCol"      -> (NameValue("outputCol" , "StringType"        ), "output column name "),
          "caseSensitive"  -> (NameValue("withMean"  , "BooleanType"       ), "whether to do case-sensitive comparison during filtering (default: false)"),
          "stopWords"      -> (NameValue("withStd"   , "Array(StringType)" ), "stop words"))
    , MLTypes.MLFeature)


  allMLNodes("StringIndexer") = OneNode("StringIndexer",
    "A label indexer that maps a string column of labels to an ML column of label indices. If the input column is numeric, we cast it to string and index the string values. The indices are in [0, numLabels), ordered by label frequencies. So the most frequent label gets index 0.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"       -> (NameValue("inputCol"  , "StringType" ), "input column name "),
          "outputCol"      -> (NameValue("outputCol" , "StringType" ), "output column name "),
          "handleInvalid"  -> (NameValue("withMean"  , "StringType" ), "how to handle invalid entries. Options are skip (which will filter out rows with bad values), or error (which will throw an errror). More options may be added later. (default: error)"))
    , MLTypes.MLFeature)


  allMLNodes("VectorAssembler") = OneNode("VectorAssembler",
    "A feature transformer that merges multiple columns into a vector column.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCols"   -> (NameValue("inputCols" , "ArrayType(StringType)" ), "inputCol: input column name"),
          "outputCol"   -> (NameValue("outputCol" , "StringType"            ), "output column name"))
    , MLTypes.MLFeature)


  allMLNodes("VectorSlicer") = OneNode("VectorSlicer",
    "This class takes a feature vector and outputs a new feature vector with a subarray of the original features.\n\nThe subset of features can be specified with either indices (setIndices()) or names (setNames()). At least one feature must be selected. Duplicate features are not allowed, so there can be no overlap between selected indices and names.\n\nThe output vector will order features with the selected indices first (in the order given), followed by the selected names (in the order given).",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"  -> (NameValue("inputCol"  , "StringType"     ), "input column name "),
          "outputCol" -> (NameValue("outputCol" , "StringType"     ), "output column name "),
          "indices"   -> (NameValue("indices"   , "Array(IntType)" ), "An array of indices to select features from a vector column. There can be no overlap with names."))
    , MLTypes.MLFeature)


  allMLNodes("VectorIndexer") = OneNode("VectorIndexer",
    "Class for indexing categorical feature columns in a dataset of Vector.\n\nThis has 2 usage modes:\n\nAutomatically identify categorical features (default behavior)\nThis helps process a dataset of unknown vectors into a dataset with some continuous features and some categorical features. The choice between continuous and categorical is based upon a maxCategories parameter.\nSet maxCategories to the maximum number of categorical any categorical feature should have.\nE.g.: Feature 0 has unique values {-1.0, 0.0}, and feature 1 values {1.0, 3.0, 5.0}. If maxCategories = 2, then feature 0 will be declared categorical and use indices {0, 1}, and feature 1 will be declared continuous.\nIndex all features, if all features are categorical\nIf maxCategories is set to be very large, then this will build an index of unique values for all features.\nWarning: This can cause problems if features are continuous since this will collect ALL unique values to the driver.\nE.g.: Feature 0 has unique values {-1.0, 0.0}, and feature 1 values {1.0, 3.0, 5.0}. If maxCategories >= 3, then both features will be declared categorical.\nThis returns a model which can transform categorical features to use 0-based indices.\n\nIndex stability:\n\nThis is not guaranteed to choose the same category index across multiple runs.\nIf a categorical feature includes value 0, then this is guaranteed to map value 0 to index 0. This maintains vector sparsity.\nMore stability may be added in the future.\nTODO: Future extensions: The following functionality is planned for the future:\n\nPreserve metadata in transform; if a feature's metadata is already present, do not recompute.\nSpecify certain features to not index, either via a parameter or via existing metadata.\nAdd warning if a categorical feature has only 1 category.\nAdd option for allowing unknown categories.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"      -> (NameValue("inputCol"  , "StringType" ), "input column name "),
          "outputCol"     -> (NameValue("outputCol" , "StringType" ), "output column name "),
          "maxCategories" -> (NameValue("indices"   , "IntType"    ), "Threshold for the number of values a categorical feature can take (>= 2). If a feature is found to have > maxCategories values, then it is declared continuous. (default: 20)"))
    , MLTypes.MLFeature)


  allMLNodes("Word2Vec") = OneNode("Word2Vec",
    "Word2Vec trains a model of Map(String, Vector), i.e. transforms a word into a code for further natural language processing or machine learning process.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "inputCol"      -> (NameValue("inputCol"      , "StringType" ), "input column name "),
          "outputCol"     -> (NameValue("outputCol"     , "StringType" ), "output column name "),
          "maxIter"       -> (NameValue("maxIter"       , "IntType"    ), "maximum number of iterations (>= 0) (default: 1)"),
          "minCount"      -> (NameValue("minCount"      , "IntType"    ), "the minimum number of times a token must appear to be included in the word2vec model's vocabulary (default: 5)"),
          "numPartitions" -> (NameValue("numPartitions" , "IntType"    ), "number of partitions for sentences of words (default: 1)"),
          "seed"          -> (NameValue("seed"          , "LongType"   ), "random seed (default: -1961189076)"),
          "stepSize"      -> (NameValue("stepSize"      , "DoubleType" ), "Step size to be used for each iteration of optimization. (default: 0.025)"),
          "vectorSize"    -> (NameValue("seed"          , "LongType"   ), "the dimension of codes after transforming from words (default: 100)"),
          "windowSize"    -> (NameValue("stepSize"      , "IntType"    ), "the window size (context words from [-window, window]) (default: 5)"))
    , MLTypes.MLFeature)


  allMLNodes("GBTClassifier") = OneNode("GBTClassifier",
    "Gradient-Boosted Trees (GBTs) (http://en.wikipedia.org/wiki/Gradient_boosting) learning algorithm for classification. It supports binary labels, as well as both continuous and categorical features. Note: Multiclass labels are not currently supported.\n\nThe implementation is based upon: J.H. Friedman. \"Stochastic Gradient Boosting.\" 1999.\n\nNotes on Gradient Boosting vs. TreeBoost:\n\nThis implementation is for Stochastic Gradient Boosting, not for TreeBoost.\nBoth algorithms learn tree ensembles by minimizing loss functions.\nTreeBoost (Friedman, 1999) additionally modifies the outputs at tree leaf nodes based on the loss function, whereas the original gradient boosting method does not.\nWe expect to implement TreeBoost in the future: [https://issues.apache.org/jira/browse/SPARK-4240]",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "cacheNodeIds"        -> (NameValue("cacheNodeIds"        , "BooleanType" ), "If false, the algorithm will pass trees to executors to match instances with nodes. If true, the algorithm will cache node IDs for each instance. Caching can speed up training of deeper trees. (default: false)"),
          "checkpointInterval"  -> (NameValue("checkpointInterval"  , "IntType"     ), "set checkpoint interval (>= 1) or disable checkpoint (-1). E.g. 10 means that the cache will get checkpointed every 10 iterations (default: 10)"),
          "featuresCol"         -> (NameValue("featuresCol"         , "StringType"  ), "features column name (default: features)"),
          "impurity"            -> (NameValue("impurity"            , "StringType"  ), "Criterion used for information gain calculation (case-insensitive). Supported options: entropy, gini (default: gini)"),
          "labelCol"            -> (NameValue("labelCol"            , "StringType"  ), "label column name (default: label)"),
          "lossType"            -> (NameValue("lossType"            , "StringType"  ), "Loss function which GBT tries to minimize (case-insensitive). Supported options: logistic (default: logistic)"),
          "maxBins"             -> (NameValue("maxBins"             , "IntType"     ), "Max number of bins for discretizing continuous features.  Must be >=2 and >= number of categories for any categorical feature. (default: 32)"),
          "maxDepth"            -> (NameValue("maxDepth"            , "IntType"     ), "Maximum depth of the tree. (>= 0) E.g., depth 0 means 1 leaf node; depth 1 means 1 internal node + 2 leaf nodes. (default: 5)"),
          "maxIter"             -> (NameValue("maxIter"             , "IntType"     ), "maximum number of iterations (>= 0) (default: 20)"),
          "maxMemoryInMB"       -> (NameValue("maxMemoryInMB"       , "IntType"     ), "Maximum memory in MB allocated to histogram aggregation. (default: 256)"),
          "minInfoGain"         -> (NameValue("minInfoGain"         , "DoubleType"  ), "Minimum information gain for a split to be considered at a tree node. (default: 0.0)"),
          "minInstancesPerNode" -> (NameValue("minInstancesPerNode" , "IntType"     ), "Minimum number of instances each child must have after split.  If a split causes the left or right child to have fewer than minInstancesPerNode, the split will be discarded as invalid. Should be >= 1. (default: 1)"),
          "predictionCol"       -> (NameValue("predictionCol"       , "StringType"  ), "prediction column name (default: prediction)"),
          "seed"                -> (NameValue("seed"                , "LongType"    ), "random seed (default: -1287390502)"),
          "stepSize"            -> (NameValue("stepSize"            , "DoubleType"  ), " Step size to be used for each iteration of optimization. (default: 0.1)"),
          "subsamplingRate"     -> (NameValue("subsamplingRate"     , "DoubleType"  ), "Fraction of the training data used for learning each decision tree, in range (0, 1]. (default: 1.0)"))
    , MLTypes.MLAlgorithm)


  allMLNodes("MultilayerPerceptronClassifier") = OneNode("MultilayerPerceptronClassifier",
    "Classifier trainer based on the Multilayer Perceptron. Each layer has sigmoid activation function, output layer has softmax. Number of inputs has to be equal to the size of feature vectors. Number of outputs has to be equal to the total number of labels.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "blockSize"           -> (NameValue("blockSize"           , "IntType"     ), "Block size for stacking input data in matrices. Data is stacked within partitions. If block size is more than remaining data in a partition then it is adjusted to the size of this data. Recommended size is between 10 and 1000 (default: 128)"),
          "featuresCol"         -> (NameValue("featuresCol"         , "StringType"  ), "features column name (default: features)"),
          "labelCol"            -> (NameValue("labelCol"            , "StringType"  ), "label column name (default: label)"),
          "layers"              -> (NameValue("layers"       , "ArrayType(IntType)" ), "Sizes of layers from input layer to output layer E.g., Array(780, 100, 10) means 780 inputs, one hidden layer with 100 neurons and output layer of 10 neurons."),
          "maxIter"             -> (NameValue("maxIter"             , "IntType"     ), "maximum number of iterations (>= 0) (default: 20)"),
          "predictionCol"       -> (NameValue("predictionCol"       , "StringType"  ), "prediction column name (default: prediction)"),
          "seed"                -> (NameValue("seed"                , "LongType"    ), "random seed (default: -1287390502)"),
          "tol"                 -> (NameValue("stepSize"            , "DoubleType"  ), "the convergence tolerance for iterative algorithms (default: 1.0E-4)"))
    , MLTypes.MLAlgorithm)


  allMLNodes("NaiveBayes") = OneNode("NaiveBayes",
    "Naive Bayes Classifiers. It supports both Multinomial NB (http://nlp.stanford.edu/IR-book/html/htmledition/naive-bayes-text-classification-1.html) which can handle finitely supported discrete data. For example, by converting documents into TF-IDF vectors, it can be used for document classification. By making every vector a binary (0/1) data, it can also be used as Bernoulli NB (http://nlp.stanford.edu/IR-book/html/htmledition/the-bernoulli-model-1.html). The input feature values must be nonnegative.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "featuresCol"             -> (NameValue("featuresCol"         , "StringType"   ), "features column name (default: features)"),
          "labelCol"                -> (NameValue("labelCol"            , "StringType"   ), "label column name (default: label)"),
          "modelType"               -> (NameValue("modelType"           , "StringType"   ), "The model type which is a string (case-sensitive). Supported options: multinomial (default) and bernoulli. (default: multinomial)"),
          "probabilityCol"          -> (NameValue("probabilityCol"      , "StringType"   ), "Column name for predicted class conditional probabilities. Note: Not all models output well-calibrated probability estimates! These probabilities should be treated as confidences, not precise probabilities (default: probability)"),
          "predictionCol"           -> (NameValue("predictionCol"       , "StringType"   ), "prediction column name (default: prediction)"),
          "rawPredictionCol"        -> (NameValue("rawPredictionCol"    , "StringType"   ), "raw prediction (a.k.a. confidence) column name (default: rawPrediction)"),
          "smoothing"               -> (NameValue("smoothing"           , "DoubleType"   ), "The smoothing parameter. (default: 1.0)"),
          "thresholds"              -> (NameValue("thresholds" , "ArrayType(DoubleType)" ), "Thresholds in multi-class classification to adjust the probability of predicting each class. Array must have length equal to the number of classes, with values >= 0. The class with largest value p/t is predicted, where p is the original probability of that class and t is the class' threshold. (undefined)"))
    , MLTypes.MLAlgorithm)


  allMLNodes("OneVsRest") = OneNode("OneVsRest",
    "Reduction of Multiclass Classification to Binary Classification. Performs reduction using one against all strategy. For a multiclass classification with k classes, train k models (one per class). Each example is scored against all k models and the model with highest score is picked to label the example.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "featuresCol"             -> (NameValue("featuresCol"         , "StringType"   ), "features column name (default: features)"),
          "labelCol"                -> (NameValue("labelCol"            , "StringType"   ), "label column name (default: label)"),
          "predictionCol"           -> (NameValue("predictionCol"       , "StringType"   ), "prediction column name (default: prediction)"),
          "classifier"              -> (NameValue("classifier"          , "StringType"   ), "base binary classifier (undefined)"))
    , MLTypes.MLAlgorithm)


  allMLNodes("RandomForestClassifier") = OneNode("RandomForestClassifier",
    "Random Forest learning algorithm for classification. It supports both binary and multiclass labels, as well as both continuous and categorical features.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "cacheNodeIds"            -> (NameValue("cacheNodeIds"        , "BooleanType"  ), "If false, the algorithm will pass trees to executors to match instances with nodes. If true, the algorithm will cache node IDs for each instance. Caching can speed up training of deeper trees. (default: false)"),
          "checkpointInterval"      -> (NameValue("checkpointInterval"  , "IntType"      ), "set checkpoint interval (>= 1) or disable checkpoint (-1). E.g. 10 means that the cache will get checkpointed every 10 iterations (default: 10)"),
          "featureSubsetStrategy"   -> (NameValue("featureSubsetStrategy", "StringType"  ), "The number of features to consider for splits at each tree node. Supported options: auto, all, onethird, sqrt, log2 (default: auto)"),
          "featuresCol"             -> (NameValue("featuresCol"         , "StringType"   ), "features column name (default: features)"),
          "impurity"                -> (NameValue("impurity"            , "StringType"   ), "Criterion used for information gain calculation (case-insensitive). Supported options: entropy, gini (default: gini)"),
          "labelCol"                -> (NameValue("labelCol"            , "StringType"   ), "label column name (default: label)"),
          "maxBins"                 -> (NameValue("maxBins"             , "IntType"      ), "Max number of bins for discretizing continuous features.  Must be >=2 and >= number of categories for any categorical feature. (default: 32)"),
          "maxDepth"                -> (NameValue("maxDepth"            , "IntType"      ), "Maximum depth of the tree. (>= 0) E.g., depth 0 means 1 leaf node; depth 1 means 1 internal node + 2 leaf nodes. (default: 5)"),
          "maxMemoryInMB"           -> (NameValue("maxMemoryInMB"       , "IntType"      ), "Maximum memory in MB allocated to histogram aggregation. (default: 256)"),
          "minInfoGain"             -> (NameValue("minInfoGain"         , "DoubleType"   ), "Minimum information gain for a split to be considered at a tree node. (default: 0.0)"),
          "minInstancesPerNode"     -> (NameValue("minInstancesPerNode" , "IntType"      ), "Minimum number of instances each child must have after split.  If a split causes the left or right child to have fewer than minInstancesPerNode, the split will be discarded as invalid. Should be >= 1. (default: 1)"),
          "numTrees"                -> (NameValue("numTrees"            , "IntType"      ), "Number of trees to train (>= 1) (default: 20)"),
          "probabilityCol"          -> (NameValue("probabilityCol"      , "StringType"   ), "Column name for predicted class conditional probabilities. Note: Not all models output well-calibrated probability estimates! These probabilities should be treated as confidences, not precise probabilities (default: probability)"),
          "predictionCol"           -> (NameValue("predictionCol"       , "StringType"   ), "prediction column name (default: prediction)"),
          "rawPredictionCol"        -> (NameValue("rawPredictionCol"    , "StringType"   ), "raw prediction (a.k.a. confidence) column name (default: rawPrediction)"),
          "seed"                    -> (NameValue("seed"                , "LongType"     ), "random seed (default: -1287390502)"),
          "thresholds"              -> (NameValue("thresholds" , "ArrayType(DoubleType)" ), "Thresholds in multi-class classification to adjust the probability of predicting each class. Array must have length equal to the number of classes, with values >= 0. The class with largest value p/t is predicted, where p is the original probability of that class and t is the class' threshold. (undefined)"),
          "subsamplingRate"         -> (NameValue("subsamplingRate"     , "DoubleType"   ), "Fraction of the training data used for learning each decision tree, in range (0, 1]. (default: 1.0)"))
    , MLTypes.MLAlgorithm)


  /* BisectingKMeans and GaussianMixture are 2.0, skip */


  allMLNodes("KMeans") = OneNode("KMeans",
    "K-means clustering with support for k-means|| initialization proposed by Bahmani et al.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "featuresCol"             -> (NameValue("featuresCol"         , "StringType"   ), "initialization algorithm (default: k-means||)"),
          "initMode"                -> (NameValue("initMode"            , "StringType"   ), "label column name (default: label)"),
          "predictionCol"           -> (NameValue("predictionCol"       , "StringType"   ), "prediction column name (default: prediction)"),
          "k"                       -> (NameValue("k"                   , "IntType"      ), "number of clusters to create (default: 2)"),
          "maxIter"                 -> (NameValue("maxIter"             , "IntType"      ), "maximum number of iterations (>= 0) (default: 20)"),
          "initSteps"               -> (NameValue("initSteps"           , "IntType"      ), "number of steps for k-means|| (default: 5)"),
          "seed"                    -> (NameValue("seed"                , "LongType"     ), "random seed (default: -1689246527)"),
          "tol"                     -> (NameValue("stepSize"            , "DoubleType"   ), "the convergence tolerance for iterative algorithms (default: 1.0E-4)"))
    , MLTypes.MLAlgorithm)


  allMLNodes("LDA") = OneNode("LDA",
    "Latent Dirichlet Allocation (LDA), a topic model designed for text documents.\n\nTerminology:\n\n\"term\" = \"word\": an element of the vocabulary\n\"token\": instance of a term appearing in a document\n\"topic\": multinomial distribution over terms representing some concept\n\"document\": one piece of text, corresponding to one row in the input data\nOriginal LDA paper (journal version): Blei, Ng, and Jordan. \"Latent Dirichlet Allocation.\" JMLR, 2003.\n\nInput data (featuresCol): LDA is given a collection of documents as input data, via the featuresCol parameter. Each document is specified as a Vector of length vocabSize, where each entry is the count for the corresponding term (word) in the document. Feature transformers such as org.apache.spark.ml.feature.Tokenizer and org.apache.spark.ml.feature.CountVectorizer can be useful for converting text to word count vectors.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "checkpointInterval"      -> (NameValue("checkpointInterval"      , "IntType"      ), "set checkpoint interval (>= 1) or disable checkpoint (-1). E.g. 10 means that the cache will get checkpointed every 10 iterations (default: 10)"),
          "docConcentration"        -> (NameValue("docConcentration"        , "StringType"   ), "Concentration parameter (commonly named \"alpha\") for the prior placed on documents' distributions over topics (\"theta\"). (undefined)"),
          "featuresCol"             -> (NameValue("featuresCol"             , "StringType"   ), "features column name (default: features)"),
          "k"                       -> (NameValue("k"                       , "IntType"      ), "number of topics (clusters) to infer (default: 10)"),
          "learningDecay"           -> (NameValue("learningDecay"           , "DoubleType"   ), "Learning rate, set as an exponential decay rate. This should be between (0.5, 1.0] to guarantee asymptotic convergence. (default: 0.51)"),
          "learningOffset"          -> (NameValue("learningOffset"          , "DoubleType"   ), "A (positive) learning parameter that downweights early iterations. Larger values make early iterations count less. (default: 1024.0)"),
          "maxIter"                 -> (NameValue("maxIter"                 , "IntType"      ), "maximum number of iterations (>= 0) (default: 20)"),
          "optimizeDocConcentration"-> (NameValue("optimizeDocConcentration", "BooleanType"  ), "Indicates whether the docConcentration (Dirichlet parameter for document-topic distribution) will be optimized during training. (default: true)"),
          "optimizer"               -> (NameValue("optimizer"               , "StringType"   ), "Optimizer or inference algorithm used to estimate the LDA model.  Supported: online, em (default: online)"),
          "seed"                    -> (NameValue("seed"                    , "LongType"     ), "random seed (default: 1435876747)"),
          "subsamplingRate"         -> (NameValue("subsamplingRate"         , "DoubleType"   ), "Fraction of the corpus to be sampled and used in each iteration of mini-batch gradient descent, in range (0, 1]. (default: 0.05)"),
          "topicConcentration"      -> (NameValue("topicConcentration"      , "StringType"   ), "Concentration parameter (commonly named \"beta\" or \"eta\") for the prior placed on topic' distributions over terms. (undefined)"),
          "topicDistribution"       -> (NameValue("topicDistribution"       , "StringType"   ), "Output column with estimates of the topic mixture distribution for each document (often called \"theta\" in the literature).  Returns a vector of zeros for an empty document. (default: topicDistribution)"))
    , MLTypes.MLAlgorithm)


  allMLNodes("BinaryClassificationEvaluator") = OneNode("BinaryClassificationEvaluator",
    "Evaluator for binary classification, which expects two input columns: rawPrediction and label. The rawPrediction column can be of type double (binary 0/1 prediction, or probability of label 1) or of type vector (length-2 vector of raw predictions, scores, or label probabilities).",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "labelCol"                -> (NameValue("labelCol"         , "StringType"   ), "label column name (default: label)"),
          "metricName"              -> (NameValue("metricName"       , "StringType"   ), "metric name in evaluation (areaUnderROC|areaUnderPR) (default: areaUnderROC)"),
          "rawPredictionCol"        -> (NameValue("predictionCol"    , "StringType"   ), "raw prediction (a.k.a. confidence) column name (default: rawPrediction)"))
    , MLTypes.MLEvaluator)


  allMLNodes("MulticlassClassificationEvaluator") = OneNode("MulticlassClassificationEvaluator",
    "Evaluator for multiclass classification, which expects two input columns: prediction and label.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "labelCol"                -> (NameValue("labelCol"         , "StringType"   ), "label column name (default: label)"),
          "metricName"              -> (NameValue("metricName"       , "StringType"   ), "metric name in evaluation (f1|precision|recall|weightedPrecision|weightedRecall) (default: f1)"),
          "predictionCol"           -> (NameValue("predictionCol"    , "StringType"   ), "prediction column name (default: prediction)"))
    , MLTypes.MLEvaluator)

  
  allMLNodes("RegressionEvaluator") = OneNode("RegressionEvaluator",
    "Evaluator for regression, which expects two input columns: prediction and label.",
    Map ( "in" -> "input", "out" -> "output"),
    Map ( "labelCol"                -> (NameValue("labelCol"         , "StringType"   ), "label column name (default: label)"),
          "metricName"              -> (NameValue("metricName"       , "StringType"   ), "metric name in evaluation (mse|rmse|r2|mae) (default: rmse)"),
          "predictionCol"           -> (NameValue("predictionCol"    , "StringType"   ), "prediction column name (default: prediction)"))
    , MLTypes.MLEvaluator)

}
