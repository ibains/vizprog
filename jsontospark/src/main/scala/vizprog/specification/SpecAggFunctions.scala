package vizprog.specification

import scala.collection.mutable.Map

class SpecAggFunctions {

  val API_VERSION = "1.6.1"

  val aggFunctions = Map[String, OneFunction]()

  aggFunctions("array_contains") = OneFunction("array_contains", "BooleanType", 2,
    Array(NameValue("columnName", "StringType"), NameValue("value", "Any")),
    "Returns true if the array contain the value.")

  aggFunctions("take_last_nth") = OneFunction("take_last_nth", "Any", 2,
    Array(NameValue("columnName", "StringType"), NameValue("index", "IntType")),
    "Returns the nth value from the end of the array")

  aggFunctions("take_nth") = OneFunction("take_nth", "Any", 2,
    Array(NameValue("columnName", "StringType"), NameValue("index", "IntType")),
    "Returns the nth value from the array")

  aggFunctions("size") = OneFunction("size", "IntType", 1,
    Array(NameValue("columnName", "StringType")),
    "Returns length of array or map.")

  aggFunctions("sort_array_o") = OneFunction("sort_array", "Any", 2,
    Array(NameValue("columnName", "StringType"), NameValue("asc", "BooleanType")),
    "Sorts the input array for the given column in ascending / descending order, according to the natural ordering " +
      "of the array elements.")

  aggFunctions("sort_array") = OneFunction("sort_array", "IntType", 1,
    Array(NameValue("columnName", "StringType")),
    "Sorts the input array for the given column in ascending order, according to the natural ordering of the " +
      "array elements.")

  aggFunctions("json_tuple") = OneFunction("json_tuple", "Any", 2,
    Array(NameValue("columnName", "StringType"), NameValue("fields", "StringVarargType")),
    "Creates a new row for a json column according to the given field names.")

  aggFunctions("get_json_object") = OneFunction("get_json_object", "StringType", 2,
    Array(NameValue("columnName", "StringType"), NameValue("path", "StringType")),
    "Extracts json object from a json string based on json path specified, and returns json string of the extracted " +
      "json object. It will return null if the input json string is invalid.")
}
