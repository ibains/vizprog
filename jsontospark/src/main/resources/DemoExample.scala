val testPath = "/vagrant/tmp/resources/20news-bydate/20news-bydate-test/*"
val testRdd = sc.wholeTextFiles(testPath)

val testRddFixed = testRdd.map { case (file, text) =>
  val fileSplit = file.split("/")
  val group = fileSplit.takeRight(2).head
  val id = fileSplit.takeRight(1).head
  (id, group, text)
}
case class Article(id: Int, topic: String, text: String)
val articles = testRddFixed.map(x => {val (a,b,c) = x; Article(a.trim.toInt, b, c)}).toDF()
////////////////////////

import org.apache.spark.sql.functions._
val takeLastNth = udf ((a: Seq[String], b: Int) => { a.takeRight(b).head})

case class WholeFiles(file: String, text: String)
val testPath = "/vagrant/tmp/resources/20news-bydate/20news-bydate-test/*"
val testRdd = sc.wholeTextFiles(testPath)
val readFiles = testRdd.map( x => { val (a,b) = x; WholeFiles(a,b)} ).toDF()

val filesWithSplit = readFiles.withColumn("split_file", split(readFiles("file"), "/"))
val filesWithSplit2 = filesWithSplit.withColumn("topic", takeLastNth(filesWithSplit("split_file"), lit(2)))
val filesWithSplit3 = filesWithSplit2.withColumn("Id", takeLastNth(filesWithSplit("split_file"), lit(1)))
val cleansedData = filesWithSplit3.select($"Id", $"topic", $"text")

val topic2Label: Boolean => Double = isSci => if (isSci) 1 else 0
val toLabel = udf(topic2Label)
val labelledDf = cleansedData.withColumn("label", toLabel($"topic".like("sci%")))

val out_n9 = labelledDf.randomSplit(Array(0.9, 0.1), 37)
val one_n9 = out_n9(0)
val two_n9 = out_n9(1)
val one_counts = one_n9.groupBy($"label").count().show()
val two_counts = two_n9.groupBy($"label").count().show()
val train = one_n9
val test = two_n9
val out_n3 = new Tokenizer().setInputCol("text").setOutputCol("words")
val out_n4 = new HashingTF().setInputCol("words").setOutputCol("myFeatures").setNumFeatures(64000 )
val dummy_out_n5 = new LogisticRegression().setMaxIter( 10 ).setRegParam( 0.001 ).setFeaturesCol("myFeatures").setLabelCol("label")
val out_m4 = new Pipeline().setStages(Array(out_n3, out_n4, dummy_out_n5))
val out_n5 = out_m4.fit(train)

import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator

val evaluator = new BinaryClassificationEvaluator().setMetricName("areaUnderROC")
val areaUnderRocTrain = evaluator.evaluate(trainOnTrain)

val testResults = trainOnTest.select($"Id", $"topic", $"label", $"prediction", $"text")
val mispredictions = testResults.filter($"label" !== $"prediction")
val falsePositives = mispredictions.filter($"label" === 0.0)
val falseNegatives = mispredictions.filter($"label" === 1.0)
val ttt = sampleToCsv(falsePositives.select($"topic", $"text").collect())
stringToFile(ttt, "/vagrant/tmp/falsePositivesOutput.csv")




