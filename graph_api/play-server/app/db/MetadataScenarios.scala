package db

import dao._

// This class simulates certain categories of usage

class MetadataScenarios {

  // Every Operation Needs Company as the context

  val companyName = "VizProg"

  def NewCompanyCreation(driver: DSEDriver) : Unit = {

    val adminName = "Bob Smith"
    val adminEmail = "bob.smith@vizprog.com"

    val company = Company + (driver, companyName, adminName, adminEmail)

    // TESTS
    NewCompanyCreationTest(company)
  }

  protected def NewCompanyCreationTest(entity: SimpleEntity) : Unit = {

    // A company was created
    assert(entity != null && entity.isCompany, s"Entity ($entity) should not be null and should be a company")

    // There exists exactly one
    val admins = entity.getRoleHolders(Permissions.admin)
    assert(admins.length == 1, s"Admins length (${admins.length}) should be 1")

    // The admin is a person
    val user = admins.head
    assert(user.isPerson, s"User ($user) should be a person")

    // The admin is the correct person
    val personUser = user.asPerson
    assert(personUser.name == "Bob Smith", s"Admin ($personUser) should be Bob Smith")
  }

  def AddUsersToCompany(driver: DSEDriver) : Unit = {

    val company = Company.getByName(driver, companyName)

    // Add Persons
    val p1 = Person + (driver, company, "Greg Dean", "Greg.Dean@vizprog.com")
    val p2 = Person + (driver, company, "Ed Johnson", "Ed.Johnson@vizprog.com")
    val p3 = Person + (driver, company, "Jane Doe", "Jane.Doe@vizprog.com")
    val p4 = Person + (driver, company, "Paul Williams", "Paul.Williams@vizprog.com")

    AddUsersToCompanyTest(driver)
  }

  def AddUsersToCompanyTest(driver: DSEDriver) : Unit = {
    val company = Company.getByName(driver, companyName)
    val users = company.users
    assert(users.length == 5, s"Expecting length 5, got ${users.length} from $users")
  }

  def AddGroupAndProjects(driver: DSEDriver) : Unit = {

    val company = Company.getByName(driver, companyName)

    // Create some groups and add them to the company
    val metadataGroup  = Group + (driver, "Metadata", "The team working on metadata design")
    val frontendGroup  = Group + (driver, "FrontEnd", "The team working on front-end implementation")
    val executionGroup = Group + (driver, "Execution", "The team working on execution and cloud deployment")

    company --> metadataGroup
    company --> frontendGroup
    company --> executionGroup

    // Add correct users to these groups

    val greg = Person.get(driver, companyName, "Greg Dean")
    val sid = Person.get(driver, companyName, "Ed Johnson")

    metadataGroup --> greg
    metadataGroup --> sid

    val bob = Person.get(driver, companyName, "Bob Smith")
    val jane  = Person.get(driver, companyName, "Jane Doe")

    frontendGroup --> bob
    frontendGroup --> jane

    val paul = Person.get(driver, companyName, "Paul Williams")

    executionGroup --> paul

    val graphsProject    = Project + (driver, "Graphs", "Explore Graph Databases")
    val demosProject     = Project + (driver, "Demos", "Get Demo Ready")
    val executionProject = Project + (driver, "Execution", "Explore Execution Technologies")

    // Add projects to the company
    company --> graphsProject
    company --> demosProject
    company --> executionProject

    // Give groups permissions over the projects
    graphsProject    <== (metadataGroup, Permissions.write)
    demosProject     <== (frontendGroup, Permissions.write)
    executionProject <== (executionGroup, Permissions.write)

  }
  def AddGroupAndProjectsTest(driver: DSEDriver) : Unit = {
    // TODO: add asserts to ensure the graph formed as expected
  }

  def AddWorkflowsAndDatasets(driver: DSEDriver) : Unit = {

    val execProject = Project.get(driver, companyName, "Execution")
    Workflow + (driver, execProject, "container_orchestration", "Graph : { description: Investigate mesosphere and kubernetes, along with docker }")
    Workflow + (driver, execProject, "spark_scheduler", "Graph : { description: Should Spark use Mesos or YARN for scheduling }")


    val demosProject = Project.get(driver, companyName, "Demos")
    Workflow + (driver, demosProject, "stat_popup", "Graph : { description: Add stats popups }")
    Workflow + (driver, demosProject, "script_node", "Graph : { description: Add script node }")

    val graphsProject = Project.get(driver, companyName, "Graphs")
    Workflow + (driver, graphsProject, "graph_api", "Graph : { description: Add graph based API }")
    val graphTech = Workflow + (driver, graphsProject, "graph_technology", "Graph : { description: Select Neo or DSE }")

    // Now, in the graph project we decide to give permission to Bob, to the technology project
    val bob = Person.get(driver, companyName, "Bob Smith")

    graphTech <== (bob, Permissions.read)

    AddWorkflowsAndDatasetsTest(driver)
  }

  def AddWorkflowsAndDatasetsTest(driver: DSEDriver) : Unit = {
    val bob = Person.get(driver, companyName, "Bob Smith")
    val bobWorkflows = bob.getWorkflows.get
    for ((permission, label, simpleEntity) <- bobWorkflows) {
      println(DB_Permissions.s(permission) + " " + simpleEntity.asWorkflow.D.name)
    }
  }
}

