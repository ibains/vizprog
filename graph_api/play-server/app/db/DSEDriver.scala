package db

import java.util

import com.datastax.driver.dse.graph.{SimpleGraphStatement, _}
import com.datastax.driver.dse.{DseCluster, DseSession}
import com.google.common.reflect.TypeToken
import dao._
import db.QueryBuilder.{GraphAccessQuery, GraphMutationQuery}
import org.apache.tinkerpop.gremlin.structure.{Edge, Vertex}
import scala.collection.JavaConversions._

// Facilitate session mgmt with persisting cluster in a session
case class DSEDriver(cluster: DseCluster) extends AutoCloseable {
  val session = cluster.connect()

  import DSEDriver.disconnect
  override def close(): Unit = disconnect(session, cluster)

  // Coerce dse to tinkerpop data structures
  // Mutations
  def addVertex(query: GraphMutationQuery) : Vertex = {
    println(s"Execute :: ${query.toString}")
    session.executeGraph(query.toString).one().as(new TypeToken[Vertex]() {})
  }

  def addEdge(v1: Vertex, label: String, v2: Vertex) : Edge = {
    println("Execute :: addEdge: " + v1.id() + " -- " + label + " --> " + v2.id())
    val s = new SimpleGraphStatement(
      "def v1 = g.V(id1).next()\n" +
        "def v2 = g.V(id2).next()\n" +
        s"v1.addEdge(${U.Q(label)}, v2)")
      .set("id1", v1.id())
      .set("id2", v2.id())
    session.executeGraph(s).one().as(new TypeToken[Edge]() {})
    //    session.executeGraph(query.toString).one().as(new TypeToken[Edge]() {})
  }

  // Queries
  def getVertex(query: GraphAccessQuery): Vertex = {
    println(s"Execute :: ${query.toString}")
    session.executeGraph(query.toString).one().as(new TypeToken[Vertex]() {})
  }

  def getVertices(query: GraphAccessQuery): List[Vertex] = {
    println(s"Execute :: ${query.toString}")
    val nodes: util.List[GraphNode] = session.executeGraph(query.toString).all()
    nodes.map(e => e.as(new TypeToken[Vertex]() {})).toList
  }

  // Console Output :: ==> {perm=4, proj=v[Demos]}
  // Name selectors as xxx-vertex or xxx-edge for default coerece
  def getValuesAsMap(query: GraphAccessQuery): Option[List[scala.collection.Map[String, AnyRef]]] = {
    println(s"Execute :: ${query.toString}")
    val results = session.executeGraph(query.toString).all()
    val values = new scala.collection.mutable.ListBuffer[scala.collection.Map[String, AnyRef]]()
    if (results != null) {
      val iterator = results.listIterator()
      while (iterator.hasNext) {
        val g = iterator.next()
        val fields = g.fieldNames().toList
        val m = scala.collection.mutable.Map.empty[String, AnyRef]
        for (f <- fields) {
          if (f contains "vertex") m += (f -> g.get(f).as(new TypeToken[Vertex]() {}))
          else if (f contains "edge") m += (f -> g.get(f).as(new TypeToken[Edge]() {}))
          else m += (f -> g.get(f))
        }
        values += m
//        values += mapAsScalaMap(g.asMap())
      }
      Some(values.toList)
    } else {
      None
    }
  }

  def getEdge(query: GraphAccessQuery): Edge = {
    println(s"Execute :: ${query.toString}")
    session.executeGraph(query.toString + ".next()").one().as(new TypeToken[Edge]() {})
  }

  def getEdges(query: GraphAccessQuery): List[Edge] = {
    println(s"Execute :: ${query.toString}")
    val nodes: util.List[GraphNode] = session.executeGraph(query.toString + ".next()").all()
    nodes.map(e => e.as(new TypeToken[Edge]() {})).toList
  }
}

object DSEDriver {

  def printSchema() = {
    val propertyKeySchema = dseKey.schemaString
    val vertexSchema      = dseVertex.schemaString
    val edgeSchema        = dseEdge.schemaString
    val indexSchema       = dseIndex.schemaString

    println(propertyKeySchema)
    println()
    println(vertexSchema)
    println()
    println(edgeSchema)
    println()
    println(indexSchema)
  }

  def connect(): DseCluster = {
    val cluster = DseCluster.builder()
      .addContactPoint("graphdb.vizprog.com")
      .withPort(9042)
      .build()

    cluster
  }

  def connect(contactPoint: String = "graphdb.vizprog.com", port: Int = 9042, graph: Option[String]): DseCluster = {
    if (graph.isDefined) {
      val cluster = DseCluster.builder()
        .addContactPoint(contactPoint)
        .withGraphOptions(new GraphOptions().setGraphName(graph.get))
        .withPort(port)
        .build()

      cluster
    } else {
      val cluster = DseCluster.builder()
        .addContactPoint(contactPoint)
        .withPort(port)
        .build()

      cluster
    }
  }

  def disconnect(session: DseSession, cluster: DseCluster): Unit = {
    if (!session.isClosed) session.close()
    if (!cluster.isClosed) cluster.close()
  }

  def createGraph(cluster: DseCluster, graphName: String) = {
    val session = cluster.connect()

    val s = new StringBuilder
    s ++= s"system.graph(${U.Q(graphName)})"
    s ++= ".option(\"graph.allow_scan\").set(\"true\")"
    s ++= ".option(\"graph.schema_mode\").set(\"Development\").ifNotExists().create()"

    val q = s.toString()

    println("Create graph query :: " + q)

    session.executeGraph(q)

    disconnect(session, cluster)
  }

  // NOTE: Setting graph name after creating cluster doesn't work, so need to
  // create schema, close session and create new session with graph name set
  // for queries of the form "g.V()" to work !!
  /**
    * Idempotent schema initialize
    */
  def initialize(cluster: DseCluster): Unit = {
    val session = cluster.connect()
//    session.executeGraph("schema.clear()")
    dseKey.keys.foreach(e => session.executeGraph(e._2.toString))
    dseVertex.vertices.foreach(e => session.executeGraph(e._2.toString))
    dseEdge.edges.foreach(e => session.executeGraph(e._2.toString))
    dseIndex.indexes.foreach(e => session.executeGraph(e._2.toString))
    session.close()
  }

  def addDummyData(cluster: DseCluster): Unit = {
    val driver = DSEDriver(cluster)
    val vertexCount = driver.session.executeGraph("g.V().count()").one().asInt()
    println("Vertices count => " + vertexCount)
    if (vertexCount == 0) {
      val scenarios = new MetadataScenarios()
      scenarios.NewCompanyCreation(driver)
      scenarios.AddUsersToCompany(driver)
      scenarios.AddGroupAndProjects(driver)
      scenarios.AddWorkflowsAndDatasets(driver)
    }
  }

  def runTests(cluster: DseCluster): Unit = {
    val driver = DSEDriver(cluster)
    val session = driver.session
    println("Vertices:" + session.executeGraph("g.V().count()").one())
    val scenarios = new MetadataScenarios()
    scenarios.NewCompanyCreation(driver)
    scenarios.AddUsersToCompany(driver)
    scenarios.AddGroupAndProjects(driver)
    scenarios.AddWorkflowsAndDatasets(driver)
    println("Vertices:" + session.executeGraph("g.V().count()").one())
    disconnect(session, cluster)
  }

  def main(args: Array[String]): Unit = {
    DSEDriver.printSchema()
    val cluster = connect()
    initialize(cluster)
    // Drop all data without dropping a graph and schema, drop all vertices.
    runTests(cluster)
    sys.exit()
  }
}


