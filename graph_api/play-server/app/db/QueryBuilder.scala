package db

import dao.{DB_PROPERTY, U}

import scala.collection.immutable.ListMap

object QueryBuilder {
  // Vertex, Edge
  sealed abstract class GraphMutationQuery(label: String, properties: List[(String, Any)]) {
    def getQueryPrefix: String

    override def toString: String = {
      val query = new StringBuilder
      val prefix = getQueryPrefix
      query ++= prefix
      properties.foreach{case (id, value) => query ++= s", ${U.Q(id)}, ${U.Q(value)}"}
      query ++= ")"
      query.toString
    }
  }

  case class addVQ(label: String, properties: List[(String, Any)]) extends GraphMutationQuery(label, properties) {
    override def getQueryPrefix = s"graph.addVertex(label, ${U.Q(label)}"
  }

  val emptyQ: String = ""

  sealed abstract class GraphAccessQuery(base: String) {
    var query = new StringBuilder(base)
    var selectors = scala.collection.immutable.ListMap[String, String]()

    override def toString: String = {
      if (selectors.nonEmpty) {
        query ++= s".select("

        var prefix = ""
        selectors.keys.foreach { s => query ++= prefix; query ++= s"${U.Q(s)}"; prefix = "," }
        query ++= ")"
        selectors.values.foreach {
          p => if (p != DB_PROPERTY.id) query ++= s".by(${U.Q(p)})" else query ++= ".by()"
        }
        selectors = ListMap.empty
      }
      query.toString
    }

    def values(): String = { query ++= ".values()" ; query.toString }
    def valueByProperty(property: String): String = { query ++= s".values(${U.Q(property)}"; query.toString }
    def count(): String = { query ++= ".count()" ; query.toString }
  }

  case class VQBuilder(base: String) extends GraphAccessQuery(base: String) {
    if (base.isEmpty) query ++= "g.V()"

    // TODO: Figure out why sangria screws up id field
    def findVByLabelPropQ(label: String, property: String, value: Any): VQBuilder = {
      if (property == DB_PROPERTY.id) {
        // community_id.member_id
        val id = if (value.toString contains "Some") value.toString.replaceAll("[^\\d\\.]", "") else value.toString
        val communityId = id.split('.')(0).toLong
        val memberId    = id.split('.')(1).toLong
        query ++= s".has(${U.Q(label)}, ${U.Q("community_id")}, ${U.Q(communityId)})"
        query ++= s".has(${U.Q(label)}, ${U.Q("member_id")}, ${U.Q(memberId)})"
      } else {
        query ++= s".has(${U.Q(label)}, ${U.Q(property)}, ${U.Q(value)})"
      }
      this
    }

    def findVByLabelQ(label: String) : VQBuilder = {
      query ++= s".hasLabel(${U.Q(label)})"
      this
    }

    def findVByEQ(edge: (String, Option[String]) ) : VQBuilder = {
      query ++= s".${edge._1}(${U.?(edge._2)})"
      this
    }

    def outE(label: Option[String]): VQBuilder = {
      if (label.isDefined) query ++= s".outE(${U.Q(label.get)})" else query ++= ".outE()"
      this
    }

    def inE(label: Option[String]): VQBuilder = {
      if (label.isDefined) query ++= s".inE(${U.Q(label.get)})" else query ++= ".inE()"
      this
    }

    def outV(): VQBuilder = {
      query ++= ".outV()"
      this
    }

    def inV(): VQBuilder = {
      query ++= ".inV()"
      this
    }

    def as(selector: String, property: String) = {
      query ++= s".as(${U.Q(selector)})"
      selectors = selectors + (selector -> property)
      this
    }
  }

  case class EQBuilder(base: String) extends GraphAccessQuery(base: String) {
    if (base.isEmpty) query ++= "g.E()"

    def findEByLabelQ(label: String): EQBuilder = {
      query ++= s".hasLabel(${U.Q(label)})"
      this
    }

    def outV(): EQBuilder = {
      query ++= ".outV()"
      this
    }

    def inV(): EQBuilder = {
      query ++= ".inV()"
      this
    }
  }

  /*case class findVQ(label: String, property: Option[(String, String)])
    extends BaseVQ {
    if (property.isDefined) {
      val pTuple = property.get
      query ++= s".has(${U.Q(label)}, ${U.Q(pTuple._1)}, ${U.Q(pTuple._2)})"
    } else {
      query ++= s".hasLabel($label)"
    }
  }

  case class findVByLabelQ(queryPattern: GraphAccessQuery, label: String) extends BaseVQ {
    query ++= s".hasLabel($label)"
  }

  case class findVByLabelPropQ(queryPattern: GraphAccessQuery, label: String, property: Option[(String, String)]) extends BaseVQ {
    query.clear()
    query ++= queryPattern.toString
    findVQ(label, property)
  }

  case class findVByEQ(queryPattern: GraphAccessQuery, edges: List[(String, Option[String])], property: Option[String]) extends BaseVQ {
    query ++= queryPattern.query
    edges.foreach{ case (direction, label) => query ++= s".$direction(${U.?(label)})" }
//    query.toString
  }

  case class findEByLabelQ(label: String) extends BaseEQ {
    query ++= s".hasLabel(${U.Q(label)})"
  }

  case class outE(queryPattern: GraphAccessQuery, label: Option[String]) extends GraphAccessQuery {
    if (label.isDefined) query ++= s".outE(${U.Q(label)})" else query ++= ".outE()"
  }

  case class inE(queryPattern: GraphAccessQuery, label: Option[String]) extends GraphAccessQuery {
    if (label.isDefined) query ++= s".inE(${U.Q(label)})" else query ++= ".inE()"
  }

  case class outV(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".inV()" }
  case class inV(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".outV()" }
  case class values(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".values()" }
  case class valueByProperty(queryPattern: GraphAccessQuery, property: String) extends GraphAccessQuery { query ++= s".values(${U.Q(property)}" }
  case class count(queryPattern: GraphAccessQuery) extends GraphAccessQuery { query ++= ".count()" }
  */
}
