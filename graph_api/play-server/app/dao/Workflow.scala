package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object Workflow {

  // ADDERS

  def + (driver: DSEDriver, project: Project, name: String, content: String) : Workflow = {
    val w = new Workflow(driver,
      driver.addVertex(addVQ(DB_VERTEX.workflow, List(
        (DB_PROPERTY.name, name), (DB_PROPERTY.content, content), (DB_PROPERTY.created, Util.tt)
      )))
    )
//    val w = new Workflow(Graph.get + DB_Workflow(None, name, content, Util.tt))
    project --> w
    w
  }

  // GETTERS - ALL

  def getAll(driver: DSEDriver) : List[Workflow] = {
    driver.getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.workflow)
    ).map(x => new Workflow(driver, x))
//    Graph.get.V.hasLabel(DB_LABELS.workflow).toList.map(x => new Workflow(x))
  }

  // GETTERS - FILTERED

  def get(driver: DSEDriver, id: String) : Workflow = {
    new Workflow(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.workflow, DB_PROPERTY.id, id)
    ))
  }

  def getDirectUsers(driver: DSEDriver, id: String): Option[List[(Person, Int)]] = {
    val m = driver.getValuesAsMap(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.workflow, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Permission)).outV().as("perm", DB_PROPERTY.permission)
        .inE(Some(DB_EDGE.Permission)).outV()
        .outE(Some(DB_EDGE.Member)).inV().as("user-vertex", DB_PROPERTY.id)
        .findVByLabelQ(DB_VERTEX.person)
    )
    val users = scala.collection.mutable.ListBuffer.empty[(Person, Int)]
    if (m.isDefined) {
      for (up <- m.get) {
        val user = new Person(driver, up("user-vertex").asInstanceOf[Vertex])
        val perm = up("perm").toString.toInt

        users += ((user, perm))
      }
    }
    Some(users.toList)
  }

  def getDirectGroups(driver: DSEDriver, id: String): Option[List[(Group, Int)]] = {
    val m = driver.getValuesAsMap(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.workflow, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Permission)).outV().as("perm", DB_PROPERTY.permission)
        .inE(Some(DB_EDGE.Permission)).outV()
        .findVByLabelQ(DB_VERTEX.group).as("group-vertex", DB_PROPERTY.id)
    )
    val groups = scala.collection.mutable.ListBuffer.empty[(Group, Int)]
    if (m.isDefined) {
      for (gp <- m.get) {
        val group = new Group(driver, gp("group-vertex").asInstanceOf[Vertex])
        val perm = gp("perm").toString.toInt

        groups += ((group, perm))
      }
    }
    Some(groups.toList)
  }
}

class Workflow(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v) {
  val D: DB_Workflow = Model.coerce(v, this).asInstanceOf[DB_Workflow]

  def getProject: Project = {
    new Project(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.workflow, DB_PROPERTY.id, D.id.get)
        .inE(Some(DB_EDGE.Member)).outV()
    ))
  }
}