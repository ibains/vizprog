package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object Execution {

  // ADDERS

  def addExecution(driver: DSEDriver, status: String) : Execution = {
    new Execution(driver, driver.addVertex(addVQ(DB_VERTEX.execution, List(
      (DB_PROPERTY.status, status)
    ))))
//    new Execution(Graph.get + DB_Execution(None, status, Util.tt, 0L))
  }
}


class Execution(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v)  {
  val D = Model.coerce(v, this).asInstanceOf[DB_Execution]

}