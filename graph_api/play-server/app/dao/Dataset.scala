package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.{Direction, Vertex}

object Dataset {

  // ADDERS

  def + (driver: DSEDriver, project: Project, name: String, schemaJson: String,
         statsJson: String, diskSizeGb: Long) : Dataset = {
    val d = new Dataset(driver, driver.addVertex(addVQ(DB_VERTEX.dataset, List(
      (DB_PROPERTY.name, name), (DB_PROPERTY.schemaJson, schemaJson),
      (DB_PROPERTY.statsJson, statsJson), (DB_PROPERTY.diskSizeGb, diskSizeGb),
      (DB_PROPERTY.created, Util.tt)
    ))))

    project --> d
    d
  }

  def get(driver: DSEDriver, id: String): Dataset = {
    new Dataset(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.dataset, DB_PROPERTY.id, id)
    ))
  }

  // Workflow --> Role --> Dataset
  def getDirectWorkflows(driver: DSEDriver, id: String): Option[List[(Workflow, Int)]] = {
    val m = driver.getValuesAsMap(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.dataset, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Permission)).outV().as("perm", DB_PROPERTY.permission)
        .inE(Some(DB_EDGE.Permission)).outV()
        .findVByLabelQ(DB_VERTEX.workflow)
        .as("wf-vertex", DB_PROPERTY.id)
    )
    val wfs = scala.collection.mutable.ListBuffer.empty[(Workflow, Int)]
    if (m.isDefined) {
      for (up <- m.get) {
        val wf = new Workflow(driver, up("wf-vertex").asInstanceOf[Vertex])
        val perm = up("perm").toString.toInt

        wfs += ((wf, perm))
      }
    }

    Some(wfs.toList)
  }

  // Project <-- Role <-- Group --> Person
  def getDirectUsers(driver: DSEDriver, id: String): Option[List[(Person, Int)]] = {
    val m = driver.getValuesAsMap(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.dataset, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Permission)).outV().as("perm", DB_PROPERTY.permission)
        .inE(Some(DB_EDGE.Permission)).outV()
        .outE(Some(DB_EDGE.Member)).inV()
        .findVByLabelQ(DB_VERTEX.person).as("user-vertex", DB_PROPERTY.id)
    )
    val users = scala.collection.mutable.ListBuffer.empty[(Person, Int)]
    if (m.isDefined) {
      for (up <- m.get) {
        val user = new Person(driver, up("user-vertex").asInstanceOf[Vertex])
        val perm = up("perm").toString.toInt
        users += ((user, perm))
      }
    }

    Some(users.toList)
  }

  def getDirectGroups(driver: DSEDriver, id: String): Option[List[(Group, Int)]] = {
    val m = driver.getValuesAsMap(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.dataset, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Permission)).outV().as("perm", DB_PROPERTY.permission)
        .inE(Some(DB_EDGE.Permission)).outV()
        .findVByLabelQ(DB_VERTEX.group).as("group-vertex", DB_PROPERTY.id)
    )
    val groups = scala.collection.mutable.ListBuffer.empty[(Group, Int)]
    if (m.isDefined) {
      for (gp <- m.get) {
        val group = new Group(driver, gp("group-vertex").asInstanceOf[Vertex])
        val perm = gp("perm").toString.toInt
        groups += ((group, perm))
      }
    }

    Some(groups.toList)
  }
}

class Dataset(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v) {
  val D: DB_Dataset = Model.coerce(v, this).asInstanceOf[DB_Dataset]

  def getProject: Project = {
    new Project(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.dataset, DB_PROPERTY.id, D.id)
        .inE(Some(DB_EDGE.Member)).outV().findVByLabelQ(DB_VERTEX.project)
    ))
  }
}
