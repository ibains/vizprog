package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object Permissions {
  val admin = 1
  val read  = 2
  val write = 4
}

/*
 * Simple Entity allows us to treat all nodes with a base class
 * and do operations common to them
 */

object SimpleEntity {
  def get(driver: DSEDriver, v: Vertex) : SimpleEntity = {
    v.label match {
      case  DB_VERTEX.company            => new Company(driver, v)
      case  DB_VERTEX.group              => new Group(driver, v)
      case  DB_VERTEX.project            => new Project(driver, v)
      case  DB_VERTEX.person             => new Person(driver, v)
      case  DB_VERTEX.role               => new Role(driver, v)
      case  DB_VERTEX.workflow           => new Workflow(driver, v)
      case  DB_VERTEX.dataset            => new Dataset(driver, v)
      case  DB_VERTEX.function           => new Function(driver, v)
      case  DB_VERTEX.execution          => new Execution(driver, v)
      case  DB_VERTEX.externaldatasource => new ExternalDataSource(driver, v)
      case  DB_VERTEX.credentials        => new Credentials(driver, v)
      case  x                            => assert(false, s"Incorrect Label: ${v.label}"); null
    }
  }
}

class SimpleEntity(driver: DSEDriver, v: Vertex) {
  var V : Vertex = v

  def isCompany             : Boolean = V.label == DB_VERTEX.company
  def isGroup               : Boolean = V.label == DB_VERTEX.group
  def isProject             : Boolean = V.label == DB_VERTEX.project
  def isPerson              : Boolean = V.label == DB_VERTEX.person
  def isRole                : Boolean = V.label == DB_VERTEX.role
  def isWorkflow            : Boolean = V.label == DB_VERTEX.workflow
  def isDataset             : Boolean = V.label == DB_VERTEX.dataset
  def isFunction            : Boolean = V.label == DB_VERTEX.function
  def isExecution           : Boolean = V.label == DB_VERTEX.execution
  def isExternalDatasource  : Boolean = V.label == DB_VERTEX.externaldatasource
  def isCredentials         : Boolean = V.label == DB_VERTEX.credentials
  def isGroupEntity         : Boolean = V.label match {  // can do runtime class lookup too
    case DB_VERTEX.company | DB_VERTEX.group | DB_VERTEX.project => true
    case _ => false
  }

  def asCompany             : Company            = this.asInstanceOf[Company]
  def asGroup               : Group              = this.asInstanceOf[Group]
  def asProject             : Project            = this.asInstanceOf[Project]
  def asPerson              : Person             = this.asInstanceOf[Person]
  def asRole                : Role               = this.asInstanceOf[Role]
  def asWorkflow            : Workflow           = this.asInstanceOf[Workflow]
  def asDataset             : Dataset            = this.asInstanceOf[Dataset]
  def asFunction            : Function           = this.asInstanceOf[Function]
  def asExecution           : Execution          = this.asInstanceOf[Execution]
  def asExternalDatasource  : ExternalDataSource = this.asInstanceOf[ExternalDataSource]
  def asCredentials         : Credentials        = this.asInstanceOf[Credentials]

  def getRoleHolders(permissions: Int): List[SimpleEntity] =
    driver.getVertices(
      VQBuilder(emptyQ).findVByEQ((inE.name, Some(DB_EDGE.Permission)))
        .outV().findVByEQ((inE.name, Some(DB_EDGE.Permission))).outV()
    ).map(x => SimpleEntity.get(driver, x))
//    V.in(DB_EDGE.Permission).in(DB_EDGE.Permission).toList.map(x => SimpleEntity.get(x) )

  def vLabel = v.label()
  def vId    = v.id()

  def <== (entity: SimpleEntity, permission: Int) = {

    // only people or persons can be given permissions
    assert(entity.isPerson || entity.isGroup)

    // you cannot give permission over a person, you'll be reported to UN
    assert(!isPerson)

    Role + (driver, V, entity.V, permission)
  }
}

/*
 * Group Entity adds a few operations on top of SimpleEntity that
 * only make sense for groups, such as adding members
 */

class GroupEntity(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v) {
  def addMember(entity: Vertex, user: Vertex) : Unit = {
    driver.addEdge(entity, DB_EDGE.Member, user)
//    entity --- DB_EDGE.Member --> user                         // TODO: dupe check
  }

  def members() : List[(String, SimpleEntity)] = for {
      member <- driver.getVertices(VQBuilder(emptyQ).findVByEQ((outE.name, Some(DB_EDGE.Member))).inV())
//      member <- v.out(DB_EDGE.Member).toList()
    } yield (member.label(), SimpleEntity.get(driver, member))
}

