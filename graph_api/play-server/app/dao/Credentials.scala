package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object Credentials {

  // ADDERS

  def + (driver: DSEDriver, credentialType: String, key1: String, key2: String) : Credentials = {
    new Credentials(driver, driver.addVertex(addVQ(DB_VERTEX.credentials, List(
      (DB_PROPERTY.credentialType, credentialType), (DB_PROPERTY.key1, key1),
      (DB_PROPERTY.key2, key2), (DB_PROPERTY.created, Util.tt)
    ))))
//    new Credentials(Graph.get + DB_Credentials(None, credentialType, key1, key2, Util.tt))
  }

}

class Credentials(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_Credentials]
}