package dao

import java.util

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object Project {

  // ADDERS

  def + (driver: DSEDriver, name: String, description: String) : Project = {
    new Project(driver,
      driver.addVertex(addVQ(DB_VERTEX.project, List(
        (DB_PROPERTY.name, name), (DB_PROPERTY.description, description), (DB_PROPERTY.created, Util.tt)
      )))
    )
//    new Project(Graph.get + DB_Project(None, name, description, Util.tt))
  }

  def get(driver: DSEDriver, id: String): Project = {
    new Project(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.id, id)
    ))
  }

  def get(driver: DSEDriver, companyName: String, name: String) : Project = {
    val company = Company.getByName(driver, companyName)

    new Project(driver,
      driver.getVertex(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.id, company.D.id.get) //company
          .findVByEQ((outE.name, Some(DB_EDGE.Member))).inV()
            .findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.name, name)
      )
    )
  }

  // Project <-- Role <-- Group --> Person
  // g.V().has("project", "community_id", 885336064).has("project", "member_id", 3).inE("permission").outV().as("perm").inE("permission").outV().outE("member").inV().hasLabel("person").as("user").select("perm","user").by("permission").by()
  def getUsers(driver: DSEDriver, id: String): Option[List[(Person, Int)]] = {
    val m = driver.getValuesAsMap(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Permission)).outV().as("perm", DB_PROPERTY.permission)
        .inE(Some(DB_EDGE.Permission)).outV()
        .outE(Some(DB_EDGE.Member)).inV()
        .findVByLabelQ(DB_VERTEX.person).as("user-vertex", DB_PROPERTY.id)
    )
    val users = scala.collection.mutable.ListBuffer.empty[(Person, Int)]
    if (m.isDefined) {
      for (up <- m.get) {
        val user = new Person(driver, up("user-vertex").asInstanceOf[Vertex])
        val perm = up("perm").toString.toInt
        users += ((user, perm))
      }
    }

    Some(users.toList)
  }

  def getGroups(driver: DSEDriver, id: String): Option[List[(Group, Int)]] = {
    val m = driver.getValuesAsMap(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Permission)).outV().as("perm", DB_PROPERTY.permission)
        .inE(Some(DB_EDGE.Permission)).outV()
        .findVByLabelQ(DB_VERTEX.group).as("group-vertex", DB_PROPERTY.id)
    )
    val groups = scala.collection.mutable.ListBuffer.empty[(Group, Int)]
    if (m.isDefined) {
      for (gp <- m.get) {
        val group = new Group(driver, gp("group-vertex").asInstanceOf[Vertex])
        val perm = gp("perm").toString.toInt
        groups += ((group, perm))
      }
    }

    Some(groups.toList)
  }

  def getWorkflows(driver: DSEDriver, id: String): List[Workflow] = {
    driver.getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.id, id)
        .outE(Some(DB_EDGE.Member)).inV().findVByLabelQ(DB_VERTEX.workflow)
    ).map(v => new Workflow(driver, v))
  }

  def getFunctions(driver: DSEDriver, id: String): List[Function] = {
    driver.getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.id, id)
        .outE(Some(DB_EDGE.Member)).inV().findVByLabelQ(DB_VERTEX.function)
    ).map(v => new Function(driver, v))
  }

  def getCompany(driver: DSEDriver, id: String): Option[Company] = {
    val v = driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.project, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Member)).outV().findVByLabelQ(DB_VERTEX.company)
    )
    if (v == null) return None
    Some(new Company(driver, v))
  }
}


class Project(driver: DSEDriver, v: Vertex) extends GroupEntity(driver, v)  {
  val D: DB_Project = Model.coerce(v, this).asInstanceOf[DB_Project]

  def --> (workflow: Workflow)             : Unit = { addMember(V, workflow.V   ) }
  def --> (function: Function)             : Unit = { addMember(V, function.V   ) }
  def --> (dataset: Dataset)               : Unit = { addMember(V, dataset.V    ) }
  def --> (datasource: ExternalDataSource) : Unit = { addMember(V, datasource.V ) }

}

