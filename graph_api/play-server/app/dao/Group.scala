package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object Group {

  // ADDERS

  def + (driver: DSEDriver, name: String, description: String) : Group = {
    new Group(driver, driver.addVertex(addVQ(DB_VERTEX.group, List(
      (DB_PROPERTY.name, name), (DB_PROPERTY.description, description),
      (DB_PROPERTY.created, Util.tt)
    ))))
//    new Group(Graph.get + DB_Group(None, name, description, Util.tt))
  }

  // GETTERS - ALL

  def getAll(driver: DSEDriver) : List[Group] = {
    driver.getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.group)).map(x => new Group(driver, x))
//    Graph.get.V.hasLabel(DB_LABELS.group).toList.map(x => new Group(x))
  }

  // GETTERS - FILTERED

  def getByName(driver: DSEDriver, name: String) : Group = {
    new Group(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.group, DB_PROPERTY.name, name)))
  }

  def get(driver: DSEDriver, id: String) : Option[Group] = {
    val v = driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.group, DB_PROPERTY.id, id))
    if (v == null) return None
    Some(new Group(driver, v))
  }

  def getCompany(driver: DSEDriver, id: String): Option[Company] = {
    val v = driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.group, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Member)).outV().findVByLabelQ(DB_VERTEX.company)
    )
    if (v == null) return None
    Some(new Company(driver, v))
  }

  def getDatasetsWithProjectPermissions(driver: DSEDriver, id: String): Option[List[(Dataset, Int, String)]] = {
    val projectIdsWithPerms =
      driver.getValuesAsMap(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.group, DB_PROPERTY.id, id)
          .outE(Some(DB_EDGE.Permission)).inV().as("perm", DB_PROPERTY.permission)
          .outE(Some(DB_EDGE.Permission)).inV().as("proj-vertex", DB_PROPERTY.id)
      )

    val datasets = scala.collection.mutable.ListBuffer.empty[(Dataset, Int, String)]

    if (projectIdsWithPerms.isDefined) {
      for (pp <- projectIdsWithPerms.get) {
        val proj = new Project(driver, pp("proj-vertex").asInstanceOf[Vertex])
        val perm = pp.get("perm").toString.toInt

        val ds = proj.members().filter(t => t._1 == DB_VERTEX.dataset).flatMap(t => List(t._2))
        ds.foreach { d => datasets += ((d.asInstanceOf[Dataset], perm, proj.D.id.get)) }
      }
    }

    Some(datasets.toList)
  }

  def getProjects(driver: DSEDriver, id: String): Option[List[(Project, Int)]] = {
    val projects =
      driver.getValuesAsMap(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.group, DB_PROPERTY.id, id)
          .outE(Some(DB_EDGE.Permission)).inV().as("perm", DB_PROPERTY.permission)
          .outE(Some(DB_EDGE.Permission)).inV()
          .findVByLabelQ(DB_VERTEX.project).as("proj-vertex", DB_PROPERTY.id)
      )
    val projectList = scala.collection.mutable.ListBuffer.empty[(Project, Int)]
    if (projects.isDefined) {
      for (pp <- projects.get) {
        val proj = new Project(driver, pp("proj-vertex").asInstanceOf[Vertex])
        val perm = pp.get("perm").toString.toInt
        projectList += ((proj, perm))
      }
    }

    Some(projectList.toList)
  }

  // Workflow <-- Role <-- Group
  def getWorkflows(driver: DSEDriver, id: String): Option[List[(Workflow, Int)]] = {
    val wfs =
      driver.getValuesAsMap(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.group, DB_PROPERTY.id, id)
          .outE(Some(DB_EDGE.Permission)).inV().as("perm", DB_PROPERTY.permission)
          .outE(Some(DB_EDGE.Permission)).inV()
          .findVByLabelQ(DB_VERTEX.workflow).as("wf-vertex", DB_PROPERTY.id)
      )
    val wfList = scala.collection.mutable.ListBuffer.empty[(Workflow, Int)]
    if (wfs.isDefined) {
      for (pp <- wfs.get) {
        val wf = new Workflow(driver, pp("wf-vertex").asInstanceOf[Vertex])
        val perm = pp.get("perm").toString.toInt
        wfList += ((wf, perm))
      }
    }

    Some(wfList.toList)
  }
}

class Group(driver: DSEDriver, v: Vertex) extends GroupEntity(driver, v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_Group]

  def --> (person: Person)  : Unit = { addMember(V, person.V ) }
}