package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object Person {

  // ADDERS

  def + (driver: DSEDriver, company: Company, name: String, email: String) : Person = {
    val v = driver.addVertex(addVQ(
      DB_VERTEX.person, List(
        (DB_PROPERTY.name, name), (DB_PROPERTY.email, email), (DB_PROPERTY.created, Util.tt)
      )
    ))
    val p = new Person(driver, v)
    // NOTE: java.lang.IllegalStateException: Edge additions not supported
    //    company.V.addEdge(DB_EDGE.Member, v) ==> won't work !!

//    val p = new Person(Graph.get + DB_Person(None, name, email, Util.tt))
    company --> p
    p
  }

  // GETTERS - ALL

  def getAll(driver: DSEDriver) : List[Person] = {
    driver.getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.person)
    ).map(x => new Person(driver, x))
//    Graph.get.V.hasLabel[DB_Person].toList.map(x => new Person(x))
  }

  // GETTERS - FILTERED

  def get(driver: DSEDriver, companyName: String, name: String) : Person = {
    val company = Company.getByName(driver, companyName)
    // TODO change to lookup by id
    val persons = driver.getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.company, DB_PROPERTY.name, company.D.name) //company
        .outE(Some(DB_EDGE.Member)).inV()
          .findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.name, name)
    )
//    val persons = company.V.out(DB_EDGE.Member).
//                            hasLabel(DB_LABELS.person).
//                            has(DB_KEY.name, name).toList()
    if (persons.length < 1)
      return null

    new Person(driver, persons.head)
  }

  def get(driver: DSEDriver, id: String) : Person = {
    val v = driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.id, id)
    )
    new Person(driver, v)
  }

  def getCompany(driver: DSEDriver, id: String, permission: Int): Company = {
    new Company(driver,
      driver.getVertex(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.id, id)
          .outE(Some(DB_EDGE.Permission)).inV().outE(Some(DB_EDGE.Permission)).inV()
          .findVByLabelQ(DB_VERTEX.company)
      )
    )
  }

  //g.V().has('person', 'name', 'Bob Smith').inE('member').outV().hasLabel('group').outE('permission').inV().outE('permission').inV().values()
  def getProjects(driver: DSEDriver, id: String): Option[List[(Project, Int)]] = {
    val projects =
      driver.getValuesAsMap(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.id, id)
          .inE(Some(DB_EDGE.Member)).outV().findVByLabelQ(DB_VERTEX.group)
          .outE(Some(DB_EDGE.Permission)).inV().as("perm", DB_PROPERTY.permission)
          .outE(Some(DB_EDGE.Permission)).inV()
          .findVByLabelQ(DB_VERTEX.project).as("proj-vertex", DB_PROPERTY.id)
      )
    val projectList = scala.collection.mutable.ListBuffer.empty[(Project, Int)]
    if (projects.isDefined) {
      for (pp <- projects.get) {
        val proj = new Project(driver, pp("proj-vertex").asInstanceOf[Vertex])
        val perm = pp.get("perm").toString.toInt
        projectList += ((proj, perm))
      }
    }

    Some(projectList.toList)
  }

  def getDatasetsWithProjectPermissions(driver: DSEDriver, id: String): Option[List[(Dataset, Int, String)]] = {
    val projectIdsWithPerms =
      driver.getValuesAsMap(
        VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.id, id)
          .inE(Some(DB_EDGE.Member)).outV().findVByLabelQ(DB_VERTEX.group)
          .outE(Some(DB_EDGE.Permission)).inV().as("perm", DB_PROPERTY.permission)
          .outE(Some(DB_EDGE.Permission)).inV().as("proj-vertex", DB_PROPERTY.id)
      )

    val datasets = scala.collection.mutable.ListBuffer.empty[(Dataset, Int, String)]

    if (projectIdsWithPerms.isDefined) {
      for (pp <- projectIdsWithPerms.get) {
        val proj = new Project(driver, pp("proj-vertex").asInstanceOf[Vertex])
        val perm = pp.get("perm").toString.toInt

        val ds = proj.members().filter(t => t._1 == DB_VERTEX.dataset).flatMap(t => List(t._2))
        ds.foreach { d => datasets += ((d.asInstanceOf[Dataset], perm, proj.D.id.get)) }
      }
    }

    Some(datasets.toList)
  }


  def getGroups(driver: DSEDriver, id: String): Option[List[Group]] = {
    val groups = driver.getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.id, id)
        .inE(Some(DB_EDGE.Member)).outV()
        .findVByLabelQ(DB_VERTEX.group)
    ).map(gV => new Group(driver, gV))

    Some(groups)
  }
}

class Person(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_Person]

  // member accessors
  def id      : String = D.id.get
  def name    : String = v.property(DB_PROPERTY.name).value()
  def email   : String = v.property(DB_PROPERTY.email).value()
  def created : Long   = v.property(DB_PROPERTY.created).value()

  def getWorkflows : Option[List[(Int, String, SimpleEntity)]] = {

    val indirectRoleNodes = driver.getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.id, id)
        .findVByEQ((inE.name, Some(DB_EDGE.Member))).outV()
        .findVByLabelQ(DB_VERTEX.group)
        .outE(Some(DB_EDGE.Permission)).inV()
        .findVByLabelQ(DB_VERTEX.role)
    )

//    val indirectRoleNodes = V.
//                              in(DB_EDGE.Member).
//                              hasLabel(DB_LABELS.group).
//                              out(DB_EDGE.Permission).
//                              hasLabel(DB_LABELS.role).toList()


    val directRoleNodes = driver.getVertices(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.person, DB_PROPERTY.id, id)
        .findVByEQ((outE.name, Some(DB_EDGE.Permission))).inV()
        .findVByLabelQ(DB_VERTEX.role)
    )

//    val directRoleNodes   = V.
//                              out(DB_EDGE.Permission).
//                              hasLabel(DB_LABELS.role).toList()

    val allRoleNodes = (directRoleNodes ::: indirectRoleNodes).
        map(x => SimpleEntity.get(driver, x).asRole)

    Some(allRoleNodes.flatMap(
      role => role.getItems(DB_VERTEX.workflow :: Nil)))
  }
}