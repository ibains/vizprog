package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.{Direction, Vertex}

object Function {

  // ADDERS

  def + (driver: DSEDriver, project: Project, name: String, category: String, description: String, content: String) : Function = {
    val f = new Function(driver,
      driver.addVertex(addVQ(DB_VERTEX.function, List(
        (DB_PROPERTY.name, name), (DB_PROPERTY.category, category), (DB_PROPERTY.description, description), (DB_PROPERTY.content, content), (DB_PROPERTY.created, Util.tt)
      )))
    )
    project --> f
    f
  }

  def get(driver: DSEDriver, id: String): Function = {
    new Function(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.function, DB_PROPERTY.id, id)
    ))
  }

  def getAll(driver: DSEDriver) : List[Function] = {
    driver.getVertices(
      VQBuilder(emptyQ).findVByLabelQ(DB_VERTEX.function)
    ).map(x => new Function(driver, x))
  }
}

class Function(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v) {
  val D: DB_Function = Model.coerce(v, this).asInstanceOf[DB_Function]

  def getProject: Project = {
    new Project(driver, driver.getVertex(
      VQBuilder(emptyQ).findVByLabelPropQ(DB_VERTEX.function, DB_PROPERTY.id, D.id.get)
        .inE(Some(DB_EDGE.Member)).outV()
    ))
  }
}
