package dao

import db.DSEDriver
import db.QueryBuilder._
import org.apache.tinkerpop.gremlin.structure.Vertex

object addExternalDataSource {

  // ADDERS

  def + (driver: DSEDriver, location: String, retrievalJsonNode: String) : ExternalDataSource = {
    new ExternalDataSource(driver, driver.addVertex(addVQ(DB_VERTEX.externaldatasource, List(
      (DB_PROPERTY.location, location), (DB_PROPERTY.retrievalJsonNode, retrievalJsonNode),
      (DB_PROPERTY.created, Util.tt)
    ))))
//    new ExternalDataSource(Graph.get + DB_ExternalDataSource(None, location, retrievalJsonNode, Util.tt))
  }

}

class ExternalDataSource(driver: DSEDriver, v: Vertex) extends SimpleEntity(driver, v) {
  val D = Model.coerce(v, this).asInstanceOf[DB_ExternalDataSource]
}

