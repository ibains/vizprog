package controllers

import com.datastax.driver.dse.DseCluster
import com.google.inject.Inject
import db.DSEDriver
import play.api.Configuration

class Bootstrap @Inject() (config: Configuration) {
  val contactPoint = config.getString("dse.host").getOrElse("graphdb.vizprog.com")
  val port = config.getInt("dse.port").getOrElse(9042)
  val graphName = config.getString("dse.defaultGraphName").getOrElse("dev")

  import Bootstrap.init
  var cluster: Option[DseCluster] = None
  try {
    cluster = init(contactPoint, port, graphName)
  } catch {
    case e: ControllerException => {
      println("Exception on init => " + e.msg)
      cluster = None
    }
  }

  def getCluster: Option[DseCluster] = cluster
}

case class ControllerException(msg: String, t: Throwable) extends Exception

object Bootstrap {
  def init(contactPoint: String, port: Int, graphName: String): Option[DseCluster] = {
    try {
      var cluster = DSEDriver.connect(contactPoint, port, None)

      // Disconnected after use
      DSEDriver.createGraph(cluster, graphName)
      // Re-initialize cluster
      cluster = DSEDriver.connect(contactPoint, port, Some(graphName))

      // Initialize graph schema in Graph database
      DSEDriver.initialize(cluster)
      DSEDriver.addDummyData(cluster)
      Some(cluster)
    } catch {
      case e: Exception => throw ControllerException("Failed provisioning dse cluster, " + e.getMessage, e)
    }
  }
}

//class OnStartupModule extends AbstractModule {
//  override def configure(): Unit = bind(classOf[Bootstrap]).asEagerSingleton()
//}


