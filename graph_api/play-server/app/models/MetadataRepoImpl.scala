package models

import com.datastax.driver.dse.DseCluster
import dao._
import db._

// TODO: Implement a Session manager - connection pooling.
// TODO: Null safe api for all operations
class MetadataRepoImpl(cluster: DseCluster) extends MetadataRepo {
  val driver = DSEDriver(cluster)

  protected def makeCompany(c: dao.Company, p: models.Permission.Value) : models.Company = {
    models.Company(c.D.id.toString, c.D.name, Some(c.D.description), p)
  }
  protected def getUserName(name: String): (Option[String], Option[String], Option[String]) = {
    if (name.nonEmpty) {
      val nameParts = name.split(" ")
      if (nameParts.nonEmpty) {
        val fName = Some(nameParts(0))
        var lName: Option[String] = None
        var fullName: Option[String] = None
        if (nameParts.length > 1) {
          lName = Some(nameParts(nameParts.length - 1))
          fullName = Some(name)
        }
        return (fName, lName, fullName)
      }
    }
    (None, None, None)
  }
  protected def makeUser(u: dao.Person, p: models.Permission.Value) : models.User = {
    val (first, last, full) = getUserName(u.name)
    models.User(u.id, u.email, first, last, full, None, p)
  }
  protected def makeTeam(g: dao.Group, p: models.Permission.Value): models.Team = {
    models.Team(g.D.id.get, g.D.name, Some(g.D.description), p)
  }
  protected def makeProject(p: dao.Project, perms: models.Permission.Value) : models.Project = {
    models.Project(p.D.id.get, p.D.name, Some(p.D.description), perms)
  }
  protected def makeWorkflow(wf: dao.Workflow, perms: models.Permission.Value) : models.Workflow = {
    models.Workflow(wf.D.id.get, wf.D.name, Some(wf.D.description.get),
      perms, wf.D.content, wf.D.created, wf.D.created, wf.getProject.D.id.get)
  }
  protected def makeDataset(d: dao.Dataset, perms: models.Permission.Value, pid: String) : models.Dataset = {
    models.Dataset(d.D.id.get, d.D.name, d.D.description,
      perms, d.D.location, d.D.format, Some(d.D.schemaJson),
      Some(d.D.statsJson), d.D.created, d.D.created, pid)
  }
  protected def makeDataset(d: dao.Dataset, perms: models.Permission.Value) : models.Dataset = {
    models.Dataset(d.D.id.get, d.D.name, d.D.description,
      perms, d.D.location, d.D.format, Some(d.D.schemaJson),
      Some(d.D.statsJson), d.D.created, d.D.created, d.getProject.D.id.get)
  }
  protected def makeFunction(f: dao.Function, perms: models.Permission.Value) : models.Function = {
    models.Function(f.D.id.get, f.D.name, f.D.category, Some(f.D.description.get),
      perms, f.D.content, f.D.created, f.D.created, f.getProject.D.id.get)
  }
  protected def coercePermission(p: Int): Permission.Value = {
    p match {
      case Permissions.admin => Permission.ADMIN
      case Permissions.read  => Permission.READ
      case Permissions.write => Permission.WRITE
      case _                 => Permission.NONE
    }
  }

  ///////////

  override def getCompany(companyId: String): Option[Company] = {
    Some(makeCompany(dao.Company.get(driver, companyId), Permission.NONE))
  }

  // These functions are from the User POV
  override def getUser(userId: String): Option[User] = {
    Some(makeUser(dao.Person.get(driver, userId), Permission.NONE))
  }

  // TODO: Shouldn't permission be a param ?
  override def getUserCompany(userId: String): Option[Company] = {
    val c = dao.Person.getCompany(driver, userId, Permissions.admin)
    Some(makeCompany(c, Permission.ADMIN))
  }

  // TODO: Use select to return permission - project map
  // Company - admins have UI implications
  override def getUserProjects(userId: String): Option[List[Project]] = {
    val projects = dao.Person.getProjects(driver, userId)
    if (projects.isEmpty) return None
    Some(projects.get.flatMap { case (proj, p) => List(makeProject(proj, coercePermission(p))) })
  }

  // TODO: lastModified field support
  // Get project details and permissions
  override def getUserWorkflows(userId: String): Option[List[Workflow]] = {
    val u = dao.Person.get(driver, userId)
    val wfl = u.getWorkflows // (permission, label, simpleEntity)
    if (wfl.isEmpty) return None
    val retVal = wfl.get.map {
      case (p: Int, l: String, e: SimpleEntity) =>
        val wf = e.asInstanceOf[dao.Workflow]
        makeWorkflow(wf, coercePermission(p))
    }
    Some(retVal)
  }

  // Here permissions vs project presence/permissions shows override
  override def getUserDatasets(userId: String): Option[List[Dataset]] = {
    val datasetsWithPerms = dao.Person.getDatasetsWithProjectPermissions(driver, userId)
    if (datasetsWithPerms.isEmpty) return None

    val datasets = for (t <- datasetsWithPerms.get; (d, pd, pid) = t)
      yield makeDataset(d, coercePermission(pd), pid)

    Some(datasets)
  }

  // These functions are from the Project POV
  override def getProject(projectId: String): Option[Project] = {
    Some(makeProject(dao.Project.get(driver, projectId), Permission.NONE))
  }

  // Get users, with their permissions over this
  override def getProjectUsers(projectId: String): Option[List[User]] = {
    val persons = dao.Project.getUsers(driver, projectId)
    if (persons.isEmpty) return None
    val users = persons.get.map {
      case (u, p) => makeUser(u, coercePermission(p))
    }
    Some(users)
  }

  override def getProjectWorkflows(projectId: String): Option[List[Workflow]] = {
    val wfs = dao.Project.getWorkflows(driver, projectId)
    val mdwfs = wfs.map(wfl => makeWorkflow(wfl, Permission.NONE))
    Some(mdwfs)
  }

  // Get members with this' permission over them
  // Scala 2.8 doesn't allow case class extension and thereby could not
  // create named tuple
  override def getProjectDatasets(projectId: String): Option[List[Dataset]] = {
    val project = dao.Project.get(driver, projectId)
    if (project == null) return None
    val datasets = project.members().filter{ case (label, d) => label == DB_VERTEX.dataset }
      .flatMap { case (label, d) => List(makeDataset(d.asInstanceOf[dao.Dataset], Permission.NONE)) }
    Some(datasets)
  }

  override def getProjectFunctions(projectId: String): Option[List[Function]] = {
    val functions = dao.Project.getFunctions(driver, projectId)
    val mdFunctions = functions.map(f => makeFunction(f, Permission.NONE))
    Some(mdFunctions)
  }

  // These functions are from the Workflow POV
  override def getWorkflow(workflowId: String): Option[Workflow] = {
    Some(makeWorkflow(dao.Workflow.get(driver, workflowId), Permission.NONE))
  }

  //
  override def getWorkflowProject(workflowId: String): Option[Project] = {
    val wf = dao.Workflow.get(driver, workflowId)
    Some(makeProject(wf.getProject, Permission.NONE))
  }

  // Project users + direct permissions users
  override def getWorkflowUsers(workflowId: String): Option[List[User]] = {
    val wf = dao.Workflow.get(driver, workflowId)
    val projectUsers = getProjectUsers(wf.getProject.D.id.get).get
    val directUsers = dao.Workflow.getDirectUsers(driver, workflowId)
    var users: List[User] = Nil
    if (directUsers.isEmpty) return None
    users = directUsers.get.map {
      case (u, p) => makeUser(u, coercePermission(p))
    }
    val allUsers = projectUsers ::: users
    Some(allUsers.distinct)
  }

  // Project users + direct permissions users - TODO
  override def getWorkflowDatasets(workflowId: String): Option[List[Dataset]] = {
    val wf = dao.Workflow.get(driver, workflowId)
    val project = wf.getProject
    if (project != null) {
      getProjectDatasets(project.D.id.get)
    } else {
      None
    }
  }

  // These functions are from the Workflow POV
  override def getDataset(datasetId: String): Option[Dataset] = {
    val d = dao.Dataset.get(driver, datasetId)
    Some(makeDataset(d, Permission.NONE, d.getProject.D.id.get))
  }

  //
  override def getDatasetProject(datasetId: String): Option[Project] = {
    val p = dao.Dataset.get(driver, datasetId).getProject
    Some(makeProject(p, Permission.NONE))
  }

  // Project users + direct permissions users
  override def getDatasetUsers(datasetId: String): Option[List[User]] = {
    val ds = dao.Dataset.get(driver, datasetId)
    if (ds == null) return None
    val projUsers = getProjectUsers(ds.getProject.D.id.get)
    val directUsers = dao.Dataset.getDirectUsers(driver, datasetId)
    if (directUsers.isDefined) {
      val dsUsers = directUsers.get.map{ case (u, p) => makeUser(u, coercePermission(p)) }
      val allUsers = projUsers.get ::: dsUsers
      Some(allUsers.distinct)
    } else {
      projUsers
    }
  }

  // Permissions - read / write express usage relations rather than permissions
  override def getDatasetWorkflows(datasetId: String): Option[List[Workflow]] = {
    val ds = dao.Dataset.get(driver, datasetId)
    if (ds == null) return None

    val projectWfs = getProjectWorkflows(ds.getProject.D.id.get)
    val directWfl = dao.Dataset.getDirectWorkflows(driver, datasetId)
    var directWfs: List[Workflow] = Nil
    if (directWfl.isDefined) {
      directWfs = directWfl.get.map {
        case (wf, p) => makeWorkflow(wf, coercePermission(p))
      }
    }
    val allWfs = projectWfs.get ::: directWfs
    Some(allWfs.distinct)
  }

  override def getCompanyProjects(companyId: String): Option[List[models.Project]] = {
    val c = dao.Company.get(driver, companyId)
    if (c == null) return None
    val projects = c.members().filter { case (label, p) => label == DB_VERTEX.project }
      .flatMap { case (label, p) => List(makeProject(p.asInstanceOf[dao.Project], models.Permission.NONE)) }
    Some(projects)
  }

  override def getCompanyTeams(companyId: String): Option[List[models.Team]] = {
    val c = dao.Company.get(driver, companyId)
    if (c != null) {
      val teams = c.members().filter { case (label, g) => label == DB_VERTEX.group }
        .flatMap { case (label, g) => List(makeTeam(g.asInstanceOf[dao.Group], models.Permission.NONE)) }
      Some(teams)
    } else {
      None
    }
  }

  override def getCompanyUsers(companyId: String): Option[List[models.User]] = {
    val c = dao.Company.get(driver, companyId)
    if (c != null) {
      val users = c.members().filter { case (label, u) => label == DB_VERTEX.group }
        .flatMap { case (label, u) => List(makeUser(u.asInstanceOf[dao.Person], models.Permission.NONE)) }
      Some(users)
    } else {
      None
    }
  }

  override def getDatasetTeams(datasetId: String): Option[List[models.Team]] = {
    val ds = dao.Dataset.get(driver, datasetId)
    if (ds == null) return None

    val projectGroups = getProjectTeams(ds.getProject.D.id.get)
    val directGroups = dao.Dataset.getDirectGroups(driver, datasetId)
    var dsGroups: List[Team] = Nil
    if (directGroups.isDefined) {
      dsGroups = directGroups.get.map {
        case (g, p) => makeTeam(g, coercePermission(p))
      }
    }
    val allGroups = projectGroups.get ::: dsGroups
    Some(allGroups.distinct)
  }

  override def getProjectCompany(projectId: String): Option[models.Company] = {
    val c = dao.Project.getCompany(driver, projectId)
    if (c.isEmpty) return None

    Some(makeCompany(c.get, Permission.NONE))
  }

  override def getProjectTeams(projectId: String): Option[List[models.Team]] = {
    val groups = dao.Project.getGroups(driver, projectId)
    if (groups.isEmpty) return None

    Some(groups.get.flatMap{ case (g, p) => List(makeTeam(g, coercePermission(p))) })
  }

  override def getTeam(teamId: String): Option[models.Team] = {
    val group = dao.Group.get(driver, teamId)
    if (group.isEmpty) return None
    Some(makeTeam(group.get, Permission.NONE))
  }

  override def getTeamCompany(teamId: String): Option[models.Company] = {
    val c = dao.Group.getCompany(driver, teamId)
    if (c.isEmpty) return None
    Some(makeCompany(c.get, Permission.NONE))
  }

  override def getTeamDatasets(teamId: String): Option[List[models.Dataset]] = {
    val ds = dao.Group.getDatasetsWithProjectPermissions(driver, teamId)
    if (ds.isEmpty) return None
    Some(ds.get.flatMap { case (d, p, pid) => List(makeDataset(d, coercePermission(p), pid)) })
  }

  override def getTeamProjects(teamId: String): Option[List[models.Project]] = {
    val projects = dao.Group.getProjects(driver, teamId)
    if (projects.isEmpty) return None
    Some(projects.get.flatMap { case (proj, p) => List(makeProject(proj, coercePermission(p))) })
  }

  override def getTeamUsers(teamId: String): Option[List[models.User]] = {
    val g = dao.Group.get(driver, teamId)
    if (g == null) return None
    Some(g.get.members().filter{ case (label, p) => label == DB_VERTEX.person }
      .flatMap { case (l, u) =>
          List(makeUser(u.asInstanceOf[Person], Permission.NONE))
      })
  }

  override def getTeamWorkflows(teamId: String): Option[List[models.Workflow]] = {
    val wfs = dao.Group.getWorkflows(driver, teamId)
    if (wfs.isEmpty) return None
    Some(wfs.get.flatMap { case (wf, p) => List(makeWorkflow(wf, coercePermission(p))) })
  }

  override def getUserTeams(userId: String): Option[List[models.Team]] = {
    val groups = dao.Person.getGroups(driver, userId)
    if (groups == null) return None
    Some(groups.get.map(g => makeTeam(g, Permission.NONE)))
  }

  // TODO: Do project users get default workflow write perms ???
  override def getWorkflowTeams(workflowId: String): Option[List[models.Team]] = {
    val groups = dao.Workflow.getDirectGroups(driver, workflowId)
    if (groups == null) return None
    Some(groups.get.map { case (g, p) => makeTeam(g, coercePermission(p)) })
  }

  override def getFunction(functionId: String): Option[Function] = {
    Some(makeFunction(dao.Function.get(driver, functionId), Permission.NONE))
  }

  override def getFunctionProject(functionId: String): Option[Project] = {
    val p = dao.Function.get(driver, functionId).getProject
    Some(makeProject(p, Permission.NONE))
  }

  override def createFunction(function: Function): Function = {
    val project = dao.Project.get(driver, function.projectId)
    makeFunction(dao.Function.+(driver, project, function.name, function.category, function.description.get, function.jsonContent), Permission.NONE)
  }

}
