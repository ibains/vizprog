package models

import models.DummyDatabase._
import play.api.Logger

/**
  * This uses the data and structures in dummy database to fulfil customer
  *  queries
  */

class DummyMetadataRepo extends MetadataRepo {


  // HELPERS ===========================================================================================================

  protected def makeFunction(f: DDBFunction, perms: Permission.Value, projId: String) : Function =
    Function(f.id, f.name, f.category, f.description, perms, f.jsonContent, f.created, f.lastModified, projId)
  protected def makeDataset(d: DDBDataset, perms: Permission.Value, projId: String) : Dataset =
    Dataset(d.id, d.name, d.description, perms, d.location, d.format, d.schemaJson, d.statsJson, d.created, d.lastModified, projId)
  protected def makeWorkflow(w: DDBWorkflow, perms: Permission.Value, projId: String) : Workflow =
    Workflow(w.id, w.name, w.description, perms, w.jsonContent, w.created, w.lastModified, projId)
  protected def makeUser(u: DDBUser, perms: Permission.Value) : User =
    User(u.id, u.email, u.firstName, u.lastName, u.fullName, u.avatarURL, perms)
  protected def makeProject(p: DDBProject, perms: Permission.Value) : Project =
    Project(p.id, p.name, p.description, perms)
  protected def makeTeam(t: DDBTeam, perms: Permission.Value) : Team =
    Team(t.id, t.name, t.description, perms)
  protected def makeCompany(c: DDBCompany, perms: Permission.Value) : Company =
    Company(c.id, c.name, c.description, perms)



  // COMPANY ===========================================================================================================


  def getCompany : Option[List[Company]] = {
    if (companies.isEmpty) return None
    Some(companies.keys.map(x => getCompany(x).get).toList)
  }
  def getCompany(companyId: String) : Option[Company] = {
    if (! companies.contains(companyId)) return None
    Some(makeCompany(companies(companyId), Permission.NONE))
  }
  def getCompanyUsers(companyId: String) : Option[List[User]] = {
    if (! companies.contains(companyId)) return None
    val x = for ((userId, perm) <- companyUser(companyId);
                 user = users(userId)
    ) yield makeUser(user, perm)
    Some(x.toList)
  }
  def getCompanyProjects(companyId: String) : Option[List[Project]] = {
    if (! companies.contains(companyId)) return None
    val x = for (projId <- companyProject(companyId);
                 proj = projects(projId)
    ) yield makeProject(proj, Permission.ADMIN)
    Some(x.toList)
  }
  def getCompanyTeams(companyId: String) : Option[List[Team]] = {
    if (! companies.contains(companyId)) return None
    val x = for (teamId <- companyTeam(companyId);
                 team = teams(teamId)
    ) yield makeTeam(team, Permission.ADMIN)
    Some(x.toList)
  }


  // TEAM ==============================================================================================================


  def getTeam(teamId: String) : Option[Team] = {
    if (! teams.contains(teamId)) return None
    Some(makeTeam(teams(teamId), Permission.NONE))
  }
  def getTeamCompany(teamId: String) : Option[Company] = {
    if (! teams.contains(teamId)) return None
    for ((cId, teamSet) <- companyTeam; tId <- teamSet)
         if (tId == teamId)
           return Some(makeCompany(companies(cId), Permission.NONE))
    None
  }
  def getTeamProjects(teamId: String) : Option[List[Project]] = {
    if (! teams.contains(teamId)) return None
    val projs = for ((projId, perms) <- teamProject(teamId)) yield makeProject(projects(projId), perms)
    Some(projs.toList)
  }
  def getTeamUsers(teamId: String) : Option[List[User]] = {
    if (!teamUser.contains(teamId)) return None
    Some(teamUser(teamId).map(x => makeUser(users(x), Permission.NONE)).toList)
  }
  def getTeamWorkflows(teamId: String) : Option[List[Workflow]] = {
    if (! teamProject.contains(teamId)) return None
    val x = for (
      (projId, perms) <- teamProject(teamId);
      wId <- projectWorkflow(projId);
      w =  workflows(wId)
    ) yield makeWorkflow(w, perms, projId)
    Some(makeUniqueWorkflows(x.toList))
  }
  def getTeamDatasets(teamId: String) : Option[List[Dataset]] = {
    if (! teamProject.contains(teamId)) return None
    val x = for (
      (projId, perms) <- teamProject(teamId);
      dId <- projectDataset(projId);
      d =  datasets(dId)
    ) yield makeDataset(d, perms, projId)
    Some(makeUniqueDatasets(x.toList))
  }


  // PROJECT ===========================================================================================================


  def getProject(projectId: String) : Option[Project] = {
    if (! projects.contains(projectId)) return None
    Some(makeProject(projects(projectId), Permission.NONE))
  }
  def getProjectCompany(projectId: String) : Option[Company] = {
    if (! projects.contains(projectId)) return None
    for ((c, projSet) <- companyProject; pId <- projSet)
      if (pId == projectId)
        return Some(makeCompany(companies(c), Permission.NONE))
    None
  }
  def getProjectTeams(projectId: String) : Option[List[Team]] = {
    if (! projects.contains(projectId)) return None
    val x = for (
      (teamId, projs) <- teamProject;
      (projId, perms) <- projs
      if projectId == projId
    ) yield makeTeam(teams(teamId), perms)
    Some(x.toList)
  }
  def getProjectUsers(projectId: String) : Option[List[User]] = {
    if (! projects.contains(projectId)) return None
    val ts = getProjectTeams(projectId)
    if (ts.isEmpty) return None
    val allUserLists = ts.get.flatMap(x => getTeamUsers(x.id))
    Some(allUserLists.flatten)
  }
  def getProjectWorkflows(projectId: String) : Option[List[Workflow]] = {
    if (! projects.contains(projectId)) return None
    val x = for (
      wId <- projectWorkflow(projectId);
      w = workflows(wId)
    ) yield makeWorkflow(w, Permission.ADMIN, projectId)
    Some(makeUniqueWorkflows(x.toList))
  }
  def getProjectDatasets(projectId: String) : Option[List[Dataset]] = {
    if (! projects.contains(projectId)) return None
    val x = for (
      wId <- projectDataset(projectId);
      d = datasets(wId)
    ) yield makeDataset(d, Permission.ADMIN, projectId)
    Some(makeUniqueDatasets(x.toList))
  }
  def getProjectFunctions(projectId: String) : Option[List[Function]] = {
    if (! projects.contains(projectId)) return None
    if (! projectFunctions.contains(projectId)) return Some(List())

    val x = for (
      fId <- projectFunctions(projectId);
      f = functions(fId)
    ) yield makeFunction(f, Permission.ADMIN, projectId)
    Some(makeUniqueFunctions(x.toList))
  }


  // USER ==============================================================================================================


  protected def getUserTeamIds(userId: String): List[String] = {
    (for ((teamId, users) <- teamUser if users.contains(userId)) yield teamId).toList
  }
  def getUser(userId: String) : Option[User] = {
    if (! users.contains(userId)) return None
    Some(makeUser(users(userId), Permission.NONE))
  }
  def getUserCompany(userId: String) : Option[Company] = {
    if (! users.contains(userId)) return None
    for ((com, allUserPermissions) <- companyUser)
      for ((u, p) <- allUserPermissions)
        if (u == userId)
          return Some(makeCompany(companies(com), p))
    None
  }
  def getUserTeams(userId: String) : Option[List[Team]] = {
    if (! users.contains(userId)) return None
    Some(getUserTeamIds(userId).map(x => makeTeam(teams(x), Permission.NONE)).toList)
  }
  def getUserProjects(userId: String) : Option[List[Project]] = {
    if (! users.contains(userId)) return None
    val ts = getUserTeamIds(userId).flatMap(x => getTeamProjects(x))
    Some(ts.flatten)
  }
  def getUserWorkflows(userId: String) : Option[List[Workflow]] = {
    if (! users.contains(userId)) return None
    getUserProjects(userId) match {
      case None => None
      case Some(projList) => Some(makeUniqueWorkflows(projList.flatMap(x => getProjectWorkflows(x.id)).flatten))
    }
  }
  def getUserDatasets(userId: String) : Option[List[Dataset]] = {
    if (! users.contains(userId)) return None
    getUserProjects(userId) match {
      case None => None
      case Some(projList) => Some(makeUniqueDatasets(projList.flatMap(x => getProjectDatasets(x.id)).flatten))
    }
  }

  protected def makeUniqueDatasets(input: List[Dataset]) : List[Dataset] = {
    val uniques = scala.collection.mutable.Map[String, Dataset]()
    for (oneInput <- input)
      uniques(oneInput.id) = oneInput
    uniques.values.toList
  }

  protected def makeUniqueWorkflows(input: List[Workflow]) : List[Workflow] = {
    val uniques = scala.collection.mutable.Map[String, Workflow]()
    for (oneInput <- input)
      uniques(oneInput.id) = oneInput
    uniques.values.toList
  }

  protected def makeUniqueFunctions(input: List[Function]) : List[Function] = {
    val uniques = scala.collection.mutable.Map[String, Function]()
    for (oneInput <- input)
      uniques(oneInput.id) = oneInput
    uniques.values.toList
  }

  // WORKFLOW ==========================================================================================================


  protected def getWorkflowProjectId(workflowId: String) : String = {
    if (workflows.contains(workflowId))
      for ((pId, wfIds) <- projectWorkflow)
        if (wfIds.contains(workflowId))
          return pId
    null
  }
  protected def getDatasetProjectId(datasetId: String) : String = {
    if (datasets.contains(datasetId))
      for ((pId, wfIds) <- projectDataset)
        if (wfIds.contains(datasetId))
          return pId
    null
  }
  protected def getFunctionProjectId(functionId: String) : String = {
    if (functions.contains(functionId))
      for ((pId, fIds) <- projectFunctions)
        if (fIds.contains(functionId))
          return pId
    null
  }
  def getWorkflow(workflowId: String) : Option[Workflow] = {
    if (! workflows.contains(workflowId)) return None
    val w : DDBWorkflow = workflows(workflowId)
    val projectId = getWorkflowProjectId(workflowId)
    if (projectId != null)
      return Some(makeWorkflow(w, Permission.NONE, projectId))
    None
  }
  def getWorkflowProject(workflowId: String) : Option[Project] = {
    if (! workflows.contains(workflowId)) return None
    val projectId = getWorkflowProjectId(workflowId)
    if (projectId != null) {
      val p : DDBProject = projects(projectId)
      return Some(Project(p.id, p.name, p.description, Permission.ADMIN))
    }
    None
  }
  def getWorkflowTeams(workflowId: String) : Option[List[Team]] = {
    if (! workflows.contains(workflowId)) return None
    val projectId = getWorkflowProjectId(workflowId)
    projectId match {
      case null => None
      case x => getProjectTeams(x)
    }
  }
  def getWorkflowUsers(workflowId: String) : Option[List[User]] = {
    if (! workflows.contains(workflowId)) return None
    getWorkflowTeams(workflowId) match {
      case None => None
      case Some(manyTeams) => Some(manyTeams.flatMap(x => getTeamUsers(x.id)).flatten)
    }
  }

  def getWorkflowDatasets(workflowId: String) : Option[List[Dataset]] = {
    if (! workflows.contains(workflowId)) return None
    val x = for (
      (dId, perm) <- workflowDatasets(workflowId);
      d = datasets(dId);
      projectId = getDatasetProjectId(dId)
    ) yield makeDataset(d, perm, projectId)
    Some(makeUniqueDatasets(x.toList))
  }

  // DATASET ===========================================================================================================


  def getDataset(datasetId: String) : Option[Dataset]  = {
    if (! datasets.contains(datasetId)) return None
    val d : DDBDataset = datasets(datasetId)
    val projectId = getDatasetProjectId(datasetId)
    if (projectId != null)
      return Some(makeDataset(d, Permission.NONE, projectId))
    None
  }
  def getDatasetProject(datasetId: String)    : Option[Project]  = {
    if (! datasets.contains(datasetId)) return None
    val pId = getDatasetProjectId(datasetId)
    if (pId != null)
      return Some(makeProject(projects(pId), Permission.ADMIN))
    None
  }
  def getDatasetTeams(datasetId: String) : Option[List[Team]] = {
    if (! datasets.contains(datasetId)) return None
    val projectId = getDatasetProjectId(datasetId)
    projectId match {
      case null => None
      case x => getProjectTeams(x)
    }
  }
  def getDatasetUsers(datasetId: String) : Option[List[User]]  = {
    if (! datasets.contains(datasetId)) return None
    getDatasetTeams(datasetId) match {
      case None => None
      case Some(manyTeams) => Some(manyTeams.flatMap(x => getTeamUsers(x.id)).flatten)
    }
  }

  def getDatasetWorkflows(datasetId: String) : Option[List[Workflow]] = {
    if (! datasets.contains(datasetId)) return None
    val x = for (
      (wId, dsets) <- workflowDatasets;
      (dId, perm) <- dsets;
      w = workflows(wId);
      projectId = getWorkflowProjectId(wId)
      if dId == datasetId
    ) yield makeWorkflow(w, perm, projectId)
    Some(makeUniqueWorkflows(x.toList))
  }

  // FUNCTION ==============================================================================================================

  def getFunction : Option[List[Function]] = {
    if (functions.isEmpty) return None
    Some(functions.keys.map(x => getFunction(x).get).toList)
  }

  def getFunction(functionId: String) : Option[Function] = {
    if (! functions.contains(functionId)) return None
    Some(makeFunction(functions(functionId), Permission.NONE, "p1"))
  }

  def getFunctionProject(functionId: String) : Option[Project] = {
    if (! functions.contains(functionId)) return None
    val pId = getFunctionProjectId(functionId)
    if (pId != null)
      return Some(makeProject(projects(pId), Permission.ADMIN))
    None
  }

  def createFunction(function: Function): Function = {
    val timestamp = java.lang.System.currentTimeMillis()
    val newFunction = new DDBFunction(
      function.id,
      function.name,
      function.category,
      function.description,
      function.jsonContent,
      timestamp,
      timestamp
    )
    functions += (function.id -> newFunction)
    projectFunctions(function.projectId) += function.id
    makeFunction(newFunction, Permission.ADMIN, function.projectId)
  }

}
