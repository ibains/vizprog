package models

import scala.collection.mutable.HashSet

/**
  * This has the data structures and dummy relationships represented in an
  * in-memory object - a dummy database as a spec
 */

object DummyDatabase {

  case class DDBUser (
    id                : String,
    email             : String,
    firstName         : Option[String],
    lastName          : Option[String],
    fullName          : Option[String],
    avatarURL         : Option[String]
  )

  case class DDBCompany (
    id                : String,
    name              : String,
    description       : Option[String]
  )
  
  case class DDBTeam (
    id                : String,
    name              : String,
    description       : Option[String]
  )
  
  case class DDBProject (
    id                : String,
    name              : String,
    description       : Option[String]
  )

  case class DDBWorkflow (
    id                : String,
    name              : String,
    description       : Option[String],
    jsonContent       : String,
    created           : Long,
    lastModified      : Long
  )

  case class DDBDataset (
     id                : String,
     name              : String,
     description       : Option[String],
     location          : Option[String],
     format            : Option[String],
     schemaJson        : Option[String],
     statsJson         : Option[String],
     created           : Long,
     lastModified      : Long
  )

  case class DDBFunction (
    id                 : String,
    name               : String,
    category           : String,
    description        : Option[String],
    jsonContent        : String,
    created            : Long,
    lastModified       : Long
  )

  // Insert data -------------------------------------------


  val t1: Long = System.currentTimeMillis / 1000
  val t2: Long = t1 + 1000

  val users = Map(
    "u1" -> DDBUser("u1", "Greg.Dean@vizprog.com",             Some("Greg"),      Some("Dean"),        Some("Greg Dean"),           None),
    "u2" -> DDBUser("u2", "Ed.Johnson@vizprog.com",             Some("Ed"),      Some("Johnson"),        Some("Ed Johnson"),             None),
    "u3" -> DDBUser("u3", "jane.doe@vizprog.com",        Some("Jane"),    Some("Doe"),     Some("Jane Doe"),        None),
    "u4" -> DDBUser("u4", "Paul.Williams@vizprog.com", Some("Paul"), Some("Williams"), Some("Paul Williams"), None),
    "u5" -> DDBUser("u5", "bob.smith@vizprog.com",           Some("Bob"),   Some("Smith"),         Some("Bob Smith"),           None))
  val teams = Map(
    "t1" -> DDBTeam("t1", "Security",                    Some("Team that build models to enhance security and to detect security threats")),
    "t2" -> DDBTeam("t2", "Network",                     Some("Team that build models to detect and prevent network outage")),
    "t3" -> DDBTeam("t3", "Customer Satisfaction",       Some("Team that analyses customer satisfaction surveys and product reviews")),
    "t4" -> DDBTeam("t4", "Geo Analytics",               Some("Team that performs analytics based on the Geographical location of the users")),
    "t5" -> DDBTeam("t5", "Mobile Devices Analytics",    Some("Team that performs analytics based on data points from mobile devices")),
    "t6" -> DDBTeam("t6", "Twitter Analytics",           Some("Team that performs analytics on twitter feed")))
  val companies = Map(
    "c1" -> DDBCompany("c1", "VizProg",           Some("VizProg Company")))
  val projects = Map (
    "p1" -> DDBProject("p1", "Product Recommendations",        Some("Recommend new products to users based on their purchase history")),
    "p2" -> DDBProject("p2", "Buying Behavior Analyses",       Some("Analyse buyers behavior")),
    "p3" -> DDBProject("p3", "Survey Analyses",                Some("Analyse customer satisfaction surveys")),
    "p4" -> DDBProject("p4", "Sale Campaigns",                 Some("Find products that could be offered for a lower price")),
    "p5" -> DDBProject("p5", "Review Analyses",                Some("Analyse product reviews")),
    "p6" -> DDBProject("p6", "Fraud Detection",                Some("Analyse patterns and detect fraud transactions")),
    "p7" -> DDBProject("p7", "Bundle Offer Discounts",         Some("Find product bundles and price to best match the users interest")),
    "p8" -> DDBProject("p8", "Trend Analyses",                 Some("Analyse buying trends")),
    "p9" -> DDBProject("p9", "Network outage detection",       Some("Detect and prevent network outages"))
    )
  val workflows = Map (
    "w1" -> DDBWorkflow("w1", "recommendations",    Some("Workflow to recommend products"), "Graph : { body }", t1, t2),
    "w7" -> DDBWorkflow("w7", "sale_campaign",      Some("Workflow to recommend sale campaigns"),  "Graph : { body }", t1, t2),
    "w2" -> DDBWorkflow("w2", "survey_analyses",    Some("Workflow to analyse customer satisfaction surveys"),            "Graph : { body }", t1, t2),
    "w3" -> DDBWorkflow("w3", "review_analyses",    Some("Workflow to analyse product reviews"),                                         "Graph : { body }", t1, t2),
    "w4" -> DDBWorkflow("w4", "bundle_offer",       Some("Workflow to recommend bundle offers"),                                          "Graph : { body }", t1, t2),
    "w5" -> DDBWorkflow("w5", "fraud_detection",    Some("Workflow to detect and prevent fraud transactions"),                                      "Graph : { body }", t1, t2),
    "w6" -> DDBWorkflow("w6", "tweet_analyses",     Some("Workflow that analyses tweets"),                                        "Graph : { body }", t1, t2),
    "w8" -> DDBWorkflow("w8", "network_outage",     Some("Workflow to detect network outage"),                                        "Graph : { body }", t1, t2),
    "w9" -> DDBWorkflow("w9", "ab_testing",         Some("Workflow to perform A/B testing"),                                        "Graph : { body }", t1, t2),
    "w10" -> DDBWorkflow("w10", "behavior_analyses",Some("Workflow to perform buyer behavior analyses"),                   "Graph : { body }", t1, t2),
    "w11" -> DDBWorkflow("w11", "Science Classification", Some("Workflow to perform text classification"),                   "Graph : { body }", t1, t2))

  val datasets = Map(
    "d1" -> DDBDataset("d1", "transactions",                             Option("Data on all the online order transactions"),
      Option("s3://S3_BUCKET/examples/data/transactions.csv"),      Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d2" -> DDBDataset("d2", "users",                        Option("Data about all the users"),
      Option("s3://S3_BUCKET/examples/data/users.csv"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d3" -> DDBDataset("d3", "orders",                        Option("Data about all the orders placed"),
      Option("s3://S3_BUCKET/examples/data/orders.csv"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d4" -> DDBDataset("d4", "geo_location",                        Option("Data about latitude and longitude of users"),
      Option("s3://S3_BUCKET/examples/data/geo_location.csv"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d5" -> DDBDataset("d5", "surveys",                        Option("Data about customer satisfaction surveys"),
      Option("s3://S3_BUCKET/examples/data/surveys.csv"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d6" -> DDBDataset("d6", "product_reviews",                        Option("Data about product reviews"),
      Option("s3://S3_BUCKET/examples/data/reviews.csv"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d7" -> DDBDataset("d7", "Twitter",                        Option("Twitter data"),
      Option("s3://S3_BUCKET/examples/data/twitter.csv"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d8" -> DDBDataset("d8", "Network uptime",                        Option("Data on network uptime"),
      Option("s3://S3_BUCKET/examples/data/network.csv"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2),

    "d9" -> DDBDataset("d9", "20 newsgroups",                        Option("20 newsgroup articles"),
      Option("s3://S3_BUCKET/examples/data/20news-bydate-test/*"), Option("csv"),
      Option("schema{ }"),                                          Option("stats{ }"), t1, t2)
  )

  var functions = Map(
    "f1" -> DDBFunction("f1", "Prepare Transactions", "Transactions", Some("Parse Transactions dataset into the right format"), "{}", t1, t2),
    "f2" -> DDBFunction("f2", "Prepare Users", "Users", Some("Parse Users dataset into the right format"), "{}", t1, t2),
    "f3" -> DDBFunction("f3", "Prepare Orders", "Orders", Some("Parse Orders dataset into the right format"), "{}", t1, t2)
  )

  val companyUser     = Map ("c1" -> HashSet(("u1", Permission.READ),
                                             ("u2", Permission.READ),
                                             ("u3", Permission.READ),
                                             ("u4", Permission.READ),
                                             ("u5", Permission.ADMIN)))
  val companyTeam     = Map ("c1" -> HashSet("t1", "t2", "t3", "t4", "t5", "t6"))
  val companyProject  = Map ("c1" -> HashSet("p1", "p2", "p3", "p4", "p5", "p6", "p7", "p8", "p9"))
  val teamUser        = Map ("t1" -> HashSet("u1", "u5"),
                             "t2" -> HashSet("u2", "u1", "u5"),
                             "t3" -> HashSet("u3", "u4"),
                             "t4" -> HashSet("u4", "u2"),
                             "t5" -> HashSet("u1", "u3"),
                             "t6" -> HashSet("u1", "u5"))
  val teamProject     = Map ("t1" -> HashSet(("p6", Permission.WRITE)),
                             "t2" -> HashSet(("p9", Permission.WRITE)),
                             "t3" -> HashSet(("p3", Permission.WRITE)),
                             "t3" -> HashSet(("p5", Permission.WRITE)),
                             "t4" -> HashSet(("p4", Permission.WRITE)),
                             "t5" -> HashSet(("p8", Permission.WRITE)),
                             "t6" -> HashSet(("p7", Permission.WRITE)))
  /*
  val userProject     = Map ("1" -> HashSet(("1", Permission.WRITE),
                                            ("3", Permission.WRITE)),
                             "2" -> HashSet(("1", Permission.WRITE)),
                             "3" -> HashSet(("2", Permission.WRITE)),
                             "4" -> HashSet(("3", Permission.WRITE)),
                             "5" -> HashSet(("2", Permission.WRITE)))
  */

  //val userWorkflow

  val workflowDatasets = Map (
                              "w1" -> HashSet(("d3", Permission.READ), ("d2", Permission.READ), ("d1", Permission.READ)),
                              "w2" -> HashSet(("d5", Permission.READ), ("d2", Permission.READ)),
                              "w3" -> HashSet(("d6", Permission.READ), ("d2", Permission.READ)),
                              "w4" -> HashSet(("d2", Permission.READ), ("d1", Permission.READ), ("d3", Permission.READ)),
                              "w5" -> HashSet(("d1", Permission.READ), ("d2", Permission.READ)),
                              "w6" -> HashSet(("d7", Permission.READ), ("d2", Permission.READ)),
                              "w7" -> HashSet(("d3", Permission.READ), ("d4", Permission.READ)),
                              "w8" -> HashSet(("d8", Permission.READ)),
                              "w9" -> HashSet(("d3", Permission.READ)),
                              "w11" -> HashSet(("d9", Permission.READ))
                              )

  val projectWorkflow = Map ("p1" -> HashSet("w1"),
                             "p2" -> HashSet("w10"),
                             "p3" -> HashSet("w2"),
                             "p4" -> HashSet("w7"),
                             "p5" -> HashSet("w3"),
                             "p6" -> HashSet("w5", "w9", "w11"),
                             "p7" -> HashSet("w4"),
                             "p8" -> HashSet("w6"),
                             "p9" -> HashSet("w8"))


  val projectDataset  = Map ("p1" -> HashSet("d2", "d4"),
                             "p2" -> HashSet("d1", "d2"),
                             "p3" -> HashSet("d2", "d5"),
                             "p4" -> HashSet("d1", "d2", "d4"),
                             "p5" -> HashSet("d3", "d2"),
                             "p6" -> HashSet("d1", "d2", "d4", "d9"),
                             "p7" -> HashSet("d3"),
                             "p8" -> HashSet("d3", "d4"),
                             "p9" -> HashSet("d8"))

  val projectFunctions = Map("p1" -> HashSet("f1", "f2"),
                             "p2" -> HashSet("f3"))

}