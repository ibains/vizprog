package models

/**
 * The purpose of these case classes is to define the data as is read by the application
 *
 * This is to be dissociated from the notion of data as it is stored
 *
 * NOTE: Every entity has permissions - the meaning of which is defined by
 *       the source and lookup entities
 *
 * TODO: Make writeable - mutability will have design implications
 *
 * TODO: Executions - these will have design implications
 *
 * TODO: Versioning - these will have the largest design implications
 *
 */

object Permission extends Enumeration {
  val NONE, READ, WRITE, ADMIN = Value
}

case class User (
  id                : String,
  email             : String,
  firstName         : Option[String],
  lastName          : Option[String],
  fullName          : Option[String],
  avatarURL         : Option[String],
  userPermission    : Permission.Value
)

case class Company (
  id                : String,
  name              : String,
  description       : Option[String],
  userPermission    : Permission.Value
)

case class Project (
  id                : String,
  name              : String,
  description       : Option[String],
  userPermission    : Permission.Value
)

case class Team (
  id                : String,
  name              : String,
  description       : Option[String],
  userPermission    : Permission.Value
)

case class Workflow (
  id                : String,
  name              : String,
  description       : Option[String],
  userPermission    : Permission.Value,
  jsonContent       : String,
  created           : Long,
  lastModified      : Long,
  projectId         : String)

case class Dataset (
  id                : String,
  name              : String,
  description       : Option[String],
  userPermission    : Permission.Value,
  location          : Option[String],
  format            : Option[String],
  schemaJson        : Option[String],
  statsJson         : Option[String],
  created           : Long,
  lastModified      : Long,
  projectId         : String)

case class Function (
  id                : String,
  name              : String,
  category          : String,
  description       : Option[String],
  userPermission    : Permission.Value,
  jsonContent       : String,
  created           : Long,
  lastModified      : Long,
  projectId         : String)



trait MetadataRepo {

  def getCompany(companyId: String)           : Option[Company]         //
  def getCompanyUsers(companyId: String)      : Option[List[User]]      // Get all users in this company
  def getCompanyProjects(companyId: String)   : Option[List[Project]]   // Get all projects in this company
  def getCompanyTeams(companyId: String)      : Option[List[Team]]      // Get all teams in this company

  def getTeam(teamId: String)                 : Option[Team]            //
  def getTeamCompany(teamId: String)          : Option[Company]         //
  def getTeamProjects(teamId: String)         : Option[List[Project]]   //
  def getTeamUsers(teamId: String)            : Option[List[User]]      // Get users, with their permissions over this
  def getTeamWorkflows(teamId: String)        : Option[List[Workflow]]  // Get members with this' permission over them
  def getTeamDatasets(teamId: String)         : Option[List[Dataset]]   // Same as workflow

  def getProject(projectId: String)           : Option[Project]         //
  def getProjectCompany(projectId: String)    : Option[Company]         //
  def getProjectTeams(projectId: String)      : Option[List[Team]]      //
  def getProjectUsers(projectId: String)      : Option[List[User]]      // Get users, with their permissions over this
  def getProjectWorkflows(projectId: String)  : Option[List[Workflow]]  // Get members with this' permission over them
  def getProjectDatasets(projectId: String)   : Option[List[Dataset]]   // Same as workflow
  def getProjectFunctions(projectId: String)  : Option[List[Function]]

  def getUser(userId: String)                 : Option[User]            //
  def getUserCompany(userId: String)          : Option[Company]         // Company - admins have UI implications
  def getUserProjects(userId: String)         : Option[List[Project]]   // Get project details and permissions
  def getUserTeams(userId: String)            : Option[List[Team]]      //
  def getUserWorkflows(userId: String)        : Option[List[Workflow]]  // Here permissions vs project presence/permissions shows override
  def getUserDatasets(userId: String)         : Option[List[Dataset]]   // Same as workflow

  def getWorkflow(workflowId: String)         : Option[Workflow]        //
  def getWorkflowProject(workflowId: String)  : Option[Project]         // Single project
  def getWorkflowTeams(workflowId: String)    : Option[List[Team]]      //
  def getWorkflowUsers(workflowId: String)    : Option[List[User]]      // Project users + direct permissions users
  def getWorkflowDatasets(workflowId: String) : Option[List[Dataset]]   // Permissions - read / write express usage relations rather than permissions

  def getDataset(datasetId: String)           : Option[Dataset]         //
  def getDatasetProject(datasetId: String)    : Option[Project]         // Single project
  def getDatasetTeams(datasetId: String)      : Option[List[Team]]      //
  def getDatasetUsers(datasetId: String)      : Option[List[User]]      // Project users + direct permissions users
  def getDatasetWorkflows(datasetId: String)  : Option[List[Workflow]]  // Permissions - read / write express usage relations rather than permissions

  def getFunction(functionId: String)         : Option[Function]
  def getFunctionProject(functionId: String)  : Option[Project]

  def createFunction(function: Function): Function
}