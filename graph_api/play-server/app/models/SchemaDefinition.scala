package models

import sangria.schema._
import sangria.relay._


/**
  * Defines Metadata GraphQL schema.
  *
  * It expects an implementation of MetadataRepo in context object
  *   example: ctx.ctx.asInstanceOf[MetadataRepo]
  *
  * This code does not change when adding new implementations of MetadataRepo
  *
 **/

object SchemaDefinition {

  val PermissionEnum = EnumType(
    "Permissions",
    Some("Ownership and relationship permissions"),
    List(
      EnumValue("NONE",  value = Permission.NONE,  description = Some("Not Applicable")),
      EnumValue("READ",  value = Permission.READ,  description = Some("Read permission or relation")),
      EnumValue("WRITE", value = Permission.WRITE, description = Some("Write permission or relation")),
      EnumValue("ADMIN", value = Permission.ADMIN, description = Some("Admin permission"))))

  val CompanyType = ObjectType("Company", "Represents a Company",
    fields[Unit, Company](
      Field("id",          StringType,             Some("The id of the Company"),                  resolve = _.value.id),
      Field("name",        StringType,             Some("The name of the Company"),                resolve = _.value.name),
      Field("description", OptionType(StringType), Some("The description of the Company"),         resolve = _.value.description),
      Field("permissions", PermissionEnum,         Some("Permissions of the user on the Company"), resolve = _.value.userPermission),
      Field("teams",       OptionType(ListType(TeamType)),
        Some("The teams in this company"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getCompanyTeams(ctx.value.id)
      ),
      Field("projects",    OptionType(ListType(ProjectType)),
        Some("The projects in this company"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getCompanyProjects(ctx.value.id)
      ),
      Field("users",    OptionType(ListType(UserType)),
        Some("The users in this company"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getCompanyUsers(ctx.value.id)
      )
    )
  )

  lazy val TeamType : ObjectType[Unit, Team] = ObjectType("Team", () =>
    fields[Unit, Team](
      Field("id",          StringType,             Some("The id of the Project"),                  resolve = _.value.id),
      Field("name",        StringType,             Some("The name of the Project"),                resolve = _.value.name),
      Field("description", OptionType(StringType), Some("The description of the Project"),         resolve = _.value.description),
      Field("permissions", PermissionEnum,         Some("Permissions of the user on the Project"), resolve = _.value.userPermission),

      Field("company",     OptionType(CompanyType),
        Some("The company for this team"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getTeamCompany(ctx.value.id)
      ),
      Field("projects",    OptionType(ListType(ProjectType)),
        Some("All projects this team has access to"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getTeamProjects(ctx.value.id)
      ),
      Field("users",       OptionType(ListType(UserType)),
        Some("All users that are in this team"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getTeamUsers(ctx.value.id)
      ),
      Field("workflows",   OptionType(ListType(WorkflowType)),
        Some("All workflows in projects this team has access to"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getTeamWorkflows(ctx.value.id)
      ),
      Field("datasets",    OptionType(ListType(DatasetType)),
        Some("All datasets in projects this team has access to"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getTeamDatasets(ctx.value.id)
      )
    )
  )

  lazy val ProjectType : ObjectType[Unit, Project] = ObjectType("Project", () =>
    fields[Unit, Project](
      Field("id",          StringType,             Some("The id of the Project"),                  resolve = _.value.id),
      Field("name",        StringType,             Some("The name of the Project"),                resolve = _.value.name),
      Field("description", OptionType(StringType), Some("The description of the Project"),         resolve = _.value.description),
      Field("permissions", PermissionEnum,         Some("Permissions of the user on the Project"), resolve = _.value.userPermission),

      Field("company",     OptionType(CompanyType),
        Some("The company for this projects"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getProjectCompany(ctx.value.id)
      ),
      Field("teams",       OptionType(ListType(TeamType)),
        Some("All teams with access to the Project"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getProjectTeams(ctx.value.id)
      ),
      Field("users",       OptionType(ListType(UserType)),
        Some("All users with access to the Project"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getProjectUsers(ctx.value.id)
      ),
      Field("workflows",   OptionType(ListType(WorkflowType)),
        Some("All workflows in this Project"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getProjectWorkflows(ctx.value.id)
      ),
      Field("datasets",    OptionType(ListType(DatasetType)),
        Some("All datasets in this Project"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getProjectDatasets(ctx.value.id)
      ),
      Field("functions",    OptionType(ListType(FunctionType)),
        Some("All functions in this Project"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getProjectFunctions(ctx.value.id)
      )
    )
  )

  // use lazy types and function definitions to get around circular references
  lazy val UserType : ObjectType[Unit, User] = ObjectType("User", () =>
    fields[Unit, User](
      Field("id",          StringType,             Some("The id of the Person"),                   resolve = _.value.id),
      Field("firstName",   OptionType(StringType), Some("The first name of the Person"),           resolve = _.value.firstName),
      Field("lastName",    OptionType(StringType), Some("The last name of the Person"),            resolve = _.value.lastName),
      Field("fullName",    OptionType(StringType), Some("The full name of the Person"),            resolve = _.value.fullName),
      Field("email",       StringType,             Some("The email of the Person"),                resolve = _.value.email),
      Field("avatarURL",   OptionType(StringType), Some("Avatar URL for the Person"),              resolve = _.value.avatarURL),
      Field("permissions", PermissionEnum,         Some("Permissions that make sense in context"), resolve = _.value.userPermission),

      Field("company",     OptionType(CompanyType),
        Some("The company for this user"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getUserCompany(ctx.value.id)
      ),
      Field("projects",    OptionType(ListType(ProjectType)),
        Some("The projects for this user"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getUserProjects(ctx.value.id)
      ),
      Field("teams",       OptionType(ListType(TeamType)),
        Some("The teams this user is part of"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getUserTeams(ctx.value.id)
      ),
      Field("workflows",   OptionType(ListType(WorkflowType)),
        Some("The workflows for this user"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getUserWorkflows(ctx.value.id)
      ),
      Field("datasets",   OptionType(ListType(DatasetType)),
        Some("The datasets for this user"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getUserDatasets(ctx.value.id)
      )
    )
  )


  lazy val WorkflowType : ObjectType[Unit, Workflow] = ObjectType("Workflow", () =>
    fields[Unit, Workflow](
      Field("id",          StringType,             Some("The id of the Workflow"),                 resolve = _.value.id),
      Field("name",        StringType,             Some("The name of the Workflow"),               resolve = _.value.name),
      Field("projectId",   StringType,             Some("The parent project id"),                  resolve = _.value.projectId),
      Field("description", OptionType(StringType), Some("The workflow description"),               resolve = _.value.description),
      Field("jsonContent", StringType,             Some("Body of the Workflow"),                   resolve = _.value.jsonContent),
      Field("permissions", PermissionEnum,         Some("Permissions that make sense in context"), resolve = _.value.userPermission),
      Field("created",     LongType,               Some("Timestamp for the created date"),         resolve = _.value.created),
      Field("lastModified",LongType,               Some("Timestamp for the last modified date"),   resolve = _.value.lastModified),

      Field("project",     OptionType(ProjectType),
        Some("The project this workflow is in"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getWorkflowProject(ctx.value.id)
      ),
      Field("teams",       OptionType(ListType(TeamType)),
        Some("All teams with access to the workflow"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getWorkflowTeams(ctx.value.id)
      ),
      Field("users",       OptionType(ListType(UserType)),
        Some("All users with access to the workflow"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getWorkflowUsers(ctx.value.id)
      ),
      Field("datasets",    OptionType(ListType(DatasetType)),
        Some("All datasets read or written by this workflow"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getWorkflowDatasets(ctx.value.id)
      )
    )
  )

  lazy val DatasetType : ObjectType[Unit, Dataset] = ObjectType("Dataset", () =>
    fields[Unit, Dataset](
      Field("id",          StringType,             Some("The id of the Workflow"),                 resolve = _.value.id),
      Field("name",        StringType,             Some("The name of the Workflow"),               resolve = _.value.name),
      Field("projectId",   StringType,             Some("The parent project id"),                  resolve = _.value.projectId),
      Field("description", OptionType(StringType), Some("The workflow description"),               resolve = _.value.description),
      Field("location",    OptionType(StringType), Some("The location of the dataset such as s3"), resolve = _.value.location),
      Field("format",      OptionType(StringType), Some("The format of dataset such as parquet"),  resolve = _.value.format),
      Field("schemaJson",  OptionType(StringType), Some("The json describing the schema"),         resolve = _.value.schemaJson),
      Field("statsJson",   OptionType(StringType), Some("The json with summary statistics"),       resolve = _.value.statsJson),
      Field("permissions", PermissionEnum,         Some("Permissions that make sense in context"), resolve = _.value.userPermission),
      Field("created",     LongType,               Some("Timestamp for the created date"),         resolve = _.value.created),
      Field("lastModified",LongType,               Some("Timestamp for the last modified date"),   resolve = _.value.lastModified),

      Field("project",     OptionType(ProjectType),
        Some("The project this dataset is in"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getDatasetProject(ctx.value.id)
      ),
      Field("teams",       OptionType(ListType(TeamType)),
        Some("All teams with access to the dataset"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getDatasetTeams(ctx.value.id)
      ),
      Field("users",       OptionType(ListType(UserType)),
        Some("All users with access to the dataset"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getDatasetUsers(ctx.value.id)
      ),
      Field("workflows",    OptionType(ListType(WorkflowType)),
        Some("All workflows that read or written this dataset"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getDatasetWorkflows(ctx.value.id)
      )
    )
  )

  lazy val FunctionType : ObjectType[Unit, Function] = ObjectType("Function", () =>
    fields[Unit, Function](
      Field("id",          StringType,             Some("The id of the Function"),                 resolve = _.value.id),
      Field("name",        StringType,             Some("The name of the Function"),               resolve = _.value.name),
      Field("category",    StringType,             Some("The category this Function belongs to"),  resolve = _.value.category),
      Field("jsonContent", StringType,             Some("The JSON content of the Function"),       resolve = _.value.jsonContent),
      Field("projectId",   StringType,             Some("The parent project id"),                  resolve = _.value.projectId),
      Field("description", OptionType(StringType), Some("The Function description"),               resolve = _.value.description),
      Field("created",     LongType,               Some("Timestamp for the created date"),         resolve = _.value.created),
      Field("lastModified",LongType,               Some("Timestamp for the last modified date"),   resolve = _.value.lastModified),

      Field("project",     OptionType(ProjectType),
        Some("The project this dataset is in"),
        resolve = ctx => ctx.ctx.asInstanceOf[MetadataRepo].getFunctionProject(ctx.value.id)
      )
    )
  )

  val ID = Argument("id", StringType, description = "id of any entity")

  val Query = ObjectType(
    "Query", fields[MetadataRepo, Unit](
      Field("Company",  OptionType(CompanyType),  arguments = ID :: Nil, resolve = ctx => ctx.ctx.getCompany(ctx arg ID)),
      Field("Team",     OptionType(TeamType),     arguments = ID :: Nil, resolve = ctx => ctx.ctx.getTeam(ctx arg ID)),
      Field("Project",  OptionType(ProjectType),  arguments = ID :: Nil, resolve = ctx => ctx.ctx.getProject(ctx arg ID)),
      Field("User",     OptionType(UserType),     arguments = ID :: Nil, resolve = ctx => ctx.ctx.getUser(ctx arg ID)),
      Field("Workflow", OptionType(WorkflowType), arguments = ID :: Nil, resolve = ctx => ctx.ctx.getWorkflow(ctx arg ID)),
      Field("Dataset",  OptionType(DatasetType),  arguments = ID :: Nil, resolve = ctx => ctx.ctx.getDataset(ctx arg ID)),
      Field("Function", OptionType(FunctionType),  arguments = ID :: Nil, resolve = ctx => ctx.ctx.getFunction(ctx arg ID))
    ))

  case class FunctionMutationPayload(clientMutationId: String, functionId: String, projectId: String) extends Mutation

  val functionMutation = Mutation.fieldWithClientMutationId[MetadataRepo, Unit, FunctionMutationPayload, InputObjectType.DefaultInput](
    fieldName = "createFunction",
    typeName = "createFunction",
    inputFields = List(
      InputField("functionName", StringType),
      InputField("category", StringType),
      InputField("description", StringType),
      InputField("jsonContent", StringType),
      InputField("projectId", IDType)),
    outputFields = fields(
      Field("function", OptionType(FunctionType), resolve = ctx => ctx.ctx.getFunction(ctx.value.functionId)),
      Field("project", OptionType(ProjectType), resolve = ctx => ctx.ctx.getProject(ctx.value.projectId))),
    mutateAndGetPayload = (input, ctx) => {
      val mutationId = input(Mutation.ClientMutationIdFieldName).asInstanceOf[String]
      val functionName = input("functionName").asInstanceOf[String]
      val category = input("category").asInstanceOf[String]
      val description = input("description").asInstanceOf[String]
      val jsonContent = input("jsonContent").asInstanceOf[String]
      val projectId = input("projectId").asInstanceOf[String]
      val timestamp = java.lang.System.currentTimeMillis()

      // TODO: placeholder code for now...
      val newFunction = ctx.ctx.createFunction(new Function(
        "id",
        functionName,
        category,
        Some(description),
        Permission.NONE,
        jsonContent,
        timestamp,
        timestamp,
        projectId
      ))
      FunctionMutationPayload(mutationId, newFunction.id, projectId)
    }
  )

  /**
    * This is the type that will be the root of our mutations, and the
    * entry point into performing writes in our schema.
    *
    * This implements the following type system shorthand:
    * type Mutation {
    * createFunction(input CreateFunctionInput!): CreateFunctionPayload
    * }
    */
  val MutationType = ObjectType("Mutation", fields[MetadataRepo, Unit](functionMutation))

  val MetadataSchema = Schema(Query, Some(MutationType))
}
