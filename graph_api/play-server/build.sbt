name := "metadata-play-server"

description := "GraphQL Metadata Server"

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  filters,
  "org.sangria-graphql" %% "sangria" % "0.7.2",
  "org.sangria-graphql" %% "sangria-relay" % "0.7.2",
  "org.sangria-graphql" %% "sangria-play-json" % "0.3.2",
  "org.slf4j" % "slf4j-simple" % "1.7.12", // "org.neo4j.driver" % "neo4j-java-driver" % "1.0.4"
  "org.apache.tinkerpop" % "gremlin-driver" % "3.2.0-incubating",
  "org.apache.tinkerpop" % "gremlin-core" % "3.2.0-incubating",
  "com.datastax.cassandra" % "dse-driver" % "1.1.0",
  "joda-time" % "joda-time" % "2.9.4",
  "org.scalatest" % "scalatest_2.9.2" % "1.9.1" % "test"
)
resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"
resolvers += "Sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

routesGenerator := InjectedRoutesGenerator

lazy val root = (project in file(".")).enablePlugins(PlayScala)

