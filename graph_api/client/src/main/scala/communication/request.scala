package communication

import scalaj.http._

class request() {

  val serverUri    = "http://localhost:3000/graphql"
  val contentType  = "application/graphql"
  val smallTimeout = 100000
  val largeTimeout = 1500000

  def postRequest(query: String, callback: String => Unit) : Boolean = {
    val result = Http(serverUri).postData(query)
      .header("Content-Type", contentType)
      .header("Charset", "UTF-8")
      .option(HttpOptions.readTimeout(smallTimeout)).asString

    if (result.code != 200)
      return false

    callback(result.body)

    true

  }
}
