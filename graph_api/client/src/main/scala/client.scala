import api.APIQuery
import communication.request


object ClientDriver {

  def callback(replyBody: String) : Unit = {
    println("Result:")
    println(replyBody)
  }

  def runQueries() = {
    val query = APIQuery.getQuery
    val req = new request

    // print and run one query
    print(query)
    if (!req.postRequest(query, callback))
      println("Server query failed")
  }

  def main(args: Array[String]): Unit = {
    ClientDriver.runQueries()
  }
}