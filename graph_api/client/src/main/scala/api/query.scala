package api

object APIQuery
{
  def getQuery: String = {
    """
    {
      __schema {
        queryType {
          name,
          fields {
            name,
            description
          }
        }
      }
    }
    """
  }
}

