name := "graph_api_client"
version := "1.0.0-SNAPSHOT"
scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.json4s" %% "json4s-native" % "3.3.0",
  "org.scalaj" %% "scalaj-http" % "2.2.1",
  "org.slf4j" % "slf4j-api" % "1.7.12",
  "commons-lang" % "commons-lang" % "2.6"
)
