# --- !Ups

ALTER TABLE datasource
  DROP COLUMN type;

ALTER TABLE dataset
  DROP COLUMN type;

ALTER TABLE datasource
  ADD COLUMN content TEXT;

ALTER TABLE dataset
  ADD COLUMN content TEXT;

# --- !Downs
ALTER TABLE datasource
  ADD COLUMN type VARCHAR(255);
ALTER TABLE dataset
  ADD COLUMN type VARCHAR(255);

ALTER TABLE datasource
  DROP COLUMN content;
ALTER TABLE dataset
  DROP COLUMN content;