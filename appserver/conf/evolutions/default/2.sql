# --- !Ups
CREATE TABLE workflow (
  id            BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name          VARCHAR(255)                      NOT NULL,
  user_id       VARCHAR(255)                      NOT NULL,
  description   VARCHAR(255),
  created       TIMESTAMP                         NOT NULL,
  last_modified TIMESTAMP                         NOT NULL,
  content       TEXT,
  CONSTRAINT workflow_user_id FOREIGN KEY (user_id) REFERENCES `user` (id)
);

CREATE TABLE datasource (
  id          BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  workflow_id BIGINT                            NOT NULL,
  name        VARCHAR(255),
  type        VARCHAR(255),
  location    VARCHAR(255),
  format      VARCHAR(255),
  created     TIMESTAMP                         NOT NULL,
  CONSTRAINT datasource_workflow_id FOREIGN KEY (workflow_id) REFERENCES workflow (id)
);

CREATE TABLE dataset (
  id          BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  workflow_id BIGINT                            NOT NULL,
  name        VARCHAR(255),
  type        VARCHAR(255),
  location    VARCHAR(255),
  format      VARCHAR(255),
  created     TIMESTAMP                         NOT NULL,
  CONSTRAINT dataset_workflow_id FOREIGN KEY (workflow_id) REFERENCES workflow (id)
);

CREATE TABLE workflow_datasource (
  workflow_id   BIGINT NOT NULL,
  datasource_id BIGINT NOT NULL,
  CONSTRAINT workflow_datasource_workflow_id FOREIGN KEY (workflow_id) REFERENCES workflow (id),
  CONSTRAINT workflow_datasource_datasource_id FOREIGN KEY (datasource_id) REFERENCES datasource (id),
  PRIMARY KEY (workflow_id, datasource_id)
);

CREATE TABLE workflow_dataset (
  workflow_id BIGINT NOT NULL,
  dataset_id  BIGINT NOT NULL,
  CONSTRAINT workflow_dataset_workflow_id FOREIGN KEY (workflow_id) REFERENCES workflow (id),
  CONSTRAINT workflow_dataset_dataset_id FOREIGN KEY (dataset_id) REFERENCES dataset (id),
  PRIMARY KEY (workflow_id, dataset_id)
);

# --- !Downs

DROP TABLE workflow_datasource;
DROP TABLE workflow_dataset;
DROP TABLE datasource;
DROP TABLE dataset;
DROP TABLE workflow;
