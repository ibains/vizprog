# --- !Ups

CREATE TABLE project (
  id      BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
  name    VARCHAR(255)                      NOT NULL,
  user_id VARCHAR(255)                      NOT NULL,
  CONSTRAINT project_user_id FOREIGN KEY (user_id) REFERENCES `user` (id)
);

# --- !Downs
DROP TABLE project;