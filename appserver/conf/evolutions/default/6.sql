# --- !Ups

ALTER TABLE datasource
  ADD COLUMN project_id BIGINT NOT NULL;
ALTER TABLE datasource
  ADD CONSTRAINT datasource_project_id FOREIGN KEY (project_id) REFERENCES project (id);

ALTER TABLE dataset
  ADD COLUMN project_id BIGINT NOT NULL;
ALTER TABLE dataset
  ADD CONSTRAINT dataset_project_id FOREIGN KEY (project_id) REFERENCES project (id);

# --- !Downs
ALTER TABLE datasource
  DROP FOREIGN KEY datasource_project_id;
ALTER TABLE datasource
  DROP COLUMN project_id;

ALTER TABLE dataset
  DROP FOREIGN KEY dataset_project_id;
ALTER TABLE dataset
  DROP COLUMN project_id;
