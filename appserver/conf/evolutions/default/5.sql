# --- !Ups

ALTER TABLE workflow
  DROP FOREIGN KEY workflow_user_id;
ALTER TABLE workflow
  DROP COLUMN user_id;

ALTER TABLE workflow
  ADD COLUMN project_id BIGINT NOT NULL;
ALTER TABLE workflow
  ADD CONSTRAINT workflow_project_id FOREIGN KEY (project_id) REFERENCES project (id);

# --- !Downs
ALTER TABLE workflow
  ADD COLUMN user_id BIGINT NOT NULL;
ALTER TABLE workflow
  ADD CONSTRAINT workflow_user_id FOREIGN KEY (user_id) REFERENCES user (id);

ALTER TABLE workflow
  DROP FOREIGN KEY workflow_project_id;
ALTER TABLE workflow
  DROP COLUMN project_id;