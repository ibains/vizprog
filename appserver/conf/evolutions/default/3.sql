# --- !Ups
ALTER TABLE datasource
  DROP FOREIGN KEY datasource_workflow_id;
ALTER TABLE datasource
  DROP COLUMN workflow_id;

ALTER TABLE dataset
  DROP FOREIGN KEY dataset_workflow_id;
ALTER TABLE dataset
  DROP COLUMN workflow_id;

# --- !Downs
ALTER TABLE datasource
  ADD COLUMN workflow_id BIGINT NOT NULL;
ALTER TABLE datasource
  ADD CONSTRAINT datasource_workflow_id FOREIGN KEY (workflow_id) REFERENCES workflow (id);

ALTER TABLE dataset
  ADD COLUMN workflow_id BIGINT NOT NULL;
ALTER TABLE dataset
  ADD CONSTRAINT dataset_workflow_id FOREIGN KEY (workflow_id) REFERENCES workflow (id);