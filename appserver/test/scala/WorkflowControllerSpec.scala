package scala

import java.time.Instant
import java.util.UUID

import com.google.inject.AbstractModule
import com.mohiva.play.silhouette.api.{Environment, LoginInfo}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import com.mohiva.play.silhouette.test._
import controllers.routes
import models.daos.{UserDAO, WorkflowDao}
import models.{User, Workflow}
import net.codingwell.scalaguice.ScalaModule
import org.specs2.execute.{AsResult, Result}
import org.specs2.mutable.Specification
import org.specs2.specification.Scope
import play.api.{Application => PlayApplication}
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsArray, Json}
import play.api.test.{FakeRequest, WithApplication}

import scala.concurrent.{Await, Future}
import scala.io.Source
import play.api.test.Helpers._

import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

class WorkflowControllerSpec extends Specification {

  "getUserWorkflows" should {
    val simpleWorkflows = Source.fromURL(getClass.getResource("/" + "simpleworkflows.json")).mkString

    val getUserWorkflowsUrl = "/app/user/workflow"

    "redirect user to sign in page if user is not authorized" in new WithApplication {
      val Some(result) = route(FakeRequest(GET, getUserWorkflowsUrl))

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must be equalTo Some("/app/signIn")
    }

    "respond with only sample workflows as json if user is authorized and has no workflows" in new Context {
      new WithApplication(application) {
        val Some(result) = route(FakeRequest(routes.WorkflowController.getUserWorkflows)
          .withAuthenticator[CookieAuthenticator](loginInfo)
        )

        status(result) must equalTo(OK)
        contentType(result) must beSome.which(_ == "application/json")
        contentAsJson(result) must be equalTo Json.parse(simpleWorkflows)
      }
    }

    "respond with sample and user workflows as json if user is authorized and has workflows" in new Context {
      val workflowsJson = Json.parse("""[{
                           |  "graph": {
                           |    "metainfo" : {
                           |      "id" : "a_simple_graph",
                           |      "memory" : 1,
                           |      "processors" : 1,
                           |      "cluster": "local",
                           |      "mode": "batch"
                           |    }
                           |  }
                           |}]""" stripMargin).as[JsArray]
      var workflowJson = Json.stringify(workflowsJson(0).get)
      val workflow = Workflow(None, "my first workflow", identity, None, Instant now , Instant now, workflowJson)
      new WithDbData(application, Set(identity), Set(workflow)) {
        val Some(result) = route(FakeRequest(routes.WorkflowController.getUserWorkflows)
          .withAuthenticator[CookieAuthenticator](loginInfo)
        )

        status(result) must equalTo(OK)
        contentType(result) must beSome.which(_ == "application/json")
        contentAsJson(result) must be equalTo (Json.parse(simpleWorkflows).as[JsArray] ++ workflowsJson)
      }
    }
  }

  "saveUserWorkflow" should {
    val saveUserWorkflow = "/app/user/workflow"

    "respond with 400 (BAD_REQUEST) code if given params are not correct" in new Context {
      new WithApplication(application) {
        val Some(result) = route(FakeRequest(POST, saveUserWorkflow)
          .withAuthenticator[CookieAuthenticator](loginInfo)
          .withHeaders("Content-type"-> "application/json")
          .withJsonBody(Json.parse("""{ "field": "value" }"""))
        )

        status(result) must equalTo(BAD_REQUEST)
        contentType(result) must beSome.which(_ == "application/json")
      }
    }

    "save and respond with 200 (OK) and json body as {success: true, id: id} if user authorized and given data is correct" in new Context {
      new WithDbData(application, Set(identity), Set.empty) {
        val Some(result) = route(FakeRequest(POST, saveUserWorkflow)
          .withAuthenticator[CookieAuthenticator](loginInfo)
          .withJsonBody(Json.parse(
            """{
              | "id": null,
              | "name": "my first workflow",
              | "description": null,
              | "jsonData": {
              |   "some_key": "some_val"
              | }
              |}
            """.stripMargin))
        )

        status(result) must equalTo(OK)
        contentType(result) must beSome.which(_ == "application/json")
        (contentAsJson(result) \"success").as[Boolean] must beTrue
        (contentAsJson(result) \"id").as[Long] must beGreaterThan(1L)
      }
    }

    "redirect user to sign in page if user is not authorized" in new WithApplication {
      val Some(result) = route(FakeRequest(POST, saveUserWorkflow).withJsonBody(Json.obj("test" -> 1)))

      status(result) must equalTo(SEE_OTHER)
      redirectLocation(result) must be equalTo Some("/app/signIn")
    }

    "respond with 415 (UNSUPPORTED_MEDIA_TYPE) code if content type is not application/json" in new WithApplication {
      route(FakeRequest(POST, saveUserWorkflow)) must beSome.which (status(_) == UNSUPPORTED_MEDIA_TYPE)
    }
  }

  /**
    * The context.
    */
  trait Context extends Scope {

    /**
      * A fake Guice module.
      */
    class FakeModule extends AbstractModule with ScalaModule {
      def configure() = {
        bind[Environment[User, CookieAuthenticator]].toInstance(env)
      }
    }

    /**
      * An identity.
      */
    val loginInfo = LoginInfo("facebook", "aidar@facebook.com")
    val identity = User(id = UUID.randomUUID(), loginInfo, None, None, None, None, None)

    /**
      * A Silhouette fake environment.
      */
    implicit val env: Environment[User, CookieAuthenticator] = new FakeEnvironment[User, CookieAuthenticator](Seq
      (identity.loginInfo -> identity))

    /**
      * The application with H2 in memory database
      */
    lazy val application = new GuiceApplicationBuilder()
      .overrides(new FakeModule)
        .configure(
          "slick.dbs.default.driver" -> "slick.driver.H2Driver$",
          "slick.dbs.default.db.driver" -> "org.h2.Driver",
          "slick.dbs.default.db.url" -> "jdbc:h2:mem:test;MODE=MySQL;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;DATABASE_TO_UPPER=FALSE")
      .build()
  }

  abstract class WithDbData(app: PlayApplication, users: Set[User], workflows: Set[Workflow]) extends WithApplication(app: PlayApplication) {
    override def around[T: AsResult](t: => T): Result = super.around {
      setupData()
      t
    }

    def setupData() {
      val app2UserDao = PlayApplication.instanceCache[UserDAO]
      val userDao: UserDAO = app2UserDao(app)
      val app2WorkflowDao = PlayApplication.instanceCache[WorkflowDao]
      val workflowDao: WorkflowDao = app2WorkflowDao(app)

      Await.result(Future.sequence(users.map(userDao.save)), 1 seconds)
      Await.result(Future.sequence(workflows.map(workflowDao.saveWorkflow)), 1 seconds)
    }
  }
}