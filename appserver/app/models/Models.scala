package models

import java.time.Instant
import java.util.UUID

import com.mohiva.play.silhouette.api.{Identity, LoginInfo}

/**
  * The user object.
  *
  * @param id        The unique ID of the user.
  * @param loginInfo The linked login info.
  * @param firstName Maybe the first name of the authenticated user.
  * @param lastName  Maybe the last name of the authenticated user.
  * @param fullName  Maybe the full name of the authenticated user.
  * @param email     Maybe the email of the authenticated provider.
  * @param avatarURL Maybe the avatar URL of the authenticated provider.
  */
case class User(
                 id: UUID,
                 loginInfo: LoginInfo,
                 firstName: Option[String],
                 lastName: Option[String],
                 fullName: Option[String],
                 email: Option[String],
                 avatarURL: Option[String]) extends Identity

case class UserInfo(id: String, email: Option[String])

case class Project(id: Long, name: String)

case class ProjectUserId(id: Long, name: String, userId: String)

case class ProjectUser(id: Long, name: String, user: UserInfo)

case class Workflow(id: Long,
                    name: String,
                    project: ProjectUser,
                    description: Option[String],
                    created: Instant,
                    lastModified: Instant,
                    content: String)

case class WorkflowCreateData(projectId: Long,
                              name: String,
                              description: Option[String],
                              content: String,
                              created: Instant = Instant now,
                              lastModified: Instant = Instant now)

case class WorkflowData(projectId: Long,
                        name: String,
                        user: User,
                        description: Option[String],
                        content: String,
                        created: Instant = Instant.now,
                        lastModified: Instant = Instant.now)


case class DatasourceData(name: String,
                          workflowId: Long,
                          projectId: Long,
                          location: String,
                          format: String,
                          content: String)

case class Datasource(id: Long,
                      name: String,
                      location: String,
                      format: String,
                      created: Instant)

case class DatasourceProject(id: Long,
                      name: String,
                      location: String,
                      format: String,
                      created: Instant,
                      project: ProjectUser)

case class DatasetData(name: String,
                       workflowId: Long,
                       projectId: Long,
                       location: String,
                       format: String,
                       content: String)

case class DataSet(id: Long,
                   name: String,
                   location: String,
                   format: String,
                   created: Instant)