package models.services

import javax.inject.{Inject, Singleton}

import models.daos.{DatasetDao, ProjectDao, WorkflowDao}
import models.{DataSet, DatasetData, User}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

@Singleton
class DatasetService @Inject()(datasetDao: DatasetDao, projectDao: ProjectDao, workflowDao: WorkflowDao) {
  /**
    * Removes relation between dataset and workflow. Relation and dataset will be removed only if its owner
    * eq to given user
    *
    * @param datasetId  identity of dataset
    * @param workflowId identity of workflow
    * @param owner      owner of dataset
    * @return true if link will be removed otherwise false
    */
  def remove(datasetId: Long, workflowId: Long, owner: User): Future[Boolean] = datasetDao.getOwnerId(datasetId) flatMap {
    case Some(userId) if userId == owner.id.toString => datasetDao.remove(datasetId, workflowId).map(_ => true)
    case _ => Future.successful(false)
  }

  /**
    * Updates dataset. Dataset will be updated in case there is an existing dataset with identity eq to
    * given datasetId and current user is owner of that dataset
    *
    * @param datasetId   identity of dataset that needs to be updated
    * @param datasetData model that contains data that will be used to update dataset
    * @param owner       owner of dataset
    * @return true if dataset was updated
    */
  def update(datasetId: Long, datasetData: DatasetData, owner: User): Future[Boolean] = datasetDao.getOwnerId(datasetId) flatMap {
    case Some(userId) if userId == owner.id.toString => datasetDao.update(datasetId, datasetData)
    case _ => Future.successful(false)
  }

  /**
    * @param owner owner of datasets those datasets needs to be fetched
    * @return sequence of datasets of given owner
    */
  def fetchDatasets(owner: User): Future[Seq[DataSet]] = datasetDao.fetchDatasets(owner)

  /**
    * Creates new dataset and saves it in database
    *
    * @param datasetData model containing data to create new datasouce
    * @param owner       owner of workflow that is related with given dataset
    * @return None if there is no workflow by given id or given user is not owner of that workflow, otherwise
    *         Some(new entity's id)
    */
  def save(datasetData: DatasetData, owner: User): Future[Option[Long]] =
    projectDao.getById(datasetData.projectId).flatMap {
      case Some(project) if project.userId == owner.id.toString => workflowDao.fetchProject(datasetData.workflowId) flatMap {
        case Some(p) if p.id == datasetData.projectId => datasetDao.save(datasetData).map(Some(_))
        case _ => Future.successful(None)
      }
      case _ => Future.successful(None)
    }
}