package models.services

import javax.inject.{Inject, Singleton}

import models.daos.WorkflowDao
import models.{User, Workflow, WorkflowCreateData}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

@Singleton
class WorkflowService @Inject()(workflowDao: WorkflowDao, projectService: ProjectService) {
  /**
    * Updates workflow it it belongs to given owner
    *
    * @param workflowId   identifier of workflow
    * @param workflowData workflow that needs to be updated
    * @param owner        workflow owner
    * @return true if it is updated otherwise false
    */
  def update(workflowId: Long, workflowData: WorkflowCreateData, owner: User) = workflowDao.fetchProject(workflowId) flatMap {
    case Some(project) if project.userId == owner.id.toString => workflowDao.update(workflowId, workflowData).map(_ > 0)
    case _ => Future.successful(false)
  }

  /**
    * Removes workflow by given workflowId and owner
    *
    * @param workflowId identity of workflow
    * @param owner      workflow owner
    * @return true if workflow was removed otherwise false
    */
  def remove(workflowId: Long, owner: User): Future[Boolean] = workflowDao.fetchProject(workflowId) flatMap {
    case Some(project) if project.userId == owner.id.toString => workflowDao.remove(workflowId)
    case _ => Future.successful(false)
  }

  /**
    * Creates new workflow if given workflowOwner is authentic for given project
    *
    * @param workflow      data to create a workflow
    * @param workflowOwner owner of workflow that needs to be created
    * @return Future[None] if given projectOwner is not owner of project of given workflow otherwise
    *         Future with Some identity of new workflow
    */
  def create(workflow: WorkflowCreateData, workflowOwner: User): Future[Option[Long]] = projectService.isUserOwnerOfProject(workflowOwner.id.toString, workflow.projectId) flatMap {
    case true => workflowDao.create(workflow).map(Some(_))
    case _ => Future.successful(None)
  }

  /**
    * Fetches workflows of given user
    *
    * @param owner workflow owner
    * @return Seq of workflows
    */
  def getWorkflows(owner: User): Future[Seq[Workflow]] = workflowDao.fetchWorkflows(owner)
}