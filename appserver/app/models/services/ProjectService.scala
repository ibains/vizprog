package models.services

import javax.inject.Inject

import models.{User, Project}
import models.daos.ProjectDao
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

class ProjectService @Inject()(projectDao: ProjectDao) {
  /**
    * Creates project with given data
    *
    * @param name name of project
    * @param owner user that owns project
    * @return identity of created project
    */
  def create(name: String, owner: User): Future[Long] = projectDao.create(name, owner)

  /**
    * Checks if user by given userId is owner of project by given projectId
    *
    * @param userId identifier of user
    * @param projectId identifier of project
    * @return true if there is a project with given id and user is owner of that project, otherwise false
    */
  def isUserOwnerOfProject(userId: String, projectId: Long): Future[Boolean] = projectDao.getById(projectId) map {
    case Some(project) if project.userId == userId => true
    case _ => false
  }

  /**
    * Fetches workflows of given user
    *
    * @param owner project owner
    * @return Seq of projects
    */
  def getProjects(owner: User): Future[Seq[Project]] = projectDao.fetchProjects(owner)
}
