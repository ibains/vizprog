package models.services

import java.util.UUID
import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.impl.providers.CommonSocialProfile
import controllers.{CodegenInitializer, InitializationCache}
import models.{DatasourceData, User, WorkflowCreateData}
import models.daos.{DatasourceDao, ProjectDao, UserDAO, WorkflowDao}
import play.api.libs.concurrent.Execution.Implicits._
import vizprog.workflow.WorkflowExecutionManager

import scala.concurrent.Future

/**
 * Handles actions to users.
 *
 * @param userDAO The user DAO implementation.
 */
class UserServiceImpl @Inject() (userDAO: UserDAO, projectDao: ProjectDao, workflowDao: WorkflowDao,
                                 datasourceDao: DatasourceDao) extends UserService {

  val wm  = new WorkflowExecutionManager
  // Setup JsonToSpark component with correct AWS/Local Settings
  CodegenInitializer.setupEnvironment(wm)
  // Read example workflows
  CodegenInitializer.readExampleWorkflows(wm)
  // Read Example datasources
  CodegenInitializer.readExampleDataSources(wm)

  /**
   * Retrieves a user that matches the specified login info.
   *
   * @param loginInfo The login info to retrieve a user.
   * @return The retrieved user or None if no user could be retrieved for the given login info.
   */
  def retrieve(loginInfo: LoginInfo): Future[Option[User]] = userDAO.find(loginInfo)

  /**
   * Saves a user.
   *
   * @param user The user to save.
   * @return The saved user.
   */
  def save(user: User) = userDAO.save(user).map(u => {
    setupSamplesToUser(u)
    u
  })

  /**
   * Saves the social profile for a user.
   *
   * If a user exists for this profile then update the user, otherwise create a new user with the given profile.
   *
   * @param profile The social profile to save.
   * @return The user for whom the profile was saved.
   */
  def save(profile: CommonSocialProfile) = {
    userDAO.find(profile.loginInfo).flatMap {
      case Some(user) => // Update user with profile
        save(user.copy(
          firstName = profile.firstName,
          lastName = profile.lastName,
          fullName = profile.fullName,
          email = profile.email,
          avatarURL = profile.avatarURL
        ))
      case None => // Insert a new user
        save(User(
          id = UUID.randomUUID(),
          loginInfo = profile.loginInfo,
          firstName = profile.firstName,
          lastName = profile.lastName,
          fullName = profile.fullName,
          email = profile.email,
          avatarURL = profile.avatarURL
        ))
    }
  }

  /**
    * Sets up samples for given user
    *
    * @param user
    */
  def setupSamplesToUser(user: User) {
    projectDao.create("Sample", user).map(projectId => {
      InitializationCache.workflows.foreach {
        case (name, content) =>
          val workflow = WorkflowCreateData(projectId, name, Some("An example workflow"), content)
          workflowDao.create(workflow).map(workflowId => {
            InitializationCache.datasources.foreach {
              case (dsName, dsContent) =>
                val datasourceData = DatasourceData(dsName, workflowId, projectId, "s3", "csv", dsContent)
                datasourceDao.save(datasourceData)
            }
          })
      }
    })
  }
}