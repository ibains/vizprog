package models.services

import javax.inject.{Inject, Singleton}

import models.daos.{DatasourceDao, ProjectDao, WorkflowDao}
import models.{Datasource, DatasourceData, DatasourceProject, User}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

@Singleton
class DatasourceService @Inject()(datasourceDao: DatasourceDao, workflowDao: WorkflowDao, projectDao: ProjectDao) {
  /**
    * Updates datasource. Datasource will be updated in case there is an existing datasource with identity eq to
    * given datasourceId and current user is owner of that datasource
    *
    * @param datasourceId   identity of datasource that needs to be updated
    * @param datasourceData model that contains data that will be used to update datasource
    * @param owner          owner of datasource
    * @return true if datasource was updated
    */
  def update(datasourceId: Long, datasourceData: DatasourceData, owner: User): Future[Boolean] = datasourceDao.getOwnerId(datasourceId) flatMap {
    case Some(userId) if userId == owner.id.toString => datasourceDao.update(datasourceId, datasourceData)
    case _ => Future.successful(false)
  }

  /**
    * @param owner owner of datasources those datasets needs to be fetched
    * @return sequence of datasources of given owner
    */
  def fetchDatasources(owner: User): Future[Seq[DatasourceProject]] = datasourceDao.fetchDatasources(owner)

  /**
    * Creates new datasource and saves it in database
    *
    * @param datasourceData model containing data to create new datasouce
    * @param owner          owner for new datasourceData
    * @return None if there is no workflow by given id or given user is not owner of that workflow, otherwise
    *         Some(new entity's id)
    */
  def save(datasourceData: DatasourceData, owner: User): Future[Option[Long]] =
    projectDao.getById(datasourceData.projectId).flatMap {
      case Some(project) if project.userId == owner.id.toString => workflowDao.fetchProject(datasourceData.workflowId) flatMap {
        case Some(p) if p.id == datasourceData.projectId => datasourceDao.save(datasourceData).map(Some(_))
        case _ => Future.successful(None)
      }
      case _ => Future.successful(None)
    }

  /**
    * Removes relation between datasource and workflow. Relation will be removed only if owner of datasource
    * eq to given user
    *
    * @param datasourceId identity of datasource
    * @param workflowId   identity of workflow
    * @param owner        owner of datasource
    * @return true if link will be removed otherwise false
    */
  def remove(datasourceId: Long, workflowId: Long, owner: User): Future[Boolean] = datasourceDao.getOwnerId(datasourceId) flatMap {
    case Some(userId) if userId == owner.id.toString => datasourceDao.remove(datasourceId, workflowId).map(_ => true)
    case _ => Future.successful(false)
  }
}