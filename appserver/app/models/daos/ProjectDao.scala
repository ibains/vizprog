package models.daos

import javax.inject.Inject

import models.{Project, ProjectUserId, User}
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

class ProjectDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends DAOSlick {
  import driver.api._

  /**
    * Fetchs project by id
    * @param projectId identity of project
    * @return None it there is no project with given id, otherwise some project
    */
  def getById(projectId: Long): Future[Option[ProjectUserId]] = db.run(projects.filter(_.id === projectId).result.headOption).map {
    o => o.map(p => ProjectUserId(p.id, p.name, p.userId))
  }

  def fetchProjects(owner: User): Future[Seq[Project]] = {
    val q = for {
      o <- slickUsers.filter(_.id === owner.id.toString)
      p <- projects.filter(_.userId === o.id)
    } yield (o, p)

    db.run(q.result).map {
      s => s.map {
        case (o, p) => {
          Project(p.id, p.name)
        }
      }
    }
  }

  /**
    * Creates project with given data
    *
    * @param name name of project
    * @param owner user that owns project
    * @return identity of created project
    */
  def create(name: String, owner: User): Future[Long] = db.run(projects returning projects.map(_.id) += DbProject(name, owner.id.toString))
}
