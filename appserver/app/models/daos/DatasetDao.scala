package models.daos

import java.sql.Timestamp
import java.time.Instant
import javax.inject.Inject

import models.{DataSet, DatasetData, User}
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

class DatasetDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends DAOSlick {

  import driver.api._

  /**
    * Removes dataset's relation with given workflow
    *
    * @param datasetId  identity of dataset
    * @param workflowId identity of workflow
    * @return count of removed dataset
    */
  def remove(datasetId: Long, workflowId: Long): Future[Int] = removeWorkflowDatasets(datasetId, workflowId)

  def removeDatasetIfItHasNoLinks(datasetId: Long): Future[Int] = {
    db.run(workflowDataSets.filter(_.dataSetId === datasetId).countDistinct.result).flatMap(count =>
      if (count == 0) db.run(dataSets.filter(_.id === datasetId).delete)
      else Future.successful(0)
    )
  }

  /**
    * Removes dataset's links to workflow
    *
    * @param datasetId identity of dataset
    * @param workflowId identity of workflow
    * @return count of removed links
    */
  private def removeWorkflowDatasets(datasetId: Long, workflowId: Long) =
    db.run(workflowDataSets.filter(wds => wds.dataSetId === datasetId && wds.workflowId === workflowId).delete)

  /**
    * Updates dataset
    *
    * @param datasetId   identity of dataset
    * @param datasetData model that contains data that will be used to update dataset
    * @return true if there is a dataset by given id, otherwise - false
    */
  def update(datasetId: Long, datasetData: DatasetData): Future[Boolean] = {
    db.run(
      dataSets.filter(_.id === datasetId)
        .map(ds => (ds.name, ds.format, ds.location, ds.content))
        .update(datasetData.name, datasetData.format, datasetData.location, datasetData.content)
    ).map(_ > 0)
  }

  /**
    * @param datasetId identity of dataset, which owner needs to be returned
    * @return identity of user which is owner of dataset with given datasetId
    */
  def getOwnerId(datasetId: Long): Future[Option[String]] = {
    val q = for {
      ds <- dataSets.filter(_.id === datasetId)
      p <- projects.filter(_.id === ds.projectId)
    } yield p.userId

    db.run(q.result.headOption)
  }

  /**
    * Retrieves all user's datasets
    *
    * @param owner user whose datasets need to return
    * @return sequence that contains all user's datasets
    */
  def fetchDatasets(owner: User): Future[Seq[DataSet]] = {
    val q = for {
      p <- projects.filter(_.userId === owner.id.toString)
      ds <- dataSets.filter(_.projectId === p.id)
    } yield ds

    db.run(q.result).map(dss => dss.map(
      ds => DataSet(ds.id.get, ds.name, ds.location, ds.format, ds.created.toInstant)))
  }

  /**
    * Creates new dataset with given data and liks with workflow
    *
    * @param datasetData model that contains data to create new dataset
    * @return identity of new dataset
    */
  def save(datasetData: DatasetData): Future[Long] = {
    val dataSet = DbDataSet(None, datasetData.name, datasetData.location, datasetData.format,
      datasetData.projectId, Timestamp from Instant.now, datasetData.content)

    val q = (for {
      ds <- dataSets returning dataSets.map(_.id) += dataSet
      _ <- workflowDataSets += DbWorkflowDataSet(datasetData.workflowId, ds)
    } yield ds).transactionally

    db.run(q)
  }
}
