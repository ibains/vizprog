package models.daos

import java.sql.Timestamp
import java.time.Instant
import javax.inject.Inject

import models._
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

class WorkflowDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends DAOSlick {
  import driver.api._

  /**
    * Fetches projec by given workflow id
    *
    * @param workflowId
    * @return
    */
  def fetchProject(workflowId: Long): Future[Option[ProjectUserId]] = {
    val q = for {
      w <- workflows.filter(_.id === workflowId)
      p <- projects.filter(_.id === w.projectId)
    } yield p

    db.run(q.result.headOption).map(_.map(p => ProjectUserId(p.id, p.name, p.userId)))
  }

  /**
    * Updates workflow by it's id
    *
    * @param id identifier of workflow that needs to be updated
    * @param workflow model that contains data for update
    * @return 1 if there is existence workflow by given id, otherwise 0
    */
  def update(id: Long, workflow: WorkflowCreateData): Future[Int] = {
    val q = for {
      w <- workflows
      if w.id === id
    } yield (w.name, w.description, w.lastModified, w.content)

    db.run(q.update(workflow.name, workflow.description, Timestamp from Instant.now, workflow.content))
  }

  /**
    * Inserts workflow into database
    *
    * @param workflow model containig info to create new db record
    * @return identity of created workflow
    */
  def create(workflow: WorkflowCreateData): Future[Long] = {
    val dbWorkflow = DbWorkflow(workflow.name, workflow.projectId, workflow.description,
      Timestamp from workflow.created, Timestamp from workflow.lastModified, workflow.content)
    db.run(workflows returning workflows.map(_.id) += dbWorkflow)
  }

  /**
    * Fetches all user's workflows
    *
    * @param owner
    * @return
    */
  def fetchWorkflows(owner: User): Future[Seq[Workflow]] = {
    val q = for {
      o <- slickUsers.filter(_.id === owner.id.toString)
      p <- projects.filter(_.userId === o.id)
      w <- workflows.filter(_.projectId === p.id)
    } yield (o, p, w)

    db.run(q.result).map {
      s => s.map {
        case (o, p, w) => {
          val userInfo = UserInfo(o.id, o.email)
          val project = ProjectUser(p.id, p.name, userInfo)
          Workflow(w.id, w.name, project, w.description, w.created toInstant, w.lastModified toInstant, w.content)
        }
      }
    }
  }

  /**
    * Removes workflow by given id
    *
    * @param id identitifier of workflow that needs to be deleted
    * @return true if there is existence workflow by id, otherwise false
    */
  def remove(id: Long): Future[Boolean] = removeDatasets(id)
    .flatMap(_ => removeDataSources(id))
    .flatMap(_ => removeWorkflow(id))
    .map(_ > 0)

  /**
    * Removes workflow by given id
    *
    * @param workflowId identity of workflow that needs to be deleted
    * @return
    */
  private def removeWorkflow(workflowId: Long) = db.run(workflows.filter(w => w.id === workflowId).delete)

  /**
    * Removes liks from datasets to workflow by given workflow's id
    *
    * @param workflowId identity of workflow that links to datasets need to be deleted
    * @return
    */
  private def removeDatasets(workflowId: Long) = db.run(workflowDataSets.filter(_.workflowId === workflowId).delete)

  /**
    * Removes liks from datasources to workflow by given workflow's id
    *
    * @param workflowId identity of workflow that links to datasources need to be deleted
    * @return
    */
  private def removeDataSources(workflowId: Long) = db.run(workflowDataSources.filter(_.workflowId === workflowId).delete)
}