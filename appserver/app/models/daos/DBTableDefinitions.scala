package models.daos

import java.sql.Timestamp

import com.mohiva.play.silhouette.api.LoginInfo
import slick.driver.JdbcProfile
import slick.lifted.ProvenShape.proveShapeOf

trait DBTableDefinitions {
  
  protected val driver: JdbcProfile
  import driver.api._

  case class DBUser (
    id: String,
    firstName: Option[String],
    lastName: Option[String],
    fullName: Option[String],
    email: Option[String],
    avatarURL: Option[String]
  )

  class Users(tag: Tag) extends Table[DBUser](tag, "user") {
    def id = column[String]("id", O.PrimaryKey)
    def firstName = column[Option[String]]("firstName")
    def lastName = column[Option[String]]("lastName")
    def fullName = column[Option[String]]("fullName")
    def email = column[Option[String]]("email")
    def avatarURL = column[Option[String]]("avatarURL")
    def * = (id, firstName, lastName, fullName, email, avatarURL) <> (DBUser.tupled, DBUser.unapply)
  }

  case class DBLoginInfo (
    id: Option[Long],
    providerID: String,
    providerKey: String
  )

  class LoginInfos(tag: Tag) extends Table[DBLoginInfo](tag, "logininfo") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def providerID = column[String]("providerID")
    def providerKey = column[String]("providerKey")
    def * = (id.?, providerID, providerKey) <> (DBLoginInfo.tupled, DBLoginInfo.unapply)
  }

  case class DBUserLoginInfo (
    userID: String,
    loginInfoId: Long
  )

  class UserLoginInfos(tag: Tag) extends Table[DBUserLoginInfo](tag, "userlogininfo") {
    def userId = column[String]("userId")
    def loginInfoId = column[Long]("loginInfoId")
    def * = (userId, loginInfoId) <> (DBUserLoginInfo.tupled, DBUserLoginInfo.unapply)
  }

  case class DBPasswordInfo (
    hasher: String,
    password: String,
    salt: Option[String],
    loginInfoId: Long
  )

  class PasswordInfos(tag: Tag) extends Table[DBPasswordInfo](tag, "passwordinfo") {
    def hasher = column[String]("hasher")
    def password = column[String]("password")
    def salt = column[Option[String]]("salt")
    def loginInfoId = column[Long]("loginInfoId")
    def * = (hasher, password, salt, loginInfoId) <> (DBPasswordInfo.tupled, DBPasswordInfo.unapply)
  }

  case class DBOAuth1Info (
    id: Option[Long],
    token: String,
    secret: String,
    loginInfoId: Long
  )

  class OAuth1Infos(tag: Tag) extends Table[DBOAuth1Info](tag, "oauth1info") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def token = column[String]("token")
    def secret = column[String]("secret")
    def loginInfoId = column[Long]("loginInfoId")
    def * = (id.?, token, secret, loginInfoId) <> (DBOAuth1Info.tupled, DBOAuth1Info.unapply)
  }

  case class DBOAuth2Info (
    id: Option[Long],
    accessToken: String,
    tokenType: Option[String],
    expiresIn: Option[Int],
    refreshToken: Option[String],
    loginInfoId: Long
  )

  class OAuth2Infos(tag: Tag) extends Table[DBOAuth2Info](tag, "oauth2info") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def accessToken = column[String]("accesstoken")
    def tokenType = column[Option[String]]("tokentype")
    def expiresIn = column[Option[Int]]("expiresin")
    def refreshToken = column[Option[String]]("refreshtoken")
    def loginInfoId = column[Long]("logininfoid")
    def * = (id.?, accessToken, tokenType, expiresIn, refreshToken, loginInfoId) <> (DBOAuth2Info.tupled, DBOAuth2Info.unapply)
  }
  
  case class DBOpenIDInfo (
    id: String,
    loginInfoId: Long
  )
  
  class OpenIDInfos(tag: Tag) extends Table[DBOpenIDInfo](tag, "openidinfo") {
    def id = column[String]("id", O.PrimaryKey)
    def loginInfoId = column[Long]("logininfoid")
    def * = (id, loginInfoId) <> (DBOpenIDInfo.tupled, DBOpenIDInfo.unapply)
  }
  
  case class DBOpenIDAttribute (
    id: String,
    key: String,
    value: String
  )
  
  class OpenIDAttributes(tag: Tag) extends Table[DBOpenIDAttribute](tag, "openidattributes") {
    def id = column[String]("id")
    def key = column[String]("key")
    def value = column[String]("value")
    def * = (id, key, value) <> (DBOpenIDAttribute.tupled, DBOpenIDAttribute.unapply)
  }

  case class DbWorkflow (
    name: String,
    projectId: Long,
    description: Option[String],
    created: Timestamp,
    lastModified: Timestamp,
    content: String,
    id: Long = 0
  )

  class Workflows(tag: Tag) extends Table[DbWorkflow](tag, "workflow") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def projectId = column[Long]("project_id")
    def description = column[Option[String]]("description")
    def created = column[Timestamp]("created")
    def lastModified = column[Timestamp]("last_modified")
    def content = column[String]("content")
    def * = (name, projectId, description, created, lastModified, content, id) <> (DbWorkflow.tupled, DbWorkflow.unapply)
    def project = foreignKey("project_id", projectId, projects)(_.id)
  }

  case class DbDataSource(
    id: Option[Long],
    name: String,
    location: String,
    format: String,
    projectId: Long,
    created: Timestamp,
    content: String)

  case class DbWorkflowDataSource(
    workflowId: Long,
    dataSourceId: Long
  )

  class WorkflowDataSource(tag: Tag) extends Table[DbWorkflowDataSource](tag, "workflow_datasource") {
    def workflowId = column[Long]("workflow_id")
    def dataSourceId = column[Long]("datasource_id")
    def * = (workflowId, dataSourceId) <> (DbWorkflowDataSource.tupled, DbWorkflowDataSource.unapply)
  }

  class DataSources(tag: Tag) extends Table[DbDataSource](tag, "datasource") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def location = column[String]("location")
    def format = column[String]("format")
    def created = column[Timestamp]("created")
    def projectId = column[Long]("project_id")
    def content = column[String]("content")
    def * = (id.?, name, location, format, projectId, created, content) <> (DbDataSource.tupled, DbDataSource.unapply)
  }

  case class DbDataSet(
    id: Option[Long],
    name: String,
    location: String,
    format: String,
    projectId: Long,
    created: Timestamp,
    content: String)

  case class DbWorkflowDataSet(
    workflowId: Long,
    datasetId: Long)

  class WorkflowDataSet(tag: Tag) extends Table[DbWorkflowDataSet](tag, "workflow_dataset") {
    def workflowId = column[Long]("workflow_id")
    def dataSetId = column[Long]("dataset_id")
    def * = (workflowId, dataSetId) <> (DbWorkflowDataSet.tupled, DbWorkflowDataSet.unapply)
  }

  class DataSets(tag: Tag) extends Table[DbDataSet](tag, "dataset") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def location = column[String]("location")
    def format = column[String]("format")
    def created = column[Timestamp]("created")
    def projectId = column[Long]("project_id")
    def content = column[String]("content")
    def * = (id.?, name, location, format, projectId, created, content) <> (DbDataSet.tupled, DbDataSet.unapply)
  }

  case class DbProject(name: String, userId: String, id: Long = 0L)

  class Projects(tag: Tag) extends Table[DbProject](tag, "project") {
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name")
    def userId = column[String]("user_id")
    def * = (name, userId, id) <> (DbProject.tupled, DbProject.unapply)
  }

  // table query definitions
  val projects = TableQuery[Projects]
  val dataSources = TableQuery[DataSources]
  val dataSets = TableQuery[DataSets]
  val slickUsers = TableQuery[Users]
  val slickLoginInfos = TableQuery[LoginInfos]
  val slickUserLoginInfos = TableQuery[UserLoginInfos]
  val slickPasswordInfos = TableQuery[PasswordInfos]
  val slickOAuth1Infos = TableQuery[OAuth1Infos]
  val slickOAuth2Infos = TableQuery[OAuth2Infos]
  val slickOpenIDInfos = TableQuery[OpenIDInfos]
  val slickOpenIDAttributes = TableQuery[OpenIDAttributes]
  val workflows = TableQuery[Workflows]
  val workflowDataSources = TableQuery[WorkflowDataSource]
  val workflowDataSets = TableQuery[WorkflowDataSet]

  // queries used in multiple places
  def loginInfoQuery(loginInfo: LoginInfo) = 
    slickLoginInfos.filter(dbLoginInfo => dbLoginInfo.providerID === loginInfo.providerID && dbLoginInfo.providerKey === loginInfo.providerKey)
}
