package models.daos

import java.sql.Timestamp
import java.time.Instant
import javax.inject.Inject

import models._
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

class DatasourceDao @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) extends DAOSlick {

  import driver.api._

  /**
    * Updates datasource
    *
    * @param datasourceId   identity of datasource
    * @param datasourceData model that contains data that will be used to update datasource
    * @return true if there is a datasource by given id, otherwise - false
    */
  def update(datasourceId: Long, datasourceData: DatasourceData): Future[Boolean] = {
    db.run(
      dataSources.filter(_.id === datasourceId)
        .map(ds => (ds.name, ds.format, ds.location, ds.content))
        .update(datasourceData.name, datasourceData.format, datasourceData.location, datasourceData.content)
    ).map(_ > 0)
  }

  /**
    * @param datasourceId identity of datasource, which owner needs to be returned
    * @return identity of user which is owner of datasource with given datasourceId
    */
  def getOwnerId(datasourceId: Long): Future[Option[String]] = {
    val q = for {
      ds <- dataSources.filter(_.id === datasourceId)
      p <- projects.filter(_.id === ds.projectId)
    } yield p.userId

    db.run(q.result.headOption)
  }

  /**
    * Retrieves all user's datasources
    *
    * @param owner user whose datasources need to return
    * @return sequence that contains all user's datasources
    */
  def fetchDatasources(owner: User): Future[Seq[DatasourceProject]] = {
    val q = for {
      p <- projects.filter(_.userId === owner.id.toString)
      ds <- dataSources.filter(_.projectId === p.id)
      u <- slickUsers.filter(_.id === p.userId)
    } yield (u, p, ds)

    db.run(q.result).map(dss => dss.map {
      case (u, p, ds) => DatasourceProject(ds.id.get, ds.name, ds.location, ds.format, ds.created.toInstant,
        ProjectUser(p.id, p.name, UserInfo(u.id, u.email)))
    })
  }

  /**
    * Creates new datasource with given data and links with workflow
    *
    * @param datasourceData model that contains data to create new datasource
    * @return identity of new datasource
    */
  def save(datasourceData: DatasourceData): Future[Long] = {
    val dataSource = DbDataSource(None, datasourceData.name, datasourceData.location, datasourceData.format,
      datasourceData.projectId, Timestamp from Instant.now, datasourceData.content)

    val q = (for {
      ds <- dataSources returning dataSources.map(_.id) += dataSource
      _ <- workflowDataSources += DbWorkflowDataSource(datasourceData.workflowId, ds)
    } yield ds).transactionally

    db.run(q)
  }

  /**
    * Removes datasource's relation with given workflow
    *
    * @param datasourceId identity of datasource
    * @param workflowId   identity of workflow
    * @return count of removed datasources
    */
  def remove(datasourceId: Long, workflowId: Long): Future[Int] = removeWorkflowDatasources(datasourceId, workflowId)

  def removeDatasourceIfItHasNoLinks(datasourceId: Long): Future[Int] = {
    db.run(workflowDataSources.filter(_.dataSourceId === datasourceId).countDistinct.result).flatMap(count =>
      if (count == 0) db.run(dataSources.filter(_.id === datasourceId).delete)
      else Future.successful(0)
    )
  }

  /**
    * Removes datasource's links to workflows
    *
    * @param datasourceId identity of datasource
    * @param workflowId   identity of workflow
    * @return count of removed links
    */
  private def removeWorkflowDatasources(datasourceId: Long, workflowId: Long) =
    db.run(workflowDataSources.filter(wds => wds.dataSourceId === datasourceId && wds.workflowId === workflowId).delete)
}
