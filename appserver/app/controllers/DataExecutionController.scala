package controllers

import java.util.UUID
import javax.inject.Inject
import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models._
import models.services.WorkflowService
import play.api.i18n.MessagesApi
import play.api.mvc._
import scala.concurrent._
import ExecutionContext.Implicits.global

import vizprog.workflow.WorkflowExecutionManager

class DataExecutionController @Inject()(val messagesApi: MessagesApi,
                                        val env: Environment[User, CookieAuthenticator],
                                        workflowService: WorkflowService) extends Silhouette[User, CookieAuthenticator]
{

  /*************************************************************************************
    *
    * Setup the environment in workflow manager for AWS/Vagrant
    *
    ***********************************************************************************/

  val wm  = new WorkflowExecutionManager
  CodegenInitializer.setupEnvironment(wm)


  /*************************************************************************************
    *
    * Public API to take care of miscellaneous functions such as uploading files
    *
    * Every function has an internal counterpart without the HTTP wrapping
    *
    ***********************************************************************************/

  //
  // This uploads a file with the given name and content
  //
  //

  protected def saveFileInternal(name: String, content: String) : Future[String] = {
    val p = Promise[String]()
    Future {
      try   { p.success(wm.exec.saveOneFile(name, content)) }
      catch { case e: Exception =>  p.failure(e) }
    }
    p.future
  }

  protected def getFileName(userId: UUID, fileName: String) : String = {
    userId + "/data/" + fileName
  }

  def saveFile(name: String) = SecuredAction.async(BodyParsers.parse.raw) { implicit request =>
    val fileName = getFileName(request.identity.id, name)
    saveFileInternal(fileName, request.body.toString())
      .map(res => Ok(res))
      .recover { case e: Exception => InternalServerError(e.getMessage) }
  }
}