package controllers


import javax.inject.Inject

import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models._
import models.services.WorkflowService
import play.api._
import play.api.i18n.MessagesApi
import play.api.mvc._
import play.api.libs.json._

import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.{Failure, Success}


// ONLY CLASSES TO USE
import vizprog.workflow.WorkflowExecutionManager

// EXECUTION MODE for wm.execute
//
// CGNone                = 0  // invalid
// CGSparkSubmit         = 1
// CGJobServerBatch      = 2
// CGInteractiveFunction = 3


class WorkflowExecutionController @Inject()(val messagesApi: MessagesApi,
                                            val env: Environment[User, CookieAuthenticator],
                                            workflowService: WorkflowService) extends Silhouette[User, CookieAuthenticator] {

  /** ***********************************************************************************
    *
    * Setup the environment in workflow manager for AWS/Vagrant
    *
    * **********************************************************************************/

  val wm = new WorkflowExecutionManager
  val log = Logger(classOf[WorkflowExecutionController])
  CodegenInitializer.setupEnvironment(wm)

  def submitRunWorkflow(id: String) = Action.async(parse.json) { request =>

    log.info(s"Workflow $id execution request received")

    val workflowBody         = Json.stringify(request.body)
    val workflowId  : String = wm.getId(workflowBody)
    val workflowMode: String = wm.getMode(workflowBody)
    val mode                 = if (workflowMode == "deploy") 4 else 1
    val futureJobId          = runWorkflowInternal("dummyProject", "john", workflowBody, mode)

    workflowMode match {

      case "deploy" =>
        futureJobId.map(
          jobStatus => {
            log.info(s"Deploy returned: $jobStatus")
            val (status, json) = jobStatus
            Ok(json.get)
          }
        ).recover { case e: Throwable =>
          log.error("Failed to execute job.", e)
          InternalServerError(e.getMessage)
        }

      case _ =>
        futureJobId.flatMap(
          jobStatus => {
            val (jobId, sessionId) = jobStatus
            val futureResult = getWorkflowResultInternal(workflowId, jobId, sessionId)
            val futureStatus = getWorkflowStatusInternal(workflowId, jobId)

            futureStatus.map(i => log.debug("Workflow status: " + i))
            futureResult
              .map(s => Ok(s))
              .recover { case e: Throwable =>
                                  log.error("Failed to execute job.", e)
                                  InternalServerError(e.getMessage)
              }
          }
        ).recover { case e: Throwable =>
          log.error("Failed to execute job.", e)
          InternalServerError(e.getMessage)
        }
    }
  }

  /** ***********************************************************************************
    *
    * Public API to submit jobs to execution service using workflowExecutionManager
    *
    * Every function has an internal counterpart without the HTTP wrapping
    *
    * **********************************************************************************/

  //
  // This submits a workflow run to the execution service
  //
  // @param id the id for the workflow in the request body
  //

  protected def runWorkflowInternal(projectId: String,
                                    userId: String,
                                    workflowJson: String,
                                    mode: Int): Future[(String, Option[String])] = {
    val p = Promise[(String, Option[String])]()
    Future {
      val workflowId = wm.getId(workflowJson)

      log.info("app_server: Running workflow id = " + workflowId)

      val res = wm.execute(projectId, userId, workflowId, workflowJson, mode)
      log.info("app_server: execute returned from json_to_spark = " + workflowId)
      res match {
        case Success(result) =>
          log.info("app_server: Success: " + workflowId)
          p.success(result)
        case Failure(ex) =>
          val errMessage = ex.getMessage
          log.error("app_server: Failure: " + errMessage)
          p.failure(new Exception(errMessage))
          p.failure(ex)
      }
    }
    p.future
  }

  def getWorkflowResult(workflowId: String, jobId: String) = Action.async {
    log.info("app_server: workflow results request: " + workflowId + ", jobId: " + jobId)
    getWorkflowResultInternal(workflowId, jobId, None)
      .map(res => Ok(res))
      .recover { case e: Throwable =>
        log.info("Failed to get the job result.", e)
        InternalServerError(e.getMessage)
      }
  }

  //
  // This async polls the execution service for results and updates status in the meantime
  //
  protected def getWorkflowResultInternal(workflowId: String, jobId: String, sessionId: Option[String]): Future[String] = {
    log.info("app_server: interim results request: " + workflowId + ", jobId: " + jobId)
    wm.getInterimResults(jobId, sessionId, workflowId)
  }


  //
  // This can be used to get workflow status after getWorkflowResult is called
  //
  // otherwise the status will never be updated
  //

  def getWorkflowStatus(workflowId: String, jobId: String) = Action.async {
    getWorkflowStatusInternal(workflowId, jobId)
      .map(res => Ok(res))
      .recover { case e: Exception => InternalServerError(e.getMessage) }
  }

  def getFunctionSpecs = Action {
    Ok(wm.getFunctionSpecification)
  }

  def getNodeSpecs = Action {
    Ok(wm.getNodeSpecification)
  }

  def clearCache = Action {
    Ok(wm.clearCache)
  }

  //
  // This returns the JSON specs of functions to be used in cleanse
  // The returned JSON is to be used as a generator for functions
  //

  /** ***********************************************************************************
    *
    * These are testing functions and show how to use the API above as well
    *
    * **********************************************************************************/

  //
  // Testing: This generates the spark code for an existing workflow
  //
  // @param path the relative S3 path or absolute vagrant path depending on environment
  //

  def getWorkflowCode(path: String, mode: Int) = Action {
    val fileContent = CodegenInitializer.readOneWorkflow(wm, path)
    val workflowId = wm.getId(fileContent)
    val (success, generatedCode) = wm.getGeneratedCode(workflowId, fileContent, mode)
    if (success) Ok(generatedCode)
    else BadRequest(generatedCode)
  }

  def runWorkflowFromPath(path: String, mode: Int) = Action.async {

    val fileContent = CodegenInitializer.readOneWorkflow(wm, path)
    val workflowId: String = wm.getId(fileContent)

    val futureJobId = runWorkflowInternal("example", "john", fileContent, mode)

    futureJobId.flatMap(
      jobId => {
        val futureResult = getWorkflowResultInternal(workflowId, jobId._1, jobId._2)
        val futureStatus = getWorkflowStatusInternal(workflowId, jobId._1)

        futureStatus.map(i => log.debug("Workflow status: " + i))
        futureResult
          .map(s => Ok(s))
          .recover { case e: Throwable =>
            log.error("Failed to execute the job", e)
            InternalServerError(e.getMessage)
          }
      }
    ).recover { case e: Throwable =>
      log.error("Failed to execute the job", e)
      InternalServerError(e.getMessage)
    }
  }

  //
  // Testing: This executes an existing workflow without going through the UI
  //
  // @param path the relative S3 path or absolute vagrant path depending on environment
  //

  protected def getWorkflowStatusInternal(workflowId: String, jobId: String): Future[String] = {
    val p = Promise[String]()
    Future {
      try {
        val status = wm.getStatus(workflowId, jobId)
        log.debug("Getting status for " + jobId + " status: " + status)
        p.success(status)
      } catch {
        case e: Exception => p.failure(e)
      }
    }
    p.future
  }

}
