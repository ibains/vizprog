package controllers

import java.io.File
import javax.inject.Inject

import com.mohiva.play.silhouette.api.{Silhouette, Environment}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import conf.ApplicationConfig
import models.User
import play.api._
import play.api.i18n.MessagesApi
import play.api.mvc._
import play.api.libs.json._

import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.io.Source._
import scala.util.{Success, Failure}

class LogsController @Inject()(val messagesApi: MessagesApi,
                               val env: Environment[User, CookieAuthenticator])
  extends Silhouette[User, CookieAuthenticator] {

  // File functions
  protected def hasFile            (path: String)  : Boolean          = new File(path) exists
  protected def canReadFile        (path: String)  : Boolean          = new File(path) canRead
  protected def readFile           (path: String)  : String           = fromFile(path).getLines mkString "\n"

  protected def readLogFile(fileName: String): Future[String] = {
    val p = Promise[String]()
    Future {
      if (fileName == null) {
        p.failure(new Exception("Unknown Log requested"))
      } else {
        Logger.info("Running this log: " + fileName)

        if (!hasFile(fileName)) {
          Logger.error("Missing log: " + fileName)
          p.failure(new Exception("Log file is missing: " + fileName))
        } else {
          if (!canReadFile(fileName)) {
            Logger.error("No permission to read log: " + fileName)
            p.failure(new Exception("Log exists, but unable to read: " + fileName))
          } else {
            p.success(readFile(fileName))
          }
        }
      }
    }
    p.future
  }

  def getLogs(logName: String) = Action.async {
    val fileName = logName match {
      case "appserver" => ApplicationConfig.appServerLogs
      case "webui"     => ApplicationConfig.webUILogs
      case _           => null
    }
    readLogFile(fileName)
      .map(res => Ok(res))
      .recover { case e: Exception => InternalServerError(e.getMessage)
      }
  }
}
