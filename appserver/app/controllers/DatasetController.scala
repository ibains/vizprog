package controllers

import javax.inject.{Inject, Singleton}

import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.services.DatasetService
import models.{DataSet, DatasetData, User}
import play.api.i18n.MessagesApi
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.{JsError, Json}
import play.api.mvc.BodyParsers

import scala.concurrent.Future

@Singleton
class DatasetController @Inject()(val messagesApi: MessagesApi,
                                  val env: Environment[User, CookieAuthenticator],
                                  datasetService: DatasetService) extends Silhouette[User, CookieAuthenticator] {
  private implicit val datasetDataReads = Json.reads[DatasetData]
  private implicit val datasetWrites = Json.writes[DataSet]

  /**
    * Saves dataset, sets current user as owner and links it with given workflow
    *
    * TODO kraken no tests
    *
    * @return json with status and dataset's id
    */
  def save = SecuredAction.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[DatasetData].fold(
      errors => Future.successful(BadRequest(Json.obj("success" -> false, "message" -> JsError.toJson(errors)))),
      dataSetData => datasetService.save(dataSetData, request.identity).map {
        case Some(id) => Ok(Json.obj("success" -> true, "id" -> id))
        case _ => BadRequest
      }
    )
  }

  /**
    * Fetches current user's datasets
    *
    * TODO kraken no tests
    *
    * @return List of current user's datasets
    */
  def fetch = SecuredAction.async { implicit request =>
    datasetService.fetchDatasets(request.identity).map(dss => Ok(Json.toJson(dss)))
  }

  /**
    * Updates dataset data. Before updating it checks whether current user is owner of dataset
    *
    * TODO kraken no tests
    *
    * @param datasetId identity of dataset
    * @return Ok if exists a dataset with given id and current user is owner of that dataset, otherwise BadRequest
    */
  def update(datasetId: Long) = SecuredAction.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[DatasetData].fold(
      errors => Future.successful(BadRequest),
      dataSetData => datasetService.update(datasetId, dataSetData, request.identity).map(if (_) Ok else BadRequest)
    )
  }

  /**
    * Removes relation between dataset and workflow
    *
    * @param datasetId  identity of dataset
    * @param workflowId identity of workflow
    * @return BadRequest if there is no relation between given dataset and workflow or
    *         if current user is not owner of it, otherwise Ok
    */
  def delete(datasetId: Long, workflowId: Long) = SecuredAction.async { implicit request =>
    datasetService.remove(datasetId, workflowId, request.identity).map(if (_) Ok else BadRequest)
  }
}