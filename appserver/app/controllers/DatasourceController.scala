package controllers

import javax.inject.{Inject, Singleton}

import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models._
import models.services.DatasourceService
import play.api.i18n.MessagesApi
import play.api.libs.json.{JsError, Json, Writes}
import play.api.mvc.BodyParsers
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

@Singleton
class DatasourceController @Inject()(val messagesApi: MessagesApi,
                                     val env: Environment[User, CookieAuthenticator],
                                     datasourceService: DatasourceService) extends Silhouette[User, CookieAuthenticator] {
  private implicit val datasourceDataReads = Json.reads[DatasourceData]
  private implicit val datasourceWrites = Json.writes[Datasource]

  private implicit val datasourceProjectWrites = new Writes[DatasourceProject] {
    def writes(datasourceProject: DatasourceProject) = Json.obj(
      "id" -> datasourceProject.id,
      "name" -> datasourceProject.name,
      "project" -> Json.obj(
        "id" -> datasourceProject.project.id,
        "name" -> datasourceProject.project.name,
        "user" -> Json.obj(
          "id" -> datasourceProject.project.user.id,
          "email" -> datasourceProject.project.user.email
        )
      ),
      "location" -> datasourceProject.location,
      "format" -> datasourceProject.format,
      "created" -> datasourceProject.created
    )
  }

  /**
    * Saves datasource, sets current user as owner and links it with given workflow
    *
    * TODO kraken no tests
    *
    * @return json with status and datasource's id
    */
  def save = SecuredAction.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[DatasourceData].fold(
      errors => Future.successful(BadRequest(Json.obj("success" -> false, "message" -> JsError.toJson(errors)))),
      datasourceData => datasourceService.save(datasourceData, request.identity).map {
          case Some(id) => Ok(Json.obj("success" -> true, "id" -> id))
          case _ => BadRequest
        }
    )
  }

  /**
    * Fetches current user's datasources
    *
    * TODO kraken no tests
    *
    * @return List of current user's datasources
    */
  def fetch = SecuredAction.async { implicit request =>
    datasourceService.fetchDatasources(request.identity).map(dss => Ok(Json.toJson(dss)))
  }

  /**
    * Updates datasource data. Before updating it checks whether current user is owner of datasource
    *
    * TODO kraken no tests
    *
    * @param datasourceId identity of datasource
    * @return Ok if exists a datasource with given id and current user is owner of that datasource, otherwise BadRequest
    */
  def update(datasourceId: Long) = SecuredAction.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[DatasourceData].fold(
      errors => Future.successful(BadRequest),
      datasourceData => datasourceService.update(datasourceId, datasourceData, request.identity).map(if (_) Ok else BadRequest)
    )
  }

  /**
    * Removes relation between datasource and workflow
    *
    * @param datasourceId identity of datasource
    * @param workflowId   identity of workflow
    * @return BadRequest if there is no relation between given datasource and workflow or
    *         if current user is not owner of it, otherwise Ok
    */
  def delete(datasourceId: Long, workflowId: Long) = SecuredAction.async { implicit request =>
    datasourceService.remove(datasourceId, workflowId, request.identity).map(if (_) Ok else BadRequest)
  }
}