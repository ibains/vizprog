package controllers

import javax.inject.Inject
import javax.inject.Singleton

import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.{User, Project}
import models.services.ProjectService
import play.api.i18n.MessagesApi
import play.api.libs.json.{JsError, Json, Writes}
import play.api.mvc.BodyParsers
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

@Singleton
class ProjectController @Inject()(val messagesApi: MessagesApi,
                                  val env: Environment[User, CookieAuthenticator],
                                  projectService: ProjectService) extends Silhouette[User, CookieAuthenticator] {
  private implicit val projectDataReads = Json.reads[ProjectData]

  private implicit val projectWrites = new Writes[Project] {
    def writes(project: Project) = Json.obj(
      "id" -> project.id,
      "name" -> project.name
    )
  }

  case class ProjectData(name: String)

  /**
    * Creates new project on behalf current user
    */
  def create = SecuredAction.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[ProjectData].fold(
      errors => Future.successful(BadRequest(Json.obj("success" -> false, "message" -> JsError.toJson(errors)))),
      projectData => projectService.create(projectData.name, request.identity)
        .map(projectId => Ok(Json.obj("success" -> true, "id" -> projectId)))
    )
  }

  /**
   * @return list of current user's projects as json
   */
  def fetch = SecuredAction.async { implicit request =>
    projectService.getProjects(request.identity) map {
      result => Ok(Json.toJson(result))
    }
  }
}
