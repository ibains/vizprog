package controllers

import conf.ApplicationConfig
import play.api.Logger
import vizprog.execution.{VagrantExecutionEnvironment, AWSExecutionEnvironment}
import vizprog.workflow.WorkflowExecutionManager

import scala.collection.mutable.Map

object InitializationCache {
  var workflows   : Map[String, String] = null
  var datasets    : Map[String, String] = null
  var datasources : Map[String, String] = null
}

// This class has no state, it is functional helper to WorkflowController

object CodegenInitializer {

  def playLogger(logType: String, message: String) : Unit = {
    logType match {
      case "info"  => Logger.info(message)
      case "warn"  => Logger.warn(message)
      case "error" => Logger.error(message)
      case _       => Logger.debug(message)
    }
  }

  def setupEnvironment(wm : WorkflowExecutionManager) : Unit = {

    ApplicationConfig.deployMode match {
      case "aws"     =>
        wm.exec = new AWSExecutionEnvironment()
        wm.exec.intermediateRoot    = "/tmp/"
        wm.exec.bucketName          = ApplicationConfig.awsBucketName
        wm.exec.executionServiceURL = ApplicationConfig.awsExecutionServiceUrl
        wm.exec.exampleDataUrl      = ApplicationConfig.awsExampleDataUrl
        wm.exec.exampleWorkflowUrl  = ApplicationConfig.awsExampleWorkflowUrl
        wm.exec.exampleDatasetUrl   = ApplicationConfig.awsExampleDatasetUrl
        wm.exec.exampleDatasourceURL= ApplicationConfig.awsExampleDatasourceUrl
        wm.exec.userDatasetUrl      = ApplicationConfig.awsUserDatasetUrl

      case "vagrant" =>
        wm.exec = new VagrantExecutionEnvironment()
        wm.exec.intermediateRoot    = "/vagrant/tmp/"
        wm.exec.executionServiceURL = ApplicationConfig.vagrantExecutionServiceUrl
        wm.exec.exampleDataUrl      = ApplicationConfig.vagrantExampleDataUrl
        wm.exec.exampleWorkflowUrl  = ApplicationConfig.vagrantExampleWorkflowUrl
        wm.exec.exampleDatasetUrl   = ApplicationConfig.vagrantExampleDatasetUrl
        wm.exec.userDatasetUrl      = ApplicationConfig.vagrantUserDatasetUrl
      case _         =>
        Logger.error("Unable to initialize execution environment, check application.conf")
    }
  }

  def readExampleWorkflows(wm : WorkflowExecutionManager) : Unit = {
    if (InitializationCache.workflows == null)
      InitializationCache.workflows = wm.exec.getWorkflows()
  }

  def readExampleDataSources(wm : WorkflowExecutionManager): Unit = {
    if (InitializationCache.datasources == null)
      InitializationCache.datasources = wm.exec.getDataSources()
  }

  def readOneWorkflow(wm : WorkflowExecutionManager, path: String): String = {
    wm.exec.readOneWorkflow(path)
  }
}


