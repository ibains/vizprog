package controllers

import javax.inject.Inject

import com.mohiva.play.silhouette.api.{Environment, Silhouette}
import com.mohiva.play.silhouette.impl.authenticators.CookieAuthenticator
import models.{User, Workflow, WorkflowCreateData}
import models.services.WorkflowService
import play.api.i18n.MessagesApi
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future


class WorkflowController @Inject()(val messagesApi: MessagesApi,
                                   val env: Environment[User, CookieAuthenticator],
                                   workflowService: WorkflowService) extends Silhouette[User, CookieAuthenticator] {

  private implicit val userWrites = new Writes[User] {
    def writes(user: User) = Json.obj(
      "id" -> user.id.toString,
      "email" -> user.email
    )
  }
  private implicit val workflowWrites = new Writes[Workflow] {
    def writes(workflow: Workflow) = Json.obj(
      "id" -> workflow.id,
      "name" -> workflow.name,
      "project" -> Json.obj(
        "id" -> workflow.project.id,
        "name" -> workflow.project.name,
        "user" -> Json.obj(
          "id" -> workflow.project.user.id,
          "email" -> workflow.project.user.email
        )
      ),
      "description" -> workflow.description,
      "created" -> workflow.created,
      "lastModified" -> workflow.lastModified,
      "content" -> Json.parse(workflow.content)
    )
  }

  /**
    * @return list of current user's woorkflows as json
    */
  def fetch = SecuredAction.async { implicit request =>
    workflowService.getWorkflows(request.identity) map {
      result => Ok(Json.toJson(result))
    }
  }

  implicit val workflowDataReads = Json.reads[WorkflowCreateData]

  /**
    * Creates workflow and sets current user as owner
    *
    * @return json with status and workflow's id
    */
  def create = SecuredAction.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[WorkflowCreateData].fold(
      errors => Future.successful(BadRequest(Json.obj("success" -> false, "message" -> JsError.toJson(errors)))),
      workflow => workflowService.create(workflow, request.identity).map {
        case Some(id) => Ok(Json.obj("success" -> true, "id" -> id))
        case _ => BadRequest
      }
    )
  }

  /**
    * Removes workflow by id
    *
    * @param workflowId identity of workflow
    * @return BadRequest if there is no workflow by given id or if current user is not owner of it, otherwise Ok
    */
   def remove(workflowId: Long) = SecuredAction.async { implicit request =>
     workflowService.remove(workflowId, request.identity).map {
       if (_) Ok(Json.obj("success" -> true))
       else BadRequest
     }
   }

  /**
    * Updates current user's workflow by it's id
    *
    * @param workflowId identity of workflow
    * @return BadRequest if there is no workflow by given id or if current user is not owner of it, otherwise Ok
    */
  def update(workflowId: Long) = SecuredAction.async(BodyParsers.parse.json) { implicit request =>
    request.body.validate[WorkflowCreateData].fold(
      errors => Future.successful(BadRequest(Json.obj("success" -> false, "message" -> JsError.toJson(errors)))),
      workflowData => workflowService.update(workflowId, workflowData, request.identity).map {
        if (_) Ok(Json.obj("success" -> true))
        else BadRequest
      }
    )
  }
}
