package utils

import javax.inject.Inject

import play.api.Configuration

class SiteUrlProvider @Inject()(configuration: Configuration) {
  val siteUrl = configuration.getString("SITE_URL").get
}
