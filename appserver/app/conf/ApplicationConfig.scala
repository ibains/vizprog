package conf

import com.typesafe.config.ConfigFactory

object ApplicationConfig {

  lazy val config = ConfigFactory.load()

  /* logs */

  lazy val appServerLogs            = config.getString("app.log.appserver.location")
  lazy val webUILogs                = config.getString("app.log.webui.location")

  /* locations on S3 */

  lazy val deployMode               = config.getString("app.deploy.mode")

  lazy val awsBucketName            = config.getString("app.aws.bucket.name")
  lazy val awsExampleDataUrl        = config.getString("app.aws.example.data.url")
  lazy val awsExampleWorkflowUrl    = config.getString("app.aws.example.workflow.url")
  lazy val awsExampleDatasetUrl     = config.getString("app.aws.example.dataset.url")
  lazy val awsExampleDatasourceUrl  = config.getString("app.aws.example.datasource.url")

  lazy val awsUserDatasetUrl        = config.getString("app.aws.user.dataset.url")
  lazy val awsExecutionServiceUrl   = config.getString("app.executionService.url")


  /* locations on Vagrant */

  lazy val vagrantExampleDataUrl        = config.getString("app.vagrant.example.data.url")
  lazy val vagrantExampleWorkflowUrl    = config.getString("app.vagrant.example.workflow.url")
  lazy val vagrantExampleDatasetUrl     = config.getString("app.vagrant.example.dataset.url")
  lazy val vagrantExampleDatasourceUrl  = config.getString("app.vagrant.example.datasource.url")

  lazy val vagrantUserDatasetUrl        = config.getString("app.vagrant.user.dataset.url")
  lazy val vagrantExecutionServiceUrl   = config.getString("app.executionService.url")
}
