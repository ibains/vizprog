name := "metadata"
version := "1.0.0-SNAPSHOT"
scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.michaelpollmeier" %% "gremlin-scala" % "3.2.1.0",
  "org.apache.tinkerpop" % "neo4j-gremlin" % "3.2.1" exclude("com.github.jeremyh", "jBCrypt"), // travis can't find jBCrypt...
  "org.neo4j" % "neo4j-tinkerpop-api-impl" % "0.4-3.0.3",
  "org.slf4j" % "slf4j-simple" % "1.7.12", // "org.neo4j.driver" % "neo4j-java-driver" % "1.0.4"
  "org.scalatest" %% "scalatest" % "2.2.5" % "test"
)
resolvers += Resolver.mavenLocal

