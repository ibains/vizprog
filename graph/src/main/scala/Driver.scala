import Metadata.Printer
import gremlin.scala.ScalaGraph
import org.apache.tinkerpop.gremlin.neo4j.structure.Neo4jGraph
import Metadata._

object MetadataGraph {
  protected var graph : Option[ScalaGraph] = None
  protected val dbPath = "target/dev_graph"

  def g = graph

  def init = {
    graph = Some(Neo4jGraph.open(dbPath))
  }

  def reset = {
    FileUtils.removeAll(dbPath)
    init
  }
}

object Driver {

  var gg: ScalaGraph = null

  def initialize : Boolean = {

    MetadataGraph.reset
    MetadataGraph.g match {
      case Some(graph) => gg = graph
      case None => return false
    }

    Graph.set(gg)
    val scenarios = new MetadataScenarios()

    scenarios.NewCompanyCreation()
    scenarios.AddUsersToCompany()
    scenarios.AddGroupAndProjects()
    scenarios.AddWorkflowsAndDatasets()

    true
  }

  def close : Unit = {
    gg.close()
  }

  def main(args: Array[String]): Unit = {

    val success = initialize

    if (success) {
      val p = new Printer()
      println(p.printGraph(gg))
    } else {
      println("couldn't open graph")
    }
    close
  }
}
