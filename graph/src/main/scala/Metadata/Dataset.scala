package Metadata

import gremlin.scala._

object Dataset {

  // ADDERS

  def + (name: String, schemaJson: String, statsJson: String, diskSizeGb: Long) : Dataset = {
    new Dataset(Graph.get + DB_Dataset(None, name, schemaJson, statsJson, diskSizeGb, Util.tt))
  }

}

class Dataset(v: Vertex) extends SimpleEntity(v) {
  val D = v.toCC[DB_Dataset]
}
