package Metadata

import gremlin.scala._

object Project {

  // ADDERS

  def + (name: String, description: String) : Project = {
    new Project(Graph.get + DB_Project(None, name, description, Util.tt))
  }

  def get(companyName: String, name: String) : Project = {
    val company = Company.get(companyName)
    val v = company.V.
                      out(DB_EDGE.Member).
                      hasLabel(DB_LABELS.project).
                      has(DB_KEY.name, name).toList()
    if (v == null || v.length < 1)
      return null
    new Project(v.head)
  }
}


class Project(v: Vertex) extends GroupEntity(v)  {
  val D = v.toCC[DB_Project]

  def --> (workflow: Workflow)             : Unit = { addMember(V, workflow.V   ) }
  def --> (dataset: Dataset)               : Unit = { addMember(V, dataset.V    ) }
  def --> (datasource: ExternalDataSource) : Unit = { addMember(V, datasource.V ) }

}

