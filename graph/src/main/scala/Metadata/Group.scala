package Metadata

import gremlin.scala._

object Group {

  // ADDERS

  def + (name: String, description: String) : Group = {
    new Group(Graph.get + DB_Group(None, name, description, Util.tt))
  }

  // GETTERS - ALL

  def getAll() : List[Group] = {
    Graph.get.V.hasLabel(DB_LABELS.group).toList.map(x => new Group(x))
  }

  // GETTERS - FILTERED

  def get(name: String) : Group = {
    new Group(Graph.get.V.hasLabel(DB_LABELS.group).has(DB_KEY.name, name).head())
  }
}

class Group(v: Vertex) extends GroupEntity(v) {
  val D = v.toCC[DB_Group]

  def --> (person: Person)  : Unit = { addMember(V, person.V ) }
}