package Metadata

import gremlin.scala._

object addExternalDataSource {

  // ADDERS

  def + (location: String, retrievalJsonNode: String) : ExternalDataSource = {
    new ExternalDataSource(Graph.get + DB_ExternalDataSource(None, location, retrievalJsonNode, Util.tt))
  }

}

class ExternalDataSource(v: Vertex) extends SimpleEntity(v) {
  val D = v.toCC[DB_ExternalDataSource]
}

