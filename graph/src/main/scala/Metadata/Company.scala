package Metadata

import gremlin.scala._

object Company {

  // ADDERS

  /*
   * Invariants:
   * 1. No user can exist without a company
   * 2. No company can exist without an administrator
   *
   * Therefore the company and it's admin shall be created together
   */

  def + (companyName: String, adminName: String, adminEmail: String) : Company = {

    val company = new Company(Graph.get + DB_Company(None, companyName, Util.tt))

    val adminUser = Person + (company, adminName, adminEmail)

    company <== (adminUser, Permissions.admin)
    company
  }

  // GETTERS - ALL

  def getAll() : List[Company] = {
    Graph.get.V.hasLabel[DB_Company].toList.map(x => new Company(x))
  }

  // GETTERS - FILTERED

  def get(name: String) : Company = {
    new Company(Graph.get.V.hasLabel(DB_LABELS.company).has(DB_KEY.name, name).head())
  }
}

class Company(v: Vertex) extends GroupEntity(v) {
  val D = v.toCC[DB_Company]

  def --> (project: Project): Unit = { addMember(V, project.V) }
  def --> (person: Person)  : Unit = { addMember(V, person.V ) }
  def --> (group: Group)    : Unit = { addMember(V, group.V  ) }

  def users : List[Person] = {
    v.out(DB_EDGE.Member).hasLabel(DB_LABELS.person).toList().map( x => new Person(x))
  }
  def projects : List[Project] = {
    v.out(DB_EDGE.Member).hasLabel(DB_LABELS.project).toList().map( x => new Project(x))
  }
  def groups : List[Group] = {
    v.out(DB_EDGE.Member).hasLabel(DB_LABELS.group).toList().map( x => new Group(x))
  }
}
