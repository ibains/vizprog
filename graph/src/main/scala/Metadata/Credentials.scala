package Metadata

import gremlin.scala._

object Credentials {

  // ADDERS

  def + (credentialType: String, key1: String, key2: String) : Credentials = {
    new Credentials(Graph.get + DB_Credentials(None, credentialType, key1, key2, Util.tt))
  }

}

class Credentials(v: Vertex) extends SimpleEntity(v) {
  val D = v.toCC[DB_Credentials]
}