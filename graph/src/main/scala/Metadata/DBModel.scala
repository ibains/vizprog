package Metadata

/*
 * This file contains all the case classes that form vertices of the graph
 *
 * The @label annotations point to the labels on graph vertices applied by default
 * The @id is provided as null and filled with a valid id by the graph database
 *
 */

import gremlin.scala.{Key, id, label}

//
// LEGAL PROPERTIES ON EDGES AND VERTICES OF GRAPHS
//

object DB_Permissions {                                   // Legal permission nodes
  val admin = 1
  val read  = 2
  val write = 4
  def s(a: Int) : String =
    a match {
      case `admin` => "admin"
      case `read`  => "read"
      case `write` => "write"
      case _       => "unknown"
    }
}
object DB_KEY {                                           // Vertex keys - for type safe retrieval of keys
  val created            = Key[Long]  ("created")
  val completed          = Key[Long]  ("completed")
  val contents           = Key[String]("contents")
  val credentialType     = Key[String]("credentialType")
  val description        = Key[String]("description")
  val diskSizeGb         = Key[Long]  ("diskSizeGb")
  val email              = Key[String]("email")
  val key1               = Key[String]("key1")
  val key2               = Key[String]("key2")
  val location           = Key[String]("location")
  val name               = Key[String]("name")
  val permission         = Key[String]("permission")
  val retrievalJsonNode  = Key[String]("retrievalJsonNode")
  val schemaJson         = Key[String]("schemaJson")
  val statsJson          = Key[String]("statsJson")
  val status             = Key[String]("status")
  val submitted          = Key[Long]  ("submitted")
}
object DB_EDGE {                                          // The legal labels for edges
  val Permission         = "permission"
  val User               = "user"
  val Member             = "member"
}
object DB_LABELS {
  val company            = "company"
  val group              = "group"
  val project            = "project"
  val person             = "person"
  val role               = "role"
  val workflow           = "workflow"
  val dataset            = "dataset"
  val execution          = "execution"
  val externaldatasource = "externaldatasource"
  val credentials        = "credentials"
}

//
// GRAPH VERTEX TYPES
//

@label(DB_LABELS.company)
case class DB_Company(@id id: Option[Long],
                      name: String,
                      created: Long)

@label(DB_LABELS.group)
case class DB_Group(@id id: Option[Long],
                    name: String,
                    description: String,
                    created: Long)

@label(DB_LABELS.project)
case class DB_Project(@id id: Option[Long],
                      name: String,
                      description: String,
                      created: Long)

@label(DB_LABELS.person)
case class DB_Person(@id id: Option[Long],
                     name: String,
                     email: String,
                     created: Long)

@label(DB_LABELS.role)
case class DB_Role(@id id: Option[Long],
                   permission: Int,
                   created: Long)

@label(DB_LABELS.workflow)
case class DB_Workflow(@id id: Option[Long],
                       name: String,
                       contents: String,
                       created: Long)

@label(DB_LABELS.dataset)
case class DB_Dataset(@id id: Option[Long],
                      name: String,
                      schemaJson: String,
                      statsJson: String,
                      diskSizeGb: Long,
                      created: Long)

@label(DB_LABELS.execution)
case class DB_Execution(@id id: Option[Long],
                        status: String,
                        submitted: Long,
                        completed: Long)

@label(DB_LABELS.externaldatasource)
case class DB_ExternalDataSource(@id is: Option[Long],
                                 location: String,
                                 retrievalJsonNode: String,
                                 created: Long)

@label(DB_LABELS.credentials)
case class DB_Credentials(@id id: Option[Long],
                          credentialType: String,
                          key1: String,
                          key2: String,
                          created: Long)
