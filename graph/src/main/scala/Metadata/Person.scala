package Metadata

import gremlin.scala._
import shapeless.ops.hlist.Comapped

object Person {

  // ADDERS

  def + (company: Company, name: String, email: String) : Person = {
    val p = new Person(Graph.get + DB_Person(None, name, email, Util.tt))
    company --> p
    p
  }

  // GETTERS - ALL

  def getAll() : List[Person] = {
    Graph.get.V.hasLabel[DB_Person].toList.map(x => new Person(x))
  }

  // GETTERS - FILTERED

  def get(companyName: String, name: String) : Person = {
    val company = Company.get(companyName)
    val persons = company.V.out(DB_EDGE.Member).
                            hasLabel(DB_LABELS.person).
                            has(DB_KEY.name, name).toList()
    if (persons.length < 1)
      return null

    new Person(persons.head)
  }
}

class Person(v: Vertex) extends SimpleEntity(v) {
  val D = v.toCC[DB_Person]

  // member accessors
  def id      : Long   = { val Some(x) = D.id ; x }
  def name    : String = D.name
  def email   : String = D.email
  def created : Long   = D.created

  def getWorkflows() : List[(Int, String, SimpleEntity)] = {

    val indirectRoleNodes = V.
                              in(DB_EDGE.Member).
                              hasLabel(DB_LABELS.group).
                              out(DB_EDGE.Permission).
                              hasLabel(DB_LABELS.role).toList()

    val directRoleNodes   = V.
                              out(DB_EDGE.Permission).
                              hasLabel(DB_LABELS.role).toList()

    val allRoleNodes =
      (directRoleNodes ::: indirectRoleNodes).
        map( x => SimpleEntity.get(x).asRole )

    allRoleNodes.flatMap(
      role => role.getItems(
        DB_LABELS.workflow::Nil))
  }
}