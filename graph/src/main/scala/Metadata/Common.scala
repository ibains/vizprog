package Metadata

import gremlin.scala._

object Permissions {
  val admin = 1
  val read  = 2
  val write = 4
}

/*
 * Simple Entity allows us to treat all nodes with a base class
 * and do operations common to them
 */

object SimpleEntity {
  def get(v: Vertex) : SimpleEntity = {
    v.label match {
      case  DB_LABELS.company            => new Company(v)
      case  DB_LABELS.group              => new Group(v)
      case  DB_LABELS.project            => new Project(v)
      case  DB_LABELS.person             => new Person(v)
      case  DB_LABELS.role               => new Role(v)
      case  DB_LABELS.workflow           => new Workflow(v)
      case  DB_LABELS.dataset            => new Dataset(v)
      case  DB_LABELS.execution          => new Execution(v)
      case  DB_LABELS.externaldatasource => new ExternalDataSource(v)
      case  DB_LABELS.credentials        => new Credentials(v)
      case  x                            => assert(false, s"Incorrect Label: ${v.label}"); null
    }
  }
}

class SimpleEntity(v: Vertex) {
  var V : Vertex = v

  def isCompany             : Boolean = V.label == DB_LABELS.company
  def isGroup               : Boolean = V.label == DB_LABELS.group
  def isProject             : Boolean = V.label == DB_LABELS.project
  def isPerson              : Boolean = V.label == DB_LABELS.person
  def isRole                : Boolean = V.label == DB_LABELS.role
  def isWorkflow            : Boolean = V.label == DB_LABELS.workflow
  def isDataset             : Boolean = V.label == DB_LABELS.dataset
  def isExecution           : Boolean = V.label == DB_LABELS.execution
  def isExternalDatasource  : Boolean = V.label == DB_LABELS.externaldatasource
  def isCredentials         : Boolean = V.label == DB_LABELS.credentials
  def isGroupEntity         : Boolean = V.label match {  // can do runtime class lookup too
    case DB_LABELS.company | DB_LABELS.group | DB_LABELS.project => true
    case _ => false
  }

  def asCompany             : Company            = this.asInstanceOf[Company]
  def asGroup               : Group              = this.asInstanceOf[Group]
  def asProject             : Project            = this.asInstanceOf[Project]
  def asPerson              : Person             = this.asInstanceOf[Person]
  def asRole                : Role               = this.asInstanceOf[Role]
  def asWorkflow            : Workflow           = this.asInstanceOf[Workflow]
  def asDataset             : Dataset            = this.asInstanceOf[Dataset]
  def asExecution           : Execution          = this.asInstanceOf[Execution]
  def asExternalDatasource  : ExternalDataSource = this.asInstanceOf[ExternalDataSource]
  def asCredentials         : Credentials        = this.asInstanceOf[Credentials]

  def getRoleHolders(permissions: Int): List[SimpleEntity] = {
    V.in(DB_EDGE.Permission).in(DB_EDGE.Permission).toList.map(x => SimpleEntity.get(x) )
  }

  def vLabel = v.label()
  def vId    = v.id()

  def <== (entity: SimpleEntity, permission: Int) = {

    // only people or persons can be given permissions
    assert(entity.isPerson || entity.isGroup)

    // you cannot give permission over a person, you'll be reported to UN
    assert(!isPerson)

    Role + (V, entity.V, permission)
  }
}

/*
 * Group Entity adds a few operations on top of SimpleEntity that
 * only make sense for groups, such as adding members
 */

class GroupEntity(v: Vertex) extends SimpleEntity(v) {
  def addMember(entity: Vertex, user: Vertex) : Unit = {
    entity --- DB_EDGE.Member --> user                         // TODO: dupe check
  }

  def members() : List[(String, SimpleEntity)] = for {
      member <- v.out(DB_EDGE.Member).toList()
    } yield (member.label(), SimpleEntity.get(member))
}

