package Metadata

/*
 * This is the printing of a graph
 *
 */

import gremlin.scala._

class Printer {

  protected def printNode(sb: StringBuilder, v: Vertex) : Unit = {
    sb++= s"\nNode ${v.id} {\n"
    sb++= s"label: ${v.label}\n"
    sb++= v.valueMap.mkString("\n")
    sb++= "\n}\n"
  }

  protected def printEdge(sb: StringBuilder, e: Edge) : Unit = {
    sb++= s"\n${e.label()} (${e.inVertex.id}) --> (${e.outVertex.id})"
    sb++= e.valueMap.mkString("\n")
  }

  def printGraph(gg: ScalaGraph) : String = {
    val s = new StringBuilder()
    s++= "\n\nVertex List: \n"
    gg.V.toList.foreach( n => printNode(s, n))
    s++= "\n\nEdge List: \n"
    gg.E.toList.foreach( e => printEdge(s, e))
    s.toString()
  }
}
