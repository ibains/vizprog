package Metadata

import gremlin.scala._

object Role {

  // ADDERS

  def + (permission: Int) : Role = {
    new Role(Graph.get + DB_Role(None, permission, Util.tt))
  }

  def + (entity: Vertex, user: Vertex, permission: Int): Unit = {
    var entityPermissionVertex: Role = null

    // see if there is an existing permission node of desired type
    val permissionVertices = entity.in(DB_EDGE.Permission).toList
    for (v <- permissionVertices)
      if (v.value2(DB_KEY.permission).head == permission)
        entityPermissionVertex = new Role(v)

    // if it doesn't exist, create such a node
    if (entityPermissionVertex == null) {
      entityPermissionVertex = Role + permission

      // role has permissions over this entity
      entityPermissionVertex.V --- DB_EDGE.Permission --> entity

    } else {
      // check the user is already there in the permission vertex

      val users = entityPermissionVertex.V.in(DB_EDGE.User).toList
      for (u <- users)
        if (u.value2(DB_KEY.name).head == user.value2(DB_KEY.name).head)
          return
    }

    // user has permission to this role
    user --- DB_EDGE.Permission --> entityPermissionVertex.V
  }

}

class Role(v: Vertex) extends SimpleEntity(v) {
  val D = v.toCC[DB_Role]
  val permission = D.permission

  def getItems() : List[(Int, String, SimpleEntity)] = {

    var ret : List[(Int, String, SimpleEntity)] = Nil

    for (p <- v.out(DB_EDGE.Permission).toList().map( x => SimpleEntity.get(x) )) {
      if (p.isGroupEntity) {
        ret = {
          for {
            (label, simpleEntity) <- p.asInstanceOf[GroupEntity].members()
          } yield (permission, label, simpleEntity) } ::: ret
      } else {
        ret = (permission, p.vLabel, p) :: ret
      }
    }
    ret
  }

  def getItems(labels: List[String]) : List[(Int, String, SimpleEntity)] = {
    for {
      (permission, label, simpleEntity) <- getItems()
      if labels.contains(label)
    } yield (permission, label, simpleEntity)
  }
}
