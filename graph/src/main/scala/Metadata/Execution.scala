package Metadata

import gremlin.scala._

object Execution {

  // ADDERS

  def addExecution(status: String) : Execution = {
    new Execution(Graph.get + DB_Execution(None, status, Util.tt, 0L))
  }
}


class Execution(v: Vertex) extends SimpleEntity(v)  {
  val D = v.toCC[DB_Execution]

}