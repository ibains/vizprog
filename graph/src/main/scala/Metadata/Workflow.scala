package Metadata


import gremlin.scala._

object Workflow {

  // ADDERS

  def + (project: Project, name: String, contents: String) : Workflow = {
    val w = new Workflow(Graph.get + DB_Workflow(None, name, contents, Util.tt))
    project --> w
    w
  }

  // GETTERS - ALL

  def getAll() : List[Workflow] = {
    Graph.get.V.hasLabel(DB_LABELS.workflow).toList.map(x => new Workflow(x))
  }

  // GETTERS - FILTERED

  def get(name: String) : Workflow = {
    new Workflow(Graph.get.V.hasLabel(DB_LABELS.workflow).has(DB_KEY.name, name).head())
  }
}

class Workflow(v: Vertex) extends SimpleEntity(v) {
  val D = v.toCC[DB_Workflow]
  val id       = D.id
  val name     = D.name
  val contents = D.contents
  val created  = D.created
}