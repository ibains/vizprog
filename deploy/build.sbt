name := """deploy-model"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.7.2", // removes play, spark conflict
  "org.apache.spark" %% "spark-core" % "1.6.0",
  "org.apache.spark" %% "spark-core" % "1.6.0",
  "org.apache.spark" %% "spark-sql" % "1.6.0",
  "org.apache.spark" %% "spark-mllib" % "1.6.0",
  "com.amazonaws" % "aws-java-sdk" % "1.11.5",
  "commons-lang" % "commons-lang" % "2.6"
)