package utils

import scala.collection.JavaConversions._
import com.amazonaws.AmazonServiceException
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3._
import com.amazonaws.services.s3.model._
import org.apache.hadoop.fs.s3native.NativeS3FileSystem
import com.amazonaws.auth.profile.ProfileCredentialsProvider


object AWSCommunication {
  import java.io.{File, PrintWriter}
  val awsCredentials = new BasicAWSCredentials (
    "AWS_KEY", "AWS_SECRET")
  val awsClient      = new AmazonS3Client (awsCredentials)
  def writeLocalFile(path: String, fc: String): Unit    = new PrintWriter(path) { write(fc); close }
  def deleteFile(path: String)                : Boolean = new File(path) delete
  def createDirectory(path: String)           : Boolean = new File(path) mkdirs
  def hasDirectory(path: String)              : Boolean = new File(path) exists
  protected def deleteS3Directory(bucketName: String, path: String) : Boolean = {
    awsClient.listObjects(bucketName, path).getObjectSummaries.foreach { file =>
      awsClient.deleteObject(bucketName, file.getKey)
    }
    true
  }
  def deleteS3Object(bucketName: String, path: String) : Boolean = {
    deleteS3Directory(bucketName, path)
  }
  def writeS3File(bucket: String, path: String, fc: String) : Unit = {
    val tmpPlace = "/tmp/vizprog/"
    if (!hasDirectory(tmpPlace))
      createDirectory(tmpPlace)
    val localFileName = tmpPlace+ "tmp.txt"
    writeLocalFile(localFileName, fc)
    awsClient.putObject(bucket, path, new File(localFileName))
    val acl = awsClient.getObjectAcl (bucket, path)
    acl.grantPermission (GroupGrantee.AllUsers, Permission.Read)
    awsClient.setObjectAcl(bucket, path, acl)
    deleteFile(localFileName)
  }
}
