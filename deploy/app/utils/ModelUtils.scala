package utils

import play.api.libs.json._

object ModelUtils {

  def toJson(th: Throwable): JsValue = Json.obj(
    "errorStackTrace" -> Json.obj(
      "errorMessage" -> JsString(th.getMessage),
      "stackTrace" -> JsArray(
        th.getStackTrace.toList.map { st =>
          Json.obj(
            "file" -> JsString(st.getFileName),
            "class" -> JsString(st.getClassName),
            "method" -> JsString(st.getMethodName),
            "line" -> JsNumber(st.getLineNumber)
          )
        })
    )
  )
}