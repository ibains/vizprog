package controllers

import javax.inject._

// PLAY
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.mvc._
import play.api.Logger

// SCALA
import scala.concurrent.Future
import scala.io.Source._
import scala.collection.JavaConversions._
import scala.util.parsing.json._
import scala.concurrent.ExecutionContext.Implicits.global

// SPARK SETUP
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext

// SPARK ML AND SQL
import org.apache.spark.ml._
import org.apache.spark.ml.regression._
import org.apache.spark.ml.classification._
import org.apache.spark.ml.clustering._
import org.apache.spark.ml.evaluation._
import org.apache.spark.ml.feature._
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.Row

// S3 and AWS
import org.apache.hadoop.fs.s3native.NativeS3FileSystem
import com.amazonaws.services.s3.model._
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.AmazonServiceException
import com.amazonaws.services.s3._
import com.amazonaws.auth.profile.ProfileCredentialsProvider

// MISC
import org.apache.commons.lang.StringEscapeUtils.escapeJava

import utils._


@Singleton
class ServeController @Inject() extends Controller {

  var model: SparkModel = null

  def init() = {
    model = new SparkModel
    model.connectAndCreateSession()
    Logger.info("\n\ninit: connectAndCreateSession done")
    model.loadModel()
    Logger.info("\n\ninit: loadModel done. Initialization complete")
  }

  init()

  def index = Action {
    Ok("Your service is up, hit the /model endpoint")
  }

  def runModel = Action.async { implicit request =>

    request.body.asJson.map { json =>

      Logger.info("Got request: " + json)

      json.validate[IO.Input].map {

        case theInput => {

          Logger.info("Validated input: " + theInput)

          model.runRequest(theInput).map {

            case theOutput =>

              val result = Json.toJson(theOutput)
              Logger.info("Scored output: " + theOutput)
              Ok(result)

          }.recover {

            case ex: Exception  =>
              Logger.error(s"Failed to run model", ex)
              InternalServerError(
                Json.obj(
                  "status"    -> "KO",
                  "message"   -> s"Failed to run model, cause: ${ex.getMessage}",
                  "traceback" -> ModelUtils.toJson(ex)
                )
              )
          } // recover
        } // case input
      } // map
      .recoverTotal { errors =>
        Future.successful[Result](
          BadRequest(
            Json.obj(
              "status" -> "KO",
              "message" -> JsError.toJson(errors)
            )
          )
        )
      }
    }.getOrElse(Future.successful[Result](BadRequest))
  }

}

object IO {

  case class Input(id: String, text: String)
  case class Output(id: String, prediction: Double)

  implicit val InputWrites: Writes[Input] = (
    (JsPath \ "id"  ).write[String] and
      (JsPath \ "text").write[String]
    ) (unlift(Input.unapply))

  implicit val InputReads: Reads[Input] = (
    (JsPath \ "id"  ).read[String] and
      (JsPath \ "text").read[String]
    ) (Input.apply _)

  implicit val OutputWrites: Writes[Output] = (
    (JsPath \ "id"        ).write[String] and
      (JsPath \ "prediction").write[Double]
    ) (unlift(Output.unapply))

  implicit val OutputReads: Reads[Output] = (
    (JsPath \ "id"        ).read[String] and
      (JsPath \ "prediction").read[Double]
    ) (Output.apply _)
}

object TrainedModel {
  var model: PipelineModel = null
  def loadModel(sc: SparkContext): Unit = {
    val hadoopConf = sc.hadoopConfiguration
    hadoopConf.set("fs.s3.awsAccessKeyId", "AWS_KEY")
    hadoopConf.set("fs.s3.awsSecretAccessKey", "AWS_SECRET")
    model = PipelineModel.load("s3://S3_BUCKET/examples/results/newsgroup/theModel")
  }
}

class SparkModel {

  var sc         : SparkContext          = null

  def connectAndCreateSession(): Unit = {

    val conf = new SparkConf().
      setAppName("science_newsgroup_classification").
      setMaster("local[4]").set("spark.ui.port", "40440").
      set("spark.sql.shuffle.partitions","4").
      set("spark.default.parallelism","4")
    sc         = new SparkContext(conf)

    val hadoopConf = sc.hadoopConfiguration
    hadoopConf.set("fs.s3.impl",               "org.apache.hadoop.fs.s3native.NativeS3FileSystem")
  }
  def loadModel(): Unit = TrainedModel.loadModel(sc)

  def runRequest(theInput: IO.Input) : Future[IO.Output] = { Future {
    val sqlContext = SQLContext.getOrCreate(sc)
    import sqlContext.implicits._

    val inputDF  : DataFrame = Seq(theInput).toDF()

    val outputDF : DataFrame = TrainedModel.model.transform(inputDF)

    val selectedDF = outputDF.select(outputDF("id"), outputDF("prediction"))
    val dataframeRow = selectedDF.collect()(0)
    val Row(id: String, prediction: Double) = dataframeRow
    IO.Output(id, prediction)
  }}
}
