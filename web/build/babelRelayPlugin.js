var babelRelayPlugin = require('babel-relay-plugin');
var introspectionQuery = require('../node_modules/graphql/utilities').introspectionQuery;
var request            = require('sync-request');
import { API_Config } from '../src/constants';

var url = API_Config.serverRoot + '/graphql';

var response = request('GET', url, {
  qs: {
    query: introspectionQuery
  }
});

var schema;

try {
  schema = JSON.parse(response.body.toString('utf-8'));
} catch (ex) {
  console.error('Failed to retrieve the graphsql schema.  Is graphql server running at', url, '?\n')
}

module.exports = babelRelayPlugin(schema.data, {
  abortOnError: true
});
