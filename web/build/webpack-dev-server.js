import webpack              from 'webpack';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import WebpackHotMiddleware from 'webpack-hot-middleware';
import historyApiFallback   from 'connect-history-api-fallback';
import express              from 'express';
import config               from '../config';
import webpackConfig        from './webpack/development_hot';

const paths = config.get('utils_paths');
const compiler = webpack(webpackConfig);
const app = express();

app.use(historyApiFallback({
  verbose: false
}));

app.use(WebpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  contentBase: paths.src(),
  hot: true,
  quiet: false,
  noInfo: false,
  lazy: false,
  // watchOptions to make incremental compile work inside Vagrant VM
  watchOptions: {
    poll: 2000,
    ignore: /node_modules/
  },
  stats: {
    colors: true
  }
}));

app.use(WebpackHotMiddleware(compiler));

app.use('/assets', express.static(paths.static('assets')));

export default app;
