import webpack           from 'webpack';
import config            from '../../config';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import autoprefixer      from 'autoprefixer';
import ExtractTextPlugin from 'extract-text-webpack-plugin';

const paths = config.get('utils_paths');

const webpackConfig = {
  name    : 'client',
  target  : 'web',
  entry   : {
    app : [
      paths.project(config.get('dir_src')) + '/app.js'
    ],
    vendor : config.get('vendor_dependencies')
  },
  output : {
    filename   : '[name].js',
    path       : paths.project(config.get('dir_dist')),
    publicPath : '/'
  },
  plugins : [
    new ExtractTextPlugin('react-toolbox.css', { allChunks: true }),
    new webpack.DefinePlugin(config.get('globals')),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new HtmlWebpackPlugin({
      template : paths.src('index.html'),
      hash     : true,
      filename : 'index.html',
      inject   : 'body'
    })
  ],
  resolve : {
    extensions : ['', '.js', '.jsx', '.scss'],
    alias      : config.get('utils_aliases'),
    packageMains: ["webpack", "browser", "web", "browserify", ["jam", "main"], "main", "style"],
  },
  /* workaround for https://github.com/pugjs/pug-loader/issues/8 */
  node: {
    fs: 'empty'
  },
  module : {
    noParse: [
      /plotly\.js/
    ],
    loaders : [
      { test: /\.json$/, loader: "json" },
      {
        test : /\.(js|jsx)$/,
        exclude : /node_modules/,
        loader  : 'babel-loader',
      },
      {
        test    : /\.css$/,
        loaders : [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test    : /(core|index)\.scss$/,
        loaders : [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test    : /\.scss$/,
        exclude : /((node_modules)\/react-toolbox|\/styles)/,
        loaders : [
          'style-loader',
          require.resolve('css-loader') + '?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test    : /\.scss$/,
        include : /(node_modules)\/react-toolbox/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap&modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!postcss!sass!toolbox?sourceMap')
      },
      /* eslint-disable */
      { test: /\.woff(\?.*)?$/,  loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff" },
      { test: /\.woff2(\?.*)?$/, loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff2" },
      { test: /\.ttf(\?.*)?$/,   loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/octet-stream" },
      { test: /\.eot(\?.*)?$/,   loader: "file-loader?prefix=fonts/&name=[path][name].[ext]" },
      { test: /\.svg(\?.*)?$/,   loader: "url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=image/svg+xml" },
      { test: /\.(png|jpg)$/,    loader: 'url-loader?limit=25000' }
      /* eslint-enable */
    ]
  },
  sassLoader : {
    includePaths : paths.src('styles')
  },
  postcss : [ autoprefixer({ browsers : ['last 2 versions'] }) ],
  toolbox: {
    theme: paths.src('styles/_theme.scss')
  }
  /*
  profile : true,
  stats: {
    hash: true,
    version: true,
    timings: true,
    assets: true,
    chunks: true,
    modules: true,
    reasons: true,
    children: true,
    source: false,
    errors: true,
    errorDetails: true,
    warnings: true,
    publicPath: true
  },
  */
};

// NOTE: this is a temporary workaround. I don't know how to get Karma
// to include the vendor bundle that webpack creates, so to get around that
// we remove the bundle splitting when webpack is used with Karma.
const commonChunkPlugin = new webpack.optimize.CommonsChunkPlugin(
  'vendor', '[name].js'
);
commonChunkPlugin.__KARMA_IGNORE__ = true;
webpackConfig.plugins.push(commonChunkPlugin);

export default webpackConfig;
