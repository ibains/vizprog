import React, {Component} from 'react'
import { API_Config } from 'constants'
import { Link } from 'react-router'

export default class BrowseView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      workflows: [],
      datasourcesAndDatasets: []
    }
  }

  fetchData = (url) => {
    const headers = new Headers()
    headers.append("Content-Type", "application/json")
    return fetch(url, {
      method: "get",
      headers: headers,
      credentials: 'same-origin'
    })
      .then(response => response.json())
      .catch(() => {
        alert('Error communicating with the server.  Try again later.')
      })
  }

  componentDidMount() {
    this.fetchData(`${API_Config.fetchWorkflowsUrl}`)
      .then(workflows => this.setState({workflows: workflows}))

    this.fetchData(`${API_Config.fetchDataSetsUrl}`)
      .then(dataSets => {
        dataSets.forEach(d => d.type = 'Dataset')
        this.setState({
          datasourcesAndDatasets: this.state.datasourcesAndDatasets.concat(dataSets)
        })
      })

    this.fetchData(`${API_Config.fetchDataSourcesUrl}`)
      .then(datasources => {
        datasources.forEach(d => d.type = 'Datasource')
        this.setState({
          datasourcesAndDatasets: this.state.datasourcesAndDatasets.concat(datasources)
        })
      })
  }

  render() {
    var datasourcesAndDatasets = this.state.datasourcesAndDatasets.map((item, index) => {
      return (
      <tr key={'datasourcesAndDatasets' + index}>
        <td>{item.name}</td>
        <td>{item.project.name}</td>
        <td>{item.type}</td>
        <td>{item.location}</td>
        <td>{item.format}</td>
        <td>{item.project.user.email}</td>
        <td>{item.created}</td>
      </tr>
      )
    })

    var workflows = this.state.workflows.map((item, index) => {
      return (
        <tr key={'workflow' + index}>
          <td>{item.name}</td>
          <td>{item.project.name}</td>
          <td>{item.project.user.email}</td>
          <td>{item.created}</td>
          <td>{item.lastModified}</td>
        </tr>
      )
    })

    return (
      <div className="container">
      <h2 className="title">Browse
        <Link to="/" className="back-btn" ><i className="material-icons">keyboard_backspace</i></Link></h2>
      <h3 className="subtitle pull-left">Datasources and Datasets</h3>
      <div className="tagsinput pull-right">
        <input type="text" data-role="tagsinput" id='dataset' placeholder='Search' />
      </div>
      <div className="clearfix"></div>
      <div className="wide table-border table">
        <table id='browse' cellspacing="0" cellpadding="0" className="wide " data-table='dataset'>
          <thead>
          <tr>

            <th>
              <span>Name</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Project</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Type</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Location</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Format</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>User</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Created</span><i className="material-icons md-9">unfold_more</i></th>

          </tr>
          </thead>
          <tbody>
          {datasourcesAndDatasets}
          </tbody>
        </table>
      </div>
      <div className="crossfiler">
        <i className="material-icons md-16">arrow_drop_down</i>
        <span>Crossfilter</span>
        <i className="material-icons md-16">arrow_drop_up</i>
      </div>
      <h3 className="subtitle pull-left">Workflows</h3>
      <div className="tagsinput pull-right filter-table">
        <input type="text" value="" data-role="tagsinput" data-table="order-table" className="light-table-filter" id='workflows' placeholder='Search' />
      </div>

      <div className="clearfix"></div>
      <div className="wide table-border table">
        <table id='browse' cellspacing="0" cellpadding="0" className="wide " data-table='workflows'>
          <thead>
          <tr>

            <th>
              <span>Name</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Project</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>User</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Created</span><i className="material-icons md-9">unfold_more</i></th>
            <th>
              <span>Last modified</span><i className="material-icons md-9">unfold_more</i></th>
          </tr>
          </thead>
          <tbody>
            {workflows}
          </tbody>
        </table>
      </div>
    </div>)
  }
}
