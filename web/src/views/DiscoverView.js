import React from 'react';
import { connect } from 'react-redux'
import { push, replace } from 'react-router-redux'
import { actions } from '../reducers/workflows'
import { Workflow } from 'components/Workflow'
import { WorkflowCard } from 'components/WorkflowCard'
import { API_Config } from 'constants'

export class DiscoverView extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      workflows: []
    }
  }

  static propTypes = {
    //getWorkflows: React.PropTypes.func.isRequired,
    workflows: React.PropTypes.object.isRequired,
    setActiveWorkflow: React.PropTypes.func.isRequired
  }

  setWorkflows = () => {
    const headers = new Headers()
    headers.append("Content-Type", "application/json")
    let workflows = []
    fetch(`${API_Config.serverUrl}/workflows`, {
      method: "get",
      headers: headers
    }).then(response => {
      response.json().then(workflows => {
        this.setState({ workflows })
      })
    }).catch(response => {
      alert('Error communicating with the server.  Try again later.')
    })
  }

  componentDidMount() {
    this.setWorkflows()
  }

  componentWillUnmount() {
  }

  onMakeActive(workflow) {
    this.props.setActiveWorkflow(workflow)
    this.props.push('/')
  }

  render() {
    const workflowCards = this.state.workflows.map(workflow => {
      return (<WorkflowCard key={workflow.graph.metainfo.id} workflow={workflow} onMakeActive={() => { this.onMakeActive(workflow)}} />)
    })

    return (
      <div id="discover-view">
        {workflowCards}
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    workflows: state.workflows,
    routerState : state.routing
  }
}

export default connect(mapStateToProps, {...actions, push, replace})(DiscoverView);
