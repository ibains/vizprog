import React from 'react'

import App from '../components/App';

import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppTheme from 'views/AppTheme'

const AppView = (props) => (
  <MuiThemeProvider muiTheme={getMuiTheme(AppTheme)}>
    <App {...props} />
  </MuiThemeProvider>
);

export default AppView;
