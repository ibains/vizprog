import React, {Component} from 'react'
import Relay from 'react-relay'
import userRoute from '../../routes/userRoute'
import AccountUserViewComponent from 'components/account/AccountUserViewComponent'
import Loading from 'components/Loading'

export default class AccountUserView extends Component {
  render() {
    return(
      <Relay.RootContainer
        Component={AccountUserViewComponent}
        route={new userRoute({userID: this.props.params.userID})}
        renderFetched={ (data) =>
          <AccountUserViewComponent {...data} {...this.props} />
        }
        renderFailure={ (error, retry) => {
            return (
              <div>
                <p>{error.message}</p>
                <p><button onClick={retry}>Retry?</button></p>
              </div>
            );
          }
        }
        renderLoading={ () => {
            return (<Loading />);
          }
        }
      />
    );
  }
}

