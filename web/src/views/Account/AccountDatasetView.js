import React, {Component} from 'react'
import Relay from 'react-relay'
import accountDatasetRoute from '../../routes/accountDatasetRoute'
import AccountDatasetViewComponent from 'components/account/AccountDatasetViewComponent'
import Loading from 'components/Loading'

export default class AccountDatasetView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  render() {
    return(
      <Relay.RootContainer
        Component={AccountDatasetViewComponent}
        route={new accountDatasetRoute({datasetID: this.props.params.datasetID})}
        renderFetched={ (data) =>
          <AccountDatasetViewComponent {...data} {...this.props} />
        }
        renderFailure={ (error, retry) => {
            return (
              <div>
                <p>{error.message}</p>
                <p><button onClick={retry}>Retry?</button></p>
              </div>
            );
          }
        }
        renderLoading={ () => {
            return (<Loading />);
          }
        }
      />
    );
  }
}

