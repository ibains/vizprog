import React, {Component} from 'react'
import Relay from 'react-relay'
import accountWorkflowRoute from '../../routes/accountWorkflowRoute'
import AccountWorkflowViewComponent from 'components/account/AccountWorkflowViewComponent'
import Loading from 'components/Loading'

export default class AccountWorkflowView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  render() {
    return(
      <Relay.RootContainer
        Component={AccountWorkflowViewComponent}
        route={new accountWorkflowRoute({workflowID: this.props.params.workflowID})}
        renderFetched={ (data) =>
          <AccountWorkflowViewComponent {...data} {...this.props} />
        }
        renderFailure={ (error, retry) => {
            return (
              <div>
                <p>{error.message}</p>
                <p><button onClick={retry}>Retry?</button></p>
              </div>
            );
          }
        }
        renderLoading={ () => {
            return (<Loading />);
          }
        }
      />
    );
  }
}

