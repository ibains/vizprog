import React, {Component} from 'react'
import Relay from 'react-relay'
import accountProjectRoute from '../../routes/accountProjectRoute'
import AccountProjectViewComponent from 'components/account/AccountProjectViewComponent'
import Loading from 'components/Loading'

export default class AccountProjectView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  render() {
    return(
      <Relay.RootContainer
        Component={AccountProjectViewComponent}
        route={new accountProjectRoute({projectID: this.props.params.projectID})}
        renderFetched={ (data) =>
          <AccountProjectViewComponent {...data} {...this.props} />
        }
        renderFailure={ (error, retry) => {
            return (
              <div>
                <p>{error.message}</p>
                <p><button onClick={retry}>Retry?</button></p>
              </div>
            );
          }
        }
        renderLoading={ () => {
            return (<Loading />);
          }
        }
      />
    );
  }
}

