import React, {Component} from 'react'
import Relay from 'react-relay'
import accountTeamRoute from '../../routes/accountTeamRoute'
import AccountTeamViewComponent from 'components/account/AccountTeamViewComponent'
import Loading from 'components/Loading'

export default class AccountTeamView extends Component {
  constructor(props, context) {
    super(props, context);
  }
  render() {
    return(
      <Relay.RootContainer
        Component={AccountTeamViewComponent}
        route={new accountTeamRoute({teamID: this.props.params.teamID})}
        renderFetched={ (data) =>
          <AccountTeamViewComponent {...data} {...this.props} />
        }
        renderFailure={ (error, retry) => {
            return (
              <div>
                <p>{error.message}</p>
                <p><button onClick={retry}>Retry?</button></p>
              </div>
            );
          }
        }
        renderLoading={ () => {
            return (<Loading />);
          }
        }
      />
    );
  }
}

