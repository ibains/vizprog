import React from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import * as WorkflowActions from '../actions/workflowActions';
import * as SpecActions from '../actions/specActions';
import ProcessLibrary from '../components/ProcessLibrary'
import NewWorkflowMenu from '../components/NewWorkflowMenu';

import WorkflowTabs from 'components/WorkflowTabs';

export class WorkflowView extends React.Component {
  static propTypes = {
    workflows: React.PropTypes.object.isRequired,
    actions: React.PropTypes.object.isRequired,
  };

  componentWillMount() {
    this.props.actions.getAllWorkflows();
    this.props.actions.getFunctionDefinitions();
    this.props.actions.getNodeSpecifications();
  }

  componentWillUpdate(nextProps) {
    if (!nextProps.id && nextProps.workflows.size) {
      nextProps.actions.selectWorkflow(nextProps.workflows.keySeq().first());
    }
  }

  render() {
    const { workflows, id, actions } = this.props;
    return (
      <div id="workflow-view">
        { id ? <ProcessLibrary addProcess={actions.addWorkflowProcess.bind(null, id)} /> : null }
        <div className="clearfix"></div>
        { id ? <WorkflowTabs workflows={workflows} workflowID={id} actions={actions} removeWorkflow={actions.removeWorkflow}/> : null }
        <NewWorkflowMenu createWorkflow={actions.createWorkflow} loadWorkflow={actions.loadWorkflow} alert={!id} />
      </div>
    );
  }
}

let mapDispatchToProps = dispatch => ({
  actions: Object.assign({},
            bindActionCreators(WorkflowActions, dispatch),
            bindActionCreators(SpecActions, dispatch)),
});
export default connect(state => ({ id: state.workflowID, workflows: state.workflows }), mapDispatchToProps)(WorkflowView);
