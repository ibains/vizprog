import React from 'react'
import 'react-toolbox/lib/commons.scss'
import 'rc-menu/assets/index.css';
import 'styles/core.scss'
import 'styles/index.scss'

function CoreLayout ({ children }) {
  return (
    <div className='page-container'>
      <div className='view-container'>
        {children}
      </div>
    </div>
  )
}

CoreLayout.propTypes = {
  children: React.PropTypes.element
}

export default CoreLayout
