import Immutable from 'immutable';

import * as ProcessConstants from './process';

export const FAKE_WORKFLOW = {
  label: 'A Very Fake Workflow',
  processes: {
    'FAKE_PROCESS_1': { x: 50, y: 50 },
    'FAKE_PROCESS_2': { x: 50, y: 200 },
  },
  connections: [],
};


const _meta = { id: 'SimpleTextClassificationPipeline', memory: 1, processors: 1, cluster: 'local', mode: 'batch' }

export const TEXT_CLASSIFICATION_WORKFLOW = Immutable.fromJS({
  label: _meta.id,
  metainfo : _meta,
  inports  : {},
  outports : {},
  groups   : [
    {
      name: "STA_GENERATE_FEATURES",
      component : "MLFeature",
      nodes: ["n3", "n4"],
      metadata: { description: "Generate Features based on text frequency", color: 0 }
    },
    {
      name: "STA_LINEAR_REGRESSION",
      component : "MLPipeline",
      nodes: ["m1", "n5"],
      metadata: { description: "Linear Regression based Text Classification pipeline", color: 0 },
      ports: { inputs: [ ], outputs: ["out"] }
    },
    {
      name: "STA_TEXT_CLASSIFICATION",
      component : "MLModel",
      nodes: ["m2", "n6", "n7"],
      metadata: { description: "Text Classification Model", color: 0 }
    }
  ],
  processes: {
    STA_GENERATE_TRAINING: { x: 100, y: 100 },
    STA_GENERATE_TESTING: { x: 300, y: 100 },
    STA_TOKENIZE_TEXT: { x: 500, y: 100 },
    STA_TOKEN_FREQUENCY: { x: 700, y: 100 },
    STA_LOGISTIC_REGRESSION: { x: 100, y: 300 },
    STA_BUILD_MODEL: { x: 300, y: 300 },
    STA_APPLY_MODEL: { x: 500, y: 300 },
    STA_SELECT_PREDICTION: { x: 700, y: 300 },
    STA_WRITE_PREDICTION: { x: 100, y: 500 },
  },
  connections: [
    {
      src: { process: "STA_GENERATE_TRAINING", port: "out" },
      tgt: { process: "STA_BUILD_MODEL", port: "train_data" },
      metadata: { route: "e1" }
    },
    {
      src: { process: "STA_GENERATE_TESTING", port: "out" },
      tgt: { process: "STA_APPLY_MODEL", port: "data" },
      metadata: { route: "e2" }
    },
    {
      src: { process: "STA_TOKENIZE_TEXT", port: "out" },
      tgt: { process: "STA_LOGISTIC_REGRESSION", port: "in" },
      metadata: { route: "e3"
      }
    },
    {
      src: { group: "STA_LINEAR_REGRESSION", port: "out" },
      tgt: { process: "STA_BUILD_MODEL", port: "pipeline" },
      metadata: { route: "e4" }
    },
    {
      src: { process: "STA_BUILD_MODEL", port: "model" },
      tgt: { process: "STA_APPLY_MODEL", port: "model" },
      metadata: { route: "e5" }
    },
    {
      src: { process: "STA_APPLY_MODEL", port: "out" },
      tgt: { process: "STA_SELECT_PREDICTION", port: "in" },
      metadata: { route: "e6" }
    },
    {
      src: { process: "STA_SELECT_PREDICTION", port: "out" },
      tgt: { process: "STA_WRITE_PREDICTION", port: "in" },
      metadata: { route: "e7" }
    }
  ]
});

export const samples = {}
