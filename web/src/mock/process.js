export const FAKE_PROCESS = {
  "component": "FileInput",
  "metadata": {
    "label": "Fake Process"
  },
  "properties": {
    "fileType": "csv",
    "header": "true",
    "inferSchema": "true",
    "fileName": "/vagrant/tmp/resources/artists.csv",
    "interim": true,
    "import": true,
    "location": "/vagrant/tmp/resources/imports/artists"
  },
  "ports": {
    "inputs": [],
    "outputs": ["out"],
  }
};

export const GENERATE_TRAINING_DATA = {
  "component": "Script",
  "metadata": {
    "label": "generate_train_data"
  },
  "properties": {
    "package": "datagen",
    "language": "scala",
    "code": "import org.apache.spark.sql.DataFrame\\n\\ncase class LabeledDocument(id: Long, text: String, label: Double)\\ndef generateTrainingData(sc: SparkContext) : DataFrame = {\\nval training = sc.parallelize(Seq(LabeledDocument(0L, \"a b c d e spark\", 1.0),\\nLabeledDocument(1L, \"b d\", 0.0),\\nLabeledDocument(2L, \"spark f g h\", 1.0),\\nLabeledDocument(3L, \"hadoop mapreduce\", 0.0)))\\ntraining.toDF()\\n}",
    "call": "generateTrainingData"
  },
  "ports": {
    "inputs": [],
    "outputs": [
      "out"
    ]
  }
};

export const GENERATE_TEST_DATA = {
  "component": "Script",
  "metadata": {
    "label": "generate_test_data"
  },
  "properties": {
    "package": "datagen",
    "language": "scala",
    "code": "import org.apache.spark.sql.DataFrame\\n\\ncase class Document(id: Long, text: String)\\nobject TestGen {\\ndef generateTestData(sc: SparkContext) : DataFrame = {\\nval test = sc.parallelize(Seq(\\nDocument(4L, \"spark i j k\"),\\nDocument(5L, \"l m n\"),\\nDocument(6L, \"spark hadoop spark\"),\\nDocument(7L, \"apache hadoop\")))\\ntest.toDF()\\n}\\n}",
    "call": "TestGen.generateTestData"
  },
  "ports": {
    "inputs": [],
    "outputs": [
      "out"
    ]
  }
};

export const TOKENIZE_TEXT = {
  "component": "Tokenizer",
  "metadata": {
    "label": "tokenize_text"
  },
  "properties": {
    "ml": "feature",
    "inputColumn": "text",
    "outputColumn": "words"
  },
  "ports": {
    "inputs": [
      "in"
    ],
    "outputs": [
      "out"
    ]
  }
};

export const TOKEN_FREQUENCY = {
  "component": "HashingTF",
  "metadata": {
    "label": "token_frequency"
  },
  "properties": {
    "ml": "feature",
    "numFeatures": 1000,
    "inputColumn": "words",
    "outputColumn": "myFeatures"
  },
  "ports": {
    "inputs": [
      "in"
    ],
    "outputs": [
      "out"
    ]
  }
};

export const LOGISTIC_REGRESSION = {
  "component": "LogisticRegression",
  "metadata": {
    "label": "Logistic Regression"
  },
  "properties": {
    "ml": "classifier",
    "maxIter": 10,
    "regParam": 0.001,
    "featureColumn": "myFeatures"
  },
  "ports": {
    "inputs": [
      "in"
    ],
    "outputs": [
      "out"
    ]
  }
};

export const BUILD_MODEL = {
  "component": "ModelBuilder",
  "metadata": {
    "label": "Build Model"
  },
  "properties": {
    "ml": "fitter"
  },
  "ports": {
    "inputs": [
      "pipeline",
      "train_data"
    ],
    "outputs": [
      "model"
    ]
  }
};

export const APPLY_MODEL = {
  "component": "ModelApplier",
  "metadata": {
    "label": "Apply Model"
  },
  "properties": {
    "ml": "applier"
  },
  "ports": {
    "inputs": [
      "model",
      "data"
    ],
    "outputs": [
      "out"
    ]
  }
};

export const SELECT_PREDICTION = {
  "component": "Select",
  "metadata": {
    "label": "Select Prediction"
  },
  "properties": {
    "columns": ["id", "text", "probability", "prediction"]
  },
  "ports": {
    "inputs": [
      "in"
    ],
    "outputs": [
      "out"
    ]
  }
};

export const WRITE_PREDICTION = {
  "component": "FileOutput",
  "metadata": {
    "label": "write_prediction"
  },
  "properties": {
    "collect": true,
    "fileType": "csv",
    "fileName": "/vagrant/tmp/resources/SimpleTextClassification_predictions.csv"
  },
  "ports": {
    "inputs": [
      "in"
    ],
    "outputs": []
  }
};
