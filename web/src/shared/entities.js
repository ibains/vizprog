export const joinTypes = [
  {
    label: 'Inner',
    value: 'inner'
  },
  {
    label: 'Outer',
    value: 'outer'
  },
  {
    label: 'Left',
    value: 'left_outer'
  },
  {
    label: 'Right',
    value: 'right_outer'
  },
  {
    label: 'Left Semi',
    value: 'leftsemi'
  }
];

export const compareOperators = [
  {
    label: '===',
    value: '==='
  },
  {
    label: '!==',
    value: '!=='
  },
  {
    label: '>',
    value: '>'
  },
  {
    label: '>=',
    value: '>='
  },
  {
    label: '<',
    value: '<'
  },
  {
    label: '<=',
    value: '<='
  },
  {
    label: '<=>',
    value: '<=>'
  }
];
