export const MODE_TEMPLATES = {
  'deploy': {
    metainfo: {
      id: "Deploy",
      memory: 1,
      processors: 1,
      cluster: "local",
      mode: "deploy"
    },
    inports: {},
    outports: {},
    groups: [],
    processes: {
      n1: {
        component: "RESTInput",
        metadata: {
          x: 100,
          y: 100,
          label: "Convert input"
        },
        properties: {
          schema: [
            [
              "id",
              "StringType"
            ],
            [
              "text",
              "StringType"
            ]
          ]
        },
        ports: {
          inputs: [],
          outputs: [
            "out"
          ]
        }
      },
      n2: {
        component: "ApplyStoredModel",
        metadata: {
          x: 100,
          y: 100,
          label: "trained model"
        },
        properties: {
          modelType: "PipelineModel",
          fileName: "s3://",
          fileSystem: "s3",
          credentials: "system"
        },
        ports: {
          inputs: [
            "in"
          ],
          outputs: [
            "out"
          ]
        }
      },
      n3: {
        component: "RESTOutput",
        metadata: {
          x: 100,
          y: 100,
          label: "Prediction "
        },
        properties: {
          schema: [
            [
              "id",
              "StringType"
            ],
            [
              "prediction",
              "DoubleType"
            ]
          ]
        },
        ports: {
          inputs: [
            "in"
          ],
          outputs: []
        }
      }
    },
    connections: [
      {
        src: {
          process: "n1",
          port: "out"
        },
        tgt: {
          process: "n2",
          port: "in"
        },
        metadata: {
          route: "e1"
        }
      },
      {
        src: {
          process: "n2",
          port: "out"
        },
        tgt: {
          process: "n3",
          port: "in"
        },
        metadata: {
          route: "e2"
        }
      }
    ]
  },
  'batch': {
    metainfo: {
      id: "Batch",
      memory: 1,
      processors: 1,
      cluster: "local",
      mode: "batch"
    },
    inports: {},
    outports: {},
    groups: [],
    processes: {
    },
    connections: [
    ]
  }
}

export default MODE_TEMPLATES




