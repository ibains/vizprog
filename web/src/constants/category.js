import Immutable from 'immutable';

export const CATEGORY_DEFINITIONS = new Immutable.fromJS({
  'io': {
    label: 'Input / Output',
    icon: 'IconWorkflowIO',
    color: '#00CBD5'
  },
  'transform': {
    label: 'Transform',
    icon: 'IconWorkflowTransform',
    color: '#C37DCF'
  },
  'join-split': {
    label: 'Join / Split',
    icon: 'IconWorkflowIO',
    color: '#876CF8'
  },
  'ml': {
    label: 'Machine Learning',
    icon: 'IconWorkflowIO',
    color: '#0297E6'
  },
  'script': {
    label: 'Script / Prepare',
    icon: 'IconWorkflowIO',
    color: '#AE3ED4'
  },
  'visualize': {
    category: 'visualize',
    label: 'Visualize',
    icon: 'IconWorkflowIO',
    color: '#FF379C'
  }
});
