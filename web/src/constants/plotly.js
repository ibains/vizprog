export const CONFIG = {
    showLink: false,
    displayModeBar: true,
    displaylogo: false,
    modeBarButtons: [['toImage']],
};

export const DEFAULT_LAYOUT = {
  autoresize: true,
  margin: {
    l: 60,
    r: 20,
    b: 60,
    t: 10,
    pad: 4,
  },
}
