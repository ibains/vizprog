import keyMirror from 'keymirror';

export const LOCAL_STORAGE_TOKEN_KEY = 'denim.session.token';

export const ActionTypes = keyMirror({
	LOGIN_REQUESTED: null,
	LOGIN_SUCCESS: null,
	LOGIN_FAILED: null,
	LOGOUT: null
});

export const API_Config = {
  serverRoot: 'http://dev.vizprog.com',
  serverUrl: 'http://dev.vizprog.com/app',
  signInPath: '/signIn',
  signOutPath: '/signOut',
  currentUserCredentialsPath: '/currentUserCredentials'
};

API_Config.fetchWorkflowsUrl = API_Config.serverUrl + "/user/workflow"
API_Config.fetchDataSetsUrl = API_Config.serverUrl + "/user/dataset"
API_Config.fetchDataSourcesUrl = API_Config.serverUrl + "/user/datasource"

export const LOADER_OPTIONS = {
    lines: 13,
    length: 20,
    width: 10,
    radius: 30,
    corners: 1,
    rotate: 0,
    direction: 1,
    color: '#000',
    speed: 1,
    trail: 60,
    shadow: false,
    hwaccel: false,
    zIndex: 2e9,
    top: '50%',
    left: '50%',
    scale: 1.00
};
