var keyMirror = require('keymirror');

export const WorkflowActions = keyMirror({
  RUN_WORKFLOW: null,
  GET_WORKFLOWS: null,
  CREATE_WORKFLOW: null,
  REMOVE_WORKFLOW: null,
  SELECT_WORKFLOW: null,
  MOVE_PROCESS: null,
  ADD_PROCESS_TO_WORKFLOW: null,
  REMOVE_PROCESS_FROM_WORKFLOW: null,
  CREATE_CONNECTION: null,
  REMOVE_CONNECTION: null,
  AUTO_LAYOUT: null,
  SHIFT_WORKFLOW: null,
});

export const ProcessActions = keyMirror({
  ADD_PROCESS: null,
  SELECT_PROCESS: null,
  REMOVE_PROCESS: null,
  UPDATE_PROCESS: null,
  SET_PROCESS_JOIN: null,
  SET_PROCESS_FILTER: null,
  SET_PIE_CHART_COLUMNS: null,
  SET_LINE_CHART_COLUMNS: null,
  SET_BAR_CHART_COLUMNS: null,
  SET_BOX_PLOT_COLUMN: null,
  SET_SCATTER_PLOT_COLUMNS: null,
  SET_VISUALIZE_COLUMNS: null,
  INSERT_CLEANSE_PROCESS: null,
  ADD_CLEANSE_FUNCTION: null,
  REMOVE_CLEANSE_FUNCTION: null,
  REORDER_CLEANSE_FUNCTIONS: null
});

export const SampleActions = keyMirror({
  SELECT_SAMPLE: null,
});

export const ProjectActions = keyMirror({
  GET_PROJECTS: null,
  CREATE_PROJECT: null,
});

export const UserActions = keyMirror({
  GET_USER: null,
});

export const SpecActions = keyMirror({
  GET_FUNCTION_DEFINITIONS: null,
  GET_PROCESS_DEFINITIONS: null,
  GET_NODE_SPECIFICATIONS: null
});

export default Object.assign({}, WorkflowActions, ProcessActions, SampleActions, ProjectActions, UserActions, SpecActions);
