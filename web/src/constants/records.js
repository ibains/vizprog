import Immutable from 'immutable';

export const ProjectRecord = Immutable.Record({ id: null, name: null, user: null });
export const GraphRecord = Immutable.Record({
  processes: Immutable.Map(),
  connections: Immutable.List(),
  groups: Immutable.List(),
  metainfo: Immutable.Map(),
  inports: Immutable.Map(),
  outports: Immutable.Map(),
  samples: null
});
export const ContentRecord = Immutable.Record({ graph: new GraphRecord() });
export const WorkflowRecord = Immutable.Record({
  name: null,
  project: new ProjectRecord(),
  description: null,
  created: new Date(),
  lastModified: new Date(),
  content: new ContentRecord(),
  isRunning: false,
  deployUrl: ''
});

export const UserRecord = Immutable.Record({ id: null, name: null, email: null, isFetching: false });
export const ProcessRecord = Immutable.Record({
  component: null,
  properties: Immutable.Map(),
  metadata: Immutable.Map(),
  ports: Immutable.Map()
});
export const SampleRecord = Immutable.Record({ schema: null, stats: null, histogram: null, nulls: null, data: null, topn: null });
export const StatsRecord = Immutable.Record({ count: Immutable.Map(), mean: Immutable.Map(), stddev: Immutable.Map(), min: Immutable.Map(), max: Immutable.Map() });
export const GroupRecord = Immutable.Record({ name: null, component: null, nodes: null, metadata: null, properties: null, ports: null });
export const PortRecord = Immutable.Record({ process: '', port: '' });
export const PortListRecord = Immutable.Record({ inputs: Immutable.List(), outputs: Immutable.List() });
export const ConnectionRecord = Immutable.Record({ src: new PortRecord(), tgt: new PortRecord(), metadata: Immutable.Map() });
export const MetaInfoRecord = Immutable.Record({ id: null, memory: 1, processors: 1, cluster: 'local', mode: 'batch' });
export const FunctionRecord = Immutable.Record({
  addReplaceColumn: null,
  functionName: null,
  inputColumns: Immutable.List(),
  inputLiterals: Immutable.List(),
  comment: null
});
export const FunctionDefinitionRecord = Immutable.Record({
  name: null,
  outputType: null,
  numInputs: null,
  inputTypes: Immutable.List(),
  description: null
});
export const DataTypeRecord = Immutable.Record({
  name: null,
  IsNumeric: null,
  IsDatetime: null,
  IsAggregate: null,
  inputTypes: Immutable.List(),
  description: null
});
export const FunctionTypesRecord = Immutable.Record({
  AggregateFunctions: Immutable.Map(),
  DateTimeFunctions: Immutable.Map(),
  MathFunctions: Immutable.Map(),
  MiscFunctions: Immutable.Map(),
  StringFunctions: Immutable.Map()
});
export const NodeSpecsRecord = Immutable.Record({
  MLNodes: Immutable.Map()
});
export const MLNodeRecord = Immutable.Record({
  name: null,
  description: null,
  nodeType: null,
  ports: Immutable.Map(),
  properties: Immutable.Map()
});
export const MLNodePropertiesRecord = Immutable.Record({
  name: null,
  datatype: null,
  description: null
});
export const InputTypeRecord = Immutable.Record({ name: null, datatype: null });
