export const MODES = [
  { label: 'BATCH', value: 'batch' },
  { label: 'STREAMING', value: 'streaming' },
  { label: 'MACHINE LEARNING', value: 'ml' },
  { label: 'DEPLOYMENT', value: 'deploy'}
]

export default MODES
