export const DEFAULT_WORKFLOW_STATE = {
  nodeSelector: {
    isActive: false,
    source: { x: 0, y: 0 },
    target: { x: 0, y: 0 }
  },
  selectedNodes: {},
  isDragged: false,
  draggedNode: null,
  isNodeEditorShown: false,
  isEdgeEditorShown: false,
  edgeEditorSamples: {},
  nodeEditorNodeId: null,
  nodeEditorNode: {},
  visualizerSamples: {},
  visualizerType: null,
  isContextMenuShown: false,
  isModalAlertShown: false,
  modalAlertTitle: '',
  modalAlertContent: '',
  modalAlertFormat: '',
  isRunning: false,
  lastRunStatus: 'UNKNOWN',
  lastRunDetails: '',
  isLastRunStatusShown: false,
  contextMenuPosition: { x: 0, y: 0 },
  anchorEl: {},
}

export default DEFAULT_WORKFLOW_STATE;
