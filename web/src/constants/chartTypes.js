export const CHART_TYPES = [
  {value: 'PieChart', label: 'Pie Chart', icon: 'pie_chart'},
  {value: 'BarChart', label: 'Bar Chart' , icon: 'equalizer' },
  {value: 'LineChart', label: 'Line Chart', icon: 'timeline'},
  {value: 'ScatterPlot', label: 'Scatter Plot', icon: 'grain'},
  {value: 'BoxPlot', label: 'Box Plot', icon: 'crop_portrait'}
];
