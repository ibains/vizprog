import React from 'react';
import Immutable from 'immutable';
import { connect } from 'react-redux';

import { Tab, Tabs } from 'react-toolbox';
import { Button } from 'react-toolbox/lib/button';
import { Layout, NavDrawer, Panel, Sidebar } from 'react-toolbox';
import Chip from 'react-toolbox/lib/chip';
import Tooltip from 'react-toolbox/lib/tooltip';
import FontIcon from 'react-toolbox/lib/font_icon';
import TextField from 'material-ui/TextField';
import AutoComplete from 'material-ui/AutoComplete';
import { IconButton } from 'react-toolbox/lib/button';
import {SortableContainer, SortableElement, SortableHandle, arrayMove} from 'react-sortable-hoc';
import { uniqueID } from '../../utils/id';

import SampleTable from 'components/SampleTable';
import FunctionOptions from 'components/FunctionOptions';

import theme from './DataCleanseEditor.scss';

const TooltipChip = Tooltip(Chip);

export class DataCleanseEditorSwitch extends React.Component {
  render() {
    return this.props.workflow.samples ? <DataCleanseEditor {...this.props} /> : <div>{'No samples available'}</div>;
  }
}

export class DataCleanseEditor extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { dialogOpen: false, func: null };
  }
  onSortEnd = ({oldIndex, newIndex}) => {
    this.props.actions.reorderCleanseFunctions( arrayMove(this.props.process.properties.get('functions', Immutable.Map()).toArray(), oldIndex, newIndex) );
  };
  render() {
    const { workflow, id, process, actions, functionDefinitions } = this.props;
    const functions = process.properties.get('functions', Immutable.Map());
    const connection = workflow.connections.find(conn => conn.tgt.process == id);
    const samples = workflow.samples.get(connection.src.process + '_' + connection.src.port);

    const firstColumnName = samples.schema.keys().next().value;
    const numRows = samples.stats.count.get(firstColumnName);
    const numColumns = samples.schema.size;

    let columnNames = [];
    samples.schema.map((type, name) => columnNames.push(name));

    const SortableItem = SortableElement(({value}) => <li> <AppliedFunction func={value} actions={actions} /> </li>);
    const SortableList = SortableContainer(({items}) => {
      return (
          <ul>
            {items.map((value, index) =>
                <SortableItem key={`item-${index}`} index={index} value={value} />
            )}
          </ul>
      );
    });
    return (
      <Layout theme={theme}>
        <NavDrawer theme={theme} active={true} pinned={true}>
          <div className={theme.header}>
            <h5>{'Prepare Sequence'}</h5>
          </div>
          <div className={theme.content}>
            { functions.size == 0 ? <span>{'No functions applied yet'}</span> : null }

            <SortableList items={functions.toArray()} onSortEnd={this.onSortEnd} useDragHandle={true} helperClass="sort_li"/>
            { this.state.func ? <NewFunction func={this.state.func} close={() => this.setState({func: null})} columns={columnNames} actions={actions} /> : null }

            <FontIcon
              onClick={() => this.setState({ dialogOpen: true })}
              style={{ cursor: 'pointer', color: '#BDBDBD', margin: '5px 0px 20px' }}
              className='material-icons'>{'add'}</FontIcon>
            { functionDefinitions.size ? <FunctionOptions
              active={this.state.dialogOpen}
              onSelect={(func) => this.setState({ func })}
              close={() => this.setState({ dialogOpen: false })}
              functions={functionDefinitions}
             /> : null }
          </div>
        </NavDrawer>
        <Panel theme={theme}>
          <div className={theme.header}>
            <h5>Dataframe</h5>
            <div className={theme.stats}>
              {numRows} rows, {numColumns} columns
            </div>
          </div>
          <SampleTable samples={samples} />
        </Panel>
      </Layout>
    );
  }
}

export class AppliedFunction extends React.Component {

  render() {

    const columnDisplay = this.props.func.get('addReplaceColumn') ?
      <div>{ this.props.func.get('addReplaceColumn') }<strong>{' is '}</strong></div> : '';

    const withDisplay = this.props.func.get('inputLiterals').toArray().length > 0 ?
      <div>{ this.props.func.get('functionName') }<strong>{' with '}</strong>{ this.props.func.get('inputLiterals').toArray().join(', ')}</div> : <div>{ this.props.func.get('functionName') }</div>

    const inputColumnsDisplay = this.props.func.get('inputColumns').toArray().length > 0 ?
      <div><strong>{' on '}</strong>{ this.props.func.get('inputColumns').toArray().join(', ') }</div> : '';

    const DragHandle = SortableHandle(() => <FontIcon value='menu' className={theme.fontIcon} />);

    return (
      <div className={theme.appliedFunction}>
        <DragHandle/>
        <div className={theme.info}>
          <div>
            <IconButton icon='close' className={'close'} onMouseUp={(event) => { this.props.actions.removeCleanseFunction(this.props.func) }}/>
            { columnDisplay }
            { withDisplay }
            { inputColumnsDisplay }
          </div>
          <small>{this.props.func.get('comment')}</small>
        </div>
      </div>
    );
  }
}

export class NewFunction extends React.Component {
  constructor(props, context) {
    super(props, context);
    let inputColumns = [];
    props.func.get('inputTypes').map( (input, k) => { inputColumns.push(input.get('name')) });
    this.state = { addReplaceColumn: '', comment: '', functionName: this.props.func.name, inputColumns: inputColumns, inputLiterals: [], id:uniqueID(), dataSource: props.columns};
    this.save = this.save.bind(this);
  }
  handleUpdateInput(value, dataSource) {
    dataSource = (dataSource.indexOf(value) > -1)?dataSource:dataSource.concat([value]);
    this.setState({
      dataSource: dataSource,
      addReplaceColumn: value
    });
  }
  save() {
    let func = this.state;
    delete func['dataSource'];
    func.inputColumns = Immutable.List(func.inputColumns);
    func.inputLiterals = Immutable.List(func.inputLiterals);
    this.props.actions.addCleanseFunction(func);
    this.props.close();
  }
  render() {
    const { addReplaceColumn, comment, dataSource, inputLiterals } = this.state;
    return (
      <div className={theme.newFunction}>
        { this.props.func.name }
        <AutoComplete
          floatingLabelText='Add/replace Column'
          value={addReplaceColumn}
          dataSource={dataSource}
          onUpdateInput={(value)=> {this.handleUpdateInput(value, dataSource)}}
        />
        <TextField floatingLabelText='Description' onChange={(e) => this.setState({comment: e.target.value})} value={comment} />
        { this.props.func.get('inputTypes').map( (input, k) => { return <InputType inputLiterals={inputLiterals} k={k} key={input.get('name')} input={input} onChange={(value, key) => { inputLiterals[key] = value; this.setState({inputLiterals: inputLiterals})}}/> }) }
        <button type="button" onClick={this.save} className="btn">Add</button>
      </div>
    );
  }
}

export const InputType = (props) => (
  <TextField onChange={(e)=>props.onChange(e.target.value, props.k)} value={props.inputLiterals[props.k]} floatingLabelText={props.input.get('name')} hintText={props.input.get('datatype') + ': ' + props.input.get('name')} />
);

export default connect(state => ({
  functionDefinitions: state.functionDefinitions,
}), null)(DataCleanseEditorSwitch);
