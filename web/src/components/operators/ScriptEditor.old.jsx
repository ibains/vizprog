import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Immutable from 'immutable'

import FlatButton from 'material-ui/FlatButton'
import DropDownMenu from 'material-ui/DropDownMenu'
import Dialog from 'material-ui/Dialog'
import { MenuItem } from 'material-ui/Menu'

import AceEditor from 'react-ace'
import 'brace/mode/java'
import 'brace/theme/github'

export class ScriptEditor extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    node: React.PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      interpreter: 'Spark'
    }
  }

  componentDidMount() {
  }

  componentWillMount() {
    console.debug('ScriptEditor componentWillMount')
  }

  componentWillReceiveProps(nextProps) {
    console.debug('ScriptEditor willReceiveProps')
  }

  onAceLoad() {
    console.log('ace loaded')
    debugger
  }

  onAceChange(newValue) {
    console.log('change', newValue);
  }

  onInterpreterChange = (event, index, value) => {
    this.setState({ interpreter: value})
  }

  render() {
    console.debug('ScriptEditor render')

    const node = this.props.node

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.props.onCancel}
      />
    ]

    const code = (node.properties && node.properties.body) ? node.properties.body.split('\\n').join('\n') : ''

    const imports = (node.properties && node.properties.imports) ?
      <div style={{
            marginTop: 10,
            marginBottom: 10,
            marginLeft: 20
          }}>
        <div>Imports</div>
        <div>{
          node.properties.imports.map(_import => {
            return <div key={_import}>{_import}</div>
          })
        }</div>
      </div> : ''

    return (
      <Dialog
        title={this.props.title}
        actions={actions}
        modal={true}
        contentStyle={{
          width: '80%',
          height: '80%',
          maxWidth: 'none',
          maxHeight: '90%'
        }}
        autoScrollBodyContent={false}
        open={this.props.isShown}
        onRequestClose={this.props.onCancel}
      >
        <div style={{
          position: 'relative'
        }}>
          <div style={{
            position: 'fixed',
            left: '24px',
            top: 0,
            right: '24px',
            zIndex: 1000,
            background: '#fff',
            height: '50px',
            borderBottom: '1px solid #ccc'
          }}>
            <DropDownMenu value={this.state.interpreter} onChange={this.onInterpreterChange}>
              <MenuItem value="SQL" primaryText="SQL"/>
              <MenuItem value="Python" primaryText="Python"/>
              <MenuItem value="R" primaryText="R"/>
              <MenuItem value="Spark" primaryText="Spark"/>
            </DropDownMenu>
          </div>

          <div style={{
            paddingTop: '30px'
          }}>
            {imports}

            <AceEditor
              mode="java"
              theme="github"
              onChange={this.onAceChange}
              name="codeEditor"
              editorProps={{$blockScrolling: true}}
              showGutter={false}
              fontSize={14}
              width="100%"
              value={code}
              showPrintMargin={false}
            />
          </div>
        </div>
      </Dialog>
    )
  }
}
