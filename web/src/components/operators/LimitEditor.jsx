import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Immutable from 'immutable'

import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import FontIcon from 'material-ui/FontIcon'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import DropDownMenu from 'material-ui/DropDownMenu'
import { MenuItem } from 'material-ui/Menu'

export class LimitEditor extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    process: React.PropTypes.object.isRequired,
    onChangeValue: React.PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount() {
  }

  componentWillMount() {
    console.debug('LimitEditor componentWillMount')
  }

  componentWillReceiveProps(nextProps) {
    console.debug('LimitEditor willReceiveProps')
  }

  render() {
    console.debug('LimitEditor render')

    const process = this.props.process
    const comment = process.properties.comment || ''

    return (
      <div>
        <div>
          <TextField fullWidth={true}
                     floatingLabelText="Comment" hintText="Comment" defaultValue={comment} multiLine={true}
                     onChange={(event) => { this.props.onChangeValue('properties.comment', event.target.value) }}
          />
        </div>
        <div>
          <TextField fullWidth={true}
                     floatingLabelText="Limit" hintText="Limit" defaultValue={process.properties.limit}
                     onChange={(event) => { this.props.onChangeValue('properties.limit', event.target.value) }}
          />
        </div>
      </div>
    )
  }
}
