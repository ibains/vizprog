import React from 'react'

import { Button } from 'react-toolbox/lib/button';
import Input from 'react-toolbox/lib/input'
import Dropdown from 'react-toolbox/lib/dropdown'

//const TEXT_STYLE = { padding: 0, margin: 0, overflow: 'auto' }
const TEXT_FIELD_STYLE = { width: '100%' }
const LIST_STYLE = { paddingTop: 0, paddingBottom: 0, margin: 0, border: 'none' }

export class ApplyStoredModelEditor extends React.Component {

  static propTypes = {
    process: React.PropTypes.object.isRequired,
    //onChange: React.PropTypes.func.isRequired
  }

  constructor(props, context) {
    super(props, context)

    const { modelType, fileName, fileSystem, credentials } = props.process.properties.toJS()
    this.state = {
      modelType,
      fileName,
      fileSystem,
      credentials
    }
  }

  onSave = () => {
    this.props.close()
  }

  modelTypes = [
    { value: 'PipelineModel', label: 'Pipeline Model' },
  ]

  fileSystems = [
    { value: 's3', label: 'S3' },
    { value: 'local', label: 'Local' }
  ]

  onChangeInput(name, value) {
    this.setState({...this.state, [name]: value});
  }

  render() {

    //const { id, process, workflow, onChange } = this.props

    return (
      <div style={{ border: '1px solid #C1C1C1', padding: 24 }}>
        <h4 style={{ marginBottom: 20, fontFamily: 'Source Sans Pro' }}>Model Details</h4>
            <div>
              <Dropdown
                auto
                label="Model Types"
                source={this.modelTypes}
                value={this.state.modelType}
                onChange={this.onChangeInput.bind(this, 'modelType')}
              />
            </div>
            <div>
              <Dropdown
                auto
                label="Storage Location"
                source={this.fileSystems}
                value={this.state.fileSystem}
                onChange={this.onChangeInput.bind(this, 'fileSystem')}
              />
            </div>
            <div>
              <Input type="text" label="Credentials" value={this.state.credentials} onChange={this.onChangeInput.bind(this, 'credentials')} />
            </div>
            <div>
              <Input type="text" label="Storage Path" value={this.state.fileName} onChange={this.onChangeInput.bind(this, 'fileName')}/>
            </div>
          <div className="clearfix">
            <div style={{ float: 'right' }}>
              <Button
                label="Load Model"
                raised
                primary
                disabled={false}
                onClick={this.onSave} />
            </div>
          </div>
      </div>
    )
  }
}
