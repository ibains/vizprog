import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Immutable from 'immutable'

import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import FontIcon from 'material-ui/FontIcon'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import DropDownMenu from 'material-ui/DropDownMenu'
import { MenuItem } from 'material-ui/Menu'

export class SplitEditor extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    process: React.PropTypes.object.isRequired,
    onChangeValue: React.PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount() {
  }

  componentWillMount() {
    console.debug('SplitEditor componentWillMount')
  }

  componentWillReceiveProps(nextProps) {
    console.debug('SplitEditor willReceiveProps')
  }

  render() {
    console.debug('SplitEditor render')

    const properties = this.props.process.properties.toJS()

    return (
      <div>
        <div>
          <TextField fullWidth={true}
                     floatingLabelText="Ratio" hintText="Ratio" defaultValue={properties.ratio}
                     onChange={(event) => { this.props.onChangeValue('properties.ratio', event.target.value) }}
          />
        </div>
        <div>
          <TextField fullWidth={true}
                     floatingLabelText="Seed" hintText="Seed" defaultValue={properties.seed}
                     onChange={(event) => { this.props.onChangeValue('properties.seed', event.target.value) }}
          />
        </div>
      </div>
    )
  }
}
