import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Immutable from 'immutable'
import { connect } from 'react-redux';

import RaisedButton from 'material-ui/RaisedButton'
import Popover from 'material-ui/Popover'
import FontIcon from 'material-ui/FontIcon'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import DropDownMenu from 'material-ui/DropDownMenu'
var FontAwesome = require('react-fontawesome');
import IconButton from 'material-ui/IconButton';
import { Button } from 'react-toolbox/lib/button'
import { MenuItem } from 'material-ui/Menu'

export class MLNodeEditor extends React.Component {

  constructor(props) {
    super(props);
    this.state = { propertyValues: []};
  }

  save() {

    //this.props.actions.addCleanseFunction(func);
    this.props.close();
  }

  render() {
    const {component, MLNodeSpecs} = this.props;
    const {propertyValues} = this.state;
    const specs = MLNodeSpecs.get(component);
    return (
      <div>
        {
          specs.get('properties').map( (property, i) => <div style={{ float: 'left', width: '48%', margin: '1%' }}>
                        <InputType propertyValues={propertyValues} k={i}
                         key={property.get('name')} property={property}
                         onChange={(value, key) => {
                           propertyValues[key] = value;
                           this.setState({propertyValues: propertyValues})
                         }}/>
                      </div>

          )
        }
        <div style={{ clear: 'both' }}></div>
        <Button style={{ float: 'right', marginTop: 10 }} label='Save' raised primary onMouseUp={ () => {this.save()} } />
      </div>
    )
  }
}

const InputType = (props) => (
  <TextField onChange={(e)=>props.onChange(e.target.value, props.k)} fullWidth={true} value={props.propertyValues[props.k]} floatingLabelFixed={true} floatingLabelText={<div><span>{props.property.get('name')}</span><IconButton tooltip="test" ><FontIcon className="info" /></IconButton></div>} hintText={props.property.get('description')} />
);

export default connect(state =>  ({
  MLNodeSpecs: state.nodeSpecs.get('MLNodes'),
}) , null)(MLNodeEditor);
