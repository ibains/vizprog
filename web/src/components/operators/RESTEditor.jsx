import React from 'react'

import Immutable from 'immutable'

import Card from '../Card';
import Column from '../Column';
import Autocomplete from 'react-toolbox/lib/autocomplete';
import Input from 'react-toolbox/lib/input';
import Dropdown from 'react-toolbox/lib/dropdown';
import Checkbox from 'react-toolbox/lib/checkbox';
import { Button, IconButton } from 'react-toolbox/lib/button';

class RESTEditor extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    const ioLabel = this.props.type.toUpperCase()

    const schemaJS = this.props.process.toJS().properties.schema

    const fieldTypes = [
      { value: 'StringType', label: 'String' },
      { value: 'DoubleType', label: 'Double' },
      { value: 'LongType', label: 'Long' },
      { value: 'BooleanType', label: 'Boolean' },
    ]

    // currently we have two different patterns for the "schema" object (one for batch and another for deploy)
    // we are not accommodating both in records.js
    // this is a temporary hack to handle the "deploy" version of "schema"
    const schema = Immutable.fromJS(schemaJS, (key, value) => {
      return value.toList()
    }).toJS()

    const schemaItems = schema.map((s) => {
      const fieldName = s[0]
      const fieldType = s[1]

      return (
        <div key={fieldName} style={{ display: 'flex', width: '100%', justifyContent: 'space-between' }}>
          <div style={{ flexGrow: 1, marginRight: 20  }}>
            <Input type="text" label='Field Name' value={fieldName} />
          </div>
          <div style={{ flexGrow: 1, marginLeft: 20 }}>
            <Dropdown label='Field Type' auto value={fieldType}
                      source={fieldTypes} onChange={ () => {} } />
          </div>
        </div>
      )
    })

    const example = '{\n' + schema.map((s) => {
      const fieldName = s[0]
      const fieldType = s[1]
      const sampleValue = ((fieldName, fieldType) => {
        switch (fieldType) {
          case 'StringType':
            return `"some ${fieldName}"`
          case 'DoubleType':
            return '1.0'
        }
      })(fieldName, fieldType)
      return (
        `  "${fieldName}": ${sampleValue}`
      )
    }).join(',\n') + '\n}\n'

    return (
      <div>
        <Column style={{width: '50%'}}>
          <Card title={`REST ${ioLabel} Schema`}>
            <div style={{ padding: 8 }}>
              { schemaItems }
              <div className="clearfix">
                <Button style={{ float: 'right', marginTop: 10 }} label='Save Schema'
                        raised primary onMouseUp={this.props.close} />
              </div>
            </div>
          </Card>
        </Column>
        <Column style={{ width: '50%' }}>
          <Card title={`Example ${ioLabel} JSON`}>
            <pre style={{ padding: 8 }}>{ example }</pre>
          </Card>
        </Column>
      </div>
    )
  }
}

export class RESTOutputEditor extends React.Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
    }
  }

  render() {
    return (
      <RESTEditor {...this.props} type="output" />
    )
  }
}

export class RESTInputEditor extends React.Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
    }
  }

  render() {
    return (
      <RESTEditor {...this.props} type="input" />
    )
  }
}
