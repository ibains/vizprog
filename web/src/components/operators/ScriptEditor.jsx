import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Immutable from 'immutable'

import { MenuItem } from 'material-ui/Menu'
import Dialog from 'react-toolbox/lib/dialog'
import Input from 'react-toolbox/lib/input'
import Dropdown from 'react-toolbox/lib/dropdown'
import {Tab, Tabs} from 'react-toolbox'
import { Button, IconButton } from 'react-toolbox/lib/button'

import AceEditor from 'react-ace'
import 'brace/mode/python'
import 'brace/mode/r'
import 'brace/mode/scala'
import 'brace/mode/sql'
import 'brace/theme/github'

import theme from './ScriptEditor.scss'
import dialogTheme from '../Dialog.scss'

const ACE_OPTIONS = {
  mode: 'scala',
  theme: 'github',
  name: 'codeEditor',
  editorProps: {$blockScrolling: true},
  showGutter: false,
  fontSize: 12,
  showPrintMargin: false,
  width: '100%',
  height: '40vh'
}

export class ScriptEditor extends React.Component {
  constructor(props, context) {
    super(props, context)
    const properties = props.process.properties.toJS()
    const ports = props.process.ports.toJS()
    this.state = {
      code: properties.body ? properties.body.split('\\n').join('\n') : '',
      language: properties.language,
      imports: properties.imports,
      name: properties.name,
      ports: ports,
      package: properties.package,
      definition: properties.definition,
      showSaveAsDialog: false,
    }
  }

  render() {
    const { code, language, imports, name } = this.state;
    return (
      <div className={theme.scriptEditor}>
        <LeftPanel {...this.state} />
        <RightPanel {...this.state} />
        <div style={{ clear: 'both' }} />
        <ScriptTabs {...this.state} showSaveAsDialog={ () => { this.setState({ showSaveAsDialog: true})} } />
        <SaveAsDialog saveAs={ (category, functionName) => { this.setState({ showSaveAsDialog: false }) } } show={ this.state.showSaveAsDialog } close={ () => { this.setState({ showSaveAsDialog: false }) } } />
      </div>
    )
  }
}

const FunctionName = (props) => (
  <div className={theme.function}>
    <div className={theme.functionHeader}>Function</div>
    <Input className={theme.functionName} value={props.name} onChange={() => {}} maxLength={40} />
  </div>
)

const Port = (props) => (
  <div className={theme.port} style={{
    borderLeft: (props.type == 'input') ? '4px solid #049DF8' : '4px solid #E778FA',
  }}>{props.port}</div>
)

const PortList = (props) => (
  <div>
    <div style={{
      float: 'left',
      width: '50%'
    }}>
      <div className={theme.portHeader}>Input Ports</div>
      <div>
        { props.ports.inputs.map(port => <Port port={port} key={port} type="input" />) }
      </div>
    </div>
    <div style={{
      float: 'left',
      width: '50%'
    }}>
      <div className={theme.portHeader}>Output Ports</div>
      <div>
        { props.ports.outputs.map(port => <Port port={port} type="output" />) }
      </div>
    </div>
  </div>
)

const LeftPanel = (props) => (
  <div className={theme.leftPanel}>
    <FunctionName {...props} />
    <PortList {...props} />
  </div>
)

const PackageName = (props) => (
  <div className={theme.package}>
    <div className={theme.packageHeader}>Package</div>
    <Input value={props.package} className={theme.packageName} />
  </div>
)

const LanguageItem = (props) => (
  <MenuItem value={props.value} primaryText={props.primaryText} style={{
    fontFamily: 'Source Sans Pro',
    fontSize: 14
  }} />
)

const LanguageSelector = (props) => {
  const languageNames = [
    { value: 'scala', label: 'Scala' },
    { value: 'sql', label: 'SQL' },
    { value: 'python', label: 'Python' },
    { value: 'r', label: 'R' }
  ]

  return (
    <div className={theme.language}>
      <div className={theme.languageHeader}>Language</div>
      <Dropdown value={props.language} onChange={props.onSelect} source={languageNames} className={theme.languageName} />
    </div>
  )
}

const ImportList = (props) => (
  <div className={theme.imports}>
    <div className={theme.importHeader}>Imports</div>
    <div>
      { props.imports.map(imp => <div className={theme.import} key={imp}>{imp}</div>) }
    </div>
  </div>
)

const RightPanel = (props) => (
  <div className={theme.rightPanel}>
    <PackageName package={props.package} />
    <LanguageSelector language={props.language} onSelect={(language) => this.setState({ language })} />
    <div style={{ clear: 'both' }}></div>
    <ImportList imports={props.imports} />
  </div>
)

class SaveAsDialog extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      category: '',
      functionName: '',
    }
  }

  render() {
    return <Dialog style={{width: 200}} type='medium' active={this.props.show} onOverlayClick={this.props.close}
                   theme={dialogTheme}>
      <IconButton icon='close' className={dialogTheme.close} onMouseUp={this.props.close}/>
      <div style={{border: '1px solid #c1c1c1', padding: 24}}>
        Share this function under the Script menu with the specified category and name:
        <Input label='Category' value={this.state.category} onChange={ (category) => this.setState({ category }) }/>
        <Input label='Function Name' value={this.state.functionName} onChange={ (functionName) => this.setState({ functionName }) }/>
        <div className='clearfix'>
          <Button style={{float: 'right'}} label='Share Now' raised primary
                  onMouseUp={ () => this.props.saveAs(this.state.category, this.state.functionName) } />
        </div>
      </div>
    </Dialog>
  }
}

class ScriptTabs extends React.Component {

  constructor(props, context) {
    super(props, context)
    this.state = {
      index: 0,
      language: props.language,
      code: props.code
    }
  }

  render() {
    return (
      <div className={theme.scriptBody}>
        <Tabs index={this.state.index} onChange={(index) => { this.setState({ index })}}>
          <Tab label="Body">
            <div className={theme.scriptEditorBox}>
              <AceEditor {...ACE_OPTIONS} mode={this.state.language} onChange={code => this.setState({ code })} value={this.state.code} />
            </div>
          </Tab>
          <Tab label="Definitions">
            <div className={theme.scriptEditorBox}>
              <AceEditor {...ACE_OPTIONS} onChange={definition => this.setState({ definition })} value={this.state.definition} />
            </div>
          </Tab>
        </Tabs>
        <div className="clearfix">
          <Button style={{ float: 'right', marginTop: 10, marginLeft: 10 }} label='Share...'
                  raised primary onMouseUp={this.props.showSaveAsDialog} />
          <Button style={{ float: 'right', marginTop: 10 }} label='Save Script' raised primary onMouseUp={ () => {} } />
        </div>
      </div>
    )
  }
}
