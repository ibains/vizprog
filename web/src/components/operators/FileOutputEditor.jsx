import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'

import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { Step, Stepper, StepLabel } from 'material-ui/Stepper';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import Toggle from 'material-ui/Toggle';
import FontIcon from 'material-ui/FontIcon';
import Dropzone from 'react-dropzone';
import * as entities from '../../shared/entities';
import _ from 'lodash';

export class FileOutputEditor extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate;

  static propTypes = {
    process: React.PropTypes.object.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      activeStep: 0,
      fileType: "CSV",
      fileLocation: "local",
      files: [],
      useHeader: true,
      trimWhitespaces: false,
      delimiter: "Comma"
    };
  }

  handleFileTypeChange = (event, index, value) => {
    this.setState({
      fileType: value
    });
  }

  handleLocationChange = (event, index, value) => {
    this.setState({
      fileLocation: value
    });
  }

  handleUseHeaderToggle = (event, value) => {
    this.setState({
      useHeader: value
    });
  }

  handleWhitespacesToggle = (event, value) => {
    this.setState({
      trimWhitespaces: value
    });
  }

  handleDelimiterChange = (event, index, value) => {
    this.setState({
      delimiter: value
    });
  }

  handleFileDrop = (files) => {
    console.log('handleFileDrop', files);

    this.setState({
      files: files
    });
  }

  render() {
    return (
      <div>
        <Stepper activeStep={this.state.activeStep}>
          <Step>
            <StepLabel>Where to Store</StepLabel>
          </Step>

          <Step>
            <StepLabel>Check/Adjust Schema</StepLabel>
          </Step>

          <Step>
            <StepLabel>Preview Data</StepLabel>
          </Step>

          <Step>
            <StepLabel>You're Done</StepLabel>
          </Step>
        </Stepper>

        <Card>
          <CardText>
            <div className="flex">
              <div className="flex-2">
                <SelectField value={this.state.fileType}
                             onChange={this.handleFileTypeChange}
                             fullWidth={true}
                             floatingLabelText="File Type">
                  <MenuItem value="CSV" primaryText="CSV" />
                  <MenuItem value="XLS" primaryText="Excel (XLS)" />
                </SelectField>
              </div>

              <div className="flex-2">
                <SelectField value={this.state.fileLocation}
                             onChange={this.handleLocationChange}
                             fullWidth={true}
                             floatingLabelText="Store">
                  <MenuItem value="local" primaryText="Desktop (local)" />
                  <MenuItem value="s3" primaryText="S3" />
                </SelectField>
              </div>
            </div>
          </CardText>
        </Card>

        <Card>
          <CardText>
            <div className="flex">
              <div className="flex-3">
                <br />
                <Toggle
                  label="Use Header for Column Title"
                  labelPosition="right"
                  onToggle={this.handleUseHeaderToggle}
                  toggled={this.state.useHeader}
                />
              </div>

              <div className="flex-3">
                <br />
                <Toggle
                  label="Trim Whitespaces"
                  labelPosition="right"
                  onToggle={this.handleWhitespacesToggle}
                  toggled={this.state.trimWhitespaces}
                />
              </div>

              <div className="flex-3">
                <SelectField value={this.state.delimiter}
                             onChange={this.handleDelimiterChange}
                             fullWidth={true}
                             floatingLabelText="Delimiter">

                  <MenuItem value="Comma" primaryText="Comma" />
                  <MenuItem value="Tab" primaryText="Tabs" />
                  <MenuItem value="Semi-Colon" primaryText="Semi-colons" />
                  <MenuItem value="Pipe" primaryText="Pipes" />

                </SelectField>
              </div>
            </div>
          </CardText>
        </Card>
      </div>
    )
  }
}
