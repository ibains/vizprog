import React from 'react';
import Immutable from 'immutable';

import Card from '../Card';
import Column from '../Column';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Add from 'material-ui/svg-icons/content/add';
import Clear from 'material-ui/svg-icons/content/clear';


import {List, ListItem} from 'material-ui/List';

import * as entities from '../../shared/entities';
const jsep = require("jsep");
const jseb = require("jseb");

const TEXT_STYLE = { padding: 0, margin: 0, height: '205px', overflow: 'auto' };
const LIST_STYLE = { paddingTop: 0, paddingBottom: 0, margin: 0, border: 'none' };
const ITEM_STYLE = { fontSize: '14px', lineHeight: 'auto', padding: '0', paddingLeft: '8px', border: 'none', color: '#000', fontFamily: "'Source Sans Pro', sans-serif", };

export default class JoinEditorSwitch extends React.Component {
  render() {
    return this.props.workflow.samples ? <JoinEditor {...this.props} /> : <div>{'No samples available'}</div>;
  }
}

export class JoinEditor extends React.Component {
  constructor(props, context) {
    super(props, context);
    const type = props.process.properties.get('joinType');
    const expression = props.process.properties.get('joinCondition');
    const selectedColumns = props.process.properties.get('selectColumns').map(this._parseColumnName);
    this.state = { selectedColumns, type, expression };
    this.addColumn = this.addColumn.bind(this);
    this.removeColumn = this.removeColumn.bind(this);
    this.save = this.save.bind(this);
    this.modifyJoin = this.modifyJoin.bind(this);
    this._parseJoinExpression = this._parseJoinExpression.bind(this);
  }
  _parseColumnName(columnName) {
    const match = /([^"]+)\("([^"]+)"\)/.exec(columnName);
    return { process: match[1], id: match[2] }
  }
  componentWillUpdate(nextProps) {
    const oldValues = this.props.process.properties.filter((v, k) => ['joinType', 'condition', 'selectColumns'].includes(k));
    const newValues = nextProps.process.properties.filter((v, k) => ['joinType', 'condition', 'selectColumns'].includes(k));
    if (!Immutable.is(oldValues, newValues)) {
      this.props.close();
    }
  }
  save() {
    const { selectedColumns, leftOperand, rightOperand, type, expression } = this.state;
    const { operator, leftColumn, rightColumn } = this._parseJoinExpression(expression);
    this.props.onChange(type, operator, leftColumn.id, rightColumn.id, selectedColumns);
  }
  _parseJoinExpression(expression) {
    const parsed = jsep(expression);
    return {
      operator: parsed.operator,
      leftColumn: { id: parsed.left.arguments[0].value, process: parsed.left.callee.name },
      rightColumn: { id: parsed.right.arguments[0].value, process: parsed.right.callee.name },
    };
  }
  _getColumnData(processId, workflow) {
    const leftConnection = workflow.connections.find(conn => conn.tgt.process == processId && conn.tgt.port == 'left');
    const rightConnection = workflow.connections.find(conn => conn.tgt.process == processId && conn.tgt.port == 'right');
    const leftColumns = workflow.samples.getIn([leftConnection.src.process + '_' + leftConnection.src.port, 'schema']).keySeq();
    const rightColumns = workflow.samples.getIn([rightConnection.src.process + '_' + rightConnection.src.port, 'schema']).keySeq();
    return { leftColumns, rightColumns };
  }
  addColumn(id, process) {
    this.setState(prev => {
      if (prev.selectedColumns.findIndex(col => col.id == id && col.process == process) == -1) {
        return { selectedColumns: prev.selectedColumns.push({ id, process }) }
      }
    });
  }
  removeColumn(id, process) {
    this.setState(prev => ({
      selectedColumns: prev.selectedColumns.filter(col => col.id != id || col.process != process),
    }));
  }
  modifyJoin({leftColumn = null, rightColumn = null, type = null, operator = null}) {
    this.setState(prevState => {
      let parsed = this._parseJoinExpression(prevState.expression);
      leftColumn = leftColumn || parsed.leftColumn;
      rightColumn = rightColumn || parsed.rightColumn;
      operator = operator || parsed.operator;
      type = type || prevState.type;
      return {
        expression: `${leftColumn.process}("${leftColumn.id}") ${operator} ${rightColumn.process}("${rightColumn.id}")`,
        type: type,
      };
    });
  }
  render() {
    const { selectedColumns, type, expression } = this.state;
    const { leftColumns, rightColumns } = this._getColumnData(this.props.id, this.props.workflow);
    const { leftColumn, rightColumn, operator } = this._parseJoinExpression(expression);
    let { connections, samples } = this.props.workflow;
    connections = connections.filter(c => c.tgt.process == this.props.id);
    return (
      <div style={{ overflow: 'auto' }}>
        <ColumnOptions connections={connections} samples={samples} selectColumn={this.addColumn} selectedColumns={selectedColumns} />
        <SelectedColumns connections={connections} removeColumn={this.removeColumn} columns={selectedColumns} />
        <JoinSelector
          type={type}
          types={entities.joinTypes}
          operator={operator}
          operators={entities.compareOperators}
          leftColumn={leftColumn}
          leftColumns={leftColumns}
          rightColumn={rightColumn}
          rightColumns={rightColumns}
          modifyJoin={this.modifyJoin}
          connections={connections}
          save={this.save} />
      </div>
    )
  }
}

export class ColumnOptions extends React.Component {
  render() {
    const { selectedColumns, connections, samples, selectColumn } = this.props;
    const leftConnection = connections.find(conn => conn.tgt.port == 'left');
    const rightConnection = connections.find(conn => conn.tgt.port == 'right');
    const leftColumns = samples.getIn([leftConnection.src.process + '_' + leftConnection.src.port, 'schema']).keySeq();
    const rightColumns = samples.getIn([rightConnection.src.process + '_' + rightConnection.src.port, 'schema']).keySeq();
    return (
      <Column style={{ paddingLeft: '0px' }}>
        <Card title={'left'} style={{ height: '275px' }} highlightColor='#0297E6' textStyle={TEXT_STYLE}>
          <ColumnList selectedColumns={selectedColumns} color='#0297E6' columns={leftColumns.map(c => ({ id: c, process: leftConnection.src.process }))} selectColumn={selectColumn} />
        </Card>
        <Card title={'right'} style={{ height: '275px' }} highlightColor='#AF3FD5' textStyle={TEXT_STYLE}>
          <ColumnList selectedColumns={selectedColumns} color='#AF3FD5' columns={rightColumns.map(c => ({ id: c, process: rightConnection.src.process }))} selectColumn={selectColumn}/>
        </Card>
      </Column>
    );
  }
};

export class SelectedColumnListItem extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { hovered: false };
  }
  render() {
    const { hovered } = this.state;
    const { leftProcess, column, removeColumn } = this.props;
    let style = Object.assign({}, ITEM_STYLE);
    style.borderTop = '1px solid #F2F2F2';
    style.borderLeft = column.process == leftProcess ? '4px solid #0297E6' : '4px solid #AF3FD5';
    style.paddingLeft = '4px';
    return <ListItem 
      onMouseEnter={() => this.setState({ hovered: true })}
      onMouseLeave={() => this.setState({ hovered: false })}
      style={style} 
      rightIcon={hovered ? <Clear /> : null} 
      primaryText={column.id} 
      onClick={removeColumn.bind(null, column.id, column.process)}
    />;
  }
}

export class SelectedColumns extends React.Component {
  render() {
    const { connections, columns, removeColumn } = this.props;
    const leftProcess = connections.find(conn => conn.tgt.port == 'left').src.process;
    const textStyle = Object.assign({}, TEXT_STYLE);
    delete textStyle.height;
    textStyle.maxHeight = '500px';
    return (
      <Column>
        <Card title='Selected Columns' style={{ height: '570px' }} textStyle={textStyle}>
          <List style={LIST_STYLE}>
            { columns.map(column => <SelectedColumnListItem leftProcess={leftProcess} column={column} removeColumn={removeColumn} /> ).toArray() }
          </List>
        </Card>
      </Column>
    );
  }
}

const OPERAND_STYLE = { float: 'left', width: '39%' };
const OPERATOR_STYLE = { float: 'left', width: '22%' };
export class JoinSelector extends React.Component {
  constructor(props, context) {
    super(props, context);
    this._selectType = this._selectType.bind(this);
    this._selectOperator = this._selectOperator.bind(this);
    this._selectLeftColumn = this._selectLeftColumn.bind(this);
    this._selectRightColumn = this._selectRightColumn.bind(this);
  }
  _selectType(event, key, value) {
    this.props.modifyJoin({ type: value });
  }
  _selectOperator(event, key, value) {
    this.props.modifyJoin({ operator: value });
  }
  _selectLeftColumn(event, key, value) {
    const process = this.props.connections.find(c => c.tgt.port == 'left').src.process;
    const column = value;
    this.props.modifyJoin({ leftColumn: { process, id: column } });
  }
  _selectRightColumn(event, key, value) {
    const process = this.props.connections.find(c => c.tgt.port == 'right').src.process;
    const column = value;
    this.props.modifyJoin({ rightColumn: { process, id: column } });
  }
  render() {
    const { type, types, operator, operators, leftColumn, rightColumn, leftColumns, rightColumns, save } = this.props;
    const textStyle = Object.assign({}, TEXT_STYLE);
    delete textStyle.height;
    const leftUnderline = { borderTop: '2px solid #0297E6' }
    const rightUnderline = { borderTop: '2px solid #AF3FD5' }
    const labelStyle = { height: '3rem', fontSize: '1.5rem', padding: 'none' };
    return (
      <Column style={{ paddingRight: '0px', float: 'right' }}>
        <Card title='Conditions' className='joinConditions' style={{ height: '570px', position: 'relative' }} textStyle={textStyle}>
          <div className='row'>
            <div>{ 'Join Type Condition:'}</div>
            <DropDownMenu className='shiftUp' labelStyle={{ height: '3rem', fontSize: '1.5rem', padding: 'none' }} value={type} onChange={this._selectType}>
              { types.map(type => <MenuItem value={type.value} primaryText={type.label} />) }
            </DropDownMenu>
          </div>
          <div className='row joinSelector'>
            <DropDownMenu autoWidth={false} underlineStyle={leftUnderline} style={{width: '35%'}} labelStyle={labelStyle} value={leftColumn.id} onChange={this._selectLeftColumn}>
              { leftColumns.map(column => <MenuItem value={column} primaryText={column} />) }
            </DropDownMenu>
            <DropDownMenu autoWidth={false} style={{ width: '30%'}} labelStyle={labelStyle} value={operator} onChange={this._selectOperator}>
              { operators.map(operator => <MenuItem value={operator.value} primaryText={operator.label} />) }
            </DropDownMenu>
            <DropDownMenu autoWidth={false} underlineStyle={rightUnderline} style={{width: '35%'}} labelStyle={labelStyle} value={rightColumn.id} onChange={this._selectRightColumn}>
              { rightColumns.map(column => <MenuItem value={column} primaryText={column} />) }
            </DropDownMenu>
          </div>
          <button type="button" style={{ position: 'absolute', bottom: '12px', width: 'calc(100% - 24px)', left: '12px' }} onClick={save} className="btn">Join Tables</button>
        </Card>
      </Column>
    );
  }
}

export class ColumnList extends React.Component {
  render() {
    const { color, columns, selectColumn, selectedColumns } = this.props;
    return (
      <List style={LIST_STYLE}>
        { columns.map(column => <ColumnListItem selectedColumns={selectedColumns} color={color} column={column} key={column.id} select={selectColumn} />) }
      </List>
    );
  }
}

export class ColumnListItem extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { hovered: false };
  }
  render() {
    const { hovered } = this.state;
    const { selectedColumns, column, select, color } = this.props;
    const disabled = selectedColumns.find(col => col.id == column.id && col.process == column.process);
    const style = Object.assign({}, ITEM_STYLE, disabled ? { color: '#CCC' } : null);
    return <ListItem 
      onMouseEnter={() => this.setState({ hovered: true })}
      onMouseLeave={() => this.setState({ hovered: false })}
      style={style} 
      rightIcon={hovered && !disabled ? <Add color={color} /> : null} 
      primaryText={column.id} 
      onClick={() => { select(column.id, column.process); this.setState({ hovered: false }); }} 
    />;
  }
}
