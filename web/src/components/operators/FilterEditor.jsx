import React from 'react'

import Card from '../Card';
import Column from '../Column';
import { compareOperators } from '../../shared/entities';
import { ASTTree } from '../expression-builder/ASTTree';
import QueryBuilder from '../query-builder/QueryBuilder'
import {List, ListItem} from 'material-ui/List';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

const TEXT_STYLE = { padding: 0, margin: 0, overflow: 'auto' };
const TEXT_FIELD_STYLE = { width: '100%' };
const LIST_STYLE = { paddingTop: 0, paddingBottom: 0, margin: 0, border: 'none' };

export default class FilterEditorSwitch extends React.Component {
  render() {
    return this.props.workflow.samples ? <FilterEditor {...this.props} /> : <div>{'No samples available'}</div>;
  }
}

export class FilterEditor extends React.Component {
  constructor(props, context) {
    super(props, context);
    let condition = props.process.properties.get('condition');
    this.state = {
      AST: condition ? QueryBuilder.getRulesFromSQL(condition) : null,
      condition: condition || null,
    };
    this.updateAST = this.updateAST.bind(this);
  }
  updateAST(AST) {
    this.setState({ AST });
    this.setState({ condition: AST.sql });
  }
  componentWillUpdate(nextProps) {
    const oldCondition = this.props.process.properties.get('condition');
    const newCondition = nextProps.process.properties.get('condition');
    if (oldCondition != newCondition) {
      this.props.close();
    }
  }
  render() {
    const { AST, condition } = this.state;
    const { id, process, workflow, onChange } = this.props;
    const connection = workflow.connections.find(conn => conn.tgt.process == id);
    const columns = workflow.samples.get(connection.src.process + '_' + connection.src.port).schema;
    const fields = {}
    columns.map((type, name) => fields[name] = {'name' : name, 'type' : type.replace("Type", "").toLowerCase()});
    return (
      <div>
        <Column style={{ height: '570px' }}>
          <Card title={'Input Schema'} highlightColor='#0297E6' textStyle={TEXT_STYLE}>
            <List style={LIST_STYLE}>
              { columns.map((type, name) => <ListItem key={name} primaryText={name} secondaryText={type.replace('Type', '')} />) }
            </List>
          </Card>
        </Column>
        <Column style={{width: '66.6%'}}>
          <Card title={'Filter Expression Builder'}>
            <TextField hintText={'Or manually enter expression'}
              value={condition}
              style={TEXT_FIELD_STYLE}
              onChange={(e) => this.setState({ condition: e.target.value })}
              onBlur={(e) => this.setState({ AST: QueryBuilder.getRulesFromSQL(e.target.value) })} />
            { AST ? <QueryBuilder fields={columns.keySeq().toArray()}
                controlClassnames={{'queryBuilder':'query-builder',
                'ruleGroup':'rules-group-container rules-group-header',
                'combinators':'group-conditions',
                'addRule': 'add-rule',
                'addGroup': 'add-group',
                'removeGroup': 'remove-group'}}
                isSQL={true}
                query={AST}
                onQueryChange={this.updateAST}/> : null }
          </Card>
          <RaisedButton
            label="Filter"
            primary={true}
            fullWidth={true}
            disabled={condition == this.props.process.properties.get('condition')}
            onClick={onChange.bind(null, condition)} />
        </Column>
      </div>
    )
  }
}
