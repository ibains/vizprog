import React from 'react';
import ReactDOM from 'react-dom';
import Immutable from 'immutable';
import AutoComplete from 'material-ui/AutoComplete';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import Popover from 'material-ui/Popover';

export default class FunctionList extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { selected: null };
  }
  render() {
    const { selected } = this.state;
    const { functions } = this.props;
    let allFunctions = functions.reduce((fns, category) => fns.merge(category), Immutable.Map());
    let currentFunction = allFunctions.find(fn => fn.name == selected);
    return (
      <div style={{ overflow: 'show', padding: '10px' }}>
        <AutoComplete
          floatingLabelText='Search functions'
          onUpdateInput={(object) => this.setState({ selected: object.text })}
          onNewRequest={(object) => this.setState({ selected: object.text })}
          searchText={selected}
          filter={AutoComplete.fuzzyFilter}
          dataSource={allFunctions.map(fn => ({ text: fn.functionName, value: <FunctionDescription name={fn.name} inputs={fn.inputTypes} /> }))}
          maxSearchResults={5}
          fullWidth={true} />
        { currentFunction ? currentFunction.inputTypes.map(col => <AutoComplete
          floatingLabelText={'Argument: ' + col.get('name')}
          filter={AutoComplete.fuzzyFilter}
          dataSource={['one','two','three','four']}
          maxSearchResults={5}
          fullWidth={true} />) : null }
        <RaisedButton label="Apply Function" primary={true} style={{ width: '100%', marginTop: '30px', marginBottom: '10px' }} disabled={!currentFunction} />
        <ul className='functionList'>
          { allFunctions.map(fn => <FunctionDisplay name={fn.name} inputs={fn.inputTypes} selected={selected} select={(selected) => this.setState({ selected })}/>) }
        </ul>
      </div>
    );
  }
}

export const FunctionDescription = (props) => <div>{ props.name + props.inputs.join(',')}</div>;

export class FunctionDisplay extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { showTooltip: false };
  }
  render() {
    const { name, comment, inputs, select, selected } = this.props;
    const tooltip = `(${name}') ${comment} ${inputs.join(',')}`
    return (
      <li onMouseEnter={() => this.setState({ showTooltip: true })} onMouseLeave={() => this.setState({ showTooltip: false })}
        onClick={select.bind(null, name)} className={name == selected ? 'selected' : null}>
        <a> { name } </a>
      </li>
    );
  }
}
