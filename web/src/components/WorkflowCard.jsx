import React from 'react'
import ReactDOM from 'react-dom'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Paper from 'material-ui/Paper'
import FontIcon from 'react-toolbox/lib/font_icon'

const modesTitleStyle = {
  fontSize: 18,
  fontWeight: 'bold',
  marginTop: 50,
  color: '#666666',

}

const titleStyle = {
  fontSize: 18,
  fontWeight: 500,
  marginTop: 10,
  color: '#666666',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
  marginRight: 10,
  marginLeft: 10
}

const labelStyle = {
  fontSize: 14
}

export class ClickableCard extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  constructor(props) {
    super(props)
    this.state = { depth: 1 }
  }

  render() {

    let style = {
      height: 120,
      width: 260,
      margin: 15,
      marginLeft: 0,
      textAlign: 'center',
      display: 'inline-block',
      cursor: 'pointer'
    }

    if (this.props.selected) {
      style.boxShadow = '0 0 10px 0 #049DF8'
    }

    return (
      <Paper style={style} zDepth={this.state.depth} onClick={ () => this.props.onSelect() }
             onMouseOver={() => this.setState({depth: 3}) } onMouseOut={() => this.setState({depth: 1}) }
      >
        {this.props.children}
      </Paper>
    )
  }
}

export class NewWorkflowCard extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  constructor(props) {
    super(props)
  }

  render() {

    const title = `${this.props.modeLabel}`

    return (
      <ClickableCard onSelect={ this.props.onSelect } selected={ this.props.selected }>
        <div style={modesTitleStyle}>
          {title}
        </div>
        <div style={{
          marginTop: 10
        }}>
        </div>
      </ClickableCard>
    )
  }
}

export class WorkflowCard extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  render() {

    const { id, title, description, author, createdAt } = this.props

    return (
      <ClickableCard onSelect={ this.props.onSelect } selected={ this.props.selected }>
        <div style={titleStyle}>
          {title}
        </div>
        <div style={{
          marginTop: 10
        }}>
          <div style={labelStyle}><i>{ description }</i></div>
          <div style={ { ...labelStyle, marginTop: 10 } }>{ (author) ? 'By ' : '' }<a href="#">{ author }</a></div>
          <div style={{ marginTop: 10, color: '#a0a0a0', fontSize: 14 }}>
            <i>{ (createdAt) ? 'Created ' : '' }{createdAt}</i>
          </div>
        </div>
      </ClickableCard>
    )
  }
}
