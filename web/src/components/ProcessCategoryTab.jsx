import React from 'react'
import Immutable from 'immutable';

import ProcessSelectionDropdown from './ProcessSelectionDropdown';
import ProcessLogo from './ProcessLogo';

import { PROCESS_DEFINITIONS } from '../constants/process';

export default class ProcessCategoryTab extends React.Component {
  _getProcessesInCategory(category) {
    const processes = Immutable.fromJS(PROCESS_DEFINITIONS).filter(p => p.get('category') == category);
    return processes.sortBy(process => process.get('label'))
  }
  _getSubcategories(processes) {
    processes = processes.filter(p => p.has('subcategory'));
    const subcategories = processes.reduce((subs, p) => subs.add(p.get('subcategory')), Immutable.Set());
    return Immutable.Map(subcategories.map(sub => [sub, processes.filter(p => p.get('subcategory') == sub)]));
  }
  render() {
    const { category, details, addProcess } = this.props;
    const processes = this._getProcessesInCategory(category);
    const subcategories = this._getSubcategories(processes);
    return (
      <li className={'dropdown ' + (subcategories.size ? 'two-column' : 'single-column')} style={{ cursor: 'pointer' }}>
        <div className="process-icon">
          <ProcessLogo category={category} height='100%' width='100%' />
        </div>
        <a style={{ color: details.get('color') }}>{details.get('label')}</a>
        <ProcessSelectionDropdown processes={processes} subcategories={subcategories} addProcess={addProcess} />
      </li>
    );
  }
}
