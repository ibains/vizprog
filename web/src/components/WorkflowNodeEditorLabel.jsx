import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import TextField from 'material-ui/TextField'

export class WorkflowNodeEditorLabel extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    label: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func.isRequired
  }

  componentDidMount() {
  }

  onChange = (event) => {
    this.props.onChange(event.target.value)
  }

  render() {
    return (
      <TextField fullWidth={true}
                 floatingLabelText="Label" hintText="Label (optional)"
                 defaultValue={this.props.label}
                 onChange={this.onChange}
      />
    )
  }
}
