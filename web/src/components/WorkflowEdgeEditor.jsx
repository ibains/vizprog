import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import FontIcon from 'material-ui/FontIcon'
import FlatButton from 'material-ui/FlatButton'
import { Table, TableBody, TableFooter, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } from 'material-ui/Table';
import TextField from 'material-ui/TextField'
import colors from 'material-ui/styles/colors'
import Plotly from 'react-plotlyjs'
import { DataCleaner } from './DataCleaner'
import Icon from 'react-fa'

export class WorkflowEdgeEditor extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  constructor(props) {
    super(props)
    const tableState = {
      fixedHeader: true,
      fixedFooter: true,
      stripedRows: false,
      showRowHover: false,
      selectable: false,
      multiSelectable: true,
      enableSelectAll: false,
      displaySelectAll: false,
      deselectOnClickaway: true,
      displayRowCheckbox: false,
      height: '100%'
    }

    this.state = {
      selectedColumn: '',
      ...tableState
    }
  }

  static propTypes = {
    samples: React.PropTypes.object.isRequired,
    isShown: React.PropTypes.bool.isRequired,
    anchorEl: React.PropTypes.object.isRequired,
    onApply: React.PropTypes.func.isRequired,
    onCancel: React.PropTypes.func.isRequired
  }

  componentDidMount() {
  }

  handleClose = (e) => {
    if (e && e.preventDefault) {
      e.preventDefault();
    }

    // TODO: Add checks here if data in popup has been changed and react accordignly
    // by asking user for confirmation if they want to lose their edits

    this.setState({
      isShown: false
    });

    if (this.props.onCancel) {
      this.props.onCancel(this);
    }
  };

  render() {

    console.log('WorkflowEdgeEditor.render', this.props);

    const anchorOrigin = {
      "horizontal":"middle",
      "vertical":"center"
    }

    const targetOrigin = {
      "horizontal":"middle",
      "vertical":"center"
    }

    const rowStyle = {
    }

    const cellStyle = {
      borderRight: '1px solid #e0e0e0'
    }

    const selectedCellStyle = {
      ...cellStyle,
      backgroundColor: colors.cyan50
    }

    const histogramSize = { width: '100%', height: 60 }

    const idCellWidth = 20

    const idCellStyle = {
      ...cellStyle,
      borderLeft: '1px solid #e0e0e0',
      backgroundColor: colors.grey600,
      color: colors.white,
      width: idCellWidth,
      textAlign: 'right'
    }

    const headerStyle = {
      textAlign: 'center'
    }

    const healthBarStyle = {
      width: '100%',
      height: 6,
      backgroundColor: colors.tealA700,
      borderRight: '1px solid #ffffff'
    }

    const columnNames = Object.keys(this.props.samples.schema)
    let columnTypes = []
    let columnData = {}

    columnNames.forEach((column, idx) => {
      columnData[idx] = []
    })

    const rows = Object.keys(this.props.samples.data).map((rowKey, idx) => {
      const rowValues = this.props.samples.data[rowKey]

      const rowData = rowValues.map((value, idx) => {
        const style = (columnNames[idx] == this.state.selectedColumn) ? selectedCellStyle : cellStyle
        columnData[idx].push(value)
        return <TableRowColumn key={rowKey + ':' + idx} style={style}>{value}</TableRowColumn>
      })
      return (
        <TableRow key={rowKey} style={rowStyle}>
          <TableRowColumn style={idCellStyle}>{idx + 1}</TableRowColumn>
          {rowData}
        </TableRow>
      )
    })

    const samples = this.props.samples

    const headerColumns = columnNames.map((column, idx) => {
      const columnType = samples.schema[column]
      columnTypes.push(columnType)

      const histogramData = samples.histogram[column]
      const nullsData = samples.nulls[column]
      const countData = samples.stats.count[column]
      const minData = samples.stats.min[column]
      const maxData = samples.stats.max[column]
      const meanData = samples.stats.mean[column]
      const stddevData = samples.stats.stddev[column]

      //debugger

      const histogram = (() => {
        if (columnType == 'IntegerType') {
          const minVal = Math.min(...columnData[idx]) || 0
          const maxVal = Math.max(...columnData[idx]) || 0
          const binSize = (maxVal - minVal) / 10.0 || 1.0

          console.debug(`bin ${column}: min=${minVal} max=${maxVal} bin size=${binSize}`)

          let xData = []
          let yData = []
          histogramData.forEach((val) => {
            xData.push(val.min)
            yData.push(val.count)
          })

          const trace1 = {
            x: xData,
            y: yData,
            type: 'bar',
            marker: {
              color: 'rgb(0, 188, 212)'
            }
          }

          const data = [trace1];
          const layout = {
            bargap: 0.25,
            width: 100,
            height: 60,
            margin: {
              b: 0,
              l: 0,
              r: 0,
              t: 0
            },
            showlegend: false
          }
          const config = {
            displayModeBar: false
          }
          return <Plotly className="whatever" data={data} layout={layout} config={config} onBeforeHover={() => { } }/>
        } else {
          return <div style={histogramSize} />
        }
      })()

      return (
        <TableHeaderColumn key={column} style={{ padding: 2 }}>
          <div style={{ width: '100%', height: 64, marginBottom: 4 }}>{histogram}</div>
          <div style={ headerStyle }>{column}</div>
          <div style={ healthBarStyle } />
        </TableHeaderColumn>
      )
    });

    const dialogTitleStyle = {
      padding: '24px 50px 12px 50px',
      height: 'auto'
    };
    const dialogBodyStyle = {
      padding: '50px 50px 50px 50px'
    };

    return (
      <Dialog open={true}
              modal={false}
              className="dialogPopup"
              titleStyle={dialogTitleStyle}
              autoScrollBodyContent={true}
              autoDetectWindowHeight={false}
              repositionOnUpdate={false}
              contentStyle={{maxWidth:'auto', width:'100%', maxWidth:'100%', height: '100%', maxHeight: '100%', transform: 'none', transition: 'none'}}
              bodyStyle={dialogBodyStyle}
              onRequestClose={this.handleClose.bind(this)}>

        <div>
          <div style={{
            padding:20,
            width: 'calc(100% - 400px)'
          }}>
            <div style={{
              marginBottom: 4
            }}>
              <Icon name='arrow-left' />
              <span style={{ marginLeft: 20 }}>X Rows</span>
              <span style={{ marginLeft: 20 }}>{columnNames.length} Columns</span>
              <div style={{ float: 'right', marginTop: -10 }}>
                <TextField
                  hintText="Search"
                />
              </div>
              <div style={{ clear: 'both' }} />
            </div>
            <Table
              height={this.state.height}
              fixedHeader={this.state.fixedHeader}
              fixedFooter={this.state.fixedFooter}
              selectable={this.state.selectable}
              multiSelectable={this.state.multiSelectable}
              enableSelectAll={this.state.enableSelectAll}
              onCellClick={(row, col) => { this.setState({ selectedColumn: columnNames[col-2]}) }}
            >
              <TableHeader displaySelectAll={this.state.displaySelectAll}>
                <TableRow>
                  {headerColumns}
                </TableRow>
              </TableHeader>
              <TableBody
                deselectOnClickaway={this.state.deselectOnClickaway}
                showRowHover={this.state.showRowHover}
                stripedRows={this.state.stripedRows}
                displayRowCheckbox={this.state.displayRowCheckbox}
              >
                {rows}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TableRowColumn colSpan={headerColumns.length} style={{textAlign: 'center'}}>
                  </TableRowColumn>
                </TableRow>
              </TableFooter>
            </Table>
            <div style={{
              position: 'absolute',
              right: 0,
              top: 0,
              width: 360
            }}>
              <DataCleaner column={this.state.selectedColumn} />
            </div>
          </div>
        </div>
        <div style={{padding: 10, textAlign: 'right'}}>
          <FlatButton secondary={true} label="Cancel"
                        onTouchTap={this.props.onCancel} />
          <RaisedButton primary={true} label="Download" onTouchTap={this.props.onApply} />
        </div>
      </Dialog>
    )
  }
}
