import React from 'react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'
import { push, replace } from 'react-router-redux'
import { Tab, Tabs } from 'material-ui/Tabs'

import NavOptions from './NavOptions'
import NavBar from './NavBar'
import BottomMenu from './BottomMenu'
import * as UserActions from '../actions/userActions'

import { API_Config } from 'constants'

export class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      open: false,
      path: '',
    }
    this._toggleMenu = this._toggleMenu.bind(this);
    this._onNavigate = this._onNavigate.bind(this);
  }

  static propTypes = {
    history: React.PropTypes.object.isRequired,
    children: React.PropTypes.element,
  }

  componentDidMount() {
    this.props.actions.getUser();
  }

  componentDidUpdate() {
    if (!this.props.user.isFetching && !this.props.user.email) {
      window.location.href = API_Config.serverUrl + API_Config.signInPath;
    }
  }

  _onNavigate(path) {
    this.props.push(path)
    this.setState({ isMenuShown: false, path })
  }

  _toggleMenu(open) {
    if (typeof(open) == 'boolean') {
      this.setState({ open });
    } else {
      this.setState(prev => ({ open: !prev.open }));
    }
  }

  render() {
    const { open } = this.state;
    if (!this.props.user.id) {
      return <div></div>
    }
    return (
      <div className="main-content hide-menu">
        <NavBar toggle={this._toggleMenu} />
        <NavOptions open={open} toggle={this._toggleMenu} onNavigate={this._onNavigate} user={this.props.user} />
        <BottomMenu onNavigate={this._onNavigate} path={this.props.location.pathname} />
        <div id="app-content" className="clearfix">
          {this.props.children}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  routerState : state.routing,
  user: state.user,
})

const mapDispatchToProps = (dispatch) => ({
  push: bindActionCreators(push, dispatch),
  replace: bindActionCreators(replace, dispatch),
  actions: bindActionCreators(UserActions, dispatch),
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
