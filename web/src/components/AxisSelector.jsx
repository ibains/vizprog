import React from 'react'

import SelectField from 'material-ui/SelectField';
import { MenuItem } from 'material-ui/Menu';

import Card from './Card';

export default class AxisSelector extends React.Component {
  render() {
    const { label, column, columns, select, value } = this.props;
    return (
      <Card title={label} subtitle={columns.get(column)} expandable>
        <SelectField floatingLabelText={label} value={column} onChange={(event, ind, val) => select(ind)} style={{width: '100%'}}>
          { columns.map((col, ind) => <MenuItem key={col} value={ind} primaryText={col} />) }
        </SelectField>
      </Card>
    );
  }
}
