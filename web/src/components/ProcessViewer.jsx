import React from 'react';
import Immutable from 'immutable';

import * as WorkflowActions from '../actions/workflowActions';

import Dialog from 'react-toolbox/lib/dialog';

import JoinEditor from './operators/JoinEditor';
import FilterEditor from './operators/FilterEditor';
import BarGraph from './BarGraph'
import BoxPlot from './BoxPlot'
import LineGraph from './LineGraph'
import PieChart from './PieChart'
import ScatterPlot from './ScatterPlot'
import Visualize from './Visualize'
import DataCleanseEditor from './operators/DataCleanseEditor';
import MLNodeEditor from './operators/MLNodeEditor'
import { ExceptEditor } from './operators/ExceptEditor'
import { FileInputEditor } from './operators/FileInputEditor'
import { FileOutputEditor } from './operators/FileOutputEditor'
import { LimitEditor } from './operators/LimitEditor'
import { OrderByEditor } from './operators/OrderByEditor'
import { ScriptEditor } from './operators/ScriptEditor'
import { SplitEditor } from './operators/SplitEditor'
import { UnionAllEditor } from './operators/UnionAllEditor'
import { RESTInputEditor, RESTOutputEditor } from './operators/RESTEditor'
import { ApplyStoredModelEditor } from './operators/ApplyStoredModelEditor'
import { componentDefinition } from './mock/componentDefinition'
import FunctionsTheme from './operators/DataCleanseEditor.scss'
import theme from './Dialog.scss';

import { IconButton } from 'react-toolbox/lib/button';

export default class ProcessViewer extends React.Component {
  render() {
    const { process, actions } = this.props;
    const close = actions.selectWorkflowProcess.bind(null, null);
    const _theme = (process && process.component == 'ApplyFunctions') ? FunctionsTheme : theme;
    return (
      <Dialog type={'large'} active={!!process} onOverlayClick={close} theme={_theme}>
        <IconButton icon='close' className={theme.close} onMouseUp={close}/>
        { process ? <ProcessSwitch {...this.props} close={close} /> : null }
      </Dialog>
    );
  }
}

// TODO: Get rid of these awful ternary operators
export class ProcessSwitch extends React.Component {
  render() {
    const { actions, ...props } = this.props;
    const process = this.props.process;
    let componentDef = componentDefinition[process.component] || [];
    return (
      <div style={{ overflow: 'auto' }}>
        { process.component == 'FileInput' ? <FileInputEditor {...props} /> : null }
        { process.component == 'FileOutput' ? <FileOutputEditor {...props} /> : null }
        { process.component == 'Join' ? <JoinEditor {...props} onChange={actions.setProcessJoin.bind(null, props.id)} /> : null }
        { process.component == 'Except' ? <ExceptEditor {...props} /> : null }
        { process.component == 'UnionAll' ? <UnionAllEditor {...props} /> : null }
        { process.component == 'Limit' ? <LimitEditor {...props} /> : null }
        { process.component == 'Split' ? <SplitEditor {...props} /> : null }
        { process.component == 'OrderBy' ? <OrderByEditor {...props} /> : null }
        { process.component == 'Script' ? <ScriptEditor {...props} /> : null }
        { process.component == 'Filter' ? <FilterEditor {...props} onChange={actions.setProcessFilter.bind(null, props.id)} /> : null }
        { process.component == 'RESTInput' ? <RESTInputEditor {...props} /> : null }
        { process.component == 'RESTOutput' ? <RESTOutputEditor {...props} /> : null }
        { process.component == 'ApplyStoredModel' ? <ApplyStoredModelEditor {...props} /> : null }
        { process.component == 'BarChart' ? <BarGraph {...props} onChange={actions.setBarChartColumns.bind(null, props.id)} /> : null }
        { process.component == 'BoxPlot' ? <BoxPlot {...props} onChange={actions.setBoxPlotColumn.bind(null, props.id)} /> : null }
        { process.component == 'LineChart' ? <LineGraph {...props} onChange={actions.setLineChartColumns.bind(null, props.id)}/> : null }
        { process.component == 'PieChart' ? <PieChart {...props} onChange={actions.setPieChartColumns.bind(null, props.id)} /> : null }
        { process.component == 'ScatterPlot' ? <ScatterPlot {...props} onChange={actions.setScatterPlotColumns.bind(null, props.id)} /> : null }
        { process.component == 'Visualize' ? <Visualize {...props} onChange={actions.setVisualizeColumns.bind(null, props.id)} /> : null }
        { process.component == 'ApplyFunctions' ? <DataCleanseEditor {...props} actions={_.mapValues(actions, a => a.bind(null, props.id))} onChange={() => alert('foo')} /> : null }
        { componentDef.category == 'ml' ? <MLNodeEditor {...props} component={process.component} /> : null }
      </div>
    );
  }
}
