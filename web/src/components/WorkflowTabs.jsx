import React from 'react'

import Workflow from './Workflow'
import theme from './WorkflowTabs.scss';

import { Tab, Tabs } from 'react-toolbox';
import { Button, IconButton } from 'react-toolbox/lib/button';

export default class WorkflowTabs extends React.Component {
  render() {
    const { actions, workflows, workflowID, removeWorkflow } = this.props;
    const workflowOrder = [...workflows.keys()]; // We don't really care about the order right now
    const onChange = (index) => actions.selectWorkflow(workflowOrder[index]);
    const activeTab = workflowOrder.indexOf(workflowID);
    return (
      <Tabs onChange={onChange} index={activeTab} theme={theme}>
        { workflows.map((workflow, id) => (
            <Tab
              theme={theme}
              label={<TabLabel theme={theme} name={workflow.name} close={removeWorkflow.bind(null, id)} />}>
              <Workflow workflow={workflow} id={id} />
            </Tab>
          )) }
      </Tabs>
    );
  }
}

// Using button instead of IconButton component because
// the IconButton theming doesn't seem to work correctly
const TabLabel = (props) => {
  const { name, close } = props;
  return (
    <span>
      {name}
      <button className='close-btn'>
        <i className='material-icons md-9' onClick={(event) => { event.preventDefault(); close() }}>close</i>
      </button>
    </span>
  );
}
