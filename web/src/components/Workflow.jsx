import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Chip from 'react-toolbox/lib/chip';
import { Snackbar } from 'react-toolbox';

import WorkflowPane from './WorkflowPane';
import RunWorkflowMenu from './RunWorkflowMenu';
import ProcessViewer from './ProcessViewer';
import ConnectionViewer from './ConnectionViewer';

import _ from 'lodash';

import { connectWorkflowToJsPlumb } from '../utils/jsplumb';
import * as WorkflowActions from '../actions/workflowActions';
import * as SampleActions from '../actions/sampleActions';
import * as SpecActions from '../actions/specActions';

import modes from 'constants/modes'

const WorkflowModeBadge = (props) => {
  const mode = modes.find(mode => { return mode.value == props.mode} )

  const label = mode.label
  let deployUrlLabel = ''
  // TODO: remove this temporary demo hack
  if (props.workflow.deployUrl) {
    deployUrlLabel = <span> - Running at <b>{props.workflow.deployUrl}</b></span>
  }

  return (mode) ? <Chip style={{ backgroundColor: '#', position: 'absolute', top: 10, right: 10 }}>{ label }{deployUrlLabel}</Chip> : <div></div>
}

export class Workflow extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      notificationMessage: ''
    }
  }

  clearNotificationMessage = () => {
    this.setState({ notificationMessage: '' })
  }

  // TODO: clean up this temporary demo workaround
  componentWillReceiveProps(nextProps) {
    if (nextProps.workflow.deployUrl != this.props.workflow.deployUrl) {
      this.setState({ notificationMessage: 'The model was deployed successfully at ' + nextProps.workflow.deployUrl})
    } else if (!nextProps.workflow.isRunning && this.props.workflow.isRunning) {
      this.setState({ notificationMessage: 'The workflow executed successfully' })
    }
  }

  render() {
    // Note: dragging and onDragEnd both come from jsplumb higher order component
    const { id, workflow, sampleID, processID, functionDefinitions, actions, dragging, onDragEnd } = this.props;
    const boundActions = _.mapValues(actions, action => action.bind(null, id));
    const { samples, processes, connections, metainfo } = workflow.content.graph;
    const connection = connections.find(conn => {
      const [process, port] = (sampleID || '').split('_');
      return conn.src.process == process && conn.src.port == port;
    });
    const mode = metainfo.get('mode')
    const notificationMessage = this.state.notificationMessage

    return (
      <div className='workflow'>
        <WorkflowPane workflow={workflow.content.graph} actions={boundActions} dragging={dragging} onDragEnd={onDragEnd} />
        { processID ? <ProcessViewer workflow={workflow.content.graph} id={processID} process={processes.get(processID)} actions={boundActions} /> : null }
        { sampleID ? <ConnectionViewer connection={connection} samples={samples.get(sampleID)} actions={boundActions} /> : null }
        { processes ? <RunWorkflowMenu running={workflow.get('isRunning')} start={boundActions.startWorkflowExecution} /> : null }
        <WorkflowModeBadge mode={ mode } workflow={workflow}/>
        <Snackbar
          action='Dismiss'
          active={notificationMessage != ''}
          icon='question_answer'
          label={notificationMessage}
          timeout={5000}
          onClick={this.clearNotificationMessage}
          onTimeout={this.clearNotificationMessage}
          type='cancel'
        />
      </div>
    );
  }
}

let mapDispatchToProps = dispatch => ({
  actions: Object.assign({},
     bindActionCreators(SampleActions, dispatch),
     bindActionCreators(SpecActions, dispatch),
     bindActionCreators(WorkflowActions, dispatch)),
});
Workflow = connectWorkflowToJsPlumb(Workflow);
export default connect(state => ({
  processes: state.processes,
  processID: state.processID,
  sampleID: state.sampleID,
  connections: state.connections,
}), mapDispatchToProps)(Workflow);
