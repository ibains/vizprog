import React from 'react'
import COLORS from '../utils/colors';

import SvgIcon from 'material-ui/SvgIcon';

const ICON_STYLE = {padding: '10px', height: '40px', width: '40px', cursor: 'pointer', display: 'inline'};

export const ColorIcon = (props) => {
    return (
        <SvgIcon {...Object.assign({style: ICON_STYLE}, props)}>
            <rect fill={props.color} width='100%' height='100%' />
        </SvgIcon>
    );
}

const ColorSelector = (props) => (
    <div className='colorSelector' style={{textAlign: 'center', width: '256px'}}>
        { (props.colors || COLORS).map(color => <ColorIcon onClick={props.onSelect.bind(null, color)} color={color} />) }
    </div>
);

export default ColorSelector;
