import React from 'react';
import theme from './Box.scss'

const Box = (props) => {
  return (
    <div className={theme.box} style={props.style}>
      { props.children }
    </div>
  );
};

export default Box;
