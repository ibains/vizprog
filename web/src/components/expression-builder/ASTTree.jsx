import React from 'react'
import {LogicalExpression} from './LogicalExpression';
import {BinaryExpression} from './BinaryExpression';

export class ASTTree extends React.Component {

  static propTypes = {
    node: React.PropTypes.object.isRequired,
    columns: React.PropTypes.array.isRequired,
    operators: React.PropTypes.array.isRequired,
    onChange: React.PropTypes.func.isRequired
  };

  constructor(props) {
    super(props)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (nextProps.node != this.props.node);
  }

  onChange(nodeRef, node) {
    if (this.props.onChange) {
      this.props.onChange(nodeRef, node);
    }
  }

  render() {
    let node = this.props.node;
    let tree = '';

    if (node) {
      if (node.type == "LogicalExpression") {
        tree = (
          <LogicalExpression ast={this}
                             node={node}
                             onChange={this.onChange}
          />
        );
      }
      else if (node.type == "BinaryExpression") {
        tree = (
          <dl className="rules-group-container">
            <dt className="rules-group-header">
              <div className="group-conditions">
                Expression
              </div>
            </dt>
            <dd className="rules-group-body">
              <ul className="rules-list">
                <li className="rule-container">
                  <BinaryExpression ast={this}
                                     node={node}
                                     onChange={this.onChange}
                  />
                </li>
              </ul>
            </dd>
          </dl>
        );
      }
      else {
        tree = 'Unknown type of expression node';
      }
    }

    return (
      <div className="query-builder">
        {tree}
      </div>
    )
  }
}
