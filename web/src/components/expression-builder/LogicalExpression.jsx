import React from 'react'
import {BinaryExpression} from './BinaryExpression';

export class LogicalExpression extends React.Component {

  static propTypes = {
    ast: React.PropTypes.object.isRequired,
    node: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.setState({
      operator: this.props.node.operator
    })
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      operator: nextProps.node.operator
    })
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.modified) {
      let node = Object.assign({}, this.props.node, { operator: this.state.operator });
      this.props.ast.onChange(this.props.node, node);
      this.modified = false;
    }
  }

  onChangeOperator = (event) => {
    event.preventDefault();

    let className = event.target.className.toLowerCase().split(" ");
    if (className.indexOf("and") >= 0) {
      this.setState({
        operator: "&&"
      });
      this.modified = true;
    }
    else if (className.indexOf("or") >= 0) {
      this.setState({
        operator: "||"
      });
      this.modified = true;
    }
  }

  render() {
    let node = this.props.node;

    let left = '';
    if (node.left.type == "LogicalExpression") {
      left = (
        <LogicalExpression ast={this.props.ast}
                           node={node.left}
        />
      )
    }
    else if (node.left.type == "BinaryExpression") {
      left = (
        <li className="rule-container">
          <BinaryExpression ast={this.props.ast}
                            node={node.left}
          />
        </li>
      )
    }

    let right = '';
    if (node.right.type == "LogicalExpression") {
      right = (
        <LogicalExpression ast={this.props.ast}
                           node={node.right}
        />
      )
    }
    else if (node.right.type == "BinaryExpression") {
      right = (
        <li className="rule-container">
          <BinaryExpression ast={this.props.ast}
                            node={node.right}
          />
        </li>
      )
    }

    return (
      <dl className="rules-group-container">
        <dt className="rules-group-header">
          <div className="group-conditions">
            <label className={(this.state.operator == "&&") ? "and active" : "and"} onClick={this.onChangeOperator}>AND</label>
            <label className={(this.state.operator == "||") ? "or active" : "or"} onClick={this.onChangeOperator}>OR</label>
          </div>
        </dt>
        <dd className="rules-group-body">
          <ul className="rules-list">
            {left}
            {right}
          </ul>
        </dd>
      </dl>
    )
  }
}
