import React from 'react'
import Autocomplete from '../../shared/Autocomplete';

export class BinaryExpression extends React.Component {

  static propTypes = {
    ast: React.PropTypes.object.isRequired,
    node: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super(props)
    this.state = {
      operator: ''
    }
  }

  componentDidMount() {
    let node = this.props.node;

    this.setState({
      operator: node.operator,
      leftType: node.left.type,
      leftName: node.left.name,
      leftValue: node.left.value,
      leftRawValue: node.left.raw,
      rightType: node.right.type,
      rightName: node.right.name,
      rightValue: node.right.value,
      rightRawValue: node.right.raw
    });
  }

  componentWillReceiveProps(nextProps) {
    let node = nextProps.node;

    this.setState({
      operator: node.operator,
      leftType: node.left.type,
      leftName: node.left.name,
      leftValue: node.left.value,
      leftRawValue: node.left.raw,
      rightType: node.right.type,
      rightName: node.right.name,
      rightValue: node.right.value,
      rightRawValue: node.right.raw
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.modified) {
      let node = Object.assign({}, this.props.node);
      let state = this.state;

      node.operator = state.operator;

      Object.assign(node.left, {
        type: state.leftType,
        name: state.leftName,
        value: state.leftValue,
        raw: state.leftRawValue
      });

      Object.assign(node.right, {
        type: state.rightType,
        name: state.rightName,
        value: state.rightValue,
        raw: state.rightRawValue
      });

      this.props.ast.props.onChange(this.props.node, node);
      this.modified = false;
    }
  }

  onChangeOperator = (event) => {
    if (event.target.value != this.state.operator) {
      this.setState({
        operator: event.target.value
      });
      this.modified = true;
    }
  }

  onEditField = (side, value) => {
    let ast = this.props.ast;
    let astProps = ast.props;

    if (astProps.columns.indexOf(value)) {
      // Potentially column ID - interpret as Identifier
      this.setState({
        [side + "Type"]: "Identifier",
        [side + "Name"]: value,
        [side + "Value"]: null,
        [side + "RawValue"]: null
      });
    }
    else {
      // Looks like regular literal
      this.setState({
        [side + "Type"]: "Literal",
        [side + "Name"]: null,
        [side + "Value"]: value,
        [side + "RawValue"]: value
      });
    }
    this.modified = true;
  }

  onSelectField = (side, value) => {
    this.setState({
      [side + "Type"]: "Identifier",
      [side + "Name"]: value,
      [side + "Value"]: null,
      [side + "RawValue"]: null
    });
    this.modified = true;
  }

  render() {
    let state = this.state;
    let ast = this.props.ast;
    let astProps = ast.props;

    let leftValue = '';
    if (state.leftType == "Identifier") {
      leftValue = state.leftName;
    }
    else if (state.leftType == "Literal") {
      leftValue = state.leftRawValue;
    }

    let rightValue = '';
    if (state.rightType == "Identifier") {
      rightValue = state.rightName;
    }
    else if (state.rightType == "Literal") {
      rightValue = state.rightRawValue;
    }

    return (
      <div className="expression">
        <div className="operand">
          <Autocomplete
            value={leftValue}
            items={astProps.columns}
            getItemValue={ (item) => item }
            onChange={(event, value) => { this.onEditField('left', value) } }
            onSelect={value => { this.onSelectField('left', value) } }
            renderItem={(item, isHighlighted) => (
           <div key={item}>{item}</div>
          )}
          />
        </div>

        <div className="operator">
          <select value={this.state.operator}
                  onChange={this.onChangeOperator}>
            {
              astProps.operators.map(op => {
                return (
                  <option key={op.value} value={op.value}>{op.label}</option>
                )
              })
            }
          </select>
        </div>

        <div className="operand">
          <Autocomplete
            value={rightValue}
            items={astProps.columns}
            getItemValue={ (item) => item }
            onChange={(event, value) => { this.onEditField('right', value) } }
            onSelect={value => { this.onSelectField('right', value) } }
            renderItem={(item, isHighlighted) => (
             <div key={item} className={"autocompleteDropdownItem" + (isHighlighted ? " highlight" : "")}>{item}</div>
            )}
          />
        </div>
      </div>
    )
  }
}
