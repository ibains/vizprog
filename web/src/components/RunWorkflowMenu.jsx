import React from 'react'

import FontIcon from 'material-ui/FontIcon'
import CircularProgress from 'material-ui/CircularProgress'

import FloatingActionButton from 'material-ui/FloatingActionButton'

const MENU_STYLE = {
  position: 'absolute',
  bottom: 0,
  right: 0,
  zIndex: 1000,
  margin: '30px',
}

const BUTTON_STYLE = {
  marginTop: 8,
  boxShadow: '0 10px 20px rgba(0,0,0,0.19),0 6px 6px rgba(0,0,0,0.23)',
}

const ICON_STYLE = { fontSize: '35px' };

export default class RunWorkflowMenu extends React.Component {
  render() {
    const { start, stop, running } = this.props;
    return (
      <div style={MENU_STYLE}>
        <FloatingActionButton backgroundColor="#3023AE" mini={false} onClick={start} style={BUTTON_STYLE} iconStyle={ICON_STYLE}>
	  <span>
            <RunWorkflowStatus running={running} />
	  </span>
        </FloatingActionButton>
      </div>
    );
  }
}

export const RunWorkflowStatus = (props) => {
  if (props.running) {
    return <CircularProgress color='white' size={0.5} style={{ marginTop: 4 }} />;
  } else {
    return <FontIcon color='white' className='material-icons'>{'play_arrow'}</FontIcon>;
  }
}
