import React from 'react';

import MaterialUIDialog from 'material-ui/Dialog';
import CloseDialogButton from './CloseDialogButton';

const CONTENT_STYLE = {
  maxWidth: '1100px',
  overflowY: 'auto',
  maxHeight: '100vh'
};

const Dialog = (props) => (
  <MaterialUIDialog className='dialogPopup' {...props}
    modal={false}
    displaySelectAll={false}
    autoDetectWindowHeight={false}
    contentStyle={Object.assign({}, CONTENT_STYLE, props.contentStyle || {})}>
    {props.children}
    <CloseDialogButton close={props.onRequestClose} />
  </MaterialUIDialog>
);

export default Dialog;
