import React from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import { CHART_TYPES } from '../constants/chartTypes';
import theme from './ChartSelector.scss'


export default class ChartSelector extends React.Component {
  render() {
    const { value, select } = this.props;
    const selected = (value)?value:CHART_TYPES[0].value;
    return (
      <div className={theme.buttonGroup} style={{display: 'flex', flexDirection: 'row', padding: '5px 5px 15px'}}>
        {
          CHART_TYPES.map((chartType, i) => {
            const color = (selected === chartType.value)?'#0096FF':'#C1C1C1';
            return ( <div style={{display: 'flex', flexDirection: 'column', width: '20%', margin: '1px'}} key={i}>
                <RaisedButton className={theme.raisedButton} onClick={() => {select(chartType.value)}}
                icon={<FontIcon className="material-icons" style={{color: color}}>{chartType.icon}</FontIcon>}
                />
              </div>
            )
          })
        }
      </div>
    );
  }
}
