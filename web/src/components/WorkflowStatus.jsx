import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

export class WorkflowStatus extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    isShown: React.PropTypes.bool.isRequired,
    content: React.PropTypes.string.isRequired,
    onClose: React.PropTypes.func.isRequired
  }

  componentDidMount() {
  }

  render() {
    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.props.onClose}
      />
    ];

    return (
      <Dialog
        title="Workflow Status Details"
        actions={actions}
        modal={false}
        contentStyle={{
          width: '80%',
          maxWidth: 'none'
        }}
        autoScrollBodyContent={true}
        open={this.props.isShown}
        onRequestClose={this.props.onClose}
      >
        <code style={{
          fontSize: 12
        }}>
          {this.props.content}
        </code>
      </Dialog>
    )
  }
}
