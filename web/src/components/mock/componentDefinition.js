export const componentDefinition = {
  "FileInput": {
    "label": "File Reader",
    "category": "io",
    "properties": {
      "fileName": null,
      "fileType": "csv",
      "header": "true",
      "inferSchema": "true"
    },
    "ports": {
      "inputs": [],
      "outputs": [
        "out"
      ]
    }
  },
  "MultiFileInput": {
    "label": "Multiply Files Input",
    "category": "io",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": []
    }
  },
  "FileOutput": {
    "label": "File Writer",
    "category": "io",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": []
    }
  },

  "Join": {
    "label": "Join",
    "category": "join-split",
    "properties": {
      "joinCondition": null,
      "joinType": null
    },
    "ports": {
      "inputs": [
        "left",
        "right"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Split": {
    "label": "Split",
    "category": "join-split",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },

  "Select": {
    "label": "Select",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Filter": {
    "label": "Filter",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Limit": {
    "label": "Limit",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "UnionAll": {
    "label": "Union All",
    "category": "transform",
    "ports": {
      "inputs": [
        "left",
        "right"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "OrderBy": {
    "label": "OrderBy",
    "category": "transform",
    "properties": {
      "columns": null
    },
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Intersect": {
    "label": "Intersect",
    "category": "transform",
    "ports": {
      "inputs": [
        "left",
        "right"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Except": {
    "label": "Except",
    "component": "except",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Distinct": {
    "label": "Distinct",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Sample": {
    "label": "Sample",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Aggregate": {
    "label": "Aggregate",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Cube": {
    "label": "Cube",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "WindowFunction": {
    "label": "Window Function",
    "category": "transform",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  /*
   "DropMissing": {
   "label": "Drop Missing",
   "category": "transform",
   "ports": {
   "inputs": [
   "in"
   ],
   "outputs": [
   "out"
   ]
   }
   },
   */
  /*
   "AddColumn": {
   "label": "Add Column",
   "category": "transform",
   "ports": {
   "inputs": [
   "in"
   ],
   "outputs": [
   "out"
   ]
   }
   },
   */
  },

  "Script": {
    "label": "Script",
    "category": "script",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Cleanse": {
    "label": "Cleanse",
    "category": "script",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "BarGraph": {
    "label": "Bar Graph",
    "category": "visualize",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
      ]
    }
  },
  "LineGraph": {
    "label": "Line Graph",
    "category": "visualize",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
      ]
    }
  },
  "BoxPlot": {
    "label": "Box Plot",
    "category": "visualize",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
      ]
    }
  },
  "ScatterPlot": {
    "label": "Scatter Plot",
    "category": "visualize",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
      ]
    }
  },
  "PieChart": {
    "label": "Pie Chart",
    "category": "visualize",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
      ]
    }
  },
  'Visualize': {
    'label': 'Visualize',
    'category': 'visualize',
    'ports': {
      'inputs': [
        'in'
      ],
      'outputs': [
      ]
    }
  },
  "LogisticRegression": {
    "label": "Logistic Regression",
    "category": "ml",
    "subcategory": "Classification",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "DecisionTrees": {
    "label": "Decision Trees",
    "category": "ml",
    "subcategory": "Classification",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "GradientBoostTrees": {
    "label": "Gradient Boost Trees",
    "category": "ml",
    "subcategory": "Classification",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "NaiveBayes": {
    "label": "Naive Bayes",
    "category": "ml",
    "subcategory": "Classification",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "MultilayerPerceptron": {
    "label": "Multilayer Perceptron",
    "category": "ml",
    "subcategory": "Classification",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Probablistic": {
    "label": "Probablistic",
    "category": "ml",
    "subcategory": "Classification",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "RandomForest": {
    "label": "Random Forest",
    "category": "ml",
    "subcategory": "Classification",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "ALS": {
    "label": "ALS",
    "category": "ml",
    "subcategory": "Recommendation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "LDA": {
    "label": "LDA",
    "category": "ml",
    "subcategory": "Clustering",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "KMeans": {
    "label": "KMeans",
    "category": "ml",
    "subcategory": "Clustering",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "AFTSurvivalRegression": {
    "label": "AFT Survival",
    "category": "ml",
    "subcategory": "Regression",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "DecisionTree": {
    "label": "Decision Tree",
    "category": "ml",
    "subcategory": "Regression",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "GBT": {
    "label": "GBT",
    "category": "ml",
    "subcategory": "Regression",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "IsotonicRegression": {
    "label": "Isotonic Regression",
    "category": "ml",
    "subcategory": "Regression",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "LinearRegression": {
    "label": "Linear Regression",
    "category": "ml",
    "subcategory": "Regression",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "RandomForestReg": {
    "label": "Random Forest",
    "category": "ml",
    "subcategory": "Regression",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Binarizer": {
    "label": "Binarizer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Bucketizer": {
    "label": "Bucketizer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "ChiSquareSelector": {
    "label": "Chi Square Selector",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "CountVectorizer": {
    "label": "Count Vectorizer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "DCT": {
    "label": "DCT",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "ElementwiseProduct": {
    "label": "Elementwise Product",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "HashingTF": {
    "label": "Hashing TF",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "IDF": {
    "label": "IDF",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "IndexToString": {
    "label": "Index To String",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "MinMaxScaler": {
    "label": "Min Max Scaler",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "NGram": {
    "label": "N Gram",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Normalizer": {
    "label": "Normalizer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "OneHotEncoder": {
    "label": "One Hot Encoder",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "PCA": {
    "label": "PCA",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "PolynomialExpansion": {
    "label": "Polynomial Expansion",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "QuantileDiscretizer": {
    "label": "Quantile Discretizer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "RegexTokenizer": {
    "label": "Regex Tokenizer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Rformula": {
    "label": "Rformula",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "SQLTransformer": {
    "label": "SQL Transformer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "StandardScaler": {
    "label": "Standard Scaler",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "StopWordsRemover": {
    "label": "Stop Words Remover",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "StringIndexer": {
    "label": "String Indexer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Tokenizer": {
    "label": "Tokenizer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "VectorAssembler": {
    "label": "Vector Assembler",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "VectorIndexer": {
    "label": "Vector Indexer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "VectorSlicer": {
    "label": "Vector Slicer",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "Word2Vec": {
    "label": "Word 2 Vec",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "ModelBuilder": {
    "label": "Model Builder",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "ModelApplier": {
    "label": "Model Applier",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "DirectParameter": {
    "label": "Direct Parameter",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "ApplyFunctions": {
    "label": "Prepare",
    "category": "script",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  },
  "BinaryClassificationEvaluator": {
    "label": "Binary Classification Evaluator",
    "category": "ml",
    "subcategory": "Feature Generation",
    "ports": {
      "inputs": [
        "in"
      ],
      "outputs": [
        "out"
      ]
    }
  }
}
