export const graph = {
  "graph": {
    "metainfo": {
      "id": "a_simple_graph",
      "memory": 1,
      "processors": 1,
      "cluster": "local",
      "mode": "batch"
    },
    "inports": {},
    "outports": {},
    "groups": [],
    "processes": {
      "n1": {
        "component": "FileInput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "artists"
        },
        "properties": {
          "fileType": "csv",
          "header": "true",
          "inferSchema": "true",
          "fileName": "/vagrant/tmp/resources/artists.csv"
        },
        "ports": {
          "inputs": [],
          "outputs": [
            "out"
          ]
        }
      },
      "n2": {
        "component": "FileInput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "user_artists"
        },
        "properties": {
          "fileType": "csv",
          "header": "true",
          "inferSchema": "true",
          "fileName": "/vagrant/tmp/resources/user_artists.csv"
        },
        "ports": {
          "inputs": [],
          "outputs": [
            "out"
          ]
        }
      },
      "n4": {
        "component": "FileOutput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "write_user_artists"
        },
        "properties": {
          "fileType": "csv",
          "fileName": "/vagrant/tmp/resources/write_user_artists.csv"
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": []
        }
      },
      "n3": {
        "component": "Join",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "joined_user_artists"
        },
        "properties": {
          "joinType": "outer",
          "joinCondition": "n2(\"artistID\") === n1(\"id\")"
        },
        "ports": {
          "inputs": [
            "left",
            "right"
          ],
          "outputs": [
            "out"
          ]
        }
      },
      "n5": {
        "component": "Filter",
        "metadata": {
          "y": "100",
          "x": "100",
          "label": "smaller_artists"
        },
        "properties": {
          "comment": "Filter using boolean expression on one or more columns",
          "condition": "id < 100"
        },
        "ports": {
          "outputs": ["out"],
          "inputs": ["in"]
        }
      },
      "n6": {
        "component": "Except",
        "metadata": {
          "y": "100",
          "x": "100",
          "label": "bigger_artists"
        },
        "properties": {
          "comment": "Rows in one that are not present in two"
        },
        "ports": {
          "outputs": ["out"],
          "inputs": ["one", "two"]
        }
      },
      "n7": {
        "component": "UnionAll",
        "metadata": {
          "y": "100",
          "x": "100",
          "label": "all_combined_artists"
        },
        "properties": {
          "comment": "Combine both inputs, duplicates are kept"
        },
        "ports": {
          "outputs": ["out"],
          "inputs": ["one", "two"]
        }
      },
      "n8": {
        "component": "Limit",
        "metadata": {
          "y": "100",
          "x": "100",
          "label": "top20"
        },
        "properties": {
          "comment": "Only keep Limit number of rows",
          "limit": 20
        },
        "ports": {
          "outputs": ["out"],
          "inputs": ["in"]
        }
      },
      "n9": {
        "component": "Split",
        "metadata": {
          "y": "100",
          "x": "100",
          "label": "splitter"
        },
        "properties": {
          "comment": "Only keep Limit number of rows",
          "ratio": 0.4,
          "seed": 37
        },
        "ports": {
          "outputs": ["one", "two"],
          "inputs": ["in"]
        }
      },
      "n10": {
        "component": "OrderBy",
        "metadata": {
          "y": "100",
          "x": "100",
          "label": "sorter"
        },
        "properties": {
          "comment": "Sort rows by given columns/expressions",
          "columns": ["weight"]
        },
        "ports": {
          "outputs": ["out"],
          "inputs": ["in"]
        }
      },
      "n11": {
        "component": "FileOutput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "write_something"
        },
        "properties": {
          "fileType": "csv",
          "fileName": "/vagrant/tmp/resources/one.csv"
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": []
        }
      }
    },
    "connections": [
      {
        "src": {
          "process": "n2",
          "port": "out"
        },
        "tgt": {
          "process": "n3",
          "port": "left"
        },
        "metadata": {
          "route": "e1"
        }
      },
      {
        "src": {
          "process": "n1",
          "port": "out"
        },
        "tgt": {
          "process": "n3",
          "port": "right"
        },
        "metadata": {
          "route": "e2"
        }
      },
      {
        "src": {
          "process": "n3",
          "port": "out"
        },
        "tgt": {
          "process": "n5",
          "port": "in"
        },
        "metadata": {
          "route": "e3"
        }
      },
      {
        "src": {
          "process": "n5",
          "port": "out"
        },
        "tgt": {
          "process": "n6",
          "port": "two"
        },
        "metadata": {
          "route": "e4"
        }
      },
      {
        "src": {
          "process": "n3",
          "port": "out"
        },
        "tgt": {
          "process": "n6",
          "port": "one"
        },
        "metadata": {
          "route": "e5"
        }
      },
      {
        "src": {
          "process": "n5",
          "port": "out"
        },
        "tgt": {
          "process": "n7",
          "port": "one"
        },
        "metadata": {
          "route": "e6"
        }
      },
      {
        "src": {
          "process": "n6",
          "port": "out"
        },
        "tgt": {
          "process": "n7",
          "port": "two"
        },
        "metadata": {
          "route": "e7"
        }
      },
      {
        "src": {
          "process": "n7",
          "port": "out"
        },
        "tgt": {
          "process": "n8",
          "port": "in"
        },
        "metadata": {
          "route": "e8"
        }
      },
      {
        "src": {
          "process": "n9",
          "port": "one"
        },
        "tgt": {
          "process": "n4",
          "port": "in"
        },
        "metadata": {
          "route": "e9"
        }
      },
      {
        "src": {
          "process": "n9",
          "port": "two"
        },
        "tgt": {
          "process": "n10",
          "port": "in"
        },
        "metadata": {
          "route": "e11"
        }
      },
      {
        "src": {
          "process": "n10",
          "port": "out"
        },
        "tgt": {
          "process": "n11",
          "port": "in"
        },
        "metadata": {
          "route": "e12"
        }
      },
      {
        "src": {
          "process": "n8",
          "port": "out"
        },
        "tgt": {
          "process": "n9",
          "port": "in"
        },
        "metadata": {
          "route": "e10"
        }
      }
    ]
  }
}

export const samples = {
  "a_simple_graph": {
    "n3_out": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "92834", "artistID": "92834", "weight": "92834", "id": "92834"},
        "mean": {
          "summary": "mean",
          "userID": "1037.0104810737446",
          "artistID": "3331.1231445375615",
          "weight": "745.2439300256372",
          "id": "3331.1231445375615"
        },
        "stddev": {
          "summary": "stddev",
          "userID": "610.8704359923324",
          "artistID": "4383.590502170076",
          "weight": "3751.322080387689",
          "id": "4383.590502170076"
        },
        "min": {"summary": "min", "userID": "2", "artistID": "1", "weight": "1", "id": "1"},
        "max": {"summary": "max", "userID": "2100", "artistID": "18745", "weight": "352698", "id": "18745"}
      },
      "histogram": {
        "userID": [{"min": 2, "max": 211.8, "count": 9847}, {
          "min": 211.8,
          "max": 421.6,
          "count": 9621
        }, {"min": 421.6, "max": 631.4000000000001, "count": 9357}, {
          "min": 631.4000000000001,
          "max": 841.2,
          "count": 9638
        }, {"min": 841.2, "max": 1051, "count": 8827}, {
          "min": 1051,
          "max": 1260.8000000000002,
          "count": 9119
        }, {"min": 1260.8000000000002, "max": 1470.6000000000001, "count": 8882}, {
          "min": 1470.6000000000001,
          "max": 1680.4,
          "count": 9146
        }, {"min": 1680.4, "max": 1890.2, "count": 9202}, {"min": 1890.2, "max": 2100, "count": 9195}],
        "artistID": [{"min": 1, "max": 1875.4, "count": 54402}, {
          "min": 1875.4,
          "max": 3749.8,
          "count": 12662
        }, {"min": 3749.8, "max": 5624.200000000001, "count": 6602}, {
          "min": 5624.200000000001,
          "max": 7498.6,
          "count": 4713
        }, {"min": 7498.6, "max": 9373, "count": 3456}, {
          "min": 9373,
          "max": 11247.400000000001,
          "count": 2919
        }, {"min": 11247.400000000001, "max": 13121.800000000001, "count": 2460}, {
          "min": 13121.800000000001,
          "max": 14996.2,
          "count": 2064
        }, {"min": 14996.2, "max": 16870.600000000002, "count": 1859}, {
          "min": 16870.600000000002,
          "max": 18745,
          "count": 1697
        }],
        "weight": [{"min": 1, "max": 35270.7, "count": 92730}, {
          "min": 35270.7,
          "max": 70540.4,
          "count": 64
        }, {"min": 70540.4, "max": 105810.09999999999, "count": 17}, {
          "min": 105810.09999999999,
          "max": 141079.8,
          "count": 11
        }, {"min": 141079.8, "max": 176349.5, "count": 6}, {
          "min": 176349.5,
          "max": 211619.19999999998,
          "count": 1
        }, {"min": 211619.19999999998, "max": 246888.89999999997, "count": 1}, {
          "min": 246888.89999999997,
          "max": 282158.6,
          "count": 1
        }, {"min": 282158.6, "max": 317428.3, "count": 0}, {"min": 317428.3, "max": 352698, "count": 3}],
        "id": [{"min": 1, "max": 1875.4, "count": 54402}, {"min": 1875.4, "max": 3749.8, "count": 12662}, {
          "min": 3749.8,
          "max": 5624.200000000001,
          "count": 6602
        }, {"min": 5624.200000000001, "max": 7498.6, "count": 4713}, {
          "min": 7498.6,
          "max": 9373,
          "count": 3456
        }, {"min": 9373, "max": 11247.400000000001, "count": 2919}, {
          "min": 11247.400000000001,
          "max": 13121.800000000001,
          "count": 2460
        }, {"min": 13121.800000000001, "max": 14996.2, "count": 2064}, {
          "min": 14996.2,
          "max": 16870.600000000002,
          "count": 1859
        }, {"min": 16870.600000000002, "max": 18745, "count": 1697}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["548", "3", "37", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row2": ["1287", "3", "330", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row3": ["2100", "3", "408", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row4": ["23", "7", "212", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row5": ["59", "7", "1410", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row6": ["85", "7", "185", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row7": ["124", "7", "102", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row8": ["127", "7", "419", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row9": ["139", "7", "385", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row10": ["142", "7", "53", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row11": ["162", "7", "215", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row12": ["172", "7", "249", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row13": ["179", "7", "270", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row14": ["189", "7", "553", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row15": ["191", "7", "32", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row16": ["206", "7", "70", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row17": ["213", "7", "429", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row18": ["229", "7", "1", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row19": ["232", "7", "1491", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row20": ["238", "7", "1234", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"]
      }
    },
    "n7_out": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "92834", "artistID": "92834", "weight": "92834", "id": "92834"},
        "mean": {
          "summary": "mean",
          "userID": "1037.0104810737446",
          "artistID": "3331.1231445375615",
          "weight": "745.2439300256372",
          "id": "3331.1231445375615"
        },
        "stddev": {
          "summary": "stddev",
          "userID": "610.8704359923332",
          "artistID": "4383.590502170072",
          "weight": "3751.3220803876834",
          "id": "4383.590502170072"
        },
        "min": {"summary": "min", "userID": "2", "artistID": "1", "weight": "1", "id": "1"},
        "max": {"summary": "max", "userID": "2100", "artistID": "18745", "weight": "352698", "id": "18745"}
      },
      "histogram": {
        "userID": [{"min": 2, "max": 211.8, "count": 9847}, {
          "min": 211.8,
          "max": 421.6,
          "count": 9621
        }, {"min": 421.6, "max": 631.4000000000001, "count": 9357}, {
          "min": 631.4000000000001,
          "max": 841.2,
          "count": 9638
        }, {"min": 841.2, "max": 1051, "count": 8827}, {
          "min": 1051,
          "max": 1260.8000000000002,
          "count": 9119
        }, {"min": 1260.8000000000002, "max": 1470.6000000000001, "count": 8882}, {
          "min": 1470.6000000000001,
          "max": 1680.4,
          "count": 9146
        }, {"min": 1680.4, "max": 1890.2, "count": 9202}, {"min": 1890.2, "max": 2100, "count": 9195}],
        "artistID": [{"min": 1, "max": 1875.4, "count": 54402}, {
          "min": 1875.4,
          "max": 3749.8,
          "count": 12662
        }, {"min": 3749.8, "max": 5624.200000000001, "count": 6602}, {
          "min": 5624.200000000001,
          "max": 7498.6,
          "count": 4713
        }, {"min": 7498.6, "max": 9373, "count": 3456}, {
          "min": 9373,
          "max": 11247.400000000001,
          "count": 2919
        }, {"min": 11247.400000000001, "max": 13121.800000000001, "count": 2460}, {
          "min": 13121.800000000001,
          "max": 14996.2,
          "count": 2064
        }, {"min": 14996.2, "max": 16870.600000000002, "count": 1859}, {
          "min": 16870.600000000002,
          "max": 18745,
          "count": 1697
        }],
        "weight": [{"min": 1, "max": 35270.7, "count": 92730}, {
          "min": 35270.7,
          "max": 70540.4,
          "count": 64
        }, {"min": 70540.4, "max": 105810.09999999999, "count": 17}, {
          "min": 105810.09999999999,
          "max": 141079.8,
          "count": 11
        }, {"min": 141079.8, "max": 176349.5, "count": 6}, {
          "min": 176349.5,
          "max": 211619.19999999998,
          "count": 1
        }, {"min": 211619.19999999998, "max": 246888.89999999997, "count": 1}, {
          "min": 246888.89999999997,
          "max": 282158.6,
          "count": 1
        }, {"min": 282158.6, "max": 317428.3, "count": 0}, {"min": 317428.3, "max": 352698, "count": 3}],
        "id": [{"min": 1, "max": 1875.4, "count": 54402}, {"min": 1875.4, "max": 3749.8, "count": 12662}, {
          "min": 3749.8,
          "max": 5624.200000000001,
          "count": 6602
        }, {"min": 5624.200000000001, "max": 7498.6, "count": 4713}, {
          "min": 7498.6,
          "max": 9373,
          "count": 3456
        }, {"min": 9373, "max": 11247.400000000001, "count": 2919}, {
          "min": 11247.400000000001,
          "max": 13121.800000000001,
          "count": 2460
        }, {"min": 13121.800000000001, "max": 14996.2, "count": 2064}, {
          "min": 14996.2,
          "max": 16870.600000000002,
          "count": 1859
        }, {"min": 16870.600000000002, "max": 18745, "count": 1697}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["548", "3", "37", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row2": ["1287", "3", "330", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row3": ["2100", "3", "408", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row4": ["23", "7", "212", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row5": ["59", "7", "1410", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row6": ["85", "7", "185", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row7": ["124", "7", "102", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row8": ["127", "7", "419", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row9": ["139", "7", "385", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row10": ["142", "7", "53", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row11": ["162", "7", "215", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row12": ["172", "7", "249", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row13": ["179", "7", "270", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row14": ["189", "7", "553", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row15": ["191", "7", "32", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row16": ["206", "7", "70", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row17": ["213", "7", "429", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row18": ["229", "7", "1", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row19": ["232", "7", "1491", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row20": ["238", "7", "1234", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"]
      }
    },
    "n9_one": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "4", "artistID": "4", "weight": "4", "id": "4"},
        "mean": {"summary": "mean", "userID": "292.75", "artistID": "6.0", "weight": "487.25", "id": "6.0"},
        "stddev": {
          "summary": "stddev",
          "userID": "172.32793350663343",
          "artistID": "2.0",
          "weight": "522.9776126502294",
          "id": "2.0"
        },
        "min": {"summary": "min", "userID": "172", "artistID": "3", "weight": "37", "id": "3"},
        "max": {"summary": "max", "userID": "548", "artistID": "7", "weight": "1234", "id": "7"}
      },
      "histogram": {
        "userID": [{"min": 172, "max": 209.6, "count": 1}, {
          "min": 209.6,
          "max": 247.2,
          "count": 2
        }, {"min": 247.2, "max": 284.8, "count": 0}, {"min": 284.8, "max": 322.4, "count": 0}, {
          "min": 322.4,
          "max": 360,
          "count": 0
        }, {"min": 360, "max": 397.6, "count": 0}, {"min": 397.6, "max": 435.2, "count": 0}, {
          "min": 435.2,
          "max": 472.8,
          "count": 0
        }, {"min": 472.8, "max": 510.40000000000003, "count": 0}, {"min": 510.40000000000003, "max": 548, "count": 1}],
        "artistID": [{"min": 3, "max": 3.4, "count": 1}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 3}],
        "weight": [{"min": 37, "max": 156.7, "count": 1}, {"min": 156.7, "max": 276.4, "count": 1}, {
          "min": 276.4,
          "max": 396.1,
          "count": 0
        }, {"min": 396.1, "max": 515.8, "count": 1}, {"min": 515.8, "max": 635.5, "count": 0}, {
          "min": 635.5,
          "max": 755.2,
          "count": 0
        }, {"min": 755.2, "max": 874.9, "count": 0}, {"min": 874.9, "max": 994.6, "count": 0}, {
          "min": 994.6,
          "max": 1114.3,
          "count": 0
        }, {"min": 1114.3, "max": 1234, "count": 1}],
        "id": [{"min": 3, "max": 3.4, "count": 1}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 3}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["548", "3", "37", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row2": ["172", "7", "249", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row3": ["213", "7", "429", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row4": ["238", "7", "1234", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"]
      }
    },
    "n8_out": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "20", "artistID": "20", "weight": "20", "id": "20"},
        "mean": {"summary": "mean", "userID": "332.25", "artistID": "6.4", "weight": "404.25", "id": "6.4"},
        "stddev": {
          "summary": "stddev",
          "userID": "494.808934417061",
          "artistID": "1.4653901941300926",
          "weight": "449.29053722742174",
          "id": "1.4653901941300926"
        },
        "min": {"summary": "min", "userID": "23", "artistID": "3", "weight": "1", "id": "3"},
        "max": {"summary": "max", "userID": "2100", "artistID": "7", "weight": "1491", "id": "7"}
      },
      "histogram": {
        "userID": [{"min": 23, "max": 230.7, "count": 15}, {
          "min": 230.7,
          "max": 438.4,
          "count": 2
        }, {"min": 438.4, "max": 646.0999999999999, "count": 1}, {
          "min": 646.0999999999999,
          "max": 853.8,
          "count": 0
        }, {"min": 853.8, "max": 1061.5, "count": 0}, {
          "min": 1061.5,
          "max": 1269.1999999999998,
          "count": 0
        }, {"min": 1269.1999999999998, "max": 1476.8999999999999, "count": 1}, {
          "min": 1476.8999999999999,
          "max": 1684.6,
          "count": 0
        }, {"min": 1684.6, "max": 1892.3, "count": 0}, {"min": 1892.3, "max": 2100, "count": 1}],
        "artistID": [{"min": 3, "max": 3.4, "count": 3}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 17}],
        "weight": [{"min": 1, "max": 150, "count": 6}, {"min": 150, "max": 299, "count": 5}, {
          "min": 299,
          "max": 448,
          "count": 5
        }, {"min": 448, "max": 597, "count": 1}, {"min": 597, "max": 746, "count": 0}, {
          "min": 746,
          "max": 895,
          "count": 0
        }, {"min": 895, "max": 1044, "count": 0}, {"min": 1044, "max": 1193, "count": 0}, {
          "min": 1193,
          "max": 1342,
          "count": 1
        }, {"min": 1342, "max": 1491, "count": 2}],
        "id": [{"min": 3, "max": 3.4, "count": 3}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 17}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["548", "3", "37", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row2": ["1287", "3", "330", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row3": ["2100", "3", "408", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row4": ["23", "7", "212", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row5": ["59", "7", "1410", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row6": ["85", "7", "185", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row7": ["124", "7", "102", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row8": ["127", "7", "419", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row9": ["139", "7", "385", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row10": ["142", "7", "53", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row11": ["162", "7", "215", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row12": ["172", "7", "249", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row13": ["179", "7", "270", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row14": ["189", "7", "553", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row15": ["191", "7", "32", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row16": ["206", "7", "70", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row17": ["213", "7", "429", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row18": ["229", "7", "1", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row19": ["232", "7", "1491", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row20": ["238", "7", "1234", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"]
      }
    },
    "n10_out": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "16", "artistID": "16", "weight": "16", "id": "16"},
        "mean": {"summary": "mean", "userID": "342.125", "artistID": "6.5", "weight": "383.5", "id": "6.5"},
        "stddev": {
          "summary": "stddev",
          "userID": "551.0589653869479",
          "artistID": "1.3662601021279464",
          "weight": "445.75150027790147",
          "id": "1.3662601021279464"
        },
        "min": {"summary": "min", "userID": "23", "artistID": "3", "weight": "1", "id": "3"},
        "max": {"summary": "max", "userID": "2100", "artistID": "7", "weight": "1491", "id": "7"}
      },
      "histogram": {
        "userID": [{"min": 23, "max": 230.7, "count": 13}, {
          "min": 230.7,
          "max": 438.4,
          "count": 1
        }, {"min": 438.4, "max": 646.0999999999999, "count": 0}, {
          "min": 646.0999999999999,
          "max": 853.8,
          "count": 0
        }, {"min": 853.8, "max": 1061.5, "count": 0}, {
          "min": 1061.5,
          "max": 1269.1999999999998,
          "count": 0
        }, {"min": 1269.1999999999998, "max": 1476.8999999999999, "count": 1}, {
          "min": 1476.8999999999999,
          "max": 1684.6,
          "count": 0
        }, {"min": 1684.6, "max": 1892.3, "count": 0}, {"min": 1892.3, "max": 2100, "count": 1}],
        "artistID": [{"min": 3, "max": 3.4, "count": 2}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 14}],
        "weight": [{"min": 1, "max": 150, "count": 5}, {"min": 150, "max": 299, "count": 4}, {
          "min": 299,
          "max": 448,
          "count": 4
        }, {"min": 448, "max": 597, "count": 1}, {"min": 597, "max": 746, "count": 0}, {
          "min": 746,
          "max": 895,
          "count": 0
        }, {"min": 895, "max": 1044, "count": 0}, {"min": 1044, "max": 1193, "count": 0}, {
          "min": 1193,
          "max": 1342,
          "count": 0
        }, {"min": 1342, "max": 1491, "count": 2}],
        "id": [{"min": 3, "max": 3.4, "count": 2}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 14}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["229", "7", "1", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row2": ["191", "7", "32", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row3": ["142", "7", "53", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row4": ["206", "7", "70", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row5": ["124", "7", "102", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row6": ["85", "7", "185", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row7": ["23", "7", "212", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row8": ["162", "7", "215", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row9": ["179", "7", "270", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row10": ["1287", "3", "330", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row11": ["139", "7", "385", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row12": ["2100", "3", "408", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row13": ["127", "7", "419", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row14": ["189", "7", "553", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row15": ["59", "7", "1410", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row16": ["232", "7", "1491", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"]
      }
    },
    "n1_out": {
      "schema": {"id": "IntegerType", "name": "StringType", "url": "StringType", "pictureURL": "StringType"},
      "stats": {
        "count": {"summary": "count", "id": "17632"},
        "mean": {"summary": "mean", "id": "9156.636853448275"},
        "stddev": {"summary": "stddev", "id": "5392.5152958021135"},
        "min": {"summary": "min", "id": "1"},
        "max": {"summary": "max", "id": "18745"}
      },
      "histogram": {
        "id": [{"min": 1, "max": 1875.4, "count": 1867}, {
          "min": 1875.4,
          "max": 3749.8,
          "count": 1807
        }, {"min": 3749.8, "max": 5624.200000000001, "count": 1828}, {
          "min": 5624.200000000001,
          "max": 7498.6,
          "count": 1841
        }, {"min": 7498.6, "max": 9373, "count": 1797}, {
          "min": 9373,
          "max": 11247.400000000001,
          "count": 1764
        }, {"min": 11247.400000000001, "max": 13121.800000000001, "count": 1734}, {
          "min": 13121.800000000001,
          "max": 14996.2,
          "count": 1655
        }, {"min": 14996.2, "max": 16870.600000000002, "count": 1686}, {
          "min": 16870.600000000002,
          "max": 18745,
          "count": 1653
        }]
      },
      "nulls": {"id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["1", "MALICE MIZER", "http://www.last.fm/music/MALICE+MIZER", "http://userserve-ak.last.fm/serve/252/10808.jpg"],
        "Row2": ["2", "Diary of Dreams", "http://www.last.fm/music/Diary+of+Dreams", "http://userserve-ak.last.fm/serve/252/3052066.jpg"],
        "Row3": ["3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row4": ["4", "Moi dix Mois", "http://www.last.fm/music/Moi+dix+Mois", "http://userserve-ak.last.fm/serve/252/54697835.png"],
        "Row5": ["5", "Bella Morte", "http://www.last.fm/music/Bella+Morte", "http://userserve-ak.last.fm/serve/252/14789013.jpg"],
        "Row6": ["6", "Moonspell", "http://www.last.fm/music/Moonspell", "http://userserve-ak.last.fm/serve/252/2181591.jpg"],
        "Row7": ["7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row8": ["8", "DIR EN GREY", "http://www.last.fm/music/DIR+EN+GREY", "http://userserve-ak.last.fm/serve/252/46968835.png"],
        "Row9": ["9", "Combichrist", "http://www.last.fm/music/Combichrist", "http://userserve-ak.last.fm/serve/252/51273485.jpg"],
        "Row10": ["10", "Grendel", "http://www.last.fm/music/Grendel", "http://userserve-ak.last.fm/serve/252/5872875.jpg"],
        "Row11": ["11", "Agonoize", "http://www.last.fm/music/Agonoize", "http://userserve-ak.last.fm/serve/252/31693309.jpg"],
        "Row12": ["12", "Behemoth", "http://www.last.fm/music/Behemoth", "http://userserve-ak.last.fm/serve/252/54196161.jpg"],
        "Row13": ["13", "Hocico", "http://www.last.fm/music/Hocico", "http://userserve-ak.last.fm/serve/252/34892635.jpg"],
        "Row14": ["15", "Dimmu Borgir", "http://www.last.fm/music/Dimmu+Borgir", "http://userserve-ak.last.fm/serve/252/52216127.png"],
        "Row15": ["16", "London After Midnight", "http://www.last.fm/music/London+After+Midnight", "http://userserve-ak.last.fm/serve/252/5364091.jpg"],
        "Row16": ["17", "Psyclon Nine", "http://www.last.fm/music/Psyclon+Nine", "http://userserve-ak.last.fm/serve/252/35246025.jpg"],
        "Row17": ["18", "The Crüxshadows", "http://www.last.fm/music/The+Cr%C3%BCxshadows", "http://userserve-ak.last.fm/serve/252/10323129.jpg"],
        "Row18": ["19", ":wumpscut:", "http://www.last.fm/music/%3Awumpscut%3A", "http://userserve-ak.last.fm/serve/252/541326.jpg"],
        "Row19": ["20", "Limbonic Art", "http://www.last.fm/music/Limbonic+Art", "http://userserve-ak.last.fm/serve/252/29314111.jpg"],
        "Row20": ["21", "Artista sconosciuto", "http://www.last.fm/music/Artista+sconosciuto", "http://userserve-ak.last.fm/serve/252/17969009.png"]
      }
    },
    "n5_out": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "4145", "artistID": "4145", "weight": "4145", "id": "4145"},
        "mean": {
          "summary": "mean",
          "userID": "995.4781664656213",
          "artistID": "65.2021712907117",
          "weight": "1453.4224366706876",
          "id": "65.2021712907117"
        },
        "stddev": {
          "summary": "stddev",
          "userID": "617.2764027581505",
          "artistID": "22.058925235447123",
          "weight": "7573.57375370614",
          "id": "22.058925235447123"
        },
        "min": {"summary": "min", "userID": "2", "artistID": "1", "weight": "1", "id": "1"},
        "max": {"summary": "max", "userID": "2100", "artistID": "99", "weight": "352698", "id": "99"}
      },
      "histogram": {
        "userID": [{"min": 2, "max": 211.8, "count": 484}, {
          "min": 211.8,
          "max": 421.6,
          "count": 455
        }, {"min": 421.6, "max": 631.4000000000001, "count": 506}, {
          "min": 631.4000000000001,
          "max": 841.2,
          "count": 413
        }, {"min": 841.2, "max": 1051, "count": 367}, {
          "min": 1051,
          "max": 1260.8000000000002,
          "count": 423
        }, {"min": 1260.8000000000002, "max": 1470.6000000000001, "count": 321}, {
          "min": 1470.6000000000001,
          "max": 1680.4,
          "count": 403
        }, {"min": 1680.4, "max": 1890.2, "count": 380}, {"min": 1890.2, "max": 2100, "count": 393}],
        "artistID": [{"min": 1, "max": 10.8, "count": 210}, {"min": 10.8, "max": 20.6, "count": 99}, {
          "min": 20.6,
          "max": 30.400000000000002,
          "count": 82
        }, {"min": 30.400000000000002, "max": 40.2, "count": 78}, {"min": 40.2, "max": 50, "count": 70}, {
          "min": 50,
          "max": 59.800000000000004,
          "count": 887
        }, {"min": 59.800000000000004, "max": 69.60000000000001, "count": 1005}, {
          "min": 69.60000000000001,
          "max": 79.4,
          "count": 525
        }, {"min": 79.4, "max": 89.2, "count": 947}, {"min": 89.2, "max": 99, "count": 242}],
        "weight": [{"min": 1, "max": 35270.7, "count": 4129}, {
          "min": 35270.7,
          "max": 70540.4,
          "count": 6
        }, {"min": 70540.4, "max": 105810.09999999999, "count": 7}, {
          "min": 105810.09999999999,
          "max": 141079.8,
          "count": 2
        }, {"min": 141079.8, "max": 176349.5, "count": 0}, {
          "min": 176349.5,
          "max": 211619.19999999998,
          "count": 0
        }, {"min": 211619.19999999998, "max": 246888.89999999997, "count": 0}, {
          "min": 246888.89999999997,
          "max": 282158.6,
          "count": 0
        }, {"min": 282158.6, "max": 317428.3, "count": 0}, {"min": 317428.3, "max": 352698, "count": 1}],
        "id": [{"min": 1, "max": 10.8, "count": 210}, {"min": 10.8, "max": 20.6, "count": 99}, {
          "min": 20.6,
          "max": 30.400000000000002,
          "count": 82
        }, {"min": 30.400000000000002, "max": 40.2, "count": 78}, {"min": 40.2, "max": 50, "count": 70}, {
          "min": 50,
          "max": 59.800000000000004,
          "count": 887
        }, {"min": 59.800000000000004, "max": 69.60000000000001, "count": 1005}, {
          "min": 69.60000000000001,
          "max": 79.4,
          "count": 525
        }, {"min": 79.4, "max": 89.2, "count": 947}, {"min": 89.2, "max": 99, "count": 242}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["548", "3", "37", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row2": ["1287", "3", "330", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row3": ["2100", "3", "408", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row4": ["23", "7", "212", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row5": ["59", "7", "1410", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row6": ["85", "7", "185", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row7": ["124", "7", "102", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row8": ["127", "7", "419", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row9": ["139", "7", "385", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row10": ["142", "7", "53", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row11": ["162", "7", "215", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row12": ["172", "7", "249", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row13": ["179", "7", "270", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row14": ["189", "7", "553", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row15": ["191", "7", "32", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row16": ["206", "7", "70", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row17": ["213", "7", "429", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row18": ["229", "7", "1", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row19": ["232", "7", "1491", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row20": ["238", "7", "1234", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"]
      }
    },
    "n2_out": {
      "schema": {"userID": "IntegerType", "artistID": "IntegerType", "weight": "IntegerType"},
      "stats": {
        "count": {"summary": "count", "userID": "92834", "artistID": "92834", "weight": "92834"},
        "mean": {
          "summary": "mean",
          "userID": "1037.0104810737446",
          "artistID": "3331.1231445375615",
          "weight": "745.2439300256372"
        },
        "stddev": {
          "summary": "stddev",
          "userID": "610.870435992336",
          "artistID": "4383.590502170073",
          "weight": "3751.3220803876743"
        },
        "min": {"summary": "min", "userID": "2", "artistID": "1", "weight": "1"},
        "max": {"summary": "max", "userID": "2100", "artistID": "18745", "weight": "352698"}
      },
      "histogram": {
        "userID": [{"min": 2, "max": 211.8, "count": 9847}, {
          "min": 211.8,
          "max": 421.6,
          "count": 9621
        }, {"min": 421.6, "max": 631.4000000000001, "count": 9357}, {
          "min": 631.4000000000001,
          "max": 841.2,
          "count": 9638
        }, {"min": 841.2, "max": 1051, "count": 8827}, {
          "min": 1051,
          "max": 1260.8000000000002,
          "count": 9119
        }, {"min": 1260.8000000000002, "max": 1470.6000000000001, "count": 8882}, {
          "min": 1470.6000000000001,
          "max": 1680.4,
          "count": 9146
        }, {"min": 1680.4, "max": 1890.2, "count": 9202}, {"min": 1890.2, "max": 2100, "count": 9195}],
        "artistID": [{"min": 1, "max": 1875.4, "count": 54402}, {
          "min": 1875.4,
          "max": 3749.8,
          "count": 12662
        }, {"min": 3749.8, "max": 5624.200000000001, "count": 6602}, {
          "min": 5624.200000000001,
          "max": 7498.6,
          "count": 4713
        }, {"min": 7498.6, "max": 9373, "count": 3456}, {
          "min": 9373,
          "max": 11247.400000000001,
          "count": 2919
        }, {"min": 11247.400000000001, "max": 13121.800000000001, "count": 2460}, {
          "min": 13121.800000000001,
          "max": 14996.2,
          "count": 2064
        }, {"min": 14996.2, "max": 16870.600000000002, "count": 1859}, {
          "min": 16870.600000000002,
          "max": 18745,
          "count": 1697
        }],
        "weight": [{"min": 1, "max": 35270.7, "count": 92730}, {
          "min": 35270.7,
          "max": 70540.4,
          "count": 64
        }, {"min": 70540.4, "max": 105810.09999999999, "count": 17}, {
          "min": 105810.09999999999,
          "max": 141079.8,
          "count": 11
        }, {"min": 141079.8, "max": 176349.5, "count": 6}, {
          "min": 176349.5,
          "max": 211619.19999999998,
          "count": 1
        }, {"min": 211619.19999999998, "max": 246888.89999999997, "count": 1}, {
          "min": 246888.89999999997,
          "max": 282158.6,
          "count": 1
        }, {"min": 282158.6, "max": 317428.3, "count": 0}, {"min": 317428.3, "max": 352698, "count": 3}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0},
      "data": {
        "Row1": ["2", "51", "13883"],
        "Row2": ["2", "52", "11690"],
        "Row3": ["2", "53", "11351"],
        "Row4": ["2", "54", "10300"],
        "Row5": ["2", "55", "8983"],
        "Row6": ["2", "56", "6152"],
        "Row7": ["2", "57", "5955"],
        "Row8": ["2", "58", "4616"],
        "Row9": ["2", "59", "4337"],
        "Row10": ["2", "60", "4147"],
        "Row11": ["2", "61", "3923"],
        "Row12": ["2", "62", "3782"],
        "Row13": ["2", "63", "3735"],
        "Row14": ["2", "64", "3644"],
        "Row15": ["2", "65", "3579"],
        "Row16": ["2", "66", "3312"],
        "Row17": ["2", "67", "3301"],
        "Row18": ["2", "68", "2927"],
        "Row19": ["2", "69", "2720"],
        "Row20": ["2", "70", "2686"]
      }
    },
    "n9_two": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "16", "artistID": "16", "weight": "16", "id": "16"},
        "mean": {"summary": "mean", "userID": "342.125", "artistID": "6.5", "weight": "383.5", "id": "6.5"},
        "stddev": {
          "summary": "stddev",
          "userID": "551.0589653869476",
          "artistID": "1.3662601021279464",
          "weight": "445.75150027790147",
          "id": "1.3662601021279464"
        },
        "min": {"summary": "min", "userID": "23", "artistID": "3", "weight": "1", "id": "3"},
        "max": {"summary": "max", "userID": "2100", "artistID": "7", "weight": "1491", "id": "7"}
      },
      "histogram": {
        "userID": [{"min": 23, "max": 230.7, "count": 13}, {
          "min": 230.7,
          "max": 438.4,
          "count": 1
        }, {"min": 438.4, "max": 646.0999999999999, "count": 0}, {
          "min": 646.0999999999999,
          "max": 853.8,
          "count": 0
        }, {"min": 853.8, "max": 1061.5, "count": 0}, {
          "min": 1061.5,
          "max": 1269.1999999999998,
          "count": 0
        }, {"min": 1269.1999999999998, "max": 1476.8999999999999, "count": 1}, {
          "min": 1476.8999999999999,
          "max": 1684.6,
          "count": 0
        }, {"min": 1684.6, "max": 1892.3, "count": 0}, {"min": 1892.3, "max": 2100, "count": 1}],
        "artistID": [{"min": 3, "max": 3.4, "count": 2}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 14}],
        "weight": [{"min": 1, "max": 150, "count": 5}, {"min": 150, "max": 299, "count": 4}, {
          "min": 299,
          "max": 448,
          "count": 4
        }, {"min": 448, "max": 597, "count": 1}, {"min": 597, "max": 746, "count": 0}, {
          "min": 746,
          "max": 895,
          "count": 0
        }, {"min": 895, "max": 1044, "count": 0}, {"min": 1044, "max": 1193, "count": 0}, {
          "min": 1193,
          "max": 1342,
          "count": 0
        }, {"min": 1342, "max": 1491, "count": 2}],
        "id": [{"min": 3, "max": 3.4, "count": 2}, {"min": 3.4, "max": 3.8, "count": 0}, {
          "min": 3.8,
          "max": 4.2,
          "count": 0
        }, {"min": 4.2, "max": 4.6, "count": 0}, {"min": 4.6, "max": 5, "count": 0}, {
          "min": 5,
          "max": 5.4,
          "count": 0
        }, {"min": 5.4, "max": 5.800000000000001, "count": 0}, {
          "min": 5.800000000000001,
          "max": 6.2,
          "count": 0
        }, {"min": 6.2, "max": 6.6, "count": 0}, {"min": 6.6, "max": 7, "count": 14}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["1287", "3", "330", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row2": ["2100", "3", "408", "3", "Carpathian Forest", "http://www.last.fm/music/Carpathian+Forest", "http://userserve-ak.last.fm/serve/252/40222717.jpg"],
        "Row3": ["23", "7", "212", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row4": ["59", "7", "1410", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row5": ["85", "7", "185", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row6": ["124", "7", "102", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row7": ["127", "7", "419", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row8": ["139", "7", "385", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row9": ["142", "7", "53", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row10": ["162", "7", "215", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row11": ["179", "7", "270", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row12": ["189", "7", "553", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row13": ["191", "7", "32", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row14": ["206", "7", "70", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row15": ["229", "7", "1", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"],
        "Row16": ["232", "7", "1491", "7", "Marilyn Manson", "http://www.last.fm/music/Marilyn+Manson", "http://userserve-ak.last.fm/serve/252/2558217.jpg"]
      }
    },
    "n6_out": {
      "schema": {
        "userID": "IntegerType",
        "artistID": "IntegerType",
        "weight": "IntegerType",
        "id": "IntegerType",
        "name": "StringType",
        "url": "StringType",
        "pictureURL": "StringType"
      },
      "stats": {
        "count": {"summary": "count", "userID": "88689", "artistID": "88689", "weight": "88689", "id": "88689"},
        "mean": {
          "summary": "mean",
          "userID": "1038.9515497976074",
          "artistID": "3483.7603648705026",
          "weight": "712.1462526356144",
          "id": "3483.7603648705026"
        },
        "stddev": {
          "summary": "stddev",
          "userID": "610.5038077103735",
          "artistID": "4426.299511981597",
          "weight": "3467.7728848458037",
          "id": "4426.299511981597"
        },
        "min": {"summary": "min", "userID": "2", "artistID": "100", "weight": "1", "id": "100"},
        "max": {"summary": "max", "userID": "2100", "artistID": "18745", "weight": "324663", "id": "18745"}
      },
      "histogram": {
        "userID": [{"min": 2, "max": 211.8, "count": 9363}, {
          "min": 211.8,
          "max": 421.6,
          "count": 9166
        }, {"min": 421.6, "max": 631.4000000000001, "count": 8851}, {
          "min": 631.4000000000001,
          "max": 841.2,
          "count": 9225
        }, {"min": 841.2, "max": 1051, "count": 8460}, {
          "min": 1051,
          "max": 1260.8000000000002,
          "count": 8696
        }, {"min": 1260.8000000000002, "max": 1470.6000000000001, "count": 8561}, {
          "min": 1470.6000000000001,
          "max": 1680.4,
          "count": 8743
        }, {"min": 1680.4, "max": 1890.2, "count": 8822}, {"min": 1890.2, "max": 2100, "count": 8802}],
        "artistID": [{"min": 100, "max": 1964.5, "count": 51118}, {
          "min": 1964.5,
          "max": 3829,
          "count": 12201
        }, {"min": 3829, "max": 5693.5, "count": 6407}, {"min": 5693.5, "max": 7558, "count": 4605}, {
          "min": 7558,
          "max": 9422.5,
          "count": 3432
        }, {"min": 9422.5, "max": 11287, "count": 2886}, {"min": 11287, "max": 13151.5, "count": 2453}, {
          "min": 13151.5,
          "max": 15016,
          "count": 2050
        }, {"min": 15016, "max": 16880.5, "count": 1850}, {"min": 16880.5, "max": 18745, "count": 1687}],
        "weight": [{"min": 1, "max": 32467.2, "count": 88590}, {
          "min": 32467.2,
          "max": 64933.4,
          "count": 67
        }, {"min": 64933.4, "max": 97399.6, "count": 11}, {"min": 97399.6, "max": 129865.8, "count": 9}, {
          "min": 129865.8,
          "max": 162332,
          "count": 3
        }, {"min": 162332, "max": 194798.2, "count": 4}, {"min": 194798.2, "max": 227264.4, "count": 1}, {
          "min": 227264.4,
          "max": 259730.6,
          "count": 2
        }, {"min": 259730.6, "max": 292196.8, "count": 0}, {"min": 292196.8, "max": 324663, "count": 2}],
        "id": [{"min": 100, "max": 1964.5, "count": 51118}, {"min": 1964.5, "max": 3829, "count": 12201}, {
          "min": 3829,
          "max": 5693.5,
          "count": 6407
        }, {"min": 5693.5, "max": 7558, "count": 4605}, {"min": 7558, "max": 9422.5, "count": 3432}, {
          "min": 9422.5,
          "max": 11287,
          "count": 2886
        }, {"min": 11287, "max": 13151.5, "count": 2453}, {"min": 13151.5, "max": 15016, "count": 2050}, {
          "min": 15016,
          "max": 16880.5,
          "count": 1850
        }, {"min": 16880.5, "max": 18745, "count": 1687}]
      },
      "nulls": {"userID": 0, "artistID": 0, "weight": 0, "id": 0, "name": 0, "url": 0, "pictureURL": 0},
      "data": {
        "Row1": ["403", "1035", "249", "1035", "Breathe Carolina", "http://www.last.fm/music/Breathe+Carolina", "http://userserve-ak.last.fm/serve/252/46069997.jpg"],
        "Row2": ["211", "4308", "757", "4308", "Client", "http://www.last.fm/music/Client", "http://userserve-ak.last.fm/serve/252/303586.jpg"],
        "Row3": ["477", "903", "1013", "903", "Amy Winehouse", "http://www.last.fm/music/Amy+Winehouse", "http://userserve-ak.last.fm/serve/252/366300.jpg"],
        "Row4": ["364", "6143", "608", "6143", "John Stoneham", "http://www.last.fm/music/John+Stoneham", "http://userserve-ak.last.fm/serve/252/5640625.jpg"],
        "Row5": ["1684", "349", "26", "349", "The Pussycat Dolls", "http://www.last.fm/music/The+Pussycat+Dolls", "http://userserve-ak.last.fm/serve/252/31329805.png"],
        "Row6": ["668", "1045", "854", "1045", "Fresno", "http://www.last.fm/music/Fresno", "http://userserve-ak.last.fm/serve/252/61941727.png"],
        "Row7": ["971", "1415", "40", "1415", "Creedence Clearwater Revival", "http://www.last.fm/music/Creedence+Clearwater+Revival", "http://userserve-ak.last.fm/serve/252/37481533.jpg"],
        "Row8": ["319", "3729", "572", "3729", "Madball", "http://www.last.fm/music/Madball", "http://userserve-ak.last.fm/serve/252/51916561.jpg"],
        "Row9": ["1685", "436", "52", "436", "The Smashing Pumpkins", "http://www.last.fm/music/The+Smashing+Pumpkins", "http://userserve-ak.last.fm/serve/252/887234.jpg"],
        "Row10": ["769", "6857", "1745", "6857", "Grégory Lemarchal", "http://www.last.fm/music/Gr%C3%A9gory+Lemarchal", "http://userserve-ak.last.fm/serve/252/17657583.png"],
        "Row11": ["1564", "646", "104", "646", "RBD", "http://www.last.fm/music/RBD", "http://userserve-ak.last.fm/serve/252/59211131.png"],
        "Row12": ["1018", "811", "571", "811", "Sonic Syndicate", "http://www.last.fm/music/Sonic+Syndicate", "http://userserve-ak.last.fm/serve/252/4859016.jpg"],
        "Row13": ["1915", "17586", "18", "17586", "Selambé", "http://www.last.fm/music/Selamb%C3%A9"],
        "Row14": ["1625", "1800", "5", "1800", "Amethystium", "http://www.last.fm/music/Amethystium", "http://userserve-ak.last.fm/serve/252/46302273.jpg"],
        "Row15": ["45", "792", "178", "792", "Thalía", "http://www.last.fm/music/Thal%C3%ADa", "http://userserve-ak.last.fm/serve/252/40337541.png"],
        "Row16": ["975", "536", "828", "536", "Good Charlotte", "http://www.last.fm/music/Good+Charlotte", "http://userserve-ak.last.fm/serve/252/55721971.jpg"],
        "Row17": ["1941", "716", "249", "716", "Kaiser Chiefs", "http://www.last.fm/music/Kaiser+Chiefs", "http://userserve-ak.last.fm/serve/252/2681098.jpg"],
        "Row18": ["1063", "1098", "240", "1098", "Björk", "http://www.last.fm/music/Bj%C3%B6rk", "http://userserve-ak.last.fm/serve/252/41268145.png"],
        "Row19": ["593", "302", "167", "302", "P!nk", "http://www.last.fm/music/P%21nk", "http://userserve-ak.last.fm/serve/252/56011579.png"],
        "Row20": ["1440", "14568", "361", "14568", "Limão com Mel", "http://www.last.fm/music/Lim%C3%A3o+com+Mel", "http://userserve-ak.last.fm/serve/252/23906983.jpg"]
      }
    }
  }
}
