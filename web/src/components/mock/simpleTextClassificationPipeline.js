export const graph = {
  "graph": {
    "metainfo" : {
      "id" : "SimpleTextClassificationPipeline",
      "memory" : 1,
      "processors" : 1,
      "cluster": "local",
      "mode": "batch"
    },
    "inports"  : {},
    "outports" : {},
    "groups"   : [
      {
        "name": "m1",
        "component" : "MLFeature",
        "nodes": ["n3", "n4"],
        "metadata": {
          "description": "Generate Features based on text frequency",
          "color": 0
        }
      },
      {
        "name": "m2",
        "component" : "MLPipeline",
        "nodes": ["m1", "n5"],
        "metadata": {
          "description": "Linear Regression based Text Classification pipeline",
          "color": 0
        },
        "ports": {
          "inputs": [ ],
          "outputs": ["out"]
        }
      },
      {
        "name": "m3",
        "component" : "MLModel",
        "nodes": ["m2", "n6", "n7"],
        "metadata": {
          "description": "Text Classification Model",
          "color": 0
        }
      }
    ],
    "processes": {
      "n1": {
        "component": "Script",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "generate_train_data"
        },
        "properties": {
          "package": "datagen",
          "language": "scala",
          "code": "import org.apache.spark.sql.DataFrame\\n\\ncase class LabeledDocument(id: Long, text: String, label: Double)\\ndef generateTrainingData(sc: SparkContext) : DataFrame = {\\nval training = sc.parallelize(Seq(LabeledDocument(0L, \"a b c d e spark\", 1.0),\\nLabeledDocument(1L, \"b d\", 0.0),\\nLabeledDocument(2L, \"spark f g h\", 1.0),\\nLabeledDocument(3L, \"hadoop mapreduce\", 0.0)))\\ntraining.toDF()\\n}",
          "call": "generateTrainingData"
        },
        "ports": {
          "inputs": [],
          "outputs": [
            "out"
          ]
        }
      },
      "n2": {
        "component": "Script",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "generate_test_data"
        },
        "properties": {
          "package": "datagen",
          "language": "scala",
          "code": "import org.apache.spark.sql.DataFrame\\n\\ncase class Document(id: Long, text: String)\\nobject TestGen {\\ndef generateTestData(sc: SparkContext) : DataFrame = {\\nval test = sc.parallelize(Seq(\\nDocument(4L, \"spark i j k\"),\\nDocument(5L, \"l m n\"),\\nDocument(6L, \"spark hadoop spark\"),\\nDocument(7L, \"apache hadoop\")))\\ntest.toDF()\\n}\\n}",
          "call": "TestGen.generateTestData"
        },
        "ports": {
          "inputs": [],
          "outputs": [
            "out"
          ]
        }
      },
      "n3": {
        "component": "Tokenizer",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "tokenize_text"
        },
        "properties": {
          "ml": "feature",
          "inputColumn": "text",
          "outputColumn": "words"
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": [
            "out"
          ]
        }
      },
      "n4": {
        "component": "HashingTF",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "token_frequency"
        },
        "properties": {
          "ml": "feature",
          "numFeatures": 1000,
          "inputColumn": "words",
          "outputColumn": "myFeatures"
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": [
            "out"
          ]
        }
      },
      "n5": {
        "component": "LogisticRegression",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "Logistic Regression"
        },
        "properties": {
          "ml": "classifier",
          "maxIter": 10,
          "regParam": 0.001,
          "featureColumn": "myFeatures"
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": [
            "out"
          ]
        }
      },
      "n6": {
        "component": "ModelBuilder",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "Build Model"
        },
        "properties": {
          "ml": "fitter"
        },
        "ports": {
          "inputs": [
            "pipeline",
            "train_data"
          ],
          "outputs": [
            "model"
          ]
        }
      },
      "n7": {
        "component": "ModelApplier",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "Apply Model"
        },
        "properties": {
          "ml": "applier"
        },
        "ports": {
          "inputs": [
            "model",
            "data"
          ],
          "outputs": [
            "out"
          ]
        }
      },
      "n8": {
          "component": "Select",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "Select Prediction"
        },
        "properties": {
          "columns": ["id", "text", "probability", "prediction"]
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": [
            "out"
          ]
        }
      },
      "n9": {
        "component": "FileOutput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "write_prediction"
        },
        "properties": {
          "collect": true,
          "fileType": "csv",
          "fileName": "/vagrant/tmp/resources/SimpleTextClassification_predictions.csv"
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": []
        }
      }
    },
    "connections": [
      {
        "src": {
          "process": "n1",
          "port": "out"
        },
        "tgt": {
          "process": "n6",
          "port": "train_data"
        },
        "metadata": {
          "route": "e1"
        }
      },
      {
        "src": {
          "process": "n2",
          "port": "out"
        },
        "tgt": {
          "process": "n7",
          "port": "data"
        },
        "metadata": {
          "route": "e2"
        }
      },
      {
        "src": {
          "process": "n3",
          "port": "out"
        },
        "tgt": {
          "process": "n5",
          "port": "in"
        },
        "metadata": {
          "route": "e3"
        }
      },
      {
        "src": {
          "process": "m2",
          "port": "out"
        },
        "tgt": {
          "process": "n6",
          "port": "pipeline"
        },
        "metadata": {
          "route": "e4"
        }
      },
      {
        "src": {
          "process": "n6",
          "port": "model"
        },
        "tgt": {
          "process": "n7",
          "port": "model"
        },
        "metadata": {
          "route": "e5"
        }
      },
      {
        "src": {
          "process": "n7",
          "port": "out"
        },
        "tgt": {
          "process": "n8",
          "port": "in"
        },
        "metadata": {
          "route": "e6"
        }
      },
      {
        "src": {
          "process": "n8",
          "port": "out"
        },
        "tgt": {
          "process": "n9",
          "port": "in"
        },
        "metadata": {
          "route": "e7"
        }
      }
    ]
  }
}

export const samples = {}
