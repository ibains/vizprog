export const componentCategoryDefinition = [
  {
    category: 'io',
    label: 'Input, Output',
    icon: 'IconWorkflowIO',
    color: '#00CBD5'
  },
  {
    category: 'transform',
    label: 'Transform',
    icon: 'IconWorkflowTransform',
    color: '#C37DCF'
  },
  {
    category: 'join-split',
    label: 'Join, Split',
    icon: 'IconWorkflowIO',
    color: '#876CF8'
  },
  {
    category: 'ml',
    label: 'Machine Learning',
    icon: 'IconWorkflowIO',
    color: '#0297E6'
  },
  {
    category: 'script',
    label: 'Script, Cleanse',
    icon: 'IconWorkflowIO',
    color: '#AE3ED4'
  },
  {
    category: 'visualize',
    label: 'Visualize',
    icon: 'IconWorkflowIO',
    color: '#FF379C'
  }
]
