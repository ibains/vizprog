export const graph = {
  "graph": {
    "metainfo" : {
      "id" : "simple_join"
    },
    "inports"  : {},
    "outports" : {},
    "groups"   : [],
    "processes": {
      "n1": {
        "id": "n1",
        "component": "fileInput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "artists"
        },
        "properties": {
          "fileType": "csv",
          "header": "true",
          "inferSchema": "true",
          "fileName": "/tmp/code/jsontospark/src/main/resources/artists.csv"
        },
        "ports": {
          "inputs": [],
          "outputs": [
            "out"
          ]
        }
      },
      "n2": {
        "id": "n2",
        "component": "fileInput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "user_artists"
        },
        "properties": {
          "fileType": "csv",
          "header": "true",
          "inferSchema": "true",
          "fileName": "/tmp/code/jsontospark/src/main/resources/user_artists.csv"
        },
        "ports": {
          "inputs": [],
          "outputs": [
            "out"
          ]
        }
      },
      "n4": {
        "id": "n4",
        "component": "fileOutput",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "write_user_artists"
        },
        "properties": {
          "fileType": "csv",
          "fileName": "/tmp/code/jsontospark/src/main/resources/write_user_artists.csv"
        },
        "ports": {
          "inputs": [
            "in"
          ],
          "outputs": []
        }
      },
      "n3": {
        "id": "n3",
        "component": "join",
        "metadata": {
          "x": 100,
          "y": 100,
          "label": "joined_user_artists"
        },
        "properties": {
          "joinType": "outer",
          "joinCondition": "$artistID === $id"
        },
        "ports": {
          "inputs": [
            "left",
            "right"
          ],
          "outputs": [
            "out"
          ]
        }
      }
    },
    "connections": [
      {
        "src": {
          "process": "n2",
          "port": "out"
        },
        "tgt": {
          "process": "n3",
          "port": "left"
        },
        "metadata": {
          "route": "e1"
        },
      },
      {
        "src": {
          "process": "n1",
          "port": "out"
        },
        "tgt": {
          "process": "n3",
          "port": "right"
        },
        "metadata": {
          "route": "e2"
        }
      },
      {
        "src": {
          "process": "n3",
          "port": "out"
        },
        "tgt": {
          "process": "n4",
          "port": "in"
        },
        "metadata": {
          "route": "e3"
        }
      }
    ]
  }
}
