import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Dialog from 'react-toolbox/lib/dialog';
import Input from 'react-toolbox/lib/input';
import Checkbox from 'react-toolbox/lib/checkbox';
import { Button, IconButton } from 'react-toolbox/lib/button';
import { NewWorkflowCard, WorkflowCard } from './WorkflowCard';

import theme from './NewWorkflowDialog.scss';

import * as ProjectActions from '../actions/projectActions';
import * as WorkflowActions from '../actions/workflowActions';
import modes from 'constants/modes';

const TemplateCard = (props) => {
  const template = props.template
  return (
    <Card style={{ width: 100 }}>
      <CardText>
        { template.content.graph.metainfo.id }
      </CardText>
    </Card>
  )
}

// <Dropdown label='Select a template (optional)' auto value={this.state.template} source={templateSource} onChange={(template) => this.setState({ template })} />
const TemplateCardStack = (props) => {
  const templateCards = props.templates.map((template, id) => {
    return (
      <WorkflowCard key={id} selected={props.selectedTemplateId == id} onSelect={ () => props.onSelect(id) } title={template.content.graph.metainfo.id} description={'No description'} author={'John'} createdAt={'2 days ago'} />
    )
  })
  return (
    <div>
      { templateCards.toArray() }
    </div>
  )
}

const NewCardStack = (props) => {
  const cards = modes.map(mode => (
    <NewWorkflowCard key={mode.value} selected={ props.selectedMode == mode.value } onSelect={ () => props.onSelect(mode.value) } mode={mode.value}
      modeLabel={mode.label}
    />
  ))
  return (
    <div>
      { cards }
    </div>
  )
}

export class NewWorkflowDialog extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { project: 'default', workflow: 'My Workflow', showTemplates: false, templateId: '', mode: '' }
    this._createWorkflow = this._createWorkflow.bind(this);
  }

  _createWorkflow() {
    const { templateId, mode, project, workflow } = this.state;
    const { projects, actions } = this.props;
    actions.createNewWorkflow(workflow, project, templateId, mode);
  }

  componentWillUpdate(nextProps) {
    // close after a new workflow is created
    if (nextProps.workflows.size != this.props.workflows.size) {
      this.props.close();
    }
  }

  onSelectExistingWorkflow = (templateId) => {
    this.setState({ templateId, mode: '' })
  }

  onSelectNewWorkflow = (mode) => {
    this.setState({ mode, templateId: '' })
  }

  render() {

    const { createWorkflow, open, close, templates, projects } = this.props;

    return (
      <Dialog type='fullscreen' active={open} onOverlayClick={close} theme={theme}>
        <IconButton icon='close' className={theme.close} onMouseUp={close}/>
        <div style={{ border: '1px solid #c1c1c1', padding: 24 }}>
          <div style={{ fontSize: 24, marginBottom: 24, color: '#666' }}>
            Create a New Workflow
          </div>
          <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between' }}>
            <div style={{ flexGrow: 1, marginRight: 20  }}>
              <Input label='Name your workflow' value={this.state.workflow} onChange={(workflow) => this.setState({ workflow })} maxLength={40} />
            </div>
            <div style={{ flexGrow: 1, marginLeft: 20 }}>
              <Input style={{ flexGrow: 1, margin: 'auto' }} label='Which project?' value={this.state.project} onChange={(project) => this.setState({ project })} maxLength={40} />
            </div>
          </div>
          <NewCardStack selectedMode={this.state.mode} onSelect={this.onSelectNewWorkflow} />
          <Checkbox checked={this.state.showTemplates} label="Select from templates" onChange={ () => this.setState({ showTemplates: !this.state.showTemplates }) } />
          {
            (this.state.showTemplates) ?
            <TemplateCardStack selectedTemplateId={this.state.templateId} templates={templates} onSelect={this.onSelectExistingWorkflow} /> : ''
          }
          <div className="clearfix">
            <Button style={{ float: 'right', marginTop: 10 }} label='Create Workflow' raised primary disabled={!this.state.project || !this.state.workflow} onMouseUp={this._createWorkflow} />
          </div>
        </div>
      </Dialog>
    );
  }
}

let mapDispatchToProps = dispatch => ({
  actions: Object.assign({},
                         bindActionCreators(WorkflowActions, dispatch),
                         bindActionCreators(ProjectActions, dispatch))
});
export default connect(state => ({ templates: state.templates, projects: state.projects, workflows: state.workflows }), mapDispatchToProps)(NewWorkflowDialog);
