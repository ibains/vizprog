import React from 'react';
import Rule from './Rule';

export default class RuleGroup extends React.Component {
    static get defaultProps() {
        return {
            id: null,
            parentId: null,
            rules: [],
            combinator: 'and',
            schema: {},
        };
    }

    onCombinatorChange(e) {
        const value = e.target.className.toLowerCase().replace('active', '').trim()
        const {onPropChange} = this.props.schema;

        onPropChange('combinator', value, this.props.id);
    }

    addRule(e) {
        e.preventDefault();
        e.stopPropagation();

        const {createRule, onRuleAdd} = this.props.schema;

        const newRule = createRule();
        onRuleAdd(newRule, this.props.id)
    }

    addGroup(e) {
        e.preventDefault();
        e.stopPropagation();

        const {createRuleGroup, onGroupAdd} = this.props.schema;
        const newGroup = createRuleGroup();
        onGroupAdd(newGroup, this.props.id)
    }

    removeGroup(e, groupId) {
        e.preventDefault();
        e.stopPropagation();

        this.props.schema.onGroupRemove(groupId, this.props.parentId);
    }

    render() {
        const {combinator, rules, schema: {combinators, onRuleRemove, isRuleGroup, classNames}} = this.props;

        return (
            <dl className={`ruleGroup ${classNames.ruleGroup}`}>
                <dt className="rules-group-header">
                    <div className={`ruleGroup-combinators ${classNames.combinators}`}>
                        <label className={(this.props.combinator.toLowerCase() == "and") ? "and active" : "and"}
                                onClick={e => this.onCombinatorChange(e)}>AND</label>
                        <label className={(this.props.combinator.toLowerCase() == "or") ? "or active" : "or"}
                                onClick={e => this.onCombinatorChange(e)}>OR</label>

                        <button className={`ruleGroup-addRule ${classNames.addRule}`}
                            onClick={e => this.addRule(e)}>+RULE</button>
                        <button className={`ruleGroup-addGroup ${classNames.addGroup}`}
                                onClick={e => this.addGroup(e)}>+GROUP</button>
                        {
                            (this.props.parentId) ?
                                <button className={`ruleGroup-remove ${classNames.removeGroup}`}
                                        onClick={e => this.removeGroup(e, this.props.id)}>X</button>
                                : null
                        }
                    </div>
                </dt>

                <dd className="rules-group-body">
                    <ul className="rules-list">
                    {
                        rules.map(r=> {
                            return (
                                isRuleGroup(r)
                                    ? <RuleGroup key={r.id}
                                                 id={r.id}
                                                 schema={this.props.schema}
                                                 parentId={this.props.id}
                                                 combinator={r.combinator}
                                                 rules={r.rules}/>
                                    : <Rule key={r.id}
                                            id={r.id}
                                            field={r.field}
                                            value={r.value}
                                            operator={r.operator}
                                            schema={this.props.schema}
                                            parentId={this.props.id}
                                            onRuleRemove={onRuleRemove}/>
                         );
                     })
                    }
                    </ul>
                </dd>
            </dl>
        );
    }
}