import React from 'react';
import Select from 'react-select';
// TODO: Not sure if this is the right way to import css
import 'react-select/dist/react-select.css';

export default class Rule extends React.Component {
	static get defaultProps() {
		return {
			id: null,
			parentId: null,
			field: null,
			operator: null,
			value: null,
			schema: null
		};
	}

	componentWillMount() {
		this.setState({value: ''});
	}

	onValueChanged(field, value) {
		if (field == 'value') {
			console.log(value)
		}
		const {id, schema: {onPropChange}} = this.props;
		onPropChange(field, value, id);
	}

	removeRule(e) {
		e.preventDefault();
		e.stopPropagation();
		this.props.schema.onRuleRemove(this.props.id, this.props.parentId);
	}

	fieldsArrayToMap(fields) {
        return fields.map((e) => {return {'value': e, 'label': e}});
    }

	operatorsObjectToMap(operators) {
		return Object.keys(operators).map((e) => {return {'value': operators[e].name, 'label': operators[e].label.toUpperCase()}});
	}

	promptTextCreator (label) {
        return `"${label}"`;
    }

	render() {
		const {field, operator, value, schema: {fields, getEditor, getOperators, classNames}} = this.props;
		const options = this.fieldsArrayToMap(fields);
        // if the new field is not in the option then add it at top
        if (options.filter((e) => e.value === field).length === 0) {
            options.unshift({'value': field, 'label': field});
        }
		return (
			<div className={`rule rule-container expression ${classNames.rule}`}>
				<div className={`rule-filter-container operand ${classNames.fields}`}>
					<Select.Creatable name={`rule-fields ${classNames.fields}`}
						multi={false}
						clearable={false}
						value={field}
						options={options}
						onChange={e => this.onValueChanged('field', e.value)}
						promptTextCreator={this.promptTextCreator} />
				</div>

				<div className={`rule-operator-container operator ${classNames.operators}`}>
					<Select name={`rule-fields ${classNames.fields}`}
						multi={false}
						clearable={false}
						value={operator}
						options={this.operatorsObjectToMap(getOperators())}
						onChange={e => this.onValueChanged('operator', e.value)}/>
				</div>

				<div className={`rule-value-container operand ${classNames.values}`}>
					{

						getEditor({
							field,
							value,
							operator,
							classNames,
							onChange: v => this.onValueChanged('value', v)
						})
					}
				</div>

				<button className={`rule-remove ${classNames.removeRule}`}
						onClick={e => this.removeRule(e)}>x</button>
			</div>
		);
	}
}