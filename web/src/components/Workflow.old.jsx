import React from 'react'
import ReactDOM from 'react-dom'
import { connect } from 'react-redux'
import Icon from 'react-fa'
import jsplumb from 'script!../../node_modules/jsplumb'
import klay from 'klayjs'
import shouldPureComponentUpdate from 'react-pure-render/function'
import keymaster from 'keymaster'
import fetch from 'isomorphic-fetch'

import { theme } from './theme'
import { API_Config } from 'constants'
import { getNodeRenderProps, getCategoryRenderProps, nofloToKieler } from 'utils/WorkflowUtil'
import { ModalAlert } from './ModalAlert'
import { WorkflowRunner } from './WorkflowRunner'
import { WorkflowStatus } from './WorkflowStatus'
import { WorkflowContextMenu } from './WorkflowContextMenu'
import { WorkflowNode } from './WorkflowNode'
import { WorkflowNodeGroup } from './WorkflowNodeGroup'
import { WorkflowNodeEditor } from './WorkflowNodeEditor'
import { WorkflowEdgeEditor } from './WorkflowEdgeEditor'
import { DataVisualizer } from './DataVisualizer'

import {componentDefinition} from 'components/mock/componentDefinition'

export class Workflow extends React.Component {

  constructor(props) {
    console.debug('Workflow constructor')

    super(props)

    this.state = {
      nodeSelector: {
        isActive: false,
        source: {
          x: 0,
          y: 0
        },
        target: {
          x: 0,
          y: 0
        }
      },
      selectedNodes: {},
      isDragged: false,
      draggedNode: null,
      isNodeEditorShown: false,
      isEdgeEditorShown: false,
      edgeEditorSamples: {
      },
      nodeEditorNodeId: null,
      nodeEditorNode: {
      },
      isDataVisualizerShown: false,
      isContextMenuShown: false,
      isModalAlertShown: false,
      modalAlertTitle: '',
      modalAlertContent: '',
      modalAlertFormat: '',
      isRunning: false,
      lastRunStatus: 'UNKNOWN',
      lastRunDetails: '',
      isLastRunStatusShown: false,
      contextMenuPosition: {
        x: 0,
        y: 0
      },
      anchorEl: {},
      workflow: this.props.workflow
    }
  }

  static propTypes = {
    onAddEdge: React.PropTypes.func.isRequired,
    onRemoveEdge: React.PropTypes.func.isRequired,
    onUpdateNode: React.PropTypes.func.isRequired,
    onRemoveNode: React.PropTypes.func.isRequired,
    onSetSamples: React.PropTypes.func.isRequired,
    onSetNodePosition: React.PropTypes.func.isRequired,
    onShiftNodePositions: React.PropTypes.func.isRequired,
    workflow: React.PropTypes.object
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.debug('Workflow shouldComponentUpdate')
    // we need to make sure that certain state changes don't trigger re-render because
    // events that should be triggered for jsPlumb (e.g., clicking on an edge overlay) don't work otherwise.
    //
    // skip re-render on mouse down to activate nodeSelector
    if (!this.state.nodeSelector.isActive && nextState.nodeSelector.isActive) {
      return false
    }

    // TODO: skip re-render on mouse move if the mouse has not moved much from the original mouse down
    // skip re-render on mouse up if the mouse has not moved much from the original mouse down
    let currentNS = this.state.nodeSelector;
    let nextNS = nextState.nodeSelector;

    if (currentNS.isActive && !nextNS.isActive &&
      Math.abs(currentNS.source.x - nextNS.target.x) < 10 &&
      Math.abs(currentNS.source.y - nextNS.target.y) < 10) {
      return false
    }

    return true
  }

  initialRender = true

  nodeWidth = theme.node.width
  nodeHeight = theme.node.height

  getLayout(graph) {

    const nodes = graph.processes
    const groups = graph.groups
    const edges = graph.connections

    let kGraphCache = {}

    // keeps track of whether the group/leaf nodes have parents or not;
    // used to determine whether to write out to the final kGraph to eliminate redundancy
    let nodeHasParent = {}

    const convertToKNode = (nodeKey => {
      const inports = nodes[nodeKey].ports.inputs.map((inputKey, idx) => {
        return {
          id: nodeKey + '_' + inputKey,
          x: 0,
          y: (idx+1) * 10
        }
      })
      const outports = nodes[nodeKey].ports.outputs.map((outputKey, idx) => {
        return {
          id: nodeKey + '_' + outputKey,
          x: 100,
          y: (idx+1) * 10
        }
      })
      return {
        id: nodeKey,
        width: this.nodeWidth - 30,
        height: this.nodeHeight,
        ports: inports.concat(outports),
        properties: {
          portConstraints: "FIXED_POS"
        }
      }
    })

    const convertToKGraph = (id => {
      if (kGraphCache[id]) {
        return kGraphCache[id]
      }
      if (nodes[id]) {
        const kGraph = convertToKNode(id)
        nodeHasParent[id] = true
        kGraphCache[id] = kGraph
        return kGraph
      } else {
        const groupNode = groups.find(group => {
          return group.name == id
        })
        const children = groupNode.nodes.map(_id => {
          nodeHasParent[_id] = true
          return convertToKGraph(_id)
        })
        const kGraph = {
          id,
          children
        }
        kGraphCache[id] = kGraph
        return kGraph
      }
    })

    let kGraph = {
      "id": "root",
      "properties": {
        "direction": "RIGHT"
      },
      "children": [],
      "edges": []
    }

    const groupGraphs = groups.map(group => {
      const groupKGraph = convertToKGraph(group.name)
      return groupKGraph
    })

    const unseenGroupGraphs = groupGraphs.filter(group => {
      return !nodeHasParent[group.id]
    })

    const unseenNodeGraphs = Object.keys(nodes).filter(key => {
      return !nodeHasParent[key]
    }).map(key => {
      return convertToKNode(key)
    })

    kGraph.children = unseenNodeGraphs.concat(unseenGroupGraphs)

    kGraph.edges = edges.map(edge => {
      return {
        id: edge.metadata.route,
        source: edge.src.process,
        sourcePort: edge.src.process + '_' + edge.src.port,
        target: edge.tgt.process,
        targetPort: edge.tgt.process + '_' + edge.tgt.port
      }
    })

    //const kGraph = nofloToKieler(graph, "RIGHT")

    let newLayout = {}

    $klay.layout({
      graph: kGraph,
      options: {
        "intCoordinates": true,
        "algorithm": "de.cau.cs.kieler.klay.layered",
        "layoutHierarchy": true,
        "spacing": 90,
        "borderSpacing": 50,
        "edgeSpacingFactor": 0.5,
        //"inLayerSpacingFactor": 2.0,
        "nodePlace": "BRANDES_KOEPF",
        "nodeLayering": "NETWORK_SIMPLEX",
        "edgeRouting": "POLYLINE",
        "crossMin": "LAYER_SWEEP",
        "direction": "RIGHT"
      },
      success: function(_layout) {
        newLayout = _layout
      },
      error: function(error) {
        console.error('klay autolayout failed')
        console.error(error)
      }
    })

    // newLayout gives a recursive tree; flatten it for easier consumption

    const flattenLayout = (node, xOffset, yOffset) => {
      if (node.children) {
        const flattenChildren = node.children.reduce((obj, child) => {
          const flat = flattenLayout(child, xOffset + (node.x || 0), yOffset + (node.y || 0))
          return Object.assign(obj, flat)
        }, {})
        // don't add children to the node since we are flattening
        const { children, ...otherProps } = node
        const nodeLayout = Object.assign({}, { [node.id]: otherProps }, flattenChildren)
        return nodeLayout
      } else {
        return { [node.id]: Object.assign({}, node, { x: node.x + xOffset, y: node.y + yOffset }) }
      }
    }

    const flatLayout = flattenLayout(newLayout, 0, 0)

    //const nodeLayout = flatLayout.reduce((obj, item) => {
    //  obj[item.id] = item
    //  return obj
    //}, {})

    return { "nodes": flatLayout }

    /*
    const graph = new dagre.graphlib.Graph()
    graph.setGraph({
      nodesep: 50,
      ranksep: 50,
      rankdir: 'LR',
      marginx: 0,
      marginy: 0
    })
    graph.setDefaultEdgeLabel(function() { return {} })

    Object.keys(nodes).forEach((key) => {
      const node = nodes[key]
      graph.setNode(key, { label: node.label, width: 100, height: 100 })
    })

    connections.forEach(function (connection) {
      graph.setEdge(connection.src.process, connection.tgt.process)
    })

    dagre.layout(graph)
    return graph
    */
  }

  componentDidMount() {
    console.debug('Workflow componentDidMount')

    keymaster('backspace', (event) => {
      // prevent browser from navigating back
      event.preventDefault()
      // delete selected nodes
      this.props.onRemoveNodes(this.state.selectedNodes)
    })

    this.jsplumb = jsPlumb.getInstance()

    this.jsplumb.importDefaults({
      //Connector : [ "Bezier", { curviness: 101 } ],
      Connector : [ "Flowchart", {cornerRadius: 5} ],
      EndpointStyle : { fillStyle : "gray", radius: 5 },
      //EndpointHoverStyle:{ radius: 7 },
      PaintStyle: { strokeStyle: "#ccc", lineWidth: 3 }
      //HoverPaintStyle: { lineWidth: 4 },
      //Overlays: [ "Arrow", { width: 10, length: 10 } ]
      //DeleteEndpointsOnDetach: false
    })

    this.workflowPane = jsPlumb.getInstance();

    this.workflowPane.draggable('workflow-pane', {
      stop: function (event) {
        // update the node positions in model when the user is done panning (panning is handled by jsplumb)
        this.props.onShiftNodePositions(event.pos[0], event.pos[1])
      }.bind(this)
    })

    this.syncWorkflow()
  }

  // since onClick handler is invoked when you stop dragging a jsplumb node for some reason,
  // we need to distinguish between an actual click vs drag-induced click
  // this method is to be called by WorkflowNode to reset the drag tracking state
  onDragDone = (x, y) => {
    const workflow = this.props.workflow.toJS()
    Object.keys(workflow.graph.processes).forEach(nodeId => {
      const domNode = document.getElementById(nodeId)
      this.props.onSetNodePosition(nodeId, { x: domNode.offsetLeft, y: domNode.offsetTop })
    })
    this.setState({
      'isDragged': false,
      'draggedNode': null
    })
  }

  syncWorkflow() {
    console.debug('Workflow syncWorkflow')
    console.time('syncWorkflow')

    this.jsplumb.reset()

    const scale = 1.0
    this.jsplumb.setContainer("workflow-pane")
    const containerDom = this.jsplumb.getContainer()
    containerDom.style.cssText = "-webkit-transform: scale(" + scale + ")"
    this.jsplumb.setZoom(scale)

    // bind to jsplumb events so that we can update the model based on edge addition/removal via dragging
    // endpoints
    // note that originalEvent is undefined if the event was triggered programmatically; in such case,
    // do not update model because it's just the render function reconstructing the jsplumb objects
    this.jsplumb.bind("connection", (info, originalEvent) => {
      // update the model to reflect the new connection if initiated by dragging
      if (originalEvent) {
        console.debug('connection created between ' + info.sourceEndpoint.anchor.cssClass +
          ' and ' + info.targetEndpoint.anchor.cssClass)
        this.props.onAddEdge(info.sourceEndpoint.anchor.cssClass, info.targetEndpoint.anchor.cssClass)
      }
    })

    this.jsplumb.bind("connectionDetached", (info, originalEvent) => {
      // update the model to reflect the detached connection if initiated by dragging
      if (originalEvent) {
        console.debug('connection detached from ' + info.sourceEndpoint.anchor.cssClass +
          ' and ' + info.targetEndpoint.anchor.cssClass)
        this.props.onRemoveEdge(info.sourceEndpoint.anchor.cssClass, info.targetEndpoint.anchor.cssClass)
      }
    })

    this.jsplumb.bind("connectionMoved", (info) => {
      console.log('connection moved: ' + info)
    })

    this.jsplumb.draggable(document.getElementsByClassName("workflow-node"), {
      containment: true, // prevent elements being dragged out of the main panel
      start: (event) => {
        // this works in conjunction with onDragDone
        console.debug('begin dragging the node...')
        this.setState({'isDragged': true, 'draggedNode': event.el.id})
      }
    })

    const workflow = this.props.workflow.toJS()

    // add all endpoints
    let endpoints = {}
    const nodes = workflow.graph.processes
    const anchorOffset = { x: 0, y: 0 }
    Object.entries(nodes).forEach(([key, node]) => {
      const inputs = node.ports.inputs
      const numInPorts = inputs.length
      // distribute the ports vertically on the left side for inPorts and on the right side for outPorts
      const {color} = getCategoryRenderProps(componentDefinition[node.component]['category'])
      inputs.forEach((inPort, idx) => {
        const endpointId = key + ':' + inPort
        endpoints[endpointId] = this.jsplumb.addEndpoint(
          key,
          { isTarget: true,
            anchor: [ 0, (idx + 1) / (numInPorts + 1), -1, 0, -anchorOffset.x, -anchorOffset.y, endpointId],
            paintStyle: {fillStyle: color, radius: 3},
            cssClass: 'workflow-node-endpoint'
          }
        )
      })
      const outputs = node.ports.outputs
      const numOutPorts = outputs.length
      outputs.forEach((outPort, idx) => {
        const endpointId = key + ':' + outPort
        const {color} = getCategoryRenderProps(componentDefinition[node.component]['category'])
        endpoints[endpointId] = this.jsplumb.addEndpoint(
          key,
          {
            isSource: true,
            maxConnections: -1,
            anchor: [ 1, (idx + 1) / (numOutPorts + 1), 1, 0, -anchorOffset.x, -anchorOffset.y, endpointId],
            paintStyle: {fillStyle: color, radius: 2.5},
            cssClass: 'workflow-node-endpoint'
          }
        )
      })
    })

    // add all connections
    const connections = workflow.graph.connections
    connections.forEach((conn, idx) => {
      // connect conn.src.process with conn.tgt.process
      const sourceEndpointId = conn.src.process + ':' + conn.src.port
      const targetEndpointId = conn.tgt.process + ':' + conn.tgt.port

      let connectionSpec = {
        source: endpoints[sourceEndpointId],
        target: endpoints[targetEndpointId],
        endPoint: "circle",
        deleteEndpointsOnDetach: false,
        overlays:[
          [ "PlainArrow", { location: -10, width: 10, length: 5, cssClass: 'workflow-edge-overlay' } ],
          [ "PlainArrow", { location: 20, width: 10, length: 5, cssClass: 'workflow-edge-overlay' } ],
          [ "Label", { label: conn.src.port.toUpperCase(), location: 20, cssClass: 'workflow-edge-overlay-label workflow-edge-path-label' }],
          [ "Label", { label: conn.tgt.port.toUpperCase(), location: -20, cssClass: 'workflow-edge-overlay-label workflow-edge-path-label right' }]
          //[ "Custom", { create: (component) => '<i className="material-icons chart md-16">insert_chart</i>', location:0.5 }]
        ],
        cssClass: 'workflow-edge'
      }

      // if the samples exist on the edge, inject the custom overlay for edge editor
      if (workflow.samples && workflow.samples[sourceEndpointId.replace(':', '_')]) {
        const customOverlay = [
          "Custom", {
            create: (component) => {
              const iconBox = document.createElement('div')
              // iconBox.style.alpha = 0.5
              const icon = document.createElement('i')
              icon.className = 'material-icons'
              icon.innerHTML = 'insert_chart'
              iconBox.appendChild(icon)
              return iconBox
            },
            location: 0.5,
            cssClass: 'workflow-edge-overlay workflow-edge-chart',
            id: "label",
            events: {
              click: (overlay, originalEvent) => {
                if (originalEvent) {
                  console.debug('edge overlay clicked')
                  this.showEdgeEditor(originalEvent, sourceEndpointId)
                }
              }
            }
          }
        ]
        // console.log('custom overlay: ', customOverlay)
        connectionSpec.overlays.push(customOverlay)
      }
      this.jsplumb.connect(connectionSpec)
    })

    const selectedNodeKeys = Object.keys(this.state.selectedNodes)
    if (selectedNodeKeys.length >= 2) {
      this.jsplumb.addToPosse(selectedNodeKeys, "selectedNodes")
    }

    console.timeEnd('syncWorkflow')
  }

  componentDidUpdate(prevProps, prevState) {
    console.debug('Workflow componentDidUpdate')
    if (prevProps !== this.props || prevState.nodeSelector.isActive && !this.state.nodeSelector.isActive) {
      console.debug('Workflow.props.workflow changed or nodeSelector selection was made')
      console.debug('Syncing workflow with jsplumb')
      this.syncWorkflow()
    }
  }

  componentWillUnmount() {
    console.debug('Workflow componentWillUnmount')
    this.jsplumb.reset()

    keymaster.unbind('backspace')
  }

  clear() {
    this.jsplumb.reset()
    this.props.onReset()
  }

  // BEGIN handle node selection

  fromClientToElementCoordinates(x, y) {
    const {top, left} = ReactDOM.findDOMNode(this).getBoundingClientRect()
    return [x - left, y - top]
  }
/*
  onMouseDown = (event) => {
    console.debug('Workflow onMouseDown')
    // disable text highlighting on mouse drag by suppressing default browser behavior
    event.preventDefault()

    const [x, y] = this.fromClientToElementCoordinates(event.clientX, event.clientY)

    this.setState({
      nodeSelector: {
        isActive: true,
        source: {
          x,
          y
        },
        target: {
          x,
          y
        }
      }
    })
  }

  onMouseUp = (event) => {
    console.debug('Workflow onMouseUp')
    if (this.state.nodeSelector.isActive) {
      this.setState((prevState) => {
        return {
          nodeSelector: {
            isActive: false,
            source: prevState.nodeSelector.source,
            target: prevState.nodeSelector.target
          }
        }
      })
    }
  }

  onMouseMove = (event) => {
    if (this.state.nodeSelector.isActive) {

      // save coordinates in local variables because the event object
      // won't be available in the setState callback
      const [x,y] = this.fromClientToElementCoordinates(event.clientX, event.clientY)

      // detect which nodes are under selection
      const source = this.state.nodeSelector.source

      const minX = Math.min(source.x, x)
      const maxX = Math.max(source.x, x)
      const minY = Math.min(source.y, y)
      const maxY = Math.max(source.y, y)

      const selectedNodes = {}
      const domNodes = document.getElementsByClassName('workflow-node')
      for (let idx = 0; idx < domNodes.length; idx++) {
        const node = domNodes[idx]
        const rect = node.getBoundingClientRect()
        const [ rectMinX, rectMinY ]  = this.fromClientToElementCoordinates(rect.left, rect.top)
        const [ rectMaxX, rectMaxY ]  = this.fromClientToElementCoordinates(rect.right, rect.bottom)

        const overlap = !(maxX < rectMinX ||
        minX > rectMaxX ||
        maxY < rectMinY ||
        minY > rectMaxY)

        if (overlap) {
          selectedNodes[node.id] = true
        }
      }

      this.setState((prevState) => {
        return {
          nodeSelector: {
            isActive: prevState.nodeSelector.isActive,
            source: prevState.nodeSelector.source,
            target: {
              x,
              y
            }
          },
          selectedNodes
        }
      })
    } else if (this.state.isDragged) {
      console.debug('node is being dragged')
      const [x, y] = this.fromClientToElementCoordinates(event.clientX, event.clientY)
      // this re-renders the node group box if any, but this is too slow at the moment so commenting it out
      // this.props.onSetNodePosition(this.state.draggedNode, {x, y})
    }

  }
*/
  // END handle node selection

  // BEGIN handle node editor
  showNodeEditor = (event, nodeId, node) => {
    if (node['category'] == 'visualize') {
      this.showDataVisualizer(event);
    } else {
      this.setState({
        nodeEditorNodeId: nodeId,
        nodeEditorNode: node,
        isNodeEditorShown: true,
        anchorEl: event.currentTarget
      })
    }
  }

  onSaveNodeEditor = (nodeId, node) => {
    this.props.onUpdateNode(nodeId, node)
    this.setState({
      isNodeEditorShown: false
    })
  }

  onCancelNodeEditor = () => {
    this.setState({
      isNodeEditorShown: false
    })
  }
  // END handle node editor

  // BEGIN handle edge editor
  showEdgeEditor = (event, endpointId) => {
    const edgeEditorSamples = this.props.workflow.toJS().samples[endpointId.replace(':', '_')]
    if (edgeEditorSamples) {
      this.setState({
        edgeEditorSamples,
        isEdgeEditorShown: true,
        anchorEl: event.srcElement
      })
    }
  }

  onApplyEdgeEditor = () => {
    this.setState({
      isEdgeEditorShown: false
    })
  }

  onCancelEdgeEditor = () => {
    this.setState({
      isEdgeEditorShown: false
    })
  }
  // END handle edge editor

  // BEGIN handle data visualizer
  showDataVisualizer = (event) => {
    this.setState({
      isDataVisualizerShown: true,
      anchorEl: event.currentTarget
    })
  }

  onApplyDataVisualizer = () => {
    this.setState({
      isDataVisualizerShown: false
    })
  }

  onCancelDataVisualizer = () => {
    this.setState({
      isDataVisualizerShown: false
    })
  }
  // END handle data visualizer

  // BEGIN handle context menu
  onContextMenu = (event) => {
    console.debug('Workflow onContextMenu')
    event.preventDefault()
    const { clientX: x , clientY: y } = event
    this.setState({
      nodeSelector: {
        isActive: false,
        source: {
          x: 0,
          y: 0
        },
        target: {
          x: 0,
          y: 0
        }
      },
      isContextMenuShown: true,
      contextMenuPosition: {
        x,
        y
      }
    })
  }

  onGroupNodes = () => {
    console.debug('Workflow: grouping selected nodes')
    this.props.onGroupNodes(this.state.selectedNodes)
    this.setState({
      isContextMenuShown: false
    })
  }

  onRemoveNodes = () => {
    console.debug('Workflow: deleting selected nodes')
    this.props.onRemoveNodes(this.state.selectedNodes)
    this.setState({
      isContextMenuShown: false
    })
  }

  onCancelContextMenu = () => {
    this.setState({
      isContextMenuShown: false
    })
  }
  // END handle context menu

  // BEGIN handle workflow lifecycle actions

  runWorkflow = () => {
    this.setState({ isRunning: true})
    // only pass "graph" (drop "samples" and any other attributes)
    let body = { graph: this.props.workflow.toJS().graph }
    // force interim data to be created for all nodes
    Object.keys(body.graph.processes).forEach(id => {
      body.graph.processes[id].properties.interim = true
    })
    const headers = new Headers()
    headers.append("Content-Type", "application/json")
    fetch(`${API_Config.serverUrl}/workflows/1/runs`, {
      method: "post",
      headers: headers,
      body: JSON.stringify(body)
    }).then(response => {
      const status = (response.ok) ? 'SUCCESS' : 'FAIL'

      if (!response.ok) {
        let responseType = 'text'
        if (response.headers.get('content-type').indexOf('text/html') >= 0) {
          responseType = 'html'
        }
        response.text().then(text => {
          const content = responseType == 'html' ?
            text : 'The workflow failed to run: ' + response.status + ' ' + response.statusText + ' ' + text
          this.showModalAlert('Failure', content, responseType)
        })
      } else {
        response.json().then(json => {
          this.props.onSetSamples(json)
          this.showModalAlert('Success', 'The workflow executed successfully')
        })
      }
      this.setState({ isRunning: false, lastRunStatus: status })
    }).catch(response => {
      this.showModalAlert('Failure', 'Error communicating with the server.  Try again later.')
      this.setState({ isRunning: false, lastRunStatus: 'FAIL' })
    })
  }

  getWorkflowRun = () => {
    const headers = new Headers()
    headers.append("Content-Type", "application/json")
    fetch(`${API_Config.serverUrl}/workflows/a_simple_graph/runs/1/samples`, {
      method: "get",
      headers: headers
    }).then(response => {
      response.json().then(json => {
      })
    }).catch(response => {
      alert('Error communicating with the server.  Try again later.')
    })
  }

  getWorkflowStatus = () => {
    const workflowId = this.props.workflow.getIn(['graph','metainfo','id'])
    const headers = new Headers()
    headers.append("Content-Type", "application/json")
    fetch(`${API_Config.serverUrl}/workflows/${workflowId}/runs/1/status`, {
      method: "get",
      headers: headers
    }).then(response => {
      response.text().then(text => {
        this.setState({lastRunDetails: text, isLastRunStatusShown: true })
      })
    }).catch(response => {
      alert('Error communicating with the server.  Try again later.')
    })
  }

  stopWorkflow = () => {
    alert('not implemented')
  }

  // END handle workflow lifecycle actions

  // BEGIN handle status popup

  onCloseWorkflowStatus = () => {
    this.setState({'isLastRunStatusShown': false})
  }

  // END handle status popup

  showModalAlert(title, content, format) {
    format = format || 'text'
    this.setState({
      isModalAlertShown: true,
      modalAlertTitle: title,
      modalAlertContent: content,
      modalAlertFormat: format
    })
  }

  closeModalAlert() {
    this.setState({
      isModalAlertShown: false,
      modalAlertTitle: '',
      modalAlertContent: '',
      modalAlertFormat: ''
    })
  }

  render() {
    console.debug('Workflow render')
    console.time('Workflow render')

    const workflow = this.props.workflow.toJS()

    // perform autolayout on initial render for now
    // TODO: remember the node coordinates instead
    const layout = (this.initialRender) ? this.getLayout(workflow.graph) : null

    // render all nodes in the graph
    const nodes = Object.keys(workflow.graph.processes).map(key => {
      const node = workflow.graph.processes[key]

      const x = layout ? layout.nodes[key].x : node.metadata.x
        //document.getElementById(key) ? document.getElementById(key).offsetLeft : node.metadata.x
      const y = layout ? layout.nodes[key].y : node.metadata.y
        //document.getElementById(key) ? document.getElementById(key).offsetTop : node.metadata.y

      if (!workflow.rendered) {
        node.metadata.x = x
        node.metadata.y = y
      }

      const isSelected = !!this.state.selectedNodes[key];
      let componentDef = componentDefinition[node.component] || [];

      return (
        <WorkflowNode ref={key}
                      key={key}
                      id={key}
                      x={node.metadata.x}
                      y={node.metadata.y}
                      label={node.metadata.label}
                      type={node.component}
                      category={componentDef['category']}
                      isSelected={isSelected}
                      isDragged={this.state.isDragged}
                      onClick={(event) => this.showNodeEditor(event, key, node)}
                      onDragDone={(x, y) => this.onDragDone(x, y)}
                      onRemoveNode={this.props.onRemoveNode} />
      )
    })

    let nodeSelector = ''
    if (this.state.nodeSelector.isActive) {
      const { source, target } = this.state.nodeSelector
      const width = Math.abs(target.x - source.x)
      const height = Math.abs(target.y - source.y)

      nodeSelector =
        <svg width={width} height={height}
             style={{
               position: "absolute",
               left: Math.min(source.x, target.x),
               top: Math.min(source.y, target.y)
             }}>

          <rect width={width}
                height={height}
                fill="#ffffff"
                fillOpacity="0.5"
                strokeWidth="1"
          />

        </svg>
    }

    // find the bounding box recursively
    const getBoundingBox = (itemId) => {

      const node = workflow.graph.processes[itemId]

      if (node) {
        // this is a node
        return {
          left: node.metadata.x,
          right: node.metadata.x + this.nodeWidth,
          top: node.metadata.y,
          bottom: node.metadata.y + this.nodeHeight
        }
      } else {
        // this is a group
        // the bounding box is the smallest box thats contain all the nodes/groups nested inside
        const group = workflow.graph.groups.find((group) => {
          return group.name == itemId
        })

        let minX = Number.MAX_VALUE, minY = Number.MAX_VALUE, maxX = Number.MIN_VALUE, maxY = Number.MIN_VALUE

        group.nodes.forEach(groupItemId => {
          const { top, right, bottom, left } = getBoundingBox(groupItemId)
          if (left < minX) {
            minX = left
          }
          if (top < minY) {
            minY = top
          }
          if (right > maxX) {
            maxX = right
          }
          if (bottom > maxY) {
            maxY = bottom
          }
        })

        const groupTopPadding = 36
        const groupBottomPadding = 50
        const groupSidePadding = 20

        return {
          top: minY - groupTopPadding,
          right: maxX + groupSidePadding,
          bottom: maxY + groupBottomPadding,
          left: minX - groupSidePadding
        }
      }
    }

    const groups = workflow.graph.groups.map(group => {
      // for each group, draw a bounding box that is just big enough to contain all the nodes/groups nested inside

      const { top, right, bottom, left } = getBoundingBox(group.name)

      const width = right - left
      const height = bottom - top
      return (
        <WorkflowNodeGroup id={group.name}
                           label={group.name}
                           key={group.name}
                           name={group.component}
                           x={left}
                           y={top}
                           width={width}
                           height={height}
                           isSelected={false}
                           onRemove={() => {}}
                           onClick={() => {}}
        />
      )
    })

    const workflowEdgeEditor = (this.state.isEdgeEditorShown) ? (
      <WorkflowEdgeEditor samples={this.state.edgeEditorSamples}
                          isShown={this.state.isEdgeEditorShown}
                          anchorEl={this.state.anchorEl}
                          onApply={this.onApplyEdgeEditor}
                          onCancel={this.onCancelEdgeEditor}
      />
    ) : ''

    // save node coordinates in the model if this was a initial render and klayjs calculated the layout automatically
    if (this.initialRender) {
      Object.keys(workflow.graph.processes).forEach(nodeId => {
        const node = workflow.graph.processes[nodeId]
        if (node) {
          this.props.onSetNodePosition(nodeId, { x: node.metadata.x, y: node.metadata.y} )
        }
      })
    }

    const modalAlert = this.state.isModalAlertShown ? (
      <ModalAlert title={this.state.modalAlertTitle}
                  isShown={this.state.isModalAlertShown}
                  content={this.state.modalAlertContent}
                  format={this.state.modalAlertFormat}
                  onClose={() => { this.closeModalAlert() }}
      />
    ) : ''

    this.initialRender = false

    console.timeEnd('Workflow render')

    return (
      <div className="workflow"
           onContextMenu={this.onContextMenu}
           onMouseDown={this.onMouseDown}
           onMouseUp={this.onMouseUp}
           onMouseMove={this.onMouseMove}>

        <div className="workflow-pane" id="workflow-pane">
          {nodes}

          {groups}
        </div>

        {nodeSelector}

        <WorkflowRunner isShown={true}
                        isRunning={this.state.isRunning}
                        lastRunStatus={this.state.lastRunStatus}
                        lastRunDetails={this.state.lastRunDetails}
                        onRun={this.runWorkflow}
                        onStop={this.stopWorkflow}
                        onGetStatus={this.getWorkflowStatus}
        />

        <WorkflowStatus isShown={this.state.isLastRunStatusShown}
                        content={this.state.lastRunDetails}
                        onClose={this.onCloseWorkflowStatus}
        />

        {modalAlert}

        <WorkflowContextMenu isShown={this.state.isContextMenuShown}
                             position={this.state.contextMenuPosition}
                             node={this.state.nodeEditorNode}
                             onGroupNodes={this.onGroupNodes}
                             onRemoveNodes={this.onRemoveNodes}
                             onCancel={this.onCancelContextMenu}
        />

        <WorkflowNodeEditor isShown={this.state.isNodeEditorShown}
                            id={this.state.nodeEditorNodeId}
                            node={this.state.nodeEditorNode}
                            anchorEl={this.state.anchorEl}
                            onSave={this.onSaveNodeEditor}
                            onCancel={this.onCancelNodeEditor}
        />

        {workflowEdgeEditor}

        <DataVisualizer isShown={this.state.isDataVisualizerShown}
                        anchorEl={this.state.anchorEl}
                        onApply={this.onApplyDataVisualizer}
                        onCancel={this.onCancelDataVisualizer}
        />
      </div>
    )
  }
}
