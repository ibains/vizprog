import React from 'react';
import Immutable from 'immutable';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton'
import AutoComplete from 'material-ui/AutoComplete';

import Column from './Column';

import * as ProjectActions from '../actions/projectActions';
import * as WorkflowActions from '../actions/workflowActions';

//we probably shouldnt be importing this here, but we don't
//currently manage project creation well so it's a bit of a hack
import { ProjectRecord } from '../constants/records';
import { convertToImmutableRecords } from '../utils/records';

const NEW_WORKFLOW = convertToImmutableRecords({ content: { graph: {} } });

class NewWorkflowOptions extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { template: null, projectName: '', workflowName: '' };
    this._createWorkflow = this._createWorkflow.bind(this);
  }
  componentWillMount() {
    this.props.actions.getUserProjects();
  }
  _createWorkflow() {
    const { template, projectName, workflowName } = this.state;
    const { projects, actions } = this.props;
    let newWorkflow = (template || NEW_WORKFLOW).set('name', workflowName);
    let existingProject = projects.find(p => p.name == projectName);
    if (existingProject) {
      newWorkflow = newWorkflow.set('project', existingProject);
      actions.createWorkflow(newWorkflow);
    } else {
      actions.createWorkflowWithNewProject(newWorkflow, new ProjectRecord({ name: projectName }));
    }
    this.props.close();
  }
  render() {
    const { projectName, workflowName, template } = this.state;
    const { projects, templates } = this.props;
    return (
      <Column style={{ width: '66.66%' }}>
        <AutoComplete
          floatingLabelText='Name your project'
          onUpdateInput={(val) => this.setState({ projectName: val })}
          onNewRequest={(val) => this.setState({ projectName: val })}
          searchText={projectName}
          filter={AutoComplete.fuzzyFilter}
          dataSource={projects.valueSeq().toJS().map(p => p.name)}
          maxSearchResults={5}
          fullWidth={true} />
        <TextField fullWidth
          hintText='Name your workflow'
          floatingLabelText='Workflow'
          onChange={(event, value) => this.setState({ workflowName: value })}
          value={ workflowName } />
        <SelectField fullWidth
          value={template}
          onChange={(e, ind, val) => this.setState({ template: val, workflow: val.description })}
          floatingLabelText='Select a template (optional)'>
          { templates.map((template) => <MenuItem value={template} primaryText={template.name} />).toArray() }
        </SelectField>
        <RaisedButton disabled={!workflowName || !projectName} primary={true} label="Create" style={{ float: 'right' }} onTouchTap={this._createWorkflow} />
      </Column>
    );
  }
}

let mapDispatchToProps = dispatch => ({
  actions: Object.assign({},
                         bindActionCreators(WorkflowActions, dispatch),
                         bindActionCreators(ProjectActions, dispatch))
});
export default connect(state => ({ templates: state.templates, projects: state.projects }), mapDispatchToProps)(NewWorkflowOptions);
