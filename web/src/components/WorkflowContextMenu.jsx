import React from 'react'
import ReactDOM from 'react-dom'
import shouldPureComponentUpdate from 'react-pure-render/function'
import MenuItem from 'material-ui/MenuItem'
import Popover from 'material-ui/Popover'
import FontIcon from 'material-ui/FontIcon'

export class WorkflowContextMenu extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    node: React.PropTypes.object.isRequired,
    isShown: React.PropTypes.bool.isRequired,
    position: React.PropTypes.object.isRequired,
    onGroupNodes: React.PropTypes.func.isRequired,
    onRemoveNodes: React.PropTypes.func.isRequired,
    onCancel: React.PropTypes.func.isRequired
  }

  componentDidMount() {
  }

  render() {
    const anchorOrigin = {
      "horizontal":"left",
      "vertical":"bottom"
    }

    const targetOrigin = {
      "horizontal":"left",
      "vertical":"top"
    }

    const anchorDiv = (
      <div ref='anchorDiv' style={{
          position: 'fixed',
          top: this.props.position.y,
          left: this.props.position.x
        }} />
    )

    return (
      <div>
        {anchorDiv}
        <Popover open={this.props.isShown}
                 anchorEl={this.refs.anchorDiv}
                 anchorOrigin={anchorOrigin}
                 targetOrigin={targetOrigin}
                 onRequestClose={this.props.onCancel}
                 style={{ marginLeft: 5, marginTop: 5 }}>
          <div>
            <MenuItem onTouchTap={this.props.onGroupNodes}>Group</MenuItem>
            <MenuItem onTouchTap={this.props.onRemoveNodes}>Delete</MenuItem>
          </div>
        </Popover>
      </div>
    )
  }
}
