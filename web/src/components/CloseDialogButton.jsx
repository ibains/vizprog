import React from 'react';

const CloseDialogButton = (props) => (
  <button type="button" className="closeButton" title="Close" onClick={props.close}>
    <i className="material-icons md-16">{'close'}</i>
  </button>
);

export default CloseDialogButton;
