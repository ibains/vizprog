import React from 'react'
import ReactDOM from 'react-dom';
import Icon from 'react-fa'

import ProcessLogo from './ProcessLogo';

import { PROCESS_DEFINITIONS } from '../constants/process';

export default class ProcessNode extends React.Component {
  render() {
    const { id, process, dragging, onDragEnd, actions } = this.props;
    const style = { left: process.metadata.get('x'), top: process.metadata.get('y') };
    const { category, label } = PROCESS_DEFINITIONS[process.component];
    return (
      <div id={id} className={'node-box workflow-node ' + category} draggable='true' style={style} >
        <div className="node" onClick={dragging ? onDragEnd : actions.selectWorkflowProcess}>
          <Icon name="close" className='closeIcon' onClick={(event) => { actions.removeWorkflowProcess(); event.stopPropagation(); }}></Icon>
          <ProcessLogo category={category} height='100%' width='100%'/>
        </div>
        <h7 className="node-title">{label}</h7>
        <h7 className="node-subtitle">{process.metadata.get('label')}</h7>
      </div>
    );
  }
}
