import React from 'react';
import Immutable from 'immutable';
import { ResizableBarChart, ResizableLineChart, ResizablePieChart, ResizableBoxPlot, ResizableScatterPlot } from './ResizableGraph';
import Column from './Column';
import Card from './Card';
import AxisSelector from './AxisSelector';
import ChartSelector from './ChartSelector';
import { CHART_TYPES } from '../constants/chartTypes';

export default class Visualize extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.save = this.save.bind(this);
    const columns = this._getColumns(props.id, props.workflow);
    const xColumnValue = props.workflow.processes.get(props.id).properties.get('xColumn');
    const yColumnValue = props.workflow.processes.get(props.id).properties.get('yColumn');
    const chartType = props.workflow.processes.get(props.id).properties.get('chartType');
    const xColumn = columns.findIndex(col => col == xColumnValue);
    const yColumn = columns.findIndex(col => col == yColumnValue);
    this.state = {
      xColumn: xColumn > -1 ? xColumn : 0,
      yColumn: yColumn > -1 ? yColumn : 1,
      chartType: chartType ? chartType : CHART_TYPES[0].value
    };
  }
  _getColumns(processId, workflow) {
    const connection = workflow.connections.find(conn => conn.tgt.process == processId);
    return workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'schema']).keySeq();
  }
  _getData(processId, xColumn, yColumn, workflow) {
    let connection = workflow.connections.find(conn => conn.tgt.process == processId);
    let data = workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'data']);
    return data.mapEntries(([label, row]) => [row.get(xColumn), row.get(yColumn)]);
  }
  _getBoxPlotData(processId, column, workflow) {
    let connection = workflow.connections.find(conn => conn.tgt.process == processId);
    let data = workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'data']);
    return data.map(row => row.get(column));
  }
  save() {
    const { xColumn, yColumn, chartType } = this.state;
    const columns = this._getColumns(this.props.id, this.props.workflow);
    this.props.onChange(columns.get(xColumn), columns.get(yColumn), chartType);
  }
  componentWillUpdate(nextProps) {
    const oldColumns = this.props.workflow.processes.get(this.props.id).properties.filter((v, k) => ['xColumn', 'yColumn', 'chartType'].includes(k));
    const newColumns = nextProps.workflow.processes.get(this.props.id).properties.filter((v, k) => ['xColumn', 'yColumn', 'chartType'].includes(k));
    if (!Immutable.is(oldColumns, newColumns)) {
      this.props.close();
    }
  }
  render() {
    const { xColumn, yColumn, chartType } = this.state;
    const { id, workflow } = this.props;
    const columns = this._getColumns(id, workflow);
    let data = null;
    let resizableChart = null;
    let label = {x: 'X-Axis', y: 'Y-Axis'};
    let yAxisSelector = null;
    switch (chartType) {
      case 'BarChart':
        data = this._getData(id, xColumn, yColumn, workflow);
        resizableChart = <ResizableBarChart data={data} xAxis={columns.get(xColumn)} yAxis={columns.get(yColumn)} />;
        break;
      case 'ScatterPlot':
        data = this._getData(id, xColumn, yColumn, workflow);
        resizableChart = <ResizableScatterPlot data={data} xAxis={columns.get(xColumn)} yAxis={columns.get(yColumn)} />;
        break;
      case 'LineChart':
        data = this._getData(id, xColumn, yColumn, workflow);
        resizableChart = <ResizableLineChart data={data} xAxis={columns.get(xColumn)} yAxis={columns.get(yColumn)} />;
        break;
      case 'PieChart':
        data = this._getData(id, xColumn, yColumn, workflow);
        label = {x: 'Label', y: 'Value'};
        resizableChart = <ResizablePieChart data={data} />;
        break;
      case 'BoxPlot':
        data = this._getBoxPlotData(id, xColumn, workflow);
        label = {x: 'Value'};
        resizableChart = <ResizableBoxPlot data={data} />;
        break;
      default:
        resizableChart = <div>Select a valid chart type.</div>;
    }
    if(label.hasOwnProperty('y')) {
      yAxisSelector = <AxisSelector columns={columns} label={label.y} column={yColumn} select={(yColumn) => this.setState({ yColumn })} />;
    }
    return (
      <div>
        <Column>
          <ChartSelector select={(value) => this.setState({ chartType: value })} value={chartType} />
          <AxisSelector columns={columns} label={label.x} column={xColumn} select={(xColumn) => this.setState({ xColumn })} />
            {yAxisSelector}
          <button type="button" onClick={this.save} className="btn">Save Options</button>
        </Column>
        <Column style={{ width: '66.66%' }}>
          <Card highlight={false}>
            {resizableChart}
          </Card>
        </Column>
      </div>
    );
  }
}
