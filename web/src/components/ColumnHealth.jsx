import React from 'react'

const HEALTH_BAR_STYLE = {
  width: '100%',
  height: 6,
  backgroundColor: 'rgb(0, 188, 212)',
  borderRight: '1px solid #ffffff'
};

const ColumnHealth = (props) => {
  let height = '4px';
  if(props.height) {
    height = props.height;
  }
  return (
  <div width='100%' height={height}>
    <div style={{ display: 'inline-block', float: 'left', backgroundColor: 'rgb(0, 188, 212)', width: props.health * 100 + '%', height: height }} />
    <div style={{ display: 'inline-block', float: 'left', backgroundColor: '#7F7F7F', width: (1 - props.health) * 100 + '%', height: height }} />
  </div>
  )
};

export default ColumnHealth;
