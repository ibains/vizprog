import React from 'react';
import Immutable from 'immutable';
import { ResizableBoxPlot } from './ResizableGraph';

import Column from './Column';
import Card from './Card';
import AxisSelector from './AxisSelector';

import { ROW_REGEX } from '../utils/data';

export default class BoxPlot extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.save = this.save.bind(this);
    const columns = this._getColumns(props.id, props.workflow);
    const columnValue = props.workflow.processes.get(props.id).properties.get('column');
    const column = columns.findIndex(col => col == columnValue);
    this.state = {
      column: column > -1 ? column : 0,
    };
  }
  _getColumns(processId, workflow) {
    const connection = workflow.connections.find(conn => conn.tgt.process == processId);
    return workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'schema']).keySeq();
  }
  _getData(processId, column, workflow) {
    let connection = workflow.connections.find(conn => conn.tgt.process == processId);
    let data = workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'data']);
    return data.map(row => row.get(column));
  }
  save() {
    const { column } = this.state;
    const columns = this._getColumns(this.props.id, this.props.workflow);
    this.props.onChange(columns.get(column));
  }
  componentWillUpdate(nextProps) {
    const oldColumn = this.props.workflow.processes.get(this.props.id).properties.get('column');
    const newColumn = nextProps.workflow.processes.get(nextProps.id).properties.get('column');
    if (!Immutable.is(oldColumn, newColumn)) {
      this.props.close();
    }
  }
  render() {
    const { column } = this.state;
    const { id, workflow } = this.props;
    const columns = this._getColumns(id, workflow);
    const data = this._getData(id, column, workflow);
    return (
      <div>
        <Column>
          <AxisSelector columns={columns} label={'Value'} column={column} select={(column) => this.setState({ column })} />
          <button type="button" onClick={this.save} className="btn">Save Options</button>
        </Column>
        <Column style={{ width: '66.66%' }}>
          <Card highlight={false}>
            <ResizableBoxPlot data={data} />
          </Card>
        </Column>
      </div>
    );
  }
}
