import React, {Component} from 'react'
import theme from './Loading.scss'
var path = require('./../../static/assets/images/balls.svg');

export default class Loading extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div className={theme.container}>
        <div className={theme.center}>
          <div>
            <img src={path} style={{width: '80px', height: '80px', float: 'left'}} /> <span style={{fontSize: '3rem', float: 'left', marginTop: '20px', marginLeft: '-8px'}}>Loading...</span>
          </div>
        </div>
      </div>
    );
  }
}
