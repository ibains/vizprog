import React from 'react'
import Immutable from 'immutable';

// This component is a nightmare, caused by the schema of the data in the
// process definitions. TODO: Reformat that schema in an intelligent way.
// An alternative might be to refactor the CSS so that the list elements
// automatically format into multiple columns.
export default class ProcessSelectionDropdown extends React.Component {
  render() {
    const { subcategories, processes, addProcess } = this.props;
    if (subcategories.size) {
      return <DualColumnDropdown subcategories={subcategories} addProcess={addProcess} />;
    } else {
      return <SingleColumnDropdown processes={processes} addProcess={addProcess} />;
    }
  }
}

export class DualColumnDropdown extends React.Component {
  _splitProcesses(subcategories, addProcess) {
    const processCount = subcategories.reduce((count, processes) => count + processes.size, 0);
    let currentCount = 0;
    let left = [];
    let right = [];
    subcategories.sort((a,b) => a.size - b.size).forEach((processes, subcategory) => {
      currentCount += processes.size;
      let column = (currentCount <= Math.floor(processCount/2) ? left : right);
      column.push(<li className='subcategory' key={subcategory}>{subcategory}</li>);
      column.push(...processes.map((p,type) => <ProcessLink key={type} type={type} label={p.get('label')} addProcess={addProcess} />).values());
    })
    return { left, right }
  }
  render() {
    const { left, right } = this._splitProcesses(this.props.subcategories, this.props.addProcess);
    return (
      <div className="dropdown-content">
        <ul>{ left }</ul>
        <ul>{ right }</ul>
      </div>
    );
  }
}

export const SingleColumnDropdown = (props) => (
  <ul className="dropdown-content">
    { props.processes.map((process, type) => <ProcessLink type={type} label={process.get('label')} addProcess={props.addProcess} />) }
  </ul>
);

export const ProcessLink = (props) => {

  let onClickHandler = (event) => {
    props.addProcess(props.type, event.clientX, event.clientY);
  };

  return (
    <li onClick={onClickHandler}>
      <a>
        {props.label}
      </a>
    </li>
  );

};
