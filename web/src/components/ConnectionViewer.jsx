import React from 'react';

import Dialog from 'react-toolbox/lib/dialog';
import { Button, IconButton } from 'react-toolbox/lib/button';

import SampleTable from 'components/SampleTable';
import theme from './ConnectionViewer.scss';

export default class ConnectionViewer extends React.Component {
  render() {
    const { samples, connection, actions, functionDefinitions } = this.props;
    return (
      <Dialog theme={theme} type='large' active={true} onOverlayClick={actions.selectSample.bind(null, null, null)}>
        <IconButton icon='close' className={'close'} onMouseUp={actions.selectSample.bind(null, null, null)}/>
        <SampleTable samples={samples} />
        {/*<Button label='Prepare Data' theme={theme} raised primary onMouseUp={actions.insertWorkflowCleanseProcess.bind(null, connection)} />*/}
      </Dialog>
    );
  }
}
