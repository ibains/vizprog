import React from 'react'
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui/svg-icons/content/add';
import DataUsageIcon from 'material-ui/svg-icons/device/data-usage';
import SendIcon from 'material-ui/svg-icons/content/send';
import InboxIcon from 'material-ui/svg-icons/content/inbox';
import ReportIcon from 'material-ui/svg-icons/content/report';
import PlayCircleFilledIcon from 'material-ui/svg-icons/av/play-circle-filled';

const Icon = () => <IconButton><AddIcon color="#9B9B9B" /></IconButton>;

const BottomMenu = (props) => (
  <div className="app-bottom-menu">
    <IconMenu iconButtonElement={<IconButton />} onChange={(e, path) => props.onNavigate(path)} value={props.path}>
      <MenuItem value="/"><DataUsageIcon color="#757575" /><span>Data Source</span></MenuItem>
      <MenuItem value="/discover"><SendIcon color="#757575" /><span>Dataset</span></MenuItem>
      <MenuItem value="/explore"><InboxIcon color="#757575" /><span>Workflow</span></MenuItem>
      <MenuItem value="/program"><ReportIcon color="#757575" /><span>Example workflow</span></MenuItem>
      <MenuItem value="/admin"><PlayCircleFilledIcon color="#757575" /><span>Execution</span></MenuItem>
    </IconMenu>
  </div>
);

export default BottomMenu;
