import React from 'react'

const ProcessLogo = (props) => {
  props = Object.assign({ style: { backgroundColor: 'white' }}, props);
  switch (props.category) {
    case 'io': return <IOLogo {...props} />; break;
    case 'transform': return <TransformLogo {...props} />; break;
    case 'ml': return <MLLogo {...props} />; break;
    case 'join-split': return <JoinSplitLogo {...props} />; break;
    case 'visualize': return <VisualizeLogo {...props} />; break;
    case 'script': return <ScriptLogo {...props} />; break;
    default:
      console.warn('Unknown process category: ' + props.category);
      return <ScriptLogo {...props} />; break;
  };
};

const UnknownLogo = (props) => (
  <svg width={props.width} height={props.height} viewBox='0 0 70 70' style={props.style}>
  </svg>
)

const IOLogo = (props) => (
  <svg width={props.width} height={props.height} viewBox='0 0 70 70' style={props.style}>
  </svg>
);

const TransformLogo = (props) => (
  <svg width={props.width} height={props.width} viewBox="0 0 70 70" style={props.style}>
  </svg>
);

const MLLogo = (props) => (
  <svg width={props.width} height={props.height} viewBox="0 0 70 70" style={props.style}>
  </svg>
);

const JoinSplitLogo = (props) => (
  <svg width={props.width} height={props.height} viewBox="0 0 70 70" style={props.style}>
  </svg>
);

const VisualizeLogo = (props) => (
  <svg width={props.width} height={props.height} viewBox="0 0 70 70" style={props.style}>
  </svg>
);

const ScriptLogo = (props) => (
  <svg width={props.width} height={props.height} viewBox="0 0 70 70" style={props.style}>
  </svg>
)

export default ProcessLogo;
