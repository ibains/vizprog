import React from 'react';
import Immutable from 'immutable';
import { ResizableBarChart } from './ResizableGraph';

import Column from './Column';
import Card from './Card';
import AxisSelector from './AxisSelector';

import { ROW_REGEX } from '../utils/data';

export default class BarGraph extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.save = this.save.bind(this);
    const columns = this._getColumns(props.id, props.workflow);
    const xColumnValue = props.workflow.processes.get(props.id).properties.get('x');
    const yColumnValue = props.workflow.processes.get(props.id).properties.get('y');
    const xColumn = columns.findIndex(col => col == xColumnValue);
    const yColumn = columns.findIndex(col => col == yColumnValue);
    this.state = {
      xColumn: xColumn > -1 ? xColumn : 0,
      yColumn: yColumn > -1 ? yColumn : 1,
    };
  }
  _getColumns(processId, workflow) {
    const connection = workflow.connections.find(conn => conn.tgt.process == processId);
    return workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'schema']).keySeq();
  }
  _getData(processId, xColumn, yColumn, workflow) {
    let connection = workflow.connections.find(conn => conn.tgt.process == processId);
    let data = workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'data']);
    return data.mapEntries(([label, row]) => [row.get(xColumn), row.get(yColumn)])
  }
  save() {
    const { xColumn, yColumn } = this.state;
    const columns = this._getColumns(this.props.id, this.props.workflow);
    this.props.onChange(columns.get(xColumn), columns.get(yColumn));
  }
  componentWillUpdate(nextProps) {
    const oldColumns = this.props.workflow.processes.get(this.props.id).properties.filter((v, k) => ['xColumn', 'yColumn'].includes(k))
    const newColumns = nextProps.workflow.processes.get(this.props.id).properties.filter((v, k) => ['xColumn', 'yColumn'].includes(k))
    if (!Immutable.is(oldColumns, newColumns)) {
      this.props.close();
    }
  }
  render() {
    const { xColumn, yColumn } = this.state;
    const { id, workflow } = this.props;
    const columns = this._getColumns(id, workflow);
    const data = this._getData(id, xColumn, yColumn, workflow);
    return (
      <div>
        <Column>
          <AxisSelector columns={columns} label={'X-Axis'} column={xColumn} select={(xColumn) => this.setState({ xColumn })} />
          <AxisSelector columns={columns} label={'Y-Axis'} column={yColumn} select={(yColumn) => this.setState({ yColumn })} />
          <button type="button" onClick={this.save} className="btn">Save Options</button>
        </Column>
        <Column style={{ width: '66.66%' }}>
          <Card highlight={false}>
            <ResizableBarChart data={data} xAxis={columns.get(xColumn)} yAxis={columns.get(yColumn)} />
          </Card>
        </Column>
      </div>
    );
  }
}
