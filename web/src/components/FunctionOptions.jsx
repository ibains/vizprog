import React from 'react';
import Immutable from 'immutable';

import Autocomplete from 'react-toolbox/lib/autocomplete';
import Chip from 'react-toolbox/lib/chip';
import {Tab, Tabs} from 'react-toolbox';
import Dialog from 'react-toolbox/lib/dialog';
import Tooltip from 'react-toolbox/lib/tooltip';
import theme from './FunctionOptions.scss';
import { Layout, NavDrawer, Panel, Sidebar } from 'react-toolbox';
import Navigation from 'react-toolbox/lib/navigation';
import Link from 'react-toolbox/lib/link';
import { IconButton } from 'react-toolbox/lib/button'

export default class FunctionOptions extends React.Component {
  constructor(props, context) {
    super(props, context);
    const category = props.functions.size ? props.functions.keySeq().first() : null;
    const func = category ? props.functions.get(category).keySeq().first() : null;
    this.state = { func, category, index: 0 };
  }
  makeDropdownItem(item) {
    const contentStyle = {
      display: 'flex',
      flexDirection: 'column',
      flexGrow: 2
    };
    return (
      <div style={contentStyle}>
        <strong>{item.name}</strong>
        <small>{item.category.replace('Functions', ' Function')}</small>
      </div>
    );
  }
  render() {
    const { func, category, index } = this.state;
    const { functions, active, close, onSelect } = this.props;
    return (
      <Dialog type='large' active={active} onOverlayClick={close} theme={theme}>
        <IconButton icon='close' className={'close'} onMouseUp={close}/>
        <div>
          {'Prepare Functions'}
        </div>
        <Layout theme={theme}>
          <NavDrawer active={true} pinned={true} theme={theme}>
            <Navigation type='vertical' theme={theme}>
              { functions.map((fns, cat) => <Link
                theme={theme}
                className={category == cat ? theme.active : null}
                onClick={() => this.setState({ category: cat })}
                label={cat.replace('Functions','')} /> ) }
            </Navigation>
          </NavDrawer>
          <Panel theme={theme}>
            { functions.get(category).map((fn, name) => <TooltipLink
              active={true}
              theme={theme}
              tooltip={<TooltipContent func={fn} />}
              label={name}
              icon='stop'
              onMouseUp={() => { onSelect(fn); close() }}
            />) }
          </Panel>
        </Layout>
      </Dialog>
    );
  }
}

const TooltipLink = Tooltip(Link);

export const TooltipContent = (props) => {
  return (
    <div>
      <h5>{ props.func.name }</h5>
      <p>{ props.func.description }</p>
      <h5>{'Arguments:'}</h5>
      { props.func.inputTypes.map((input) => <h6>{input.get('name') + ': ' + input.get('datatype')}</h6>) }
    </div>
  );
}
