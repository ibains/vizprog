import React, { PropTypes } from 'react'

import { getGroupBounds } from '../utils/layout';

import { NODE_WIDTH, NODE_HEIGHT } from '../constants/theme';

const GROUP_PADDING = 50;

export default class ProcessGroup extends React.Component {
  render() {
    const { group } = this.props;
    const { processes, groups } = this.props.workflow;
    const { top, bottom, left, right } = getGroupBounds(group, groups, processes);
    const style = { zIndex: -1, left, top, width: right - left, height: bottom - top };
    return (
      <div id={group.name} className="node-group workflow-node-group">
        <div className="node-group-overlay" style={style}>
          <a className="minimize-icon" href="javascript:void(0)"></a>

          <div className="add-group-label">
            <span>{ group.name }</span>
            <i className="material-icons chart md-9">mode_edit</i>
            <input type="text" />
          </div>

          <a href="javascript:void(0)"><i className="material-icons node-group-close md-9">close</i></a>
        </div>
      </div>
    )
  }
}

