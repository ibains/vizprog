import React from 'react';

const DEFAULT_STYLE = {
  float: 'left',
  padding: '0 7px',
  width: '33.33%',
  height: '100%',
}

const Column = (props) => {
  return (
    <div className={'column ' + (props.className || '')} style={Object.assign({}, DEFAULT_STYLE, props.style)}>
      { props.children }
    </div>
  );
}

export default Column;
