import React from 'react';
import Immutable from 'immutable';
import _ from 'lodash';

import ProcessNode from './ProcessNode';
import ProcessGroup from './ProcessGroup';

export default class WorkflowPane extends React.Component {
  componentWillMount() {
    const { processes } = this.props.workflow;
    if (processes.size) {
      const x = processes.first().metadata.get('x');
      const y = processes.first().metadata.get('y');
      if (processes.every(p => p.metadata.get('x') == x && p.metadata.get('y') == y)) {
        this.props.actions.layoutWorkflow();
      }
    }
  }
  render() {
    const { actions, workflow } = this.props;
    const { processes, groups } = workflow;
    return (
      <div className='workflow-pane'>
        { processes.map((p, id) => <ProcessNode {...this.props} key={id} id={id} process={p} actions={_.mapValues(actions, a => a.bind(null, id))} />) }
        { groups.map(g => <ProcessGroup {...this.props} key={g.name} group={g} />) }
      </div>
    );
  }
}

