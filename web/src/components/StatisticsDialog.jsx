import React from 'react';
import Immutable from 'immutable';
import Plotly from 'react-plotlyjs';
import { Histogram, ResizableHorizontalBoxPlot } from './ResizableGraph';
import ColumnHealth from './ColumnHealth.jsx';

import Table from 'react-toolbox/lib/table';
import { Layout, NavDrawer, Panel, Sidebar } from 'react-toolbox';
import Dialog from 'react-toolbox/lib/dialog';
import { IconButton } from 'react-toolbox/lib/button';
import theme from './StatisticsDialog.scss';
import {Tab, Tabs} from 'react-toolbox';
import Column from './Column';
import Card from './Card';
import Tooltip from 'react-toolbox/lib/tooltip';
import Link from 'react-toolbox/lib/link';

const TooltipDiv = Tooltip(Link);
const TooltipText = props => <p>{props.data.split('\n').map(string => <span>{string}<br/></span>)}</p>;

export default class StatisticsDialog extends React.Component {
  render() {
    const { active, close, columnName, samples} = this.props;
    return (
      <Dialog type='large' active={true} onOverlayClick={close} theme={theme}>
        <IconButton icon='close' className={'close'} onMouseUp={close}/>
            <Column style={{width: '30%'}}>
              <Card>
                <LeftPanel {...this.props} />
              </Card>
            </Column>
            <Column style={{width: '70%'}}>
              <div className={`${theme.flexContainer} ${theme.row}`}>
                <StatsTabs samples={samples} columnName={columnName} />
              </div>
            </Column>
      </Dialog>
    );
  }
}

const LeftPanel = (props) => {
  const empty = parseInt(props.samples.nulls.get(props.columnName));
  const count = parseInt(props.samples.stats.count.get(props.columnName));
  const health = 1.0 - (empty / count);
  const empty_percent = parseFloat(parseFloat((empty/count)*100).toFixed(2));
  const valid = parseInt(count-empty);
  const valid_percent = parseFloat(parseFloat((valid/count)*100).toFixed(2));
  const invalid = 0;
  const invalid_percent = 0;
  return (
    <div className={`${theme.flexContainer} ${theme.col} ${theme.leftPanel}`}>
      <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.col} ${theme.padding}`}>
        <div className={theme.header}>
          <h5>{props.columnName}</h5>
          <small>{props.samples.schema.get(props.columnName).replace('Type', '')}</small>
        </div>
      </div>
      <div className={`${theme.flexItem} ${theme.justifyCenter} ${theme.col} ${theme.padding}`}>
        <div className={theme.content}>
          <h5>Quality</h5>
          <div style={{padding: "10px 0px 20px"}}>
            <ColumnHealth health={health} height="1rem"/>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '5px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '15%'}}>
              <div style={{ width: '1rem', height: '1rem', backgroundColor: 'rgb(0, 188, 212)' }}/>
            </div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.row}`} style={{ width: '30%'}}>Valid</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '30%'}}>{valid}</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '30%'}}>{valid_percent}%</div>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '5px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '15%'}}>
              <div style={{ width: '1rem', height: '1rem', backgroundColor: 'red' }}/>
            </div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.row}`} style={{ width: '30%'}}>Invalid</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '30%'}}>{invalid}</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '30%'}}>{invalid_percent}%</div>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '5px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '15%'}}>
              <div style={{ width: '1rem', height: '1rem', backgroundColor: '#7F7F7F' }}/>
            </div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.row}`} style={{ width: '30%'}}>Empty</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '30%'}}>{empty}</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '30%'}}>{empty_percent}%</div>
          </div>
        </div>
      </div>
      <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.col} ${theme.padding}`}>
        <h5>Summary</h5>
        <div style={{padding: '10px 0px 20px'}}>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '5px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.row}`} style={{ width: '50%'}}>Count</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '50%'}}>{props.samples.stats.count.get(props.columnName)}</div>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '5px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.row}`} style={{ width: '50%'}}>Distinct</div>
            <div className={`${theme.flexItem} ${theme.justifyBottom} ${theme.row}`} style={{ width: '50%'}}></div>
          </div>
        </div>
      </div>
    </div>
  );
};

class StatsTabs extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      index: 0
    }
  }
  render() {
    const { samples, columnName } = this.props;
    const model = {
      value: {
        type: 'String',
        title: 'Value'
      },
      count: {
        type: 'Number',
        title: 'Count'
      },
      percent: {
        type: 'String',
        title: 'Percent'
      },
      cum_percent: {
        type: 'String',
        title: 'Cum Percent'
      }
    };
    const count = parseInt(samples.stats.count.get(columnName));
    let cumPercent = 0;
    const rows = samples.topn.get(columnName).map((row) => {
      let percent = parseFloat(parseFloat((row.get('count')/count)*100).toFixed(2));
      cumPercent += percent;
      cumPercent = parseFloat(parseFloat(cumPercent).toFixed(2));
      let rowValue = row.get('value');

      return {
        value: <div>
          <TooltipDiv
            tooltip={rowValue.includes('\n') ? <TooltipText data={rowValue} /> : rowValue}
            theme={theme}
            label={rowValue} />
          <div className={theme.histogramBar} style={{width: percent+'%'}}></div>
        </div>,
        count: row.get('count'),
        percent: percent+'%',
        cum_percent: cumPercent+'%'
      }
    });
    const data = samples.histogram.get(columnName);
    let boxPlotData = null;
    let statistics = <div>Statistics not available for this field.</div>;
    if (data) {
      boxPlotData = [...samples.histogram.get(columnName).values()].map((v,k)=> {return v.get('min')});
      statistics = <div>
        <Column style={{width: '35%'}}>
          <div style={{marginTop: '40px', padding: '15px 0px'}}>
            <h5>Statistics</h5>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '8px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              Count</div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              {count}</div>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '8px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              Min</div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              {samples.stats.min.get(columnName)}</div>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '8px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              Max</div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              {samples.stats.max.get(columnName)}</div>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '8px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              Mean</div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              {parseFloat(parseFloat(samples.stats.mean.get(columnName)).toFixed(2))}</div>
          </div>
          <div className={`${theme.flexContainer} ${theme.row}`} style={{padding: '8px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              Standard Deviation</div>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.row}`} style={{width: '50%'}}>
              {parseFloat(parseFloat(samples.stats.stddev.get(columnName)).toFixed(2))}</div>
          </div>
        </Column>
        <Column style={{width: '65%'}}>
          <div className={`${theme.flexContainer} ${theme.col}`} style={{padding: '5px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.col}`} style={{width: '100%', height: '20vh'}}>
              <ResizableHorizontalBoxPlot data={boxPlotData} name={'box'}/>
            </div>
          </div>
          <div className={`${theme.flexContainer} ${theme.col}`} style={{padding: '5px 0px'}}>
            <div className={`${theme.flexItem} ${theme.justifyTop} ${theme.alignCenter} ${theme.col}`} style={{width: '100%', height: '60vh'}}>
              <Histogram data={data} color={'#0096FF'}/>
            </div>
          </div>
        </Column>
        </div>
    }
    return (
      <div style={{width: '100%'}}>
        <Tabs index={this.state.index} onChange={(index) => { this.setState({ index })}}>
          <Tab label="Pattern">
            <div style={{ height: '80vh', overflow: 'scroll'}}>
              <Table model={model} source={rows.toArray()} selectable={false} theme={theme} />
            </div>
          </Tab>
          <Tab label="Stats">
            {statistics}
          </Tab>
        </Tabs>
      </div>
    )
  }
}
