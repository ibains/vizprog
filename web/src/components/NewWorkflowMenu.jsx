import React from 'react';

import Popover from 'material-ui/Popover';
import { Menu, MenuItem } from 'material-ui/Menu';
import FontIcon from 'material-ui/FontIcon';

import NewWorkflowDialog from './NewWorkflowDialog';

import join from '../mock/join.json';
import simple from '../mock/simple.json';

export default class NewWorkflowMenu extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { open: false };
  }
  _handleClick = (event) => this.setState({ open: true, anchorEl: event.currentTarget });
  render() {
    const { createWorkflow, alert } = this.props;
    const layerIcon = <FontIcon className='material-icons'>{'layers'}</FontIcon>;
    return (
      <div style={{ position: 'absolute', bottom: 0, left: 0, margin: '30px'}}>
        <FontIcon
          onClick={() => this.setState({ open: true })}
          color={ alert ? '#D50000' : '#BDBDBD' }
          style={{ cursor: 'pointer' }}
          className='material-icons'>{'add'}</FontIcon>
        <NewWorkflowDialog open={this.state.open} close={() => this.setState({ open: false })} createWorkflow={createWorkflow} />
      </div>
    );
  }
}
