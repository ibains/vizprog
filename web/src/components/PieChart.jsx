import React from 'react';
import Immutable from 'immutable';
import { ResizablePieChart } from './ResizableGraph';

import Column from './Column';
import Card from './Card';
import AxisSelector from './AxisSelector';

import { ROW_REGEX } from '../utils/data';

export default class PieChart extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.save = this.save.bind(this);
    const columns = this._getColumns(props.id, props.workflow);
    const labelColumnValue = props.workflow.processes.get(props.id).properties.get('label');
    const countColumnValue = props.workflow.processes.get(props.id).properties.get('count');
    const labelColumn = columns.findIndex(col => col == labelColumnValue);
    const countColumn = columns.findIndex(col => col == countColumnValue);
    this.state = {
      labelColumn: labelColumn > -1 ? labelColumn : 0,
      countColumn: countColumn > -1 ? countColumn : 1,
    };
  }
  _getColumns(processId, workflow) {
    const connection = workflow.connections.find(conn => conn.tgt.process == processId);
    return workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'schema']).keySeq();
  }
  _getData(processId, countColumn, labelColumn, workflow) {
    let connection = workflow.connections.find(conn => conn.tgt.process == processId);
    let data = workflow.samples.getIn([connection.src.process + '_' + connection.src.port, 'data']);
    return data.mapEntries(([row, columns]) => [columns.get(labelColumn), columns.get(countColumn)]);
  }
  save() {
    const { labelColumn, countColumn } = this.state;
    const columns = this._getColumns(this.props.id, this.props.workflow);
    this.props.onChange(columns.get(labelColumn), columns.get(countColumn));
  }
  componentWillUpdate(nextProps) {
    const oldValues = this.props.workflow.processes.get(this.props.id).properties.filter((v, k) => ['label', 'count'].includes(k));
    const newValues = nextProps.workflow.processes.get(nextProps.id).properties.filter((v, k) => ['label', 'count'].includes(k));
    if (!Immutable.is(oldValues, newValues)) {
      this.props.close();
    }
  }
  render() {
    const { countColumn, labelColumn } = this.state;
    const { id, workflow } = this.props;
    const columns = this._getColumns(id, workflow);
    const data = this._getData(id, countColumn, labelColumn, workflow);
    return (
      <div>
        <Column>
          <AxisSelector columns={columns} label={'Value'} column={countColumn} select={(countColumn) => this.setState({ countColumn })} />
          <AxisSelector columns={columns} label={'Label'} column={labelColumn} select={(labelColumn) => this.setState({ labelColumn })} />
          <button type="button" onClick={this.save} className="btn">Save Options</button>
        </Column>
        <Column style={{ width: '66.66%' }}>
          <Card highlight={false}>
            <ResizablePieChart data={data} />
          </Card>
        </Column>
      </div>
    );
  }
}
