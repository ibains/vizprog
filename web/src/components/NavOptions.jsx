import React from 'react'
import Drawer from 'material-ui/Drawer'
import { MenuItem } from 'material-ui/Menu'
import { API_Config } from 'constants'

const MENU_ITEM_STYLE = {
  border: 'none',
  color: '#848484',
  fontSize: '14px',
  background: 'none'
};

export default class NavOptions extends React.Component {

  static propTypes = {
    open: React.PropTypes.bool.isRequired,
    onNavigate: React.PropTypes.func.isRequired,
    toggle: React.PropTypes.func.isRequired,
    user: React.PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      showUser: false
    }
  }

  navigate = (path) => {
    this.props.onNavigate(path)
  }

  logout() {
    window.location.href = API_Config.serverUrl + API_Config.signOutPath
  }

  render() {
    const { showUser } = this.state
    const { user, toggle, open } = this.props;
    let content = null;
    if (showUser) {
      content = (
        <div className="app-menu-content">
          <MenuItem className="app-menu-item" style={MENU_ITEM_STYLE}>Settings</MenuItem>
          <MenuItem className="app-menu-item" style={MENU_ITEM_STYLE} onTouchTap={this.logout}>Logout</MenuItem>
        </div>
      )
    } else {
      content = (
        <div className="app-menu-content">
          <MenuItem className="app-menu-item" style={MENU_ITEM_STYLE} onTouchTap={() => this.navigate('/')}><span><i className="material-icons">device_hub</i>Workflow</span></MenuItem>
          <MenuItem className="app-menu-item" style={MENU_ITEM_STYLE} onTouchTap={() => this.navigate('/account/user/u5')}><span><i className="material-icons">search</i>Browse</span></MenuItem>
          <MenuItem className="app-menu-item" style={MENU_ITEM_STYLE} onTouchTap={() => this.navigate('/explore')}><span><i className="material-icons">history</i>Job History</span></MenuItem>
          <MenuItem className="app-menu-item" style={MENU_ITEM_STYLE}><span><i className="material-icons">play_circle_filled</i>Execute</span></MenuItem>
          <MenuItem className="app-menu-item" style={MENU_ITEM_STYLE} onTouchTap={() => this.navigate('/admin')}><span><i className="material-icons">settings</i>Admin</span></MenuItem>
        </div>
      )
    }
    return (
      <Drawer open={open} style={{ overflowY: 'scroll' }} overlayStyle={{backgroundColor: 'transparent'}}
        docked={false} onRequestChange={open => toggle(open)}>
        <NavOptionsHeader onClick={() => this.setState({ showUser: !showUser })} user={user} expanded={showUser} />
        {content}
      </Drawer>
    )
  }
}

const NavOptionsHeader = (props) => {
  const { user, onClick, expanded } = props;
  return (
    <div className='app-menu-header' onClick={onClick}>
      { user.email || 'Anonymous user' }
      <i className='material-icons'> { expanded ? 'arrow_drop_up' : 'arrow_drop_down' }
      </i>
      <i className='material-icons'>{expanded ? 'keyboard_arrow_left' : null}</i>
    </div>
  )
}
