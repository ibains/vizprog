import React from 'react';
import Immutable from 'immutable';
import Plotly from 'react-plotlyjs';
import { Histogram } from './ResizableGraph';
import ColumnHealth from './ColumnHealth';

import colors from 'material-ui/styles/colors';
//import { Table, TableHeader, TableBody, TableRow, TableRowColumn, TableFooter } from 'material-ui/Table';
//import SampleTableColumnLabel from './SampleTableColumnLabel';
import Table from 'react-toolbox/lib/table';
import Tooltip from 'react-toolbox/lib/tooltip';
import Link from 'react-toolbox/lib/link';
import { IconButton } from 'react-toolbox/lib/button'
import StatisticsDialog from './StatisticsDialog'

import theme from './SampleTable.scss';

const TooltipDiv = Tooltip(Link);
const TooltipText = props => <p>{props.data.split('\n').map(string => <span>{string}<br/></span>)}</p>;

export default class SampleTable extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {dialogOpen: false, statsColumn: null, hovered: {}}
  }
  _makeRow(columns, rowData) {
    let row = rowData.toOrderedMap().mapKeys(key => columns.get(key));
    return row.map(data => <TooltipDiv
      tooltip={data.includes('\n') ? <TooltipText data={data} /> : data}
      theme={theme}
      label={data} />).toObject();
  }
  onMouseEnterHandler = (e, name) => {
    var hovered = this.state.hovered;
    hovered[name] = true;
    this.setState({ hovered: hovered });
  };
  onMouseLeaveHandler = (e, name) => {
    var hovered = this.state.hovered;
    hovered[name] = false;
    this.setState({hovered: hovered});
  };
  render() {
    const {dialogOpen, statsColumn, hovered} = this.state;
    const { samples } = this.props;
    const model = samples.schema.map((type, name) => {
      const health = 1.0 - (samples.nulls.get(name) / samples.stats.count.get(name));
      type = type.replace('Type', '');
      const iconButton = (hovered[name])?<IconButton icon='insert_chart' onMouseUp={(e) => {this.setState({dialogOpen: true, statsColumn: name})}}></IconButton>:null;
      return {
        type: type,
        title: <div className={theme.tableHeader} onMouseEnter={(e) => this.onMouseEnterHandler(e, name)}
                    onMouseLeave={(e) => this.onMouseLeaveHandler(e, name)}>
          <div>{name}</div>
          {iconButton}
          <div><small>{type}</small></div>
          <ColumnHealth health={health}/>
        </div>
      }
    }).toObject();
    const columns = samples.schema.keySeq();
    const rows = samples.data.map(this._makeRow.bind(null, columns));
    const statisticsDialog = dialogOpen?<StatisticsDialog close={() => this.setState({dialogOpen: false})} columnName={statsColumn} samples={samples}/>:null;
    return (
      <div>
        <Table model={model} source={rows.toArray()} selectable={false} theme={theme} />
        {statisticsDialog}
      </div>
    );
  }
}
