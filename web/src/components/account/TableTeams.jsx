import React from 'react'
import Table from 'react-toolbox/lib/table';
import { Link } from 'react-router'
import Immutable from 'immutable'
import theme from './Account.scss'

export default class TableTeams extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render () {
    const model = {
      name: {
        type: 'String',
        title: 'Name'
      },
      description: {
        type: 'String',
        title: 'Description'
      }
    };
    let teams = null;
    const {source, breadcrumbs} = this.props;
    let out = <div>No teams found</div>;
    if(source) {
      teams = source.map( (team) => {
        return Immutable.Map(team).merge({
          name: <Link to={{ pathname: `/account/team/${team.id}`, query: {breadcrumbs: JSON.stringify(breadcrumbs)} }} className={theme.link}> {team.name} </Link>,
        }).toJS();
      });
      out = <Table model={model} source={teams} selectable={false} theme={theme}/>
    }
    return out;
  }
}
