import React from 'react'
import Table from 'react-toolbox/lib/table';
import { Link } from 'react-router'
import Immutable from 'immutable'
import theme from './Account.scss'

export default class TableUsers extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render () {
    const model = {
      fullName: {
        type: 'String',
        title: 'Name'
      },
      email: {
        type: 'String',
        title: 'Email'
      }
    };
    let users = null;
    const {source, breadcrumbs} = this.props;
    let out = <div>No users found</div>;
    if(source) {
      users = source.map((user) => {
        return Immutable.Map(user).merge({
          fullName: <Link to={{pathname: `/account/user/${user.id}`, query: {breadcrumbs: JSON.stringify(breadcrumbs)} }} className={theme.link}> {user.fullName} </Link>
        }).toJS();
      });
      out = <Table model={model} source={users} selectable={false} theme={theme}/>
    }
    return out;
  }
}
