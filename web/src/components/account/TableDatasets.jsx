import React from 'react'
import Table from 'react-toolbox/lib/table';
import { Link } from 'react-router'
import theme from './Account.scss'
import Immutable from 'immutable'
var moment = require('moment');

export default class TableDatasets extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render () {
    const model = {
      name: {
        type: 'String',
        title: 'Name'
      },
      description: {
        type: 'String',
        title: 'Description'
      },
      format: {
        type: 'String',
        title: 'Format'
      },
      created: {
        type: 'String',
        title: 'Created'
      },
      lastModified: {
        type: 'String',
        title: 'Last Modified'
      },
      projectName: {
        type: 'String',
        title: 'Project Name'
      }
    };
    let datasets = null;
    const {source, breadcrumbs} = this.props;
    let out = <div>No datasets found</div>;
    if(source) {
      datasets = source.map( (dataset) => {
        return Immutable.Map(dataset).merge({
          name: <Link to={{pathname: `/account/dataset/${dataset.id}`, query: {breadcrumbs: JSON.stringify(breadcrumbs)} }} className={theme.link}> {dataset.name} </Link>,
          created: moment.unix(dataset.created).format("D MMM YYYY hh:mm A"),
          lastModified: moment.unix(dataset.lastModified).format("D MMM YYYY hh:mm A"),
          projectName: dataset.project.name
        }).toJS();
      });
      out = <Table model={model} source={datasets} selectable={false} theme={theme}/>
    }
    return out;
  }
}
