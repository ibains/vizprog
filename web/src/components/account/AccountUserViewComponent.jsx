import React, {Component} from 'react'
import Relay from 'react-relay'
import Breadcrumbs from './Breadcrumbs'
import Column from './../Column'
import {Tab, Tabs} from 'react-toolbox';
import Box from './../Box'
import theme from './Account.scss'
import TableTeams from './TableTeams'
import TableWorkflows from './TableWorkflows'
import TableDatasets from './TableDatasets'
import TableProjects from './TableProjects'
import TableInfo from './TableInfo'
var FontAwesome = require('react-fontawesome');

class AccountUserViewComponent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      index: 0
    }
  }

  render() {
    const defaultIcon = <FontAwesome name="user" style={{fontSize: '12rem', opacity: 0.8}} />;
    const avatar = (this.props.viewer.avatarURL) ? <img src={this.props.viewer.avatarURL} alt={this.props.viewer.fullName} style={{width: '20rem', height: '20rem'}} /> : defaultIcon;
    const { firstName, lastName, company, email, permissions } = this.props.viewer;
    let breadcrumbs = [];
    const queryParam = (this.props.location.search)?this.props.location.search:'';
    if(this.props.location.query.breadcrumbs) {
      try {
        breadcrumbs = JSON.parse(this.props.location.query.breadcrumbs);
      } catch(e) {
        breadcrumbs = [];
      }
    }
    breadcrumbs = breadcrumbs.concat({
      name: this.props.viewer.fullName,
      path: '/account/user/' + this.props.viewer.id + queryParam,
      type: 'user'
    });
    const labelValuePairs = [
      {
        label: "First Name",
        value: firstName
      },
      {
        label: "Last Name",
        value: lastName
      },
      {
        label: "Email",
        value: email
      },
      {
        label: "Company",
        value: company.name
      }
    ];
    return (
      <div className={theme.accountView}>
        <div className={theme.absolute}></div>
        <div className={theme.container}>
        <div>
          {
            <Breadcrumbs
              breadcrumbs={breadcrumbs}
            />
          }
        </div>
        <div className="flexContainer col">
          <div className="flexItem row">
            <Column style={{ width: '50%' }}>
              <Box>
                <div className="flexContainer row">
                  <div className="flexItem col" style={{ flexGrow: '0', alignSelf: 'center', alignContent: 'center', alignItems: 'center', width: '20rem', height: '20rem', marginRight: '2rem' }}>
                    {avatar}
                  </div>
                  <div className="flexItem col">
                    <TableInfo source={labelValuePairs} />
                  </div>
                </div>
              </Box>
            </Column>
            <Column style={{width: '50%'}}/>
            <div style={{clear: 'both'}}></div>
          </div>
          <div className="flexItem row">
            <div style={{width: '100%'}}>
              <Tabs index={this.state.index} onChange={(index) => { this.setState({ index })}}>
                <Tab label="Teams">
                  <TableTeams breadcrumbs={breadcrumbs} source={this.props.viewer.teams} />
                </Tab>
                <Tab label="Projects">
                  <TableProjects breadcrumbs={breadcrumbs} source={this.props.viewer.projects} />
                </Tab>
                <Tab label="Workflows">
                  <TableWorkflows breadcrumbs={breadcrumbs} source={this.props.viewer.workflows} />
                </Tab>
                <Tab label="Datasets">
                  <TableDatasets breadcrumbs={breadcrumbs} source={this.props.viewer.datasets} />
                </Tab>
                <Tab label="XDataSources">
                  XDataSources
                </Tab>
                <Tab label="Models">
                  Models
                </Tab>
              </Tabs>
            </div>
          </div>
        </div>
      </div>
      </div>
        )
  }
}

export default Relay.createContainer(AccountUserViewComponent, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        id
        fullName
        firstName
        lastName
        email
        avatarURL
        permissions 
        company {
          name
        }
        teams {
          id
          name
          description
          permissions
        }
        projects {
          id
          name
          description
          permissions
        }
        workflows {
          id
          name
          description
          permissions
          created
          lastModified
          project {
            name
          }
        }
        datasets {
          id
          name
          description
          location
          created
          format
          permissions
          lastModified
          project {
            name
          }
        }
      }
    `,
  },
});
