import React from 'react'
import Table from 'react-toolbox/lib/table';
import { Link } from 'react-router'
import theme from './Account.scss'
import Immutable from 'immutable'
var moment = require('moment');

export default class TableWorkflows extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render () {
    const model = {
      name: {
        type: 'String',
        title: 'Name'
      },
      description: {
        type: 'String',
        title: 'Description'
      },
      created: {
        type: 'String',
        title: 'Created'
      },
      lastModified: {
        type: 'String',
        title: 'Last Modified'
      },
      projectName: {
        type: 'String',
        title: 'Project Name'
      }
    };
    let workflows = null;
    const {source, breadcrumbs} = this.props;
    let out = <div>No workflows found</div>;
    if(source) {
      workflows = source.map( (workflow) => {
        return Immutable.Map(workflow).merge({
          name: <Link to={{ pathname: `/account/workflow/${workflow.id}`, query: {breadcrumbs: JSON.stringify(breadcrumbs)} }} className={theme.link}> {workflow.name} </Link>,
          created: moment.unix(workflow.created).format("D MMM YYYY hh:mm A"),
          lastModified: moment.unix(workflow.lastModified).format("D MMM YYYY hh:mm A"),
          projectName: workflow.project.name
        }).toJS();
      });
      out = <Table model={model} source={workflows} selectable={false} theme={theme}/>
    }
    return (
      out
    );
  }
}
