import React from 'react'
import Table from 'react-toolbox/lib/table';
import theme from './TableInfo.scss'

export default class TableInfo extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render () {
    const model = {
      label: {
        type: 'String',
        title: 'Label'
      },
      value: {
        type: 'String',
        title: 'Value'
      }
    };

    const {source} = this.props;
    let out = null;
    if(source) {
      out = <Table className={theme.infoTable} heading={false} model={model} source={source} selectable={false}/>
    }
    return out;
  }
}
