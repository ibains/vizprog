import React from 'react'
import Table from 'react-toolbox/lib/table';
import { Link } from 'react-router'
import Immutable from 'immutable'
import theme from './Account.scss'

export default class TableProjects extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render () {
    const model = {
      name: {
        type: 'String',
        title: 'Name'
      },
      description: {
        type: 'String',
        title: 'Description'
      }
    };
    let projects = null;
    const {source, breadcrumbs} = this.props;
    let out = <div>No projects found</div>;
    if(source) {
      projects = source.map( (project) => {
        return Immutable.Map(project).merge({
          name: <Link to={{ pathname: `/account/project/${project.id}`, query: {breadcrumbs: JSON.stringify(breadcrumbs)} }} className={theme.link}> {project.name} </Link>,
        }).toJS();
      });
      out = <Table model={model} source={projects} selectable={false} theme={theme}/>
    }
    return out;
  }
}
