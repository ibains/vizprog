import React from 'react'
import { Link } from 'react-router'
import theme from './Account.scss'
import FontIcon from 'material-ui/FontIcon';
var FontAwesome = require('react-fontawesome');

export default class Breadcrumbs extends React.Component {
  constructor(props, context) {
    super(props, context);
  }

  render () {
    const {breadcrumbs} = this.props;
    let breadcrumbDisplay = null;
    if(breadcrumbs) {
      breadcrumbDisplay =
        <ul className={theme.breadcrumbsList}>
          {breadcrumbs.map( (breadcrumb, i) => {
            let icon = null;
            switch(breadcrumb.type) {
              case "dataset":
                icon = <FontAwesome className={theme.fa} name="database" style={{fontSize: '14px'}} />;
                break;
              case "project":
                icon = <FontAwesome className={theme.fa} name="flask" style={{fontSize: '14px'}} />;
                break;
              case "team":
                icon = <FontAwesome className={theme.fa} name="users" style={{fontSize: '14px'}} />;
                break;
              case "user":
                icon = <FontAwesome className={theme.fa} name="user" style={{fontSize: '14px'}} />;
                break;
              case "workflow":
                icon = <FontIcon className="material-icons" style={{fontSize: "16px", marginRight: '5px', top: '3px', color:'#ffffff'}}>device_hub</FontIcon>;
                break;
              default:
                icon = <FontAwesome className={theme.fa} name="question" style={{fontSize: '14px'}} />;
            }

            return (
              <li key={i}>
                <Link
                  onlyActiveOnIndex={true}
                  activeClassName={theme.breadcrumbActive}
                  to={breadcrumb.path || ''}>
                  {icon}{breadcrumb.name || ''}
                </Link>
              </li>
            );
            }
          )}
        </ul>;
    }
    return breadcrumbDisplay;
  }
}
