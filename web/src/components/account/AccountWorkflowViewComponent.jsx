import React, {Component} from 'react'
import Relay from 'react-relay'
import Breadcrumbs from './Breadcrumbs'
import Column from './../Column'
import {Tab, Tabs} from 'react-toolbox';
import Box from './../Box'
import theme from './Account.scss'
import TableInfo from './TableInfo'
import TableUsers from './TableUsers'
import TableTeams from './TableTeams'
import TableDatasets from './TableDatasets'
var moment = require('moment');
import FontIcon from 'material-ui/FontIcon';

class AccountWorkflowViewComponent extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      index: 0
    }
  }

  render() {
    const avatar = <FontIcon className="material-icons" style={{fontSize: '12rem', color: '#0297E6', opacity: 0.6}}>device_hub</FontIcon>;
    const {name, description, project, created, lastModified, permissions} = this.props.viewer;
    let breadcrumbs = [];
    const queryParam = (this.props.location.search)?this.props.location.search:'';
    if(this.props.location.query.breadcrumbs) {
      try {
        breadcrumbs = JSON.parse(this.props.location.query.breadcrumbs);
      } catch(e) {
        breadcrumbs = [];
      }
    }
    breadcrumbs = breadcrumbs.concat({
      name: this.props.viewer.name,
      path: '/account/workflow/' + this.props.viewer.id + queryParam,
      type: 'workflow'
    });

    const labelValuePairs = [
      {
        label: "Workflow",
        value: name
      },
      {
        label: "Description",
        value: description
      },
      {
        label: "Project Name",
        value: project.name
      },
      {
        label: "Created",
        value: moment.unix(created).format("D MMM YYYY hh:mm A")
      },
      {
        label: "Last Modified",
        value: moment.unix(lastModified).format("D MMM YYYY hh:mm A")
      }
    ];
    return (
      <div className={theme.accountView}>
        <div className={theme.absolute}></div>
        <div className={theme.container}>
        <div>
          {
            <Breadcrumbs
              breadcrumbs={breadcrumbs}
            />
          }
        </div>
        <div className="flexContainer col">
          <div className="flexItem row">
            <Column style={{ width: '50%' }}>
        <Box>
          <div className="flexContainer row">
            <div className="flexItem col" style={{ flexGrow: '0', alignSelf: 'center', alignContent: 'center', alignItems: 'center', width: '20rem', height: '20rem', marginRight: '2rem' }}>
              {avatar}
            </div>
            <div className="flexItem col">
              <TableInfo source={labelValuePairs} />
            </div>
          </div>
        </Box>
      </Column>
            <Column style={{width: '50%'}}/>
            <div style={{clear: 'both'}}></div>
          </div>
          <div className="flexItem row">
            <div style={{width: '100%'}}>
              <Tabs index={this.state.index} onChange={(index) => { this.setState({ index })}}>
                <Tab label="Users">
                  <TableUsers breadcrumbs={breadcrumbs} source={this.props.viewer.users} />
                </Tab>
                <Tab label="Teams">
                  <TableTeams breadcrumbs={breadcrumbs} source={this.props.viewer.teams} />
                </Tab>
                <Tab label="Datasets">
                  <TableDatasets breadcrumbs={breadcrumbs} source={this.props.viewer.datasets} />
                </Tab>
                <Tab label="XDataSources">
                  XDataSources
                </Tab>
                <Tab label="Models">
                  Models
                </Tab>
              </Tabs>
            </div>
          </div>
        </div>
      </div>
      </div>
        )
  }
}

export default Relay.createContainer(AccountWorkflowViewComponent, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on Workflow {
        id
        name
        description
        permissions 
        created
        lastModified
        project {
          name
        }
        users {
          id
          fullName
          email
          permissions
        }
        teams {
          id
          name
          description
          permissions
        }
        datasets {
          id
          name
          description
          location
          created
          format
          permissions
          lastModified
          project {
            name
          }
        }
      }
    `,
  },
});
