import React from 'react'
import ReactDOM from 'react-dom'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Icon from 'react-fa'
import { getNodeRenderProps, getCategoryRenderProps } from 'utils/WorkflowUtil'
import { theme } from '../constants/theme'
import { componentDefinition } from './mock/componentDefinition'

export class WorkflowNode extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    x: React.PropTypes.number,
    y: React.PropTypes.number,
    text: React.PropTypes.string,
    label: React.PropTypes.string,
    isSelected: React.PropTypes.bool.isRequired,
    type: React.PropTypes.string.isRequired,
    category: React.PropTypes.string.isRequired,
    onClick: React.PropTypes.func.isRequired,
    onEditNodeLabel: React.PropTypes.func.isRequired,
    onRemoveNode: React.PropTypes.func.isRequired
  }

  componentWillMount() {
    this.setState({
      label: this.props.label
    });
  }

  onClick = (event) => {
    if (this.props.isDragged) {
      // this click was induced by end drag
      // do not fire onClick but notify Workflow's onDragDone method to update the position of this node
      const domNode = ReactDOM.findDOMNode(this)
      this.props.onDragDone(domNode.offsetLeft, domNode.offsetTop)
      return
    }
    this.props.onClick(event)
  }

  onRemoveNode = (e) => {
    this.props.onRemoveNode(this.props.id)
    e.stopPropagation()
  }

  onLabelClick = (e) => {
    e.stopPropagation();
    this.setState({
      label: this.props.label,
      inEditMode: true
    });
  }

  onLabelEditorKeyDown = (e) => {
    if (e.keyCode == 13) {
      e.stopPropagation();

      this.setState({
        inEditMode: false
      });

      if (this.props.onEditNodeLabel) {
        this.props.onEditNodeLabel(this.state.label);
      }
    }
    else if (e.keyCode == 27) {
      e.stopPropagation();

      this.setState({
        label: this.props.label,
        inEditMode: false
      });
    }
  }

  onLabelEditorChange = (e) => {
    this.setState({
      label: e.target.value
    });
  }

  render() {

    // const { iconName, color } = getNodeRenderProps(this.props.type)
    const { backgroundImage, color } = getCategoryRenderProps(this.props['category'])

    const nodeStyle = {
      textAlign: 'center',
      position: 'absolute',
      left: this.props.x,
      top: this.props.y,
      width: theme.node.width,
      height: theme.node.height
    }

    const boxStyle = {
      position: 'relative',
      margin: '0 auto',
      width: this.props.width || theme.node.width,
      height: this.props.height || theme.node.height,
      border: "1px solid #CCCCCC",
      borderRadius: "2px",
      cursor: "pointer",
      //backgroundColor: (this.props.isSelected) ? color : "#ffffff"
      background: `white ${backgroundImage} no-repeat center center`
    }

    /*
     const deleteStyle = {
     position: "absolute",
     top: -6,
     right: -6
     }

     const circleStyle = {
     fill: "#ffffff",
     stroke: "#4586f3",
     strokeWidth: 3,
     cursor: "pointer",
     cx: 10,
     cy: 10,
     r: 8
     }
     */

    const xStyle = {
      position: "absolute",
      top: 3,
      right: 4,
      color: color,
      cursor: "pointer"
    }

    const componentLabelStyle = {
      position: "absolute",
      marginTop: '7px',
      marginLeft: "auto",
      marginRight: "auto",
      left: 0,
      right: 0,
      color: this.props.isSelected ? '#ffffff' : color,
      textTransform: 'uppercase'
    }

    const labelStyle = {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      marginTop: '20px'
    }

    const iconStyle = {
      marginTop: "4px",
      color: this.props.isSelected ? '#ffffff' : color
    }

    let componentDef = componentDefinition[this.props.type] || [];
    const componentLabel = componentDef['label'];
    const componentCategory = componentDef['category'];
    return (
      <div style={nodeStyle} className={"node-box workflow-node " + componentCategory} id={this.props.id}>
        <div className="node" style={boxStyle} onClick={this.onClick}>
          {/*
           <svg height="20" width="20" style={deleteStyle}>
           <circle style={circleStyle}
           onClick={() => this.handleClick(this.props.id) }></circle>
           <Icon name="close" style={xStyle}
           onClick={() => this.handleClick(this.props.id) }></Icon>
           </svg>

          <Icon name={iconName} style={iconStyle} />
          <Icon name="close" style={xStyle}
                onClick={this.onRemoveNode}></Icon>
          */}
        </div>

        <h3 className="node-title">{componentLabel}</h3>

        {(() => {
          if (this.state.inEditMode) {
            return (
              <div className="node-subtitle-editor">
                <input type="text" value={this.state.label} onChange={this.onLabelEditorChange.bind(this)} onKeyDown={this.onLabelEditorKeyDown.bind(this)} />
              </div>
            )
          }
          else {
            return (
              <span className="node-subtitle" onClick={this.onLabelClick.bind(this)}>{this.props.label}</span>
            )
          }
        })()}
      </div>
    )
  }
}
