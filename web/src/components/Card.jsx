import React from 'react';

import {Card as MaterialUICard, CardHeader, CardText} from 'material-ui/Card';

const DEFAULT_STYLE = {
  border: '1px solid #CCC',
  boxShadow: 'none',
  height: '100%',
  marginBottom: '20px',
}

const HEADER_STYLE = {
  paddingTop: '24px',
  paddingLeft: '24px',
  paddingRight: '24px',
  paddingBottom: '24px',
  height: '60px',
  fontSize: '18px',
  fontFamily: "'Source Sans Pro', sans-serif",
  textTransform: 'uppercase',
};

export default class Card extends React.Component {

  render() {

    const props = this.props

    const style = Object.assign({}, DEFAULT_STYLE, props.style || null)

    const { highlightColor, textStyle, ...cardProps } = props

    if (props.highlightColor) {
      style.borderLeft = '4px solid ' + props.highlightColor;
    }

  return (
      <MaterialUICard {...cardProps} style={style}>
        { props.title ? <CardHeader
          title={props.title}
          subtitle={props.subtitle} style={HEADER_STYLE}
          actAsExpander={props.expandable}
          showExpandableButton={props.expandable}/> : null }
        <CardText expandable={props.expandable} style={props.textStyle}>
          { props.children }
        </CardText>
      </MaterialUICard>
    )
  }
}
