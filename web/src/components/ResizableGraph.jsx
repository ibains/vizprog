import React  from 'react';
import Plotly from 'plotly.js';

import { CONFIG, DEFAULT_LAYOUT } from '../constants/plotly';
import COLORS from '../utils/colors';
import { sortAdjacent } from '../utils/data';

export default class ResizableGraph extends React.Component {
  static propTypes = {
    data: React.PropTypes.array,
    layout: React.PropTypes.object,
    config: React.PropTypes.object,
    onClick: React.PropTypes.func,
    onBeforeHover: React.PropTypes.func,
    onHover: React.PropTypes.func,
    onUnHover: React.PropTypes.func,
    onSelected: React.PropTypes.func,
  };

  shouldComponentUpdate(nextProps) {
    //TODO logic for detecting change in props
    return true;
  }

  componentDidMount() {
    let {data, layout, config} = this.props;
    let { height, width } = this.props;
    let gd3 = Plotly.d3.select(this.container)
      .style({ width: '100%',
        'margin-left': 0,
        height: '100%',
        'margin-top': 0,
      });
    this.node = gd3.node();
    Plotly.plot(this.node, data, layout, config);
    if (this.props.onClick) { this.container.on('plotly_click', this.props.onClick); }
    if (this.props.onBeforeHover) { this.container.on('plotly_beforehover', this.props.onBeforeHover); }
    if (this.props.onHover) { this.container.on('plotly_hover', this.props.onHover); }
    if (this.props.onUnHover) { this.container.on('plotly_unhover', this.props.onUnHover); }
    if (this.props.onSelected) { this.container.on('plotly_selected', this.props.onSelected); }
    window.addEventListener('resize', this.handleResize);
  };

  componentDidUpdate() {
    Plotly.newPlot(this.node, this.props.data, this.props.layout);
  };

  handleResize = () => { Plotly.Plots.resize(this.node); };

  componentWillUnmount() {
    //Remove some cruft left behind by plotly
    var cruft = document.getElementById("js-plotly-tester");
    if(cruft && cruft.hasOwnProperty('parentNode')) {
      cruft.parentNode.removeChild(cruft);
    }

    this.container.removeAllListeners('plotly_click');
    this.container.removeAllListeners('plotly_beforehover');
    this.container.removeAllListeners('plotly_hover');
    this.container.removeAllListeners('plotly_unhover');
    this.container.removeAllListeners('plotly_selected');
    window.removeEventListener('resize', this.handleResize);
  };

  render() {
    let {data, layout, config, ...other } = this.props;
    return (
      <div {...other} ref={(node) => this.container=node} />
    );
  };
}

// Just a thin wrapper around plotly for ease of use and consistency of styling.
export const ResizablePieChart = (props) => {
  const data = [{
    type: 'pie',
    labels: [...props.data.keys()],
    values: [...props.data.values()],
    marker: { colors: COLORS },
  }];
  return <ResizableGraph data={data} layout={Object.assign({}, DEFAULT_LAYOUT, props.layout)} config={CONFIG} />
}

// Just a thin wrapper around plotly for ease of use and consistency of styling.
export const ResizableBoxPlot = (props) => {
    const data = [{
        type: 'box',
        name: props.name,
        y: [...props.data.values()].map(v => Number(v)),
        marker: { color: props.color || COLORS[0] },
        boxpoints: 'all', // add the points that make up the data
        jitter: 0.3,
        pointpos: -1.8
    }];
    const layout = {
      margin: { l: 70, r: 30, b: 0, t: 10, pad: 4 }
    };
    return <ResizableGraph data={data} layout={Object.assign({}, DEFAULT_LAYOUT, layout, props.layout)} config={CONFIG} />
}

export const ResizableHorizontalBoxPlot = (props) => {
  const data = [{
    type: 'box',
    name: props.name,
    x: props.data.map(v => Number(v)),
    marker: { color: props.color || COLORS[0] },
    boxpoints: false, // add the points that make up the data
    jitter: 0.3,
    pointpos: -1.8
  }];
  const layout = {
    margin: { l: 50, r: 20, b: 20, t: 10, pad: 4 },
    xaxis: { zeroline: false, showticklabels: false, showaxeslabels: false, showgrid: false },
    yaxis: { zeroline: false, showticklabels: false, showaxeslabels: false, showgrid: false }
  };
  return <ResizableGraph data={data} layout={Object.assign({}, DEFAULT_LAYOUT, layout, props.layout)} config={CONFIG} />
}

export const ResizableLineChart = (props) => {
    //const { x, y } = sortAdjacent([...props.data.keys()], [...props.data.values()], (v) => Number(v));
    const x = [...props.data.keys()];
    const y = [...props.data.values()];
    const data = [{ type: 'scatter', marker: { color: props.color || COLORS[0] }, x, y }];
    const layout = { xaxis: { title: props.xAxis }, yaxis: { title: props.yAxis } };
    return <ResizableGraph data={data} layout={Object.assign({}, DEFAULT_LAYOUT, layout, props.layout)} config={CONFIG} />
}

export const ResizableBarChart = (props) => {
    //const { x, y } = sortAdjacent([...props.data.keys()], [...props.data.values()], (v) => Number(v));
    const x = [...props.data.keys()];
    const y = [...props.data.values()];
    const data = [{ type: 'bar', marker: { color: props.color || COLORS[0] }, x, y }];
    const layout = { xaxis: { title: props.xAxis }, yaxis: { title: props.yAxis } };
    return <ResizableGraph data={data} layout={Object.assign({}, DEFAULT_LAYOUT, layout, props.layout)} config={CONFIG} />
}

// Just a thin wrapper around plotly for ease of use and consistency of styling.
export const ResizableScatterPlot = (props) => {
    //const { x, y } = sortAdjacent([...props.data.keys()], [...props.data.values()], (v) => Number(v));
    const x = [...props.data.keys()];
    const y = [...props.data.values()];
    const data = [{ type: 'scatter', mode: 'markers', marker: { color: props.color || COLORS[0] }, x, y }];
    const layout = { xaxis: { title: props.xAxis }, yaxis: { title: props.yAxis } };
    return <ResizableGraph data={data} layout={Object.assign({}, DEFAULT_LAYOUT, layout, props.layout)} config={CONFIG} />
}

const HISTOGRAM_LAYOUT = {
  bargap: 0.1,
  margin: { b: 70, l: 70, r: 30, t: 20, },
  showlegend: false,
  xaxis: { zeroline: true, showticklabels: true, showaxeslabels: true, showgrid: false, },
  yaxis: { zeroline: true, showticklabels: true, showaxeslabels: true, showgrid: false, },
};

const HISTOGRAM_CONFIG = { displayModeBar: false, staticPlot: true, };

export const Histogram = (props) => {
  const data = [{
    type: 'bar',
    x: props.data ? props.data.map(val => val.get('min')).toArray() : [],
    y: props.data ? props.data.map(val => val.get('count')).toArray() : [],
    marker: { color: props.color || 'rgb(0, 188, 212)' },
  }];
  return <ResizableGraph data={data} layout={Object.assign({}, DEFAULT_LAYOUT, HISTOGRAM_LAYOUT, props.layout)} config={CONFIG} />
};

