import React from 'react';

import Table as MaterialUITable from 'material-ui/Table';

const DEFAULT_PROPS = {
  fixedHeader: true,
  //fixedFooter: true,
  //selectable: false,
  //multiSelectable: true,
  //enableSelectAll: false,
  //height: '100%',
  autoDetectWindowHeight: false,
  enableSelectAll: false,
};

const Table = (props) => (
  <MaterialUITable {...Object.assign({}, DEFAULT_PROPS, props)}>
    {props.children}
  </MaterialUITable>
);

export default Table;
