import React from 'react';

import { TableBody as MaterialUITableBody } from 'material-ui/Table'

const DEFAULT_PROPS = {
  deselectOnClickaway: true,
  showRowHover: false,
  stripedRows: false,
  displayRowCheckbox: false,
};

const TableBody = (props) => (
  <MaterialUITableBody {...Object.assign({}, DEFAULT_PROPS, props)}>
    { props.children }
  </MaterialUITableBody>
);

export default TableBody;
