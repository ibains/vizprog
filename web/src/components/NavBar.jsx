import React from 'react'

import AppBar from 'material-ui/AppBar'

const STYLE = {
  display: 'block',
  backgroundColor: '#333333',
  opacity: 0.95,
  position: 'fixed',
  right: 0,
  top: 0,
}

const NavBar = (props) => (
  <AppBar className="app-bar" onLeftIconButtonTouchTap={props.toggle} style={Object.assign({}, STYLE, { height: props.height, minHeight: props.height })}>
    <a className="logo">
      <img src="/assets/images/logo-icon.svg" alt="VizProg" title="VizProg" />
      {'VizProg'}
    </a>
  </AppBar>
);

export default NavBar;
