import React from 'react'
import shouldPureComponentUpdate from 'react-pure-render/function'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'

export class ModalAlert extends React.Component {

  shouldComponentUpdate = shouldPureComponentUpdate

  static propTypes = {
    isShown: React.PropTypes.bool.isRequired,
    title: React.PropTypes.string.isRequired,
    content: React.PropTypes.string.isRequired,
    format: React.PropTypes.string.isRequired,
    onClose: React.PropTypes.func.isRequired
  }

  componentDidMount() {
  }

  render() {

    const content = this.props.format == 'html' ?
      <div dangerouslySetInnerHTML={
        {
          __html: this.props.content
        }
      } /> : this.props.content

    const actions = [
      <FlatButton
        label="Close"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.props.onClose}
      />
    ]

    return (
      <Dialog
        title={this.props.title}
        actions={actions}
        modal={true}
        contentStyle={{
          width: '80%',
          maxWidth: 'none'
        }}
        autoScrollBodyContent={true}
        open={this.props.isShown}
        onRequestClose={this.props.onClose}
      >
        <code style={{
          fontSize: 12
        }}>
          {content}
        </code>
      </Dialog>
    )
  }
}
