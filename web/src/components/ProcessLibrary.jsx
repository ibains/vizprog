import React from 'react'

import ProcessCategoryTab from './ProcessCategoryTab';

import { CATEGORY_DEFINITIONS } from '../constants/category';

export default class ProcessLibrary extends React.Component {
  render() {
    const { addProcess } = this.props;
    return (
      <div className="subheader">
        <ul>
          { CATEGORY_DEFINITIONS.map((details, category) => <ProcessCategoryTab category={category} details={details} addProcess={addProcess} />) }
        </ul>
      </div>
    )
  }
}
