import React from 'react'
import ReactDOM from 'react-dom'
import createBrowserHistory from 'history/lib/createBrowserHistory'
import { syncHistoryWithStore } from 'react-router-redux'
import getRoutes from './routes'
import Root from './containers/Root'
import configureStore from './store/configureStore'
import injectTapEventPlugin from 'react-tap-event-plugin'
import { browserHistory } from 'react-router'

const store = configureStore(window.__INITIAL_STATE__)

const history = syncHistoryWithStore(browserHistory, store)

// syncReduxAndRouter(history, store, (state) => state.router)

//Needed for onTouchTap
//Can go away when react 1.0 release
//Check this repo:
//https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

// Render the React application to the DOM
ReactDOM.render(
  <Root routes={getRoutes(history, store)} store={store} />,
  document.getElementById('root')
)
