import thunkMiddleware from 'redux-thunk'
import promiseMiddleware from '../middlewares/promiseMiddleware'
import rootReducer from '../reducers'
import { routerMiddleware } from 'react-router-redux';
import createLogger from 'redux-logger';
import { browserHistory } from 'react-router';
import { persistState } from 'redux-devtools';

import { applyMiddleware, compose, createStore, combineReducers } from 'redux';
import * as reducers from '../reducers';

const finalCreateStore = compose(
    applyMiddleware(routerMiddleware(browserHistory), thunkMiddleware, promiseMiddleware),
)(createStore);

export default function configureStore(initialState) {
    //const reducer = combineReducers(reducers);
    return finalCreateStore(rootReducer, initialState);
};
