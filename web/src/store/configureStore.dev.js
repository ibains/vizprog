import thunkMiddleware from 'redux-thunk'
import promiseMiddleware from '../middlewares/promiseMiddleware'
import rootReducer from '../reducers'
import DevTools from '../containers/DevTools';
import Immutable from 'immutable';
import { routerMiddleware } from 'react-router-redux';
import createLogger from 'redux-logger';
import { browserHistory } from 'react-router';
import { persistState } from 'redux-devtools';

import { applyMiddleware, compose, createStore } from 'redux';

const loggerMiddleware = createLogger({
    stateTransformer: (state) => {
        var newState = {};
        for (var i of Object.keys(state)) {
            if (Immutable.Iterable.isIterable(state[i])) {
                newState[i] = state[i].toJS();
            } else {
                newState[i] = state[i];
            }
        };
        return newState;
    }
});

function getDebugSessionKey() {
    // By default we try to read the key from ?debug_session=<key> in the address bar
    const matches = window.location.href.match(/[?&]debug_session=([^&]+)\b/);
    return (matches && matches.length > 0)? matches[1] : null;
}

const finalCreateStore = compose(
    applyMiddleware(routerMiddleware(browserHistory), thunkMiddleware, promiseMiddleware, loggerMiddleware),
    DevTools.instrument({ maxAge: 30 }),
    persistState(getDebugSessionKey()),
)(createStore);

export default function configureStore(initialState) {
    //const reducer = combineReducers(reducers);
    const store = finalCreateStore(rootReducer, initialState);
    if (module.hot) {
      // the hot swap of the reducers needs to be done explicitly
      const nextRootReducer = require('../reducers').default
      store.replaceReducer(nextRootReducer)
    }
    return store;
};
