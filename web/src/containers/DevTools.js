import React from 'react'
import { createDevTools } from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor';
import DockMonitor from 'redux-devtools-dock-monitor';

export default createDevTools(
  <DockMonitor
    defaultIsVisible={false}
    toggleVisibilityKey='ctrl-h'
    changePositionKey='ctrl-q'>
    <LogMonitor />
  </DockMonitor>
)

//const DevTools = createDevTools(
    //// Monitors are individually adjustable with props.
    //// Consult their repositories to learn about those props.
    //// Here, we put LogMonitor inside a DockMonitor.
    //<DockMonitor toggleVisibilityKey='ctrl-h' changePositionKey='ctrl-q'>
        //<LogMonitor theme='solarized' select={state => state} expandStateRoot={false} />
    //</DockMonitor>
//);

//export default DevTools;
