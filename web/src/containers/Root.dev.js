import React, { Component } from 'react';
import { Provider } from 'react-redux';
import DevTools from './DevTools';

export default class Root extends React.Component {
  static propTypes = {
    routes: React.PropTypes.element.isRequired,
    store: React.PropTypes.object.isRequired
  }

  render () {
    return (
      <Provider store={this.props.store}>
        <div style={{ height: '100%' }}>
          {this.props.routes}
          <DevTools />
        </div>
      </Provider>
    )
  }
}
