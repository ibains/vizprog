import Immutable from 'immutable'
import { ProcessActions } from '../constants/actions';

const initialState = null;

export default function processID(processID = initialState, action) {
  switch (action.type) {
    case ProcessActions.SELECT_PROCESS:
      return action.payload.process.id;
      break;
    default:
      return processID;
      break;
  }
}
