import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import workflowID from './workflowID';
import processID from './processID';
import sampleID from './sampleID';
import workflows from './workflows';
import templates from './templates';
import projects from './projects';
import user from './user';
import functionDefinitions from './functionDefinitions';
import nodeSpecs from './nodeSpecs';

export default combineReducers({
	user,
  workflows,
  templates,
  projects,
  workflowID,
  processID,
  sampleID,
  functionDefinitions,
  nodeSpecs,
  routing: routerReducer
})
