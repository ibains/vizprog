import Immutable from 'immutable'

import ActionConstants from '../constants/actions';
import { FAKE_WORKFLOW } from '../mock/workflow';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';
import _ from 'lodash';

import { createWorkflow } from '../constants/records';

const initialState = Immutable.Map({});

export default function project(projects = initialState, action) {
  switch (action.type) {
    case ActionConstants.GET_PROJECTS:
      return (action.status == SUCCESS) ? projects.merge(action.payload.map(p => [p.id, p])) : projects;
      break;
    default:
      return projects;
      break;
  }
}
