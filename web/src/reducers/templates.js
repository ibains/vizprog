import Immutable from 'immutable'

import ActionConstants from '../constants/actions';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';
import * as utils from '../utils/records';

const initialState = Immutable.Map({});

export default function templates(templates = initialState, action) {
  switch (action.type) {
    case ActionConstants.GET_WORKFLOWS:
      return (action.status == SUCCESS) ? utils.addTemplatesFromAction(templates, action) : templates;
    default:
      return templates;
  }
}
