import Immutable from 'immutable'

import ActionConstants from '../constants/actions';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';
import * as utils from '../utils/records';

const initialState = Immutable.List();

export default function specs(specs = initialState, action) {
  switch (action.type) {
    case ActionConstants.GET_NODE_SPECIFICATIONS:
      return (action.status == SUCCESS) ? utils.getNodeSpecsFromAction(specs, action) : specs;
    default:
      return specs;
  }
}
