import Immutable from 'immutable'
import { WorkflowActions } from '../constants/actions';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';

const initialState = null;

export default function workflowID(workflowID = initialState, action) {
  switch (action.type) {
    case WorkflowActions.SELECT_WORKFLOW:
      return action.payload;
      break;
    case WorkflowActions.CREATE_WORKFLOW:
      return (action.status == SUCCESS) ? action.payload.id : workflowID;
      break;
    case WorkflowActions.REMOVE_WORKFLOW:
      if (action.status == SUCCESS && action.context.id == workflowID) {
        return initialState;
      } else {
        return workflowID;
      }
    default:
      return workflowID;
      break;
  }
}
