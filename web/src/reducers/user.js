import Immutable from 'immutable'
import { UserActions } from '../constants/actions';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';
import { UserRecord } from '../constants/records';

let initialState = new UserRecord({});

export default function user(user = initialState, action) {
  switch (action.type) {
    case UserActions.GET_USER:
      if (action.status == SUCCESS) {
        return new UserRecord(action.payload);
      } else if (action.status == UNAUTHORIZED) {
        return initialState;
      } else if (action.status == PENDING) {
        return user.set('isFetching', true);
      } else {
        return user;
      }
      break;
    default:
      return (action.status == UNAUTHORIZED) ? initialState : user;
      break;
  }
}
