import Immutable from 'immutable'

import ActionConstants from '../constants/actions';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';
import * as utils from '../utils/records';

const initialState = Immutable.List();

export default function functions(functions = initialState, action) {
  switch (action.type) {
    case ActionConstants.GET_FUNCTION_DEFINITIONS:
      return (action.status == SUCCESS) ? utils.getFunctionSpecsFromAction(functions, action) : functions;
    default:
      return functions;
  }
}
