import Immutable from 'immutable'
import { SampleActions } from '../constants/actions';

const initialState = null;

export default function sampleID(sampleID = initialState, action) {
  switch (action.type) {
    case SampleActions.SELECT_SAMPLE:
      return action.payload.sample.id;
      break;
    default:
      return sampleID;
      break;
  }
}
