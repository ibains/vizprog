import Immutable from 'immutable';

import ActionConstants from '../constants/actions';
import { FAKE_WORKFLOW } from '../mock/workflow';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';
import * as utils from '../utils/records';

const initialState = Immutable.Map({});

export default function workflows(workflows = initialState, action) {
  switch (action.type) {
    case ActionConstants.CREATE_WORKFLOW:
      return (action.status == SUCCESS) ? utils.addWorkflowFromAction(workflows, action) : workflows;
    case ActionConstants.AUTO_LAYOUT:
      return (action.status == SUCCESS) ? utils.updateWorkflowFromAction(workflows, action) : workflows;
    case ActionConstants.GET_WORKFLOWS:
      return (action.status == SUCCESS) ? utils.addWorkflowsFromAction(workflows, action) : workflows;
    case ActionConstants.REMOVE_WORKFLOW:
      return (action.status == SUCCESS) ? workflows.remove(action.context.id) : workflows;
    case ActionConstants.ADD_PROCESS:
      return (action.status == SUCCESS) ? utils.addProcessFromAction(workflows, action) : workflows;
    case ActionConstants.REMOVE_PROCESS:
      return (action.status == SUCCESS) ? utils.removeProcessFromAction(workflows, action) : workflows;
    case ActionConstants.UPDATE_PROCESS:
      return (action.status == SUCCESS) ? utils.updateProcessFromAction(workflows, action) : workflows;
    case ActionConstants.CREATE_CONNECTION:
      return (action.status == SUCCESS) ? utils.addConnectionFromAction(workflows, action) : workflows;
    case ActionConstants.REMOVE_CONNECTION:
      return (action.status == SUCCESS) ? utils.removeConnectionFromAction(workflows, action) : workflows;
    case ActionConstants.RUN_WORKFLOW:
      // TODO: remove this temporary demo workaround
      debugger
      if (action.status != SUCCESS) {
        workflows = workflows.setIn([action.context.id, 'isRunning'], true)
      } else {
        const workflow = workflows.get(action.context.id)
        if (workflow.content.graph.metainfo.mode == 'deploy') {
          workflows = workflows.setIn([action.context.id, 'deployUrl'], 'http://master.vizprog.com:12022/model')
          workflows = workflows.setIn([action.context.id, 'isRunning'], false)
        } else {
          workflows = utils.addSamplesFromAction(workflows, action)
        }
      }
      return workflows
    case ActionConstants.SHIFT_WORKFLOW:
      return (action.status == SUCCESS) ? utils.shiftWorkflowFromAction(workflows, action) : workflows;
    case ActionConstants.SET_PROCESS_JOIN:
      return (action.status == SUCCESS) ? utils.addProcessJoinFromAction(workflows, action) : workflows;
    case ActionConstants.SET_PROCESS_FILTER:
      return (action.status == SUCCESS) ? utils.addProcessFilterFromAction(workflows, action) : workflows;
    case ActionConstants.SET_PIE_CHART_COLUMNS:
      return (action.status == SUCCESS) ? utils.addPieChartColumnsFromAction(workflows, action) : workflows;
    case ActionConstants.SET_LINE_CHART_COLUMNS:
      return (action.status == SUCCESS) ? utils.addLineChartColumnsFromAction(workflows, action) : workflows;
    case ActionConstants.SET_BAR_CHART_COLUMNS:
      return (action.status == SUCCESS) ? utils.addBarChartColumnsFromAction(workflows, action) : workflows;
    case ActionConstants.SET_BOX_PLOT_COLUMN:
      return (action.status == SUCCESS) ? utils.addBoxPlotColumnFromAction(workflows, action) : workflows;
    case ActionConstants.SET_SCATTER_PLOT_COLUMNS:
      return (action.status == SUCCESS) ? utils.addScatterPlotColumnsFromAction(workflows, action) : workflows;
    case ActionConstants.SET_VISUALIZE_COLUMNS:
      return (action.status == SUCCESS) ? utils.addVisualizeColumnsFromAction(workflows, action) : workflows;
    case ActionConstants.INSERT_CLEANSE_PROCESS:
      return (action.status == SUCCESS) ? utils.insertCleanseProcessFromAction(workflows, action) : workflows;
    case ActionConstants.ADD_CLEANSE_FUNCTION:
      return (action.status == SUCCESS) ? utils.addCleanseFunctionFromAction(workflows, action) : workflows;
    case ActionConstants.REMOVE_CLEANSE_FUNCTION:
      return (action.status == SUCCESS) ? utils.removeCleanseFunctionFromAction(workflows, action) : workflows;
    case ActionConstants.REORDER_CLEANSE_FUNCTIONS:
      return (action.status == SUCCESS) ? utils.reorderCleanseFunctionsFromAction(workflows, action) : workflows;
    default:
      return workflows;
  }
}
