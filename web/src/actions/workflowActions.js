import ActionConstants from '../constants/actions';
import Immutable from 'immutable';
import { SUCCESS, PENDING } from '../constants/requests';
import { dispatchedRequest } from '../utils/api';
import * as ProjectActions from '../actions/projectActions';
import * as SampleActions from '../actions/sampleActions';
import { getWorkflowLayout } from '../utils/layout';
import { uniqueID } from '../utils/id';
import { ConnectionRecord, PortRecord, ProcessRecord } from '../constants/records';
import { convertToImmutableRecords } from '../utils/records';
import { WorkflowRecord, ProjectRecord, GraphRecord } from '../constants/records';
import { PROCESS_DEFINITIONS } from '../constants/process';
import { MODE_TEMPLATES } from '../constants/modeTemplates';

function _prepareWorkflow(workflow) {
  let data = workflow.toJS();
  data['projectId'] = data.project.id;
  delete data.content.graph.samples;
  console.log('sending ', Object.assign({}, data.content));
  data.content = JSON.stringify(data.content);
  return data;
}

export function layoutWorkflow(id) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let processes = getWorkflowLayout(workflow).filter((el, id) => workflow.content.graph.processes.get(id));
    let context = { id };
    // Note: this is far from the most efficient way to do this...It just happens to be an easy to understand way.
    // And, given the limitations of the ImmutableJS withMutations method, it seemed reasonable enough
    workflow = processes.reduce((reduction, p, pid) => reduction.mergeIn(['content', 'graph', 'processes', pid, 'metadata'], { x: p.get('x'), y: p.get('y') }), workflow);
    context.workflow = workflow;

    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.AUTO_LAYOUT, _prepareWorkflow(workflow), context));
  }
}

export function createWorkflowConnection(id, source, target, sourcePort, targetPort) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let connection = new ConnectionRecord({
      src: new PortRecord({ process: source, port: sourcePort }),
      tgt: new PortRecord({ process: target, port: targetPort }),
      metadata: Immutable.Map({ route: uniqueID() }),
    });
    let context = { id, connection };
    workflow = workflow.updateIn(['content', 'graph', 'connections'], Immutable.List(), connections => connections.push(connection));
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.CREATE_CONNECTION, _prepareWorkflow(workflow), context));
  }
}

export function removeWorkflowConnection(id, connection) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    workflow = workflow.updateIn(['content', 'graph', 'connections'], Immutable.List(), connections => connections.filter(c => !Immutable.is(c, connection)));
    const context = { id, connection: connection };
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.REMOVE_CONNECTION, data, context));
  }
}

export function selectWorkflow(id) {
  return { type: ActionConstants.SELECT_WORKFLOW, payload: id, status: SUCCESS }
}

export function removeWorkflow(id) {
  return dispatchedRequest('DELETE', '/user/workflow/' + id, ActionConstants.REMOVE_WORKFLOW, null, { id });
}

export function createNewWorkflow(name, projectName, templateID, mode) {
  return (dispatch, getState) => {
    let template = getState().templates.get(templateID);
    let workflow = new WorkflowRecord(template);
    // if mode is specified, it's a new workflow
    if (mode) {
      const graphRecord = convertToImmutableRecords(MODE_TEMPLATES[mode], GraphRecord)
      workflow = workflow.setIn(['content', 'graph'], graphRecord)
    }
    workflow = workflow.set('name', name);
    return dispatch(ProjectActions.createProject(new ProjectRecord({ name: projectName }))).then(action => dispatch(createWorkflow(workflow.set('project', action.payload))))
  }
}

export function createWorkflow(workflow) {
  return dispatchedRequest('POST', '/user/workflow', ActionConstants.CREATE_WORKFLOW, _prepareWorkflow(workflow), { workflow });
}

export function createWorkflowWithNewProject(workflow, project) {
  return function(dispatch) {
    return dispatch(ProjectActions.createProject(project)).then(action => dispatch(createWorkflow(workflow.set('project', action.payload))));
  }
}

export function getAllWorkflows() {
  return dispatchedRequest('GET', '/user/workflow', ActionConstants.GET_WORKFLOWS);
}

export function startWorkflowExecution(id) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    workflow = workflow.content.toJS();

    // do not send samples for running workflow
    if (workflow.graph.samples) {
      delete workflow.graph.samples
    }

    // TODO: remove this temporary demo workaround
    debugger
    if (workflow.graph.metainfo.mode == 'deploy') {
        dispatch({
          type: ActionConstants.RUN_WORKFLOW,
          status: PENDING,
          payload: workflow || {},
          context: { id },
        })
        setTimeout(() => {
          dispatch({
            type: ActionConstants.RUN_WORKFLOW,
            status: SUCCESS,
            payload: workflow || {},
            context: { id },
          })
        }, 15000)
    } else {
      return dispatch(dispatchedRequest('POST', '/workflows/' + id + '/runs', ActionConstants.RUN_WORKFLOW, workflow, {id}));
    }
  }
}

export function moveWorkflowProcess(id, processId, x, y) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processId };
    workflow = workflow.mergeIn(['content', 'graph', 'processes', processId, 'metadata'], Immutable.Map({ x, y }));
    context.process = workflow.content.graph.processes.get(processId);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.UPDATE_PROCESS, _prepareWorkflow(workflow), context));
  }
}

export function addWorkflowProcess(id, processType, x, y) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let processId = uniqueID();
    let context = { id, processId };
    let process = Object.assign({}, PROCESS_DEFINITIONS[processType], { component: processType }, {metadata: {x:x, y: y}});
    process = convertToImmutableRecords(process, ProcessRecord);
    workflow = workflow.setIn(['content', 'graph', 'processes', processId], process);
    context.process = process;
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.ADD_PROCESS, _prepareWorkflow(workflow), context));
  }
}

export function insertWorkflowCleanseProcess(id, connection) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);

    // delete connection
    workflow = workflow.updateIn(['content', 'graph', 'connections'], Immutable.List(), connections => connections.filter(c => !Immutable.is(c, connection)));

    // add new process
    let processId = uniqueID();
    let process = Object.assign({}, PROCESS_DEFINITIONS['ApplyFunctions'], { component: 'ApplyFunctions' });
    process = convertToImmutableRecords(process, ProcessRecord);
    workflow = workflow.setIn(['content', 'graph', 'processes', processId], process);


    // add two new connections
    let firstConnection = connection.mergeIn(['tgt'], { process: processId, port: 'in' }).setIn(['metadata', 'route'], uniqueID());
    let secondConnection = connection.mergeIn(['src'], { process: processId, port: 'out' }).setIn(['metadata', 'route'], uniqueID());
    let connections = [firstConnection, secondConnection];
    let context = { id, connection, connections, processId, process };
    workflow = workflow.updateIn(['content', 'graph', 'connections'], Immutable.List(), conns => conns.concat(connections));

    // submit request
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.INSERT_CLEANSE_PROCESS, _prepareWorkflow(workflow), context))
      .then(action => dispatch(SampleActions.selectSample(id, null)))
      .then(action => dispatch(selectWorkflowProcess(id, processId)))
      .then(action => dispatch(layoutWorkflow(id)));
  }
}

export function removeWorkflowProcess(id, processId) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processId };
    workflow = workflow.deleteIn(['content', 'graph', 'processes', processId]);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.REMOVE_PROCESS, _prepareWorkflow(workflow), context));
  }
}

export function shiftWorkflow(id, dx, dy) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, dx, dy };
    workflow.updateIn(['content', 'graph', 'processes'], Immutable.Map(), processes => processes.map(p => {
      p = p.setIn(['metadata', 'x'], p.metadata.get('x') + dx);
      p = p.setIn(['metadata', 'y'], p.metadata.get('y') + dy);
      return p;
    }));
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SHIFT_WORKFLOW, _prepareWorkflow(workflow), context));
  }
}

export function setProcessJoin(id, processID, type, condition, leftOperand, rightOperand, columns) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    const left = workflow.content.graph.connections.find(c => c.tgt.process == processID && c.tgt.port == 'left').src.process;
    const right = workflow.content.graph.connections.find(c => c.tgt.process == processID && c.tgt.port == 'right').src.process;
    let expression = `${left}("${leftOperand}") ${condition} ${right}("${rightOperand}")`;
    let selectColumns = columns.map(col => `${col.process}("${col.id}")`);
    let context = { id, processID, expression, type, selectColumns };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'joinCondition'], expression);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'joinType'], type);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'selectColumns'], selectColumns);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_PROCESS_JOIN, _prepareWorkflow(workflow), context));
  }
}

export function setProcessFilter(id, processID, condition) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processID, condition };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'condition'], condition);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_PROCESS_FILTER, _prepareWorkflow(workflow), context));
  }
}

// TODO: Reduce column setting actions into a single action which allows setting of fields on process property
export function setPieChartColumns(id, processID, labelColumn, countColumn) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processID, labelColumn, countColumn };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'label'], labelColumn);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'count'], countColumn);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_PIE_CHART_COLUMNS, _prepareWorkflow(workflow), context));
  }
}

export function setBoxPlotColumn(id, processID, column) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processID, column };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'column'], column);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_BOX_PLOT_COLUMN, _prepareWorkflow(workflow), context));
  }
}

export function setLineChartColumns(id, processID, xColumn, yColumn) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processID, xColumn, yColumn };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_LINE_CHART_COLUMNS, _prepareWorkflow(workflow), context));
  }
}

export function setBarChartColumns(id, processID, xColumn, yColumn) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processID, xColumn, yColumn };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_BAR_CHART_COLUMNS, _prepareWorkflow(workflow), context));
  }
}

export function setScatterPlotColumns(id, processID, xColumn, yColumn) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processID, xColumn, yColumn };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_SCATTER_PLOT_COLUMNS, _prepareWorkflow(workflow), context));
  }
}

export function setVisualizeColumns(id, processID, xColumn, yColumn, chartType) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processID, xColumn, yColumn, chartType };
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
    workflow = workflow.setIn(['content', 'graph', 'processes', processID, 'properties', 'chartType'], chartType);
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.SET_VISUALIZE_COLUMNS, _prepareWorkflow(workflow), context));
  }
}

export function selectWorkflowProcess(workflowID, processID) {
  return {
    type: ActionConstants.SELECT_PROCESS,
    payload: {
      workflow: { id: workflowID },
      process: { id: processID }
    },
    status: SUCCESS
  }
}

export function removeCleanseFunction(id, processId, func) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processId, func };
    workflow = workflow.updateIn(['content', 'graph', 'processes', processId, 'properties', 'functions'], Immutable.List(), functions => functions.filter(f => !Immutable.is(f, func)));
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.REMOVE_CLEANSE_FUNCTION, _prepareWorkflow(workflow), context));
  }
}

export function addCleanseFunction(id, processId, func) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processId, func };
    workflow = workflow.updateIn(['content', 'graph', 'processes', processId, 'properties', 'functions'], Immutable.List(), functions => functions.push(Immutable.Map(func)) );
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.ADD_CLEANSE_FUNCTION, _prepareWorkflow(workflow), context));
  }
}

export function reorderCleanseFunctions(id, processId, functions) {
  return (dispatch, getState) => {
    let workflow = getState().workflows.get(id);
    let context = { id, processId, functions };
    workflow = workflow.updateIn(['content', 'graph', 'processes', processId, 'properties', 'functions'], Immutable.List(), f => Immutable.List(functions));
    return dispatch(dispatchedRequest('POST', '/user/workflow/' + id, ActionConstants.REORDER_CLEANSE_FUNCTIONS, _prepareWorkflow(workflow), context));
  }
}
