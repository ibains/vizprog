import ActionConstants from '../constants/actions';
import { SUCCESS } from '../constants/requests';
import { dispatchedRequest } from '../utils/api';

export function getUserProjects() {
  return dispatchedRequest('GET', '/user/project', ActionConstants.GET_PROJECTS);
}

export function createProject(project) {
  return dispatchedRequest('POST', '/user/project', ActionConstants.CREATE_PROJECT, project, project);
}
