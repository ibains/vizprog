import ActionConstants from '../constants/actions';
import Immutable from 'immutable';
import { SUCCESS } from '../constants/requests';
import { dispatchedRequest } from '../utils/api';
import { convertToImmutableRecords } from '../utils/records';

export function getFunctionDefinitions(id) {
  return dispatchedRequest('GET', '/specs', ActionConstants.GET_FUNCTION_DEFINITIONS);
}

export function getNodeSpecifications(id) {
  return dispatchedRequest('GET', '/specs/nodes', ActionConstants.GET_NODE_SPECIFICATIONS);
}
