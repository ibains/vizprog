import ActionConstants from '../constants/actions';
import { dispatchedRequest } from '../utils/api';

export function getUser() {
  return dispatchedRequest('GET', '/currentUserCredentials', ActionConstants.GET_USER);
}
