import Immutable from 'immutable';
import ActionConstants from '../constants/actions';
import { SUCCESS } from '../constants/requests';

export function selectSample(workflowID, sampleID) {
  return {
    type: ActionConstants.SELECT_SAMPLE,
    payload: {
      workflow: { id: workflowID },
      sample: { id: sampleID },
    },
    status: SUCCESS,
  }
}
