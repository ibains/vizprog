import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import SvgIcon from 'material-ui/SvgIcon';

const IconWorkflowIO = React.createClass({

  mixins: [PureRenderMixin],

  render() {
    return (
      <SvgIcon {...this.props}>
      </SvgIcon>
    );
  }

});

export default IconWorkflowIO;
