import React from 'react'
import Relay from 'react-relay'
import { Router, Route, IndexRoute, IndexRedirect, applyRouterMiddleware } from 'react-router'
import AppView from 'views/AppView'
import CoreLayout from 'layouts/CoreLayout'
import LoginView from 'views/LoginView'
import BrowseView from 'views/BrowseView'
import DiscoverView from 'views/DiscoverView'
import WorkflowView from 'views/WorkflowView'
import AccountUserView from 'views/Account/AccountUserView'
import AccountWorkflowView from 'views/Account/AccountWorkflowView'
import AccountTeamView from 'views/Account/AccountTeamView'
import AccountDatasetView from 'views/Account/AccountDatasetView'
import AccountProjectView from 'views/Account/AccountProjectView'

import useRelay from 'react-router-relay'

export default (history, store) => {

	const requireLogin = (nextState, replace, cb) => {
    const { auth: { token }} = store.getState();
    if (!token) {
      // oops, not logged in, so can't be here!
      replace('/login');
    }
    cb();
  };

	return (
		<Router
      history={history}
      render={applyRouterMiddleware(useRelay)}
      environment={Relay.Store}
    >
		  <Route name="App" component={AppView} path='/' /*onEnter={requireLogin}*/ >
		    <IndexRoute component={WorkflowView} />
		    <Route path='discover' component={DiscoverView} />
        <Route path="browse" component={BrowseView} />
        <Route name="User"  path="account/user/:userID" component={AccountUserView} />
        <Route name="Workflow" path="account/workflow/:workflowID" component={AccountWorkflowView} />
        <Route name="Team"  path="account/team/:teamID" component={AccountTeamView} />
        <Route name="Dataset" path="account/dataset/:datasetID" component={AccountDatasetView} />
        <Route name="Project"  path="account/project/:projectID" component={AccountProjectView} />
		  </Route>
      <Route path="login" component={LoginView} />
		</Router>
  );
}
