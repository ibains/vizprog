import Relay from 'react-relay';

export default class extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`
      query {
        Workflow (id: $workflowID)
      }
    `,
  };
  static paramDefinitions = {
    // By setting `required` to true, `ProfileRoute` will throw if a `workflowID`
    // is not supplied when instantiated.
    workflowID: {required: true},
  };
  static routeName = 'accountWorkflowRoute';
}
