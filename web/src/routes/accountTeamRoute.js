import Relay from 'react-relay';

export default class extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`
      query {
        Team (id: $teamID)
      }
    `,
  };
  static paramDefinitions = {
    // By setting `required` to true, `ProfileRoute` will throw if a `teamID`
    // is not supplied when instantiated.
    teamID: {required: true},
  };
  static routeName = 'accountTeamRoute';
}
