import Relay from 'react-relay';

export default class extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`
      query {
        Project (id: $projectID)
      }
    `,
  };
  static paramDefinitions = {
    // By setting `required` to true, `ProfileRoute` will throw if a `projectID`
    // is not supplied when instantiated.
    projectID: {required: true},
  };
  static routeName = 'accountProjectRoute';
}
