import Relay from 'react-relay';

export default class extends Relay.Route {
  static queries = {
    viewer: () => Relay.QL`
      query {
        Dataset (id: $datasetID)
      }
    `,
  };
  static paramDefinitions = {
    // By setting `required` to true, `ProfileRoute` will throw if a `datasetID`
    // is not supplied when instantiated.
    datasetID: {required: true},
  };
  static routeName = 'accountDatasetRoute';
}
