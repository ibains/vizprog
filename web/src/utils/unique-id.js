let lastId = 0;

export default function(prefix='id_') {
    lastId++;
    return `${prefix}${lastId}`;
}