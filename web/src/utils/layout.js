import Immutable from 'immutable';

import { NODE_WIDTH, NODE_HEIGHT } from '../constants/theme'

const GROUP_PADDING = 50;

const KLAY_OPTIONS = {
  "intCoordinates": true,
  "layoutHierarchy": true,
  "spacing": 30,
  "borderSpacing": 50,
  "nodeLayering": "NETWORK_SIMPLEX",
  "edgeRouting": "POLYLINE",
  "crossMin": "LAYER_SWEEP",
  "direction": "RIGHT",
};

export function getWorkflowLayout(workflow) {
  const processes = workflow.content.graph.processes
  const groups = workflow.content.graph.groups
  const connections = workflow.content.graph.connections


  const topLevelNodes = _findTopLevelProcesses(groups, processes).map(_convertProcessToKNode).toArray();
  const topLevelGroups = groups.map(g => _convertToKGraph(groups, processes, g.name)).toArray();
  const edges = connections.map(_convertConnectionToKEdge).toArray();

  let kGraph = {
    id: 'root',
    children: topLevelNodes.concat(topLevelGroups),
    edges: edges
  }

  let newLayout = {}
  // TODO: Use promises to resolve the graph layout
  $klay.layout({
    graph: kGraph,
    options: KLAY_OPTIONS,
    success: (layout) => {
      newLayout = layout
    },
    error: (error) => {
      console.error('klay layout failed: ', error);
    },
  });
  return _flattenKlayLayout(newLayout);
}

// Memo-ized recursive function to convert any level element
// type (graph, group, subgroup^n, or process) to kGraph format
let _convertToKGraph = (function() {
  let graphMemo = {};
  let _recursiveConvert = function(groups, processes, id) {
    if (graphMemo[id]) {
      return graphMemo[id];
    } else {
      if (processes.get(id)) {
        const kNode = _convertProcessToKNode(processes.get(id), id);
        graphMemo[id] = kNode;
        return kNode;
      } else {
        const group = groups.find(g => g.name == id);
        const children = group.nodes.map(nodeId => {
          return _recursiveConvert(groups, processes, nodeId);
        }).toArray();
        graphMemo[id] = { id, children };
        return graphMemo[id];
      }
    }
  }
  return _recursiveConvert;
}());

function _findTopLevelProcesses(groups, processes) {
  let unseenProcesses = processes;
  let recursiveFind = function(id) {
    if (processes.get(id)) {
      unseenProcesses.remove(id);
    } else {
      groups.find(g => g.name == id).nodes.forEach(nodeId => recursiveFind(nodeId));
    }
  }
  groups.forEach(g => recursiveFind(g.name));
  return unseenProcesses;
}

function _flattenKlayLayout(layout) {
  let _flattenElement = function(element, xOffset, yOffset) {
    if (element.children) {
      const children = element.children.reduce((collection, child) => {
        const flattenedChild = _flattenElement(child, xOffset + (element.x || 0), yOffset + (element.y || 0));
        return collection.merge(flattenedChild);
      }, Immutable.Map());
      delete element.children;
      return children.set(element.id, Immutable.Map(element));
    } else {
      return { [element.id] : Immutable.Map(element).merge({x: element.x + xOffset, y: element.y + yOffset}) }
    }
  }
  let a = _flattenElement(layout, 0, 0).remove('root');
  return a;
}

function _convertProcessToKNode(process, id) {
  const inPorts = process.ports.inputs.map((key, index) => ({ id: id + '_' + key, x: 0, y: 0 })).toArray();
  const outPorts = process.ports.outputs.map((key, index) => ({ id: id + '_' + key, x: 100, y: 0 })).toArray();
  return {
    id: id,
    width: NODE_WIDTH,
    height: NODE_HEIGHT + 20,
    ports: inPorts.concat(outPorts),
    properties: { portConstraints: 'FIXED_POS' }
  }
}

function _convertConnectionToKEdge(connection) {
  return {
    id: connection.metadata.get('route'),
    source: connection.src.process,
    sourcePort: connection.src.process + '_' + connection.src.port,
    target: connection.tgt.process,
    targetPort: connection.tgt.process + '_' + connection.tgt.port
  }
}

export function getGroupBounds(group, allGroups, allProcesses) {
  function _getNodeBounds(nodeId) {
    let process = allProcesses.get(nodeId);
    if (process) {
      return {
        left: process.metadata.get('x'),
        right: process.metadata.get('x') + NODE_WIDTH,
        top: process.metadata.get('y'),
        bottom: process.metadata.get('y') + NODE_HEIGHT,
      }
    } else {
      let group = allGroups.find(g => g.name == nodeId);
      let allNodeBounds = group.nodes.map(_getNodeBounds);
      return {
        top: Math.min(...allNodeBounds.map(b => b.top - GROUP_PADDING).values()) || 0,
        bottom: Math.max(...allNodeBounds.map(b => b.bottom + GROUP_PADDING + 20).values()) || Number.MAX_VALUE,
        right: Math.max(...allNodeBounds.map(b => b.right + GROUP_PADDING).values()) || Number.MAX_VALUE,
        left: Math.min(...allNodeBounds.map(b => b.left - GROUP_PADDING).values()) || 0,
      }
    }
  }
  return _getNodeBounds(group.name);
}
