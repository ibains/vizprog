export const ROW_REGEX = /Row([0-9]+)/;

// there is a huge assumption here, which is that the order
// of the schema will match the order of the data. This is
// NOT GUARANTEED BY ANY JAVASCRIPT IMPLEMENTATION. However,
// there is no other way to associate the two. Long term,
// this should be addressed by restructuring the data stores.
// Short term, most javascript implementations actually do
// respect order, so it probably won't be an issue for demo
// purposes. TODO: Fix
export function samplesToColumns(samples) {
    let columns = [];
    for (let prop in samples.schema) {
        if (samples.schema.hasOwnProperty(prop)) {
            columns.push(prop);
        }
    }
    return columns;
}

// The data in the store is in an object, with each row
// in the data set being represented by another object
// under the "Row{x}" field, where x is the index of the
// column
export function extractColumnData(data, column=0) {
    let columnData = [];
    for (let prop in data) {
        if (ROW_REGEX.test(prop)) {
            columnData.push(data[prop][column]);
        }
    }
    return columnData;
}

// Sort an array of independent variables along with the
// corresponding dependent variable. This is intended to
// be used to take arrays of data like x = [2,3,1] and
// y = ['b','a','c'] and return it as { x, y } where
// now x = [1,2,3] and y=['c','b','a']
export function sortAdjacent(unsortedX, unsortedY, transform=(v) => v) {
  var combined = [];
  for (var index in unsortedX) {
    combined.push({'x': transform(unsortedX[index]), 'y': transform(unsortedY[index])});
  }
  combined.sort((a,b) => (a.x < b.x) ? -1 : ((a.x == b.x) ? 0 : 1));
  let x = [];
  let y = [];
  for (var index in combined) {
    x[index] = combined[index].x;
    y[index] = combined[index].y;
  }
  return { x, y }
}
