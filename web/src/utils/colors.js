export const OTHER_COLORS = [
    'rgb(248, 222, 253)',
    'rgb(192, 254, 255)',
    'rgb(204, 234, 255)',
    'rgb(255, 204, 153)',
    'rgb(255, 229, 204)',
    'rgb(87, 41, 177)',
    'rgb(184, 159, 233)',
    'rgb(157, 9, 209)',
    'rgb(239, 199, 253)',
    'rgb(34, 34, 34)',
    'rgb(127, 127, 127)',
];

export const MAIN_COLORS = [
    'rgb(218, 90, 244)',
    'rgb(0, 191, 195)',
    'rgb(0, 150, 255)',
    'rgb(255, 127, 1)',
]

export const ALL_COLORS = OTHER_COLORS.concat(MAIN_COLORS);

export default MAIN_COLORS;
