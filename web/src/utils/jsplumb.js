import React from 'react'
import ReactDOM from 'react-dom';

import jsplumb from 'script!../../node_modules/jsplumb';

import { PROCESS_DEFINITIONS } from '../constants/process';
import { CATEGORY_DEFINITIONS } from '../constants/category';

const SOURCE_ARROW = { location: -10, width: 10, length: 5, cssClass: 'workflow-edge-overlay' };
const TARGET_ARROW = { location: 20, width: 10, length: 5, cssClass: 'workflow-edge-overlay' };
const SOURCE_LABEL = { label: null, location: 20, cssClass: 'workflow-edge-overlay-label workflow-edge-path-label' };
const TARGET_LABEL = { label: null, location: -20, cssClass: 'workflow-edge-overlay-label workflow-edge-path-label right' };
const JSPLUMB_DEFAULTS = {
  Connector : [ "Flowchart", {cornerRadius: 5} ],
  EndpointStyle : { fillStyle : "gray", radius: 5 },
  PaintStyle: { strokeStyle: "#ccc", lineWidth: 3 }
};
const SAMPLE_OVERLAY = {
  create: (component) => {
    const iconBox = document.createElement('div');
    const icon = document.createElement('i');
    icon.className = 'material-icons';
    icon.innerHTML = 'insert_chart';
    iconBox.appendChild(icon);
    return iconBox;
  },
  location: 0.5,
  cssClass: 'workflow-edge-overlay workflow-edge-chart',
  id: 'label'
}
const DELETE_OVERLAY = {
  create: (component) => {
    const iconBox = document.createElement('div');
    const icon = document.createElement('i');
    icon.className = 'material-icons';
    icon.innerHTML = 'cancel';
    iconBox.appendChild(icon);
    return iconBox;
  },
  location: 0.8,
  cssClass: 'workflow-edge-delete-icon workflow-edge-overlay-label',
  id: 'delete'
}

// This is a bit of a mess of a higher order component. However, JSPlumb just doesn't play
// that nicely with React. Longterm, TODO: implement using something other that JSPlumb
export const connectWorkflowToJsPlumb = (InnerComponent) => class extends React.Component {
  constructor(props, context) {
    super(props, context);
    this._createConnection = this._createConnection.bind(this);
    this._createPorts = this._createPorts.bind(this);
    this._updateJsPlumb = this._updateJsPlumb.bind(this);
    this.state = { dragging: false };
  }
  _createConnection(connection, samples) {
    const source = this._portsByProcess[connection.src.process];
    const target = this._portsByProcess[connection.tgt.process];
    if (!source || !target) {
      console.warn('Unknown source and target!: ', source, target);
      return;
    }
    const sampleId = connection.src.process + '_' + connection.src.port;
    let overlays = [
      ['PlainArrow', SOURCE_ARROW],
      ['PlainArrow', TARGET_ARROW],
      ['Label', Object.assign({}, SOURCE_LABEL, { label: connection.src.port.toUpperCase() })],
      ['Label', Object.assign({}, TARGET_LABEL, { label: connection.tgt.port.toUpperCase() })],
      ['Custom', Object.assign({}, DELETE_OVERLAY, {
        events: {
          click: this.props.actions.removeWorkflowConnection.bind(null, this.props.workflow, connection)
        }
      })]
    ];
    if (samples && samples.get(sampleId)) {
      const selectSample = this.props.actions.selectSample.bind(null, { id: this.props.id }, sampleId);
      overlays.push(['Custom', Object.assign({}, SAMPLE_OVERLAY, { events: { click: selectSample } })]);
    }
    this.jsplumb.connect({
      source: source[connection.src.port],
      target: target[connection.tgt.port],
      overlays: overlays,
      endPoint: 'circle',
      deleteEndpointsOnDetach: false,
      cssClass: 'workflow-edge'
    });
  }
  _createPorts(id, process) {
    let _endpointRefs = {};
    let processType = PROCESS_DEFINITIONS[process.component].category;
    let inputs = process.ports.inputs;
    let outputs = process.ports.outputs;
    let color = CATEGORY_DEFINITIONS.getIn([processType, 'color']);
    inputs.forEach((input, ind) => {
      _endpointRefs[input] = this.jsplumb.addEndpoint(id, {
        isTarget: true,
        anchor: [0, (ind + 1) / (inputs.size + 1), -1, 0, -1.6, -1.6, input],
        paintStyle: { fillStyle: CATEGORY_DEFINITIONS.getIn([processType,'color']), radius: 3 },
        cssClass: 'workflow-node-endpoint',
      });
    });
    outputs.forEach((output, ind) => {
      _endpointRefs[output] = this.jsplumb.addEndpoint(id, {
        isSource: true,
        maxConnections: -1,
        anchor: [1, (ind + 1) / (outputs.size + 1), 1, 0, -2, -1.25, output],
        paintStyle: { fillStyle: CATEGORY_DEFINITIONS.getIn([processType,'color']), radius: 2.5 },
        cssClass: 'workflow-node-endpoint'
      });
    });
    return _endpointRefs;
  }
  _updateJsPlumb() {
    this.workflowPane.reset();
    this.jsplumb.reset();

    const { id, workflow, actions } = this.props;
    this.jsplumb.bind('connection', (info, event) => {
      if (event) {
        const { sourceId, targetId } = info;
        const srcPort = info.sourceEndpoint.anchor.cssClass;
        const tgtPort = info.targetEndpoint.anchor.cssClass;
        actions.createWorkflowConnection(id, sourceId, targetId, srcPort, tgtPort);
      }
    });

    this.jsplumb.bind("connectionDetached", (info, event) => {
      if (event) {
        const { sourceId, targetId } = info;
        const srcPort = info.sourceEndpoint.anchor.cssClass;
        const tgtPort = info.targetEndpoint.anchor.cssClass;
        actions.removeWorkflowConnection({ id }, sourceId, targetId, srcPort, tgtPort);
      }
    });

    this.jsplumb.draggable(document.getElementsByClassName("workflow-node"), {
      containment: false,
      start: () => this.setState({ dragging: true }),
      stop: (info) => {
        actions.moveWorkflowProcess(this.props.id, info.el.id, info.el.offsetLeft, info.el.offsetTop);
      },
    });

    let clientX = null;
    let clientY = null;
    this.workflowPane.draggable(document.getElementsByClassName('workflow-pane'), {
      //stop: (event) => this.props.actions.shiftWorkflow(event.pos[0], event.pos[1])
    });

    this._portsByProcess = {};
    const samples = workflow.content.graph.samples;
    workflow.content.graph.processes.forEach((process, id) => this._portsByProcess[id] = this._createPorts(id, process));
    workflow.content.graph.connections.map(c => c.toJS()).forEach(conn => this._createConnection(conn, samples));
  }
  componentWillUnmount() {
    this.workflowPane.reset();
    this.jsplumb.reset();
  }
  componentDidMount() {
    this.jsplumb = jsPlumb.getInstance();
    this.workflowPane = jsPlumb.getInstance();
    this.jsplumb.importDefaults(JSPLUMB_DEFAULTS);
    this._updateJsPlumb();
  }
  componentDidUpdate(prevProps, prevState) {
    this._updateJsPlumb();
  }
  render() {
    return <InnerComponent {...this.props} dragging={this.state.dragging} onDragEnd={() => this.setState({ dragging: false})}/>;
  }
}
