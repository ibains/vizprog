import fetch from 'isomorphic-fetch';

import ActionConstants from '../constants/actions';
import { ERROR, UNAUTHORIZED, PENDING, SUCCESS } from '../constants/requests';

import { API_Config } from '../constants';

class ServerError extends Error {
  constructor(message) {
    super(message);
    this.name = 'ServerError';
    this.message = message;
  }
}

class UnauthorizedError extends Error {
  constructor(message) {
    super(message);
    this.name = 'UnauthorizedError';
    this.message = message;
  }
}

function handleStatus(response, redirect) {
  if (response.status >= 200 && response.status < 300) {
    return Promise.resolve(response)
  } else if (response.status == 401 || response.status == 303) {
    return Promise.reject(new UnauthorizedError());
  } else {
    return Promise.reject(new ServerError(response.statusText));
  }
}

export function dispatchedRequest(method, url, actionType, data, context = null, json = true) {
  // we dispatch the pending action with some data so that the reducers
  // can know what data we're attempting to operate on, even if that
  // operation isn't yet successful. TODO: Use redux-saga so we don't
  // have this horrible chained promises mess.
  return dispatch => {
    dispatch({
      type: actionType,
      status: PENDING,
      payload: data || {},
      context: context,
    });
    return fetch(API_Config.serverUrl + url, {
      method: method,
      credentials: 'include', // CORS Hack
      headers: json ? { 'Content-Type': 'application/json; charset=utf-8', 'Accept': 'application/json;' } : false,
      body: json ? JSON.stringify(data) : data })
      .then(handleStatus)
      //.then(response => { console.log(response); return response; })
      .then(response => response.json())
      //.then(json => { console.log(json); return json })
      .then(json => dispatch({
        type: actionType,
        status: SUCCESS,
        payload: json,
        context: context,
      }))
      .catch(error => {
        const warn = (console.warn || console.log).bind(console);
        let status = ERROR;
        switch (error.name) {
          case 'UnauthorizedError':
            status = UNAUTHORIZED;
            break;
          case 'ServerError':
            status = ERROR;
            warn('Server Error: ' + error.message);
            break;
          default:
            warn('Error (' + error.name + ') after request: ' + error.message);
            warn(error.stack);
            status = ERROR;
            break;
        }
        return dispatch({
          type: actionType,
          status: status,
          payload: Object.assign(data || {}, {message: error.message || {}}),
          context: context,
        })
      });
  };
}
