import uuid from 'node-uuid';

export const uniqueID = () => uuid.v1().split('-').join('');
