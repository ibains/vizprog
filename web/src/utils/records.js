import Immutable from 'immutable';

import * as Records from '../constants/records';

export const convertToImmutableRecords = (data, rootRecord = Records.WorkflowRecord) => {
  if (rootRecord == Records.WorkflowRecord) {
    // this hack is required because the workflow returned by the api doesn't necessary
    // have all of the fields we expect. This could be solved by flattening the state
    // in the app, or doing more strict checking on both the app and API side
    if (!data.content) { data.content = { graph: {} }; };

    // this hack is required because only missing fields trigger the default value
    // in a record, so we actually have to remove the field itself.
    if (!data.content.graph.samples) { delete data.content.graph.samples; }

  }
  return Immutable.fromJS(data, (key, value) => {
    var isIndexed = Immutable.Iterable.isIndexed(value);
    if (key == 'cos') {
      console.log(value);
    }
    switch (key) {
      case 'schema': return value.toOrderedMap(); break;
      case 'data': return value.toOrderedMap(); break;
      case 'project': return new Records.ProjectRecord(value); break;
      case 'user': return new Records.UserRecord(value); break;
      case 'content': return new Records.ContentRecord(value); break;
      case 'graph': return new Records.GraphRecord(value); break;
      case 'processes': return value.toMap().map(p => new Records.ProcessRecord(p)); break;
      case 'connections': return value.toList().map(c => new Records.ConnectionRecord(c)); break;
      case 'groups': return value.toList().map(g => new Records.GroupRecord(g)); break;
      case 'samples': return value.toMap().map(s => new Records.SampleRecord(s)); break;
      case 'nodes': return value.toList(); break;
      case 'metadata': return value.toMap(); break;
      case 'properties': return value.toMap(); break;
      case 'ports': return new Records.PortListRecord(value); break;
      case 'src': return new Records.PortRecord(value); break;
      case 'tgt': return new Records.PortRecord(value); break;
      case 'metainfo': return new Records.MetaInfoRecord(value); break;
      case 'stats': return new Records.StatsRecord(value); break;
      case 'Specification': return new Records.FunctionTypesRecord(value); break;
      case 'DataType': return value.toMap().map(d => new Records.DataTypeRecord(d)); break;
      case 'StringFunctions': return value.toMap().map(f => new Records.FunctionDefinitionRecord(f)); break;
      case 'DateTimeFunctions': return value.toMap().map(f => new Records.FunctionDefinitionRecord(f)); break;
      case 'AggregateFunctions': return value.toMap().map(f => new Records.FunctionDefinitionRecord(f)); break;
      case 'MathFunctions': return value.toMap().map(f => new Records.FunctionDefinitionRecord(f)); break;
      case 'MiscFunctions': return value.toMap().map(f => new Records.FunctionDefinitionRecord(f)); break;
      case '': return new rootRecord(value); break;
      default: return isIndexed ? value.toList() : value.toMap(); break;
    }
  });
};

export const convertNodeSpecsToImmutableRecords = (data, rootRecord = Records.NodeSpecsRecord) => {
  return Immutable.fromJS(data, (key, value) => {
    var isIndexed = Immutable.Iterable.isIndexed(value);
    switch (key) {
      case 'Specification': return new Records.NodeSpecsRecord(value); break;
      case 'MLNodes': return value.toMap().map(d => new Records.MLNodeRecord(d)); break;
      case 'ports': return new Records.PortListRecord(value); break;
      case 'properties': return value.toMap().map(d => new Records.MLNodePropertiesRecord(d)); break;
      case '': return new rootRecord(value); break;
      default: return isIndexed ? value.toList() : value.toMap(); break;
    }
  });
};

export const cleanConnections = (workflow) => workflow.updateIn(['content', 'graph', 'connections'], Immutable.List(), connections => {
  let processes = workflow.content.graph.processes;
  return connections.filter(c => processes.find((p, id) => id == c.src.process) && processes.find((p, id) => id == c.tgt.process));
});

export const addWorkflowFromAction = (workflows, action) => {
  let workflow = convertToImmutableRecords(action.context.workflow);
  workflow = cleanConnections(workflow);
  return workflows.set(action.payload.id, workflow);
};

export const updateWorkflowFromAction = (workflows, action) => {
  let workflow = convertToImmutableRecords(action.context.workflow);
  workflow = cleanConnections(workflow);
  return workflows.set(action.context.id, workflow);
};

export const addWorkflowsFromAction = (workflows, action) => {
  let newWorkflows = action.payload.filter(w => w.project.name != 'Sample'); // Hack to deal with Sample workflows
  return workflows.merge(newWorkflows.map(w => [w.id, cleanConnections(convertToImmutableRecords(w)) ]));
};

export const addTemplatesFromAction = (templates, action) => {
  let newTemplates = action.payload.filter(t => t.project.name == 'Sample'); // Hack to deal with Sample workflows
  return templates.merge(newTemplates.map(t => [t.id, cleanConnections(convertToImmutableRecords(t)) ]));
};

export const addProcessFromAction = (workflows, action) => {
  return workflows.setIn([action.context.id, 'content', 'graph', 'processes', action.context.processId], action.context.process);
};

export const removeProcessFromAction = (workflows, action) => {
  workflows = workflows.deleteIn([action.context.id, 'content', 'graph', 'processes', action.context.processId]);
  return workflows.set(action.context.id, cleanConnections(workflows.get(action.context.id)));
};

export const updateProcessFromAction = (workflows, action) => {
  workflows = workflows.mergeDeepIn([action.context.id, 'content', 'graph', 'processes', action.context.processId], action.context.process);
  return workflows.set(action.context.id, cleanConnections(workflows.get(action.context.id)));
};

export const addConnectionFromAction = (workflows, action) => {
  // TODO: Move this work into action creator
  const src = new Records.PortRecord(action.context.connection.src);
  const tgt = new Records.PortRecord(action.context.connection.tgt);
  const metadata = Immutable.Map(action.context.connection.metadata);
  const connection = Records.ConnectionRecord({ src, tgt, metadata });
  return workflows.updateIn([action.context.id, 'content', 'graph', 'connections'], Immutable.List(), connections => connections.push(connection));
};

export const removeConnectionFromAction = (workflows, action) => {
  let filter = connections => connections.filter(c => !Immutable.is(c, Immutable.fromJS(action.context.connection)));
  return workflows.updateIn([action.context.id, 'content', 'graph', 'connections'], Immutable.List(), filter);
};

export const addSamplesFromAction = (workflows, action) => {
  let metaId = workflows.get(action.context.id).content.graph.metainfo.id;
  let data = { samples: action.payload[metaId] };
  let samples = convertToImmutableRecords(data, Records.GraphRecord).samples;
  workflows = workflows.setIn([action.context.id, 'content', 'graph', 'samples'], samples);
  workflows = workflows.setIn([action.context.id, 'isRunning'], false);
  return workflows;
};

export const shiftWorkflowFromAction = (workflows, action) => {
  return workflows.updateIn([action.context.id, 'content', 'graph', 'processes'], Immutable.Map(), processes => processes.map(p => {
    p = p.setIn(['metadata', 'x'], p.metadata.get('x') + action.context.dx);
    p = p.setIn(['metadata', 'y'], p.metadata.get('y') + action.context.dy);
    return p;
  }));
};

export const addProcessJoinFromAction = (workflows, action) => {
  const { id, processID, expression, type, selectColumns } = action.context;
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'joinType'], type);
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'joinCondition'], expression);
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'selectColumns'], selectColumns);
};

export const addProcessFilterFromAction = (workflows, action) => {
  const { id, processID, condition } = action.context;
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'condition'], condition);
};

export const addPieChartColumnsFromAction = (workflows, action) => {
  const { id, processID, labelColumn, countColumn } = action.context;
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'label'], labelColumn);
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'count'], countColumn);
};

export const addLineChartColumnsFromAction = (workflows, action) => {
  const { id, processID, xColumn, yColumn } = action.context;
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
};

export const addBarChartColumnsFromAction = (workflows, action) => {
  const { id, processID, xColumn, yColumn } = action.context;
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
};

export const addScatterPlotColumnsFromAction = (workflows, action) => {
  const { id, processID, xColumn, yColumn } = action.context;
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
};

export const addVisualizeColumnsFromAction = (workflows, action) => {
  const { id, processID, xColumn, yColumn, chartType } = action.context;
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'xColumn'], xColumn);
  workflows = workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'chartType'], chartType);
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'yColumn'], yColumn);
};

export const addBoxPlotColumnFromAction = (workflows, action) => {
  const { id, processID, column } = action.context;
  return workflows.setIn([id, 'content', 'graph', 'processes', processID, 'properties', 'column'], column);
};

export const getFunctionSpecsFromAction = (functions, action) => {
  // Returning a map because there is some weird behavior occuring when trying to map over
  // a record. It seems like the record is being mutated...
  const spec = convertToImmutableRecords(action.payload.Specification, Records.FunctionTypesRecord).toMap();
  // spec is not sorted by function key from the API, so sort it first
  const sortedSpec = spec.map((category) => {
    return category.sortBy((v, k) => { return k })
  })
  return sortedSpec
};

export const getNodeSpecsFromAction = (functions, action) => {
  const spec = convertNodeSpecsToImmutableRecords(action.payload.Specification, Records.NodeSpecsRecord).toMap();
  return spec;
};

export const insertCleanseProcessFromAction = (workflows, action) => {
    const { id, connections, connection, processId, process } = action.context;

    // delete connection
    workflows = workflows.updateIn([id, 'content', 'graph', 'connections'], Immutable.List(), connections => connections.filter(c => !Immutable.is(c, connection)));

    // add new process
    workflows = workflows.setIn([id, 'content', 'graph', 'processes', processId], process);

    // add two new connections
    workflows = workflows.updateIn([id, 'content', 'graph', 'connections'], Immutable.List(), conns => conns.concat(connections));

    return workflows;
};

export const addCleanseFunctionFromAction = (workflows, action) => {
  const { id, processId, func } = action.context;
  return workflows.updateIn([id, 'content', 'graph', 'processes', processId, 'properties', 'functions'], Immutable.List(), functions => functions.push(Immutable.Map(func)));
};

export const removeCleanseFunctionFromAction = (workflows, action) => {
  const { id, processId, func } = action.context;
  return workflows.updateIn([id, 'content', 'graph', 'processes', processId, 'properties', 'functions'], Immutable.List(), functions => functions.filter(f => !Immutable.is(f, func)));
};

export const reorderCleanseFunctionsFromAction = (workflows, action) => {
  const { id, processId, functions } = action.context;
  return workflows.updateIn([id, 'content', 'graph', 'processes', processId, 'properties', 'functions'], Immutable.List(), f => Immutable.List(functions));
};
