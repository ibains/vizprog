const outColor = "#8e44ad"
const inColor = "#00B16A"
const tfColor = "#4586f3"
const mlColor = "#4586f3"

export function getNodeRenderProps(type) {
  switch (type) {
    case "FileInput":
      return { iconName: "file-text-o", color: inColor }
    case "FileOutput":
      return { iconName: "file-text-o", color: outColor }
    case "Join":
      return { iconName: "random", color: tfColor }
    case "BarGraph":
      return { iconName: "bar-chart", color: outColor }
    case "LineGraph":
      return { iconName: "line-chart", color: outColor }
    case "PieChart":
      return { iconName: "pie-chart", color: outColor }
    case "AreaGraph":
      return { iconName: "area-chart", color: outColor }
    case "BoxPlot":
      return { iconName: "area-chart", color: outColor }
    case "Filter":
      return { iconName: "filter", color: tfColor }
    case "Cube":
      return { iconName: "cube", color: tfColor }
    case "AddColumn":
      return { iconName: "plus", color: tfColor }
    case "WindowFunction":
      return { iconName: "windows", color: tfColor }
    case "Sample":
      return { iconName: "hand-lizard-o", color: tfColor }
    case "Except":
      return { iconName: "exclamation", color: tfColor }
    case "DropMissing":
      return { iconName: "trash", color: tfColor }
    case "Classification":
      return { iconName: "sitemap", color: mlColor }
    case "Clustering":
      return { iconName: "group", color: mlColor }
    case "Regression":
      return { iconName: "line-chart", color: mlColor }
    case "Script":
    default:
      return { iconName: "file-code-o", color: tfColor }
  }
}

export function getCategoryRenderProps(category) {
  switch (category) {
    case "io":
      return {
        color: '#00CBD5',
        backgroundImage: `url("data:image/svg+xml,%3Csvg width='48' height='42' viewBox='0 0 48 42' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eshape2 copy 3%3C/title%3E%3Cdefs%3E%3ClinearGradient x1='-12.883%25' y1='-36.8%25' x2='81.457%25' y2='144.84%25' id='a'%3E%3Cstop stop-color='%2313A5AC' offset='0%25'/%3E%3Cstop stop-color='%2316EAF5' offset='100%25'/%3E%3C/linearGradient%3E%3C/defs%3E%3Cg transform='translate%28-46 -38%29' fill='url%28%23a%29' fill-rule='evenodd'%3E%3Cpath d='M58.15 79.347L46.24 58.713 58.15 38.08h23.825l11.913 20.633-11.913 20.634H58.15z'/%3E%3Cpath d='M58.15 79.347L81.976 38.08v41.267H58.15z' opacity='.3'/%3E%3Cpath d='M58.15 38.08l23.825 41.267H58.15V38.08z' opacity='.3'/%3E%3Cpath d='M46.238 58.713h47.65L58.15 38.08 46.24 58.713z' opacity='.3'/%3E%3C/g%3E%3C/svg%3E")`
      }
    case "transform":
      return {
        color: '#C37DCF',
        backgroundImage: `url("data:image/svg+xml,%3Csvg width='35' height='48' viewBox='0 0 35 48' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eshape4%3C/title%3E%3Cdefs%3E%3ClinearGradient x1='121.384%25' y1='82.979%25' x2='77.946%25' y2='100%25' id='a'%3E%3Cstop stop-color='%23C37DCF' offset='0%25'/%3E%3Cstop stop-color='%239D3CAD' offset='100%25'/%3E%3C/linearGradient%3E%3C/defs%3E%3Cg transform='translate%28-54 -157%29' fill='url%28%23a%29' fill-rule='evenodd'%3E%3Cpath d='M60.685 205L54 184.253 71.5 157 89 184.253 82.314 205h-21.63z'/%3E%3Cpath d='M71.5 157l-10.938 48H82.44L71.5 157z' opacity='.3'/%3E%3Cpath d='M54 183.182h35L60.703 203.91 54 183.18z' opacity='.3'/%3E%3Cpath d='M82.438 205L71.55 157 54 184.253 82.437 205z' opacity='.3'/%3E%3C/g%3E%3C/svg%3E")`
      }
    case "ml":
      return {
        color: '#0297E6',
        backgroundImage: `url("data:image/svg+xml,%3Csvg width='46' height='44' viewBox='0 0 46 44' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eshape1%3C/title%3E%3Cdefs%3E%3ClinearGradient x1='215.307%25' y1='-28.149%25' y2='123.676%25' id='a'%3E%3Cstop stop-color='%233023AE' offset='0%25'/%3E%3Cstop stop-color='%2300A8FF' offset='100%25'/%3E%3C/linearGradient%3E%3C/defs%3E%3Cg transform='translate%28-47 -281%29' fill='url%28%23a%29' fill-rule='evenodd'%3E%3Cpath d='M55.932 324.384l-8.698-26.767 22.77-16.542 22.768 16.542-8.698 26.767H55.932z'/%3E%3Cpath d='M70.003 281.075l-14.07 43.31h28.14l-14.07-43.31z' opacity='.3'/%3E%3Cpath d='M47.234 297.617h45.538l-36.815 27.23-8.723-27.23z' opacity='.3'/%3E%3Cpath d='M84.13 324.384l-14.127-43.31-22.77 16.543 36.897 26.767z' opacity='.3'/%3E%3C/g%3E%3C/svg%3E")`
      }
    case "join-split":
      return {
        color: '#876CF8',
        backgroundImage: `url("data:image/svg+xml,%3Csvg width='48' height='38' viewBox='0 0 48 38' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eshape6%3C/title%3E%3Cdefs%3E%3ClinearGradient x1='-59.049%25' y1='-8.486%25' x2='85.307%25' y2='67.832%25' id='a'%3E%3Cstop stop-color='%239988DE' offset='0%25'/%3E%3Cstop stop-color='%234726CC' offset='100%25'/%3E%3C/linearGradient%3E%3C/defs%3E%3Cg transform='translate%28-46 -406%29' fill='url%28%23a%29' fill-rule='evenodd'%3E%3Cpath d='M70.353 444.062l-24.32-18.495v-19.48l24.32 17.862v20.112z'/%3E%3Cpath d='M70.353 444.062l-24.32-37.975 24.32 17.862v20.112z' opacity='.3'/%3E%3Cpath d='M46.033 425.567l24.32-1.618v20.112l-24.32-18.495z' opacity='.3'/%3E%3Cg%3E%3Cpath d='M70.353 444.062l23.536-18.495v-19.48L70.352 423.95v20.112z'/%3E%3Cpath d='M70.353 444.062l23.536-37.975-23.537 17.862v20.112z' opacity='.3'/%3E%3Cpath d='M93.89 425.567l-23.537-1.618v20.112l23.536-18.495z' opacity='.3'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E")`
      }
    case "visualize":
      return {
        color: '#FF379C',
        backgroundImage: `url("data:image/svg+xml,%3Csvg width='45' height='51' viewBox='0 0 45 51' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3EGroup%3C/title%3E%3Cdefs%3E%3ClinearGradient x1='50.028%25' y1='-77.64%25' x2='50.028%25' y2='101.407%25' id='a'%3E%3Cstop stop-color='%23FF80BC' offset='0%25'/%3E%3Cstop stop-color='%23FF0084' offset='100%25'/%3E%3C/linearGradient%3E%3ClinearGradient x1='50.057%25' y1='-61.786%25' x2='50.057%25' y2='117.261%25' id='b'%3E%3Cstop stop-color='%23FFF' offset='0%25'/%3E%3Cstop stop-color='%23FF1D91' offset='100%25'/%3E%3C/linearGradient%3E%3ClinearGradient x1='50.028%25' y1='-61.871%25' x2='50.028%25' y2='117.312%25' id='c'%3E%3Cstop stop-color='%23FF2B97' offset='0%25'/%3E%3Cstop stop-color='%23CBF7FC' offset='100%25'/%3E%3C/linearGradient%3E%3C/defs%3E%3Cg fill='none' fill-rule='evenodd'%3E%3Cpath d='M12.73 43.373L.235 21.72 12.73.1h24.996l12.497 21.62-12.497 21.653H12.73z' fill='url%28%23a%29' transform='rotate%2830 16.536 18.402%29'/%3E%3Cpath d='M12.73 43.373L37.727.1v43.273H12.73z' fill='url%28%23b%29' opacity='.3' transform='rotate%2830 16.536 18.402%29'/%3E%3Cpath d='M50.223 21.486H.233l37.493 21.62 12.497-21.62z' fill='url%28%23c%29' opacity='.3' transform='rotate%2830 16.536 18.402%29'/%3E%3C/g%3E%3C/svg%3E")`
      }
    case "script":
      return {
        color: '#AE3ED4',
        backgroundImage: `url("data:image/svg+xml,%3Csvg width='28' height='48' viewBox='0 0 28 48' xmlns='http://www.w3.org/2000/svg'%3E%3Ctitle%3Eshape5%3C/title%3E%3Cdefs%3E%3ClinearGradient x1='0%25' y1='0%25' x2='102%25' y2='101%25' id='a'%3E%3Cstop stop-color='%233023AE' offset='0%25'/%3E%3Cstop stop-color='%23C96DD8' offset='100%25'/%3E%3C/linearGradient%3E%3C/defs%3E%3Cg transform='translate%28-57 -645%29' fill='url%28%23a%29' fill-rule='evenodd'%3E%3Cpath d='M57.244 657l13.572-11.987L84.386 657v23.967l-13.57 11.985-13.572-11.985V657z'/%3E%3Cpath d='M57.244 657l27.14 23.967h-27.14V657z' opacity='.3'/%3E%3Cpath d='M84.385 657l-27.14 23.967V657h27.14z' opacity='.3'/%3E%3Cpath d='M70.816 645.013v47.94l13.57-35.954-13.57-11.987z' opacity='.3'/%3E%3C/g%3E%3C/svg%3E")`
      }
    default:
      return {
        color: 'black',
        backgroundImage: 'none'
      }
  }
}

var cleanArray = function(array) {
  for (var i = 0; i < array.length; i++) {
    if (array[i] === null || array[i] === undefined) {
      array.splice(i, 1);
      i--;
    }
  }
  return array;
}

export const nofloToKieler = function (_graph, direction) {

  // convert graph to the noflo format
  const graph = Object.assign({ nodes: [], edges: [] }, _graph)

  const portInfo = {}

  Object.keys(graph.processes).forEach(id => {
    const node = graph.processes[id]
    node.id = id
    node.metadata.height = 100
    node.metadata.width = 100
    graph.nodes.push(node)
    portInfo[id] = {
      inports: {},
      outports: {}
    }
    const ports = portInfo[id]
    node.ports.inputs.forEach((portName, idx) => {
      ports.inports[portName] = {
        label: portName,
        route: 0,
        type: "all",
        x: 0,
        y: 10*idx
      }
    })
    node.ports.outputs.forEach((portName, idx) => {
      ports.outports[portName] = {
        label: portName,
        route: 0,
        type: "all",
        x: 100,
        y: 10*idx
      }
    })
  })

  graph.edges = graph.connections.map(connection => {
    return {
      metadata: {
        route: connection.metadata.route
      },
      from: {
        node: connection.src.process,
        port: connection.src.port
      },
      to: {
        node: connection.tgt.process,
        port: connection.tgt.port
      }
    }
  })

  // Default direction is left to right
  direction = direction || 'RIGHT';
  var portConstraints = 'FIXED_POS';
  // Default port and node properties
  var portProperties = {
    inportSide: 'WEST',
    outportSide: 'EAST',
    width: 10,
    height: 10
  };
  if (direction === 'DOWN') {
    portProperties.inportSide = 'NORTH';
    portProperties.outportSide = 'SOUTH';
  }
  var nodeProperties = {
    width: 100,
    height: 100
  };
  // Start KGraph building
  var kGraph = {
    id: graph.name,
    children: [],
    edges: []
  };
  // Encode nodes
  var nodes = graph.nodes;
  var idx = {};
  var countIdx = 0;
  var nodeChildren = nodes.map(function (node) {
    var inPorts = portInfo[node.id].inports;
    var inPortsKeys = Object.keys(inPorts);
    var inPortsTemp = inPortsKeys.map(function (key) {
      return {
        id: node.id + '_' + key,
        width: portProperties.width,
        height: portProperties.height,
        x: portInfo[node.id].inports[key].x - portProperties.width,
        y: portInfo[node.id].inports[key].y
      };
    });
    var outPorts = portInfo[node.id].outports;
    var outPortsKeys = Object.keys(outPorts);
    var outPortsTemp = outPortsKeys.map(function (key) {
      return {
        id: node.id + '_' + key,
        width: portProperties.width,
        height: portProperties.height,
        x: portInfo[node.id].outports[key].x,
        y: portInfo[node.id].outports[key].y
      };
    });

    var kChild = {
      id: node.id,
      labels: [{text: node.metadata.label}],
      width: nodeProperties.width,
      height: nodeProperties.height,
      ports: inPortsTemp.concat(outPortsTemp),
      properties: {
        'portConstraints': portConstraints
      }
    };
    idx[node.id] = countIdx++;
    return kChild;
  });

  // Graph i/o to kGraph nodes
  var inports = graph.inports;
  var inportsKeys = Object.keys(inports);
  var inportChildren = inportsKeys.map(function(key){
    var inport = inports[key];
    var tempId = "inport:::"+key;
    // Inports just has only one output port
    var uniquePort = {
      id: inport.port,
      width: portProperties.width,
      height: portProperties.height,
      properties: {
        'de.cau.cs.kieler.portSide': portProperties.outportSide
      }
    };

    var kChild = {
      id: tempId,
      labels: [{text: key}],
      width: nodeProperties.width,
      height: nodeProperties.height,
      ports: [uniquePort],
      properties: {
        'portConstraints': portConstraints,
        "de.cau.cs.kieler.klay.layered.layerConstraint": "FIRST_SEPARATE"
      }
    };
    idx[tempId] = countIdx++;
    return kChild;
  });
  var outports = graph.outports;
  var outportsKeys = Object.keys(outports);
  var outportChildren = outportsKeys.map(function(key){
    var outport = outports[key];
    var tempId = "outport:::"+key;
    // Outports just has only one input port
    var uniquePort = {
      id: outport.port,
      width: portProperties.width,
      height: portProperties.height,
      properties: {
        'de.cau.cs.kieler.portSide': portProperties.inportSide
      }
    };

    var kChild = {
      id: tempId,
      labels: [{text: key}],
      width: nodeProperties.width,
      height: nodeProperties.height,
      ports: [uniquePort],
      properties: {
        'portConstraints': portConstraints,
        "de.cau.cs.kieler.klay.layered.layerConstraint": "LAST_SEPARATE"
      }
    };
    idx[tempId] = countIdx++;
    return kChild;
  });

  // Combine nodes, inports, outports to one array
  kGraph.children = nodeChildren.concat(inportChildren, outportChildren);

  // Encode edges (together with ports on both edges and already
  // encoded nodes)
  var currentEdge = 0;
  var edges = graph.edges;
  edges.map(function (edge) {
    if (edge.data !== undefined) {
      return;
    }
    var source = edge.from.node;
    var sourcePort = edge.from.port;
    var target = edge.to.node;
    var targetPort = edge.to.port;
    kGraph.edges.push({
      id: 'e' + currentEdge++,
      source: source,
      sourcePort: source + '_' + sourcePort,
      target: target,
      targetPort: target + '_' + targetPort
    });
  });

  // Graph i/o to kGraph edges
  var inportEdges = inportsKeys.map(function (key) {
    var inport = inports[key];
    var source = "inport:::"+key;
    var sourcePort = key;
    var target = inport.process;
    var targetPort = inport.port;
    var inportEdge = {
      id: 'e' + currentEdge++,
      source: source,
      sourcePort: source + '_' + sourcePort,
      target: target,
      targetPort: target + '_' + targetPort
    };
    return inportEdge;
  });
  var outportEdges = outportsKeys.map(function (key) {
    var outport = outports[key];
    var source = outport.process;
    var sourcePort = outport.port;
    var target = "outport:::"+key;
    var targetPort = key;
    var outportEdge = {
      id: 'e' + currentEdge++,
      source: source,
      sourcePort: source + '_' + sourcePort,
      target: target,
      targetPort: target + '_' + targetPort
    };
    return outportEdge;
  });

  // Combine edges, inports, outports to one array
  kGraph.edges = kGraph.edges.concat(inportEdges, outportEdges);

  // Encode groups
  var groups = graph.groups;
  var countGroups = 0;
  // Mark the nodes already in groups to avoid the same node in many groups
  var nodesInGroups = [];
  groups.map(function (group) {
    // Create a node to use as a subgraph
    var node = {
      id: 'group' + countGroups++,
      children: [],
      edges: []
    };
    // Build the node/subgraph
    group.nodes.map(function (n) {
      var nodeT = kGraph.children[idx[n]];
      if (nodeT === null) {
        return;
      }
      if (nodesInGroups.indexOf(nodeT) >= 0) {
        return;
      }
      nodesInGroups.push(nodeT);
      node.children.push(nodeT);
      node.edges.push(kGraph.edges.filter(function (edge) {
        if (edge) {
          if ((edge.source === n) || (edge.target === n)) {
            return edge;
          }
        }
      })[0]);
      node.edges = cleanArray(node.edges);

      // Mark nodes inside the group to be removed from the graph
      kGraph.children[idx[n]] = null;

    });
    // Mark edges too
    node.edges.map(function (edge) {
      if (edge) {
        kGraph.edges[parseInt(edge.id.substr(1), 10)] = null;
      }
    });
    // Add node/subgraph to the graph
    kGraph.children.push(node);
  });

  // Remove the nodes and edges from the graph, just preserve them
  // inside the subgraph/group
  kGraph.children = cleanArray(kGraph.children);
  kGraph.edges = cleanArray(kGraph.edges);

  return kGraph;
}
