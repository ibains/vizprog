package spark.jobserver

import org.json4s._

object Interpreter {

  abstract class ExecuteResponse

  case class ExecuteSuccess(content: String, `type`: String) extends ExecuteResponse
  case class ExecuteError(ename: String,
                          evalue: String,
                          traceback: Seq[String] = Seq()) extends ExecuteResponse
  case class ExecuteIncomplete() extends ExecuteResponse
  case class ExecuteAborted(ename: String,
                            evalue: String,
                            traceback: Seq[String] = Seq()) extends ExecuteResponse
  case object TypesDoNotMatch extends Exception
  case object InvalidInterpreter extends Exception
}

object MimeTypes {
  type MimeTypeMap = List[JField]

  val APPLICATION_JSON = "application/json"
  val APPLICATION_TABLE_JSON = "application/vnd.table.v1+json"
  val IMAGE_PNG = "image/png"
  val TEXT_PLAIN = "text/plain"
}