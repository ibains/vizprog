package spark.jobserver

import org.apache.spark.repl.SparkIMain
import spark.jobserver.Interpreter.ExecuteResponse

/**
  * Represents an interpreter based on SparkIMain.
  *
  * The Job Server can spin up any Context with any interpreter type enabled.
  */
trait InterpreterLike {

  /**
    * The underlying Interpreter
    */
  def intp: SparkIMain

  /**
    * The Class server uri to be used by the context.
    */
  def classServerUri: String

  /**
    * Responsible for performing any cleanup, including calling the underlying context's
    * stop method.
    */
  def stop(): Unit

  /**
    * @return true if the interpreter is stopped or in the midst of stopping.
    */
  def isStopped: Boolean

  /**
    * Executes the code on to the interpreter.
    *
    * @param code The Code statement to be executed on the interpreter.
    */
  def execute(code: String): ExecuteResponse

  def loop()
}
