package spark.jobserver.context

import java.io.{ByteArrayOutputStream, File}
import java.net.{URI, URISyntaxException}

import org.apache.spark.rdd.RDD
import org.json4s.JsonAST._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import org.apache.commons.lang3.SystemUtils
import org.apache.spark._
import org.apache.spark.repl.{SparkILoop, SparkIMain}
import org.json4s.{Extraction, DefaultFormats}
import org.slf4j.LoggerFactory
import spark.jobserver.Interpreter._
import spark.jobserver.{MimeTypes, InterpreterLike}

import scala.tools.nsc.interpreter.{InteractiveReader, Results, JPrintWriter}
import scala.tools.nsc.util.ClassPath
import scala.tools.nsc.{Properties, Settings, SparkHelper}

/**
  * @param nativeClasspath identifies the mandatory classpath to support native platform support.
  *
  */
class SparkInterpreter(nativeClasspath: String) extends InterpreterLike {

  private implicit def formats = DefaultFormats

  private val MAGIC_REGEX = "^%(\\w+)\\W*(.*)".r

  //  Logger
  private val logger = LoggerFactory.getLogger(classOf[SparkInterpreter])

  private val outputStream = new ByteArrayOutputStream()
  private val sparkILoopOut = new JPrintWriter(outputStream)

  var intp: SparkIMain = _
  var stopped: Boolean = false

  /**
    * Another extension point that could be used to add extra classpath added to job server.
    *
    */
  private def getAddedJars: Array[String] = {
    val envJars = sys.env.get("ADD_JARS")
    if (envJars.isDefined) {
      logger.warn("ADD_JARS environment variable is deprecated, use --jar spark submit argument instead")
    }
    val propJars = sys.props.get("spark.jars").flatMap { p => if (p == "") None else Some(p) }
    val jars = propJars.orElse(envJars).getOrElse("")
    Utils.resolveURIs(jars).split(",").filter(_.nonEmpty)
  }

  private def nativeClasspathJars: Array[String] = {
    new File(nativeClasspath).listFiles.filter(_.getName.endsWith(".jar")).map(_.getAbsolutePath)
  }

  /**
    * Constructs a new interpreter.
    */
  protected def createInterpreter() {

    // Spark Classpath settings.
    val settings = new Settings()
    settings.usejavacp.value = true

    // Extension point to be used to attach custom dependencies provided.
    //if (addedClasspath != "") settings.classpath.append(addedClasspath)
    val addedJars =
      if (Utils.isWindows) {
        // Strip any URI scheme prefix so we can add the correct path to the classpath
        // e.g. file:/C:/my/path.jar -> C:/my/path.jar
        getAddedJars.map { jar => new URI(jar).getPath.stripPrefix("/") }
      } else {
        // We need new URI(jar).getPath here for the case that `jar` includes encoded white space (%20).
        getAddedJars.map { jar => new URI(jar).getPath }
      }

    // work around for Scala bug
    val totalClassPath = (addedJars ++ nativeClasspathJars).foldLeft(
      settings.classpath.value)((l, r) => ClassPath.join(l, r))
    settings.classpath.value = totalClassPath

    intp = new SparkILoopInterpreter(settings, sparkILoopOut)
  }

  /**
    * Executes the code on to the interpreter.
    *
    * @param code The Code statement to be executed on the interpreter.
    */
  override def execute(code: String): ExecuteResponse = {

    require(intp != null)

    logger.info("Executing the Code to the Spark Interpreter.")
    executeLines(code.trim.split("\n").toList, ExecuteSuccess("", MimeTypes.TEXT_PLAIN))
  }

  private def executeLines(lines: List[String], result: ExecuteResponse): ExecuteResponse = {
    lines match {
      case Nil => result
      case head :: tail =>
        val result = executeLine(head)

        result match {
          case ExecuteIncomplete() =>
            tail match {
              case Nil =>
                result

              case next :: nextTail =>
                executeLines(head + "\n" + next :: nextTail, result)
            }
          case ExecuteError(_, _, _) =>
            result

          case _ =>
            executeLines(tail, result)
        }
    }
  }

  private def executeLine(code: String): ExecuteResponse = {

    logger.info(s"Executing line: $code")
    code match {
      case MAGIC_REGEX(magic, rest) =>
        executeMagic(magic, rest)
      case _ =>
        scala.Console.withOut(outputStream) {
          intp.interpret(code) match {
            case Results.Success =>
              ExecuteSuccess(readStdout(), MimeTypes.TEXT_PLAIN)
            case Results.Incomplete => ExecuteIncomplete()
            case Results.Error => ExecuteError("Error", readStdout())
          }
        }
    }
  }

  private def readStdout() = {
    val output = outputStream.toString("UTF-8").trim
    outputStream.reset()

    output
  }

  private var in: InteractiveReader = _   // the input stream from which commands come


  /** The main read-eval-print loop for the repl.  It calls
    *  command() for each line of input, and stops when
    *  command() returns false.
    */
  def loop() {
    def readOneLine() = {
      outputStream.flush()
      in readLine Properties.shellPromptString
    }
    // return false if repl should exit
    def processLine(line: String) = {
      executeLine(line)
    }

    def innerLoop() {
      val shouldContinue = try {
        processLine(readOneLine())
      } catch {case t: Throwable => logger.warn("")}
      innerLoop()
    }

    innerLoop()
  }

  private def executeMagic(magic: String, rest: String): ExecuteResponse = {
    magic match {
      case "json" => executeJsonMagic(rest)
      case "table" => executeTableMagic(rest)
      case _ =>
        ExecuteError("UnknownMagic", f"Unknown magic command $magic")
    }
  }

  private def executeJsonMagic(name: String): ExecuteResponse = {
    try {
      val value = intp.valueOfTerm(name) match {
        case Some(obj: RDD[_]) => obj.asInstanceOf[RDD[_]].take(10)
        case Some(obj) => obj
        case None => return ExecuteError("NameError", f"Value $name does not exist")
      }

      ExecuteSuccess(compact(Extraction.decompose(value)), MimeTypes.APPLICATION_JSON)
    } catch {
      case _: Throwable =>
        ExecuteError("ValueError", "Failed to convert value into a JSON value")
    }
  }

  private def executeTableMagic(name: String): ExecuteResponse = {
    val value = intp.valueOfTerm(name) match {
      case Some(obj: RDD[_]) => obj.asInstanceOf[RDD[_]].take(10)
      case Some(obj) => obj
      case None => return ExecuteError("NameError", f"Value $name does not exist")
    }

    extractTableFromJValue(Extraction.decompose(value))
  }

  private def extractTableFromJValue(value: JValue): ExecuteResponse = {
    // Convert the value into JSON and map it to a table.
    val rows: List[JValue] = value match {
      case JArray(arr) => arr
      case _ => List(value)
    }

    try {
      val headers = scala.collection.mutable.Map[String, Map[String, String]]()

      val data = rows.map { case row =>
        val cols: List[JField] = row match {
          case JArray(arr: List[JValue]) =>
            arr.zipWithIndex.map { case (v, index) => JField(index.toString, v) }
          case JObject(obj) => obj.sortBy(_._1)
          case value: JValue => List(JField("0", value))
        }

        cols.map { case (k, v) =>
          val typeName = convertTableType(v)

          headers.get(k) match {
            case Some(header) =>
              if (header.get("type").get != typeName) {
                throw TypesDoNotMatch
              }
            case None =>
              headers.put(k, Map(
                "type" -> typeName,
                "name" -> k
              ))
          }

          v
        }
      }

      ExecuteSuccess(compact(
        Extraction.decompose(
          ("headers" -> headers.toSeq.sortBy(_._1).map(_._2)) ~ ("data" -> data)
        )
      ), MimeTypes.APPLICATION_TABLE_JSON)
    } catch {
      case TypesDoNotMatch =>
        ExecuteError("TypeError", "table rows have different types")
    }
  }

  private def convertTableType(value: JValue): String = {
    value match {
      case (JNothing | JNull) => "NULL_TYPE"
      case JBool(_) => "BOOLEAN_TYPE"
      case JString(_) => "STRING_TYPE"
      case JInt(_) => "BIGINT_TYPE"
      case JDouble(_) => "DOUBLE_TYPE"
      case JDecimal(_) => "DECIMAL_TYPE"
      case JArray(arr) =>
        if (allSameType(arr.iterator)) {
          "ARRAY_TYPE"
        } else {
          throw TypesDoNotMatch
        }
      case JObject(obj) =>
        if (allSameType(obj.iterator.map(_._2))) {
          "MAP_TYPE"
        } else {
          throw TypesDoNotMatch
        }
    }
  }

  private def allSameType(values: Iterator[JValue]): Boolean = {
    if (values.hasNext) {
      val type_name = convertTableType(values.next())
      values.forall { case value => type_name.equals(convertTableType(value)) }
    } else {
      true
    }
  }

  /**
    * Exposes the class server uri.
    *
    */
  def classServerUri: String = intp.classServerUri

  /**
    * Responsible for performing any cleanup, including calling the underlying context's
    * stop method.
    */
  override def stop(): Unit = {
    intp.close()
    stopped = true
  }

  /**
    * Initializes the Spark Interpreter
    */
  def init() {
    logger.info("Creating the Spark Interpreter.")
    createInterpreter()

    logger.debug("Initializing the Spark Interpreter.")
    intp.initializeSynchronous()
  }

  /**
    * Post initialization step.
    *
    */
  def postInit(sparkContext: SparkContext): Unit = {
    intp.beQuietDuring {
      intp.bind("sc", "org.apache.spark.SparkContext", sparkContext, List("""@transient"""))
    }
  }

  def printWelcome() {
    echo("""Welcome to
      ____              __
     / __/__  ___ _____/ /__
    _\ \/ _ \/ _ `/ __/  '_/
   /___/ .__/\_,_/_/ /_/\_\   version %s
      /_/
         """.format(SPARK_VERSION))
    import Properties._
    val welcomeMsg = "Using Scala %s (%s, Java %s)".format(
      versionString, javaVmName, javaVersion)
    echo(welcomeMsg)
    echo("Type in expressions to have them evaluated.")
    echo("Type :help for more information.")
  }

  private def echo(msg: String) = {
    sparkILoopOut println msg
    sparkILoopOut.flush()
  }

  class SparkILoopInterpreter(settings: Settings, out: JPrintWriter) extends SparkIMain(settings, out) {
    outer =>
    override protected def parentClassLoader = SparkHelper.explicitParentLoader(settings)
      .getOrElse(classOf[SparkInterpreter].getClassLoader)
  }

  object Utils {

    /**
      * Return a well-formed URI for the file described by a user input string.
      *
      * If the supplied path does not contain a scheme, or is a relative path, it will be
      * converted into an absolute path with a file:// scheme.
      */
    def resolveURI(path: String): URI = {
      try {
        val uri = new URI(path)
        if (uri.getScheme() != null) {
          return uri
        }
        // make sure to handle if the path has a fragment (applies to yarn
        // distributed cache)
        if (uri.getFragment() != null) {
          val absoluteURI = new File(uri.getPath()).getAbsoluteFile().toURI()
          return new URI(absoluteURI.getScheme(), absoluteURI.getHost(), absoluteURI.getPath(),
            uri.getFragment())
        }
      } catch {
        case e: URISyntaxException =>
      }
      new File(path).getAbsoluteFile().toURI()
    }

    /** Resolve a comma-separated list of paths. */
    def resolveURIs(paths: String): String = {
      if (paths == null || paths.trim.isEmpty) {
        ""
      } else {
        paths.split(",").map { p => Utils.resolveURI(p) }.mkString(",")
      }
    }

    /**
      * Whether the underlying operating system is Windows.
      */
    val isWindows = SystemUtils.IS_OS_WINDOWS

    /**
      * Whether the underlying operating system is Mac OS X.
      */
    val isMac = SystemUtils.IS_OS_MAC_OSX
  }

  /**
    * @return true if interpreter is stopped or in the midst of stopping.
    */
  def isStopped: Boolean = stopped
}
