package spark.jobserver.context

import com.typesafe.config.Config
import org.apache.spark.{SparkConf, SparkContext}
import spark.jobserver.util.SparkJobUtils
import spark.jobserver.{ContextLike, InterpreterLike, SparkJob, SparkJobBase}

/**
 * Factory trait for creating a SparkContext or any derived Contexts,
 * such as SQLContext, StreamingContext, HiveContext, etc.
 * My implementing classes can be dynamically loaded using classloaders to ensure that the entire
 * SparkContext has access to certain dynamically loaded classes, for example, job jars.
 */
trait SparkContextFactory {
  import SparkJobUtils._

  type C <: ContextLike

  /**
   * Creates a SparkContext or derived context.
    *
    * @param sparkConf the Spark Context configuration.
   * @param config the context config
   * @param contextName the name of the context to start
   * @return the newly created context.
   */
  def makeContext(sparkConf: SparkConf, config: Config,  contextName: String): C

  /**
   * Creates a SparkContext or derived context.
    *
    * @param config the overall system / job server Typesafe Config
   * @param contextConfig the config specific to this particular context
   * @param contextName the name of the context to start
   * @return the newly created context.
   */
  def makeContext(config: Config, contextConfig: Config, contextName: String): C = {
    val sparkConf = configToSparkConf(config, contextConfig, contextName)
    makeContext(sparkConf, contextConfig, contextName)
  }
}

object DefaultSparkContextFactory {
  // This is a test class added for verifying an end to end execution.
  // Make sure you make spark dependencies as compiled for executing these.
  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("Spark Shell").setMaster("local[2]")
    val intp = new DefaultSparkContextFactory().makeIntpSparkContext(
      sparkConf, "/app/dev/lib")
    val code =
      """
         val out_n2 = sc.textFile("s3://S3_BUCKET/examples/data/user_artists.csv")
        |out_n2.take(2).foreach(println)
      """.stripMargin
    println(intp.sparkIntp.get.execute(code))
  }
}

/**
 * The default factory creates a standard SparkContext.
 * In the future if we want to add additional methods, etc. then we can have additional factories.
 * For example a specialized SparkContext to manage RDDs in a user-defined way.
 *
 * If you create your own SparkContextFactory, please make sure it has zero constructor args.
 */
class DefaultSparkContextFactory extends SparkContextFactory {

  type C = SparkContext with ContextLike

  def makeContext(sparkConf: SparkConf, config: Config,  contextName: String): C = {
    val developerMode = config.getBoolean("developer-mode")
    val additionalClasspath = config.getString("libs-native")
    val sc = if(developerMode) {
      makeIntpSparkContext(sparkConf, additionalClasspath)
    } else {
      makeSparkContext(sparkConf)
    }
    for ((k, v) <- SparkJobUtils.getHadoopConfig(config)) sc.hadoopConfiguration.set(k, v)
    sc
  }

  def makeIntpSparkContext(sparkConf: SparkConf, classpath: String): C = {
    val interpreter = new SparkInterpreter(classpath)
    interpreter.init()
    sparkConf.set("spark.repl.class.uri", interpreter.classServerUri)
    val sc = new SparkContext(sparkConf) with ContextLike {
      def sparkContext: SparkContext = this
      override def sparkIntp: Option[InterpreterLike] = Some(interpreter)
      def isValidJob(job: SparkJobBase): Boolean = job.isInstanceOf[SparkJob]
      override def stop(): Unit = {
        super.stop()
        interpreter.stop()
      }
    }
    interpreter.postInit(sc)
    sc
  }

  def makeSparkContext(sparkConf: SparkConf): C = {
    new SparkContext(sparkConf) with ContextLike {
      def sparkContext: SparkContext = this
      def isValidJob(job: SparkJobBase): Boolean = job.isInstanceOf[SparkJob]
    }
  }
}
