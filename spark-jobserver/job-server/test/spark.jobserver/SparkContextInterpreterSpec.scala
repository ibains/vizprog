package spark.jobserver

import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FunSpec, Matchers}
import spark.jobserver.Interpreter.ExecuteSuccess
import spark.jobserver.context.{DefaultSparkContextFactory, SparkContextFactory}

object SparkContextInterpreterSpec {
  val config = ConfigFactory.parseString("""
    spark {
      master = "local[2]"
      temp-contexts {
        num-cpu-cores = 4           # Number of cores to allocate.  Required.
        memory-per-node = 512m      # Executor memory per node, -Xmx style eg 512m, 1G, etc.
      }
      jobserver.job-result-cache-size = 100
      jobserver.context-creation-timeout = 5 s
      jobserver.yarn-context-creation-timeout = 40 s
      jobserver.named-object-creation-timeout = 60 s
      contexts {
        olap-demo {
          num-cpu-cores = 4
          memory-per-node = 512m
        }
      }

      context-settings {
        num-cpu-cores = 2
        memory-per-node = 512m
        developer-mode = false
        context-factory = spark.jobserver.context.DefaultSparkContextFactory
        passthrough {
          spark.driver.allowMultipleContexts = true
          spark.ui.enabled = false
        }
        hadoop {
          mapreduce.framework.name = "ayylmao"
        }
      }
    }
    akka.log-dead-letters = 0
                                         """)

  val contextConfigIntpEnabled = ConfigFactory.parseString("""
    num-cpu-cores = 2
    memory-per-node = 512m
    developer-mode = true
    context-factory = spark.jobserver.context.DefaultSparkContextFactory
    passthrough {
      spark.driver.allowMultipleContexts = true
      spark.ui.enabled = false
    }
    hadoop {
      mapreduce.framework.name = "ayylmao"
    }
                                                          """)

  val contextConfigIntpDisabled = ConfigFactory.parseString("""
    num-cpu-cores = 2
    memory-per-node = 512m
    developer-mode = false
    context-factory = spark.jobserver.context.DefaultSparkContextFactory
    passthrough {
      spark.driver.allowMultipleContexts = true
      spark.ui.enabled = false
    }
    hadoop {
      mapreduce.framework.name = "ayylmao"
    }
                                                           """)
}

class SparkContextInterpreterSpec extends FunSpec with Matchers with BeforeAndAfter with BeforeAndAfterAll{

  import SparkContextInterpreterSpec._

  var contextWithIntp: ContextLike = _
  var contextWithoutIntp: ContextLike = _
  var sparkContextFactory: SparkContextFactory = _


  before {
    contextWithIntp = sparkContextFactory
      .makeContext(config, contextConfigIntpEnabled, "SparkContextWithInterpreter")
    contextWithoutIntp = sparkContextFactory
      .makeContext(config, contextConfigIntpDisabled, "SparkContextWithoutInterpreter")
  }

  describe("Spark Context with Interpreter") {
    it("should indicate that the Context is interpreter enabled") {
      contextWithIntp.isIntpEnabled() shouldBe true
    }

    it("should have a valid Interpreter") {
        contextWithIntp.sparkIntp should not be None
    }

    it("should have a valid Spark Context") {
      contextWithIntp.sparkContext should not be null
      contextWithIntp.sparkContext.isLocal shouldBe true
    }

    it("can stop Spark Context and Interpreter") {
      contextWithIntp.sparkContext.isStopped shouldBe false
      contextWithIntp.sparkIntp.get.isStopped shouldBe false
      contextWithIntp.stop()
      contextWithIntp.sparkContext.isStopped shouldBe true
      contextWithIntp.sparkIntp.get.isStopped shouldBe true
    }

    it("can work on simple word count distributed job") {
      val code =
        """
          import org.apache.spark.SparkContext._
          val source = Array(("a", 1), ("b", 1), ("a", 1), ("a", 1), ("b", 1), ("b", 1), ("b", 1), ("b", 1))
          val x = sc.parallelize(source, 1)
          val y = x.reduceByKey(_ + _)
          y.collect
          val n = 2+3
        """.stripMargin
      contextWithIntp.sparkIntp.get.loop()
      val execResponse = contextWithIntp.sparkIntp.get.execute(code)
      execResponse shouldBe a [ExecuteSuccess]
      execResponse match {
        case ExecuteSuccess(content, mimeType) =>
          println(content)
        case _ =>
          println("Error")
      }
    }
  }

  describe("Spark Context without Interpreter") {
    it("should indicate that the Context is not interpreter enabled") {
      contextWithoutIntp.isIntpEnabled() shouldBe false
    }

    it("should not contain an interpreter") {
      contextWithoutIntp.sparkIntp shouldBe None
    }
  }

  override protected def beforeAll(): Unit = {
    sparkContextFactory = new DefaultSparkContextFactory
  }
}
